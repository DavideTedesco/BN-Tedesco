# Appunti della lezione di martedì 26 luglio 2022

## Perchè conoscere l'origine di un individuo?

Per capire e definire la conoscenza nei riguardi della voce di un individuo.

Edgard Morine scritti sulla complessità e la difficoltà nei gruppi corali.

## Sviluppo ed evoluzione

Essi sono due cose diverse:
- sviluppo -> fisiologia
- evoluzione ->studio

Vi è quindi una differenza fra i due termini. Quando si sviluppa si parla di aumento delle dimensioni. Noi umani ci sviluppiamo per un tot di tempo, mentre l'evoluzione è un processo senza fine, che non si ferma mai.

>Un'evoluzione dal punto di vista etico e morale è un processo che riguarda aspetti interiori.

Nell'età dello sviluppo si ha anche uno sviluppo dell'immaginazione.

Le scuole elementari sono infatti una carta bianca, mentre è molto più complessa la situazione per adolescenza e pre-adolescenza.

## Adolescenza e voce

Quando si è più piccoli si hanno voci bianche, con voci con timbro che non si differenzia, in genere si suddivide al massimo in:
- prima
- seconda
- terza voce

Mentre successivamente la voce si delinea e acquisisce un timbro che si differenzia molto.

Il processo che avviene è una differenziazione in base al sesso ed all'allargarsi della propria gola, vi è un timore ad utilizzare la propria voce.

La laringe si irrobustisce cambiando timbro e altezza della voce.

## Caratteristiche corde vocali

Corde rotonde, voci acute, mentre corde vocali piatte sono nastriformi e appartengono a voci più particolari.

>Chi ha cantato da voce bianca poi da adulto saprà fare il falsetto.

Il canto con falsetto è diffuso nei paesi anglosassoni e la formazione corale è infatti molto diffusa nelle scuole (come la materia _English and drama_ insegnata a scuola).

Il perdurare della diffusione musicale è dovuto anche alla presenza della chiesa.

>Italia altissima specializzazione corale di pochi, Inghilterra bassissima specializzazione di molti.

Vi è molta inibizione delle voci maschili nel riguardo del canto.

_Ultimamente i talent hanno fatto fuoriuscire una pratica canora anche maschile nell'età dell'adolescenza.

## Fare con i ragazzi

Si mette in gioco per primo l'insegnante.

## Esercizi per la voce

Si parte in genere con il __riscaldamento vocale__. Si risveglia al mattino la muscolatura facendo in modo che si scaldino i "muscoli".

Dopo il riscaldamento vi sono i __vocalizzi__, che è la parte sonora della nostra voce.

I vocalizzi sono utili quando più riusciamo a legare un vocalizzo al brano che vogliamo fare, tanto più ne gioveremo.

Il docente deve essere una guida per il brano, facendo esercitare con vocalizzi adeguati. Ad esempio inventando i vocalizzi partendo dal brano.


>Alcuni vocalizzi possono essere fuorvianti se non sono legati al brano.

Preparare esercizi preparatori desunti dal brano stesso.

Il canone è un qualcosa che si può definire come popolare, esso era una delle prime forme di canto da realizzare insieme agli altri.

Parole aggiunte alle melodie, per farlo ricordare.

Realizzare esercizi finalizzati a un brano, è un qualcose che va realizzato dal docente.

Studi ed esercizi per strumenti che restano sempre su di un elemento per affinare la tecnica.

### Riscaldamento vocale

1. esercizi di respirazione
2. esercizi con s ed m
3. esercizi con consonanti (appunti scritti a mano)
4. esercizi con le vocali


![1](1.png)
## Canto corale e tradizione

L'estensione della voce cresce con l'età.

Da piccoli l'estensione rimane su 3 suoni:
- profili a dente di sega (con 3 suoni)
- canti popolari con picco e poi discesa (folk)
- canto lirico, picco sugli acuti nel momento di climax (classico)

>Gli etnomusicologi definiscono i canti in modi differenti.

L'estensione vocale deve coincidere con l'estensione vocale:
![2](2.png)

Il _modalismo_,  esiste già nei canti popolari come quelli scritti da Kodaly per l'unificazione di Budapest.


### Musica ascendente e discendente

>Per la voce è naturale realizzare qualcosa di discendente, utilizzati dai greci.

Il canto in musica è stato poi utilizzato per sistemi sonori ascendenti.

### Sistemi sonori di riferimento

Essi sono scale, modi, etc...

Più una società è complessa più diviene complesso il sistema di riferimento.

I sistemi sonori di riferimento in oggetto alternano toni e semitoni.

#### Tetracordi

![tetra](tetra.png)

#### Modi

![Scale_Modali_in_chiave_di_violino.png](Scale_Modali_in_chiave_di_violino.png)

>Utilizzo del do mobile

Differenza note:
- nota reale
- nota funzione

Esse sono scale modali eptafoniche. Ci possiamo basare sulle triadi.

## Evoluzione nella storia della musica

Abbiamo visto Imberty, Morin ed ora vediamo dal testo di Azzaroni, la dimensione orizzontale della musica.

La dissonanza con il coro tende a polarizzarsi.

![3](3.png)

## Settime

![4](4.png)

## Lavoro per gruppi: spiegazione

Tracce modali con gruppo con consegne a basso impatto.

Identificare i modi anche quando non saranno palesi.

Gruppi che dovranno inserire:
- testo
- variazioni ritmico melodiche
- accompagnamento

Questionario osservativo per vedere i risultati.

Realizzare un testo...

## Come ottenere le note dal LA del diapason

Il diapason è una chiave per l'intonazione delle altezze.


---
Fine prima parte
---

## Lavoro di gruppo

Dopo l'ascolto dei lavori di gruppo, valutazione con questionari valutativi.

In classe bisogna andare sulla qualità e serve per capire se l'esperienza è stata significativa.

La programmazione per una classe è a:
- breve termine
- medio termine
- lungo termine

### Discussione dei questionari

>Il signore delle mosche, film

Parole chiave:
- rispetto
- curiosità
- collaborazione
