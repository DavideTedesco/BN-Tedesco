# Appunti della lezione di lunedì 25 luglio 2022

## Informazioni introduttive

### Struttura del corso

- parte passiva
- parte riflessiva

### Raccoglimento di informazioni sui candidati del corso

Quali esperienze abbiamo realizzato con direzione di orchestra o ensemble corali.

### File alla fine del corso

I file sono dei saggi di diversi autori con concetti di autori diversi.

## Il corso

Discpilina con una sua complessità (Edgard orine), essa ha bisogno di:
- abilità
- competenze

Le capacità relazionali o life skills crescono negli adulti e tali skills hanno una ricaduta sulla vocalità.

Giorno 1: vocalità

Giorno 2: vocalità

Giorno 3: progetti

Giorno 4: progettualità che diviene la verifica finale

## Il fine di questa disciplina

Realizzare materiali già esistenti, con apprendimento cooperativo.

## Cos'è il _cooperative learning_?

Differenza fra le discipline e il capire il funzionamento del gruppo è

### Esempio: 4 gruppi di lavoro

Immaginando 4 gruppi di lavoro che lavorano su argomenti diversi correlati ad una questione (come l'inquinamento acustico di una zona).

I gruppi lavorano su 4 elementi differenti ed i 4 gruppi si incrociano e ritornano poi nei gruppi, realizzando una moltiplicazione di conoscenze.

### Cos'è il _pair learning_?

Apprendimento fra pari. Esso è nuovo nella scuola e dovremo rimodulare anche il modo di realizzare la lezione.

_Lettura 1 Apprendimento cooperativo: quando l’educazione è un gioco di squadra_

### Apprendimento cooperativo, fatti chiave

Apprendimento cooperativo:
- apprendimento fra pari
- fiducia reciproca
- chiarezza del pubblico per cui si realizza un progetto
- nella didattica a distanza(invenzione di metodologie)
- reti di relazione
- comunità educativa
- metodi di studio individuale, competitivo,riproduttivo

Vantaggi del _cooperative learning_:
- all'estero vi è una maggiore tradizione di sperimentazione e valutazione
  - sistema scolastico finlandese
    - molti fondi
    - molto territorio
    - risoluzione di problemi immediata

#### Criteri per valutare

Criteri:
- numerici
- qualitativi

È più importante il numero o la qualità?

Bisogna sempre tenere da conto questi due criteri.

### L'importanza della parola "partecipanti"

È importante utilizzare tale termine per non definire strettamente chi essi siano.

### Prassi organizzative per l'apprendimento creativo

- eterogeneità delle caratteristiche degli individui (mettere insieme studenti con studi diversi)
- coinvolgimento attivo di tutti i partecipanti
- reale interdipendenza positiva (far crescere un determinato obiettivo)
- aiuto reciproco
- leadership distribuita (orizzontalità della leadership)
  - in coro
  - in gruppo strumentale

>Musica con carriere manageriali alla luce di leadership distribuita.

Non avere strutture piramidali, i partecipanti devono avere una partecipazione distribuita.

È importante la dimensione relativa e relazionale.

#### Convergenza e divergenza dei corsi

Strumento e studio dello stesso con convergenza, altri elementi con divergenza.

Il pensiero creativo interviene dove vi è un pensiero divergente (come la pasticceria).

Pasticceria -> convergente

Cucina -> divergente

### Il _cooperative learning_ e il modellarsi sul gruppo

Gruppi diversi con gli stessi elementi realizzano qualcosa di diverso.

### I vantaggi dell’apprendimento cooperativo

Dall'articolo.

### Critical thinking

Pensiero creativo, modi per organizzare il pensiero.

#### Pensiero critico e lezioni

gruppo che venne esposto a 3 insegnamenti diversi, tra i docenti vi era Marianne Talbot (Oxford University)-

>Per il Critical thinking vi devono essere due o più premesse progettuali e vere.

#### Pensiero delle macchine

Magno Caliman, insegnante di programmazione con Arduino

#### Contrappunto del '500 vocale

Ambiguità e flessibilità delle regole musicali, si fluttua sulle regole e si possono avere tanti argomenti.

### Quanti pensieri utilizziamo nella musica?

Abbiamo fatto esperienza di queste 3 forme di pensiero in maniera sviluppato.

### I pensieri
Pensiero:
- critico
- creativo
- divergente (rispetto al modello senza passare per un'unica soluzione)

### Valutazione bidirezionale

La valutazione deve essere bidirezionale, e non solo docente->studente

>La valutazione con pochi dati deve essere fatta in maniera qualitativa.

### Apprendimento cooperativo

Esso è un alleato strategico per l'evoluzione:
- tecnologica
- sociale
- culturale

## Canto come strumento interno utile all'unificazione

Esso sta dentro di noi e non solo esterno, non si hanno bisogno di molti investimenti.

L'insegnamento della musica era _musica e canto corale_ essa era:
- musica = teoria
- canto corale = unico momento collettivo

Il canto corale è un qualcosa di economico e veloce da realizzare, anche utile a livello comunitario, e si diceva che l'Italia venne unita dalla "leva obbligatoria e dal canto corale".

Cantando ciascuno abbandonava il dialetto e la pronuncia del testo da cantare.

>Molti testi sul canto corale sono tradotti in italiano.

### Settecento, i trattati e i manuali

Si iniziarono a vedere tutti gli aspetti di una disciplina,facendo allargare la cultura, allargandosi il target dovette scendere.

Nell'Ottocento nascono i manuali per abbassare il livello.

Nel canto abbiamo avuto trattati di Italiani:
- Garcia
- Tosi
- ...

Abbiamo realizzato i trattati per il canto d'opera.

### Vocalità come impronta digitale

Anche i gemelli che hanno la stessa struttura, sviluppano anche voci diverse.

È importante far conoscere agli studenti la propria voce.

>La voce che personalmente ascoltiamo è diversa da quella che ascoltano gli altri.

L'orecchio interno ci porta ad abbassare la frequenza.

## Chi usa la voce e per cosa usarla?

Essa non si fa carico di sole informazioni, ma passa anche un'affettività con parametri che sono anche musicali.

>Pause craxiane...

Far passare tutto attraverso la voce...

Chi usa la voce:
- Voce utilizzata per lavoro
- Figure che si occupano di voce:
  - otorinolaringoiatra
  - foniatra
  - logopedista
  - (compositore)

### In che ordine si confrontano tali figure

1. otorino -> quando vi è qualche problema
2. foniatra -> quando si hanno problemi, esso ci da esercizi, i muscoli delle corde vocali sono molto piccoli e potrebbero portare ad un iper-vascolarizzazione
3. logopedista -> quando si sono avuti traumi, o problemi, si realizza una riabilitazione con ascolto e percezione di quello che si dice.

### Il nostro circuito audiofonatorio

Con al di sotto un circuito pneumofonico.

### Da quanto esistono figure che studiano la voce?

Tali figure esistono dall'antica Grecia e servivano per la politica.

Lo scatto culturale che si ebbe fu importante, per recuperare ad esempio la retorica (come la retorica di Quintiliano nelle variazioni Goldberg).

Si costruiscono teatri con acustica tale che l'attore potesse stare in aree diverse del teatro, portando la voce in maniera adeguata al pubblico.

### La laringe e il suo scopo
Da quando nasciamo la voce è un qualcosa di secondario.

Gli umani hanno tutti la laringe, essa realizza l'utilizzo della voce come un qualcosa di secondario.

Il mondo nacque con il suono, con la voce, e con la voce si da un senso, e si deve permettere agli altri di esprimersi con la propria voce.

>Libro di Murray Schaefer "... quando le parole suonano..."

### Prendere consapevolezza della Voce

Si prende consapevolezza della propria voce prima degli altri sensi.

L'orecchio e l'udito è fondamentale per la nostra sopravvivenza e l'orecchio è molto complesso.

#### Facoltà dell'orecchio

Esso è complesso e ci permette di percepire elementi laterali, frontali e posteriori.

L'orecchio è importante per l'equilibrio.

##### Orecchio esterno e interno

###### Orecchio esterno

Esso è composto dal timpano


##### Orecchio interno

Catena degli ossicini, i muscoli sono antagonisti.

La discriminazione delle altezze è in varie parti dell'orecchio e noi abbiamo l'apparato cigliare:

![orecchio-interno.jpg](orecchio-interno.jpg)

##### Origine della nostra cultura musicale e orecchio

Michel Imberty dice:
>Nasciamo tonalmente acculturati.

I bambini nati in altri luoghi non hanno la stessa arpa europea.

##### Perchè è un sistema audiofonatorio

Secondo la teoria neuro-cronoassiale il cervello da input alle corde vocali.

Delle persone non sono mute, ma non hanno rimandi tra cervello e voce.

##### Fonatorio non autogenerante

Abbiamo un qualcosa che deve essere messo in vibrazione, il cervello dice cosa fare, ma l'azione la produce l'aria che passa in mezzo al sistema audiofonatorio.

Non si potrà pensare che la capacità polmonare sia di tutti uguale.

### Perchè sapere come siamo strutturati?

Per parlare di respirazioni diverse per poter continuare a sentire dentro di noi cosa facciamo.

Abbiamo noi umani un'inspirazione involontaria per vivere.

Come siamo strutturati:

- ghiandola particolare - timo (radiazioni)
- costole fluttuanti con muscoli che fanno alzare ed allargare la gabbia in maniera tridimensionale
- polmone avvolto nella pleura (dobbiamo avere umido tra pleura e polmone)

La vera respirazione per il canto è quella maschile, per le voci femminili invece bisogna istruirla maggiormente perchè il diaframma si posiziona in maniera diversa per prepararsi alla camera gestazionale.

Gli addominali sono da allenare e i risultati si ottengono con il muscoli tonici oggigiorno.

Possiamo agire su muscoli di alti addominali e bassi addominali.

### Nel coro le respirazioni sono distribuite

Osserviamo che prendendo un respiro sfalsato nel coro non si percepirà, mentre coordinandosi con i respiri si realizzerà una pausa.

Nella scuola si riesce a tenere il suono coperto per avere più ragazzi e ragazze.

I ragazzi ascoltandosi si rispettano.

## Capire che tipo di intervento abbiamo nel risuonare

Noi siamo:
- corde
- sistema aereo
- cassa armonica
- siamo anche filtro

Il nostro timbro viene formato dalla nostra testa, in verità le corde vocali definiscono il range.

Ciò che da la forma al nostro timbro è la composizione della faccia, del palato delle guance, della fronte.

>Voce è come allungare delle vocali...

La voce lirica gestisce le espressioni facciali.

(Sbadiglio con utilizzo del palato molle)

>Persone con voci diverse le une dalle altre


## Utilizzo del diapason

Maestria del diapason (farlo vibrare sopra e lasciarlo libero sotto).

È importante sentire il diapason a destra.

Esperienza con il diapason, facendolo vibrare in parti diverse del corpo.

>Noi non sentiamo solo con l'orecchio esterno.

## Domande

- quanto da 1 a 5 siamo favorevoli verso un'attività mai esperita? Quanto siamo interessati a fare cose nuove?
- quanto da 1 a 5 ci sentiamo confortati nel riprodurre attività che sappiamo fare?

## Correlazione tra attività che facciamo e tempo che ci mettiamo

![curva_ideale.svg](curva_ideale.svg)

La curva utopica ha sensi di assimilazione
