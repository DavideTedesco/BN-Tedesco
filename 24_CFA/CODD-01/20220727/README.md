# Appunti della lezione di mercoledì 27 luglio 2022

## Direzione corale: teoria

Vedremo argomenti sulla direzione corale e domani avremo dei documenti sulla direzione corale.

La direzione corale è più delicata di quella d'ensemble, la differenza fra di essi la parte del'istruzione dello strumento per il direttore dell'ensemble non ci compete, ma si interverrà invece con una direzione e orchestrazione.

Per un'ensemble corale invece bisogna insegnare anche il canto.

Alla fine della lezione cercheremo di realizzare un brano, e gli esercizi che vedremo saranno da riproporre in classe.

### Direzione come gestualità ancestrale

Alcuni elementi hanno una significazione interna che vanno sulla gestualità non verbale.

Goffmann studia il fatto che la persona pensa sempre di essere sempre guardato, enfatizzando i movimenti in pubblico (in [Relazioni in pubblico](https://www.raffaellocortina.it/scheda-libro/erving-goffman/relazioni-in-pubblico-9788860301789-1044.html)).

### Bruciare le tappe?

Bruciare le tappe può essere un problema, e la direzione ha una gestione ad iceberg, vi è molto al di sotto di quello che si vede in concerto.

In classe, o in un coro associazionistico il direttore del coro costruisce tutto e guida anche il coro e non si può pretendere che i vagoni si spingano da soli.

### Testi utili di riferimento per la direzione corale

- S. Korn -  Direzione e Educazione Corale - (Rugginenti) - [link](https://www.rugginenti.it/website/it/edizioni/item/3707-direzione-e-educazione-corale)
- K. Thomas - Metodo di direzione corale (Guerini) - [link](https://www.guerini.it/index.php/prodotto/metodo-di-direzione-corale/)
- Adone Zecchi

>Entrambi i testi sono tradotti.

### Tabella di riferimento per la direzione corale

|__Istruzione__|__Concertazione__|__Direzione__|
|---|---|---|
|Voci=corda (1,2,3 o SATB)|Dimensione verticale|Postura generale   |
|Direttore -> voce   |Se imitativo, seguiamo l'ordine delle voci   |<span style="color:blue">Posizione di partenza Pp </span>|
|Dimensione orizzontale   |Se omoritmico, dipende dalla scrittura   |<span style="color:blue">Gesto preparatorio Gp</span>   |
|Intonazione |Orecchio armonico |<span style="color:blue">Attacco/hi At</span> |
|Senso ritmico| |<span style="color:red">Metri: 2 3 4 5</span>|
|Testo: comprensione, pronuncia,traduzione | |<span style="color:red">Dinamiche</span> |
|Forma (con preventiva analisi del brano)||<span style="color:red">Fraseggio</span>|
|||<span style="color:green">Chiusa: di frase, finale, fermata (corona), fermata consonante finale</span>|
|||<span style="color:yellow">Doppio battito, polso, rimbalzo, battito interrotto</span>|

>Essere attenti alle differenze nei brani per non farle sembrare degli errori.

>Concertare deriva da "combattere", si parte da tanti sassolini spigolosi che si smussano via via divenendo dei sassolini levigati come ciottoli di un fiume.

>Quali voci si mettono su per prime?
- se si ha uno stile imitativo (come canone), si realizza la prima voce che entra
-se si ha uno stile omoritmico, dipende dalla melodia e dal basso

>Testo insegnato subito se la melodia è complessa, testo insegnato dopo se si hanno ribattuti

>Note date con diapason all'inizio. Proiezione dell'intervallo musicale successivo ![1](1.png)

#### Postura generale

Per poter cantare posizionarsi sicuri nello spazio, scegliere un piede d'appoggio con le gambe morbide, busto come il nostro schermo, esso indica una massima estensione della vocalità.

L'orecchio segnala lo sbaglio e bisogna immagazzinare la partitura pre macro sezioni.

Viso importante, come lo sguardo che indica al coro, con ad esempio lo sguardo per l'attacco e la chiusura; cercare di rimandare un'impressione di similitudine.

Bisogna tenere una giusta e comoda postura generale.

#### Come gestire i piani

Utilizzo di un piano parallelo al nostro corpo, mentre il piano perpendicolare è il piano della dinamica.

#### Dinamiche
Angoli delle braccia e gesti proporzionali alla dinamica.

#### Fraseggio

Esso ci da la qualità del gesto con 3 gesti principali:
- legato -> (linee curvilinee)
- marcato (orientato al ritmo) -> (linee staccate e spigolose)
- staccato -> (fraseggio come dei punti)

![2](2.png)

#### Bacchetta o non bacchetta?

Essa si è resa necessaria con le grandi orchestre, essa prolunga il braccio, ma essa amplifica gli errori.

Gli errori dovuti a lassità dei legamenti sono amplificati dalla bacchetta.

Il battere deve dare maggior senso di stabilità, ovvero una sensazione di appoggio. (braccia parallele e parallele al pavimento)

![3](3.png)

Ritmo in 5:
![4](4.png)

>Utilizzare sempre due mani.

#### Ritmo
Al metro si sovrappone il ritmo. Bisogna percorrere la durata del suono.

Il ritmo è probabilistico, mentre il metro è deterministico.

#### Tipi di attacchi

Due tipi di attacchi:
- attacco su movimento pieno
- attacco sulla suddivisione

#### Posizione di partenza

Essa coinvolge il direttore, e la posizione di partenza indica molti elementi, anche qualcosa di relativo al metro.

#### Gesto preparatorio

Dalla posizione di partenza

### Chi è coinvolto in cosa?

|__Direttore__|__Gruppo vocale/strumentale__|
| ----------| ---|
|Pp|Pp|
|Gp(Respiro)|Respiro|
|2 pulsazioni prima fermi, 1 gesto|At|

>La battuta prima per il direttore, non serve.

### Attacco su movimento pieno

![5](5.png)


Pp, Gp e At, sono tutti nel gesto dove compare l'attacco.
![6](6.png)

### Chiuse

#### Chiuse sul battere
Il battere deve sembrare comodo, l'ultimo deve essere in alto.

Se siamo in 3/4 non chiudiamo le crome, chiudiamo dalla pulsazione in su.

Non tutte le pause che vediamo,sono da chiudere:
![7](7.png)

#### Chiusa finale

##### Finale senza corona
![8](8.png)

##### Finale con la corona

![9](9.png)

##### Chiusa con consonante finale

![10](10.png)

### Attacco in suddivisione

Usiamo la suddivisione solo quando tutti gli altri gesti non sono sufficienti.

Abbiamo 4 macro gesti:
1. doppio battito, esso coinvolge tutto il gesto in maniera doppia, esso si usa:
  - rallentati finali -> non si può rallentare la battuta, ma si può sottolineare la suddivisione
  - cambi di tempo

![11](11.png)

### Cambi di tempo

![12](12.png)


### Movimenti di polso

![13](13.png)

Scrittura due a due.

### Tempi composti (rimbalzo)

Sfruttamento del ritorno inerziale. Esso da più tempo in aria che atterra al movimento della mano.

Come le pulci che vanno in aria.
![14](14.png)

### Battito interrotto (fermata sul ritorno inerziale)

Mi fermo su un qualcosa su cui non mi dovrei fermare, come l'utilizzo della sincope.

![15](15.png)

---
Fine prima parte
---

## Direzione corale: pratica

### Esercizi con direttore corale diverso

Dare il nome alle note serve a cambiare sillaba.

Come primo esercizio si possono utilizzare le sole vocali delle note. Senza il nome delle note mettiamo attenzione sulle altezze.

1. nome della nota
2. vocale della nota
3. sillaba comune
4. unità di misura
5. unità di misura+unità di movimento
6. variare sistema sonoro di riferimento

>In realtà il gest più grande dall'ultimo al primo

### Realizzazione di un brano

Costruzione del brano di Palestrina _Ahi, che quest'occhi miei_.
