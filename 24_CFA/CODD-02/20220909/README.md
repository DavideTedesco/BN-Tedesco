# Appunti della lezione di venerdì 9 settembre 2022

## Introduzione alla lezione

Il certificato dei 24 crediti serve quando viene bandito il concorso richiedendolo alla segreteria.

Le assenze vengono tenute in considerazione in sede di esame.

## A cosa serve questo corso?

Per accedere alla scuola secondaria di primo e secondo grado:
- scuola media musicale (obbligo scolastico) -> si va per non far i ragazzi dei musicisti
- liceo musicale -> non si precludono altri percorsi di studio

## Partecipazione al concorso

Essa aspira a una docenza il cui obiettivo è quello di formare dei cittadini.

## A cosa servono gli elementi di composizione: contestualizzazione della lezione odierna

Essi sono utili non per essere impartiti nella scuola media, ma per poter usare i principi costitutivi e fondamentali della tecnica per operare azioni nell'ambito compositivo e nell'ambito creativo.


## Quale obiettivo?

I programmi per la scuola media [al link](https://www.miur.gov.it/web/guest/-/decreto-interministeriale-recante-la-disciplina-dei-percorsi-a-indirizzo-musicale-delle-scuole-secondarie-di-primo-grado?fbclid=IwAR3S4Nxx9nv7hXHMa-KKMhfmvFfsdICdyvjF4xEuIzeg_03tOm3V4VntIWk)

Le indicazioni nazionali non sono prescrittive.

### Comporre: mettere inseme

Mettere insieme quello che c'è e quello che non c'era...

### Forma sonata

Due temi con caratteristiche diverse:
- tema maschile (forte, tetico, sulla tonica)
- tema femminile (carattere che viene fuori)

>Contrasto fra tonica e dominante, fra primo e secondo tema.

### Canto gregoriano
Finalis e repercussio in contrasto.

### Pop
Strofa e ritornello.

### Dualismo nella composizione

Come primo tema e secondo tema, il due è un elemento importante nella composizione.

### Il timbro

Ragionamento sulla percussione con le nocche sul tavolo o su un altro oggetto.

>Sgombrare il campo dal genere e dal repertorio.

### Obiettivo di un corso con parti di composizione

Nel corso della storia della musica, vedremo che qualunque composizione ed espressione musicale è composta da elementi che fra loro si contrastano.

### Partire dal materiale sonoro conosciuto
Percepisco una forma quando realizzo che vi sono degli elementi comunicativi noti.

Si utilizzano i materiali sonori conosciuti per la composizione

Come con Cage e 4'33", materiali sonori non programmabili.

## Grandi obietti e grandi aree

1. tecnica polifonica
2. tecnica omofonica

Tutte le composizioni rispondono a una delle due tecniche.

### Tecnica polifonica

Poli e fonia(voci), tecnia contrappuntistica. Essa presuppone che le singole voci (una voce è una linea melodica ovvero elementi musicali che si susseguono l'un l'altro su un piano orizzontale e che sono in grado di disporre).

Tante voci, ovvero tante linee orizzontali sulle quali sono disposti elementi sonori, nella polifonia tali elementi orizzontali si sovrappongono l'un l'altro mantenendo però fra di loro una certa indipendenza.

Abbiamo tante linee orizzontali indipendenti l'una dall'altra. Se una linea viene riprodotta da sola, essa ha un senso compiuto, con espressioni orizzontali fra loro sovrapposte e indipendenti.

Vedremo in seguito la tecnica del contrappunto, mettendoci in contatto con esso.

### Tecnica omofonica

Essa prevede che le voci non siano tutte indipendenti, ma vi è una sola voce e le altre sono delle voci di contorno, accompagnamento o riempimento (quindi di sostegno). Tali altre voci sono complementari e servono a sostenere quell'unica voce.

>Ad esempio il basso albertino, da solo non ha un significato unico.

## Argomenti delle prossime lezioni

Le lezioni tratteranno 3 macro argomenti:
1. tecnica polifonica
2. tecnica omofonica
3. tecniche utilizzate nel XX e XXI secolo

### Musica come serie di eventi sonori

Il comporre è un comporre a largo raggio.
