# Appunti della lezione di venerdì 10 ottobre 2022

## Informazioni sull'elaborato da realizzare

Elaborato da realizzare in una decina di giorni (massimo il 20/21 ottobre), con esito il 25/26 ottobre.

La traccia sarà inviata oggi pomeriggio.

Gli argomenti della prova sono scelti dallo studente, ma la scelta dovrà essere ricondotta al contesto in cui si andrà ad operare.

Parlando ad esempio della musica aleatoria l'argomento andrà contestualizzato nel panorama normativo e legislativo che informa la scuola, primaria e secondaria. Nelle prime lezioni abbiamo parlato e diffuso dei documenti ministeriali utili per indirizzare i contenuti tecnici-musicali per la scuola.

I documenti visti sono le indicazioni ministeriali, i programmi, le indicazioni generali, che forniscono _come_ e _perchè_ vanno trattati degli argomenti per la scuola.

>Oltre al contenuto sarà importante trattare il contesto.

Un'altra sezione può essere dedicata alla riflessione personale e dei contenuti sempre relati al contesto.

L'elaborato dovrebbe avere orientativamente 3 ambiti di azione:
1. contesto in cui si opera e perché si opera (la scuola in cui entro, cosa fa studiare agli alunni?)
2. contenuti specifici dell'argomento scelto con relazione alla scuola italiana, essa non è una semplice esposizione di un argomento, ma essa dovrà essere una trattazione che tiene conto del suo utilizzo e della funzionalità (io insegnante come posso contribuire a quello che la scuola chiede di essere fatto?)
3. riflessione personale che dovrebbe portare a dire come ed in che modo l'argomento trattato, si intende spendere applicandolo e servirlo in un contesto scolastico che dovremo individuare personalmente(ad esempio affrontare una questione di tipo metodologico specificando le competenze che abbiamo ricevuto), bisogna quindi specificare quale contesto e come proporre l'argomento al contesto specificato (in che modo io insegnante, come posso fare quello che le indicazioni nazionali mi suggeriscono)

>Una prima parte è comune a qualsiasi tema si dia.

Entrare nel procedimento metodologico...

Lunghezza del testo 5-10 pagine.

>La parte più impegnativa è la terza sulla nostra riflessione.

Firmare l'elaborato ed inserire tutti i dati.

>Bisogna essere in grado di sapere perché si sta facendo qualcosa, e questo sapere il perchè si fanno le cose, è una consapevolezza richiesta all'insegnante.

Come raggiungere gli obbiettivi attraverso la musica ed attraverso a del materiale che sia riconducibile alla composizione, specificando come portare avanti il mio discorso, obiettivo e intenzione.

Osservare in base alle indicazioni nazionali cosa insegnare in diversi contesti scolastici.

Ad esempio una modalità interattiva si può applicare a qualsiasi contenuto...

## Dalla tecnica del contrappunto in poi
Sappiamo che il contrappunto è legato anche a qualcosa del ventesimo secolo...

### Metodo di composizione per dodici suoni

Abbiamo parlato della tecnica del contrappunto e di cosa ess ha in se.

Esso è chiamato in genere dodecafonia e l'esigenza che ha portato a tale metodo è un'esigenza di ricerca nel campo di organizzazione dei suoni, cercando di organizzarli in maniera diversa.

Si chiesero quindi se fosse possibile organizzare i suoni in altro modo.

Dalla fine dell'Ottocento si cerco di superare quello che era il dominio della tonica sugli altri suoni.

Si cercò quindi di superare le sole sette altezze, portando la scala alla considerazione di tutti e 12 i suoni.

Già negli ultimi quartetti di Beethoven si intravedeva un uso voluto e non casuale di tutti e 12 i suoni del totale cromatico(non della scala).

Si vedeva infatti nelle composizioni di Beethoven il voler toccare tutti e dodici i suoni, anche se non era ancora chiara l'esigenza.

Il primo a teorizzare la composizione con dodici suoni fu Arnold Schonberg e ciò anticipava la pratica.

Da Schonberg quest teoria fu formulata a priori sperimentando parzialmente ciò che stava scrivendo.

_Visione dell'articolo di Schonberg introduttivo alla dodecafonia._

![1](1.png)

Nel metodo di composizione con dodici suoni viene determinata a priori una serie e tale serie deve contenere tutti suoni del totale cromatico.

Nell'articolo viene fornita una serie che è alla base della composizione che si vuole fare.

Non esisterà una serie per tutte le composizioni, nel sistema dodecafonico si utilizza per una composizione, una serie e bisognerà comporre con una serie ed alla base della composizione vi è una serie.

Bisogna inserire anche criteri che realizzano la serie e vi sono criteri che tendono ad allontanare il più possibile la composizione ed il risultato percettivo da ciò che sono gli stilemi della composizione tonale.

La serie non deve essere in alcun modo uguale alla scala cromatica.

#### Le forme della serie
La serie ha inoltre quattro forme:
- serie originale
- serie retrograda, partendo dall'ultima nota ed arrivando alla prima
- serie inversa, costruita invertendo gli intervalli
- serie inversa retrogrado, esso è il retrogrado dell'inverso

Questo criterio di disporre di materiale che deriva da altro materiale è un qualcosa di cui abbiamo parlato nello stile classico, ed esse possiamo riconoscerle come forme di variazione di una serie di 12 suoni.

>Se si volesse un materiale di tre suoni, potremo applicare al materiale di tre suoni.

- Do, Re, Mi
- retrogrado Mi, Re, D0,
- inverso Do, Si b, La b
- retrogrado inverso La b, Si b, Do

>La serie come le sue trasposizioni è trasponibile su ogni grado del totale cromatico.

Potremo avere 12 serie originali, una per ogni semitono e quindi 12 serie retrograde, inverse e retrograde inverse.

Per costruire un brano derivando da una stessa serie potremo avere 48 serie da cui possiamo organizzare.
#### Utilizzo della serie
Schonberg numera i suoni della serie e nel comporre dal quintetto a fiati fa vedere nell'articolo come espone dei suoni con un pensiero orizzontale.

Vediamo che i suoni non sono disposti uno dietro l'altro e i dodici suoni della serie sono utilizzati in varie voci e livelli:
![2](2.png)

Vediamo che si sovrappongono dei suoni e nelle prime battute la serie viene esposta, mentre nelle battute che seguono si ricomincia la numerazione sovrapponendo anche i suoni.

#### Applicazione del procedimento dodecafonico

L'applicazione e la disposizione dei suoni è una scelta realizzata dal compositore quindi il fatto che due suoni appaiano insieme o meno dipende dal compositore.

#### Ascolti

_[Ascolto del preludio dell'Op.25 per pianoforte di Schoenberg](https://www.youtube.com/watch?v=bQHR_Z8XVvI)_

Vediamo che la serie originale sta sulla mano sinistra.

La mano destra è la serie originale trasposta di un ottava più un tritono.

![3](3.png)

La serie originale esposta nella mano destra è accompagnata dalla serie originale trasposta nella mano sinistra.

>A conferma di un pensiero orizzontale contrappuntistico, come per il tema e soggetto della fuga, è un criterio che proviene da lontano come anche il criterio di sviluppo orizzontale.

Osservare anche le dinamiche, che sono diverse per la mano sinistra, che ha dinamiche diverse con dita diverse.

Le voci anche se sono omoritmiche devono distinguersi con una diversa attribuzione dinamica.

>Sappiamo che una voce è importante quando le altre sono subordinate, e le altre voci non sono di sostegno perchè ogni voce ha un suo senso e non sono elementi di attributo e non sono quindi subordinate.

Ascoltare i brani dodecafonici ponendo ascolto ai contrasti che vi sono e come.

>Osservare che abbiamo dei ribattuti che ci portiamo avanti della ripetizione dello stile classico.

La ripetizione vi è qui come anche nella Quinta di Beethoven.

>Non vi è una voce principale sotto il profilo melodico...

Il metodo qui utilizzato parla e tratta solo di organizzazione delle altezze, si è dimostrata quindi carente sotto altri profili musicali.

#### Pierre Boulez, la critica a Schonberg e Messiaen

Egli disse che sarebbe stato opportuno applicare il concetto di serie anche agli altri parametri della musica con quello che noi chiamiamo musica o tecnica seriale.

>Evoluzione degli artisti e riconsiderazione dei propri pensieri.

Messiaen combina serie di altezze, durate e dinamiche:
![4](4.png)

Boulez poi definì e spiegò meglio il serialismo.

Osservare la tabella che mostra il serialismo secondo Boulez:
![5](5.png)

>Inoltre l'artista dovrebbe porre attenzione a non contaminare l'opera e l'opera deve essere quanto più perfetta possibile con il minor intervento possibile dell'uomo, perchè l'uomo è imperfetto e la vita dell'uomo è talmente breve, perchè ciò che pensa un uomo non potrà mai essere condizionante per uomini di epoche diverse. L'opera dovrà prescindere dalle imperfezioni umane.

I materiali che costituiscono l'opera devono sottostare a criteri soggettivi che non possono stare sotto la condizione umana.

Gli altri valori delle durate si determinano ogni volta aggiungendo un trentaduesimo.

>La determinazione di una serie di dodici durate è avvenuta in automatico, viene usato un criterio oggettivo per determinare le durate.

Lo stesso principio di distacco la troviamo nella composizione e determinazione di altezze ed intensità, vediamo che anche tali serie si saranno autodeterminate.

Osserviamo nell'articolo come le serie vengono utilizzate da Boulez.

Il riferirsi a elementi matematici allontana la soggettività del compositore:
![6](6.png)

Anche Boulez indica i numeri delle note della serie.

Vediamo che rispetto alla scrittura di Schonberg abbiamo interventi e suoni che non danno equivoci tematici o di melodia.

>Nel mondo artistico l'importanza dell'idea è superiore a ciò che l'idea realizza.

Il compositore cerca di fare qualcosa che supera, non qualcosa che piaccia.

>L'arte non è per tutti, e se è per tutti non è arte.

-A. Schoenberg

#### Luigi Nono e la musica nelle fabbriche

La musica di Nono venne portata nelle fabbriche e si pensò che la scarsa frequentazione è un qualcosa che ci portiamo dietro.

>La musica e la sua fruizione è cambiata quindi nel corso del tempo.

#### A cosa serve l'arte?

La funzione della musica e dell'opera d'arte.

Sviluppare meccanismi di crescita intellettuale del soggetto a cui si rivolge con varie metodologie...

## Conclusioni

Ci arriveranno via email le indicazioni per scrivere la traccia d'esame.
