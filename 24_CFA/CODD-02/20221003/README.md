# Appunti della lezione di lunedì 3 ottobre 2022

## Riepilogo

La scorsa volta abbiamo terminato la tecnica omofonica.

Oltre la costruzione del tema la tecnica omofonica dovrebbe proseguire con altre sezioni che dovrebbero dare la completezza di un brano musicale.

## Tecnica dei contrasti

Bisogna inserire contrasti e variazioni. Più la percezione è lontana più avremo percezione del contrasto. Aggiungendo mattoncini nella percezione musicale, il testo di Schönberg si ferma alla forma semplice ternaria.

## La forma ternaria

Essa è A-B-A'.
Sapremo che:
- la parte A è costituita dal tema ed è di 8 battute, essa è un'alternanza tra ripetizioni e contrasti
- la parte A' è una ripetizione
- la parte B è contrasto

>Questa sarà la macroorganizzazione di un pezzo musicale ed essa sarà la composizione dei pezzi estetici a cui ci riferiamo.

Questo è relativo a tutte quelle composizioni costruite diversamente dalle composizioni basate sul solo ascolto.

### Vexations di Erik Satie

Essi sono brani di durata di 36 ore, e sono denominati Vessazioni e sono finalizzati a qualcos'altro.

### Brano per organo di John Cage

Sappiamo che vi è un brano di Cage che sta suonando da molti anni.

[link al brano](https://universes.art/en/specials/john-cage-organ-project-halberstadt)

>Questo brano non è per l'ascolto, ma ha un altro significato.

### 4'33'' di John Cage

Valorizzazione rumoristica del suono, sappiamo che la musica è organizzazione del suono.

Un compositore ha un'idea e la trasmette al mondo, ed il mondo prende un'idea e si chiede quale utilità abbia qualcosa per gli altri.

>Provocazione al modo di fruire e affrontare la musica, con un'operazione concettuale.

Le operazioni concettuali servono a spostare il paradigma.
## Cos'è il comporre

Cos'è il comporre, esso è il mettere insieme, il bravo compositore è chi mette assieme meglio di altri; il compositore è un artigiano, mette insieme meglio di altri.

>Il mettere insieme presuppone tutta una serie di ragionamenti.

Comporre significa quindi mettere assieme.

Ma mettere assieme cosa?

Prendendo e mettendo insieme eventi sonori...

Qualsiasi evento sonoro è suscettibile ad essere composto, ovvero ad essere messo insieme ad altri.

Ad esempio il Do centrale può essere composto e accorpato a qualunque evento sonoro.

>Per comporre oggi abbiamo anche gli strumenti per la registrazione con eventi sonori che possiamo recuperare noi.

## Noi come esecutori

Abbiamo imparato a riprodurre qualcosa realizzato da qualcun altro.

Anche gli esecutori di se stessi, hanno soddisfazione a eseguire quello che scrivono.

Sappiamo inoltre che Mozart, Brahms, etc facevano concerti solo con i propri brani.

Il musicista prima era pieno, ed il musicista sapeva scrivere e la sapeva anche suonare.

Quando il compositore suona, si rende conto anche della struttura del brano.

### C'è più gusto a farla la musica, o è meglio ascoltarla?

È sicuramente meglio farla anche se bisogna perdere molto tempo a farla la musica.

## Educazione

Trarre fuori, quello che lo studente ha dentro di se, per valorizzare, indagare e sviluppare quello che qualcuno ha dentro.

## Ritornando alla forma ternaria

Sappiamo che in nella prima sezione (A) avevamo un contenitore vuoto e potremo metterci qualunque evento sonoro combinato.

Nella parte A vi sono piccole sezione ed esse erano:
- proposizione
- ripetizione
- contrasto
- chiusa

>In A abbiamo alcune sezioni che ripetono ed altre che contrastano.

Avremo quindi due principi fondamentali di comunicazione:
- inventare una cosa e poi ripeterla
- inventrare una cosa e poi contrastarla

Questa dinamica si ripete in una struttura più ampia che è la parte A, B, A'.

La parte B è il contrasto mentre la parte A' è la ripetizione variata di A.

Vedremo che inizialmente in A vi erano 8 battute e avremo una riproposizione in forme più ampie di proposizione, ripetizione e contrasto.

>Nella musica classica solitamente il contrasto è avvenuto con privilegio del piano armonico.

Ad esempio se la prima è in un modo la seconda la contrasto armonicamente, la prima parte è in tonica, la seconda sarà alla dominante.

Il contrasto nel sistema armonico tonale è un contrasto che avviene quasi esclusivamente sul piano armonico.

L'indugiare in regioni diverse da elementi contrastanti e sappiamo che la ripetizione e il contrasto la possiamo realizzare con qualsiasi materiale.\

>Il concetto di ripetizione ed il concetto di contrasto si possono applicare a qualunque materiale.

### Un brano musicale e i suoi ingredienti
Come per fare un dolce, per fare una composizione abbiamo bisogno degli ingredienti.

## Musica cos'è?

Essa è un insieme di significanti...

![Dark_Side_of_the_Moon.png](Dark_Side_of_the_Moon.png)

Ogni ascoltatore ha percezione diversa della musica, rispetto a quella che ha ogni altro individuo.

>La musica che compongo è come se inserito un fascio di luce bianca in un prisma, l'ascoltatore riconosce i colori che fuoriescono.

## Spostare conoscenza sugli altri

Spostare conoscenza sugli altri è la missione dell'insegnante di musica. Sappiamo che nel ventesimo e ventunesimo secolo, anche gli strumenti sono stati utilizzati in maniera diversa da quella tradizionale.

### Henry Cowell - Aeolian Harp (1923)

Abbiamo una scrittura tradizionale ma con pianoforte suonato all'interno della cordiera del pianoforte:
![1](1.png)

Vi sono indicazioni per suonare gli accordi come:
- sw -> swept

>Le note di Eolian Harp hanno tasti portati giù senza fermare le corde.

Corda sollecitata dentro al pianoforte.

_[ascolto e visione del video](https://www.youtube.com/watch?v=XEb8aoW8W-s&ab_channel=FrancoMeoli)_

### Henry Cowell - The Banshee (1925)

Sempre utilizzando la tecnica per suonare dentro il pianoforte vi sono spiegazioni con simboli molto diversi.

![2](2.png)

Con una partitura diversa:
![3](3.png)

In questa partitura l'aspetto visivo è molto significativo per come deve essere eseguito.

>Servono per questo brano, minimo 2 esecutori.

[ascolto di The Banshee](https://www.youtube.com/watch?v=WaIByDlFINk&ab_channel=V%C3%ADctorTrescol%C3%ADSan)

Vi sono tanti modi di attacco diversi ed il materiale è organizzato anche secondo principi precedenti con ripetizione e variazione.

Avremo concetti della composizione a prescindere dai materiali fondamentali. Alla base dei contrasti vi sarà quindi la variazione.

>L'attenzione qui è spostata sul timbro e non sull'intonazione.

### Bruno Maderna - Serenata per un satellite

![4](4.png)

Esso è un brano che ha il suo interno l'alea.

>L'alea non è il caso, ed essa è un concetto che presuppone un caso regolato e controllato.

L'alea viene controllata e indirizzata.

L'alea viene comandata e il brano potrà durare da 4 a 12 minuti.

Gli strumenti con cui si può suonare sono indicati in partitura.

>Ma! - Con le note scritte.

Le esecuzioni di Serenta per un satellite sono molte, ma non si hanno degli elementi per riconoscerla...

_Ascolto di varie versioni di Serenata per un Satellite_

- due versioni per ensemble
- una versione per chitarra(essa non soddisfa le indicazioni date, ovvero la durata)

Il brano in teoria non è così riconoscibile come potrebbe essere una Sinfonia di Beethoven.

Si cerca di spostare l'attenzione su altri elementi inserendo accenti e tempi strani.

>L'opera diventa tale nel momento della fruizione che ci viene fatta arrivare dall'interprete.

L'opera assume il suo valore ed il suo significato per mezzo dell'esecutore.

### Gyorgi Ligeti - Volumina (1966)

Brano per organo. Questo è un tipo di partitura con notazione analogica in cui si ragiona per analogia.

>Sappiamo che la notazione tradizionale è una notazione astratta, non analogica.

Sappiamo che l'intavolatura è una notazione analogica.

Con Piaget si sono stabilite delle fasi dello sviluppo:
- 0-3 anni fase egocentrica
- il massimo dello sviluppo porta al massimo del pensiero astratto(si può parlare di qualcosa di analogico astraendolo)

![5](5.png)

[Visione del video su Volumina](https://www.youtube.com/watch?v=MoA7vgEgxHg)

## Astrazione e partitura analogica

Astrarre significa capire anche senza vedere.

___

L'ultima lezione sarà lunedì 10 ottobre.

Vi sarà un termine di consegna per scrivere un elaborato.
