# Appunti della lezione di lunedì 19 settembre 2022

(dopo la lettura delle pagine da 3 a 11 del testo _Felix Salzer e Carl Schachter - Contrappunto e Composizione ridotto_)

## Cantus firmus: i criteri di costruzione dello stesso

Esso è una parte data che toglie di impaccio il compositore dal trovarsi davanti alla pagina bianca.

Sappiamo che le composizioni sono state realizzate partendo da uno spunto, ovvero da un canto che spesso è stato utilizzato per realizzare il Cantus Firmus.

Sappiamo che i ragazzi conoscono una parte data già conosciuta e ci vorrà poco ad inquadrare la stessa in un sistema condiviso di notazione.

Considerare la composizione come un punto di partenza con una parte data è un qualcosa da tenere in considerazione.

Il compositore si deve abituare da se stesso a realizzare la parte data, poichè un cantus a valori lunghi mette il compositore nella necessità di realizzare un Contrappunto con altre voci.

>Non tutte le linee orizzontali sono ugualmente sovrapponibili ad altre, come una melodia è meno sovrapponibile di un soggetto di fuga. Un tema è meno sovrapponibile di un soggetto di fuga, ma più di una melodia.

All'interno delle linee orizzontali dobbiamo avere la consapevolezza che ciascuna melodia, tema, etc... ha delle caratteristiche.

Ad esempio sappiamo che un tema si possa sviluppare in forma sonata, ed esso avrà delle caratteristiche che lo pongono in un alveo utile.

>Un'area d'opera non si presta ad essere trattata come una forma sonata.

Bisogna avere consapevolezza delle linee orizzontali per poterle utilizzare nel modo più utile (come degli atleti di discipline diverse).

>Le linee orizzontali sono diverse fra loro e una data melodia va bene per un dato scopo.

### Costruire un Cantus Firmus in maniera adeguata

La tecnica del Contrappunto vuole delle linee che siano non subordinate l'una all'altra e che abbiano una indipendenza ed un loro senso compiuto.

>Sappiamo che il Cantus Firmus è una struttura lineare in cui l'elemento viene astratto da altri aspetti del disegno melodico (ritmo e durate) perchè le note hanno la stessa durata e sono valori lunghi.

I valori del cantus firmus, sono lunghi e sono valori tutti uguali.

Il cantus firmus riguarda le qualità estetiche del canto.


### Caratteristiche del Cantus Firmus

Un cantus firmus deve avere queste caratteristiche:
- direzione
- continuità
- varietà
- equilibrio
- completezza

### Estensione del cantus firmus

In generale le voci sono meglio scritte, se il loro ambito(distanza tra nota più acuta e nota più grave) è all'interno del pentagramma(senza andarci fuori) ovvero nove posti(con al massimo una distanza di nona). Ciò fa si che la linea orizzontale sia più facilmente contrappuntata e contrappuntabile.

>Ci si deve limitare all'interno di un pentagramma.

### Due voci possono incrociarsi

Quando due voci si incrociano, l'orecchio può andare in confusione, a meno che non abbiano le voci un timbro nettamente distinto.


### Non vi devono essere accentuazioni tra tempi deboli e tempi forti

Non si devono avere differenze tra accenti.

### I criteri descritti e altri sistemi

I criteri descritti sono applicabili a tutti i sistemi scalari che noi intendiamo utilizzare (anche se noi lo vedremo nella tonalitá).

Gli intervalli dissonanti sono:
- tritono (quinta diminuita o quarta aumentata)
- settima (maggiore e minore)

Intervallo di seconda e intervallo di quarta nella dimensione orizzontale sono usabili al pari degli altri intervalli (anche se verticalmente dissonanti).

### Il concetto di __direzione__

Dei suoni che si succedono l'uno all'altro vanno in una direzione (ascendente o discendente), abbiamo quindi una prima definizione di direzione (verso su/verso giù).

>Se il Cantus Firmus deve avere una caratteristica e non deve mancare di alcune condizioni estetiche.

Un cantus con una direzionalità puramente ascendente, ma non ha la qualità estetica di un cantus, perchè per poter essere funzionale, deve avere un'alternanza di direzione (che sale e che scende).

L'_esempio 1.1 a_ ad esempio cambia troppo di direzione:

![1](1.png)

- il ripetere spesso la stessa nota può portare alla monotonia, quindi vi è scarsa varietà
- è tutto per grado congiunto (quindi i suoni che si muovono tutti per grado congiunto generano una impressione di scarsa varietà)
- il culmine del cantus è ripetuto due volte, perchè?

Nell'esempio _a_ la direzione non funziona, quindi se prendessimo l'esempio _b_, esso è di poco migliore perchè non ragiona sempre per grado congiunto (vi sono due salti di terza).

I salti (seppur pochi e piccoli) rendono il Cantus _b_ meno monotono.

Abbiamo quindi:
- salti piccoli
- salti grandi
- direzione

Abbiamo quindi delle direzioni e quindi dei movimenti tra un suono ed un altro che possono essere o di grado o di salto.

>Avendo un suono e potendo andare ad un suono successivo, posso avere 12 possibilità pensando una scala diatonica.

Sceglieremo il salto avendo la certezza che quello che scriviamo prima condiziona ciò che viene subito dopo.

In _b_ non vi sono salti grandi, mentre in _c_ vediamo che vi sono salti piccoli e grandi, con una varietà di salti:
- grado congiunto
- salto piccolo
- salto grande

>Ogni volta che vi è un salto, deve essere riempito successivamente.

#### Culmine

Quello che rende il cantus adeguato è anche il punto di culmine, in cui il cantus raggiunge un punto massimo (come se si caricasse un arco e poi l'energia debba essere liquidata successivamente per raggiungere una posizione di riposo).

Il procedimento di carica-rilascio dell'energia, si ottiene attraverso la direzione e l'uso appropriato della direzione.

Un primo concetto di direzione _Do-Re_ relativo alle note lo abbiamo visto, ma vi può essere anche una direzione globale del canto come _Do-Re-Fa-Mi-La_, in cui osserviamo una direzione ascendente (a prescindere dalle relazioni relative delle singole note).

Nella direzionalità ascendente di _c_ che va da Do a La, abbiamo un movimento direzionale con cambio di direzione, e questo effetto di cambio di direzione è una caratteristica fondamentale per evidenziare il punto di culmine (nota più acuta o più grave del cantus).

>Possiamo desumere che il punto di culmine di un Cantus è il risultato (per essere percepito come punto di culmine) di un movimento direzionale che ha al suo interno dei cambi di direzione.

### Cos'e

Un cantus è una linea orizzontale melodica con:
- inizio
- culmine
- fine

Tali elementi danno la patente di compiutezza al cantus, ma il cantus per essere compiuto deve avere al suo interno dei cambi di direzione.

## Scriviamo dei Canti Firmi

### Scriviamo un Cantus Firmus insieme
Evitiamo la ripetizione perchè essa toglie energia.
Osserviamo che dopo un salto grande è necessario invertire la direzione.
![2](2.png)

![3](3.png)

Tale cantus è vario, ha una direzione, un culmine ben raggiunto.


### Scriviamo un altro cantus firmus

>Il culmine è un risultato di un movimento direzionale, non una sola nota

Osserviamo che facendo dei salti grandi dovremo invertire la direzione, e tale direzione che invertiamo, possiamo invertirla per grado congiunto, ma anche per salto.

Abbiamo fatto gradi congiunti e salti grandi, ma mancano ancora salti piccoli.

![4](4.png)

![5](5.png)

La durata di un cantus va dalle 8 alle 16 battute con un culmine abbastanza centrale.

>Ricordiamoci che all'interno di un cantus possiamo avere anche un secondo culmine.

Raggiunto il culmine dobbiamo raggiungere il riposo, ovvero la fine.

Mettiamo una nota dietro l'altra perchè consideriamo il profilo che dobbiamo costruire con note scritte prima e note scritte dopo.

>Le stesse cose a seconda del contesto, assumono significati diversi.

### Scriviamo un altro cantus firmus

![6](6.png)

Osserviamo che manca un salto grande e vi è una ripetizione, il che rende il tutto debole.

Il culmine è culmine ma è un cantus debole:
![7](7.png)

Ora dobbiamo liquidare il culmine, e osserviamo che potremo farlo in questa maniera:
![8](8.png)

Anche se con tale modo, manca un salto grande e vi sono delle ripetizioni.

Lo sentiamo debole perchè vi sono degli elementi che non sono esattamente funzionali a quello che noi ricercavamo.

Ci sembra un po' scarno perchè vi sono degli elementi che ce lo fanno sentire povero:
![9](9.png)

>Dovremo essere in grado di autovalutare perchè qualcosa non ci piace.

## Considerazioni finali sul cantus firmus

Ciò che abbiamo detto è tutto racchiuso nel testo nelle pagine 3-11.

Abbiamo quindi visto come costruire il Cantus, in particolare i criteri di costruzione del Cantus sono gli stessi che dobbiamo tenere in conto anche quando dobbiamo costruire le altre voci.

## Contrappunto a più voci

L'altra linea deve avere le stesse caratteristiche del Cantus, quindi deve avere:
- un inizio
- una fine
- un culmine

Il nostro lavoro deve andare a sovrapporre una voce al Cantus e la voce deve essere costruita avendo attenzione alle stesse accortezze che sono servite per costruire il Cantus.

### Elemento verticale nel contrappunto
Sappiamo che un'altra linea melodica va sovrapposta, ma non consideriamo l'elemento verticale, generermo quindi un effetto che noi definiamo degli intervalli verticali (quasi armonici).

Gli intervalli verticali hanno una loro definizione di consonanza e dissonanza diversa(rispetto a quella orizzontale), come ad esempio avremo come dissonanze verticali intervalli di:
- seconda
- quarta

Questi intervalli nella successione orizzontale sono considerati utilizzabili.

Quando sovrapponiamo una voce ad un'altra, dopo aver costruito linearmente una voce devo avere l'attenzione di non trovarmi con degli intervalli verticali che siano dissonanti (seconda, quarta e settima).

>Non bisogna produrre intervalli dissonanti tra le note delle due voci

![11](11.png)

Ovvero vedremo:

![12](12.png)

### Gradi di stabilità di un intervallo rispetto ad un altro

Per poter classificare gli intervalli in base alla loro stabilità, cosa dovrei fare?

Nella costruzione musicale ho stabilità e dinamicità.

In un brano di musica con cosa possiamo rappresentare una situazione di stabilità?

__Sappiamo che la fine per sua connotazione è stabile.__ Abbiamo bisogno del ritorno a casa.

Per poter ultimare un brano e far capire che esso sta finendo sarà meglio che utilizziamo il do rispetto al mi.
Vi sono ovviamente delle gradazioni di stabilità e la nota Do è in Do maggiore la più stabile per iniziare e finire.

Dopo la tonica vi sono altri suoni della scala diversamente stabili che si possono considerare.

Ragionando in termini armonici l'accordo più stabile per finire è l'accordo di tonica, e le note finali dovrebbero essere le note dell'accordo.

#### Scala di stabilità dei vari intervalli
Abbiamo quindi un concetto di stablità che possiamo applicare ai suoni ed alle melodie.

Il libro di testo esprime come ci possano essere degli intervalli consonanti più o meno stabili.

>Un intervallo stabile è ad esempio un intervallo di ottava che è più stabile di uno di terza.

Un intervallo con stabilità meno netta è quello di quinta perchè è l'armonico successivo della nota iniziale.

Il secondo intervallo che mi da stabilità e1 l'intervallo di quinta.

Sappiamo ovviamente che l'intervallo più stabile è l'unisono.

In ordine per stabilità degli intervalli avremo:

1. unisono
2. ottava
3. quinta
4. terza (intervallo dinamico)
5. sesta (intervallo fortemente dinamico)

>Se vi sono gradi di stabilità degli intervalli, andando a costruire la seconda voce dovrò sia distinguere le due voci che introdurre della varietà, rispondendo alle istanze precedenti:
- varietà
- equilibrio
- etc...

Non bisogna quindi generare monotonia.

### Coordinamento dei movimenti (moto delle parti)

Ogni voce ha un suo profilo e si rapporta con un'altra voce che ha un altro profilo. I profili si incontrano in 4 tipologie possibili di incontro.

#### Moto parallelo

Due voci vanno nella stessa direzione esso si chiama _moto parallelo_:
![13](13.png)

#### Moto simile

Due voci vanno nella stessa direzione ma con intervalli diversi esso si chiama _moto simile_:
![14](14b.png)

#### Moto obliquo

Una voce sta ferma e l'altra si muove ed esso è chiamato _moto obliquo_:
![15](15.png)

#### Moto contrario

Due voci sovrapposte che vanno in direzioni diverse _moto contrario_:

![16](16.png)

### Cosa succede se faccio andare due voci sempre per moto parallelo?

Due voci non saranno indipendenti l'una dall'altra se le facessi andare sempre per moto parallelo (obliquo o simile).

>Bisogna stare attenti a che il moto delle parti abbia una sua varietà, quindi i vari moti si devono alternare come si alternano gli intervalli a seconda della loro stabilità.

### Ottave e quinte parallele

In generale si dice che quinte e ottave parallele sono da evitare.

### Quinte e ottave nascoste

Sappiamo che anche gli intervalli nascosti vanno evitati, la ragione della proibizione del parallelismo risiede nel fatto che l'intervallo di quinta ed ottava sono intervalli perfetti nella prima serie degli armonici che hanno un carattere di forte stabilità che impediscono in un preciso momento la percezione dell'indipendenza delle parti.

>Quindi la seconda voce che ha l'ottava(nel caso di ottava) è già compresa tra gli armonici della nota più grave.

Il senso di vuoto creato dall'ottava è dovuto ad una voce che risuonana già in un'altra.

Si evitano le ottave e le quinte per non dare sensazioni di vuoto. Quindi le due voci quando tra loro si incontrano, allora in quel momento smentiscono la loro indipendenza le due voci.

>Conoscendo la ragione per evitare quinte e ottave, saremo in grado di superare tali errori da superare.

### Polifonia e parallelismi

Quando vogliamo ottenere una scrittura polifonica dobbiamo quindi evitare di realizzare quinte e ottave parallele, proprio perchè possono offuscare l'indipendenza delle due voci.

Saputo che quinte e ottave parallele tradiscono l'indipendenza delle parti, quando non sto scrivendo qualcosa di polifonico, le quinte e le ottave parallele si possono utilizzare.

>Solo capendo le ragioni e contestualizzandole siamo in grado di dominarle.

## Cosa serve oltre la teoria

Oltre la teoria servirebbe solamente l'esercizio...

Sappiamo che il contrappunto è tale, solamente se il canto fermo può essere posto in una delle tre parti:
- acuta
- grave
- media

## Ascolto di parte dell' _Arte della fuga_ di Bach

[Link al video](https://www.youtube.com/watch?v=vd4ssVshPlo&ab_channel=FabFugues)

Osserviamo la caratteristica del soggetto della fuga, con un cantus sulla prima voce che vedremo poi risuonerà in tutte le tonalità:
![10](10.png)

È importante cogliere la versatilità e l'uso molteplice del soggetto della fuga.

Questo soggetto è andato in giro per le varie voci ed ogni volta assume significati diversi con riferimento alla collocazione, ed ad esempio nell'ultima parte assume una collocazione di chiusa.

Il risultato della tecnica del contrappunto è una composizione del genere in cui le voci sono percepibili indipendenti fra loro e il canto è posizionabile senza nessuna difficoltà.

## Per la prossima volta

- Ascoltare l'arte della fuga di Bach, individuando le caratteristiche individuate nelle lezioni
- Leggere il libro sulla sezione vista oggi
