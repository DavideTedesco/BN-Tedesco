# Appunti della lezione di lunedì 26 settembre 2022

## Modalità d'esame

Elaborato scritto con una domanda ampia in cui si chiede di ragionare attorno alle nozioni affrontate.

## Contrappunto

### Problema del ritmo ed incompletezza con la quinta specie

Sappiamo che per tutta la durata di un esercizio in prima specie avremo sempre nota contro nota uno contro uno, mentre nella seconda specie avremo sempre due contro uno.
Nella quinta specie (stile fiorito) è possibile utilizzare durate diverse che si alternano a durate brevi; quando dobbiamo andare a sovrapporre tra loro due voci che sono in quinta specie, ci troveremo davanti ad un problema che riguarda la necessità di _dover rendere indipendenti le due voci_ mentre da un altro lato _non dobbiamo realizzare dei vuoti ritimici che diano una sensazione di incompletezza_.

#### Esempi di contrappunti a tre parti

Vediamo che abbiamo un cantus mal posto in _a_:
![1](1.png)

Bisognerà avere una disposizione dei suoni all'interno del pentagramma.

Vediamo che l'esempio _c_ è collocato meglio poichè è sempre all'interno del pentagramma, mentre nell'esempio _a_ abbiamo che la chiave di basso è mal collocata.

#### Sovrapposizione di più di una voce sul cantus e mescolanze

Vediamo nell'esempio _a_ che vi sono due voci che si sovrappongono con il cantus in seconda specie(due contro uno), mentre le due voci superiori si rapportano fra loro in prima specie(uno contro uno).

>Bisogna sempre rapporta le voci prima con il cantus, e poi eseguire il rapporto fra le voci.

Vediamo che in _c_ abbiamo un contralto in seconda specie con due note contro una con il cantus, e violino e contralto sono in seconda specie fra loro, mentre abbiamo violino e cantus in terza specie.

![2](2.png)

In questi altri esempi vediamo una mescolanze di specie (in _e_ e _d_) mentre in _f_ abbiamo delle sincopi con le legature (ricordarsi che dove vi è la legatura è come se fosse una nota unica di 4/4).

![3](3.png)

Si possono usare tutte le combinazioni possibili per delle esercitazioni. E questo modo di mischiare fra loro le varie specie vie denominato __mescolanze__.

Quando si arriva in _i_ a mescolare più specie diverse con le varie durate che si alternano, bisognerà fare attenzione alla parte ritmica delle due voci in quinta specie, nel momento in cui si rapportano fra loro.

>Osserviamo che in generale il principio che regola la con durate diverse è un principio generale che possiamo qualificare come la ricerca del ritmo complementare.

Il principio del ritmo complementare ci suggerisce di far muovere una voce, laddove una voce è ferma.

>Dove una parte è ferma, l'altra parte si muove:
![4](4.png)


Il tactus non visibile è dato all'ascolto

### Imitazione ed altro

![5](5.png)

Leggiamo qui _5+5 con imitazione_ cio3 significa cinque note più cinque con processo di imitazione di una voce con un'altra.

Vediamo quindi negli esempi _j_ e _k_ la presenza dell'imitazione. L'imitazione può avvenire su tutti i gradi della scala, purchè venga mantenuto il profilo ed il ritmo del frammento stesso.
L'imitazione può anche essere un'imitazione che si sviluppa non esattamente come una copia, ma essa può avere delle varianti che ricorrono in relazione a dei modi con cui si può trattare il frammento da imitare, questi modi sono _aggravazioni_, _diminuzioni_, _retrogradi_ ed _inversi_.

#### Procedimenti per le durate delle note

Procedimenti che cambiano il ritmo del frammento:
- _aggravazione_ (anche _aumentazione_)-> aggrava le durate per cui le durate che sono minima e semiminima, diventano ad esempio semibreve e minima, allargando ed aggravando il frammento (il frammento diviene più lungo) si potrà lavorare per aggravamento aggiungendo valori diversi, esso va ad aumentare le durate dei singoli suoni
- _diminuzione_ -> contrario alla possibilità di aggravamento, i valori vengono ad essere accorciati e quindi laddove ci fosse una minima, avremo una semiminima

>Esse sono variazioni che prevedono aumentazione e diminuzione del singolo frammento.

#### Procedimenti sugli intervalli

![6](6.png)
Essi sono i procedimenti che cambiano l'ordine delle altezze:
- _retrogrado_ -> partire dall'ultima nota di una melodia (mantenendo la stessa sequenza ritmica o meno) ed arrivare alla prima; do-re-mi diviene mi-re-do
- _inverso_ -> vengono invertiti gli intervalli e permanendo lo stesso frammento, abbiamo come primo intervallo (nel caso dell'esempio) un intervallo di seconda discendente, quindi con un'inversione avremo un intervallo di seconda ascendente, l'inverso del frammento evidenziato sarà _si-si, do, do, re, mi_; oppure do-re-mi diviene do-si-la

>Esse sono variazioni di retrogrado, inverso e retrogrado dell'inverso.

#### Considerazioni sui procedimenti

Una combinazione di questi procedimenti realizza molte possibilità per la scrittura e l'applicazione di questi principi è anche possibile a materiali molto semplici.

## Tecnica omofonica

Una voce è la principale e le altre le fanno da accompagnamento, la tecnica omofonica raggiunge il suo apice nel periodo classico ed è intesa essere rappresentata nella cosiddetta forma sonata, quindi la forma sonata è la summa della forma omofonica, ovvero laddove vi è un tema che si rapporta a tutta la composizione, vi sarà quindi una voce in particolare che chiameremo tema.

Tale tecnica si sviluppa in concomitanza anche con lo sviluppo della musica strumentale. La musica strumentale infatti determinò due punti chiave nella storia della musica:
- svincolarsi da una scrittura che le soli voci potevano dare, iniziando a scrivere con maggiori estensioni(come quella per pianoforte)
- trattamento delle dissonanze melodiche, infatti una voce che deve intonare un intervallo dissonante trova delle difficoltà, mentre uno strumento come il pianoforte ha minore difficoltà, e non si avranno più limitazioni dal punto di vista melodico, con intervalli dissonanti all'interno dei temi che difficilmente le voci potrebbero intonare



### Organizzazione formale della musica
>La musica strumentale ha dato apertura alla scrittura, ma ha creato un problema ovvero l'organizzazione formale di un pezzo di musica.

Attraverso delle sezioni della musica, bisogna dare delle sezioni per dare senso alla musica. Come per il parlato la suddivisione del discorso deve seguire una forma che distingue varie sezioni.

Ovviamente all'interno di alcune sezioni vi sono altre sezioni, come ad esempio la nostra lezione. La lezione deve infatti seguire una forma.

All'interno di ogni singola sezione, ve ne sono altre e ciascuna sezione ha un'autonomia ed un senso compiuto ed ognuna delle sezioni ha una sua _chiusa_ ed ha quindi una sua cadenza.

Avremo quindi un'organizzazione fatta di tante sezioni più o meno grandi fatte di un'inizio ed una fine.

Ragioniamo quindi sempre:
- inizio
- sviluppo
- fine (chiusa)

>Tutto quanto tende ad una chiusa, ovvero ad una parte in cui si tende ad esaurire.

All'interno delle sezioni vi sono altri momenti di inizio e fine.

Dobbiamo ragionare dal punto di vista di un brano con una serie di parti che si susseguono fra loro una dentro l'altra che costituiscono del pezzo la sua inintellegibilità.

>Questo si è formulato con la strumentalità, perchè precedentemente era il testo poetico che dettava la forma.

Laddove c'è un testo, la forma musicale è quella del testo.

#### Esempio di un testo e forma dello stesso

Testo musicato da Monteverdi:

![7](7.png)

Osserviamo che abbiamo tre strofe, ovvero tre sezioni.

Vediamo che il contenuto della seconda strofa è diverso dal contenuto della prima strofa.

Come anche il contenuto della terza strofa è diverso dalle precedenti.

Vediamo che abbiamo gli ultimi due versi uguali per le tre strofe. Vediamo che all'interno di ogni strofa abbiamo altre sezioni e poi all'interno di ogni verso abbiamo anche una forma.

_Ascolto del testo musicato da Monteverdi al [seguente link](https://www.youtube.com/watch?v=q-eHoSSCtNY&ab_channel=FrancoRadicchia%26Armoniosoincanto-Topic)_.

Si percepisce l'esistenza delle voci che corrispondono al verso ed alla strofa, si notano i procedimenti di imitazione e ripetizione del testo e della musica.

Il fatto di riferirsi sempre alle forme poetiche ha evitato nel tempo una strutturazione degli aspetti formali della musica.

Ma senza testo si è dovuto iniziare ad organizzare il materiale in maniera diversa.

#### Sezioni, forma ed altro

Le sezioni sono in rapporto tra loro, come fossero le parti di un organismo vivente, quindi l'organizzazione formale è un'organizzazione che deve tenere da conto i rapporti, le somiglianze e ciò che avviene tra una parte ed un'altra.

Ad esempio l'organizzazione della lezione passa attraverso frasi, periodi ed argomenti e quello che si dice in un istante sarà in relazione a ciò che è venuto in un passato e ciò che avverrà nel futuro.

Tra loro le sezioni sono legate e le une sono legate alle altre.


### Il testo di Schoenberg e la forma

Utilizzeremo il testo di Schoenberg: _[Elementi di composizione musicale](/Schoenberg_Arnold_Elementi_di_composizione_musicale.pdf)_

>Bisogna sapere l'ampiezza delle singole parti e la complessità fra di esse.

![8](8.png)

Le parti sono di varia ampiezza e sono in relazione fra loro.

#### La proposizione


![9](9.png)
Essa è la più piccola unità strutturale della musica.

Bisogna rendere questa nozione teorica, pratica, ma tale proposizione quanto deve essere lunga?

Una serie di suoni ha un senso compiuto quando:
![10](10.png)

Ogni volta che prendo un fiato arrivo a determinare una proposizione. Ciò è un riferimento all'umanità ed all'essere umano.

Esempi di proposizioni:

![11](11.png)

>Ciò che cantiamo con un fiato solo dovremo ricondurlo a una strutturazione con le battute.

Non prendere in considerazione le battute è solamente un atteggiamento evoluto rispetto al tempo che serve per un fiato.

Un fiato sarà in genere una durata convenzionale di due battute.

Vediamo infatti negli esempi precedentemente mostrati un paio di battute a proposizione con inizi in levare (come la primissima proposizione) o inizi tetici (come il terzo esempio).

- acefalo (manca il  battere) -> quinta di Beethoven (sol-sol-sol-mi)
- anacrusico (in levare) -> primo esempio
- tetico -> secondo esempio, terzo esempio

#### Tema

Sappiamo che il tema è alla base della musica omofonica, esso è elemento fondamentale dell'organizzazione musicale.

Un tema, secondo Schoenberg, ha una durata convenzionale di 8 battute.

Un tema ci dice sempre Schoenberg è di 8 battute, quindi la proposizione sappiamo essere di due, quindi per costruire un tema serviranno quattro proposizioni.

Il nostro obiettivo sarà di costruire un tema, partendo da una proposizione di partenza, e vi sono due modi per costruire le 8 battute del tema, e avremo una distinzione di:
- frase
- periodo

Le 8 battute del tema sono articolate o in forma di frase o in forma di periodo.
![12](12.png)

Il tema parte, arriva ad una tonica ed arriva ad una chiusa che presuppone la continuazione.

Il tema si costruisce attraverso l'utilizzo di due concetti fondamentali che troviamo anche nel contrappunto, e che nella tecnica omofonica sono alla base dello sviluppo della tecnica, tali concetti fondamentali sono quelli di:
- ripetizione -> importantissimo nella musica e nella didattica, oltre che nel parlato
- contrasto -> utile per rinforzare le informazioni precedentemente presentate, contrapponendo due tipologie di elementi che ci permettono di percepire due elementi in contrasto fra loro

>Il linguaggio musicale dello stile classico/omofonico non può prescindere dalla ripetizione e dal contrasto.

##### Come fare le ripetizioni?

La ripetizione può essere di due macro-tipologie:
- ripetizione identica -> in cui si ripete esattamente la stessa cosa, con un risultato che genera una monotonia per chi ascolta, avendo l'accortezza di posizionare nel posto giusto
- ripetizione variata -> la variazione è una tecnica indispensabile per poter procedere nella composizione, quindi una ripetizione variata che mi rende percepibile la ripetizione come tale, è una ripetizione trasposta, come potrebbe essere _sol-sol-sol-mi, fa-fa-fa-re_; in cui la ripetizione variata è la stessa proposizione ma a diversa altezza, ciò dà la percezione all'orecchio che sia una ripetizione ma senza che si ripeti esattamente ciò ascoltato precedentemente, in questo caso la variazione minima interviene solo su un parametro del suono, ovvero la frequenza dove avviene la proposizione

###### Variazione di una figura

Un uomo con occhiali, maglietta e cuffia è una persona, togliendosi gli occhiali, si ha sempre la stessa persona, modificando altri aspetti di una persona, con una parrucca o del trucco si avrebbe una ulteriore variazione della persona, quindi le variazioni possono essere lontane o vicine alla persona vista inizialmente, quindi se io variassi oltre alla collocazione della frequenza, il ritmo o se volessi aggiungere alla variante delle ottave.

Il contrasto con la proposizione di partenza potrà nascere ad esempio da una determinazione di variazioni simultanee di più parametri.

>Più la variazione è complessa, più la percezione va verso il contrasto.

#### Come si ottiene la ripetizione
La ripetizione si ottiene attraverso variazioni piccole di uno o due parametri.

#### Come si ottiene il contrasto

Attraverso la variazione di più parametri simultaneamente.

#### Esempio di variazione

![13](13.png)

Vediamo ad esempio che nella quinta sinfonia di Beethoven, si ottiene per mezzo della variazione un modo per trovare materiale che si possa usare.

#### Variazione partendo dalle tecniche esposte per il contrappunto

![14](14.png)

La conoscenza delle modalità di variazione usate nella tecnica contrappuntistica sono utili anche per il tema.

### Lettura del testo di Schoenberg

In particolare dall'inizio fino a pagina 126.

## Forma ternaria: la piccola forma ternaria A-B-A'

Sappiamo che il tema debba essere di 8 batture e possiamo avere esso in due forme:
- forma di periodo
- forma di frase

La tipologia di forma è data a seconda di dove interviene la ripetizione.

### Periodo

Sappiamo che nelle prime due battute abbiamo la proposizione quindi essa è uguale sia nella forma di periodo che nella forma di frase ed occupa le prime due battute(come l'inizio della Quinta di Beethoven). Abbiamo quindi una ripetizione immediata con una piccola variazione, ovvero nel caso della Quinta di Beethoven avremo una trasposizione.

Nella forma di periodo si prevede la ripetizione della prima proposizione.

![15](15.png)

Vediamo che nella terza parte del tema avremo il contrasto, mentre nelle ultime battute avremo la chiusa, ovvero la liquidazione di tutti gli elementi, e con il sostegno aromonico essa è maggiormente efficace con una cadenza su dominante seguita da tonica (liquidiamo gli elementi più importanti che hanno preceduto).

Vediamo quindi un inizio, uno sviluppo(con ripetizione e contrasto) ed una fine.

### Frase

Essa è una forma che ha la ripetizione diversamente collocata, ovvero la ripetizione tra la quinta e la sesta battuta. Avremo qui una ripetizione identica alla proposizione iniziale.

![16](16.png)

Sulla parte finale abbiamo sempre la chiusa, mentre avremo nelle battute 3 e 4 il contrasto.

### Considerazioni finali su periodo e frase
In base a come abbiamo la posizione degli elementi avremo una forma diversa:

![17](17.png)

Vediamo che abbiamo una possibilità con il periodo con una ripetizione seguita da un contrasto, mentre nella frase abbiamo prima un contrasto e poi una ripetizione.

### Considerazioni sull'armonia
Con questo ragionamento ci siamo fino ad ora limitati alla parte melodico-rimtica, ma non abbiamo osservato la parte armonica che dovrebbe essere osservata ancor prima della parte melodica.

La struttura armonica costituisce lo scheletro ci cemento armato di una costruzione.

Avendo una struttura forte, sopra a una struttura armonica potremo costruire vari elementi.

Molto repertorio monodico si basa su una forte alternanza di V-I.

È importante avere una struttura armonica ben definita e sappiamo che dovremo avere primo grado all'inizio ed alla fine:
![18](18.png)

Avremo quindi una struttura che inizia a prender forma, avremo quindi una ripetizione al quinto grado, ed un contrasto potremo farlo al primo grado, o potremo inserire il quarto grado per quanto riguarda il periodo.

![19](19.png)

___

La prossima volta deci
