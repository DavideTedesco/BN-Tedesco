# Appunti della lezione di giovedì 17 marzo 2022

## Psicologia culturale

>Cos'è il sapere e come si acquisice?


Adottiamo la cultura dell'educazione di Gerome Bruner, che acquisiamo con un confronto fra pari.

Si mette a punto una cultura che si negozia in un lavoro lungo e laboratoriale con una community di pari.

Vi è uno scambio fra pari che motiva l'apprendimento.

Dobbiamo quindi essere costretti con uno scontro fra pari.

## Scontro fra pari

(Give p's a chance) con le 4 P dell'apprendimento creativo.

___

## Riferimenti bilbiositografici

Ci siamo messi nell'opzione culturale dei gruppi e in un'opzione sull'apprendimento chiamata euristica che ritiene che il laboratorio sia una condizione irrinunciabile per imparare la musica.

Raramente si fa laboratorio, perchè si inizia dal quaderno pentagrammato?

Ci siamo posti in un'opzione laboratoriale, in cui assumiamo che ovunque anche in un'aula di primo grado, dove non c'è niente, la musica è così pervasiva che un insegnate creativo dice: quali sono gli ambiti di attività per poter realizzare dei laboratori per indurre delle azioni con il gruppo.

Poter quindi indurre dalla teoria, la pratica.

### Quattro schemi per l'attività laboratoriale
Abbiamo condiviso degli schemi che riportavano:
- parlando -> il parlare centra con la musica
- suonando -> servono gli strumenti (ma si può utilizzare anche la body percussion)
- ascoltando
- cantando

### Cinque codici comunicativi

- comunicare con il timbro -> con la musica si comunica con il timbro, esso è un codice comunicativo
- comunicare con il ritmo
- comunicare con la melodia
- comunicare con la forma
- comunicare con l'armonia -> modo in cui si tessono delle sovrapposizioni

### Testi

![1](1.png)

__Insegnare la musica. Guida all'ascolto di comunicare con i suoni. Lucchetti, Ferrari, Freschi__

Vi sono 5 capitoli ed in base al gruppo in cui siamo procurarsi il capitolo del gruppo.

A tutti i gruppi si chiederà di presentare un progetto completo e un piccolo testo su come si può lavorare a scuola su un elemento in particolare.

>Dobbiamo imparare a condividere il saper e il saper fare con altri elementi che non fanno parte della nostra community.

Un Riferimento bibliografico per cercare degli aiuti per quello che il gruppo sta facendo e dovrà fare.

### Riferimento sitografico

__[musicaascuola](www.musicascuola.indire.it)__

Indire da qualche anno ha acceso una lente sulla musica sull'ambito laboratoriale. In questo portale vi sono alcune indicazioni generali che si chiamano ["Quadro didattico"](https://musicascuola.indire.it/index.php?action=vedi_pagina&id=1422).

È importante un'altra sezione denominata ["Buone pratiche"](https://musicascuola.indire.it/index.php?action=buone_pratiche) con progetti allestiti dal docente per poter motivare i suoi studenti in una data direzione.

Ora siamo in una seconda fase, ora cosa costruiamo, cosa inventiamo, cosa componiamo?

La terza fase sarà una fase comunicativa in cui si condivide il sapere.

Esplorare -> Coinvolgere -> Motivare

Esse sono sempre le 3 fasi di un progetto.

Nelle Buone pratiche vi sono delle playlist che spiegano come ha funzionato un progetto per imparare:
- il sapere
- il saper fare
- il saper far fare

Interessanti e utili dalle buone pratiche:
- Sud Orf
- Battimani
- Paesaggi sonori del Novecento
- Gridava
- Soundscapes

Se ne si trova uno interessante, ricavare alcuni concetti chiave che un'insegnante propone.

>Il libro e il sito fotgrafano un momento del presente.

___

## Riepilogo delle scorse lezioni: esplorazione e elaborazione

Abbiamo iniziato parlando con una terzina dell'inferno di Dante con gruppi che hanno trattato:
- Timbro
- ritmo
- forma
- melodia

Visione della traccia delle lezioni 1 e 2.

I primi 3 gruppi hanno realizzato la fase 1 e la fase 2:
- fase 1 -> esplorazione
- fase 2 -> elaborazione

Avevamo visto i dispositivi che sono venuti fuori:
![2](2.png)

Ritrovarsi e lavorare ancora per costruire qualcosa di ciò che è indicato nei quadri sottostanti, ovvero il "cosa e come educere"

In Giallo vediamo gli strumenti compensativi per favorire la direzione in cui si potrebbe andare.

Ad esempio le flashcards.

(Mozart un "Piccolo tema anapestico" sulla sinfonia 40.)

## Strumenti di supporto

Le flashcards sono ad esempio degli elementi per guidare un'intelligenza ritmica.

## Intelligenza
Cos'è l'intelligenza rimtica?
Essa è la capacità di:
 - comprendere un ritmo
 - riprodurre un ritmo

>Le varie intelligenze sono delle aree neuronali che si collegano a varie rappresentazioni mentali, con film sonori che si fanno nella testa.

Un'intelligenza è un corredo di elementi, ovvero un corredo di correlazioni mentali possibili.

Ognuno ha nella sua intelligenza ritmica (capacità di evocare alla mente pensieri rimtici) con zone più o meno sviluppato, e lo zoccolo duro sono le memorie.

Vi sono dei pattern più presenti nella memoria musicale ed altri meno e nell'archivio del database dell'intelligenza ritmica, si avranno elementi diversi.

## Dispositivi per lo sviluppo dell'intelligenza ritmica

![3](3.png)

I 3 gruppi ascoltati si erano inventati 3 cose per il ritmo.

>Promuovere laboratori per sviluppare l'intelligenza ritmica.

I pattern ritmici, le sequenze ritmiche, che riesco a comunicare con maggiore efficacia, sono quelli che ho più chiari e che ho compreso maggiormente.

Senza la comprensione approfondita di un brano, il messaggio importante non si capisce.

### Gruppo 4

#### Fase 1: esplorazione
- gruppo 4
  - rappresentazione del movimento libero e declamato
  - cercare le pulsazioni
  - trovare e suonare lo schema ritmico del verso

#### Fase 2: elaborazione

Avevamo visto il metodo Orff.

Con pattern ritmi ostinati con delle flashcards con pattern ritmici.

Se un solo verso sembra difficile possiamo prendere mezzo verso.

Le parole guidano un ostinato verbale, che diviene un ostinato corporeo, con uno sviluppo macrocorporeo.

>La manualità fine dello sviluppo corporeo aiuta al suonare.

Le parole offrono una guida per la comprensione:
- guida per la comprensione verbale
- guida per la comprensione sonora
- guida per la comprensione corporea

Al primo gruppo si propone di lavorar sugli ostinati, proponendo di realizzare degli ostinati ritmici.

### Gruppo 5
#### Fase 1: esplorazione


Essi avevano inventato uno strumento compensativo: modi diversi di scrivere i versi di dante, andando a capo e cercando di individuare tramite la scrittura un ritmo, realizzando quindi un pattern con individuazioni ritmiche determinate dagli a capo.

#### Fase 2: elaborazione

Si chiederà di rilavorare sull'intenzione ritmica, con un'intelligenza ritmica capace di riorganizzare il flow, avvalendosi anche di confronti musicali.

Composizione di playlist comunicativa di elementi dal rap alla melodia infinita.

>Pensare all'intelligenza ritmica come la gamma di intelligenza che ho per articolare il ritmo.

Esempi di musica strumentale con flussi articolati diversamente:
- beat
- fraseggio
- preludio del Tristano

È importante realizzare un dettato ritmico, andando a capo con la musica, imparando a sentire le segmentazioni del flow.

Applicare la terzina a diversi generi di segmentazione musicale.

- A -> costruire un'antologia
- B -> segmentare in modi diversi trasferendo a un qualcosa di noto

Si può chiedere dopo aver proposto degli esempi registrati(antologia di frammenti):
- come viene la lettura con modi di leggere diversi
- con una sequenza ritmica, ad esempio con dei versi suonati
- con una sequenza melodica

>Fare delle prove di commutazione, con un concetto di rimtica indicato dal fraseggio.

### Gruppo 6

#### Fase 1: esplorazione

Vediamo qualcosa di affine al gruppo 4.

Trovare dei pattern ritmici a diversi allievi serve per dare dei ruoli diversi.

Concertare dei ruoli diversi, ovvero individuando la macchina ritmica (da Dalcroze).


#### Fase 2: elaborazione

Sovrapposizione di ostinati in cui le parole ci guidano.

![4](4.png)

Visione del video del gruppo 6.

>Bisogna saper organizzare i ruoli di tutti i tipi, e realizzare anche un qualcosa di più difficile.

Realizzare un congegno con giro di monodie.

L'identità musicale della prof deve far permeare la passion, la passione.

### Riepilogo fino ad ora

- abbiamo citato Dalcroze -> perseguendo l'idea di Dalcroze, ci chiediamo, come facciamo a comprendere un ritmo? bisogna comprendere a livello macrocorporeo prima di comprenderlo a livello microcorporeo, improvvisando una sequenza in cui vi è tante volte un pattern, facendolo entrare in una sequenza macrocorporea "qualunque tipo di intelligenza musicale si può affinare con il corpo"
- Orff

>Mettere gli alunni a risolvere un problema, dicendo che si è scoperta una cosa

### Gruppo 7

#### Fase 1: esplorazione

Dispositivo trasferibile che può far diventare il parlato -> melodia.

3 momenti:
- non melodia, letto in maniera statica
- letto conseguentemente
- letto con melodia ascendente
- letto con melodia discendente

>Anche l'intelligenza melodica è comprendere e rieseguire.

Griglie chiave della comprensione melodica, proprie di una cultura.

Hook per le scale musicali con una grammatica della melodia. Con una gamma limitata.

Prendere un contenuto parlato e cantarlo.

>Canzoni per intervalli -> Vaccalli

Per far cantare i bambini e sviluppare l'intelligenza melodica:
- buona pratica è il songwriting con i loro nomi
- prendere dei repertori di canzoni che vadano per intervalli precisi (con incipit particolari)
- utilizzo dei dispositivi usati nella musica tradizionale:
  - utilizzo della scala
  - accordo, ovvero intervallo di terza

Per comporre uno hook ad esempio:
- seguiamo uno schema scalare
- seguiamo uno schema accordale

>Prima della melodia viene la prosodia, parlato con un senso melodico (Verdi che enfatizza la prosodia popolare).

La melodia ha due schemi chiave per la comprensione:
- scala
- arpeggio

>Le melodie hanno una grammatica.

Molte delle melodie che ci ricordiamo sono agganciate ad uno schema di direzionalità, ma anche ad uno schema di organizzazione delle note.

(Bonifacio Asioli, il primo grammatico della melodia che doveva codificare il bel canto?)

>Per lo sviluppo dell'intelligenza melodica ognuno ha un proprio modo.

Le melodie più facili da apprendere sono quelle che seguono uno schema.

>La scala è un esercizio per le orecchie oltre che per le dita.
#### Fase 2: elaborazione

Un'esercizio è cambiare scala per cambiare lingua come in Madama Butterfly.

Un modo può essere classificare le canzoni, come intervalli o impianto melodico.
___
## Music Lab: Chrome Experiments

![5](5.png)


![6](6.png)
___

### Gruppo 8

#### Fase 1: esplorazione
Partire dalla conoscenza

Melodia chiave

#### Fase 2: elaborazione
Far capire l'articolazione ritmica della melodia.

Fill the gap, lavorare sulle domande e le risposte, lavorando sulla canzone bucata.

La musica ha dei buchi, preparare un repertorio di compiti in cui si lascia uno spazio, e il maestro accetta anche delle variazioni.

Differenza tra acculturazione e alfabetizzazione...

1. I bambini devono cantare
2. bisogna far cantare cose di varie epoche
3. gioco delle commutazioni, per far rendere chiaro ciò che rimane invariato e ciò che cambia (cambiare la melodia, il ritmo, e lavorare sui parametri già conquistati)

>Dispositivi per mettere in moto l'_intelligenza melodica_, comprendere dei paletti e poi far capire molto chiaramente la melodia.

Gioco del completamento melodico, con modalità di trasmissione del canto che non è puramente di ascolto, ma è interattiva.

Ciò serve a capire dei concetti di rapporti musicali:
- accompagnamento
- melodia

___
Giovanni Piazza -> strumenti a barre

Bisogna fare da subito il gioco dell'orchestra.

Per scoprire come completare si utilizzano gli strumenti a barre mobili usate da Orff.

Coinvolgere il gruppo anche con melodie diverse.
___

Registrare audio:
- con comprensione dell'articolazione ritmica
- preparare delle soluzioni pratiche da far ascoltare agli allievi (della tipologia "fill in the gap")
- registrare la melodia accompagnata con sostegni armonici fissi

### Gruppo 9

#### Fase 1: esplorazione

Citare l'identità musicale con un legame.

Caratterizzare il mood per mezzo della melodia.

Il dispositvo è:
- cantare in modo diverso, facendo diverse prove fino a trovare una fluenza

Sensibilità alla voce ed alla voce cantata.

I versi se li ricorderanno a memoria per molto tempi, usare una frequenza mirata.

Intonare il parlato per acquisire una ritualità. Nei codici rituali di tutte le culture.

Gli schemi cognitivi sono universali.

Per l'homo musicus intonare vuol dire dare un testo ed una maniera.

Come intoniamo un testo? Modalità ibrida.

Cantare fa esprimere le radici, stile di canto, come il canto monodico delle regioni del sud Italia.

Alan Lomax e la registrare...

Stili e funzioni della melodia, un bambino sente melodie per parlare e per ballare, dalla tipologia di ritmo e melodia.

>Modi di parlar cantando.

#### Fase 2: elaborazione

Dato un incipit, generare melodie diverse.

Data la coda, generare melodie diverse.

Se si ha un incipit di canto monodico italiano, si fa una differenza tra canti monodici e polifonici.

I capitoli si possono suddividere in base alle funzioni.

Prendere uno stimolo e realizzare un'etimologia di cantori italiani che cantano.

"Chant d'Italie" pubblicata dai francesi.

- incipit
- sviluppo
- chiusa

Chiedendo di inventare a tono quelli mancanti.

Schema 1, anche grafizzato:
- inventare un audio in cui c'è una parte e si realizza la chiusura
- sviluppare un qualcosa da una logica di stile melodico

___

## Prova d'esame per il gruppo
Nella verifica del corso vi sarà una prova per i gruppi in cui si confeziona per la fase due in un modo che sia performativo.

Costruire un testo un testo per l'intelligenza specifica, per capire come si sia costruito.

Realizzare un testo che corredi le cose che si sono fatte.

___

Prima sessione, seconda settimana di giugno.

___

Il progetto:
- progetto con sonorità affini con le parole chiave
- multitraccia che fonde
- pensiero timbrico -> pensare a delle possibilità varie
- archivi di suoni

1. Ricerca timbrica
2. Esplorare in maniera più chiara

1. ricerca sul paesaggio sonoro
2. ricerca sulla formalizzazione

Scandire i tre momenti in maniera chiara...

___
## Progetti INDIRE
- Paesaggi sonori del Novecento
- Soundscapes
- Stele
- Kandinksij
- Danze di adolescenti -> sagra della primavera 
- il canone
