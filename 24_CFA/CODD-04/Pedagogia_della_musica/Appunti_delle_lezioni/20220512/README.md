# Appunti della lezione di giovedì 12 maggio 2022

Professore di Franca Ferrari (Gino Stefani).

## L'esame

L'esame in questa sessione estiva avrà due appelli:
- 24 maggio per chi sono più frettolosi
- sessione successiva a fine giugno

Seguendo i [temi di studio](https://docs.google.com/document/d/1EZTtdkYfy4FogDdXI13xfubQXW_MCZZz/edit) prepareremo delle scalette ognuno per i vari temi, per articolare ognuno dei temi all'interno dei temi.

L'esame sarà scritto sarà necessaria una firma e una foto.

Scrivere i concetti anche in maniera schematica...

Quasi tutti i quesiti presuppongo dei lavori realizzati collettivamente.

Ricordarsi la normativa in vigore nel nostro paese sui lavori collettivi.

Specificare che il lavoro è stato discusso insieme nelle varie componenti.

>Se si svolge un lavoro con il gruppo, inserire i nomi delle componenti del gruppo.

Le domande hanno già delle indicazioni...
- Domanda 1 già svolta allegando il progetto del gruppo 1 (elaborazione sonora e elaborazione testuale, realizzando un film musicale)
- Domanda 2 -> si può svolgere sulla traccia del progetto già fatto o sulla traccia dei progetti di Federica Pilotti

Per tutte le altre richieste, si ha una richiesta di riferimento a modelli educativi, si può partire da delle modellizzazioni viste in gruppo.

>Un buon progetto lo si può usare per molte domande.

- Domanda 8 -> spiegare come applicare il progetto svolto per il quesito numero 1, redarre un qualcosa che sia improntato all'utilizzo per la classe, progettando come se fosse un'azione per una classe

- Domanda 5 -> importante tematizzare come costruire progetti musicali interculturale, facendo capire che si dispone sia di modelli teorici che di modelli pratici: la multicultura non la si fa da sa, già c'è; l'inter-cultura la si può spingere, ponendo dei segni di apertura verso altre culture, cosa collega due musiche? vi è qualcosa di più a livello culturale? _Se vi sono funzioni trasversali, si possono creare delle playlist che riguardino alla funzione?_ Si può quindi costruire un progetto in cui i documenti di culture diverse vengono intrecciati.

[Arte dei Partimenti e tarantelle del Gargano: dalle tradizioni musicali italiane, nuovi sentieri per la didattica strumentale e per l’improvvisazione in classe](https://www.youtube.com/watch?v=6tuJOTLnb4U&feature=youtu.be&ab_channel=MusicaaScuolaIndire)

___
Allegare dei link e degli esempi...

Cosa significa:
- __modello teorico__ a cui arriviamo realizzando una forma per i gruppi
- __modello operativo__

Fino ai 15 anni ci si porta dietro qualcosa del pensiero concreto, mentre poi si arriva al pensiero logico-formale dai 15 anni in poi(quante musiche ABA si conoscono? quante musiche con i ritornelli si conoscono?) pensare per forme ed organizzare dei progetti piccoli basati sulla forma è molto importante, con ad esempio l'arrangiamento realizzato con il progetto _Diverso non stona_ in cui con l'allegretto di Diabelli inseriamo all'interno della forma elementi, facendo crescere con il pensiero formale (la strategia sono degli arrangiamenti che fanno esaltare delle componenti della musica anche complesse).

- Domanda 7 -> sul paesaggio sonoro, ed il lockdown ha rilanciato il paesaggio sonoro come ambiente di apprendimento musicale, chiedendosi cosa fare alla passeggiata sonora (sound walk), l'analisi e ricomposizione (sound scape analysis e soundscape composition), soundscape design come la fine del video di Enrico Strobino. Riferirsi ai progetti riportati [qui](https://musicascuola.indire.it/index.php?action=buone_pratiche) come i lavori di Maurizio Vitali

Template EAS di Federica Pilotti su progetto di Flavia Gianfreda sulla Ninna Nanna(https://drive.google.com/drive/folders/1MbLSsfVAPItC2fnLiGiUu_RoQ1l0FNJ_), importante è inserire anche la valutazione.

>Sul file dei temi di studio osservare

Pagine 55 e 56 del curricolo...

___

## Ascolto del brano sul timbro sul Laboratorio su Dante Gruppo 1

Condividendo l'ipertesto preoccupatevi che si capisca tutto, ovvero come si è svolto il lavoro, si parte da un testo e si arriva a un lavoro sonoro? In cosa consiste l'ipotesi progettuale? Pensare che scrivendo il testo stiamo parlando con un collega un insegnante che dovrà capire di cosa si tratta il progetto descritto...

## Ascolto della Ninna Nanna di Benedetto Oliva

Didattica e composizione, questa Ninna Nanna dove la possiamo porre?

Si può realizzare una piccola playlist di modelli di Ninna Nanna già realizzati.

- elementi come i ritornelli, ripetizioni di una melodia
- ritmo ->
  - pulsazione lenta
  - metro di 3(Ninna Nanna di Brahms) o 6(metro composto)
    - in 3 qualcosa di ipnotico
    - in 6 dondolare o cullare
- timbro -> omogeneo, tastiera e archi, portamenti del violino, il violinista mima una voce(aspetto ritmico delle ninna nanne che riguarda l'articolazione dei suoni)
- melodia -> frasi che si ripetono con variazioni
- armonia
- forma composizione senza fine, un continuum

Laboratorio di composizione condivisa a più mani.

>Per apprendere i ragazzi e coinvolgerli nella collettività bisogna dargli un compito.

>Ninna Nanna che vai, movimento che trovi; gesto ondualtorio supporta i 6/8...

[Atahualpa Yupanqui - Duerme negrito](https://www.youtube.com/watch?v=ROJzhe-zw98&ab_channel=BriandaVeronna)

Le scelte metriche derivano dalla melodia, che è una melodia che cantava la nonna di Benedetto Oliva.

Ninna nanne:
- nord in maggiore
- sud in minore

I testi delle ninna nanne erano l'unica modalità espressiva per gli stati di malessere delle donne (testi sulla struttura testuale di una ninna nanna).

Bella Ciao, e le sue origini, nel libro ([i canti popolari italiani](https://www.amazon.it/canti-popolari-italiani-Roberto-Leydi/dp/B003O9F8BA)), esso è nei canti e filastrocche infantili. Essa è una filastrocca che se realizzata lenta può divenire una Ninna Nanna.

Archeologia del suono per riportare in vita anche dei suoni che sono scomparsi.

Lo strumento numero 1 di un etnomusicologo è un registratore.

Allenarsi ad avere un castello a 5 porte?

### Considerazioni sull'ascolto

- fase 1
  - ascoltare una Ninna Nanna di un compositore famoso
  - composizione di una Ninna Nanna per i ragazzi

La fase 1 serve per capire gli elementi di una musica, per sviluppare una competenza fonologica e non solo fonica...

Analisi motivata e funzionale...

___

## Lavoro con il gruppo per guardare insieme i quesiti

[Temi di studio commentati con il gruppo](https://docs.google.com/document/d/1F3I0DjVd16vzqfikERBhuzex6hJiwzdYGOl1rcITvkM/edit)

___
Sarà introdotta una Scuola di Specializzazione dell'insegnamento.

Sorvegliare alcuni elementi sulla forma, cosa vuol dire dare forma a un materiale sonoro.

Progetto è projectare
