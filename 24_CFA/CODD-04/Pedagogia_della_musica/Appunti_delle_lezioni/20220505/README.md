# Appunti della lezione di giovedì 5 maggio 2022

## Webinar consigliati della settimana INDIRE

Roberto Agostini e Leo Izzo

Daniele Berardino

## Cosa sviluppare come EAS (Episodi di Apprendimento Situato)

La lezione del Docente Federica Pilotti.

Sintesi delle [4 proposte](https://drive.google.com/drive/folders/1MbLSsfVAPItC2fnLiGiUu_RoQ1l0FNJ_) realizzate rivedendo e sentendo i lavori di gruppo sentendo la terzina di Dante.

Usciamo dal corso avendo una serie di UDA, Unità Di Apprendimento.

Come l'unità di apprendimento osservata con la Pilotti la scorsa settimana.

Abbiamo osservato che quando si lavora con un gruppo è importante che tutto il gruppo capisca dove stiamo andando, motivando con la costruzione di un oggetto concreto.

Realizzare il tempo dell'anno accademico in cicli, articolando in progetti didattici in cui coinvolgiamo e mettiamo a punto una serie di risorse per costruire un qualcosa entro una scadenza. Realizzare dei traguardi di competenza che si realizzano in connessione con tracce concrete.

>Il tempo non alfabetizzato si confonde.

È molto difficile gestire il tempo, se tale tempo non ha un ritmo.

Con gli EAS si creano quindi episodi conclusi.

### Il tempo deve avere un ritmo

Vedremo come la musica aiuterà a rappresentare le rappresentazioni mentali del tempo.

Realizzare episodi e quindi apprendimento situato, ovvero _compito autentico_.

Realizzare quindi ascolti:
- cantando
- ascoltando anche un audio

### Apprendimento situato come giochi di ruolo

Realizzare dei giochi di ruolo, ponendo delle basi teoriche e metodologiche legate alla didattica di Pier Cesare Rivoltella.

Il concetto di UDA è un qualcosa in cambiamento, ovvero un progetto che si situa in un orizzonte di traguardi più vasto del proprio oltre i propri traguardi.

Il progetto, ovvero l'azione didattica disciplinare dovrebbe avere un orizzonte più ampio.

#### Costruire una sedia da giardino

Realizzazione di una tabella che aiuti tutto quello a cui i ragazzi debbano pensare, con una tabella che guida all'analisi del campionario e che guida anche alla realizzazione del modello da giardino dei ragazzi.

##### Cosa ha fatto Flavia

Flavia ha realizzato uno schema sulla base dello schema della Pilotti.

>Il compito situato in prima e seconda media si potrebbe pensare di farlo realizzare.

> osservazione di testo e musica, trascrivendo le parole ([libro di Stefano La Via](https://www.amazon.it/Poesia-musica-poesia-trovatori-Conte/dp/8843038990))

1. il primo passo è ascoltare più ninna nanne (utilizando più link) scegliendo almeno 3 link, fornendo la playlist
2. ricercare azioni e elementi testuali e operazioni sul testo della ninna nanna(anche al docente di Italiano interessa l'analisi del testo):
  - ricercando versi
  - riconoscendo la composizione sillabica
  - ricercando ritornelli
  - rime baciate
3. comprendere:
  - componenti timbriche nei vari esempi proposti
  - descrivere il suono
  - osservare le componenti ritmiche:
    - beat
    - pulsazione
  - metro(misura del movimento)
    - regolare
    - non regolare
  - figure ritmiche ricorrenti
  - melodia e sua composizione
    - catturare frasi e motivi che si ripetono
  - forma:
    - si può ripetere o meno
  - melodia
4. capire la funzione di un dato elemento

>Osservare nella tabella tutte le componenti musicali, ponendo il problema dell'analisi. Già distinguere, ritmo, poesia e forma è molto importante.

###### Strumenti compensativi

![1](1.png)

Sfidare gli alunni, fornendo dritte per fargli accogliere la sfida, ovvero fornendo strumenti compensativi che mostrano eventuali elementi che aiutino a capire.

>Sviluppare la ricerca dell'effetto riconoscendone la causa, porsi il problema e cercare la causa del suono.

Esempi di strumenti compensativi:
- utilizzare l'app metronomo -> segnandosi di fianco il metronomo
- cercare uno o più moduli di body percussion che si sincronizzino bene con la body percussion
- utilizzare le flash card
![2](2.png)

>Aiutare a produrre in modo consapevole realizzando un qualcosa di sensato.

Oservare esempio di una ninna nanna di metro composto o no:
![3](3.png)

Altra ninna nanna:
![4](4.png)

Scrivere:
- ritornello guerra di Piero

De Andrè si è ispirato ai modi della tradizione italiana secondo le figure ritmiche e i metri composti.

Realizzare un codice che ci aiuti a carprie con le orecchie le figure ritmiche.

La Jamboard è uno strumento compensativo.

>Imparare a discriminare le unità di senso nel continuum percettivamente.

Discriminare in base a come le nominiamo il ritmo.

Passare alla notazione musicale dalle parole è molto più semplice per discriminare le figure ritmiche.

>Utilizzare quindi gli alfabetieri...

(realizzazione di un gruppo di pari, e l'identificazione con un adulto)

Il designer non è più una cosa da fare con i ragazzi, ma una cosa da far fare a loro.

>Realizzare gruppi di compiti che gli si hanno preparati.

##### Strumenti, progetti e link utili con le nuove tecnologie

[Chrome Music Lab](https://musiclab.chromeexperiments.com/Experiments)

[Daniela Berardino e variazioni della follia di spagna]()

[Cooperativa Il Giro del Cielo](https://www.girodelcielo.com/)

[Progetto Stele di Simone Francia](https://musicascuola.indire.it/index.php?action=vedi_singola_esperienza&id_scheda=1344)

>Sono necesarri ambienti e strategie per far comprendere la modalità di apprendimento fra pari.

#### Conclusione del progetto di Flavia

Realizzata l'analisi con la tabella bisogna anche individuare indicazioni per la valutazione:
![5](5.png)

### Questi per l'esame riformulati

Utilizzare le espressioni per formulare delle UDA.

Realizzare delle unità di apprendimento dai progetti iniziali.

Lavorare sulle competenze chiave che riguardino tutti gli insegnanti di una scuola.

>Fornire una UDA con competenze chiare e trasversali. UDA che riguardino competenze chiare trasferibili, componendo ed eseguendo delle cose.

Scrivere delle colonne con componenti musicali e poi con strumenti compensativi.

Realizzare:
- 1 UDA scrivendo quello che si è già fatto
- 1 UDA guardando quello che ha realizzato un altro gruppo (singolarmente o in gruppo descrivendo chi ha realizzato la UDA)

>Mettere in tasca 2 UDA alla fine del corso.

Realizzare in questa settimana le tabelle e il progetto di Flavia.

### Usare la community come gruppo di competenza: Confezione di una domanda con risposte a scelta multipla in un ambito del proprio sapere musicale (simulazione per la prima prova del concorso)

Domanda formulata e 5 risposte di cui due sono quasi giusta.

Giochi sul'identità che passano per il confronto delle facce.

Osservare vedendo il pentagramma di chi è una sinfonia.

>Confezionare una domanda con letteratura sinfonica.

Costringere a mettere a confronto gli altri ragazzi con temi del proprio ambito di studio.

>Considerare che nel test si possono inserire delle immagini.

Il concorso riguarda:
- la storia della musica
- domanda sulla metrica

Cosa c'è di condivisibile con altri di:
- sapere musicale -> risultato del quiz del Cineca
- saper fare
- saper far fare

### Episodio di apprendimento situato (dal link inserito da Progetto Flavia Gianfreda...)

Comporre una ninna nanna con un insegnante che ha realizzato un pre-progetto che ha vinto l'Abbado Award.

L'insegnante di musica ha convinto a trasformare la scuola di Novara per una settimana uno studio di registrazione ed avendo mandato una lettera a tutte le famiglie della scuola (sopratutto le mamme non Novaresi) con mamme estere.

>Non vi sono particolari lavori di analisi e la professoressa ha composto una sorta di presentazione sulla ninna nanna, relazione di maternità.

[Progetto Ninna Nanne Abbado Award](https://www.youtube.com/watch?v=nCyoS8FOF4s&ab_channel=FederazioneCEMAT)

### Progetto Ascoltare il Paesaggio: IC Biella (Enrico Strobino)

Progetto che vinse l'Abbado Award: [Ascoltare il Paesaggio](https://www.youtube.com/watch?v=8QQEbkjwOXw&ab_channel=FederazioneCEMAT)

Visione del progetto Ascoltare il Paesaggio.

Steps:
(All'esterno)
1. FASE 1: pensare il paesaggio sonoro: capire cos'è il paesaggio (anche domandandolo agli alunni)
2. FASE 2: registrazione del suono del paesaggio capendo un aspetto del suono che non abbiamo capito (ascoltare l'inaspettato, documentarlo, osservare), e sentendolo attraversoil microfono
 - comprensione dei suoni predominanti all'interno di uno spazio
 - realizzazione di soundscape per catturare i suoni spostandosi nello spazio
3. FASE 2: fase di comprensione delle registrazioni
  - discernere cosa vi è all'interno di un paesaggio sonoro
  - grave/acuto, intermittente/continuo, verso forte/piano
4. FASE 2: fase di realizzazione e modifica audio in multitraccia
  - utilizzando software libero come Audacity realizzare un'elaborazione del suono
  - ascolto dell'elaborazione
  - utilizzo del multitraccia come Logic
5. FASE 3: utilizzo degli strumenti nel parco con una fase di comunicazione insieme ai suoni ascoltati nel paesaggio sonoro precedentemente (importante il ritorno al parco) -> riprendere la visita con i dispositivi musicali

>Finale da educazione alla cittadinanza con gruppi di strumenti che fanno parte del paesaggio sonoro finale.

[Strobino - Il suono, l'istante e l'avventura. Educazione musicale e improvvisazione.](https://www.amazon.it/listante-lavventura-Educazione-musicale-improvvisazione/dp/B09WPZSMR2/ref=sr_1_1?qid=1651745730&refinements=p_27%3AEnrico+Strobino&s=books&sr=1-1)

[Strobino - Dum Dum Tak](https://www.progettisonori.it/prodotto/dum-dum-tak/?v=e48a9a5f6dfd)

[Strobino - Suoni di carta](https://www.amazon.it/Suoni-carta-DVD-Enrico-Strobino/dp/8888003517)
