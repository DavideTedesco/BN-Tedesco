# Appunti della lezione di giovedì 21 aprile 2022

## Avere un progetto
Siamo dentro lo sviluppo di una competenza di progettazione.

Ricordando:
- passion
- play
- peer
- project


>Realizzare qualcosa da condividere insieme.

Come per i ragazzi del Liceo Santa Rosa di Viterbo, avere due progetti da poter presentare.

Lavorare sul progetto passando da un progetto ad un altro...


### Visione dello schema del gruppo 2(di Davide Carlini) sull'IC Pino Torinese

>Mettere in moto il problem solving.

![1b](1b.png)

>Riconoscere una direzione di lettura di un monile peruviano.

>Riconoscere i movimenti del corpo.

[Musanim](https://musanim.com/)

Fino a 15 anni siamo nell'età del pensiero concreto e i ragazzi non hanno ancora un concetto astratto di tempo.

>Nell'età del pensiero concreto, la fase di comunicazione, significa far vedere quello che si è costruito.

![2](2.png)

Raccolta di un book di buone pratiche...

![3](3.png)

>Rivedere lo schema dell'Istituto Pino Torinese.

### Visione dello schema del gruppo 11 del Liceo Santa Rosa

![4](4.png)

Recupero di Bennato e della canzone popolare.

![5](5.png)

![6](6.png)

Lavoro sull'identità...

#### [Competenze trasversali](https://asnor.it/it-schede-15-le_competenza_chiave_europee)
Tra le competenze trasversali sviluppate:
- Competenza multi linguistica (comprensione ed uso dei dialetti del sud Italia)
- competenza personale, sociale e di imparare ad imparare (intervenire in un gruppo musicale con diversi ruoli in base alle esigenze del gruppo)
- consapevolezza ed espressione culturale (conoscenza ed uso dei codici tipici di un'espressione culturale: quella relativa alle Tarantelle del Gargano)


>Fare il _continuista_ (come il bassista o il violoncellista) diviene una caratteristica sociale, un ruolo ed un'attitudine.

#### [Competenze musicali](https://asnor.it/it-schede-15-le_competenza_chiave_europee)

![7](7.png)

Gli interpreti danno suono e danno senso a un qualcosa di cui vi è solo una traccia.

### Creare un progetto per quale motivo

Realizzare un format di un progetto, ovvero un format ideativo per la presentazione alla scuola e per avere un programma da seguire dal momento in cui si propone un progetto.

Per imaparare a realizzare un mestiere, si deve realizzare un professionista competente ed efficiace.

>L'educazione è prima di tutto relazione.

## Visione di un altro progetto:

Il progetto che osserviamo è un progetto che ha come argomento i BES(Bisogni Educativi Speciali).

La scuola italiana già nel 1977, fece una scelta fortissima che gli osservatori internazionali chiamano full-inclusion.

>In una classe qualunque vi sono alunni con e senza BES.

### I BES

I Bisogni Educativi Speciali (Special Lead Education) sono di tre tipologie:
- BES culturale, ragazzi che vengono da un altro paese, che potrebbero avere difficoltà linguistiche, gli insegnanti di quella classe si deve inventare delle soluzioni (magari non verbali per far interagire, non mettendo in gioco il problema linguistico)
- BES alfabetico-funzionale
- BES ...

>Il BES non deve divenire un intralcio per l'apprendimento, ma bisogna vedere le risorse che si hanno a disposizione, ed ogni insegnante in base al suo fiuto e alla possibilità di valutare risorse, realizza un programma.

(Convegno su musica e DSA per i ragazzi BES alla Cassazione organizzato dagli avvocati, anche i conservatori hanno norme da usare per la Letto-scrittura)

Nel nostro paese, il diritto per i più fragili è diventato un diritto.

Avremo i riferimenti e articoli da leggere...

### Inter-cultura e multi-cultura

Questione dell'inter-cultura e della multi-cultura.

Possiamo osservare una società italiana come una società multi-culturale, essa è una realtà sociologica che diviene un problema o un'opportunità.

Le riflessioni sul tema della multi-cultura hanno indotto a scoprire e cercare di scoprire se la classe possa divenire anche inter-culturale.

Un'inter-cultura è un artefatto!

Oggi in una classe con un ragazzo BES proveniente da un altro paese, il ragazzo può avere un problema terribile.

La differenza la farà l'artista per la realizzazione dell

### Visione del progetto: Butulumani da E.Storbin, Dum dum ciak

Ragazzi di 13 anni, ovvero con ragazzi della scuola media.

Primo momento, fase stimolo che il prof ha concertato con la ragazzina a parte:

Altri step sono di depositazione e sviluppo.

#### Primo momento

La bambina canta una canzone della sua infanzia

#### Secondo momento

- i bambini cantano senza intonazione, quadratura ritmica
- i bambini tengono il ritmo, solo con le sillabe, per capire bene come le cose devono essere ordinate; le mani vengono battute sulla pulsazione, e il testo serve per avere delle continue ripetizioni e nel testo ci sono delle parole che si ripetono
- i  bambini cantano con l'intonazione

#### Considerazioni

Il contesto è multi-culturale, mentre il progetto è inter-culturale.

La comunicazione ritmica e l'interazione ritmica è molto presente nel progetto.

Abbiamo una fluidità all'interno del progetto, e la spontaneità può realizzarsi in un ambiente preparato.

La spontaneità è un concetto lato perchè ognuno di noi può essere spontaneo in un ambiente in cui il bambino è a suo agio.

La fluidità, il piacere e l'agio è un indicatore di qualità.

È interessante capire cosa ha predisposto l'insegnante per poter realizzare il progetto finale.

>Il progetto inter-culturale, scommette sul ritmo e sulla sillabazione del testo. Entriamo in questo modo con chiarezza all'interno del testo.

##### Cosa portare in classe e come disporre i bambini

È fondamentale capire i modi e le metodologie per organizzare il lavoro.

Per un progetto inter-culturale, dobbiamo osservare cosa vi sia un comune, ovvero quale aspetto del ritmo diventi comune.

![8](8.png)

Condividere il testo, misurandolo.

##### Altri elementi

I bambini di un'altra cultura vogliono divenire del luogo in cui vivono, allora vi deve essere una fase di innesco con la ragazza.

Il prof ha trovato il libro ["All'ombra del baobab"](https://www.mondadoristore.it/All-ombra-baobab-Africa-nera-Chantal-Grosleziat-Elodie-Nouhen-NA/eai978880451843/) mondadori editore.

La filastrocca deve avere varie tipologie ed a dispetto della lingua e dei suoni che può essere molto diversa.

Una filistroca ha:
- una ridondanza
- un beat

Sono importanti queste caratteristiche perchè queste filastrocche sono sincronizzate con il movimento, per questo qualunque sia la cultura, vi è un beat, per poter realizzare un gesto che si ripete.

Con la pulsazione ci troviamo, perchè essa è il cuore della musica.

Impariamo il testo, come un testo sonoro.

(Laboratori musicali nei campi profughi.)

##### Parlare con la bambina: fase 1

La bambina con l'aiuto della mamma e del cd ha realizzato un'operazione di recupero culturale delle radici.

La bambina prepara qualcosa in un qualcosa di misterioso.

##### Parlare con gli altri bambini e fargli cantare monodicamente battendo le mani tutti insieme: fase 2

Canto monodico e battito di mani sulla pulsazione, con ostinato parlato per i ragazzi, mentre per le ragazze canto.

Il docente ha diviso prima in ruoli.

##### I bambini si dividono a gruppi di 2: fase 3

Organizzazione di percussioni con:
- djembe
- shaker

Tutta la classe viene suddivisa in coppie.

Dando nel duo una consegna sdoppiata:
- pulsazione con shaker
- sequenza ritmica del parlato sulla pelle

Vi è un lavoro di studio dell'esecuzione in duo.

In tutte le culture abbiamo _pipe and taboor_:
- pipe (flauto) -> melodia -> sequenza ritmica del parlato
- taboor (tamburo) -> tiene la pulsazione -> beat

Tutta la classe deve studiare la cosa, avendo un tempo per poterlo fare.

La memoria del testo sostiene la struttura ritmica, e dirsi il testo a memoria è la rappresentazione mentale guida, per poter eseguire con sicurezza la melodia.

Il testo detto precedentemente diventa nel duo un binario per poter seguire tutta la sequenza ritmica (suonare a bocca con le Launeddas in Sardegna).

##### I bambini cantano tutti insieme: fase 3

Avremo quindi una cover caricata di tutto il lavoro che c'è stato. Vi è quindi un'omogeneità timbrica di voci femminili, oltre all'intonazione della melodia.

Il brano diviene un brano d'insieme. Le ragazze hanno il ruolo melodico, e i ragazzi hanno il ruolo ritmico ma diverso da quello della melodia, con un'invenzione con il contributo culturale.

Abbiamo quindi una polifonia culturale.

L'ostinato è un pezzo della sequenza ritmica.

Ricaviamo dall'intera sequenza armonica delle sezioni melodiche.

>Come insieme le ragazze si trovano tutte insieme rispetto a prima, mentre i ragazzi inseriscono l'elemento ritmico.

(dal 6 al 10 maggio festival INDIRE)

Mario Lodi, c'è speranza se essa cade al Vo.

### Altri tipi di BES

#### Video per partecipare ad un concorso sui laboratori dell'inclusione: [Diverso non stona](https://www.youtube.com/watch?v=BbLw1dOz2rc)

L'insegnante ha trascritto un brano per pianoforte per il flauto.

Il progetto è stato guidato dall'insegnante di sostegno musicista.

L'insegnante di sostegno musicista si presta al gioco dell'allegretto di Diabelli per flauti accompagnandoli al pianoforte.

Organizzazione delle risorse disponibili in base al pezzo.

Vi sono dei limiti per come comincia e come finisce il brano.

Si ha un lavoro di analisi e comprensione per arrangiare e svecchiare Diabelli.

Una struttura di concertazione come quella qui riportata è il frame di un vero musicista.

>Il sapere quando non suonare, educa il ragazzino.

Con la musica, serve un tramite, un maestro concertatore come Monteverdi, per far risaltare i bravissimi e i meno bravi.
___

__Realizzare individualmente sull'inclusione ed il tema inclusione__ avendo come riferimento:
- articolo
- i due progetti video con due modelli di BES
  - svantaggio culturale
  - svantaggio psico-fisico

___

Affiacciarsi alla fiera INDIRE per poter mettere in vetrina gli insegnanti bravi.
