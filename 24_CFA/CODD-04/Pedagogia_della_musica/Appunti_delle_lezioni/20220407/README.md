# Appunti della lezione di giovedì 7 aprile 2022

## Preparazione dei materiali per l'esame

Rivedere dei modi esaminati almeno due temi (tra melodia, ritmo, timbro, forma).

![1](1.png)

Una progettazione riesce ad individuare chiaramente con verbi alla terza persona, che devono essere preparate prima della lezione.

Bisogna individuare azioni diverse, ed è indispensabile che la modellizzazione sia sempre molto chiara.

>La stessa tabella vuota è utile per individuare nel progetto che vedremo oggi i vari elementi.

>Teaching as artistic design.

Per formare la capacità dei designer bisogna copiare da altri designer.

La modellizzazione di un progetto è l'occasione di svolgere un progetto.

>L'arte del design è un disegno preciso che coglie i particolari.

### Chi è musicista?

Uno che da forma a un tempo.

### Chi è un insegnante?

Uno che da forma a un tempo, un'ora di insegnamento può avere una forma, che la si può avere se si ha uno spazio.

Il monile peruviano ha scatenato una tattica di __problem solving__.

Libro di Dewey: _How do we think?_

Far ascoltare una musica strana e poi realizzare un __brainstorming__. Il brainstorming non si discute, ma successivamente si iniziano una serie di discorsi che assumono delle partenze diverse.

Noi artisti non abbiamo risposte convergenti, di nessuna musica.

>Abbiamo quindi una tattica tipica (con problem solving o brainstorming) per mettere in moto l'attenzione degli studenti, per la fase 1; per coinvolgere gli studenti.

## Per capire e studiare un progetto

Capire cosa il professore faccia a casa, e cosa facciano gli allievi, ovvero righe analitiche e spazi che riguardano i traguardi di competenza.

Chiedendosi quindi a cosa serva dal punto di vista delle competenze trasversali.

Vedremo quindi un laboratorio di musica d'insieme.

### Progetto sul secondo strumento

Il secondo strumento dovrebbe sviluppare le competenze che non si sviluppano con il primo strumento al Liceo Musicale, per essere costretto a sviluppare aspetti diversi della mente musicale.

L'insegnante deve però inventarsi dei progetti.

Con i ragazzi giovani vi è il problema della motivazione.

>Logica progettuale della programmazione per progetti che dura 2 mesi al massimo e non più.

Il saggio a fine anno non deve essere l'unico progetto che abbiamo.

Bisogna avere progetti bimestrali che puntano a qualcosa che deve succedere.

___
dalle 9:30


## Progetto live con i ragazzi

Prof. Valerio Di Paolo

7 ragazzi, tutti ragazzi di secondo strumento tranne uno.

Il professore ha studiato con Fabio Fasano, che aveva una passione per la musica popolare.

### Presentazione del progetto

Si avevano gruppi in stile popolare fra ragazzi come ad esempio il gruppo di Mimmo Ascione.

Il professore realizzò in base allo spostamento tra tonica e dominante riportata sulla tarantella del Gargano.

Il professore ha proposto a ragazzi del terzo anno di Liceo un percorso per il gruppo di musica di insieme.

Vi sono degli strumenti particolari per i gruppi di musica d'insieme che hanno dovuto trovare un posto per l'improvvisazione.

I ragazzi:
- Costanza (piano primo strumento - flauto secondo strumento)
- Rachele (mandolino primo strumento - )
- Filippo (fisarmonica - contrabbasso)
- Emanuele (trombone - percussioni)
- Agnese ()
- Valentina (mandolino - corno)
- Helena (canto - chitarra)

Vi è interscambio fra gli strumenti e le competenze musicali nel liceo musicali si va oltre.

#### Elementi musicali del progetto inseriti dal professore
- elemento improvvisativo
- ascolto reciproco
- responsabilizzazione dei ragazzi

#### Elementi di ispirazione del progetto

Disco di Bennato: Briganti se more, con brani scritti da Bennato.

### Concerto di Natale realizzato dai ragazzi

#### Briganti se more

Con parte scritta

>Il brigantaggio è uno dei periodi della storia d'Italia più importanti e meno documentati dal punto di vista dei briganti.

La canzone sui briganti è una canzone molto importante e particolare dal punto di vista storico e questa canzone non tratta solo del fare musica, ma riporta anche ad un qualcosa di storico.

#### Canzone per Juzzella

Con parte scritta

#### Vulessa diventare nu brigante

>Brano che è piaciuto di più ai ragazzi.

### Domande riguardo il brano ascoltato

>Cosa è la musica che suonate?

>Vi è un'interazione tra i ragazzi e altri docenti?

No, vi è stato un legame tra i ragazzi e il professore.

I ragazzi creano un gruppo, una realtà della band.

Vi sono molti scambi di ruolo tra i ragazzi.

___
dalle 10

La caratteristica della viaestesana ha particolari.

#### Viestesane

#### Riturnella

Brano calabrese

>Rallentandi finali realizzati dai ragazzi.

#### Carpinese

Brano del Gargano

>Rallentandi finali realizzati dai ragazzi.

Non vi era una partitura, ma molti degli elementi erano improvvisati.

Da ascoltare l'Arpeggiata di Cristina Blouart.


### Tabella del progetto

#### Fase 1 (Fase di motivazione – coinvolgimento)

- opportunità per provare musiche nuove (_ragazzi_)
- ascolto e visione dei video (_docente_)




#### Fase 2 (Fase di costruzione – composizione – comprensione)

- proposizione di musica realizzata a blocchi (_docente_)
- ascolto (_ragazzi_)
- riproduzione (cambiando qualcosa) (_ragazzi_)
- richiesta di realizzazione di improvvisazione (dissonanza è vicina al genere) (_docente_)
- costruzione dell'improvvisazione (_ragazzi_)
- libertà dell'improvvisazione ascoltando gli altri ragazzi (_ragazzi_)
  - discorso della musica ciclica -> migliorando sempre maggiormente

  Utilizzano ciascuno il proprio strumento o secondo strumento e sperimentano strumenti nuovi in relazione a ciò che ascoltano.



#### Fase 3 (Fase di comunicazione –  performance)

Fase di comunicazione:
- presentazione del progetto in classe con il docente ed i ragazzi a dei ragazzi come noi

Performance:
- realizzazione di video con i brani delle varie regioni.
- realizzazione di saggi

(ultima registrazione, realizzata in fase di studio)

#### Indicatori di qualità di questo progetto

- il lavorare insieme di un ensemble musicale
- il calarsi in regioni diverse d'Italia e la scoperta di realtà musicali a noi vicine (difficoltà della lingua)
- responsabilizzazione degli allievi in un ensemble musicale (cosa guida nelle entrate e nelle uscite)
- memorizzazione di un brano
- coinvolgimento dei ragazzi

#### Competenze trasversali sviluppate
- memorizzazione di una melodia
- comprensione di un dialetto di un altro luogo d'Italia
- confronto con altro docente -> doppia attenzione (etnomusicologi e musica antica(ricerca sulle musiche del mediterraneo) [Athanasius Kircher -Antidotum Tarantulae](https://www.youtube.com/watch?v=9D7BM0nz1Vw&ab_channel=argesarge2))
- competenze di base della mattina quanto sono utili rispetto a quello che si è fatto?

#### Competenze musicali sviluppate

___

Villa San Giovanni in Tuscia concerto.

INDIRE -> presentazione l'11 maggio

___

### Insegnante come concertatore

Trovarsi a concertare ed aver creato un'identità collettiva, ovvero creare una comunità di pratica musicale.

Una scuola può far realizzare delle band, ovvero realizzare delle identità collettive.

### Il bello di suonare insieme

Lasciare la libertà e creare una comunità tra i ragazzi.

_________________

## Come accumulare progetti da poter utilizzare?

### Gruppi 11:30

Realizzazione della tabella

___


## Riepilogo sul progetto

[Bennato - Brigante se more](https://www.youtube.com/watch?v=0Vs03-k2sWA)

___

Giovedì prossimo non c'è lezione, ci vedremo giovedí 21 con la lezione.

>La verifica sarà realizzata online, con domande che costringano a rivedere i contenuti, cercando di estrarre quello che servirà in futuro.

Dalla lista di domande, estrarremo due domande.
