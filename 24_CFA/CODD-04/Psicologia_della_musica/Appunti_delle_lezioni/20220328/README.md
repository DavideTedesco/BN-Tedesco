# Appunti della lezione di lunedì 28 marzo 2022

Sul drive c'è una sintesi delle ultime due lezioni.

La psicologia della musica è una disciplina molto vasta.

## Argomenti affrontati fino ad ora
- cos'è la musicalità?
- primo fascio di studi, con una prospettiva cognitivista:
  - la musicalità come uno dei modi di conoscere ed esprimere il mondo (e capire il mondo)
    - teoria di Gardner: teoria delle intelligenze multiple(scegliere tra i tantissimi stimoli), varie intelligenze a seconda del tipo di input, l'intelligenza musicale come una risorsa per capire ed esprimere il mondo attraverso suono ed attraverso musiche
    - Piaget
    - Vigotskij
    - Bruner
- disegni dei bambini che rappresentano un qualcosa realizzato con battiti di mani (stesso stimolo ma rappresentazioni diverse): ![1](1.png)
  - si cerca di riconoscere dei tipi di ascolto, ovvero dei tipi di intelligenza, scegliendo alcune risposte ovvero aspetti chiave, dicendo come i bambini capiscono
  - cercare di capire cosa i bambini abbiano capito (da capire, dal latino), ci si focalizza sempre su qualcosa di preciso, cercando sempre una figura, o un raggruppamento, lasciando perdere altri tratti, l'intelligenza ha delle trasformazioni, cerchiamo quindi di capire le diverse intelligenze
- che tipo di intelligenza dimostrano i disegni? ![2](2.png)

>Il corso come vetrina delle varie discipline approfondite.
___

## Vetrina sulle intelligenze

Quanto Gardner pensava all'intelligenza non si immaginava delle scoperte delle neuroscienze, ad esempio il privilegio di caratteristiche cromatiche.

L'attenzione fine alla qualità del suono, plasma un dominio cognitivo ovvero un modo di pensare che plasma il cervello.

## Welch: il potere della musica nello sviluppo del bambino

Essa è una sintesi sui principali studi.

>Nell'ultima mezz'ora di lezione sceglieremo una delle immagini che ci ha colpito, e cercheremo di commentarla con il gruppo, metteremo in negoziazione, ovvero come strumento di studio con un lavoro a gruppo, chiedendo di prendere una delle immagini della presentazione.

### Dov'è la musica nel nostro corpo?

![3](3.png)

Si pensò che si dovesse capire, dove sia la musica nel cervello, individuando le aree di Wernicke e Brocca.

Sappiamo che i musicisti sono delle cavie particolari per i neuroscienziati, e sappiamo che per un calciatore le sinapsi sono molto addestrate nell'emisfero destro. Per un musicista invece abbiamo più zone che vengono toccate dagli elementi musicali.

Gli studi di Wernicke e Brocca iniziarono con esperimenti con parti di cervello addormentate.

Sappiamo che il cervello funziona incrociato e quello che ascoltiamo con l'orecchio destro viene processato dall'orecchio sinistro e viceversa.

Per lungo tempo le prime rilevazioni su come funzioni la musica nel cervello avennerro con le tecniche appena descritte.

![4](4.png)

I pensieri sollecitano delle debolissime cariche elettriche, dove passano poi i positroni.

Nell'ultima immagine vediamo una risonanza funzionale, e vediamo che alcune zone sono a riposo mentre altre sono al lavoro.

Nel cervello le zone che sparano, sono molto distribuite e la parte della corteccia celebrale più coinvolta nella musica sono le zone vicino alle orecchie.

Si è studiato che il musicale è in varie zone del cervello.

### Creatività e modularità neurologica

![5](5.png)

Si scopre quindi che nel nostro cervello vi è la __modularità neurologica__. E vi è una completa diversità tra i vari musicisti e la modularità musicale rispecchia le diversità, ed è per questo che nel corso di pedagogia si parla delle diverse intelligenze ed ognuna delle intelligenze attiva nel cervello attiva certi moduli piuttosto che altri.

Vi sono stili diversi che attivano modalità cerebrali diverse.

### Ricadute nella vita reale

Coltivare delle particolarità sul piano dell'intelligenza musicale produce altro per altre attività?

![6](6.png)

Vi è un bodymind, ovvero un'integrazione tra corpo e mente?

Si, bisogna far imparare con la mente e con il corpo, e la musica comprende sia il corpo che la mente, si ha bisogno dell'integrazione tra corpo e mente.

Il corpo e la mente non devono essere divisi.

### Sistema endocrino e musica

Non vi è soltanto il corpo e la mente, ma nel sistema endocrino vi sono anche degli elementi come le ghiandole del piacere e quindi si ha una relazione tra sistema cognitivo e endocrino:
![7](7.png)

La musica ha un influenza sul sistema immunitario.
![8](8.png)

>nella diapositiva vediamo i nomi dei vari studiosi che hanno realizzato gli studi raccolti nell diapositive: psi mus 3, welch2006

### Modello modulare di elaborazione musicale

Vediamo qui la rappresentazione delle aree che si attivano quando si canta una canzone appena ascoltata:
![9](9.png)

Con l'intelligenza melodica si cerca di realizzare un'analisi, ovvero uno zoom su alcune caratteristiche specifiche e per poterle riconoscere vi sono alcune tecniche da dover utilizzare.

Le organizzazioni hanno diverse aree:
- l'organizzazione tonale ha alcuni aspetti ed aree che si attivano
- l'organizzazione temporale ha alcuni aspetti ed aree che si attivano in modo diverso dall'organizzazione tonale

Far muovere il bambino sul movimento ad esempio permette di realizzare un'analisi motoria, prima ancora che ritmica; vi è prima una dimensione della motricità globale e poi delle figure ritmiche.

(National Geographic "Il cervello di una rockstar" studio di Sting realizzato da Levitin)

#### Organizzazione temporale

Per l'organizzazione temporale bisogna realizzare prima un'analisi motoria e poi un'analisi rimtica.

#### Organizzazione tonale
Per l'analisi tonale abbiamo invece:
- analisi del contorno(come l'analisi etnomusicologica di Sachs) per questo motivo alcuni stonano, non relizzando le altre analisi successive ma solo le analisi del contorno, che è realizzata nell'emisfero destro
- analisi degli intervalli per questa analisi serve mettere in moto intuitivamente anche un'analisi degli intervalli che è realizzata nella parte sinistra del cervello e per riuscire a restituire in modo corretto bisogna nuovamente cantare
- codificazione tonale, realizzazione che vi è una tonica, ovvero una nota casa.

>Se si trova un bambino intonato si cerca di chiedere ad egli di cantare per gli altri bambini.

Nel cervello di qualcuno non musicista viene già utilizzato molto il cervello in aree diverse, nei musicisti ancor più.

### Nella testa di un cantante

Canto reale vs canto immaginato:
![10](10.png)

Vediamo che vi sono molte zone colorate.

Vediamo anche che al centro del cervello (resezione del corpo calloso) vi è un'area molto soggetta a utilizzo, vediamo che abbiamo un ispessimento del corpo calloso nei musicisti, e si dice che i musicisti hanno una connessione tra emisfero destro e sinistro con un controllo della sequenza che si fa con l'emisfero sinistro.

>Un musicista vero, a differenza di un calciatore, ha bisogno di continui rimandi tra l'emisfero sinistro e l'emisfero destro.

Il rimando continuo da una sezione del cervello all'altro è gestita dal corpo calloso.

(Guardare il video ["gli occhi di Ronaldo"](https://www.youtube.com/watch?v=YsCokWHGLXc&ab_channel=Ergoneers) per vedere con degli elettrodi cosa succeda nel cervello. )

(Il pianista di Polanski film da vedere!)

Pensare ad un brano anche quando si è in giro, fa attivare le aree simili del cervello, osserviamo che la parte del cervello frontale attiva anche aree specifiche del cervello relative al motorio e quindi ai movimenti.

#### Approfondimenti sull'ascolto e lo studio

Il tronco encefalico fa da centrale di smistamento dei segnali cerebrali, il tronco encefalico raccoglie e seleziona i segnali che arrivano dalla coclea.

Ognuno di noi ha alcuni suoni specifici, e vi è una scansione e uno smistamento realizzato dal tronco encefalico, esso ha imparato a riconoscere i suoni.

Nina Krauz dice che vi sono:
 - cervelli rumorosi -> alcuni non in grado di codificare, con un sacco di rumore di fondo, rispetto a ciò che arriva
 - cervelli tranquilli

Il brainstem impara a riconoscere il flusso che vi è.

Vari stili di improvvisazione hanno dei riscontri neuronali.

Un'attività musicale ricca e varia è uno stimolo per molte parti del cervello.

#### Cervello dei mancini?

Un mancino dovrà realizzare un percorso con lunghezza maggiore all'interno del cervello.

#### Studiare con il canto immaginato

Se si studia con il canto immaginato, più si studia un brano con varie zone del cervello, più allora si saprà come andare avanti.

Un canto immaginato molto allenato, mi permette di stare nella musica anche se non riesco a percepire bene il mio corpo.

Vi sono studi della memoria per sviluppare vari aspetti per lo studio e poter continuare a suonare anche se uno degli aspetti di memorizzazione viene a cadere.

### L'attività musicale e lo sviluppo del cervello

![11](11.png)

Osserviamo il cervello di una parrucchiera prima e dopo lo studio della musica, dopo un periodo di un anno.

Vediamo uno spostamento di attività cerebrale con una quantità i moduli che cambiano.

Abbiamo uno spostamento sui neuroni a destra che prima non sparavano proprio, spostandosi verso l'area sul retro del cervello, si pensa dopo un anno al suono, la melodia, il timbro.

Dopo un anno si sono automatizzati dei movimenti per la fonazione e ci si concentra sul suono, ovvero sugli aspetti più tipici della musicalità.

>Concentrandoci su una cosa si chiama energia, richiedendo sangue, il nostro cervello è economico e chiama energia solo nei punti in cui gli serve.

Quando si suona si mette in moto una quantità di azioni e studiare è automatizzare alcune zone.

Secondo la teoria della Gestalt, percepire è sempre selezionare, teoria di Miller sugli item a cui si pensa.

La motivazione nel libro di Damasio "L'errore di Cartesio".

>Il cambiamento delle aree sottoposte nota un qualcosa di diverso nello sviluppo.

### Il modello a Matrioshka

![12](12.png)

Gli umani hanno sensibilità e una struttura ontogenetica, la caratteristica degli umani è che lo sviluppo e la sua esposizione anche già a -3 mesi dalla nascita si aveva l'orecchio già formato.

- nucleo ontogenetico con la struttura biologica
- inculturazione, fatto di essere cresciuti in un certo posto (studio di Johanella Tafuri, vedendo cosa succeda se in famiglie normali si aiuti i genitori a cantare); l'alfabetizzazione melodica precede quella verbale

  >Chi ha genitori musicisti è più sviluppato ad avere un orecchio assoluto oppure deve essere utile avere una lingua-tono come il cinese mandarino e il vietnamita.

  Lo sviluppo umano aiuta a trasferire, servirà un contesto per poter mantenere la capacità e non perderla.
- sviluppo delle abilità musicali, trovandosi esposti a situazioni di insegnamento e apprendimento diversi -> come aiutare a ripensare ad alcune melodie e ritmi? con una realizzazione dei modelli degli apprendimenti
  >Il progetto di Welch venne sviluppato insieme ad un progetto denominato [Sing Up](https://www.singup.org/) per poter aiutare le maestre a far cantare i ragazzi.
  ![13](13.png)

___

## In gruppo

Immagine e dialogo

___

## Discussione post-discorso dei gruppi

[I benefici sul cervello di chi suona uno strumento - Anita Collins](https://www.youtube.com/watch?v=R0JKCYZ8hng)

Rita Aiello -> come funziona la memoria

Libro inglese sulla performance musicale:
![14](14.png)

Psicologo della musica e concertista che studiano ogni argomento.

From sound to sign, quando arriva la notazione?

Arnold Jacobs, libro sull'immaginazione e l'approccio di studio con una immaginazione.

>Le musiche che si conoscono meglio sono quelle su cui si realizzano più giochi.
