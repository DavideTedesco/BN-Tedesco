# Appunti della lezione di lunedì 23 maggio 2022

## Realizzazione di scalette
Dedicheremo la lezione per osservare gli 8 quesiti della lezione e osservare:
- gruppo 1, riportare
  - autori e testi di di riferimento (riportare l'autore e l'anno di riferimento del testo)
  - tracciare una scaletta tipo dei contenuti da esprimere in un quesito

  >Non usare le parole "secondo me" parlando in un contesto istituzionale di psicologia della musica.

  È quindi importante citare gli autori ed i testi

[Appunti per i temi di studio di psicologia della musica](https://docs.google.com/document/d/1FwIysv4-Xo5HS1_2q-xmBP__UGJL7rj9-UiwlTCA9XM/edit#)

## Quesito 1, Musicalità: ipotesi esplicative nella ricerca psicologica

![1](1.png)

- rilettura di Piaget, con una parafrasi di Delalande che dagli anni '70

(Il gioco simbolico è il "far finta che", pensando di dover realizzare qualcosa, ciò è racchiuso nell'imitazione. Il gioco con regole invece è la concentrazione sulla regola e quello che ci si è inventato in un dato momento.)

- musicalità e fare musica vuol dire ascoltare il mondo in un certo modo, ovvero un modo di intelligere, capire e conoscere il mondo che sviluppa aree cerebarli specifiche

- Piaget e Delalande sono all'interno del filone cognitivista

Vi sono 3 modi di intelligere il mondo:
- gioco senso-motorio (esplorare con un'intelligenza senso motoria) il tocco è infatti una questione di intelligenza senso-motoria (essa è il primo tipo di intelligenza sviluppata) (già a 30 giorni con la suzione si identificano sensorialmente degli elementi)
- gioco simbolico (scuola dell'infanzia e prima elementare)
- gioco con regole

Le intelligenze sono modalità di comprensione che si sviluppano via via.

Nel soggetto umano all'intelligenza senso-motoria si sovrappone il carattere simbolico (facendo finta di essere)

Spesso suonando certe cose ci identifichiamo e facciamo finta di essere un grande performer che ci ha colpito, il gioco simbolico passa nel far finta di essere.

Il gioco con regole è un altro modo di intelligere.

Un musicista adulto ha tutti e tre i modi di intelligere la musica.

Abbiamo tre livelli di intelligenza che sono dei modi di essere musicali: musicalità come modo di intelligere.

>Si parla citando della parafrasi di Delalande su Piaget, e Delalande focalizza il discorso sull'intelligenza musicale in tre fasi.
Nell'__intelligenza senso-motoria__ il riferimento ad un'esperienza fatta deve essere presente, si ha quindi una rappresentazione mentale se riesce a collegare, evocando alla mente rappresentazioni mentali con un'esperienza ad essa collegata. __Intelligenza simbolica__ legato al musicale significa ad esempio immedesimarsi alla maniera di Bach o Chopin, il gioco simbolico è un far finta di essere un autore preciso. Mentre nell'__intelligenza del gioco con regole__ si indica dove spari il cervello, ovvero indicando un'area in cui si concentra lo sforzo cerebrale, come si sta intellegendo, dove sta sparando il cervello.

Bisogna dimostrare che la musicalità riguarda lo sviluppo della mente e della relazione, con Threvarten, Malloch, Stern, Anzieur. Per comunicare infatti non si può non essere musicali!

I tratti sovra-segmentali dicono di più di quelli segmentali. I grandi retori sanno che l'arte della fonazione hanno a che fare di strategie musicali, organizzando la forma e la dinamica del suono.

>Vi è un filone di studi che osserva la musicalità come modalità ottimale di fonazione. La comunicazione presuppone una musicalità.

Vi sono alcuni meccanismi nervosi strettamente legati alla relazione. Daniela Lucangeli osserva le aree nervose in funzione della relazione e l'udito ed il tatto sono gli organi pro-sociali per eccellenza.

>L'udito fine lo si ha già quando si è all'interno della pancia della madre per sviluppare elementi relazionali.

Lieberman osserva che vi sono anche dei neuroni specchio uditivi, mettendo in moto una motricità, il dono delle lingue è quindi un dono dell'udito.

Rizzolati, Gallese e Welch utilizzando delle risonanze magnetiche funzionali per individuare aree cerebrali in cui il cervello spara quando si realizza musica. La musicalità si lega con il piacere, con il sistema endocrino, la gioia.

Nina Krauz, utilizza solo elettroencefalogrammi per poter avere risposte riguardo la musicalità, cercando la differenza che vi è tra un cervello musicale ed uno non musicale.

Damasio, Welch, Levitin...

Gallese, Rizzolati, Sinigallia, facendo delle risonanze funzionali si sono resi conto che alcune esperienze motorie si realizzavano anche solamente guardando l'esperienza.

>Ci si rende conto della differenza tra pianisti e violinisti a livello cerebrale.

Gardner (intelligenza propriocettiva) ha alcune dominanze di intelligenze.

## Quesito 7, Body – mind: connessioni tra mente e corpo nell'esperienza musicale

Idea delle cognizioni ovvero di coscienze che sono incorporate.

[Appunti per il quesito 7](https://docs.google.com/document/d/1ZZ2G5j5BQAxMPtGzj1g8Vc9HY4hiy3tdQoYNcXRammE/edit)

## Quesito 2,Intelligenza / Intelligenza musicale

Nascita delle neuroscienze grazie agli strumenti diagnostici elettronici nati negli ultimi 20 anni.

Le prime osservazioni su emisfero sinistro ed emisfero destro vennero fatte negli anni '70 del '900 e le neuroscienze si sono sviluppate dagli anni '90 del Novecento in poi.

Le memorie sono alla base del pensiero e il pensare è successivo alle memorie.

Il pensiero adolescente è già un pensiero adulto che elabora l'esperienza.

Abbiamo delle elaborazioni del pensiero.

Vigotskyi esprime che:
- il pensiero bambino si basa sul ricordare
- il pensiero adulto si basa sull'elaborazione dell'esperienza

>Bisogna elaborare momenti per rievocare alla mente il percorso realizzato a lezione, con tanti modi e tempi per costringere il bambino a ripensare all'esperienza, la memoria come primo grado dell'esperienza.

Non basta l'esperienza ma servono gli strumenti perchè l'alunno abbia strumenti per rievocare alla mente elementi svolti nella lezione, l'insegnante deve preoccuparsi di:
- fornire strumenti per rievocare alla mente
- fornire strumenti per rielaborare, ricostruendo mattone per mattone

>La notazione musicale è come realizzare dei nodi al fazzoletto per ricordare sequenze ritmiche e sequenze melodiche anche per ricordarsi l'armonizzazione.

La notazione musicale, come strumento o come dispositivo per la rappresentazione è molto importante.

Realizzazione di un suono da un disegno, realizzazione di un disegno da un suono.

Pensando anche alla realizzazione di un movimento legato ad esempio all'attacco della quinta, trovando i movimenti da realizzare. Prove di spostamento e di passi sulle mattonelle per collegere e memorizzare come ampiezza delle mie gambe.

Con una melodia con intervalli piccoli è molto utile camminare sul percorso intervallare.

Vigotskyi parla dei linguaggi come dispositivi.

Piú linguaggi si hanno più il pensiero si elabora.

## Quesito 3, Musica e neuroscienze: aree cerebrali coinvolte nella performance musicale

Ripercorrere i discorsi affrontati nella lezione di psicologia musicale 2.

Citando gli autori trattati nella lezione, ed in più gli autori della scuola di Montreal, Levitin, Peretz.

La scuola di Montreal ha prodotto i video "How playing an instrument affects your brain" e anche il discorso sui neuroni specchi.

Azione dei neuroni specchio nell'apprendimento e nell'insegnamento strumentale.

Schlaug si è preoccupato delle ipertrofie nei cervelli dei musicisti, che riconosce come il particolare esercizio di alcune aree neuronali, realizzino delle ipertrofie di alcune aree del cervello.

Le aree di Wernicke e Broca...

L'adattamento del cervello in relazione ai comportamenti musicali
- Peretz
- Elbert
- Ravel e il mistero del Bolero

Operatività del cervello e le zone neuronali messe in moto dal cervello.

I musicisti usano tutto il cervello e la distribuzione può diventare compensativa, e la maggior parte di studi neuronali sono pagati dai famigliari di pazienti malati di Parkinson e Alzheimer.

A cosa serve il corpo calloso e perchè diviene ipertrofico nei musicisti?

>Catturare dei concetti dal video "How playing an instrument affects your brain"?

## Quesito 6, Dialogo sonoro, rispecchiamento, identità

Infanzia e società

Aggiungere i quadratini alla mappa identità musicale e elementi euristici:
- rispecchiamento con personaggi chiave -> identificazione
- rispecchiamento in una comunità di pratica

![2](2.png)

Dalle slide: _psi mus 8, identità musicale - percorsi euristici.pdf_

Trattare di tutti i dialoghi sonori che si sono avuti in vita e non solo in quelli con i personaggi chiave, bisogna considerare le esperienze del dialogo sonoro musicale come tutta la formazione strumentale musicale.
