# Appunti della lezione di lunedì 11 aprile 2022

Partendo da cosa sia la musicalità attraversando l'esperienza di Gino Stefani.

Incontro con [Stefania Guerralisi](http://www.aimat-gdl.org/la-fondatrice-stefania-guerra-lisi/)

## Prima parte della lezione: la musicalità e l'uomo

>Il conservatorio come salvezza.

Le unità corporee dei linguaggi per mezzo elementi e spostamenti corporei.

![1](1.jpeg)

### Realizzazione per mezzo di un suono della nostra costituzione musicale

Parliamo di psiconeuroscienze, e battendo le mani realizziamo il catartico.

Il valore unico, grande che abbiamo tutti che è alla base della pedagogia è:
>vivere la vita ed esprimendosi.

Placet: proviene da piacere, come dote naturale con cui veniamo al mondo.

### Therepos: pedagogia come cura della crescita dei potenziali umani

La creazione delle metafore come linguaggio, capendo che tutto quello che noi siamo, vibra continuamente, perchè viviamo.

>Vita -> in movimento.

Trauma da contatto e musicoterapia.

La musica, dove non vi è fiducia dei contatti, può realizzare un'immersione.

>La sordità diviene linguaggio riduttivo dei segni, ovvero i segni sono suono.

I bambini musicano i gesti, propri e degli oggetti intorno a loro.

Una comunità deve riconoscere un diritto di essere disabili.

Aiutare gli individui con disabilità con elementi socialmente utili, ovvero eliminare l'ammutolimento, cercando di avere l'aiuto a tirare fuori gli elementi musicali.

Per far si che si nasca l'orgasmo deve avvenire per mezzo dell'atto sessuale, cercando un bisogno di complementarietà perchè nasca un organismo.

### Homo ludens

L'uomo gioca con il sesso, non avendo periodi di accoppiamento come alcuni animali.

Vi sono tante implicazioni che fanno perdere il potenziale umano.

Bisogna avere la possibilità per avere un buon contenimento, della famiglia:
>dal grembo materno, al grembo sociale.


Abbiamo imparato le diversità nel placet, ovvero nella placenta, vivendo sulla propria pelle, ovvero vivendo sulla prima membrana dell'ovulo fecondato.

>La vida es fiesta (Neruda)

### Prima il mercato poi la vita

Oggi si decide con facilità che un individuo è meno utile alla società, come ad esempio in Islanda o con gli istituti.

### Compassione

Bisogna imparare ad amare, provenendo dal contenimento.

Le azioni del sangue (emozioni, emos-passioni).

La musica tocca emozionalmente e compassionalmente.

Un uomo vibra, in base anche alle parole, quindi la prosodia conta.

### Approccio dei bambini al parlato

I bambini giocano con l'apparato fonatorio e i bambini prima di parlare realizzano un qualcosa di musicale con i suoni.

I bambini imparano a compiacersi rilasciando suono.

La cultura odierna invece si compiace lasciando il segno.

### Apertura delle mani e desiderio

La mano del neonato si mette sopra la curva del seno, che sente il palpitare del cuore, realizzando:
- contenimento
- rispondenza

La natura ha voluto che l'evoluzione portarsi al distaccamento dalla madre molto presto. Per lo sviluppo dei potenziali creativi dell'essere umano per vivere.

I principi della vita per un buon contenimento:
cure sociali.

La cura è una dea che attraversa una palude...

### Pinocchio segno di un essere umano in fieri

Figlio burattino che sosterrà per la vecchiaia il padre geppetto.


Geppetto è maschio e la fata turchina è sola una sorella...

Nino Rota, realizza la musica per un soggetto sgangherato.

### Diritto ad essere come si è

Quanto siamo lontani da questo diritto?

Valorizzazione o valutazione??

### Prove di ingresso alla vita

L'uomo è la specie che assiste per il momento del parto.

L'uomo elabora il lutto.

>Musica come vita

### Musicavernoteca

Discoteca come utero circolare, che convive la luminosità della madre.

Conoscere bene quali sono gli imprinting di un essere umano.

### Uomo come essere ricco di cure

La ricchezza del genere umano consiste nella cura.

Per uscire dalla realtà realizziamo un percorso che realizzi una stimolazione plurisensoriale.

### Il corpo e il toccare la psiche

(Lucio Lombardo Radice - Globalità dei linguaggi)

(Stefania Guerralisi - La scuola con il corpo)

La psico-motricità viene realizzata non per mezzo degli oggetti ma del corpo stesso.

Si cerca di scoprire con il corpo ad esempio toccando una superficie piuttosto che un'altra.

### Diritto ad essere come si è

Ascoltando la stessa musica, si hanno realizzazione di immagini soggettive e sinestesiche.

Si realizzano associazioni sulla memoria genetica umana, che ricapitola tutto ciò.

### Grembo materno e risuonare della cassa armonica

Partendo con il corpo si realizza con la musicoterapia un percorso di cura dell'uomo.

Uomo come potenziale enorme di associazioni imago-attive.

L'uomo è un essere che produce immagini...

Un uomo è l'attimo fuggente che deve essere risvegliato dal coma, associando delle immagini potenziali dei vissuti storici di una persona.

Una persona pur di vivere si rifugia in un limbo, coma, fuori dal tempo e dallo spazio quotidiano in cui si vedono luci e si sentono suoni e non si parla mai di sofferenza.

72 risvegli dal coma...

### Se non si può andare avanti si può andare indietro

Se si è nel limbo, in 9 mesi si ricapitolano molti anni di ontofilogenesi...

Unico valore: è la vita e non basta neanche la consapevolezza della morte.

Ormai vi è più paura di vivere che di morire.

pre-occupazione (ci occupiamo prima), com-piacimento...

### L'uomo è un animale che percepisce di percepire

La disumanizzazione passa per molti fili sottili...

Pacciugare...

I bambini si devono divertire, fare qualcosa che non sia obbligatorio.


### Tages: il bambino dalla barba bianca

Dal grezzo allo spirito, partendo dal corpo per arrivare allo spirito.

Distillare dal grezzo per realizzare l'essenza, con una carica di informazioni.


### Alchimia e crescita

Il terapeuta, il genitore deve attendere un tempo, ditillando progressivamente i potenziali.

Non sappiamo cosa succederà nell'ultimo atto di vita, cerchiamo di capire ma non possiamo.

Il neonato ha memoria come la cellula oltre ad avere capacità di compressione e rarefazione.

La memoria è mnemosina, ovvero per creare la memoria si accoppiano l'emisfero destro e sinistro (Zeus e le muse).

### Musica come arte impalpabile

La musica è la musica che raccoglie tutte le arti...

### Uomo e la non sacralità del corpo

Endorfine e studi per la crescita

...

### Cura di se per curare gli altri

### Visione del video

L'uomo può perdere tutto, ma non il senso estetico della vita ovvero le memorie del corpo.

Tutto ciò che è dolore viene espresso dalla musica...

Amputazione di un arto e arto fantasma, perchè solo il dolore e non il piacere con l'arto fantasma? Si cerca di stimolare e accarezzare l'arto fantasma, valutando il cambiamento di valori.

La musica come estrema qualità della vita.


#### Cronologia del video
_Ascolto di Lux Eterna di Ligeti_

_Stefania Guerralisi_

Come accompagnare bene alla morte (dame di compagnia)

Dare vita e principio di piacere attraverso il piacere percepito in prima persona.

## Seconda parte: riabilitazione del linguaggio

Come l'essere umano è produttore immaginifico.

GDL (Globalità del linguaggio)

Diritti a:
- vivere
- a comunicare

Per la Globalità dei linguaggi l'aborto è un omicidio...

### Tyna Lisi

Carattere dell'insufficienza e l'esigenza dell'essere umano di realizzare un concetto altro.

La musica ha una competenza che va oltre un linguaggio.

Il concetto di imperfezione e inabissarsi del perfetto.

Darwin "L'espressione delle emozioni nell'uomo e negli animali"...

>LA STEREOTIPIA VOCALE NELLA GLOBALITÀ DEI LINGUAGGI

Perchè i compositori si inabissano in una competenza musicale che acquisiamo nell'utero.

Ci inabbissiamo in una musicalità ed al pre-linguaggio di una frammentazione.

La vocalità ed il rigetto della parola, diventa funzionale come scarica energetica che ci consente la vita.

Tutto assolve ad un bisogno primariamente acquisito che era il primo mondo musicale che ci ha fatto acquisire la vita.

#### Che cos'è la voce?

Come sta tutto il corpo

Voce come processo che ci porta oltre il corpo...

#### Emissione della I e passaggio alle altre vocali

Il senso verticale delle vocali...

Passaggio delle vocali...

Capacità sincretica di collegamento dei linguaggi espressivi-artistici.

![2.png](2.png)

___

Goethe statistiche suono e colore...

___

Organi che rimangono latenti che senza sollecitazioni non riesco a realizzare nulla, ma con la sollecitazione vi si riesce.
