\version "2.22.2"

 \header { 
                 
                 title = "Sigfried Idyll"
                 composer = "Richard Wagner"
                
 }


\relative c''{
  \hide StaffGroup.BarLine
  
  \time 4/4
   \key e \major
   \hide TupletNumber
   
   \partial 4 gis4
   gis'2 (gis8) cis, \tuplet 3/2 { dis8 e f }
  g2 (g8) c,8  \tuplet 3/2 {d e f }
  g4 \tuplet 3/2 {e8 d c} f8 b, \tuplet 3/2 {bis8 c d}
  f e \tuplet 3/2 {a, b c} d g, \tuplet 3/2 {a b bis}
  c2 (c8) f, \tuplet 3/2 {g a b}
  c2 (c8) fisis, \tuplet 3/2 {g a b}
  cis2. d16 cis bis cis 
  gis'2. fis4
  a,1 (a2.) g4 f2. s4 
} 

\layout {
    \context {
      \Score
      proportionalNotationDuration = #(ly:make-moment 1/32)
      \override SpacingSpanner.uniform-stretching = ##t
    }
  }
