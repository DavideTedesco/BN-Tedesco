# Appunti della lezione di lunedì 9 gennaio 2023

## Il ritmo secondo Oliver Messiaen

Messiaen si dichiarava ritmista ed ornitologo.

Messiaen fu un personaggio chiave e dedicò molto tempo alla didattica insegnando a compositori del calibro di:
- Berio
- Stockhausen
- Boulez
- Maderna

Egli fu un grande organista ed improvvisatore allivo di Marcel Duprez.

Egli insegnò al Conservatorio di Parigi.

### Piccola biografia di Messiaen

Egli è un personaggio che nasce nei primi del Novecento e muore alla fine del Novecento.

Fece la Seconda Guerra Mondiale, e venne rinchiuso in un campo di concentramento. E venne rilasciato perchè egli era un soldato militare e suonava come musico.

Scrisse il Quartetto per la fine dei tempi, dove inaugura il serialismo integrale, in cui vi è lo studio dei modi a trasposizione imitata (vi era un modo per la scala pentatonica, poi un modo per la scala esafonica).

A Parigi tornò dopo il campo di concentramento e prese la cattedra di composizione.

Egli fu allievo di Dukas, e fu allievo di Guillot che fu un grande didatta.

I parigini sull'orchestrazione...

Il trattato di composizione che si utilizza oggi è il Dubois e il trattato sulla fuga l'hanno scritto i francesi il Gedalge.

I francesi ci tenevano molto alla didattica e chi studiava lì divenne un grande.

Messiaen studiò organo e composizione.

Fu un grande musicista e didatta.

>Messiaen arrivò al serialismo integrale, perchè disse che avrebbe scritto il brano in questa maniera:
- si scrive il brano nell'ambito di 12 suoni
- si decide di utilizzare la serie anche per gli attacchi dei suoni
- si usa la serie per le durate
- si usa la serie per le sonorità

Tale brano che fonda il serialismo integrale si chiama Mode de valeurs et intesites...

>I suoi allievi iniziarono a scrivere con tale tecnica, fino a quando Messiaen disse che: lo infastidì "l'importanza spropositata data al brano _Modi di valore ed intensità_ essa potrebbe essere profetica, storicamente importante ma musicalmente non è niente."

A forza di serializzare tutto, vi è stato il voler scappare e buttar via il serialismo integrale.

Negli anni '50 Messiaen venne chiamato a insegnare a Darmstadt dove si realizzò il centro culturale e la scuola più importante di tutta Europa.

Nel '66 venne nominato insegnante di composizione a Parigi.

>Il centro oggi per la composizione si è spostato a Parigi.

### Cosa ha scritto Messiaen di teorico?

Egli ha scritto due opere teoriche:
- __Tecnica del mio linguaggio musicale__, un po' come pianisticamente Cortot, o come Vincenzo Vitale sempre pianisticamente
- scrisse poi il __Trattato di Ritmica, Colore ed Ornitologia__ che consiste di 7 volumi in 8 tomi, in cui analizza la rimtica su tali sette volumi, Messiaen riteneva che essi sono i migliori compositori musicale, ed egli scrisse molte composizioni riportando i suoni degli uccelli

#### La tecnica del mio linguaggio musicale

Esso è testo del 1947, e si compone di vari capitoli.

Tratteremo i primi sette capitoli.

Il settimo capitolo riporta come scrivere la musica a livello ritmico, ed espone quattro tipi di notazione.

##### Capitolo 7

1. ![1](1.jpg) il primo modo di scrivere la musica è far sparire completamente il tempo (con il Quintaton con canne annotate per quinta), indica che la misura non deve esistere, si fa sparire l'ictus, che impone le sue regole di accentuazione, quindi la musica si deve scrivere senza tempo, l'esempio è preso dalla _Natività dell'enfante_, la ritmica di Messiaen è molto sofisticata, di questo vediamo che vi è un valor ajoute, ovvero un valore aggiunto (il Do aggiunto), si mette un piccolo valore e non si considera più 3/4 -> secondo Messiaen la notazione più bella, e la stanghetta è un fattore dell'occhio
2. ![2](2.png) il secondo tipo di notazione lo osserviamo in quest'altro esempio tratto da _Les Offrandes oublièes_ in cui abbiamo la scrittura della musica ma trascritta secondo la scrittura tradizionale: abbiamo infatti 10/8, 6/8, etc... Abbiamo inoltre i segnetti come il triangolo ideati da Roger Desormiet (direttore d'orchestra francese di musica contemporanea), ed i segni danno all'occhio se abbiamo un modulo di due ed uno di tre. -> sistema della scrittura utilizzata da Stravinskji con tutti i cambi di tempo
3.  la terza tipologia di notazione è nel _Poemes pour mi_ e osserviamo in esso un sistema a metà tra il non scrivere e lo scrivere (prima e seconda scrittura), quindi quando la musica presenta dei ritmi che possono accomunarsi fra loro, organizzando per cellule di quattro, cellule di tre e due ![3](3.jpg) ->ingabbiamo in cellule di 3,4 o 2 per semplificare la direzione
4. ![4](4.png) (dalla Liturgie de cristal) la quarta tipologia di notazione, abbiano in essa un solo tempo che attraverso l'utilizzo di sincopi ed accentuazioni riesce a far uscire il ritmo pensato dal compositore, ed esso serve quando vi sono diversi musicisti a suonare, ovvero un solo tempo ma con accentuazioni, e si riesce ad uscire da un 3/4 rispettando e mettendo le dovute accentuazioni, ingabbiando il ritmo nel tradizionale 3/4 -> un solo tempo come il 3/4 ma utilizzando sincopi ed accenti e facendo uscire il vero ritmo ingabbiato dal tempo


###### Scrittura del ritmo con le varie notazioni viste

![5](5.jpg)

65. Abbiamo il valore aggiunto in questa scrittura, e si scriverebbe tutto con le varie notazioni...
66. terza notazione (il 2 sarebbe 2+2)
67. seconda notazione
68. prima notazione
69. stesso frammento 68, scritto in una misura falsa, ovvero con l'ausilio della quarta notazione

>La prima notazione con una musica d'insieme è molto difficile da gestire.

Quando si dirige la musica contemporanea si inseriscono i segni di Desormiet.

Il triangolo addirittura si inserisce anche con indicazione di tempo binario o ternario su un particolare movimento:

![6](6.png)


##### Capitoli dall'1 al 5

Vi è un assioma tra ritmi che cambiano sempre e ritmi che rimangono simili.

![7](7.jpg)

Messiaen si rifà molto ad un tipo di struttura rimtica che si chiama Ragavardana. E da tale cellula si può costruire molto.

Messiaen prende il Ragavardana, ovvero il ritmo più semplice dell'indù, e vi sono 120 Raga...

###### Valore aggiunto

Il valore aggiunto è partendo dalla cellula base, lo sviluppo dei ritmi attraverso il valore aggiunto. Esso è un piccolo valore che si può aggiungere tramite un nota, con il valore 8 ad esempio si sconvolge con una pausa, oppure come il numero 9 si aggiunge un punto.

![8](8.jpg)

Il valore aggiunto in una composizione, osserviamo che se dovesse capitare un qualcosa del genere, come si studierà?

![9](9.jpg)

Osserviamo che la chiave è contare i valori piccoli, ovvero le semicrome.

Si vede che esiste la diminutio e l'aumentatio.

>Gli aggravamenti e le diminuzioni sono tradizionalmente del doppio o della metà, ma in realtà si può realizzare una diminuzione di una sola croma o un aggravamento di un terzo.

---

Ascoltare La natività del signore, Dieu parmineau di Messiaen, brano scritto nel ritmo ma scritto senza alcun tempo, simile a Hindemith.

---

>Il Maestro Vignanelli disse che del Clavicembalo Ben Temperato disse che non tutti vennero eseguiti, ovvero alcuni dei brani vennero scritti solamente a tavolino.

---

Di Messiaen ci manca un ultima parte...

---

Per il solfeggio va bene anche la mitragliatrice e i movimenti del braccio.

Per la teoria una domanda a piacere e poi domandine sugli altri argomenti.
