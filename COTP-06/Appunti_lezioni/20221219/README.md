# Appunti della lezione di lunedì 19 dicembre 2022

Esame il 6 (dalle 16 alle 20) febbraio 2023 con:
- esercizio di soleggio estratto da 1 all'8 del Rolle
- parte di teoria con un argomento
- in Aula Magna a Sant'Andrea delle Fratte
- esercizio solfeggiato dal repertorio

Oggi è la terzultima lezione e vi sarà una parte teorica ed una parte pratica.

Vedremo anche del materiale per approfondire.

## Articolo 1: notazioni e metri particolari

Come vengono notati i tempi ed i metri oggi da autori contemporanei.

Bartok e Stravinskij ebbe un'esigenza particolare di notare alcuni ritmi. Tale notazione venne anche studiata da Kodaly.

Vedremo come tali sistemi sono notati e come tali sistemi sono progrediti.

Vedremo anche i metri frazionari.

### Notazione di Hindemith

Per fuggire dalla tirannia dell'ictus (primo accento di ogni misura), Hindemith scrisse il suo quartetto:

![1](1.png)

Osserviamo che non vi sono indicazioni di metro, con battute di lunghezza libera e la battuta è un po' una sorta di riferimento affinche si vada insieme nella polifonia.

### Bartok, Kodaly,etc...

Utilizzo di metri che hanno come denominatore l'ottavo o il sedicesimo. Tali metri vennero usati nella prima parte del Novecento.

Queste tipologie di metri hanno dato vita a delle tipologie:
1. metri variabili (più semplici da catalogare), ovvero i metri che variano i continuazione come:
  - 3/4
  - 2/8
  - 5/16

2. metri alternati, essi sono gli stessi metri che si alternano regolarmente
  ![2a](2a.png)
3. metri additivi, ![2b](2b.png) essi si adoperano comme somma di qualcosa, si utilizzano perchè se si scrivessero come metri variabili, avremmo un ictus, ma in tale modo avremo l'ictus come primo elemento solamente, ed avremo l'ictus solamente all'inzio della sequenza, abbiamo a pagina 33 del quartetto di Bartok tale esempio ![3](3.png), a pagina 39 troviamo un'altro esempio, con 10/8 ![4](4.png) **tale tipologia di metro serve per non avere un ictus ad ogni movimento**
5. metri misti, ![2c](2c.png) esso si chiama misto perchè esso è la somma di due frazione come 2/4 e 3/16, i metri misti posso avere il denominatore multiplo dell'altro denominatore. Tale metro può essere a volte un'alternativa al metro additivo. Dal metro misto possiamo passare all'additivo, trasformando il metro misto in metro additivo. (se il primo movimento è troppo grande non funziona perchè il primo movimento sarebbe troppo grande) Non sempre il metro misto può essere semplificato in metro additivo. Osserviamo sempre i metri misti come scritti da Stockhausen:![2d](2d.png)

6. metri frazionati, Desert di Varese ![5](5.png) osserviamo qui le indicazioni di metri frazionari tali tempi il metro di Bartok additivo serve per non avere ictus ogni cambio di ritmo (si vuole l'ictus solo all'inizio), l'eliminazione degli accenti fa comprendere meglio la scrittura di un compositore ![6](6.png) osserviamo due quarti più una sua metà, etc... (Sinfonia per Meltzel con contenitore per far scrivere qualcosa di nuovo...), i metri frazionari ammettono solo la frazione precedente, e tanti metri possono essere ricondotti i metri misti, in realtà in questo caso la frazione è inserita alla fine; la formula con l'aggiunta è inserita alla fine
  - può capitare anche un modo di ottenere un accelerando scritto, ovvero la seconda specie di metri frazionati con (2/3)/4, di un quarto ne prendo solamente 2 su tre. Osserviamo la partitura di Boulez nel Marteau Sans Maitre ![9](9.png), osserviamo che nel tempo di due minime andiamo a prendere i 4/3, il (4/3)/2 è un 6/4 di cui prendiamo le prime quattro parti ![10](10.png), Boulez si è inventato il modo di accelerare la musica ma cambiano, di fatto avremo il 50% della velocità
7. metri decimali, ![7](7.png) vi sono anche i metri decimali equivalenti ad alcuni frazionati ![8](8.png) il 3.5/4 sarà 3/4+1/8,

### Altri tentativi di notazione metrica

Vi sono stati poi altri tentativi per dare la possibilità di scrivere i tempi irregolari in modalità più semplice:
- Henry Cowell si inventò un modo geometrico per indicare la divisione delle note, binarie, ternarie, quinarie ![11](11.png) la forma delle note indica se le note debbano essere una quintina, sestina...
- Bartolozzi e la ricerca di nuove notazioni per la scrittura ritmica ![12](12.png) ed ancora  ![13](13.png)

---

Per la prossima volta studiare il Rolle:
- numero 6
- numero 7
- numero 8

Realizzarli anche ritmicamente o anche con le note.
