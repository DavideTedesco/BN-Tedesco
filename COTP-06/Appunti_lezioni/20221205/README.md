# Appunti della lezione di lunedì 5 dicembre 2022

## Correzione dell'analisi sulla quinta di Beethoven

Osserviamo come la suddividiamo...

Per l'attacco si realizza un colpo morto, ovvero senza accelerazione, come se il direttore stesse giocando con la pallina.

>Bisogna cercare di capire nelle pulsazioni dove cade l'ictus.

I primi due righi sono abbastanza liberi...

La prima battatuta del terzo rigo è quindi in levare.

Le due battute finali sono libere.

>Per mezzo della corona si fa allungare la frase, con frasi che divengono di 5.

Il pubblico non si aspetta l'a-simmettria inserita dalla corona.

Dopo la seconda corona comincia il pezzo.

Nell'attacco osserviamo:
- ritmo tetico -> sul primo movimento
- acefalo -> inizio su secondo o terzo movimento
- anacrusico -> inizio sul quarto quarto

Bisogna inserire elementi raggruppati almeno due a due, con battute da uno non si riesce invece a contare.

Si può contare anche raggruppando le note, ovvero le battute.

Alcune volte la musica viene scritta in contenitori, ma guardando gli ictus a volte non sono inseriti in maniera giusta.

Ad esempio per il recitativo secco vi sono delle pause che bisogna guardare.

Per il recitativo secco bisogna tendere degli archi per studiare i recitativi e poi iniziare a studiare.

Il flusso ritmico nella musica certe volte è molto diverso rispetto a quello scritto.

## Ritmo libero

La scorsa volta abbiamo visto molti argomenti, mentre oggi vedremo le tipologie di ritmo libero.

Il ritmo libero si chiama ametria, ovvero senza metro, e l'ametria esclude la ciclicità.

### Il canto gregoriano

![1](1.png) _es 25_

Questo ritmo libero ha le caratteristiche di avere un tempo primo ed andando a trascriverlo il tempo primo sarà rappresentato dalla breve, ovvero dal quadratino.

Avremo quindi tutte crome, e in ogni croma cade una sillaba.

Come _gra-ti-a_...

Le note sono sempre brevi, tranne a fine parola in cui vi è un puntino vicino alla semibreve e vi è un puntino con l'allungamento della breve, divenendo una omega.

Si allunga quindi la nota con i puntini.

Si vedono i segnetti verticali che sono episemi che serveno per inserire accenti.

>Questa musica è aciclica, è libera, poichè non vi è ciclicità.

Vi è un tempo primo, ovvero ogni breve è il tempo primo.

### Parlando rubato

![2](2.png) _es 26_

Esempio tratto dal Dies Irae di Penderecki, in cui non si ha tempo, si ha un _parlando rubato_.

Non vi sono dei tempi primi, ma vi sono delle figure musicali sempre differenti.

### Tra le due corone: cadenza

Il secondo modo per avere il ritmo libero è fra due corone, e la prova è la quinta sinfonia di Beethoven.

![3](3.png) _es 27_

Si sente l'oboe con tempo sospeso tra le due corone.

Tra due corone vicine vi è sospensione di tempo e si può chiamare anche cadenza e tale cadenza si può anche sviluppare.

(Piero Bellugi maestro d'orchestra)...

### Variazione agogica

Chopin, creando dei rallentandi e degli accelerandi, ovvero come un ritmo libero.

Provare quando si ha un accelerando di un po' di battute, si deve fare in modo da dividere le battute quantizzando l'accelerando quindi ogni due battute si accelera un po'.

>Solo le grandi orchestre sanno accelerare in maniera continua, mentre si riesce ad accelerare due battute alla volta ottenendo un risultato simile.

### Aleatorietà

Alea viene da dado, e la musica aleatoria si basa su questo principio, ovvero su un parametro che non si conosce.

La prima forma aleatoria di musica è la Sequenza I per flauto.

Berio usò la notazione spaziale, ovvero proporzionale. E la figura ritmica sparisce e sono tutte proporzionali.

E la durata dipende dalla distanza fra contenitori.

Berio suddivide i contenitori in un tot di tempo ed all'interno di un tempo si devono realizzare dei suoni.

In base alla posizione che lo note assumono nel grafico avremo le durate.

![4](4.png) _es 28_

(Sequenza per flauto con musica aleatoria, e il controllo dei valori temporali sono in un alea controllata)

Il parametro tempo è aleatorio se esso non è precisato appieno.

(Prenderemo una parte di primo violino di 12 battute dei maestri cantori di Wagner e dovremo tradurla in notazione proporzionale.)

Osserviamo nel brano per voci, in cui i soprani primi hanno Do-Si-Mi-Re e si ha un capo di note con ritmo libero.

### Seconda scuola di Vienna: puntillismo

![5](5.png) _es 29_

Abbiamo in questo esempio di Stockhausen delle note isolate che distruggono la sensazione metrica.

Esempio tratto da Kontrapunkte di Stockhausen.

### Continuum

Esempio di continuum che distrugge il ritmo come il Secondo preludio di Bach.

O tutta la corrente minimalismo.

Ligeti, con il brano Ramifications.

### Ritmo libero con multimetria

Ovvero quando il cambiamento dei metri non da ciclicità.

Ovvero ad esempio quando Stravinskij riesce all'interno di un pezzo con l'indicazione metrica, a far sparire il ritmo...

Il fagotto di Stravinskij è ad esempio scritto in 4/4

![1](1.png) _es 25_

Osserviamo che vi è ritmo che cambia, vi è il rubato, vi è il cambiamento costante di tempi.

Abbiamo visto che il tempo primo nella multiritmia è l'unità base su cui si basa il ritmo, qui Stravinskij cerca di cambiare il tempo primo, inserendo dei valori irregolari, come terzine, quintine etc...

Abbiamo quindi la frase che inizialmente può sembrare metrica e ritmica, è completamente libera.

>Parlare di tempo rubato non ha senso se non vi è una metrica, Chopin dice che il tempo rubato è quando la mano sinistra deve essere il maestro di cappella, mentre l'altra mano deve non cadere esattamente. Vi deve essere una sorta di rubato fra le due mani e alla fine i conti dovranno tornare.

Leggere i testi di Chopin sul rubato Chopiniano.

### Notazione

Berio scrisse in notazione proporzionale e poi trascrisse nuovamente con la divisione...

---

Esercizio 4 del Rolle
