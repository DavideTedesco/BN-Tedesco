# Appunti della lezione di lunedì 21 novembre 2022

Continuiamo a vedere la parte teorica e dovremo realizzare un lavoro sulla quinta sinfonia di Beethoven.

## Riepilogo

Avevamo visto cosa sono i flussi ritmici, ovvero le unità ritmiche elementari.

Abbiamo visto che i flussi ritmici possono essere isometrici o misti.

I flussi misti sono i cosiddetti ritmi bulgari, ovvero quelli che la tradizione popolare ha iniziato ad inserire nella storia della musica come:
- Bartok
- Stravinskij

Avevamo visto che il canto gregoriano non può avere una metrica fa parte dei flussi ritmici aciclici, ovvero senza ciclo.

## Battuta

Esso è un concetto italiano, che deriva dall'italiano e non è un concetto derivato dal solfeggio, ma in realtà il concetto di battuta è legato alla pulsazione, ovvero al tactus.

Una battuta non è intesa come levare e battere, ma come tesi (giù) ed arsi (su).

Applicheremo il concetto di battuta ad i brani.

### La quinta sinfonia di Beethoven

![1](1.png)

Osserviamo che la cellula metrica fondamentale si compone di una minima.

La cellula metrica si compone di quattro crome.

Ed abbiamo che il tempo minimo, ovvero la figura che ha un tempo medio nell'arsi e nella tesi.Ci porta

Il tempo primo è in genere una metà del tempo generale.

Abbiamo quindi una minima con tempo primo alla semiminima.

### Esercitazione su Beethoven

Versione pianistica della quinta di Beethoven.

Si potrebbe battere tutto in 1 ma, il diretttore si perderebbe...

La frase va pensata con dei flussi ritmici superiori.

Osservare le differenze di accorpamenti, osservando ad esempio se si possa battere in quattro o in tre.

Si dovrebbe prendere una matita, dividendo con mega battute di 4, e non sempre il periodo è di quattro battute.

Bisogna ingabbiare i colpi da uno in battute superiori di 4 elementi.

Bisogna pensare ad un accorpamento superiore con una megafrase che va aldilà del puro metro.

>Notare l'inizio della quinta, osserviamo che ripete il Re ripetendolo. Ma perchè si ripete anche se vi è la corona?
Dal direttore Swarowski (maestro di Abbado), osserviamo che per gli austriaci vi la corona metrica (che vale un tot di battute), mentre vi è la corona segnale che serve a dare un segnale, essa è posta tra l'adagio e l'allegro di una sinfonia. E il primo violino finito l'adagio si fermava e cambiava tempo. La corona segnale è quella che tutti conosciamo. Quella che vediamo qui è una corona metrica, che serve a non ripetere le battute. Tale corona metrica si fa durare tanto quanto serve per la quadratura della frase.

Beethoven per uscire dallo schema e fa in modo che le due corone metriche siano due cose diverse, in cui abbiamo che ci porta ad una frase di quattro battute ed una frase di cinque battute.

Si cerca di rendere asimettrica la scrittura con le corone metriche in tutti gli autori classici.

(Osserveremo che Beethoven nella sinfonia per Meltzel inventore del metronomo, l'ottava)

L'ottava sinfonia è scritta in due quarti, anche se non è un due quarti, ma è un quattro ottavi, ma non si usava inizialmente il quattro ottavi.

>La prima corona è una corona segnale, mentre la seconda corona è una corona metrica.

(Beethoven inserisce elementi strani con forte e poi subito piano...)

Quantz, da leggere come inserire dinamiche e sforzati dove non vi sono dinamiche. Ciò fa capire dove inserire un aumento di volume o come comportarsi andando ad una dominante o ad una settima.

### Mendelssohn dal Sogno di una notte di mezza estate

Semiminima con punto e tempo primo è una croma.

Cercare un raggruppamento per dare la frase lunga.

![2](2.png)

### Brahms quarta sinfonia

Abbiamo due minime con due pulsazioni come battuta, mentre abbiamo una semiminima come tempo primo.

Nel tempo binario abbiamo la semiminima come tempo primo.

![3](3.png)

### Fuga dal clavicembalo ben temperato

![4](4.png)

Avremo un tempo composto pari a 2/8, e la pulsazione sarebbe con due unità metriche all'interno della misura, ovvero due crome col punto, mentre il tempo primo sarebbe.

### Esempio con il 5/4

Il 5/4 può essere una misura ritmica indipendente o la somma nascosta di due battue, con un ritmo non isometrico che avviciniamo al ritmo bartokiano.

Abbiamo due esempi, il primo è dai quadri di un'esposizione di mussorsgky:

![5](5.png)

Con esso abbiamo cinque cellule semplici, formate da una semiminima.

Mentre è diverso l'esempio A che viene dalla sesta sinfonia di Caikoskji:

![6](6.png)

Sentiamo qui un ritmo misto, in cui ogni misura ha due unità metriche di valore diverso:
- la prima vale una minima
- la seconda una minima col punto

### Messiaen


Oliver Messiaen è un compositore ed organista francese, che fu uno dei più grandi maestri a Darmstadt dove si faceva composizione, egli fu il maestro di Stockhausen e Boulez.
Messiaen scrisse i Mode de valeurs...
Messiaen amando gli uccelli, scrisse un trattato di otto volumi sulla ritmica e l'ornitologia.

Vi sono i volumi in biblioteca di parte del trattato di Messiaen.

Messiaen realizza un brano per organo aggiungendo un tempo primo ad ogni battuta.

Esempio tratto dalla natività del signore di Messiaen.

![7](7.png)

Osserviamo che abbiamo qui tempi composti.

Abbiamo diverse unità metriche in cui:
- la prima unità vale una croma col punto ocn tempo primo pari alla Semiminima
- la seconda unità vale una semiminima col punto

Messiaen, il mio linguaggio musicale, sul ritmo aggiunto, ovvero come rendere ritmi nuovi...

### 2/4 che non lo è

Abbiamo qui che contando abbiamo quattro unità metriche semplici del valore di una croma, sarebbe un 4/8. Ma il tempo metrico non corrisponde all'unità temporale.

![8](8.png)

Pietro Bellugi, direttore d'orchestra che non si interessava dei titoli che si avevano e si faceva l'esame con una partitura semplice ma sconosciuta.

Senza sapere gli autori e i titoli, si faceva esprimere la musicalità...

_La prossima volta vedremo i flussi superiori con il Traumerai di Schumann_.

___

Esercizio 2 per la prossima volta.
