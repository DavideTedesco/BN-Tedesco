# Appunti della lezione di lunedì 12 dicembre 2022

## Notazione proporzionale

Oggi parleremo della notazione proporzionale, ed alla fine ci assegnerà un compito sui Maestri Cantori di Norimberga.

### Storia della notazione proporzionale

Essa venne introdotta da due autori americani.

Tale notazione è aleatoria e fissati dei parametri all'interno di tali parametri si potranno fare delle variazioni.

I primi ad utilizzare tale tipologia di notazione furono:
- Earle Brown - Con Folio
- John Cage - Music of changes


### Introduzione ed esempi

Brown e Cage introdussero la notazione spaziale o proporzionale.

Sappiamo che vi sono due grandi filoni:
- musica aleatoria americana -> il compositore lascia all'esecutore libertà di comporre -> alea compositiva
- musica aleatoria europea -> il compositore fissa delle regole ristrette -> il compositore fissa timbri e altezze e magari si lascia libera la notazione spaziale.


Berio introdusse la musica aleatoria in Italia con la Sequenza I per flauto, con la notazione spaziale, inserendo dei dati ma lasciando parte delle libertà.

Berio dice che mentre la battuta è fissata e si possono inserire elementi musicali fino a delle certe caselle, quindi mettiamo un tot di figure musicali in una battuta, quindi la somma delle figure musicali darà un tempo.

Nella notazione spaziale, lo spazio virtuale non rappresenta più un numero, ma rappresenta una quantità di tempo. Quindi uno spazio di un centrimentro potrebbe ad esempio rappresentare un secondo. La posizione delle note determina il loro valore.

I rapporti di durata secondo Berio vengono definiti da cosa?

Osserviamo nell'esempio numero 1 la notazione normale equanto dura
![1](1.png)_confronto tra notazione mensurale e proporzionale_

Abbiamo sopra rappresentata una minima, e la seconda nota è inserita sulla quinta tacca della nota, la distanza indica quanto sarà lunga la prima nota.

La prima croma nella riga sotto dista un certo spazio dandoci il valore di quanto debba essere lunga la prima nota.

Abbiamo quindi la traslazione in notazione proporzionale.

Sappiamo inoltre che la semiminima vale due spazi.

>Le crome sono unite o meno da travatura, se essa è legata le note sono unite tra loro, se è spezzata si ha una pausa. La minima è rappresentata da una croma con opportuni spazi. Dopo la croma vi è una pausa e la travatura è spezzata.

Osserviamo che non si fa uso con la notazione spaziale delle figure ritmiche come si hanno nella notazione mensurale.
La notazione spaziale serve a far veder bene ritardandi e accellerandi.

![2](2.png)

Vediamo che il primo flauto decelera mentre il flauto due decelera. Ciò si può fare solo con la notazione proporzionale.

La nota acquista valore in base alla sua posizione rispetto ad un'altra nota.

Osserviamo anche come la terzina sia rappresentata.

>La notazione spaziale rende tutta la musica aleatoria. Essa si chiama spaziale perchè le note assumono un certo valore in base allo spazio che occupano. Più è grande distanza più la nota dura.

### Suddivisione degli spazi:Sequenza I di Berio

Possiamo descrivere come sia suddiviso lo spazio in vari modi...

Lo spazio di circa due centimetri della Sequenza I indica come spazio due semiminime da settanta.

![3](3.png)

![4](4.png)

Quindi lo spazio:
- si da in funzione di una notazione metronomica
- si da in funzione del tempo in secondi

### Esempi

Tale notazione si presta molto a ritardando e accelerando.


![5](5.png)
Ma tale notazione non è precisa per il ritmo, e tale problema ha portato Berio a trascrivere la Sequenza I.

### Partiture a confronto di Sequenza

![6](6.png)

1. la prima partitura è la partitura con notazione proporzionale - Gazzelloni fece un'esecuzione nel 1958 - Bartolozzi fece investigazioni sui legni sui multifonici - dopo le prime esecuzioni Berio non fu molto convinto della scrittura proporzionale -> Edizioni Suvini Zerboni
2. si pensa che Berio avesse concepito il brano in notazione mensurale con 2/8
3. Berio scrisse una "notazione anfibia" con nuova scrittura nel 1992 per le edizioni Universal

Le tre partiture si differenziano anche per quando e come furono realizzate.

Nella terza edizione non appaiono i segni di battuta.

I flautisti per studiare la sequenza non la trascrivono.

>La notazione proporzionale aveva dei limiti con minore accuratezza e precisione per quanto riguarda il ritmo.

### Esempi di notazione proporzionale

Essa è imprecisa dal punto di vista ritmico.

![5](5.png)

Come nell'esempio della terzina.

Un musicologo ebbe da ridire su tale notazione, perchè essa occupa molto spazio.

![7](7.png)

Stringiamo la scrittura dove non serve, mentre nella scrittura proporzionale se allarghiamo lo spazio deve rimanere uguale.

>Sappiamo che con la notazione tradizionale posso produrre un qualcosa d'insieme con parti divise, ogni esecutore deve avere tutta la partitura poter seguire le linee con tutte le situazioni.

Ligeti in Adventure utilizza sia la notazione metrica tradizionale, sia la notazione proporzionale con un quintetto.

Tale notazione serve per determinati scopi.

Osserviamo gli esempi di pause di legati e staccati, oltre chele crome:

![8](8.png)

![9](9.png)

Osserviamo che in alcuni casi si utilizzano le sole teste delle note.

![10](10.png)

Possiamo utilizzare anche le sole teste senza note lunghe.

![11](11.png)

Osserviamo quindi la travatura senza gambette.

Il bello di questa notazione è che per quanto concerne i segni di legatura, essi non cambiano.

### Atteggiamento del direttore con la notazione proporzionale

Il direttore da in genere gli attacchi, come ad esempio nel brano di Stockhausen Gruppen.

>Vi è indeterminazione per altezza, timbro, tempi e dinamiche...

### Compito per casa sulla notazione proporzionale

Utilizzare la partitura di Wagner nel Siegfried Idyll.

Osserviamo delle frecce rosse con undici battute nel mezzo.

Bisogna trascrivere le 11 battute in notazione in spaziale.

Nella musica spaziale non vi sono punti, il punto è dato dalla travatura.

Si parte specificando lo spazio che noi andremo a specificare, ovvero ogni contenitore è espresso in numero metronomico o in secondi.

Bisogna capire Ruhig bewegt cosa significhi, ovvero una sorta di andante, quindi circa 70 di metronomo.

Abbiamo quindi la semiminima uguale a 70. 35 sono per la minima, mentre 17,5 sono i battiti al minuto per semibreve.

Una battuta durerà circa 3,5".

Il contenitore sarà di tre secondi per ogni battuta.

Se la semiminima è uguale a 80, quindi la semibreve vale 20.

Fissando la semiminima uguale a 60 abbiamo...

60:4=76:x

Ogni battuta dura circa 3".

1. bisogna tradurre il tempo in spazio
2. inserire episemi con lo spazio di 3", ed avremo la traduzione in spazio della
3. la travatura può assumere vari significati:
  - legare la testa di due note
  - spessore della travatura per indicare anche la dinamica

Non prendere in considerazione le altre indicazioni di andamento.

### Franco Donatoni e la musica proporzionale

Donatoni ha utilizzato un tipo di scrittura metrico-proporzionale.

Osserviamo la partitura di Lumen.

![12](12.png)

Osserviamo che sono rispettati gli spazi delle crome anche se la notazione è metrica.

Si usa quindi la proporzione con la scrittura metrica.

Lo spazio è quindi rispettato.

---

Osserveremo la scrittura di tempi misti, Messiaen, etc...

---

Esercizi con il Rolle...

Lavorare all'esercizio 5 per la prossima lezione.
