# Appunti della lezione di Giovedì 21 Gennaio 2021

Vi era uno scambio sotterraneo tra Stravinsky e la scuola musicale di Vienna.

>Riflessione di Stravinsky, in cui avrebbe mutato la cantante per ascoltare la musica del Pierrot Lunaire.

Stravinsky rimase impressionate e scrisse le "[Le 3 poesie della lirica giapponese](https://it.wikipedia.org/wiki/Tre_poesie_della_lirica_giapponese#:~:text=Le%20Tre%20poesie%20della%20lirica,nella%20traduzione%20russa%20di%20A.)"

Ravel "[Trois Poeme di Mallarmè](https://it.wikipedia.org/wiki/Trois_po%C3%A8mes_de_Mallarm%C3%A9)" -> collegamento con le 3 poesie di Stravinsky con lo stesso organico -> anche le "[Chansons madécasses](https://it.wikipedia.org/wiki/Chansons_mad%C3%A9casses)" -> brani di bellezza incredibile -> legati alla conoscenza di Stravinsky del Pierrot Lunaire.

Webern spesso viene frenato da Schönberg

**Visione analisi di Giovanni**
Articolo di Schönberg -> che si chiede se vi siano ascoltatori pronti ad ascoltare questo genere -> rapporto dare-avere, molto difficile da intendere.

Le precisazioni sempre maggiori da parte del compositore, non vanno per forza in una direzione precisisissima, ma i capisaldi che non devono venire a mancare.

Notazione **ostensivo inferenziale** -> verità piú vicina alla mia, ma che devi cercare di interpretare.

La notazione sono elementi indizio, non piú precisazioni.

>Se vi sono tutte le indicazioni vi sono, perchè allora non vengono uguali?

L'esecutore allora deve capire come eseguirlo interpretando gli indizi.

>Il compositore parte dalla rappresentazione di un'idea, e lo sforzo è di riprodurre graficamente l'idea che ho in mente

La musica non è piú la realizzazione, ma sopratutto il progetto!

Dichiarazione sul tempo:
>La musica non è in grado di esprimere alcunchè!

Nella musica possiamo dettare le scansioni del tempo attraverso la scrittura musicale

>Abbiamo il tempo che è invisibile davanti a noi, buttando una griglia sul tempo abbiamo una scansione del tempo

>Pezzi che riescono, sono quelli che ti fanno dimenticare quello che è stato utilizzato per scrivere

Ma cosa è la cultura?
>La cultura è ciò che si fa e si dice, dimenticandosi dove lo si è studiato


[Sieben Worte Gubaidulina](https://www.youtube.com/watch?v=uAMboPrZ4jM)

**Visione analisi di Gabri**

(Forme sistemi e linguaggi musicali)
