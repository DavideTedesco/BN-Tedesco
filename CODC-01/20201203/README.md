# Appunti della lezione di Giovedì 3 Dicembre 2020

## Terzo brano Op.11 di Webern
Attesa che crea il brano nella sua brevità.

Piccolo dettaglio della terzina che va verso l'alto(mentre va tutto verso il basso).

Il problema di trascrizione musicale fa anche chiedere cosa voglia dire essere un esecutore, con indicazioni in cui vi possa essere quasi estranei.

La pausa di battuta 7 diviene un elemento strutturale.

Il fatto dell'ascesa finale vi è nell'Erwartung come in questo brano.

Malher ha dei momenti delle sinfonie molto vicine alle sonorità elettroniche, seconda sinfonia con strumenti a percussione.

## La nascita del Cromatismo

[Ascolto di Nuage Gris di Liszt](https://www.youtube.com/watch?v=tYKl41e_hoU&ab_channel=musicanth)

Sembra che si utilizzi il minimo indispensabile con una scala cromatica.

Con l'ultimo rigo vediamo che si avvicina molto a Webern.

![liszt](liszt.png)

I tre grossi fari possano essere per il cromatismo:
1. Scrjiabin -> con un accordo misitico creato per quarte
2. Liszt
3. Wagner

Cesar Franck usa il cromatismo come una tonalità allargata(a non far straripare oltre la tonalità).

Cosa succede in altre parti del mondo?

Per la prima volta compaiono i cluster

![clus](clus.png)

Introduce come colore timbrico i cluster, con una scritturra semplice come un blocco di note.

[Ascolto di The Tides of Manaunaun di Henry Cowell](https://www.youtube.com/watch?v=-a7ndvvdJTo&ab_channel=Hexamarah)

[Ascolto di Exultation di Henry Cowell]()

[Ascolto di The Tiger di Henry Cowell]()

[Ascolto di The Banshee di Henry Cowell]()

Ricerca di come usare le corde, di una ricchezza notevole, che assomiglia ad un brano di musica elettronica.

![banshee](banshee.png)

La follia del "Risveglio di una città" di Russolo.

Con gli intonarumori.
![citta](citta.png)

Il paesaggio musicale ti influenza la dove non ci pensi.

Ciò che darà ragione a Pierre Schaeffer, sarà l'avvento dell'altoparlante!

Poesia di balla che identifica un brano.

[Ascolto di Paesaggio di Balla](https://www.youtube.com/watch?v=WN0_IL9TK0U&ab_channel=SymphonyDSCH)
![paesaggio](paesaggio.png)

[Ascolto del Pierrot Lunaire di Schönberg]()

Siamo partiti dal manoscritto di Webern per poi arrivare ad i manoscritti, con certe tendenze ad enfatizzare delle note.

Oggi individuare il compositore è meno difficile. (la completezza e confusione di ruoli complica la scena)

Fare musica è solamente fare performance?

Il compositore è cantautore?

(Quando Donatoni partecipò al Maurizio Costanzo Show, uscí una figura di Donatoni con il cappello da cowboy)

Il pubblico non ha oggi degli strumenti di valutazione della musica, bisognerebbe pensare di costruire dei canali per fornire un prodotto di qualità.

Testo di Gentilucci Oltre l'avanguardia. Un invito al molteplice

la Klangfarben Melodie di Webern la prendi in termini di lettura diversa

La scelta delle note corrisponde alla realizzazione di un certo aspetto timbrico.

"Non mi interessa quali sono le note che producono un qualcosa, ma cosa realizzino le cose messe insieme."

"L'altezza è uno degli aspetti del timbro"

La differenza sostanziale è che nelle due grafizzazioni.

Per il musicista tradizionale l'altezza dei suoni è una successione con un senso con quello che viene dopo e prima.

Una regola del contrappunto è che le voci si possono incrociare solo se il timbro delle due voci è diverso.

Verklarte Nacht di Schönberg viene bocciata perchè vi è un'accordo in quarto rivolto di nona.

Nel contrappunto tradizionale il problema del timbro è sempre risolto dall'altezza.

La geniale frase di Schönberg dice che le "altezze sono un aspetto del timbro."

(Realizzare un discorso legato al timbro puro del suono... Capacità di giocare con singoli suoni che restino autonomi nel discorso.)

I primi brani di Boulez e Stockhausen:

- per una nota sola

- secondo studio per 4 o 5 note

Boris Porena modi per comporre

Brano di Guaccero il cielo e l'altre stelle con una partitura simile a quelle di Boris Porena

![porena](porena.png)
Il problema del brano è il bilanciamento tra materiale e forma.

Nella sinfonia vi è un primo e secondo tema, ma nella coda si introduce un secondo tema per sviluppare una forma piú grande.

Anche l'aspetto grafico serve per una realizzazione compositiva.

Avere già un'idea di quello che vuoi costruire di quanto materiale si ha bisogno, indubbiamente quello della forma è l'aspetto principale e piú complesso da affrontare.

"Non sono le idee che ci fanno diversi l'uno dall'altro, ma l'utilizzo che ne facciamo" - Schonberg

## Stravinskij analisi del terzo brano per quartetto d'archi

Nel caso di Stravinskij abbiamo un cambiamento della macrostruttura che influisce sulle microstrutture, mentre nella scuola di Vienna cambio gli elementi interni perchè cambi la struttura.

Questo spiega perchè Stravinskij sia piú vicino al pubblico.

Questi saranno i due grandi filoni della musica contemporanea.
1. macrostrutture -> microstrutture
2. microstrutture -> macrostrutture
