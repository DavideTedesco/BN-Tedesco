# Appunti della lezione di Giovedì 19 Novembre 2020

Daniele Paris era un direttore d'orchestra ciociaro, diresse il Conservatorio di Frosinone, allievo di Petrassi. Dopo un concerto di musiche di Petrassi andarono a cena insieme con Petrassi.

Ferrara, direttore d'orchestra, aveva un problema di crisi epilettiche durante la direzione dell'orchestra, entrava nella musica e sveniva. Fu per questo che finí a realizzare didattica.

Ferrara insegnava lettura della partitura a Santa Cecilia.

Paris era veramente un tecnico, mentre Ferrara era molto attento alla percezione.

Ferrara lezione "riprenda dalla battuta 25",
__________

## I tre brani per quartetto

### Primo brano di Stravinskij

Idea che ha dato questo brano (presa da Vlad), trucioli rimasti dalla Sagra della Primavera.

![schema_luca](schema_luca.png)

È sempre molto pratico nelle sue cose.

Individua gli elementi fondanti della percezione musicale, ma a modo suo, perchè la sua idea va oltre lo strumentario a sua disposizione.
### Secondo brano

- aspetto della politonalità, esistono delle scale russe basate su modi, dunque scale modali. Modalità che fa uso di scale particolari.

- sentire l'opera Il Naso di Schostakovic, uso di un linguaggio che era la norma

- Bartok, Kodaly

- anche le radici del rock magari possono essere riconosciuti nella musica russa, molta modernità e della musica progressive.

Testo di Vlad su Stravinskij è alla base. (Modo a trasposizione limitata, influenza fortissima da parte dell'Est) Quando arriva Stravinskij lo scrive, perchè è un vero e proprio barbaro... La carriera successiva è una sorta di recupero del metodo Neoclassico.

L'accordo centrale su cui si sviluppa Petruska, Do maggiore sovrapposto a Fa maggiore, c'è un modo russo in cui questi suoni si sovrappongono, punto di vista diverso.

Janacek, fa uno studio incredibile sull'andamento melodico sul linguaggio parlato cecoslovacco, excursus melodico molto ampio che riporta poi nel canto delle sue. Certi andamenti ritmici hanno come il modo di parlare dell'area slava.

Il melisma non è in genere considerato l'abbellimento.

Pubblicati poi nel 1928...

(questi tre pezzi diventano poi 4 pezzi per orchestra)

(subito dopo la Sagra della Primavera, c'è una reazione da camera, con pezzi per piccoli gruppi, vi è un cd Songs, miriade di pezzi piccolissimi di Stravinskij per organici minimi, su ricordi della sua infanzia)

(scrive i 3 pezzi facili per pianoforte ed i 5 pezzi per pianoforte a due mani)

Capiamo da questo brano, il repertorio di Stravinskij

Blues con i cries, e Stravinskij nel secondo brano, che lui trasforma in musica.

### Terzo brano

oreficeria etrusca
_______
**La storia della musica occidentale è stata anche una storia di selezione** che ha escluso parte della musica popolare.

Ma inglobare materiali non colti in materiale colto è ciò che da Malher a Stravinskij ha aiutato. (Les noces di Stravinskij 4 pianoforti).

Utilizzo del computer anche non sapendo bene i funzionamenti. Questione di identità, mancanza di identità.
__________

Sinfonie di Stravinskij che si avvicina alla parte corale di Atom earth mother

La musica è una possibilità che l'uomo cerca di realizzare qualche cosa

Stravinskij ha capacità di nobilitare ogni cosa che scrive, ha un contatto con la musica che è quasi una merce di scambio.

Spesso non basta dire politonale o atonale ma bisogna differenziare.

Spesso nell'analisi si inventano classificazioni che servono per la comprensione.

_______
Analisi di Roman Vlad -> anticipa uno dei procedimenti -> rigidità strutturale

(Afasia dell'uomo contemporaneo, con il primo brano)

I brani sono fonti di tante riflessioni.

__________
Introdurre almeno due lezioni sulla semiografia

Cattedrale - di Carver -> da leggere

Convinzione di un'idea di musica che Stravinskij scrive.

L'importante è preservare l'idea iniziale che ci portiamo dentro, perchè già nella rappresentazione avvengono cose che ci omogeneizzano.

La personalità è quando preservi le tue idee.

Partiamo la prossima volta con la semiografia.

**scollegamento fra le parti**

Giovedì 26 Novembre non ci sarà lezione.
