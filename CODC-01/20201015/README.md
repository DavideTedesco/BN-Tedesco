# Appunti della lezione di Giovedí 15 Ottobre 2020

Sarabanda, trasmissione per riconoscere le canzoni, impressionante capire e sentire, ma all'interno del riconoscimento di una melodia.
__________

## Gli argomenti della scorsa lezione

Eravamo partiti dal segno linguistico e poi ci siamo addentrati sulla caratteristica linguistica: significante e significato.

Poi abbiamo approfondito il piano dell'espressione e del contenuto.

![conden](conden.png)
Siamo poi arrivati a Roland Barthes e come un segno denotativo può divenire poi un segno connotativo(significati a catena), e mette in moto lo schema di Peirce.

Come noi studiamo l'analisi, studiamo le esecuzioni e mi baso su ció che riesco a comprendere anche dal punto di vista connotativo.

E siamo arrivati a:
![molino](molino.png)

Ed arriviamo alla forma simbolica di Nattiez

c'è una differenza tra segnale(unico significato), il simbolo e le forme simboliche(infiniti significati), le forme simboliche sono in genere forme artistiche
![nattiez](nattiez.png)

Ultima citazione definisce come l'intervento del destinatario sia parte di un processo estesico dell'emittente. Il messaggio ha rapporto attivo da entrambi i lati.
![mess](mess.png)

Abbiamo anche visto come lo schema prima di questo era "solamente" di tipo passivo rispetto al destinatario.

E abbiamo visto poi cosa sia il livello neutro. Abbiamo visto come l'"analisi passo-passo" sia complessa, come una cronaca. Il livello neutro è quindi la cronaca del messaggio e non dovrebbe tener conto del processo estesico e poetico.

### Analisi poeietica

Che è sulla base di tracce lasciate dall'autore e delle intenzioni dell'autore.

### Analisi del livello neutro

Non si tiene conto delle tracce o del pensiero dell'autore, qualsiasi intervento deve essere portato fino alle ultime conseguenze. Se si mette in moto un metodo statistico metti in gioco la figura dell'analista e delle proprie conseguenze, perchè in effetti non si dovrebbe mai intervenire se non alla fine. (un'analisi tale potrebbe non avere mai un termine)(è difficile mantenerlo neutro)

Le dimensioni poietiche ed estesiche sono state neutralizzate, perchè già porsi un obiettivo è un procedimento estesico, e tali analisi sono in genere di difficilissima lettura.

Ogni volta che si approccia ad un'analisi di fatto si cerca comunque qualcosa.

Umberto Eco quando parla dei suoi libri, i contenuti sono messi dai lettori, e vi è uno scambio tra il poietico, l'estesico ed il neutro.

Differenza tra livello neutro e interpretazione(problema del decostruzionismo di Eco):
![eco](eco.png)

### Decodifiche aberranti

Eco arriva ad elencare le cause di decodifica aberranti:
![aberra](aberra.png)

L'analisi entra in crisi dal 1908 al 1913 quando Schönberg inizia a scrivere atonale. (Schönberg spera che l'ascoltatore capisca...)
Questi brani sono isolati perchè non abbiamo il codice.

E le analisi prima di Schönberg divengono senza senso, come le analisi della Divina Commedia aberranti.

Esse sono:
1. incomprensione messaggio per carenza di codice
2. incomprensione per disparità di codici
3. incomprensione per interferenze circostanziali
4. rifiuto totale e delegittimazione dell'emittente

**_Visione dello sketch di Achille Campanile_**

Applico il codice in maniera diversa e che puó essere interpretato diversamente.

### Forme o pratiche simboliche

Abbiamo identificato la musica come forma o pratica simbolica, e parliamo di attività che usano non solo segnali, ma anche simboli, segni e quindi significati.

![forme](forme.png)
- Queste forme e pratiche simboliche si compongono di parametri che si compongono di parametri sostanziali come:
  - ritmo
  - timbro
  - intensità
  - modulazioni d'altezza
  - intensità

- Queste forme fanno appello a movimenti del corpo
- impiego strumenti
- creazione ed esecuzione
- dimensione pragmatica
- schema tripartito
- associazioni mentali
- giudizi estetici
- enunciati o trattati linguistici

Questo aspetto è fondamentale, perchè nell'arte contemporanea questi aspetti sono sempre stati presenti, per farla divenire un tipo d'arte diversa.

Tra le varie arti c'è uno scambio continuo, poichè all'interno di alcune attività umane vi possono essere scambi sostanziali.

**_Ascolto del canto [Inuit Katajjaq](https://www.youtube.com/watch?v=qnGM0BlA95I)_**

Cercare di imitare suoni sempre piú difficili.

Qui non vi è proprio il concetto di musica, per ottenere qualcosa di diverso da quello che noi abbiamo come idea di arte

**_Ascolto dei [banditori d'asta del Texas](https://www.youtube.com/watch?v=EoOXFYfTLFA)_**

![sator](sator.png)
Come il meccanismo del quadrato magico, che prevede all'interno tutte le forme dodecafoniche, come anche le strutture di Boulez per pianoforte
![boulez](boulez.png), ed ancora di più Metastastasis di Xenakis
![meta](meta.png)

La contaminazione di codici è uno degli aspetti piú presenti nell'arte contemporanea.

Le pratiche e forme simboliche sono un po' la riserva di tutta l'arte. Variando le dosi

### Codici

![codici](codici.png)

Arriviamo ad i codici della musica, e la maggior parte delle metodologie di analisi si riferiscono alla totalità o ad alcuni di questi codici; come quella di Sckhenker che si basa sulle altezze; o ad alcune analisi contemporanee che si basa sul timbro.

(Le stesse considerazioni sull'analisi vanno applicate al discorso dell'intervista)

_Intervista a Boulez dal testo "Per volontà e per caso"(analisi di tipo estesico delle sue opere) ed esso che è l'interprete di se stesso: da di sè un'impressione diversa, ponendosi fuori dal processo poietico e porsi fuori dall'opera -> un qualcosa di tecnica si deve tramutare in espressione_ Costruisco, ma devo capire che dall'altra parte c'è necessità di comprendere ed intendere.

C'è stata dopo questi compositori una maggiore coscienza di affrontare un discorso sul codice. Ed alcuni compositori contemporanei come Cage che mette in evidenza che la sua riflessione basata sul codice. E dice che il titolo del lavoro è la durata del brano, ed il brano si basa su codici.

![433](433.png)

"È stato eseguita da David Tudor (che è pianista), ma può essere eseguito da qualsiasi strumentista e puó essere di qualsiasi durata". Questo è il massimo di opera aperta!

Oggi c'è coscienza nella semiologia che noi utilizziamo codici e rapporto con l'ascoltatore legato alla padronanza dei codici.

**_Ascolto di Variazioni 3 di Cage_**

Esso è piú un trattato di semiologia che un brano vero e proprio.

Il codice ha avuto gioco facile per molti anni, perchè esso ha dato la possibilità di comprendere facilmente. Ed un codice che ci permette di comprendere cosa puó esserci dopo.

Per la prima volta chi produce il livello neutro è il processo estesico e non piú il processo poietico.

**_Ascolto di Atmospheres di Ligeti_**

Qui si va su una selezione del codice anche se poi c'è una possibilità limitata delle altezze

**_Ascolto di Structures di Boulez_**

Pianoforte produce solo alcuni suoni ed abbiamo una restrizione ancora maggiore.

**_Ascolto Bach Prelude No.8 in mi diesis minore_**

Partendo da Cage ed arrivando a Bach siamo partiti da un massimo di entropia per arrivare a un minimo di entropia e passando da nessuna conoscenza di codice a una massima conoscenza di codice.

Cage incarna l'approccio dadaista e porta quello con l'intuizione quello che era successo con il dadaismo nell'arte, in musica.

Cage è legato alla lettura che ne viene data.

Il condizionamento dell'esecutore "quanto è condizionato dal fatto che sta eseguendo Cage?".

L'approccio con la musica contemporanea è legato proprio al fatto che do una lettura perchè sto eseguondo Cage, e dunque Cage non esclude il pianista classico dall'esecuzione di un certo brano.

#### Arte visiva e arte sonora

Discorso, punti nodali e differenze tra le due arti.

- Discorso semiologico da un lato e legato al segno in se che ha proprio un aspetto fondamentale

- Altro approccio è legato alla psicologia della percezione.

Concetto chiave è stato: **In che modo io rappresento la realtà?**

- rappresentazione iconica -> ovvero un'icona che mi permetta di riconoscere e distinguere un qualcosa nella realtà
- rappresentazione anche numerica -> che può favorire un tipo di pensiero musicale
- rappresentazione con la gestualità

La lettura della realtà passa attreverso questi meccanismi.

Io so che se ti do un gesto o un'indicazione tu ti indirizzi in una certa dimensione.

Scollamento tra artisti e pubblico è dovuto a mancanza o lontananza dal pubblico di un codice.

(Libro consigliato sulla musica di Nyman Musica Sperimentale)

Schaeffer diceva che il problema è stato il legame con la prassi dell'ascolto.

**C'è un problema di codici anche quando non ci sono i codici!**

"Sappiamo trovare, quello che sappiamo cercare"

Anche l'opera atonale di Schönberg ha sempre in mente una ricerca della didattica, e Schönberg ha proprio un'attenzione ed una chiave di lettura della didattica e dell'aiuto alla comprensione.

N.6 op.9 di Schönberg ci fa intendere didatticamente:
- brano corto
- indicazioni nuove mai date
![scho6](scho6.png)

**_Ascolto op.9 No.6 di Schönberg_**

Qui ci sono scelte fatte per far comprendere la lontananza dalla tonalità come:
- accordi con sovrapposizione di quarte (esce fuori la scala cromatica) -> idea che non rientra in una scala di 7 suoni
- volontà di non dare punti di riferimento -> per non far ascoltare di nuovo qualcosa che ha già sentito(si limita la durata del brano)

I brani di Schönberg sono una sorta di miracolo.

Nell'op.19 Schönberg ottiene un qualcosa di straordinario poichè inizia con sol e si, poi continua e sembra che sia in sol maggiore, ma sentiremo questi suoni solo come singoli suoni, e l'obiettivo è scollarti dal codice tonale che ti senti dentro.
Diminuisce la tensione all'altezza, ma cresce la percezione del ritmo.
È lontanissima da noi l'idea della tonalità.

![op19](op19.png)

Se ci fosse l'idea della tonalità ora capiamo che la stiamo liquidando.

Se si vuole comprendere il passaggio dalla tonalità alla atonalità, che spiega perchè si arriva ad un certo punto. E poi la convinzione che Schönberg abbia il dovere morale di farlo per poterlo fare nella storia.

(Lettura consigliata del Manuale di armonia di Schönberg)

La necessità di dimostrare che con un'opera scritta dodecafonicamente si può anche ridere(Operina Von Heute auf Morgen).

Cercare di analizzare l'op.19 è un problema e la ricerca all'interno dello stesso attraverso il codice tonale.
![an19](an19.png)

C'è una coscienza impressionante di quello che fa!

Manca un codice nell'op.19 e la volontà di non avere codici, porta all'afasia, e ho il rischio(data l'educazione tonale) di percepire tutto tonalmente; ma lui non vuole proprio farlo sconfinare nel tonale.

Molti autori come Ives, giocano sul meccanismo percettivo.

![mibre#](mibre.png)

In alcuni punti mette mi bemolle e re diesis

Si permette a livello filosofico di differenziare due suoni uguali, per far riflettere su quello che si sta suonando.

Le indicazioni iniziano a crescere esponenzialmente e ad essere non musicali, inoltre i brani di Schönberg sono anche complessi da suonare.

2 libri molto consigliati
- "Manuale d'armonia" di Schönberg
- "Figure della musica" di Sciarrino

2 libri universali che calzano a pennello su qualsiasi ambito.


Immagini da la figura della musica di Sciarrino in cui ad esempio vediamo in Beethoven un processo di accumulazione e ciò lo vediamo anche in Wagner nell'Oro del Reno. Anche Nel quartetto op.127, l'inizio sembra qualcosa di contemporaneo.

**_Ascolto inizio quartetto op.127 di Beethoven_**
Altro elemento che introduce è il _little bang_, quando la parte sta languendo, vi è il pizzicato che fa astrarre dall'analisi limitata al solo approccio tonale.

**_Ascolto seconda parte del quartetto op.161 di Schubert_**

O un Big Bang
**_Ascolto dell'inizio Don di Boulez_**

**_Ascolto del quadro secondo atto terzo Boheme di Puccini_**

**_Ascolto della Sonata n.2 di Sciarrino_**

Quando ci leghiamo a meccanismi diversi, la comprensione della musica attraverso codici nuovi della musica del passato si allarga.

### I codici della musica
**Possiamo parlare della musica come oggetto comunicazionale e sonoro**

Non possiamo legare che la musica si sia legata a 4 codici:
1. altezze
2. durate
3. intensità
4. timbri

E le analisi in genere vanno per queste quattro dimensioni di codice.

Questi codici rappresentano delle "segmentizzazioni", poichè abbiamo un numero di altezze enormi, e il codice limita le altezze a quelle che vengono prescritte per la musica.

Ad ogni periodo storico corrisponde una segmentizzazione diversa.

**_Ascolto dell'inizio dell'Oro del Reno di Wagner_**

I 4 minuti iniziali liquidano totalmente la tonalità. (Che inizia con il Tristano ed Isotta)

C'è qui una forte anima teatrale, ed entra una forte immagine del fiume ed attraverso ciò scatta una molla musicale.

Wagner usa il cromatismo perchè per lui rappresenta la passione amorosa, è tonale e sfrutta il cromatismo per poter far passare il concetto di passione. Il discorso non è tecnico, ma come rappresentazione mentale, e da un marchio indelebile al cromatismo.

**_Ascolto del Preludio del Tristano ed Isotta di Wagner_**

Vi è atonalità e non si capisce bene in che tonalità sta e dove vuole andare. Sono tutti accordi che hanno una fortissima tensione a muoversi.
