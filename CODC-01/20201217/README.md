# Appunti della lezione di Giovedì 17 Dicembre 2020

[Antonino Pirrotta](https://www.treccani.it/enciclopedia/antonino-pirrotta_(Dizionario-Biografico)/)

Umberto Eco, e i giochi di parole che si divertiva a fare con Berio.

_____

Avevamo parlato delle metodologie storiche dell'analisi.

## Metodologie dell'analisi musicale
![meto](meto.png)

Superata l'indigestione di strutturalismi e serialismi, il compositore realizza un codice per ogni brano.

Oggi vi è una molteplicità di approcci alla composizione, questo tipo di metodologie e di analisi le abbiamo viste per vedere se alcune di queste metodologie possono essere usate per le nostre analisi, o se le possiamo usare anche per altro repertorio oltre quello tradizionale.

- Metodologia Schenkeriana
![sche](sche.png)
I 12 suoni sono una sorta di materiale dalla quale pescare ed usarlo, e possiamo anche usarla come fonte di deduzioni per usarla per apprezzare anche

.
.
.

- Elaborazione di réti
- analisi di Riemann
- Semiologia musicale
- teoria dell'informazione(che si addice a una visione moderna e contemporanea)
- analisi insiemistica

### Analisi della musica contemporanea
![con](con.png)

(Bent si ferma alla teoria dell'informazione)

Possiamo far vivere in una stessa analisi piú metodi diversi?

1. affidarsi inizialmente all'ascolto
2. usare poi le metodologie di analisi

#### Punto, linea e superficie

Viene meno l'approccio contenutivo per dedicarsi a quelli che sono i rapporti formali e le strutture interne a un'opera a prescindere dal momento comunicativo.

L'espressionismo ha cercato di mettere in relazione strettissima l'aspetto strutturale e l'aspetto materiale.
![villa](villa.png)

Nel Villa Rojo vediamo come i vari gesti descritti da Kandinsky vengo ripresi per la scrittura musicale.

Partendo da una trasfigurazione dei suoni si puó poi raggiungere una descrizione del tempo con linee.

![villa1](villa1.png)

Quando vediamo un brano scritto come l'esempio 14.

Rapporto linee, superificie

Studi in cui si parte da questi studi, partendo da una rappresentazione grafica della musica.

![porena](porena.png)

Recuperando un approccio grafico aperto all'alea che mette in condizioni di scrivere e rappresentare la musica.

![porena1](porena1.png)

L'aspetto grafico è fondamentale.

#### Paesaggio sonoro

[Visione di un video di Nottoli](https://www.youtube.com/watch?v=MqxfMtw6DmM&ab_channel=TecnologieMusicaliEPNRieti)

[norma ISO paesaggio sonoro](http://store.uni.com/catalogo/uni-iso-12913-1-2015?josso_back_to=http://store.uni.com/josso-security-check.php&josso_cmd=login_optional&josso_partnerapp_host=store.uni.com)

[testo di Murray Scheaffer](https://www.ibs.it/paesaggio-sonoro-libro-di-storia-libro-r-murray-schafer/e/9788875920005)

- Impronte sonore

Considerazioni:
- paesaggio sonoro identificabile
- suoni astratti irriconoscibili

Barry Truax:

[Handbook for acoustic ecology](https://www.sfu.ca/~truax/handbook2.html)
- audiosphere (musica come ambiente)
- soundscape composition (ambiente che diviene musica)

Il paesaggio sonoro diviene ciò che ci circonda e diviene tutto ciò intorno a noi.

L'ascolto diviene il primo strumento di indagine, vengo sviluppate delle teorie riferite all'ascolto.

##### Ascolto ridotto

Ascolto secondo Schaeffer in cui viene spogliato il suono ponendo l'attenzione alla materialità del suono, ascolto antinaturale.

L'uomo è tentato di riferire il suono a cosa lo produce.

Spogliando il suono di cosa non è suono, ponendosi alle dimensioni sensibili del suono.

Vi è un tentativo di considerare il suono al di fuori della causa che lo produce.

##### Conservazione del suono

Ciò è in contrasto con le teorie di Murray Schaefer che invece vuole conservare il suono legandolo a ciò che lo produce

##### L'altoparlate

Questo altoparlante permette di sentire il suono lontano dal suo ambiente, staccando il risultato sonoro dall'ambiente.

Non siamo piú legati al sonoro legato all'ambiente, ma al suono che ci portiamo dietro.

##### Soundscape composition
È difficile capire dove finisce il paesaggio sonoro

#### Dualità del paesaggio sonoro

![dua](dua.png)

1. aspetto di Murray Schafer
2. paesaggio sonoro legato a Pierre Scheffer

e quindi:
1. Musica nel paesaggio
2. Paesaggio nella musica

Lettura delle indicazioni per la musica per un ospedale.(condizionamento compositivo)

(Il paesaggio sonoro è per sua definizione anti-ecologica)

[Ascolto del paesaggio sonoro di Battistelli con Experimentum Mundi](https://www.youtube.com/watch?v=1SjFoSi7zLk&ab_channel=NPORadio4)

##### Emmerson
- discorso mimetico
- discorso uditivo
- inserire anche l'aspetto della sintassi (astratta) inserendo in modo astratto o estrapolato

Una sorta di **invito alla molteplicità** che non possiamo categorizzare e estremizzare andando da una sintassi che è il massimo del concreto a una concezione compositiva estrapolata.

La concezione del paesaggio sonoro va ad inserirsi nel solco della musica elettroacustica per arricchire un dibattito.

Rimane fuori oltre la tabella l'aspetto prettamente compositivo, sentendo un luogo come il mare.

**Tutto ciò avviene quando vi è un orecchio umano che sente tutto questo**.

(Il tramonto è arte?)

Momento in cui ci si sente che basta poco per avere un'esperienza estetica! E dipende molto dal momento emotivo che stai vivendo, ciò che io provo è un'aggiunta della mia testa.

Se noi non siamo strumenti sensibili, possiamo esserne attraversati senza avere nessuna traccia lasciata.

"Noi siamo come un filtro"

[progetto di paesaggi sonori per il coma](https://vertigo.starts.eu/calls/starts-residencies-call-3/projects/sounds-for-coma/detail/)
_________

[In piú, David Monacchi](https://www.ted.com/talks/david_monacchi_fragments_of_extinction_the_sonic_heritage_of_ecosystems/transcript?language=it)
