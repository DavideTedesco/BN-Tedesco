# Appunti della lezione di Giovedí 22 Ottobre 2020

## Ripasso

Strumenti di base per accedere a delle metodologie di analisi, che divideremo in 2 branche:
1. pre-atonalità
2. post-atonalità

Ma tutte quante hanno in comune un kernel di base.

Siamo partiti dalla semiologia e dalla definizione del segno per entrare all'interno di una partitura(segni concetto molto ampio).

Abbiamo visto cosa è la comunicazione ed i codici.

E passeremo alla metodologia della Gestalt.

Abbiamo visto la connotazione e la denotazione, e le definizioni di forma simbolica, ovvero la distinzione tra:
- segnale(uno solo)
- segno
- simbolo(veicolo tanti significati)

Le forme simboliche sono un qualcosa che deve essere interpetato.

![schema](schema.png)
E tutto è legato allo schema di Nattiez che mette al primo posto il confronto tra mittente e destinatario.
Asse portante delle nostre competenze.

(Articolo sulla visione dell'oggetto sonoro di Pierre Schaeffer e il paesaggio sono di Munray Schafer -> dare preminenza all'aspetto estesico -> torna il principio della psicologia percettiva)

Abbiamo poi visto:
- analisi poietica
- analisi del livello neutro(ce lo siamo immaginato come un qualcosa staccato dalla realtà, esso fa pensare proprio a Pierre Schaeffer)

Difficoltà degli analisti per analizzare Schönberg è partire dall'espressionismo, ma ha senso ciò?, per cercare di trovare un discorso che trovi un modo di intenderle.

Queste competenze analitiche possono anche portare ad un uso ed ad un'interpretazione e che possono portare anche a decodifiche aberranti(Eco).

Abbiamo poi iniziato a sottolineare come le forme e le pratiche simboliche si organizzano come parametri formali sostanziali. Esse danno luogo ad una determinata forma simbolica a seconda delle dosi che ne diamo.

Partiture con gesti descritti teatralmente da parte degli strumentisti, o partiture grafiche che fanno capo a discipline diverse. Le pratiche simboliche possono essere preparate dando luogo a tutte le forme pratiche-simboliche che vi sono.

Abbiamo inoltre sottolineato come la musica rientra tra gli oggetti comunicazionali con:

autore -> messaggio -> ascoltatore

Ed abbiamo visto che la griglia delle funzioni di Jakobson ha desunto dalla teoria delle comunicazioni linguistiche la possiamo applicare anche alla musica.

![oggcom](oggcom.png)

Come la funzione poetica che differenzia musica colta da non colta(come le cose sono dette e non le cose in se)

_Non sono le idee che ci rendono diversi, ma è l'uso che ne facciamo_ - Schönberg

Levi Strauss -> antropologo descrive come un musicologo le musiche di Bach.

Anche nel brano piú astratto possibile, c'è un margine comunicazionale.

L'esordio di qualsiasi brano, è sempre caratterizzato da una funzione fàtica. (Come pedale dell'Oro del Reno)

__________

## Musica e comunicazione

![deffin](deffin.png)
È da tenere bene a mente questa frase, anche se la globalizzazione ci tiene molto informati sui suoni di altre culture e ci rende aperti alle contaminazioni.

Il linguaggio musicale è un fenomeno in atto e puó cambiare da cultura a cultura, e nel tempo.

Nella musica operano dunque dei codici:
- altezze
- durate
- intensità
- timbri

Anche se possono entrare altri codici nella musica oggi giorno!

C'è una componente discontinua, la musica è un cambiamento continuo di dosaggio di questi codici.
Non c'è dunque un genio nella musica, ma il materiale che ho a disposizione, cambia continuamente, ed il genio è quello che intende per primo i cambiamenti che stanno per avvenire e li fa propri.

Tradizionalmente il continuum fonico è segmentato nei 4 termini che abbiamo appena detto. Essi sono aspetti morfologici.
1. Canto gregoriano -> testo è ciò che vale, la fonicità aggiunta è una sorta di prosodia, l'andamento verbale è preminente
2. Scrittura su pentagramma -> testo diviene la melodia, codice che prende il sopravvento mano mano sugli altri, i primi codici di notazione non prendono in considerazione le durate
3. Nel barocco hanno importanza i timbri
4. nel '900 prendono importanza le intensità.

All'interno di questo schema vi è un'organizzazione a piú livello, ed esso è materia del successivo, ma forma del precedente.

### Altezze
![altezza](altezza.png)

Regime notale, ovvero è pre-scalare.

Nelle scritture antiche piú colte vi era un codice scalare, diverso a seconda delle culture, il codice scalare segmenta le varie altezze secondo un modo individuato.

Il codice scalare lo possiamo organizzare come un sistema sintattico, e dal sistema sintattico potremmo realizzare delle forme.

Questi codici possono essere utilizzati in maniera diversa, un codice scalare puó sfruttare come sistema dodecafonico.

Quando dico codice scalare tonale organizzo i suoni in un sistema in un certo modo, e non è detto che i sistemi piú avanzati debbano escludere totalmente quello che c'era prima.(Come la Sinfonia di Berio in cui usa Malher per montare codici diversi), posso avere come in Berg, un codice dodecafonico in cui riaffiora la tonalità. Ciò non dipende soltanto dalla scrittura, Berg lo capiamo solo con il sistema di codici, come integrare vari codici? Ed entrano in gioco connotazioni e denotazioni dei vari codici.

Struttura del Wozzeck è costruita su forme musicali della tradizione con un linguaggio dodecafonico: fuga, tema e variazioni, sonata, rondò. Ogni scena dell'opera è un codice che viene integrato in un sistema che non gli appartiene.

Molte opere le riusciamo a spiegare solo in termini di codici.

_[Ascolto del Canone a 7 BMV 1078 di Bach](https://www.youtube.com/watch?v=-JrvRy25Kz0&ab_channel=AnthonyMondon)_

Esempio di come il codice polifonico e i metodi contrappuntistici possano funzionare bene. Canone a 7 voci in un regime scalare

_[Ascolto del Gamelan Javanese](https://www.youtube.com/watch?v=UEWCCSuHsuQ&ab_channel=GenelecMusicChannel)_

Esso non ha un regime tonale ben definito, e non esiste una scala o un sistema ben definito, ma esso è un sistema che cambia. Sentiamo molto le scale che sentirà Debussy.

Anche il brano di Grisey Partiels. Ed i suoni non fanno piú capo ad una scelta scalare, ma fanno anche capo ad un continuum.

### Timbro

Da un punto di vista timbrico
![timbro](timbro.png)

- coadiuvante morfologico, Sconforto di Strauss alla Sagra della primavera quando ascolta il fagotto nel registro clarinettistico. La maggior parte delle volte l'orchestrazione classica rispetta i registri.
- coadiuvante sintattico, come i soli due toni a cui sono intonati i timpani
- coadiuvante retorico, andamento retorico che impone un certo tipo di scelte
- funzione semantica, con ad esempio il Pierino e il lupo
- il timbro inizia a far parte del "gioco strumentale", con Berio, Sciarrino, fino a divenire generatore della funzione armonica, dal timbro posso generare un sistema
- Funzione armonica
- Klangfarben melodie, con brano Farben, congelare altri paramteri, partendo solo dalla successione di timbri diversi, con la differenza tra un do e un mi è di altezza

_[Ascolto di Farben di Schönberg](https://www.youtube.com/watch?v=tFT6NIYMF1I&ab_channel=fanclois)_

Periodo atonale in cui convivo codici come quello contrappuntistico e polifonico e vi è una necessità di convivere con codici precedenti.

Insistere molto sulla polifonia ed il contrappunto, poichè è un codice che è resistito sempre.

In Sciarrino il timbro diviene la casa della composizione e le altezze stesse rientrano nel timbro, ed alla totalizzazione di un approccio verticale e non ci viene mai di orizzontalizzare il discorso in termini melodici.

### Durate

- coadiuvante della sintassi tonale, se c'è una nota lunga, c'è un accordo importante, e con il sistema polifonico ed il contrappunto, le durate hanno subito avuto un impulso immediato.
- procedimenti retorici
- codici ritmici
- codici metronomici ed agogici
- codice delle proporzioni binarie e ternarie
- Modelli strutturali(Messiaen serializza le durate, mette i codici tutti sullo stesso livello, supercodice nel quale lui inaugura lo strutturalismo integrale e che sposta la composizione su qualcosa di molto meccanico)

_[Ascolto della Sagra della primavera di Stravinsky]()_

Analisi dell'aspetto ritmico sulla Sagra della primavera di Boulez, vi è anche un video divertente con Boulez e Stravinsky.

Roman Blande dice che l'accordo non è importante, ma a lui serviva un materiale sonoro per dar corpo al ritmo, e dunque se cambiamo il timbro, riconosciamo sempre la sagra della primavera, ed il ritmo può prendere il sopravvento. Grandi autori per evidenziare questi aspetti paralizzano gli altri codici, ci si ferma e viene ripetuto 40 volte il timbro, e tutti i parametri degli altri codici sono paralizzati.

Bolero di Ravel è sempre uguale ed il codice che bisogna seguire è la dinamica. Ed anche l'adagietto della settima di Beethoven, blocco i parametri per farti ascoltare le dinamiche.

_[Ascolto di 4 studi sul ritmo di Messiaen](https://www.youtube.com/watch?v=S3xEnDpM1mU&ab_channel=ContemporaryClassical)_

La difficoltà è cercare di comprendere quali siano gli appigli, cosa riesco a percepire? Cerco di mettere insieme le cose che stanno bene insieme, come i registri e le dinamiche.

Vi è un integralismo e la scelta del pianoforte non ti può neanche tirare dentro.

### Dinamiche

- coadiuvante dei codici notali o di altri parametri
- modello strutturale

### I messaggi e le tipologie

- messaggi numerici
  - linguaggio
- messaggi analogici(anche le dinamiche, intensità e durate)
  - atteggiamento psturale
  - mimica facciale
  - gestualità
  - prossemica(distanza fra due persone che comunicano)(20 cm)
  - segni paralinguistici

![mess](mess.png)

Anche la distribuzione dell'acusmonium e della musica acusmatica, degli speaker.

I segni analogici hanno normalmente più significato dei messaggi numerici. Inizio della seconda di Malher in cui vi è Fra Martino Campanaro in minore, che accompagnato da quel tipo di strumentazione, il messaggio cambia.

Sherazade di Stravinsky con linguaggio e messaggi analogici che pesano su una scrittura come in accattone di Pasolini.

Anche per un attore è molto importante il movimento ovviamente:
![act](act.png)

![emozioni](emozioni.png)

_[Ascolto dell'ottava incompiuta di Schubert](https://www.youtube.com/watch?v=0mnrHf7p0jM&ab_channel=tnsnamesoralong)_

Ci arriva l'andamento gestuale della musica.

_[Ascolto dell'aria di Papageno dal flauto magico di Mozart](https://www.youtube.com/watch?v=XAiBdDJexLA&ab_channel=PaulHardwick)_

Vicino ad esso

_[Ascolto Raga indiano Adbutha]()_

Tutte e due identificano gioia

Ma ad esempio nella partitura di Bussotti, A quattro mani, vi sono dei segni che ci portano ad interpretare dei segni analogici.
![bussotti](bussotti.png)

O anche il fontana mix di Cage che mi da indicazioni di massima, e vi è un'attenzione verso un codice analogico.
![cage](cage.png)

O della gestualità nella pittura fatta ad esempio da linee in una battaglia rappresentata da Paolo Uccello.
![Uccello](uccello.png)

Che confrontato con un liguaggio diverso, ci fa intendere messaggi analogici simili.
![guernica](guernica.png)

Sciarrino cerca di cancellare la parte digitale per lasciare viva la parte analogica.

Cromatismo esasperato fatto di un semitono discendente, è interessante come viene a far parte anche il paesaggio sonoro dell'epoca, come i segnali telegrafici, che entrano a far parte del bagaglio painistico.

_[Ascolto di Vers la flamma di Skrjabin](https://www.youtube.com/watch?v=MueioLajS2E&ab_channel=arciduca31)_

Brano con linearità e pulizia analogica.

Importante segnalare il blocco di alcuni codici, e la crisi del sistema tonale ed è evidente come al di là del linguaggio utilizzato, l'attenzione viene catturata dal gesto analogico che va dal piano all'acuto e dal grave al forte. Con un gesto radicato nell'analogia piú che nel linguaggio.

Esempi come il _Cadenzario_ di Sciarrino, in cui il gioco è ironico e lui raccoglie le cadenze piú importanti e belle della tradizione che monta con un aspetto analogico che chiama **forma a finestra**.

_[Ascolto del cadenzario di Sciarrino]()_

In maniera piú raffinata usa le forme a finestre con Efebo con radio. Con la sintonizzazione e dunque il passaggio da un linguaggio all'altro.

_[Ascolto di Efebo con radio di Sciarrino](https://www.youtube.com/watch?v=JY_C_nYt7lQ&ab_channel=tortellinototoro)_

Per mettere in evidenza che l'approccio alla contemporaneità non è più solamente legato al linguaggio, ma vi sono anche i meccansimi percettivi.

**Il compositore è ora in grado di usare codici diversi nella stessa epoca**, e forse è solo nella musica ciò è possibile.
__________

Vedremo i meccanismi percettivi e poi faremo analisi.

Eco -> passegiate in boschi inferienziali, ovvero dare indizi, suscitare una curiosità e sperare che l'esecutore faccia qualcosa.
