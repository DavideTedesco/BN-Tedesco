# Appunti della lezione di Giovedí 12 Novembre 2020

- mandare analisi

## Visione delle varie analisi del brano di Webern

Viviamo in un momento in cui i rapporti contano di più di quello che si produce.

Ma l'aspetto di quello che uno scrive è anche importante, poichè viviamo di scrittura.

Modo per superare errori e grammatica sbagliata, leggendo.

Fare elenco di testi letterari che vanno letti come software operativi per la scrittura.

Leggere ad alta voce, è il modo migliore per evitare di mettersi a studiare alcune cose.

- Tra tutte le cose cercare di capire qual'è la cosa che conduca il brano.

**Il massimo di precisione spesso coincide con il massimo della libertà della scrittura**

In un'analisi bisogna cercare di fissare un parametro e vedere se tutti gli altri confermano quel parametro.

**Noi sappiamo trovare quello che sappiamo cercare** e siamo condizionati da quello che sappiamo.

Elisione tra battuta 1 e 2!

In alcune opere è evidente che ogni elemento è una piccola forma musicale all'interno di un'altra forma grande che le contenga tutte.

La scelta delle note diviene quasi superata ed è importante altro, come la dinamica.

Webern inoltre procede per blocchi e potrei anche spostarli in maniera diversa, scrivendo un pezzo nuovo.
![blocchi](blocchi.png)

Ma da un punto di vista della consequenzialità, non cambierebbe nulla.

È già una struttura all'interno di fasce sonore, che dal punto di vista della percezione, è una risonanza.

Esiste una narrazione anche senza dire nulla.

_Lettura di un brano dal testo di Beckett "In nessun modo ancora"_ (trasposizione musicale del brano di Webern con un senso che dai tu, ma che non è per niente esplicato dalla narrazione stessa.)

Girare su stessi, ma facendo un percorso.

La narrazione devo cercare dunque di coglierla.

Questi brani qui sono molto particolari poichè il primo ha riferimenti nel passato.

![ipip](ipip.png)

![cod](cod.png)

Procedendo cosí otteniamo:
![cat](cat.png)

Corrispondono questi codici a quelle che in psicologia sono definite come le espressioni delle nostre idee, manipolerò dunque tramite idee quello che ho e che ascolto.

Il codice verbale si riferisce a un qualcosa di esecutivo.

Questi sono aspetti importanti, poichè la possibilità di scrittura di un brano possono convivere anche codici diversi.

Come in questo brano
![stru](stru.png)

Un codice di questo genere è impermeabile anche ad una percezione visiva.

![kreuz](kreuz.png)

Invece il Klavierstucke XI è un qualcosa che descrive, anche refrains, Haubenstock in Ramati Credentials.

Il gruppo di improvvisazione di Nuova Consonanza.

Kagel brano con registri mossi non dall'organista.

_Ascolto di parte di Improvisation Ajioutèe di Kahel del 1962_

![Kagel](Kagel.png)

Qualche volta la partitura tradizionale definisce anche aspetti e direzione

Qualche volta la trasformazione è anche intuibile all'interno di una partitura tradizionale.

Dare il massimo che si può con poche cose.

L'approccio esecutivo condiziona un esecutore.

La fluidità di un brano è data anche dalla scrittura

____________
Manca la storicizzazione degli elementi elettronici.

**L'enfatizzazione dei silenzi è difficile per gli esecutori** non riescono a governare il vuoto per una sala da concerto.

Guaccero diceva "quando fai il silenzio è come se rimandassi la palla all'ascoltatore", l'ascoltatore può allora:
1. ricordare ciò che ha sentito (riflessione)
2. prevedere ciò che avverrà (previsione)

Gestalt -> regola del destino comune

____________

Confrontarsi con un altro brano e segnali diversi possono arrivare da direzioni diverse come i 3 pezzi per quartetto d'archi di Stravinsikij li confrontiamo con il primo brano di questi di Webern:

cose sorprendenti con un modo diverso di intendere la musica

1. prima l'ascolto
2. inizio di un'analisi
