# Appunti della lezione di Giovedí 5 Novembre 2020

Attualizzare le metodologie storiche dell'analisi e utilizzare le metodologie attuali per un'analisi storici.

## Attualizzazione delle tipologie di analisi

Obiettivo: dare una lettura attualizzata di quello che è il linguaggio che viene usato per l'analisi

### Indicazioni sul sistema tonale

Esso possiede una serie di caratteristiche che possiamo trovare nei linguaggi contemporanei, ed è necessario vedere brevemente questo sistema.

Siamo abituati a vedere che i meccanismi armonici siano la base del sistema tonale, ovvero queste sovrapposizioni per terze, ci dimentichiamo però che il sistema tonale nasce dal movimento polifionico delle parti del contrappunto.

Per anni la musica è andata avanti con gli organa(plurale di organum) complicando sempre di piú la scrittura contrappuntistica, che non tiene conto degli incontri verticali che si vengono a creare, e tutto avviene orizzontalmente.

Viene tutto scritto sul pentagramma e vi è una mancanza di verticalità(fino al '600 quando comincia a prender piede una sensibilità verticale), che governa tutto in maniera orizzontale.

Quali sono queste regole che governano tutto in maniera orizzontale? Lo vedremo con la Gestalt.

La cosa principale era far vedere l'indipendenza di queste voci, che cerca di far vedere quello che gli arriva e lo divide in blocchi.

Il nostro orecchio fa un'operazione di smistamento, ricostruendo lo specifiche voci, ed il bravo contrappuntista, sa andare incontro agli smistamenti e sa usare questo funzionamento dell'orecchio. Sapendo mettere in crisi ed utilizzare a suo favore le linee melodiche.

### Perchè nasce l'attenzione alla verticalità
- La sensibilità verticale nasce poichè si pone il prblema per suonare le note o l'accordo sotto la linea melodica
- la musica si accorge che non ha una grammatica di base per poter concludere una frase
- nasce anche l'utilizzo della cadenza(alterando il settimo grado per far risolvere sulla tonica)
- tutto il modello compositivo prima della tonalità si basa sulla modalità, e vi sono alcuni modi che vivono anche senza la distanza di semitono dall'ottavo suono.



### Il contrappunto
![contra](contra.png)

Quando inizi a realizzare il contrappunto, iniziare a pensare al canto dato non con i gradi con i numeri romani. Tutti gli intervalli che si incontrano devono essere consonanti. Vi è inoltre una regola della Gestalt _principio del destino comune_, in cui se faccio muovere in direzione diverse due note riesco a distinguere il loro destino.

Una volta capita la _nota contro nota_ inizio a intendere e utilizzare _due note contro una_ e dunque una nota che per grado reale, passa da una nota ad un'altra.

Iniziamo a teorizzare dunque che i trattati d'armonia, sono stati dedotti dai grandi autori.

A un certo punto quando mi pongo il problema verticale, il problema diviene come muovere bene le parti, e le settime e le sensibili si risolvono da sole.

Se andiamo a vedere come muovono le parti Ligeti e Stockhausen, vediamo che le parti funzionano, perchè seguono i principi contrappuntistichi.

Il contrappunto è una sorta di organizzazione di flussi, alla cui base i meccansimi sono gli stessi del contrappunto tradizionale.

Il grande miracolo che accade in Bach è legato al fatto che per la prima volta, 4 flussi, o 4 parti contrappuntistiche, funzionano perfettamente verticalmente.
![tesoro](tesoro.png)

Per la prima volta il movimento delle parti orizzontale funziona bene come il movimento delle parti verticali.

![ab](ab.png)
Cosa ascoltano le orecchie e cosa sentono.

![partita](partita.png)
Se realizzo un barriolage, un discorso monodico diviene polifonico.


_Esperimento_
In cui vi sono due canali che mandano due segnali diversi che sono nella stessa ottava che a un orecchio arrivano a distanza di terze, e nell'altra ottava, e percepiamo
![espe](espe.png)

Parte per pianoforte di Rachmaninoff in cui il pianista suona un qualcosa e ne sente un'altra
![rach](rach.png)

_Libro Il cervello musicale consigliato da Giovanni di Daniele Schön_

### Armonia e armonizzazione

Tutto ciò che riguarda l'armonia è qui dentro:
![carica](carica.png)

Accordi con carica dinamica, statica e di transizioni.
- carica dinamica 0 -> tonica
- carica di transizione 50 -> sottodominante
- carica dinamica 100 -> dominante

IL percorso puó essere svolto solo in senso orario.

Se sostituisco a tonica, sottodominante e dominante, gli accordi del sottogruppo, posso scrivere qualsiasi cosa in tonalità:
![sottogruppi](sottogruppi.png)

Meccanismo di tensione e distensione che funziona in questo modo fisico.

Se vado a vedere i gradi sotto il corale di Bach, ho lo schema "sconcertante" descritto secondo le cariche dinamiche descritte sopra.

_[Ascolto del preludio e fuga n.8 di Bach](https://www.youtube.com/watch?v=xXwCryMItHs&ab_channel=PaulBarton)_

Ciò è di una chiarezza sconvolgente, e mima un discorso parlato.

Il problema nella storia della musica è che arrivati a Bach, la storia della musica è finita, tutto quello che avviene dopo è ricerca e arrichimento del linguaggio di non ricadere nel già detto. Ed il nodo e la crisi del linguaggio occidentale è tutta nell'armonia.

E la forza della tonalità è il percepire i rapporti di tensione e distensione anche senza sapere nulla di armonia.

E la musica leggera non si stacca dalla tonalità perchè la tonalità permette al mercato di funzionare.

Vi sono stati alcuni momenti in cui si è cercato di scollegarsi dalla tonalità o arricchirla ed ampliarla, come il cromatismo utilizzato da Stravinsikij e i 6 francesi.

## Metodologie dell'analisi musicale

La maggior parte delle analisi, si basano sull'analisi tonale.

![analisi](analisi.png)
### Analisi schenkeriana o Struttura fondamentale

#### Ursatz
Cerchiamo poi di attualizzare l'analisi, in ogni suo aspetto.

_Una composizione tonale, è caratterizzata da una determinata tonalità d'impianto._

_Ciò che distingue la musica d'arte dalla musica d'uso, è basata su una tecnica  di utilizzo del contrappunto_

Ursatz(struttura primordiale) ovvero struttura primordiale schenkeriana basata sull'accordo di tonica, in modo da avere un accordo contrappuntistico.
![ursatz](ursatz.png)

Schenker costruisce 3 Ursatz, da cui deriva tutta la scrittura musicale tonale.

L'idea è basare tutto sull'accordo di tonica.

_Libro di Schönberg in cui spiega come scrivere una melodia in Modelli per Principianti Di Composizione usando l'accordo di tonica verticale e sviluppandolo orizzontalmente_

Posso dunque basare l'impostazione schenkeriana anche per analizzare qualcosa di non tonale.

Come leggere dunque tonalmente l'op.19 di Schönberg?

#### Ma come puó derivare da ciò questa idea dell'Ursatz come fonte di tutta la scrittura tonale?

Vi sono delle tecniche di prolungamento dell'Ursatz:
![prolung](prolung.png)

Ma queste tecniche di prolungamento le potrei usare anche in ambito non tonale.

Esistono dunque 3 livelli strutturali:
![livelli](livelli.png)

Se passo dalla Ursatz posso arrivare a questi esempi:
![esempi](esempi.png)

Usando le sole note dell'accordo.

Posso quindi complicare le cose sempre di piú utilizzando le tecniche di prolungamente e lo complico sempre di piú:
![livelli2](livelli2.png)

Con le tecniche di prolungamento, si riesce a realizzare l'analisi anche di qualcosa di Bach come il Preludio in do maggiore:![bach](bach.png)

In teoria potrei anche non sapere qual'è il percorso armonico, perchè i prolungamente, producono tutto ciò.

Abbiamo dunque trasformato il principio percettivo trasformato in un qualcosa di trattatistico.

I principi di Schenker hanno una base, ovvero, anche un brano musicale puó partire ed avere un aggregato armonico dal quale viene prodotto tutto.

Viene in mente la Sagra della Primavera, che l'accordo dei ritmi, sia la nota generatrice di tutta l'opera. Possiamo quindi pensare ad un nucleo generatore, dal quale realizzare tutta un'opera ed è intrigante l'idea che si possa partire da un accordo e generare tutto.

**Cercare di prendere le varie tecniche di analisi e capire che cosa ci dicono singolarmente, ma anche incrociare le varie analisi.**

_Verificare inoltre che cosa all'ascolto mi colpisce e poi verificare con l'analisi_

(Molti musicisti non amano l'analisi, poichè non da informazioni all'analisi)

_Articolo di Egidio Pozzi sul ruolo dell'analisi, che fa un discorso molto evoluto, senza realizzare un discroso esistenziale - L'intuizione dell'esecutore..._

_Trattato di Mouse sulla Magia che descrive che portiamo delle tracce primordiali_

(Libro, il pensiero del cuore di Gaita tre cose di cui non si può parlare: il simbolo, l'inconscio e la musica. Quando ci avviciniamo a queste tre cose dovremmo riportarci all'ignoranza.)

Ma l'analisi a cosa serve?

_Birckoff si pone di realizzare una formula per valutare il valore di un'opera d'arte (ordine/complessità)_

### Elaborazione tematica di Rèti

![reti](reti.png)

Qui vi è un atteggiamento diverso, in cui la composizione è una sorta di improvvisazione che mi da un'idea di qualcosa anche di piú istintivo di quel qualcosa di Schenker.
![appa](appa.png)

Prendendo gli sviluppi, vediamo che è sempre lo stesso motivo ed è tuttp basato su un'idea iniziale che viene in mente al compositore.

Con questo tipo di analisi è piú semplice fare costruzioni di tipo tonale.

Pensiamo spesso che la serie sia un tema nella dodecafonia, ma essa è solo una scala, e all'interno della serie mi ritaglio un tema.

Il brano musicalè è che con quelle 12 note, si generano altre cellule restando con il rigore dato dalla serie dodecafonica.

Sono importanti gli intervalli che devo mettere e non i suoni singoli.

- Berg -> metto tutte terze
- Webern -> uso solo semitoni

Poi all'interno della serie mi taglio cellule che vengono poi sviluppate successivamente.

E vedere poi come delle cellule melodiche generino altre cellule melodiche.

(Bracci studiava con Sylvano Bussotti)

Non ci sono dubbi che il brano Syrinx di Debussy, che vi sia un modello o tema che venga utilizzato durante tutto il brano, e vi è unque un pensiero improvvisativo, con alternarsi e variazioni degli elementi a e b.
![syrinx](syrinx.png)
![syrinx2](syrinx2.png)

Questo brano di Debussy è generativo e fa intendere i rallentamenti e le cadute a battuta 10.

_Ascolto di Syrinx di Debussy_

Brano difficilissimo da eseguire, e devi essere molto bravo a dare il tuo tempo e le pause hanno un ruolo importantissimo.

C'è un gioco di tinte e dinamiche.

Viene messo in campo qualcosa con battito cardiaco dell'ascoltatore e una velocizzazione del discorso.

Vi è poi a battuta 15 un'intensificazione e il punto a battuta 25 con un trillo confuso e accellerato con poi una discesa.

L'idea è che sia una improvvisazione su carta.

I grandi flautisti fanno questo brano e density come bis
### Analisi funzionale di Keller

### Analisi fraseologica di Riemann
![rie](rie.png)

Vi è un passaggio intermedio tra picchi, distensioni e espansioni; quella che nella musica contemporanea viene definita densità, ed il processo di estinzione ed espansione puó dilatare, come l'elisione, l'iterazione, etc...

_Ascolto dell'inizio di Gruppen di Stockhausen_ in cui vediamo anche visivamente la densità come aspetto che realizza passaggi tra picchi.

Dal punto di vista gestaltico sentire le tre linee delle 3 orchestre, viene fuori che le idee  possa farle sentire maggiormente con la spazializzazione, ovvero usando il principio di vicinanza, e questo è uno degli aspetti piú importanti ai fini della costruzione musicale.

### Analisi Morfologica

3 principi fondamentali per a costruzione musicale:
1. ricorrenza
2. contrasti
3. variazione
![morfo](morfo.png)

Non bisogna tradire le imposizioni date dal compositore per quanto riguarda un brano. Quindi bisogna avere rispetto del compositore.

C'è una sorta di analisi del modo di scrivere di Satie, con un metodo chiamato a mosaico. Ovvero costruendo tutte cellule denominate con le lettere e poi le giustappone mettendole in sequenza in maniera che non si capisce come siano giustapposte, e monta il pezzo in questa maniera quasi cagiana. Vi è solo una logica casuale.

Per l'analisi morfologica si analizzano sezioni di brani.

### Analsi per parametri e per tratti stilistici

Analisi di tipo sincrono che la tratta come un esempio di stile in rapporto a vari stili o come anticipa stili successivi(Aspetto molto musicologico).

![para](para.png)
![stat](stat.png)

### Semiologia musicale
![semio](semio.png)

Essa è fatta di diagrammi ad albero che permette di verificare quali siano gli elementi che scompongano una frase.

![albero](albero.png)

gioco di montaggio e rimontaggio

### Teoria dell'informazione

![info](info.png)
teoria non formulata come regole comunicazionali, ma con la relazione che hanno eventi fra loro, in base al codice che utilizzo.

Un codice con grandi possibilità di scelta è altamente informativo, un

____________

Per la prossima lezione

Terzo pezzo dell'op.11 di Webern, ascolto e descrizione del brano.
_Ascolto del Terzo pezzo dell'op.11 di Webern_
![web](web.png)

Lasciarsi guidare dal brano:
- non vi è che un solo momento di pausa
- dialettica tra violoncello e pianoforte
- aspetto dinamico -> tensione e distensione
- chiedersi quanto sia importante l'altezza delle note
- rapporto tra le note quanto è importante?
- aspetto fraseologico -> con cellule melodiche -> melodicità di una sola nota

Realizzare un'analisi in grande libertà, esplicitando l'idea in cui l'opera prende corpo.

Rimanere in una struttura e brevità che è un pregio incredibile.

Con questi brani concentrati ci avviciniamo sempre piú ad un'idea.

_Raccolta alla fine del corso di brevi analisi realizzate_

Se posso partire da una tesi, la tesi va dimostrata!
