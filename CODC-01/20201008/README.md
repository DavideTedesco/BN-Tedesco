# Appunti della lezione di Giovedí 10 Ottobre 2020

L'analisi è un mondo molto vasto e per dare un ordine a ciò che andremo a fare. Importante capire su quali basi poggiarsi.

L'analisi è la base anche per le nostre composizioni, qual'è la finalità dell'analisi.

Da compositori rispetto a musicologi l'analisi è sicuramente diversa. Difficolta della musicologia per entrare nella fenomenologia della musica.

L'analisi della musica tradizionale, si corre il gusto dell'indagine fine a se stessa che fa perdere l'obiettivo ovvero: arrivare all'ossatura di un brano per arrivare a capire quali sono i meccanismi che l'hanno prodotto.

La difficoltà che si trova in un corso di analisi è quella di dover scegliere tra:
- fare una storia dell'analisi musicale
- fare un'analisi vera e propria

Dunque quali metodologie possono esserci utili per analizzare un brano?


## Programma

Ripercorrere i metodi dell'analisi musicale, e storicizzando le metodologie usate per l'analisi contemporanea.

Il taglio per dare a un certo tipo di analisi è un taglio compositivo che predilige l'approccio completo dell'analisi musicale.

Discorso della semiologia, ogni brano entra in un discorso comunicativo, in cui c'è un'emittente che produce un lavoro ed un ricevente che ascolta.

## Organizzazione lezioni

In base alle esigenze e poi anche per delle lezioni individuali.

## Come si evolve un meccanismo comunicazionale?

Powerpoint che illustra un primo approccio alla semiologia. Ed in caso approfondire con dei testi richiesti al maestro.

## Elementi basilari della comunicazione

Discorso basilare sul segno linguistico, che parte dalla linguistica e poi è stato assorbito dalla musicologia.

Andiamo a ricercare il segno, perchè molte delle difficoltà che abbiamo è nel segno linguistico.

Abbiamo il **problema del Contenuto** che viene esplicato da Saussure.
![ferd.png](ferd.png)
![ferd2.png](ferd2.png)


![segno](segno.png)

cane è immagine acustica e concetto, essi sono incollati ed insieme identificano il segno

Il suono è il "cane" e il significato è l'animale.

Un concetto è un'immagine acustica, per noi è molto legato ciò, perchè ad ogni immagine acustica corrisponde un concetto.

Noi viviamo di immagini acustiche ma non abbiamo un corrispettivo concettuale (non sappiamo dire cosa sia un certo tipo di intervallo), ciò ha portato a studi di Gino Stefani che prende in considerazione ogni singolo intervallo, per dimostrare che ad ogni intervallo vi è un significato attaccato. La sesta maggiore è un intervallo che identifica gioia, poichè l'uso che ne è stato fatto gli ha fatto assumere una caratteristica di immagine acustica. Gli intervalli dunque si legano a immagini acustiche e concetti, anche se siamo competenti, ma non coscienti di ciò che usiamo.

È importante aver legato concetti ad immagini acustiche.

Film "Anna dei miracoli" che è la storia di un'infermiera che si prende il compito di seguire una bambina sordo-muta-cieca che non ha contatti con l'esterno, perchè le difficoltà che si trovano sono come poter associare un suono (immagine acustica) ad un suono.

Capire che emettere un suono, che avesse un significato legato a qualcosa di esterno è stato importante per la storia dell'uomo.

Noi compositori viviamo di immagini acustiche e ció porta con se un concetto.

La difficoltà che spesso abbiamo è lo scoprire il concetto e quale immagine acustica viene fuori da una composizione.

L'idea che si possa legare un concetto ad un'immagine acustica, è un concetto che porta all'astrazione. Scrivere la parola cane su un foglio, posso scrivere anche segni che mi portino all'astratto, come la parola "dio" che si lega ad un concetto astratto.

Se tutta la società occidentale ha prodotto astrazione è perchè abbiamo potuto astrarre dai segni. La grande differenza dai cinesi è che se abbiamo una differenza è che se scrivo una casa, disegno la casa. Uomo importante uomo con cappello importante. Il segno cinese è dunque analogico.

Il nostro segno è portato di più all'astrazione, perchè possiamo parlare di cose che non esistono. Come disse Eco.

### Arbitrarietà

Non c'è scritto da nessuna parte che se scrivo cane esso è scelto in qualche modo, cane è scelto arbitrariamente.

Chi realizza musica applicata sa benissimo cosa voglia dire realizzare un'immagine acustica, perchè poi le possiamo usare come segni(suono spade laser di Star Wars) un suono viene associato ad un concetto(spade laser legate al combattimento).

Atonalità legata all'horror, essa è legata a qualcosa che non conosciamo, e dunque all'horror(ciò è scelto arbitrariamente).

Quando alla fine del Don Giovanni la statua torna, Mozart fa cantare 12 note tutte diverse una dall'altra, dunque non è tonale, quindi non viene da questo mondo.
Per Mozart un carattere canta in una maniera specifica, e quando si mescolano i caratteri si mescolano i canti.

L'uso che si fa di un certo tipo di musica lega e incolla un certo tipo di musica ad un significato.

 L'interpretazione che ognuno di noi da, non è controllabile, ma ciò rappresenta la musica, ovvero l'associazione ad un tipo di musica di significati dipende da ogni ascoltatore.

 La nascita del canto gregoriano è legata al fatto che i concetti non sacri si erano legati a brani sacri.

 Negli anni '60 la semiologia era molto in voga, e ci si scherzava molto (sketch di Achille Campanile in treno sul sarchiapone), intavolare un discorso su un qualcosa che non si sa, inventare un'immagine acustica ed associarci un concetto.

 Noi compositori giochiamo con le immagini acustiche, che cosa c'è dietro le immagini acustiche dunque?

 La divisione tra immagine acustica e concetto (espressione e contenuto), puó essere ancora suddivisa secondo Hjelmslev:
 ![hjem](hjem.png)

 C'è dunque un principio d'ordine, ovvero un meccanismo per andare sempre piú in profondità, con concetti di sostanza e forma, ovvero che ci sia una sostanza che dia forma.

 ### Che cos'è un codice?

 Se riempio una pagina di lettere arbitrariamente, quella è comunque una comunicazione, ma il problema di quella comunicazione ha un livello di disordine, di **entropia** altissimo. Quella pagina è dunque molto informativa, però da un punto di vista comunicazionale quella pagina è molto poco informativa. Quest'entropia per divenire comunicazionale devo introdurre un codice, se introduco un codice(come quello della lingua italiana) fa abbassare le possibilità di comunicazione ma fa alzare la possibilità dell'intendere di ciò che è scritto sulla pagina.

 Innalzo talmente tanto l'entropia che essa stessa diviene l'ordine. La tonalità è un codice che si sovrappone all'atonalità, abbassamento proprietà comunicazionale ed innalzamento di entropia.

 La musica contemporanea è stata ed è legata anche al codice che la società gli propone.

 (Corrente di insegnamento ai bambini di semitoni e quarti di tono fin da subito)

 In mano a grandi compositori il sistema tonale diviene eccelso, quando con gli elementi giochi sull'attesa per l'ascoltatore. Ciò ha portato al cromatismo, alla saturazione ed alla atonalità.

 Noi (compositori) siamo dalla parte dell'immagine acustica e poi abbiamo degli ascoltatori che sono dei concetti.

 Immaginare la difficoltà che si è avuta con la musica concreta ed usare materiale che aveva già un concetto ed un segno ed ha provato a staccare l'immagine acustica dal segno.

 Acusmatico nasce dall'ascolto senza riferimento a chi lo produce

Le prime realizzazioni di musica elettronica e concreta si legano a concetti.


Barthes dice che quanto prendiamo un **segno denotativo** uniamo un significante a un significato, ma questo diviene un segno che puó avere un altro significato, questo nuovo segno si chiama **segno connotativo**.

![barthes](barthes.png)

Se prendo un telo con 3 colori che lego (bianco, rosso e verde), questo è la bandiera che rappresenta l'italia, ma questo segno puó divenire anche "l'amor di patria", si puó legare un altro significato ad un segno, quasi come un parassita.

Simbolo pallone -> calcio -> propria squadra -> amore propria città -> etc...

Entra un apporto fortissimo che ha il fruitore, perchè il contenuto di quello che vado dicendo ha anche l'apporto dell'ascoltatore.

Questo aspetto del segno denotativo e connotativo puó essere ormai cristallizato nella cultura di un popolo e comportare significati piú individuali, significati legati da esperienze personali.

Questo aspetto è fondamentale, perchè quando utilizzo o scrivo dei segni posso collegarli fino ad un certo punto, perchè l'ascoltare lega comunque un proprio significato ad ogni segno come anche l'esecutore.

L'appartenenza a una cultura significa anche avere competenza di una cultura.

I significati connotativi per gli ascoltatori sono sempre piú difficili da descrivere ed ognuno da un significato ad ogni parola e suono.

Teoria del: "tesoro stanno suonando la nostra canzone"

Usiamo dei linguaggi che si consumano, e molto spesso quello che interviene è il fatto che la connotazione sia offensiva.

Come far risaltare simboli che hanno assunto significati diversi? Svastica come simbolo del sole etrusco prima dell'emblema nazista.

Nel tempo le connotazioni hanno nascosto il significato originario di una parola.

## Schema forme simboliche secondo Molino/Nattiez

Gli studi sulla comunicazione partivano in genere da un qualcosa di scientifico come il telefono o il telegrafo.

![nattiez](nattiez.png)

Discorso di Shannon e Wivers, mando un messaggio al telefono, il telefono trasforma il codice e porta il segnale elettrico che viene poi decodificato, il destinatario era quasi passivo.

Ma su studi di comunicazione e non prettamente scientifici, si scoprí che il destinatario recepisce in un certo modo.

### Emittente
Esso manda un messaggio in un certo modo attraverso un **processo poietico**, ed esso rimane a livello neutro.

### Destinatario
Ma il destinatario deve porre un **processo estesico** per poter comprendere un messaggio.

Possiamo dunque attuare 3 tipologie di analisi:
- analisi poietica
- analisi del livello neutro
- analisi estesica

### Analisi livello poietico
In un libro di Umberto Eco egli fa vedere come anche per l'emittente il processo poietico sia diverso dall'analisi fatta dalla stessa emittente.

### Analisi livello neutro
Il livello neutro è un livello per la musica elettronica problematico, poichè se essa si pone su nastro, poichè nella musica non ho una partitura, dunque ho condizionamenti del nastro. (Quadro che rimane nella stanza) L'oggetto in se lo hai nella partitura non nel nastro.

L'analisi del livello neutro deve essere un qualcosa di asettico. Essa può essere realizzata solo come analisi tecnica. Analisi del livello neutro è anche quella specie di cronaca che viene fatta nell'analisi del brano.

Schönberg evitare nel processo poietico la tonalità usando la dodecafonia.

La creazione della partitura dell'ascolto nasce dalla necessità che mi da solo segnali cronologici del processo che accade, questi sono rapporti interni non poeietici, ma interni.

### Analisi livello estesico
Il processo estesico è molto in alto mare, ed esso è il fatto che il messaggio si passa con una sorta di negoziazione del concetto.

**La comunicazione è un miracolo quando avviene**

Parto da mille cose derivate dalla cultura in cui vivo.

Le ultime correnti dell'analisi fanno molto capo all'ascolto. Schaeffer dice che "bisogna spostarsi dalla prassi esecutiva alla prassi dell'ascolto".

Il rapporto difficile che abbiamo con l'ascoltatore è dovuto dalle difficoltà dell'ascolto della musica contemporanea ma anche ai mezzi di massa.

La figura dell'ascoltatore non è secondaria ma questo conflitto con l'emittente ce l'abbiamo.

**Quando non comprendiamo una cosa, forse ci manca qualche competenza per poterla comprendere**, ma l'uso e la frequentazione di un repertorio puó aiutare molto.

Per la musica rock, nel meccanismo compositivo si è innescato un legame con musica contemporanea, suite da 15 minuti, legame con musica elettronica; vi è stato molto contatto dunque.

Composizioni musicali molto evolute, creano una difficoltà di vendita sul mercato; si innesca un meccanismo molto difficile da attuare.
La lotta andrebbe portata con l'educazione musicale nella scuola, ci sono linguaggi diversi ed essi possono essere compresi. Ci si può avvicinare alla musica anche con dei prodotti culturali di livelli piú elevati.

**I sistemi con cui si analizzava la musica, non funzionavano piú e dunque si è cercato di trovare modi di analizzare l'umano e la psicologia**, la scoperta di questo tipo di analisi è applicabile anche alle opere del passato.

Disegni di Kandinsky che suscitano una reazione che il punto, la linea e la superficie creano delle certe dinamiche.
![kand](kand.png)

Vediamo come una crocifissione sia drammatica per la composizione dell'immagine.
![rubens](rubens.png)

I meccanismi che entrano in gioco sono uguali a Kandinsky, ma sono velati dall'immagine davanti.

Guernica e quadro di Paolo uccello con linee uguali ma uno con linguaggio astratto e l'altro con linguaggio figurativo.

Parliamo anche noi con linguaggi astratti(il parlato)(digitale) e linguaggi analogici(i gesti). Il legame tra parlato e gesti crea un altro significato.

Punto, linea e superficie è il punto di partenza dell'arte contemporanea.  

### La musica come oggetto comunicazionale

Applicazione alla musica che Jakobson ha desunto dalla teoria delle comunicazioni applicandola alla linguistica, dividendola in funzioni:

![funzioni](funzioni.png)
Parliamo di **funzione fàtica**, che ha uno scopo(ad esempio il ballo), una tipologia di funzione del genere deve avere caratteristiche specifiche.

**Funzione emotiva** fa esprimere la soggettività.

**Funzione conativa** porta un messaggio (tamburi in battaglia), si sfrutta questa spinta, come anche l'eroica di Beethoven.

**Funzione referenziale** che si riferisce a qualcosa di esterno (inno italiano).

**Funzione metalinguistica** quella che vi è nei messaggi che parlano di se stessi. Come Stockhausen, in Studio II, mettere in evidenza il procedimento come si scrive il brano. Bach clavicembalo ben temperato, Schönberg primi brani dodecafici.

**Funzione poetica o estetica** elaborazione del messaggio per se stesso, modo in cui si dicono le cose, sono piú importanti dei messaggi stessi.

La differenza dei cantanti dipende dalla funzione e da ciò che vuole ottenere. Anche se in alcuni cantanti, opere etc... si possono condividere alcune funzioni.

Citazioni di Malher all'interno delle sue sinfonie, si hanno funzioni referenziali.

Quando ci mettiamo davanti ad un brano, dovremmo capire quali sono le funzioni e capire come un brano è stato destinato e perchè è scritta in un certo modo.

Il gruppo di appoggio che si usava nelle grandi band rock aveva una funzione fàtica, poichè preparava al gruppo rock.

Queste funzioni possono anche esserci tutte insieme.

Normalmente partiamo da una funzione poetica o estetica. L'importante è fare una rielaborazione per trasformarlo in un brano autonomo, la forza dell'autore trasforma l'input in musica.

Questa griglia di funzioni ha risvolti nei metodi compositivi.

Libro: Levi Strauss - Il crudo e il cotto

Parla di dodecafonia, e parla di funzione metalinguistica sopratutto in Bach, Schönberg e Webern.
Anche da un segno molto semplice come _crudo e cotto_ si puó derivare il pensiero molto complesso.

Dai segni iniziali molto semplici che mettiamo in gioco ne deriva un messaggio anche molto complesso.

__________
Consiglio su testi che affrontano il tempo e la gestione della forma nel tempo.

Neuroscienze e problema della percezione.

Libro di Pierantoni _La trottola di Prometeo_ libro

Enciclopedia Einaudi

__________
Lezione prossima settimana Giovedí
