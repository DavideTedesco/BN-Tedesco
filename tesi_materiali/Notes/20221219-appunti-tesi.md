# Appunti di tesi del 19 dicembre 2022

Postulati:
- l'elettronica non è attendibile, ma sono attendibili le unità di misura standard MKS
- formato testuale è attendibile con caratteri ASCII e UTF8
- il formato wav o audio non è attendibile

Cosa non ci deve essere:
- restauro del suono

Tesi:
- **come scrivere le partiture** -> la sostenibilità di un lavoro di Live Electronics dipende dalla partitura e non da tutto il resto ->  non vi è una convenzione sulla sostenibilità e vi sono solo indicazioni di chi ha restaurato
- sostenibilità dei lavori elettroacustici
- live electronics con tape, il nastro va restaurato
  - restauro togliendo ad esempio il rumore, si costruisce il filtro che crea il rumore e lo si inverte, cancellando rumore
  - rumore di fondo dei nastri di Live Electronics sono cangianti con sezioni di nastri con sorgenti diverse e rumori diversi e generalizzare una curva di soppressione del rumore è quasi impossibile
  - per ovviare al restauro si potrebbe
  - Musica su Due Dimensioni di Maderna non si può rifare il nastro
  - nel caso di Nono il rumore è accumulato
  - nel caso di Scelsi il rumore va preservato
- Infernal Machine di Luigi Nono, pitchshifter, finestre triangolari
- **raccogliere informazioni dagli esecutori ancora vivi**
  - allegare comunque le risposte all'impulso dei sistemi
Domande:
- come rappresentare un suono in formato testuale?
- file in testo da portare nel formato del momento come convertirli?

---

Inviare la lista dei testi da trovare al maestro Bernardini.
