# Bibliografia da cercare

- [x] Sustainable live electro-acosutic music

- [x] Journal of New Music Research, Volume 30, Issue 4 (2001)
  - [x] Introduction: Preserving electroacoustic music - S Canazza, A Vidolin - Journal of New Music Research, 2001

- [x] The preservation, emulation, migration, and virtualization of live electronics for performing arts: An overview of musical and technical issues - A Bonardi, J Barthélemy - Journal on Computing and Cultural Héritage

- [x] Electroacoustic preservation projects: how to move forward - D Teruggi - Organised Sound, 2004

- [x] Preserving the electroacoustic music legacy: a case study of the Sal-Mar construction at the University of Illinois - AP Cuervo - Notes, 2011

- [x] Toward a Methodology for the Restoration of Electroacoustic Music - Andrea Bari,Sergio Canazza,Giovanni De Poli &Gian Antonio Mian

- [x] RE-WIRED : REWORKING 20TH CENTURY LIVE ELECTRONICS FOR TODAY Dr Richard Polfreman
- [x] Preserving electroacoustic music: implementing a knowledge graph based on the Dorémus ontology - Alexandre Michaan, Clarisse Bardiot, Bernard Jacquemin, Jeanne Westeel, Oudom Southammavong, Daniel Koskowitz

- [x] Electroacoustic music studies and the danger of loss - Marc Battier

- [x] [draft papers SEAM](https://github.com/s-e-a-m/References/tree/master/SEAM-draft-paper)

- [x] ACTIVE PRESERVATION OF ELECTROPHONE MUSICAL
INSTRUMENTS. THE CASE OF THE “LIETTIZZATORE” OF “STUDIO DI
FONOLOGIA MUSICALE” RAI, MILANO) - Canazza, Avanzini, Novati, Rodà

- [x] Audio Objects Access: Tools for the Preservation of the Cultural Heritage - Sergio Canazza & Nicola Orio

- [x] Computing Methodologies Supporting the Preservation of Electroacoustic Music from Analog Magnetic Tape - Niccolò Pretto, Carlo Fantozzi, Edoardo Micheloni, Valentina Burini, and Sergio Canazza

- [x] (Re)discovering Sounds of CCRMA -Towards Computer Music Preservation - Kevin Dahan

- [x] On the Documentation of Electronic Music - Serge Lemouton, Alain Bonardi, Laurent Pottier and Jacques Warnier

- [x] Art and Its Preservation - David Carrier

- [Google Arts & Culture](https://artsandculture.google.com/project/preservation)

- [x] Archiving electroacoustic and mixed music, Significant knowledge involved in the creative process of works with spatialisation - Guillaume Boutard and Catherine Guastavino

- [x] A model for the conservation of interactive electroacoustic repertoire: analysis, reconstruction, and performance in the face of technological obsolescence; The conservation of interactive electroacoustic repertoire; David Brooke Wetzel

- [x] La notazione della musica elettroacustica. Scrutare il passato per contemplare il futuro - Stefano Alessandretti, Laura Zattra

- [Ctrl+s | Conversazioni sulla sopravvivenza della musica elettronica - Federica Bressan](https://www.youtube.com/playlist?list=PLOL28epWcV3d_AEjgeCzJPJkee_T359j7)

- [] Musica e computer, 4 puntate Ideato da Giuseppe Di Giugno e Nicola Bernardini

- [] Ferraris - Documentalità
