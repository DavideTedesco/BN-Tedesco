# Appunti per la realizzazione della tesi di Diploma di Secondo Livello

## Indice
- Sostenibilità oggi
- La Sostenibilità Tecnologica Musicale
- Sostenibilità della Musica Elettro-Acustica
- Conclusioni
- Appendici:
  - i brani trattati da SEAM fino ad oggi
  - esempio di brano sostenibile

## Key points
- preserve masterpieces of Electro-Acustic Music from the last century and from nowdays
- score capable of providing the necessary performance indications to the complete re-construction of the piece
- do not have to use end–user configuration patches using proprietary software and hardware technologies
- do not use of binary and especially proprietary file formats

## Progetto che consta di due tesi di Diploma di Secondo Livello

1. tesi tecnologica
2. tesi musicologica

## Bibliografia
- Bernardini, Vidolin - SUSTAINABLE LIVE ELECTRO-ACOUSTIC MUSIC
