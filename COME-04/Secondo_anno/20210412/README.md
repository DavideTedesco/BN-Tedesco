# Appunti della lezione di lunedì 12 aprile 2021

## Contatore
Avevamo realizzato un contatore che ad ogni clock aumentava...
![con2](con2.png)

![cont](cont.png)

Avevamo visto un contatore realizzato con FLIP FLOP JK.

Per usare un display a 7 segmenti devo usare un decodificatore per display a 7 segmenti

## Realizzazione di un orologio in Falstad

![or](or.png)

Per realizzare un orologio ci servono 3 contatori.

Il contatore dei secondi è composto da due contatori:
- uno in base 16->con porta AND che limita a 10
- uno in base 8 limitato a 6

## Contatore già realizzato come integrato

![conta](conta.png)


## Convertitore r2r

Avevamo visto come convertire un senale da digitale in analogico:
![r2r](r2r.png)

Vediamo che in questo modo con un contatore realizziamo un potenziometro che può andare verso su.

Ma a noi serve un potenziometro che possa andare su e giu:
![pot](pot.png)

Il convertitore è una rete di resistenze che ad ogni bit aggiunge e leva una resistenza.

>Possiamo decidere cosa faccia un contatore, se vada in su o in giú nel verso.

Vi è un integrato che può fa contatore+decodificatore per realizzare un potenziometro a scatti per mezzo di un pulsante.

Vi è inoltre un integrato che permette di realizzare un potenziometro a scatti con contatore e decodificatore al suo interno con un unico tasto.

_Abbiamo visto come usare un circuito digitale come un circuito analogico_

Per realizzare un sequencer MIDI, come quello del MOOG

## Multiplexer e demultiplexer

### Multiplexer
Sappiamo che possiamo passare dei dati sia in maniera parallela che in maniera seriale.

Ho un apparecchio che si chiama multiplexer che mi porta i dati da paralleli a seriale.

Questi circuiti venivano usati per mandare su uno stesso doppino telefonico segnali con tempo diverso.

Ad ogni valore ci scorreranno i valori
![mul](mul.png)

### Demultiplexer
Ho fatto divenire un dato da parallelo in seriale, ora per portarlo in parallelo nuovamente utilizzo il demultiplexer.

![demul](demul.png)

Un applicazione pratica è il circuito all'interno degli hard disk che tramuta un segnale da seriale a parallelo.
