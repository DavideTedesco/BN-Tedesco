# Appunti della lezione di Venerdì 22 Gennaio 2021

Cercare di capire cosa avvenga all'interno di un circuito preciso.

MCP4725 è un convertitore che va ad utilizzare su Arduino un componente presente

Avevamo realizzato l'altra volta un filtro passa basso.

## Filtro Chebyshev in uscita ad Arduino, per filtrare un segnale e realizzare un con I2S un segnale audio

![filt](filt.png)

![cheby](cheby.png)

[chebyshev su Arduino](http://interface.khm.de/index.php/lab/interfaces-advanced/arduino-dds-sinewave-generator/index.html)

Risultato:
![osci](osci.png)

[filtro Chebyshev su Matlab](https://it.mathworks.com/help/signal/ref/cheby1.html)

## Campionare un segnale con PWM

Usiamo una onda triangolare che incrociando la sinusoide, fa generare un impulso.

![pwm](pwm.png)

Sto praticamente modulando il segnale con una portante e una modulante.
![pwm2](pwm2.png)

_________

[Mozzi Arduino](https://sensorium.github.io/Mozzi/)
