# Appunti della lezione di Venerdì 5 Febbraio 2021

## Debug di circuiti

- saldature
- componenti messi male

## Arduino e la generazione dell'onda

Avevamo generato una sinusoide con la PWM e poi lo avevamo filtrato con un filtro passa basso

### Problema della risposta del potenziometro

Usando dei timer per leggere la tabella per aggiornare la tabella, solo che la lettura del valore avviene al giro successivo.

## PCM su Arduino

Essa è migliore per vari motivi

## DAC con resistenze pesate
_dispensa su DAC e resistori_

con 8 bit abbiamo 256 livelli di ampiezza

PCM -> prende un segnale e lo converte

Vediamo nello specifico come funziona un DAC

![ampop](ampop.png)

![ampop1](ampop1.png)

![somma](somma.png)

Standard TTL -> 0 volts o 5 volts

![dac](dac.png)

Essa si chiama conversione a somma pesata -> poichè dipende dal come pesiamo queste resistenze

I componenti variano il loro valore in base alla loro temperatura.

>è difficile trovare resistenze esatte per realizzare questo DAC

![somma1](somma1.png)

## DAC scala R2R

In cui si utilizza lo stesso buffer con amp op invertente

![r2r](r2r.png)

Abbiamo resistori con valori di cui non ci interessa il valore

Come facciamo i calcoli per ottenere il valore di vout?

Otterrò quindi:
![r2r2](r2r2.png)

>R/2R

Le resistenze servono solo per realizzare i rapporti

>Il numero di ingressi sarebbe il numero di bit del convertitore

>Il vantaggio di questo convertitore è la maniera di come realizziamo il rapporto tra le amplificazioni, poichè ha reso tutto molto più semplice la costruzione.

La tolleranza delle resistenze è analoga.

Togliendo la parte dell'operazionale, il circuito funziona uguale

## DAC R2R invertito (scala R2R invertito)

Ho l'ingresso non invertente collegato a massa e con le tensioni quando siamo a 0.

![invr2r2](invr2r2.png)

Tiene sotto tensione tutti e due gli ingressi e ha lo scopo di stabilizzare il circuito precedente.

## Dispositivi PCM in realtà

Teoricamente i convertitori PCM dovrebbero essere tutti uguali.

I R2R sono difficili da realizzare ed è stato necessario costruirne degli altri miniaturizzati.

## Convertitore Sigma Delta

![sigma](sigma.png)

Realizzo un sovraccampionamento con tutti 0, ed ogni 2 campioni inserisco 2 valori 0. Aumento la frequenza di campionamento.

Trasformo il segnale sovraccampionato in PWM a frequenza molto piú alta.

A circa 28 MHz...

1. ho segnale
2. lo sovraccampiono
3. filtro con LPF

>Qualitativamente ciò non funziona molto bene

Per applicarlo nella realtà si usa l'oversampling con noise shaping per togliere disturbi posizionandoli a frequenze molto elevate e filtrando con il LPF si eliminano i disturbi

## Confronti fra DAC

![conf](conf.png)

I convertitori a resistenze pesate continuano ad essere qualcosa di migliore come qualità.



## Convertitori nella vita reale

RME usa delta sigma.

[convertitori RME](https://www.akm.com/cn/en/products/audio/audio-dac/)

![U0HqGDN.jpeg](U0HqGDN.jpeg)

Delta sigma a multi livello, ovvero con piú delta sigma in parallelo con un clock che li gestisce, e poi con noise shaping aggressivo.

### Inside Fireface 800
[Inside Fireface 800](http://www.crome.org/wpn/inside-the-fireface-800/)

![fireface-800-a-1080x675](fireface-800-a-1080x675.jpg)

![fireface-800-b.jpg](fireface-800-b.jpg)

_____

### Protocollo Scasi

[https://www.reichelt.com/it/it/scsi-ii-su-ii-sub-d-st-su-sub-d-st-0-9-m-ak-scsi-041-p4056.html?PROVID=2814&gclid=CjwKCAiA9vOABhBfEiwATCi7GL3nEJ0-_E5Q20wWP9d9Y0fnapqEjlcASHu4Qw11N_kw_RfvsFDM7hoCbEMQAvD_BwE](https://www.reichelt.com/it/it/scsi-ii-su-ii-sub-d-st-su-sub-d-st-0-9-m-ak-scsi-041-p4056.html?PROVID=2814&gclid=CjwKCAiA9vOABhBfEiwATCi7GL3nEJ0-_E5Q20wWP9d9Y0fnapqEjlcASHu4Qw11N_kw_RfvsFDM7hoCbEMQAvD_BwE)

Connettore IDE -> sostituito dal SATA che è seriale

Gestire un flusso parallelo è piú difficile e costoso in digitale.

![615F0NVZoTL._AC_SY355_.jpg](615F0NVZoTL._AC_SY355_.jpg)
