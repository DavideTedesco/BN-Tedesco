# Appunti della lezione di lunedì 22 marzo 2021

## Porte logiche e elettronica analogica

### Trigger di Schmitt

Esso sfrutta il concetto di isteresi, ovvero quando un processo fisico assume in base a un ingresso:
- alto
- basso

E sotto e sopra una certa soglia abbiamo uno stato alto o basso.

Vediamo il circuito realizzato in standard TTL(5 V):

![circ](circ.png)

[iCircuit](https://www.macupdate.com/app/mac/51205/icircuit)

Attraverso il trigger di Schmitt abbiamo un onda quadra.

![circ2](circ2.png)

Esso funziona come il un grilletto, o spara il colpo o non lo spara!

![800px-Smitt_hysteresis_graph.svg](800px-Smitt_hysteresis_graph.png)

In elettronica digitale esistono due standard:
- standard TTL -> transistor transistor logic
- standard CMOS

Il circuito che realizza il trigger di Schmitt è alla base dell'elettronica digitale che porta allo sviluppo della logica booleana.

### Algebra booleana

Essa nacque da concetti di logica:
- vero
- falso

E come essi possono combinarsi fra loro.

### Operatori

- AND
- OR
- NOT
- NAND
- NOR
- XOR


#### Operatore NAND in elettronica

Esso è molto utile per realizzare

### Calcolatore a 2 bit

Usando degli interruttori:
![calc](calc.png)

- 2 porte NOT
- che entrano in due porte AND
- con porta OR finale
- porta AND finale

![lp](lp.png)

### oscillatore realizzato con una porta NAND

[oscillatore realizzato con porta NAND](https://moonarmada.com/intro-to-synth-diy)
