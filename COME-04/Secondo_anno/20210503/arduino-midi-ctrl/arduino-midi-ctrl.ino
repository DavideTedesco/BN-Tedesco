int v0 = 0; //inizializza la variabile per ingresso analogico A0
int v0_ = 0; // inizializza la variabile che servre per impostare una soglia (threhold), 
            // al di sotto della quale il controller non invia dati, altrimenti si aggiornerebbe in modo continuo

int threshold = 2; //inizializza la soglia di scrittura sulla porta seriale

void SendMidiToSerial (byte stato, byte data1, byte data2) // inizializza la variabile vettore con i tre byte 
                                                           // costituendi il messaggio MIDI (status byte, databyte1, databyte3)
{
  Serial.write(stato); //
  Serial.write(data1); // Scrittura sulla porta seriale del messaggio MIDI
  Serial.write(data2); //
}
void setup () {
  Serial.begin(31250); // inizializza il byte-rate in bps (byte per secondo) della porta seraile
}
void loop () {
  v0 = analogRead(0); // legge il valore dellíngresso analogico A0 
  v0 = map(v0, 0, 1023, 0, 127); // Mappattura dell'ingresso di analogico di Arduino convertito
                                 // a 10bit (0-1023) nello standard MIDI (0-127)
                                 
  if (v0 - v0_ >= threshold || v0_ - v0 >= threshold) // verifica che ci sia una variazione di valore in ingresso
                                                      // nel caso positivi modifica il contenuto del vettore SndMidiSerial
  {                     
    v0_ = v0;
    SendMidiToSerial(176, 42, v0);
  }
  
}
