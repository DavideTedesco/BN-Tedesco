# Appunti della lezione di lunedì 3 Maggio 2021

Avevamo visto l'implementazione di MIDI e USB...

L'OSC ha un problema è che non è usato in molti synth.

## MIDI

È un protocollo asincrono, ed è molto semplice dal punto di vista della struttura.

![m](m.png)

Abbiamo un accoppiatore ottico per la lettura del segnale MIDI, ma non abbiamo il segnale collegato elettricamente ma esso mi garantisce che mi passi il massimo della corrente ovvero i 5 volts della seriale.

Esistono solo i byte:
- di stato
- di dati

![mi](mi.png)

Il bit piú significativo può assumere due valori e indica il tipo di byte.

0 a 127 perchè si usa 2^7

All'interno dello status byte indichiamo anche un canale tra i 16 disponibili.

La frequenza MIDI è 31250 bit al secondo...

## MIDI e Arduino

```
int v0 = 0; //inizializza la variabile per ingresso analogico A0
int v0_ = 0; // inizializza la variabile che servre per impostare una soglia (threhold),
            // al di sotto della quale il controller non invia dati, altrimenti si aggiornerebbe in modo continuo

int threshold = 2; //inizializza la soglia di scrittura sulla porta seriale

void SendMidiToSerial (byte stato, byte data1, byte data2) // inizializza la variabile vettore con i tre byte
                                                           // costituendi il messaggio MIDI (status byte, databyte1, databyte3)
{
  Serial.write(stato); //
  Serial.write(data1); // Scrittura sulla porta seriale del messaggio MIDI
  Serial.write(data2); //
}
void setup () {
  Serial.begin(31250); // inizializza il byte-rate in bps (byte per secondo) della porta seriale
}
void loop () {
  v0 = analogRead(0); // legge il valore dellíngresso analogico A0
  v0 = map(v0, 0, 1023, 0, 127); // Mappattura dell'ingresso di analogico di Arduino convertito
                                 // a 10bit (0-1023) nello standard MIDI (0-127)

  if (v0 - v0_ >= threshold || v0_ - v0 >= threshold) // verifica che ci sia una variazione di valore in ingresso
                                                      // nel caso positivi modifica il contenuto del vettore SndMidiSerial
  {                     
    v0_ = v0;
    SendMidiToSerial(176, 42, v0);
  }

}
```
[Arduino e MIDI](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Midi)


## Storia del MIDI

La Sequential Circuits propose il MIDI per standardizzare le connessioni fra strumenti.

## MIDI in Falstad

![fal](fal.png)

Vediamo che abbiamo 7 bit di ingresso e due amplificatori operazionali invertenti per avere il segnale in uscita non invertito.

Abbiamo 127 livelli di tensione e con ingresso analogico riusciamo ad acquisire segnali in digitale.

(abbiamo parlato del bandaneon)
