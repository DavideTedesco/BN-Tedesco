# Appunti della lezione di Venerdì 8 Gennaio 2021

## Amplificatori operazionali
![oppo](oppo.png)

Il segno piú è l'ingresso non invertente, ed il segno meno è invertente.

Esso si chiama operazionale, poichè serve per fare operazioni.

Esempio:

Se inserisco 5 Volt non invertente un Volt nell'invertente.

Per fare delle operazioni devo combinare insieme piú operazionali.

Questa tipologia di amplificatori sono nati per realizzare le operazioni sui calcolatori analogici, che facevano le somme utilizzando i livelli di voltaggio.

![oper](oper.png)

Solo che con i calcolatori analogici se vi era uno sbalzo di tensione le operazioni si sballavano tutte.

Gli amplificatori operazionali analogici nacquero per realizzare il calcolo differenziale.

Vi erano vari problemi:
- velocità di calcolo
- memoria
- dimensioni della ram

Le memoria dei calcolatori degli anni 80 era a nuclei di ferrite, e per fare un'operazione con calcolo differenziale si aspettava qualche giorno.

### Gli scopi successivi alle operazioni

Poi gli amplificatori operazionali divennero utili anche per altro come per la musica.

Un esempio di calcolatore analogico, è un sintetizzatore, come un Moog, in cui si fa calcolo differenziale e integrale e si tramuta tutto in suono.

### A cosa serve l'amplificatore operazionale, cosa cambia rispetto all'amplificazione realizzata da un transistor?

Cosí è come lo si guarda idealmente:
![op](op.png)

Cosí è come realizzato all'interno:
![optran](optran.png)

LM386, è un operazionale general purpose.

Ma gli amplificatori servono anche per la radiofonia, e la telefonia.

Vi sono quindi circuiti BF(che lavora a bassa frequenza) o AF ad alta frequenza.

Barrel Brown realizza degli amplificatori dedicati all'audio.

### RC4558

![rc4558draw](rc4558draw.png)

Esso può essere scritto come:
![rc4558](rc4558.png)

Andando a vedere i circuiti reali vi sono molte componenti che servono per migliorare il funzionamento.

Come per la banda audio -> lavoro soltanto sulla banda audio.

Gli amplificatori possono tendere ad autoscillare.

![rc4558draw1](rc4558draw1.png)

### LM386

![lm386big](lm386big.png)

Il pin 1 e 8 va a modificare tutto il circuito interno e cosa succede con resistore in parallelo a condensatore?

Realizziamo un filtro passa alto:
![hp](hp.png)

Filtro passa basso:

![lp1](lp1.png)

Passa banda -> serie hp e lp

![bp](bp.png)

Filtro notch

![rb](rb.png)

Nell'LM386 vediamo anche un filtro passa banda finale, vi sono in serie parallelo un passa alto e un passa basso:
![lp](lp.png)

### Esempio di uso di un operazionale

![es](es.png)

A dipende dal tipo di amplificatore operazionale, e A puó essere in genere regolato.

__________

Parto dalle sottrazioni perchè da esse posso arrivare alla somma(`- * - = +`)

__________

## Cosa faremo quest'anno?
Quest'anno realizziamo qualcosa in digitale che vanno a mutare il comportamento di un circuito analogico.

Il senso è quello di avere un circuito comandato da un segnale digitale -> con circuiteria analogica.

## Arduino

Arduino usa Processing e poi lo traduce in linguaggio macchina
