# Appunti della lezione di Lunedì 15 Febbraio 2021

## Oscillatore con varie forme d'onda
Sketch oscillatore con arduino con varie forme d'onda, fluido.

![osci](osci.png)

Per simulare la sintesi analogica, possiamo utilizzare 2 arduino ad esempio.

Un arduino come portante ed uno come modulante per realizzare una FM ad esempio.

Differenza di questo sketch rispetto a quello dell'altra volta:

1. utilizza la PCM
2. lettura da wavetable

[Arduino Waveform Generator di Amanda Ghassei](https://www.instructables.com/Arduino-Waveform-Generator/)

## RM in analogico

Vedremo dalla prossima volta un ring modulator a livello analogico.

![Ring_Modulator](Ring_Modulator.png)

Esso è realizzato con 4 diodi, molto simile a un ponte di Graetz.

Il circuito è realizzato con una modulante, ovvero l'input, e vi si fa entrare una portante.

____________
[teensy](https://www.pjrc.com/store/teensy40.html)
