# Appunti della lezione di Venerdì 15 Gennaio 2021

[Zia Drive](https://www.boxguitar.com/it/overdrive/1421-durham-zia-drive-low-comp-od.html)

Iniziamo a vedere un po' di digitale sull'elettronica usando arduino come micro controllore.

## Cos'è Arduino?

Esso è stato realizzato in varie versioni, è un hardware Open Source, basato su un chip che è cambiato negli anni.

Ma basato sempre su chip simili.

Della familgia ATmega.

Esso è un micro controllore che ha la possibilità di eseguire delle istruzioni che permette di elaborare dei dati e gestirli grazie al software.

![ard](ard.png)

### Cosa hanno fatto i produttori di Arduino?

Hanno posto tutte delle componenti intorno allo stesso.

Vi è un convertitore ADC da analogico a digitale, non ha uscite analogiche.

Ha una porta USB che viene emulata come una porta seriale come una vecchia porta COM.

![porta](porta.png)

Attraverso l'usb si puó caricare il software.

### Quale linguaggio di programmazione viene usato?

Viene usato Processing un po' rivisitato.

![arduino](14Core_Arduino_UNO1.jpg)

### Uscite ed entrate

Esse sono le varie porte.

![ardu](ardu.png)

**Esso ha input analogici, ma non ha output analogici.**

### Campionamento in ingresso dei segnali su Arduino

Utilizza campionamento PCM a 10 bit e poi conversione.

Invece in uscita esso in uscita ha solo un'uscita digitale.

### Software Arduino

Scaricabile dal sito.

Max, puó leggere i dati dalla porta seriale e gestirli.

Esistono vari interpreti di segnali Arduino per Max.

Usiamo arduino2max per interpretare i dati in ingresso da Arduino.

### A cosa serve Arduino?

Non per campionare audio, esso non ha un convertitore da digitale ad analogico integrato.

### Come campiona i segnali analogici in ingresso Arduino

Con il campionamento [PCM](http://web.tiscali.it/ocham/audio2.htm).

### Come potremmo far uscire segnali digitali da esso?

Con la PWM ovvero con campionamento e poi riconversione attraverso un filtro che taglia alla frequenza di campionamento.

![pwm](pwm.png)

Essa la possiamo usare per avere un segnale analogico.

#### PWM in uscita

Usando un semplice filtro RC con resistore in serie e condensatore in parallelo.

Esiste un chip per fare una conversione.


### Segnale usato in DSD

È difficile da processare in PWM, e dunque si usa la PCM per manipolare il suono.

Non esisto workstation che lavorano in DSD, esiste

________

La prossima volta utilizziamo l'[MCP4725](https://github.com/RobTillaart/MCP4725) per fare la conversione da digitale ad analogico.
