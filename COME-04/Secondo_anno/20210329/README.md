# Appunti della lezione di lunedì 29 marzo 2021

[https://youtu.be/bGw4YCoaZxI](https://youtu.be/bGw4YCoaZxI)

[https://www.youtube.com/watch?v=C8Zl51ynrgI&list=PLZp72vkGEl4yra_4P0TZW3Yg4JJGVsHpp&index=9&t=290s&ab_channel=DanieleTartaglia](https://www.youtube.com/watch?v=C8Zl51ynrgI&list=PLZp72vkGEl4yra_4P0TZW3Yg4JJGVsHpp&index=9&t=290s&ab_channel=DanieleTartaglia)
Le RAM erano realizzati con anelli di ferrite.

I FLIP FLOP erano i circuiti a relè.

## Logica sequenziale

Oltre alle porte logiche esistono i circuiti di logica sequenziale.

Elementi che hanno risultato diverso in base a come arrivano i dati abbiamo in fatti i:
- latch SR
- flip-flop SR -> relè che cambiavano stato

Oscillatori di ondaquadra vengono chiamati astabili e bistabili con un andamento ed un livello che nel tempo varia.

>Porte sequenziali, con un'uscita in base ad una sequenza.

SR =  Set e Reset

### Latch SR
![latch](latch.png)

#### Circuito di rimbalzo con latch

Risoluzione della fase di instabilità

![rim](rim.png)

![rim2](rim2.png)

### Flip Flop SR

è un'evoluzione del latch con altri due nor

![ff](ff.png)

Se enable è a 0 non succede nulla

L'utilizzo che viene fatto è vario ed esso è praticamente un latch sincronizzato.

![ff2](ff2.png)

Il problema del Flip Flip SR è che bisogna evitare la condizione 1 1.

#### Flip Flop JK

Con questa tipologia di Flip Flop si possono avere i due stadi ad 1.

#### Flip Flop D
![d](d.png)

______

### Circuiti

#### Computer rudimentale
Con questi circuiti si possono realizzare varie applicazioni come ad esempio un rudimentale computer con addizioni e memorizzazione di dati:

![c](c.png)
![c1](c1.png)

Altri circuiti sono:
- convertitori
- multiplexer
- demultiplexer

Se vogliamo vedere dei numeri da binario ad esadecimale, prendiamo i numeri in bit e visualizziamo il codice in esadecimale.

#### Schermo a 7 linee

Per rappresentare i 4 stati nelle posizioni dei led, abbiamo quindi un decodificatore a 7 segmenti.

![ff2](ff2.png)


_____
Fare un orologio che segna secondi minuti e ore su Falstad

![24bit](24bit.png)
_____

File .txt da importare su [falstad](https://www.falstad.com/circuit/) nella cartella.

[killswitches chitarre elettriche](https://ironageaccessories.com/collections/about-killswitches)
