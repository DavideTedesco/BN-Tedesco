# Appunti della lezione di lunedì 8 marzo 2021

## Limiti di Arduino

Siamo arrivati a capire i limiti di Arduino, ma il limite è che non è un processore, ed esso è un microcontrollore, e permette quindi di generare controlli, mentre per fare musica serve almeno un minimo di potenza di calcolo.

Ideale sarebbe mettere su un circuito come Arduino con un DSP intorno.

## Realizzare un oscillatore con un solo transitor

Funzionamento di un transistor

L'amplificatore alimenta se stesso

![osc](osc.png)

amplificatore va in autoscillazione e si auto alimenta.

Avremmo un oscillatore a sfasamento...

### Oscillatori a sfasamento

![sfa](sfa.png)

Abbiamo un transistor con poi dei passaggi di sfasamento con filtri passa alto CR.

La frequenza di oscillazione è data da: ![fosc](fosc.png)

![circ](circ.png)

I filtri inseriti servono per sfasare di minimo 180 gradi.

Esistono due tipologie di implementazioni degli oscillatori a sfasamento:
- con transistor
- con operazionale

### Chip i555

È un chip molto semplice utilizzato come multi vibratore

![ne555](ne555.png)


Questo circuito può funzionare:
- con trigger
- con un clock

Fare un generatore sinusoidale con

________
[The VCO](https://secretlifeofsynthesizers.com/the-vco/)

Diodi zener usati come resistori variabili, in base alla tensione applicata

>Amplificatore che si amplifica con se stesso

[KiCad](https://kicad.org/download/macos/)

[Oscillatori](http://www.edutecnica.it/elettronica/oscillatori/oscillatori.htm)
