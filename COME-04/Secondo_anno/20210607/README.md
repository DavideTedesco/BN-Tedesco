# Appunti della lezione di lunedì 7 giugno 2021

Progetto per il secondo biennio.

## Hub per la gestione di segnali analogici e digitali

![](.png)

Gestione

## Cose di cui abbiamo parlato:

- [Organo Hammond](https://hammondorganco.com/products/leslie/)
- ruote dentate nell'organo Hammond: ![ham](ham.png)
- organo Hammond è uno dei primi strumenti elettrici basato su sintesi additiva
- Trautonium
- Onde Martenot
- [Pari organs](http://www.organmusiccenter.com/pari.html)
- [librerie hoa](http://hoalibrary.mshparisnord.fr/en/)
- ![amb](amb.jpeg)

## Per l'esame

![hub.png](hub.png)

## Il Sony Spresense

[Spresense
](https://it.emcelettronica.com/scopriamo-la-spresense-main-board-della-sony#attachment%20wp-att-171954/0/)

![spresenze-pin.jpeg](spresenze-pin.jpeg)

![spresenze-schema-a-blocchi.png](spresenze-schema-a-blocchi.png)
