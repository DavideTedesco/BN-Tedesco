# Appunti della lezione di Venerdì 19 Febbraio 2021

Rivediamo il progetto visto l'altra volta.

Leggiamo una tabella con valori memorizzati...

## Ring Modulation in analogico

### Come è realizzato?

4 diodi scotti, con maggiore velocità di transizione da uno stato all'altro.

Si puó realizzare anche senza trasformatori.

### Differenza fra RM digitale e RM analogica

L'alterazione analogica del suono è molto diversa ed è realizzata con trasformatori e diodi.

Nelle RM realizzate nei synth e nei pedali, si dosa il suono dry e si creano delle particolari sonorità.

### Ring modulator commerciale

- Ring Thing
- Vitruvian
- ...

### Perchè si chiama Ring Modulator?

Perchè vi è un anello di diodi.

_______

La prossima vediamo la RM fatta meglio e come migliorare alcuni aspetti e come li hanno implementati i vari compositori.
