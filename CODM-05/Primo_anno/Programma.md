# Programma degli argomenti delle lezioni

## Argomenti delle lezioni
1. La dodecafonia e l'influenza di Webern sulla musica Contemporanea
2. Classico o post-moderno
3. Le 3 fasi come modello di sviluppo della cultura
4. Introduzione alla contrapposizione Schönberg-Stravinskji
5. I primi brani di Stockhausen(Etude, Studio I, Studio II)
6. Cosa sono punti e gruppi per Stockhausen (Gruppen)
7. La Momentforme di Stockhausen (Momente, Gesang der Jüngelinge)
  - essa non è orientata
  - Momento è qualcosa che non cambia ed è sempre simile a se stesso, sequenza continua di climax
8. Documentare ogni aspetto della composizione(Kontakte)
  - strumenti della WDR
9. Composizione algoritmica per mezzo di regole compositive(Plus Minus, composizioni di Guaccero)
  - Regole del gioco come meccanismo produttivo in partitura(istruzioni, simboli, strutture accordali)
  - La composizione come _gioco_ o _forma ludica_
  - procedimenti stocastici
10. Rapporto tra Stockhausen e Adorno(Stokchausen disse ad Adorno: "Lei cerca una gallina in un quadro astratto")
11. Concetto di sistema/galassia di Stockhausen
 - dimensione locale
 - dimensione globale
12. La dialettica di Adorno(essa puó rovesciarsi in ogni momento)
  - Perchè Adorno è Importante -> poichè  anche nei saperi c'è una componente dialettica, l'oggetto è sempre qualcosa di diverso che ti può dare la composizione. Concetto di **assorbire il vecchio nel nuovo**, concetto della dialettica, sono elementi da assorbire e cercare di intendere e portare nella composizione.
  - Poli musicali importanti di Schoenberg e Straviniskij(egli ha vestito panni diversi in dimensioni diverse)
      - musica autentica (dodecafonia) _-> con il tempo puó divenire falsa poichè abusata dalla società e viceversa_ -> nella musica autenica contemporanea possiamo trovare varie maschere(_Maschera della morte rossa_)
      - musica falsa(che si adatta alla società)(neo-classicismp) -> il **post-moderno** implica una muisca tutta falsa
  - cosa ci dice sulla musica di oggi -> _Adorno e la sua filosofia della musica ci serve per farci intendere e capire dove stiamo andando._
  - cosa ci dice del rapporto tra compositore e sistema -> Adorno ci dice che Schönberg nella sua fase atonale è libero e poi diviene dittatore nella sua fase dodecafonica, dicendo che i compositori dal '45 in poi giocano con i loro balocchi, laddove giocare con i propri balocchi per Bartok e Schonberg li ha portati all'esilio.
13. Ammissione di uno stile di base -> ricerca trasferita agli strumenti -> lo strumento diviene opera(Lupone, Bertoncini) -> con ció cambia anche la metodologia di esecuzione dei brani
14. Materiali e drammaturgia in Gesang der Juenglinge(articolo Decroupet)
15. Discussione dei _principi operativi_ -> per Stockhausen la composizione è incentrata sul suono(per Coenen ciò è scontato, ma per Stockhausen non lo era)-> (Althusser (strutturalista marxista), che dice che _l'ideologia dominante si appropria dei segni dell'ideologia contrapposta per neutralizzarla._)
16. Microfono come strumento musicale(Mikrofonie I, Mikrofonie II)
  - Mikrofonie I -> **surrogazione di quarto ordine**(Smalley)
      - tamtam, microfono, filtro
      - ascolto del micro
      - scrittura deterministica- >partitura descrittiva, partitura verbale(con molte parole che descrivono il suono, onomatopeiche) -> come un'intavolatura
      - gestione del brano attraverso 2 "squadre" da 3 persone a mo di coro(in modo antifonico) -> stesso gesto realizzato da 3 cervelli diversi (_relazione di improvvisazione_) -> in _TUTTI_ vi è l'elemento carnevalesco
      - organizazione seriale -> 3 micro serie
  - Mikrofonie II(dimensione autobiografica e metamusicale, con la presenza di 3 composizioni che sono tutti _momenti_)
        - per organo hammond, coro e 4 modulatori ad anello
        - surrogazione della distorsione attraverso RM -> ma essa rende tutto piú pesante, fisso e meno agile
        - in questo brano fa uso di parti di altri brani(Momente, Gesang, Carèe) -> relatività progressiva
  - Cosa lega i 2 brani?
      - la struttura a 33 sezioni
      - presenza di materiale prescrittivo
      - idea che ci sia una distorsione (I microfono, II RM)
      - le "squadre" -> 2 in Mikrofonie I, 4 in Mikrofonie II
    17. Problema dell'_Agenza_ (tratto da "Living Electronic Music" di Emmerson)-> il porsi la domanda di chi abbia prodotto un certo risultato -> e dunque nell'ascolto della musica elettroacustica abbiamo le 4 tipologie di _surrogazione_ definite da Smalley (nel testo in cui tratta della _Spettromorfologia_)
        - non vedo la fonte, ma comprendo quale sia la sorgente(registrazione struemento)
        - non vedo la fonte, ma mi pare di conoscere e sapere quale sia la sorgente(strumento in FM)
        - non vedo la fonte, "sembra quasi come se fosse" ...
        - non vedo la fonte, e non ne capisco la sorgente
    - Smalley definisce anche _Eerie_ e _Weird_ come concetti dell'assenza di _agenza_

    18. Composizione e percorso costruito e disegnato(Telemusik) -> realizzato in Giappone da materiali di varie parti del mondo
        - molti materiali all'interno (di diverse culture) -> appello alla molteplicità culturale negato per il trattamento globalizzato del materiale
        - l'RM si riconosce perfettamente
        - uniformare le varie identità culturali -> uccide i contesti culturali -> mentre ad esempio in Hymnen i materiali musicali sono veramente un pretesto
    - Hymnen e Telemusik sono gli **unici esemplari di musica concreta di Stockhausen** oltre l'Etude -> essi segnano un momento di passaggio per Stockhausen -> _sincretismo egotico_ (tutte le religioni del mondo, tutti gli inni, tutti i brani di Stockhausen) -> la formulazione della _formula_ Stockhauseniana viene a mancare


## Personaggi ed artisti trattati
1. Stockhausen
2. Adorno
3. Stravinskji
4. Schönberg
5. Webern
6. Hegel

## Parole fondamentali del corso

**Serialismo** ~

**Paradigma** ~
Il Paradigma Vigente è l'insieme di una serie di credenze, assunti e rappresentazioni che caratterizza la scienza di un certo periodo.
Il paradigma è valido in ambito scientifico e musicale.
  - Un paradigma è costituito da:
    - Valori
    - Modelli
    - Generalizzazioni
    - Esemplari

**Fenomenologia** ~ Descrizione dei fenomeni, ossia del modo in cui si manifesta una realtà. In filosofia, il termine ha avuto fortuna a partire dalla Fenomenologia dello spirito (1807), in cui G.W.F. Hegel tracciò la storia delle manifestazioni dello Spirito. Oggi per f. s'intende l'indirizzo filosofico fondato da E. Husserl che, mettendo fra parentesi l'esistenza del mondo, lo riduce a un insieme di fenomeni che si danno alla coscienza e possono essere colti nella loro 'essenza' logica, universale e necessaria. Per M. Heidegger, allievo di Husserl, f. significa così "lasciar vedere in sé stesso ciò che si manifesta", liberandolo dall'occultamento in cui rischiano di farlo cadere i nostri pregiudizi.

**Dialettica** ~ Abilità verbale del sostenere una posizione, del persuadere, esercitata nella discussione; in filosofia, declinato in diverse accezioni, metodo di conoscenza fondato sul dialogo o sul confronto fra opposti; processo risultante dal contrasto fra due forze

**Formula** ~

**Galassia** ~

**Spettromorfologia**(termine coniato da Smalley) ~ vuol dire spettri mobili nel tempo, quando sentiamo un suono noi umani gli diamo una forma. L'ipotesi e l'elaborazione di un fantasma dell'elaborazione, o gesto che il suono abbia prodotto, anzichè far finta di essere invisibili, tanto vale realizzare una teoria delle surrogazioni, e dunque assumere in mancanza di una conoscenza reale di cosa sta facendo cambiare il suono, gesto o intenzione

**Sincretismo** ~ l'assunzione, fusione e rielaborazione in vista di un nuovo assetto di credenze, di credo religiosi e convinzioni metafisiche preesistenti.
_Per Stockhausen viene tutto sotto il segno di induista, grazie agli insegnamenti(non diretti) di un grande mistico indiano di nome Satprem Sri Aurobindo, i cui contenuti sono:_


## Brani di Stockhausen trattati

Kontakte, Gesang der Juenglinge, Mikrofonie I, Mikrofonie II, Plus-minus, Etude, Studie I, Studie II, Gruppen, Momente, Mixtur, Telemusik, Hymnen, Kontra-punkte, Kreuzspiel
