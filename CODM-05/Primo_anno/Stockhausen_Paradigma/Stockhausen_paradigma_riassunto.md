# Riassunto sul paradigma Stockhausen

Tratto dal testo di Alcedo Coenen"Stockhausen's Paradigm: A Survey of His Theories"

## Parte prima il paradigma in generale in scienza & musica

### Il significato della parola _paradigma_

Da ciò che esplicita Coenen un autore(come Stockhausen), agisce nell'arco della sua esistenza in base ad un paradigma(come puó essere quello delle rivoluzioni scientifiche).

Dal **paradigma vigente**, ovvero una serie di credenze, assunti e rappresentazioni che caratterizzano la scienza di un certo periodo, lo scienziato basa la sua conoscenza e le sue ipotesi. Quando si presenta un'anomalia(**crisi del vecchio paradigma**) in questo _paradigma vigente_, si fanno emergere nuove ipotesi, avviene una **rivoluzione scientifica** ed emerge un **nuovo paradigma** che crea conflitti con il precedente, questo nuovo paradigma è l'insime di convinzioni, rappresentazioni e assunti di base da quel momento in poi.

Cerchiamo dunque di capire il _paradigma vigente_ di Stockhausen per poter intendere gli sviluppi della sua _rivoluzione compositiva_.

### Di cosa si compone un paradigma

Esso è strutturato su **pregiudizi** (ovvero elementi inevitabili de pensiero e delle attività di ricerca artistica intellettuale, esso è _Nel diritto romano, azione giuridica precedente al giudizio, e tale da influire talvolta sulle decisioni del giudice competente_ ovvero _nozioni note, nozioni date per assodate a partire dalla quale si va alla ricerca dell'ignoto_ e ancora _sono dati incapsulati nelle nostre nozioni che diamo per scontato e li usiamo per andare avanti_).

Un paradigma è costituito da:

1. Valori
2. Modelli
3. Generalizzazioni
4. Esemplari

Essi vanno per valore d'importanza, dall'elemento fondante al caso specifico.

#### I valori
Essi sono gli assunti di base, le cose che nessuno mette in discussione, senza le quali non c'è possibilità di intendersi.

##### Valori per la scienza
1. universo
2. le leggi della fisica
3. il funzionamento dello spazio-tempo.

Per la scienza i paradigmi sono stati stabiliti da vari scienziati:

- Aristotele -> riferimento paradigma piú antico fisica e metafisica.
- Galileo -> paradigma più antico per le scienze astronomiche.
- Newton -> ha elaborato un'idea di spazio-tempo, considerando lo spazio ed il tempo come una scatola omogenea invariante, alla base della fisica.
- Kant -> ha stabilito un assunto di base, cioè che ogni giudizio scientifico si basa sulla nostra percezione delle spazio-tempo, e non sulle cose in se stesse.(Tagliando di fatto la possibilità di ragionare sulla cosa in se, sulla metafisica).
- Einstein -> ha cambiato attraverso una rivoluzione scientifica, il paradigma di spazio-tempo di Newton.
- Heisenberg -> ha introdotto i nuovi assunti della meccanica quantistica che la fisica odierna pratica, senza queste assunzioni di base di Einstein e Heisenberg, non potremmo costruire la fisica del presente.

##### Valori per la musica

1. Cos'è musica
  - "La musica è l'arte dei suoni" (in vecchi manuali di musica)
  - "Tutti i suoni sono musica se ascoltati con attitudine di ascolto musicale"
 - "Solo il suono organizzato è musica"(lo troviamo in Schaeffer e in Varése con il libro Il Suono Organizzato)
2. A cosa serve la musica (funzioni della musica)
3. Dove e come si suona musica (luoghi della musica)
4. Dove e come si ascolta musica (luoghi della musica)
5. Qual'è il ruolo della musica nella società e nella vita degli individui (ruolo della musica)
6. Qual'è il significato sociale della musica

![/CODM-05/Stockhausen_Paradigma_Prima_Parte/valori_modelli.png ](/CODM-05/Stockhausen_Paradigma/Stockhausen_Paradigma_Prima_Parte/valori_modelli.png "valori e modelli")

#### I modelli
##### Modelli per la scienza
1. Materia
2. Macchine

##### Modelli per la musica
Repertorio di metafore prese da altre arti o scienze:

1. Pittura, scultura
2. Drammaturgia
3. Sintassi e linguistica
4. Matematica e statistica
5. Filosofia, politica
6. Retorica

#### Le generalizzazioni
##### Le generalizzazioni per la scienza
Sono in genere delle formule molto semplici
come _E = mc^2, f=m*a_

##### Le generalizzazioni per la musica
in musica è tutto ciò che è codificato nella teoria musicale

- Sintassi armonica -> quando si fa un basso di armonia non si puó passasare da IV a VI grado
- Risoluzione delle dissonanze-> fatto che le dissonanze devono risolvere
- in dodecafonia ci sono 12 note che per definizione sono in relazione soltanto fra loro, senza obblighi di comparire in esatto ordine
- composizione dei modi -> con toni e semitoni
- gerarchia della scala -> quinte e ottave
- classificazione accordi
- suddivisione gerarchica per 3 o per 2

Essi sono i semilavorati della musica tradizionale, e possono essere in continua evoluzione

#### Gli esemplari
##### Gli esemplari per la scienza
- esempio porta come momento angolare
- pendolo per spiegare attrito o moto
- orbite per spiegare l'attrazione corpi celesti
- piano inclinato per spiegare i meccanismi gravità

##### Gli esemplari per la musica
In musica abbiamo invece delle opere esemplari come:

- Il Clavicembalo ben temperato -> scritto per dimostrare la perfetta organizzazione dello spazio temperato -> si può scrivere per ognuna delle 12 tonalità maggiori e minori
- il Gradus -> di mementium -> enciclopedia della tecnica pianistica
- Tristan und Isolde -> inizio di un certo modo di trattare l'armonia che prelude l'atonalita
- suite op.25 di Schoënberg -> è normativa sulla dodecafonia
- op 16 n.3 Schoënberg -> liberazione totale da qualsiasi vincolo teorico -> scrittura coloristica -> melodia di timbri
- Phitoprakta e Concrete PH di Xenakis -> applicazione di una concezione statistica per la distribuzione del materiale e non deterministica

Essi diventanto grazie all'unità poietica(U.P.):

Scienza|Composizione|Ubi consistam (Domanda analitica)
-------|------------|-------------
valori | assunti di base sulla musica|perchè (causale e finale)
modelli|Immagini e metafore di base|come(l'U.P. immagina e descrive)
generalizzazioni|metodi compositivi|come(l'U.P. lavora)
esemplari (1)|composizioni esistenti|cosa (l'U.P. considera normante)
esemplari (2)|composizioni nuove|cosa (l'U.P. descrive)


## Parte seconda il paradigma Stockhausen
### Come è composto il paradigma Stockhausen
#### I valori
1. **principio di relativitià progressiva**: idea che la musica abbia uno sviluppo continuo ed orientato -> esiste la musica del passato, del presente e del futuro, e il compositore si deve muovere verso la musica del futuro, lasciandosi alle spalle un certo stadio della musica, sia a livello di comunità musicale, di collettività, di individuo, c'è una certa rete di relazioni che si evolve e da luogo  a una nuova rete di relazioni "x+1"
Il paradigma musicale porta da uno stadio precedente ad uno stadio successivo. Ciò legato al post-modernismo, dunque ad una totale presenza del passato ed un'assenza del futuro, ci fa capire ed intendere quanto Stockhausen sia distante da noi e quanto sia oggi importante capire il suo punto di vista

2. **principio dell'universalità della vibrazione**: entriamo in un piano poetico e immaginifico, vibriazione nell'universo, che si basa sul fatto che tutto nell'universo vibri. Ogni entità dell'universo vibra con qualsiasi ordine di grandezza, ogni vibrazione può esser considerata come una vibrazione acustica, ed ogni vibrazione acustica produce musica.
La musica è un riflesso dell'universo.

Essi sono racchiusi all'interno degli assunti di base di Stockhausen che partono dal **sincretismo religioso e filosofico** ovvero:
- Metampsicosi
- idea della vita come evoluzione dalla materia allo spirito - > idea che la singola vita sia uno stadio intermedio di un processo piú complesso e ampio, rispetto alla vita del singolo (reincarnazione)
- scopo dell'esistenza è la **progressiva estensione dei livelli di coscienza** -> che porta dalla bruta materia al puro spirito (alla base della Relatività progressiva)-> questa relatività progressiva ci porta **da un principio di integrazione ad un principio di sostituzione**, cioè mentre in una concezione piú tradizionale, alcuni sistemi culturali tendono ad opporsi e contrapporsi ai precedenti ->  l'idea di Stockhausen è l'idea che **gli stadi di estensione della coscienza(livelli di coscienza estesi), si realizzano per integrazione, è l'idea quindi che l'esistente si allarga a qualcosa di piú che si interizza ed assume in se il sistema precedentemente sviluppato** -> pensando alla relazione di: PUNTE, GRUPPEN e MOMENTE. Vediamo il principio di relatività progressiva in azione oltre a quello di estensione dei livelli e di integrazione (PUNTE->integrati in->GRUPPEN->che allargano la consapevolezza compositiva->MOMENTE->assume in se ed integra tutto quanto già sviluppato con punti e gruppi)
- Lo scopo della scrittura musicale della nuova musica è allargare esattamente quanto lo scopo dell'esistenza è l'**allargamento dei livelli di esistenza**, la nuova musica ha come scopo, **estendere i livelli di percezione**, ovvero raffinare la percezione e la capacità di distinguere con orecchio e cervello, di riconoscere ed esplorare il suono, qui vige un regime di relatività progressiva, la capacità delle composizoni aumenta per questo motivo. Il principio dell'universalità della vibrazione ci porta anche ad un'altra conseguenza, ovvero la **musica influenza gli esseri umani, psicologicamente e mentalmente, e anche fisicamente**.Conseguenza di estensione di percezione come scopo della musica.
La creazione di opere dedicate a estensione dei livelli di percezione ha lo scopo di:

  - creare rappresentazioni dell'universo nelle sue strutture
  - inccorragiare l'estensione della percezione -> (rappresentazione microcosmica)essa è una figura di un'estensione della coscienza che è piú universale
  - incorragiare l'estensione della coscienza

#### I modelli
Immagini e metafore di caratteristiche del mondo, dell'universo e dell'operare del compositore che in Stockhausen vengono individuati in 4 momenti.
1. **Vibrazione**("respiro di Dio")-> la scala della vibrazione è variabile dal micro al macro, ció permette di estendere le proporzioni a scale e parametri diversi; presupponendo che le proporzioni siano riconoscibili, condizione ideale della **percezione estesa**, che porti a dei livelli strutturali unificati(scrittura strutturale con proporzioni distinguibili)
  - periodica(ogni cosa ha: frequenza e ampiezza)
  - aperiodica(probabilità, teoria dei punti e dei gruppi)
2. **Continuum** esso rappresenta una metafora della contrapposzione tra quantità discretizzate  e possibilità infinite. Ogni continuum è una gradazione infinita tra 2 punti estremi, non dati una volta per tutte ma variabili(esempio: il minimo e massimo che un oscillatore analogico puó esprimere), ed ogni suono, ogni gruppo ed ogni identità possono essere considerati come il punto di intersezione di innumerevoli continua(plurale latino di continuum)
3. **Spirale** essa esprime la _relatività progressiva_ ovvero il fatto che ogni cosa è connessa alla precedente ma in un modo che tende ad integrare progressivamente, e non a sostituire i metodi precedenti. Ciò esprime l'estensione del principio seriale che comporta un'estensione dell'orizzonte di ricerca. La spirale è un cerchio che si muove nelle tre dimensioni ed può avere sempre lo stesso diametro, ma in genere si allarga progressivamente.

4. **Galassia** L'idea che le note siano stelle, ed i suoni siano stelle di una galassia è in realtà una metafora poetica di Eimert della poteica di Messiaen. Essa è _l'unità piú grande in cui noi possiamo organizzare il nostro universo_, ed in quanto metafora è portatrice di caratteristiche che sono estensibili al modo di lavorare di Stockhausen. La metafora è importante, perchè riesce a portare con sè, a tradurre, ad incapsulare una quantità di implicazioni e caratteristiche che dette una per una, sarebbero molto pesanti.
  - sistema di interrelazioni
  - esiste un continuum tra dimensione globale e dimensione locale
  - La galassia è un sistema di sistemi ovvero su scale diverse riproduce dinamiche simili
  - La galassia è un organismo gerarchizzato, al centro della galassia c'è il buco nero, attorno ad esso ci sono le stelle, attorno ad esse hanno gruppi di pianeti, tutto ciò ha come conseguenza:
    - gerarchia di appartenenze, un certo satelitte fa parte di una galassia, ad esempio la luna fa parte della via lattea, ma fa parte del sistema solare, ma fa ancor piú parte del sistema terra, dunque c'è una differenziazione di livelli di appertenenza e identificabilità.
  - Metafora del centro e dell'orbita: tutti i sistemi di una galassia, sono dotati di un centro e di materiale orbitante.

Queste ultime considerazioni sulla galassia si riversano sulla _teoria dei gruppi_ e sul concetto di _formula_, tutte le composizioni che vengono dopo il pensiero della galassia(quelle con gruppi, formula e momente) rispecchiano queste caratteristiche appena descritte.

Il momento(momente) è la risposta di Stockhausen per liberarsi dalla schiavitù del tempo, ovvero di liberarsi della freccia del tempo.

#### Le generalizzazioni (o Metodi di lavoro)

Generalizzazioni e metodi di lavoro sono quei termini che utilizziamo per indicare quella fase di acquisizione all'interno di un paradigma. Nel mondo scientifico esse sono le formule, nella composizione essi/e diventano complessi/e.

##### Metodi di lavoro nella tradizione musicale
1. Principi operativi(spazio nel quale si muove la musica)
  - modalità
  - tonalità
  - politonalità
  - dodecafonia
  - atonalità
2. Materiali di base(semilavorati della musica e suoi esemplari)
  - modi
  - scale
  - funzioni tonali
  - progressioni
  - accordi
  - serie
  - sequenze
  - etc...
3. Principi costruttivi(le varie forme, principio organizzativo formale)
  - forma sonata
  - contrappunto
  - fuga
  - aria
  - madrigale
  - etc...

##### Metodi di lavoro in Stockhausen
È impossibile separare ció che è materiale da ciò che è procedura, infatti in Stockhausen è operante un principio dialettico ed un cambio di prospettiva, infatti ció che in un momento è il fine diviene in una seconda fase un mezzo e uno strumento(vedi Punkte, Gruppen e Momente).

1. Principi operativi(spazio nel quale si muove la musica di Stockhausen)
  - serialismo
  - principio della relatività progressiva
  - intuizione, che nel mondo Stockhauseniano spiritualista ed esoterico che mira alla condivisione dei saperi, mondo lontano dalla storia, dalla politica e dall'impegno sociale, mondo lontano dalla contingenza; intuizione che serve come momento di totale libertà inventiva dalla quale nasce l'idea dell'intuizione come principio creativo.

2. Materiali di base(semilavorati della musica e suoi esemplari)
  - punti
  - gruppi
  - momenti
  - il processo
  - formula

3. Principi costruttivi(le varie forme e principi organizzativi formali)
  - serializzazione
  - processo(sia come materiale che come procedura dunque)
  - proiezione della formula

###### Principi operativi in Stockhausen
- **serialismo** è il principio per cui il compositore crea o prende in considerazione un dualismo, cioè una polarità, creando una tensione polare(esempio Gesang: massima intelligibilità parole contrapposta a solo suono e nessuna intelligibilità); dopo la creazione di un dualismo il compositore crea una scala che possa andare a A a B ovvero implemetando il serialismo, di cui la serializzazione è soltanto un esito finale. Il serialismo e la serializzazione sono la risposta che Stockhausen da a un problema di fondo che è il _creare uno spazio intermedio_ tra la totale prevedibilità e regolarità degli eventi (segnale periodico) e il mondo statistico del totalmente imprevedibile.
Nel mezzo c'è una quantità di gradi intermedi che possiamo simulare.

- **intuizione** essa fa capo agli inserti arbitrari (che possono essere momenti carnevaleschi, ovvero sospensione e sovvertimento delle regole), poi vedremo che la scrittura intuitiva sarà tipica di una composizione piú tarda.

###### Materiali di base in Stockhausen
- **parametri** caratteristiche che di per se da sole non suonano, ma che accorpate in una struttura, danno luogo ad un elemento. Mettendo insieme un'altezza, una durata, un timbro ed una posizione o collocazione, otteniamo un punto, ovvero un suono compiuto.


- **elementi** essi sono i mattoni della costruzione musicale.
  - **parametri dei _punti_**
    - altezza
    - durata
    - dinamica
    - timbro
    - posizione

  - **parametri dei _gruppi_**
    - tutti i parametri dei punti
    - direzione(ad esempio direzione delle altezze)
    - l'ambito
    - la densità dei vari parametri

  - **parametri dei _momenti_**
    - tutti i parametri dei punti e dei gruppi
    - organizzazione (assetto statico, ovvero il momento come si presenta nella sua forma intemporale, nella sua forma che si puó cogliere tutta insieme, come se guardassimo la partitura con un unico colpo d'occhio.)
      1. Gestalt(profilo complessivo)
      2. Struttura(cogliere gli elementi interni)
    - tendenza temporale (Nel momento non dovrebbe succedere nulla, ma trattandosi come qualcosa che deve muoversi nel tempo, finisce per avere un comportamento anche lui. Comportamento che va dal _non succedere nulla all'applicarsi di un cambiamento o di un processo_.)
      1. stato
      2. processo(ambiguo in Stockhausen ma che significa: come viene sviluppato il decorso musicale.)

  - **il processo** (riguarda solo alcune composizioni, il processo viene inteso come focus sul materiale) esso consiste in
    - caratteristiche del processo(applicabili ad altezze, durate, dinamiche, timbri, etc...)
      1. aumentazione(+)
      2. diminuzione(-)
      3. zero(=)
    - processi non conclusi(regole imposte con partitura da finalizzare)
      - Plus-Minus
      - Solo
      - Prozession
      - Kurzwellen
    - processi conclusi(regole imposte con partitura finalizzata, unici esemplari di musica concreta prodotta da Stockhausen escludendo Etude)
      - Hymnen
      - Telemusik

  - **formula** (essa è un brano di musica che può stare in una o due pagine, essa diviene una specie di mappa o carta geografica che descrive il brano, a partire dal brano Plus-Minus in cui viene applicato per la prima volta il _suono nucleo_ o _Zentralklang_ invece di un _suono come accessorio_ o _Akzidens_, essi sono tronco e rami, vi è un'idea che ci sia una struttura fondamentale che conferisce: carattere colore ed identità ad ogni evento sonoro. Sempre tenendo in mente un'idea di _galassia_. La formula è un brano di cui la realizzazione finale è la meta-partitura, ovvero musica fatta con la musica. elementi vengono congelati nella formula e sottoposti a un processo chiamato poiezione, in cui ogni elemento viene proiettato su una dimensione temporale piú grande attraverso la proiezione della formula, e dotato di questi suoni accessori che lo arricchiscono e lo identificano e gli danno carattere. **Nella formula gli elementi stanno alla formula, come in tutti gli altri elementi i parametri stanno all'elemento.**)
    - i suoni nucleo (strutture seriali che troviamo nella formula)
    - i suoni accessori (tipologia di suoni molto ben definiti, essi sono derivati dalla pratica del nastro magnetico)
      - eco (fenomeno indesiderato registrazione, ovvero quel particolare glissato che si ha quando si avvia il nastro)
      - pre-eco (fenomeno indesiderato registrazione, ovvero quel particolare glissato che si ha quando si avvia il nastro)
      - pausa colorata (sibilo del nastro vuoto)
      - variazione (mutamento realizzato su materiali di base)
      - scala o ponte (strutture di passaggio tra uno _Zentralklang_ ed un altro)
      - modulazione (velocità del nastro, variazione ritmica)

      Categorie|Elementi|Parametri|
      ---------|--------|---------|
      Suoni(deterministico)|Punti|Altezza,  durata, dinamica, timbro, collocazione
      Statistico|Gruppi|Direzione, ambito, densità
      Forma|Momenti|Organizzazione(Gestalt,Struktur), tendenza temporale(statica, dinamica)
      Trasformazione|Processo|Mutamento, cambiamento
      Composizione|Formula|Elementi(nucleo, accessori)


###### Principi costruttivi in Stockhausen
Essi possono essere suddivisi in 3 parti:
1. La **serializzazione** essa è un'operazione concreta, mentre il serialismo è un principio(che ci permette di stabilire 2 estremi all'interno di un possibile parametro), essa è un processo di distribuzione dei gradi di questa gamma. È quindi la disposizione di questi gradi intermedi tra la totale prevedibilità e il disordine.

2. La **genesi della forma**(aspetto piú problematico dei principi di Stockhausen), il _Neunerschema_(solido cubico a 3 dimensioni rappresentante 9 schemi interconnessi) ci permette di combinare ognuno degli elementi (iscritti) in 3 sottogruppi, da esse è psossibile ricavare uno schema specifico di ogni composizione che per Stockhausen deve avere una struttura unica. Ciò da carattere alle composizioni di Stockhausen per il quale ogni brano deve avere una struttura unica.
![Stockhausen_Paradigma/Stockhausen_I_prinicipi_costruttivi/gdf.png](Stockhausen_Paradigma/Stockhausen_I_prinicipi_costruttivi/gdf.png)
  - _forme di sviluppo_(A) esse sono drammatiche, come forma Sonata o Plus-minus
  - _forme per concatenazione_(B) come la suite, esse hanno in comune degli aspetti che caratterizzano tutti i brani al suo interno: forma, omogeneità tonalità, cristallizazione delle danze
  - _momentform_(C) sia tra i materiali che tra gli schemi formali, in essa a differenza delle forme per concatenazione ci sono tendenze statistiche in ogni Momente, anche se ogni Momente è un qualcosa a se stante e puó avere durate e dimensioni diverse
  - _punti_(1)
  - _gruppi_(2)
  - sottogruppi
    - _insiemi_(3) definiti "collective" non sono ne gruppi ne aggregazione di punti ma non sono scomponibili
  - forme
    - _forme determinate_(I) la partitura è totalmente scritta
    - _forme variabili_(II) forme in cui vi sono elementi di casualità ed indeterminazione, in esse vi è un elemento di casualità
    - _forme polivalenti_(III) forme che scaturiscono da processi (come Plus-minus), in esse la forma si struttura man mano che si procede all'allestimento dell'opera con tutte forme prevedibili ed incluse nel materiale di base


3. La **proiezione della formula**
Stockhausen non ci da nessuna dimostrazione concreta della proiezione della formula. Sappiamo che _Mantra_ e _Licht_ sono scritte con una formula. Allo stesso modo in cui la carta geografica è una rappresentazione in scala del mondo reale(c'è ovviamente una somiglianza tra carta geografica e mondo reale), la proiezione della formula è una rappresentazione della formula(che non ha per forza i contorni uguali alla composizione, non sono rappresentati in maniera letterali). Un singolo evento musicale puó essere sviluppato come discorso musicale.


#### Gli Esemplari(1) (composizioni esistenti)
Ciò che Stockhausen considera normante, sono quelle composizioni che Stockhausen ha analizzato nei suoi scritti, esse sono paradigmatiche, sono fondamentali per stabilire il suo magistero compositivo

- Op. 24 Webern -> concerto
- Op. 28 Webern -> uno dei quartetti
- Sonata per due pianoforti e percussioni di Bartók
- Jeux di Debussy
- Le marteau sans maître di Boulez (_ambiente di Darmstadt_)
- Canto sospeso di Luigi Nono(_ambiente di Darmstadt_)

#### Gli Esemplari(2) (composizioni nuove)
Tutte le composizioni realizzate da Stockhausen dopo i suoi studi(Etude, Studie I, Studie II)

## Schema finale
![Stockhausen_Paradigma/Stockhausen_I_prinicipi_costruttivi/schema.png](Stockhausen_Paradigma/Stockhausen_I_prinicipi_costruttivi/schema.png)
In questo schema vediamo messi in relazione i 4 cardini del paradigma Stockhausen, ovvero:

1. gli assunti fondamentali o valori di base
2. le immagini o metafore o modelli
3. i principi operativi
4. i metodi di lavoro, suddivisi in:
  - materiali di lavoro
  - principi costruttivi

In questo schema non figurano le strutture mistiche di Stockhausen, ma è presente la relatività progressiva di Stockhausen, ovvero quella capacità che ha un determinato materiale, processo, metodo o concetto di estendersi e di essere riassorbito a un livello superiore di operatività. Cosí come i punti sono riassorbiti nei gruppi, i gruppi estendo la loro concezione ai momenti e a loro volta momenti, punti e gruppi vanno a costituire il materiale geografico all'interno delle formule.

### Assunti fondamentali

In realtà _principio di relatività progressiva_ significa: ripensare un dato stadio del proprio sviluppo(scientifico, artistico o cognitivo) e ricollocarlo in una struttura piú complessa; questa complessità è crescente fino alla formula, dopodichè Stockhausen smette di render conto di questo processo, un po' per il prevalere della musica intuitiva ed un po' poichè _la formula rappresenta il tetto di questo processo, ovvero la galassia nella sua estensione massima_, in cui non si può postulare un ulteriore principio di espansione.

### Immagini
Questo _principio di relatività progressiva_ ha delle immagini molto eloquenti in:
- spirale
- galassia
- idea di continuum
- idea di mondo vibrante(ovvero la totalità delle cose è soggetta a vibrazione ovvero è il soggetto di una qualche attività di tipo musicale)

### Metodi di principio

L'idea di _mondo vibrante_ passa per il principio di _intuizione_, che è un germe di arbitrio, di non spiegato e insbiegabile che abbiamo all'intero dei brani di Stockhausen fin dall'inizio

Il _serialismo_ è più volte descritto e definito.


### Metodi di lavoro

Che riguardano a loro volta i materiali ben precisi:
- suono -> forma punti
- parametrici statistici(moltiplicati) -> formano i gruppi
- la forma -> produce momenti (momentform)
- processi di trasformazione legate a forme drammaturgiche -> legate a processi(applicabili a pezzi finalizzati anche direttamente dall'autore)
- principio compositivo -> formula(punto di arrivo)

### Osservazioni finali sullo schema
Coenen collega l'_intuizione_ solamente alla formula, mentre il principio del _serialismo_ (legato a spirale, galassia e continuum)è onnicomprensivo. Anche se il principio dell'_intuizione_ si dovrebbe estendere a tutte quelle zone oscure(carnevalesche, ovvero in cui vi è una sospensione delle regole), che inizia ben prima del momento della musica intuitiva.

Perchè Coenen collega l'intuizione direttamente alla formula?

Perchè i processi che conducono alla proiezione della formula(che è un principio costruttivo), restano in parte inspiegati per molti versi(rimangono nell'ombra). Sono stati fatti tentativi puntuali di ristabilire legame con la formula, ma è molto difficile realizzare discorsi che vadano al di là delle ipotesi.

Sia il processo, sia i momenti, i gruppi e i punti sono legati alla _serializzazione_(legame reciproco); la _genesi della forma_ è legata a momenti (ma in teoria anche ai processi dovrebbe esser legata); la _proiezione della formula_ è collegata al principio compositivo della formula.

È dunque da notare come Coenen colleghi il modello della galassia alla proiezione della formula, ovvero un modo di descrivere il principio geografico; ovvero che ci sia una dimensione globale, una dimensione locale e un continuum che interessa questa pluralità di scale e livelli.

Quindi il principo della relatività progressiva va ad informare di se tutto, ad esempio l'allargamento progressivo del serialismo a parametri che sono via via sempre piú staccati dal suono e sempre piú concettuali ed intellegibili(non percepibili), è un'applicazione del principio di relatività progressiva, che si applica anche alla serializzazione e alle varie forme di genesi della forma.

(Restano ovviamente dei punti oscuri, come la parte dei principi costruttivi, in cui si avverte una prevalenza di necessità(quasi medievale) di simmettria e coerenza dei numeri, rispetto al concreto operare del compositore.)
