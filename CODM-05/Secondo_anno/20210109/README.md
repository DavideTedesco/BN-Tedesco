# Appunti della lezione di Sabato 9 Gennaio 2021

Gesualdo Bufalino -> scrittore siciliano -> psicologo che sosteneva che il sud Italia era arretrato rispetto al Nord Italia

Certe cose si dicono per alzare l'asticella

Cacciari ha detto che è una follia tagliar fuori dai media una persona -> assurdità porta ad assurdità.

Padre pugliese e madre abruzzese ma nato a Como.
___________

Palermo come centro culturale della musica contemporanea italiana grazie al barone Francesco Agnello, delle Settimane palermitane della musica temporanea. Essa era organizzazione privata grazie al mecenate.

La sera del 1968 che si svolse un concerto con Cage e Nono, sera con MEV, Guaccero con "La scena del potere" ed altri...

- "Winter music" prima di Cage a Palermo
- "Contrappunto dialettico alla mente" pezzo per nastro magnetico più importante di Nono

Signora Guaccero, moglie di Domenico Guaccero, parlando con lei, del racconto della liberazione di Roma nel Giugno del 1944, essa racconto il fiasco totale della "Scena del Potere".

Guaccero è il protagonista della lezione di oggi con **Musica Ex-machina**

Libro di Mastropietro "Nuovo Teatro Musicale fra Roma e Palermo, 1961–1973", da comprare per consultazione e monumentale, e tutto ciò emerge nell'arte, nella danza.
[link articolo presentazione](https://www.ilmessaggero.it/abruzzo/la_presentazione_on_line_del_libro_di_alessandro_mastropietro-5649729.html)

## Chi è Domenico Guaccero?

Esponente di punta della musica romana, di origini pugliesi(Gioa del colle), egli era uno sperimentatore.

Sul modello del paradigma di Kuhn che si basa su un limite superiore o inferiore di cosa sia la musica(come il fatto che la musica si fosse fatta con suoni e note). La barriera delle note, venne a un certo punto abbattuta e divenne composizione di suono la musica.

Prendendo ad esempio il "Songbook" di Cage, troviamo alcuni brani anche senza suoni, ovvero l'idea musicale che la musica possa esistere anche senza suoni.

Guaccero è fra i musicisti che concepisce **l'idea di composizione da svolgere insieme tra i musicisti** in cui vi sono i materiali che sono di fatto scatole di montaggio con dentro:
- materiali grezzi
- schede di montaggio
- regole

Una partitura di Guaccero è proprio un gioco di regole.

(Ricordando Plus-Minus, esso viene prima di Guaccero e nasce come esercizio di composizione, come se Stockhausen, avesse lasciato uno sguardo ai suoi allievi di composizione per poterlo montare.)

Le scatole di montaggio di Guaccero sono piú lineari di Plus-Minus, non vi sono resti, o parti che restano nell'ombra, è tutto molto fattibile.

(Il brano Rota di Guaccero aveva un'interazione fra gioco e scrittura.)

Trascrizioni di Rota di Guaccero:
![rota](rota.png)
![rota1](rota1.png)
![rota2](rota2.png)

Ciò ricorda molto dei giochi con carte, potenze, famiglie di appartenenza delle varie carte.

(Guaccero aveva anche la passione per i tarocchi)


Altro brano importante, è la Sinfonia n.2 di Guaccero, che è un meta pezzo che deve essere riempito con materiali musicali.

![sinf2](sinf2.png)
![sinf2_1](sinf2_1.png)

Questa attenzione alla scrittura, che riportano solo ciò che è pertinente, in cui vi sono solo elementi in cui viene sottolineata l'importanza che vi è nel pezzo di un elemento.

___________
### Che differenza c'è fra un trillo scritto e la notazione di tutte le note da suonare?

Lo possiamo indicare in due modi perchè nel trillo non è importante che tu suoni in un certo numero di note, l'importante, ciò che è pertinente è il gesto, perchè nel momento in cui si scrivono tutte le note, chiederemo un'attenzione alle note diversa.

Nella musica occidentale, ciò che è pertinente sono:
- le note
- le altezze

Facendo un file MIDI, con la Quinta Sinfonia di Beethoven con le durate relative e le altezze, potró riconoscere la Quinta lo stesso.

Se posso avere durate random, non riconosco piú la quinta, se metto le note a caso, non la riconosco. Se modifico una nota del tema non potrò piú identificare il tema della quinta di Beethoven. Per identificare un'opera pertiene alle durate ed alle altezze.

Tutto il resto è sovra-segmentale(al di sopra della codifica dei singoli segmenti).

La stessa frase con toni e accenti diversi cambia le denotazioni e le connotazioni, si può dire che tutto ciò che non è altezza e durata, nella musica occidentale è sovra-segmentale.

Posso scrivere nell'Ottocento scrivere la parte per pianoforte e per cantanti, perchè l'orchestrazione non è pertinente rispetto all'opera stessa.(Come i "Quadri di un'Esposizione" di Mussorgskij con orchestrazione di Ravel)

Come l'opera Kovanscina di Mussorgskij che aveva orchestrazione di Rimskij-Korsakov, che vedendo certe combinazioni strane di note non tornano armonicamente, corregge le note. "Il problema dei copisti troppo colti", quando il copista è colto, il copista semplifica e banalizza dei testi, ed il lavoro del filologo consente di recuperare cosa vi è scritto davvero nel testo.

(I copisti tendono a banalizzare e a scrivere ciò che è troppo difficile, se il copista è colto.)

Il copista non colto fa recuperare il testo originale, proprio perchè egli non corregge gli errori.

(Brani: "Preludio", "Danza delle schiave persiane")

La stessa cosa avviene con Rimskij-Korsakov che corregge Mussorgskij, nella Kovanscina, le due versioni orchestrate:
- Rimskij-Korsakov -> orchestrazione corretta
- Shostakovic -> che fa la parte del filologo e orchestra l'opera come l'avrebbe orchstrata Mussorgskij

**Dall'età classica in poi si fisserà anche un terzo parametro, ovvero il timbro.** Ovvero dalla messa appunto dell'orchestra di Mannheim di Haydn e del quartetto, si fissa il timbro.

Ciò non impedisce di fare dei brani per pianoforte che vengono trascritti per orchestra, nessuno si permetterebbe di scrivere e dire che ciò sia un'altra cosa rispetto al resto.

Ciò non ha niente a che vedere con la "musica astratta", in cui ciò che serve per identificare l'opera sono:
- le durate
- le altezze

(Ciò è anche vero per brani puramente timbrici.)

**Viene mantenuto ció che è pertinente al riconoscimento dell'opera**, come i tratti non pertinenti di una persona sono quelli dell'età, ma la persona è la stessa.
___________

Abbiamo fatto questo lungo discorso sulla pertinenza, poichè Guaccero chiama delle cose aleatorie, con cose che non lo sono.
L'affidare le cose al caso, come puó succedere se abbiamo dei frammenti che possiamo decidere in che ordine suonarli.
Un buon algoritmo si accorgerebbe che è lo stesso pezzo anche se ripeto alcuni frammenti dello stesso brano in modo casuale.

Se invece tiro un dado e vi sono suoni sempre diversi, ciò definisce il concetto di alea.

Come la terza sonata di Boulez che fu considerata un pezzo aleatorio, e Scambi di Posseur è considerato un pezzo aleatorio.

Essendo però l'alea un procedimento molto casuale, vi è un certo numero di combinazioni possibili.

Vi è poi un'alea definita in Guaccero in cui **sopprattuto nelle grafie che non specificano la nota, ma il solo registro**, e per considerare se stessa quell'opera, è il fatto che le note capitino in quel registro. Interessa dunque che il parametro avvenga all'interno della finestra, e non è importante l'elemento singolo.

Ligeti scrive tutto perchè toglie all'esecutore ciò che vuole scegliere.

(Spettralismo di Murail prende gli spettri sintetici, e li riporta nel dominio della musica, esso è un processo all'indietro molto praticato, come Maderna e Ligeti. Riportare gli spettri sintetici nel dominio strumentale era di fatto una gran furbata.)

(Lo spettralismo rappresenta il caso limite, in cui tutto è troppo pertinente, ma lo è anche in maniera sensibile, poichè non posso cambiare assolutamente nulla.)

Con la musica di Guaccero, ciò che non viene specificato non è pertinente, come "una nuvola di 5 note" suonata un agglomerato di grani. Vincenzo Caporaletti ci ha insegnato il Paradigma Audio Tattile che fra i suoi tanti elementi tiene conto dell'importanza della memoria delle mani nell'improvvisazione.

(Vincenzo Caporaletti il Paradigma Audio Tattile -> essa è una terza via per collocare il Jazz ed il Rock, tra tradizione orale e scritta -> inizia a definire che l'incisione sul disco introduce un paradigma che imparo l'assolo ascoltando e non leggendo -> ció che nasce con un'improvvisazione -> la musica incisa diviene testo attraverso l'improvvisazione -> tanti elementi divengono testo in cui vi è il fenomeno che "il pubblico identifica l'errore da un testo inciso")

(Spesso l'improvvisazione non è composizione estemporanea, ma è memoria delle mani-> come la pentatonica è un gesto si realizza come agglomerato, non è composizione nota per nota)

(John Ruso era il pianista dei dischi storici di Pino Daniele -> e fu il maestro di John Ruso per l'improvvisazione)

Spesso quello che non è pertinente è affidato a stimoli neuromotori, ovvero a stimoli audio tattili.

Quindi quando posso scegliere tra una lista di note e suonarle in modo che voglio, le note che scelgo non sono pertinenti, la velocità, e la grana lo sono. E ció che non è pertinente non vi è descritto.

**L'opera di Guaccero conserva sempre queste caratteristiche?**

Essa è dagli anni '60 con queste caratteristiche, ma non tutto è con queste caratteristiche. La cretività va al di là della creazione finita con sistemi compositivi in grado di produrre tante istanze di se, e lo è prevalentemente l'opera dell'ultimo decennio di Guaccero.

**Come arrivo a Guaccero la spinta per queste idee?**

Da spunti di Stockhausen, Bussotti, Kagel...
Guaccero non era ne un isolato ne un precursore, che si introdusse in una tendenza e in un'epoca.

Tra i brani vi è Luz, fra i 3 prezzi che descrivo il ciclo di "Descrizione del corpo", la lettura va fatta con un ciclo a spirale. Guaccero ha delle sue idiosincrasie grafiche che qui non sono presenti.
![luz](luz.png)

Nel brano "Kardia" abbiamo una parte corale, una parte per archi ed una parte per fiati e si puó scegliere se suonarle insieme o singolarmente, e si tratta dell'idea che la scrittura crei non una descrizione puntuale di quello che deve succedere nella musica, ma venga creato un insieme di possibilità.  

Ogni sezione della sinfonia presente sul lucido è uno schema organizzativo.

![sinf2_2](sinf2_2.png)

Intervento sul tema delle grafie di Pizzaleo, in cui la strada di Guaccero verso le grafie evolute, in cui come concepisco le partiture e la creazione di sistemi di scrittura e generazione dei sistemi musicale che non ha niente a che vedere ne con l'alea ne col grafismo.

(Il grafismo puó essere ad esempio un brano di Pennisi o Bussotti.
![bussotti](bussotti.png)

e la ratio spesso non è legata alla scrittura musicale ma al grafismo.)

## Cos'è "Musica Ex-machina"?
È 3 cose:
1. "Musica Ex-machina" -> saggio di musicologo tedesco Prieberg
2. nome di un ciclo divulgativo di alto livello in cui vengono affrontati i diversi temi della musica elettronica di maggiore attualità, in onda sul terzo canale della Radio Rai, con conduttore Guaccero: in cui essi sono i titoli![tit](tit.png)(la quinta puntata del 1967(momento caldo dello sviluppo della musica elettronica italiana->momento di autoriflessione) è molto importante per esponenti di MEV e improvvisazione di Ketoff e Guaccero, esso è uno dei primi interventi di storicizzazione (storicizzare significa collocare qualcosa e prendere consapevolezza e coscienza di una separazione fra un tipo di pratica(musica dal vivo e su nastro) che pone una serie di problemi))  **La musica elettronica si affaccia a una soglia e dedica uno spazio a ciò.** Nel 1967 siamo siamo al vertice di una parabola.
3. È un gruppo di improvvisazione musicale, che nasce come sodalizio tra: Guaccero(reduce da R7 e CMS), con Luca Lombardi e Alvin Currun(reduce di MEV e inzió proggetti solistici)

### Cos'era CMS?
Nel 1972 Guaccero fondò CMS(Centro per la Muscia Sperimentale), poichè in R7 vi erano troppe personalità di spicco e troppe individualità che portavano idee differenti. I giovani lavoravano ma gli altri avevano idee poco compatibili(idea commercio e arte).

In questo centro Guaccero sarà coordinatore, maestro e guida degli altri.

Guaccero scrive che i problemi di queste tipologie di associazioni sono gli stessi di tutte le epoche, perchè ha bisogno di fondi dal Ministero: ![cms](cms.png)

Tra le prime iniziative vi fu un **corso di musica elettronica per compositori** tenuto nell'istituto di cultura Italo-Latino Americana che era fino al 2011 uno dei luoghi di riferimento per la musica elettronica romana.

Il corso si basava sul fatto che bisognasse sensibilizzare una chiamata alle armi per creare un blocco piú esteso di personalità interessate alla musica elettronica. Si sentiva infatti poco interesse verso la musica elettronica che aveva poco a che vedere con la scienza e la tecnica elettronica.

(Il compositore era fino al 1960 tra il maestro di cappella e il letterato.)

Il corso fu da questo punto di vista un successo, come da persone come Pennisi.

![corso](corso.png)

Un altro punto era quello di avere **un gruppo di improvvisazione elettronica**. Come fai a realizzare un'improvvisazione elettronica, con qualcosa programmata offline per eccellenza?
Cosa a che vedere con l'improvvisazione?
Sappiamo che a Roma vi era una storia di improvvisazione ma essa non era realizzata con la musica elettronica, ma realizzava il massimo con gli strumenti tradizionali.

A loro non interessava avere una connotazione audio tattile dell'improvvisazione ma essi lavoravano direttamente sugli strumenti.

Musica Elettronica Viva improvvisava con organi elettrici modificati, microfoni a contatto (Tudor), con diavolerie elettroniche con amplificatori di onde cerebrali, oppure con il Moog, in cui MEV possedeva dei Moog, che vennero prestati a Teutelbaum per farli conoscere fuori dagli stati uniti.

Per improvvisare con l'elettronica, paradassolamente devi un po' "tradire l'elettronica" come dice Di Scipio.

Vi è il Synket in circolazione che è per ora la unica strada praticabile.

(Un Synket ha ripreso vita al CRM.)

Il sintetizzatore alla fine diviene l'unica strada praticabile "si inizia a campeggiare nel cortile di casa", e si inizia ad utilizzare il VCS3, ovvero il sintetizzatore piú importante del momento all'epoca.

La tastiera non viene mai utilizzata con il VCS3, ma viene usata la matrice e vengono usate una serie di manopole per usare il timbro.

Come interveniamo sul sintetizzatore?
- con le mani sulle manopole
- con la matrice

Solamente che l'interfaccia dello strumento che è pensata per l'allestimento e mi costringe a fare dei movimenti non semplici, ovviamente avendo piú mani potremmo realizzare un qualcosa di piú complesso.

Le cose su cui si interviene per cambiare il suono non sono da suonare, ma da preparare.

### I limiti di CMS
**La difficoltà di acquisire un gesto strumentale su un'interfaccia musicale senza tastiera**, fa si che il limite di ciò che sto facendo e il ripetersi di cosa sto facendo fece fallire il progetto.

L'altro limite fu l'impossibilità di creare i concerti.

#### Il nome originale di "Musica Ex machina"
Il nome originale fu "I synthesizers" ovvero cercare di creare un'immagine ed un proporsi al mercato popolare.

##### Quale era lo scopo iniziale di questo nome?

Guaccero voleva allargare l'interesse aldilà del pubblico tradizionale borghese di matrice colta, e che era un sognatore istancabile di nuove configurazioni sociali.
