# Appunti della lezione di Sabato 13 Febbraio 2021

## Musica Elettronica Viva

Essa è inizialmente un arcipelago, una costellazione di individui delle più diverse provenienze, raccolte ad esempio dalla musica da film romana con Vittorio Gelmetti.

Vi sono artisti visivi, gruppo Fluxus e contatti con gli USA come Christian Wolff, Larry Austin etc...

Mondo di figli e nipoti di John Cage e Tudor, che troviamo nel libro MEV nel capitolo correlazioni.

Vi è una continuità con il gruppo Nuova Consonanza, poichè vi fu una presenza di Jewsky nel gruppo GINC, che tra l'altro produsse un disco per la Detusche Gramophon.

(In genere se pubblico un libro con una casa editrice importante, ho una sorta di validità e processo di garanzia e revisione. È il motivo per cui scriversi da soli i libri di scuola è una stupidaggine.)

Il disco pubblicato con Detusche Gramophon, era come un'avanguardia che gioca a Football americano, che può permettersi di pubblicare un disco con una delle più importanti case discografiche di musica classica al mondo.

### Chi è Jewsky?

Tralasciando i commenti che fece sul pianoforte, ovvero che non suonerà mai il pianoforte...

La verità è che Jewsky, Tudor e i fratelli Kontarsky, sono gli unici pianisti in grado di interpretare la musica contemporanea.

Jewsky è ben piantato nel mezzo del produttivo...

Libro su Alvin Curran, Die Schactel, curato da Daniela Tortora, si trova che "Curran potè fare concerti anche grazie alle conoscenze di Jewsky".

_______
Un disco con la Detusche e Gramophon:
1. avanguardia che viaggia in treno -> musica universitaria, accademica, Babbit
2. avanguardia che scende e cammina sui binari -> cose alla John Cage, con forte impronta politica -> radicalismo di sinistra, di tipo non allineato e _sessantottesco_ orientato ad aspetti libertari, sinistra radicale che si basa su posizioni di filosofi, che èè un **modo di intendere l'arte penetrato di anarchia**

Questi due mondi alternativi, di chi scrive sinfonie e _concert pieces_, ovvero i _serious composers_, e i sbidonati, anche se vi è una continuità fra i due mondi, poichè entrambi sono borsisiti dell'accademia americana. Musica Elettronica Viva, dischi con la Detusche Gramophon etc... Ed il fatto che tornano come docenti alle università della Ivy League dove tornano a insegnare, e le premesse che uno al culmine della sua attività radicale, torna a insegnare in una top university americana.

Maturità da pompieri che interesserà tutti...

### La costellazione di Musica Elettronica Viva

- **Fredric Rzewsky** -> con background piú consistente
- **Allan Bryant** -> personaggio di Detroit, pazzoide che costruiscono da un organino elettrico, il raggio della morte, hacker di strumenti elettronici, capace di scrivere pezzi per elastici di gomma, e per gli strumenti scrive tonale, vi è un'idea scorrelata tra l'avanguardia degli strumenti e il suono e un'idea di avanguardia, di scrittura tonale -> geniale inventore di macchinari fai da te
- **John Fettleys** -> violoncellista, unico che non aveva avuto una formazione universitaria di alto livello, ed è l'unico che quando il gruppo comincerà a valorizzare la musica della strada, quando il professinismo verrà discriminato(sembra che nelle improvvisazioni, chi è bravo a suonare, non debba farlo vedere, perchè cerca di tirar fuori le radici che si vogliono rifiutare) -> quando questa violenza diviene estrema, lui lascia il gruppo -> perchè vuole divenire bravo, e vuole riconoscimento -> egli rinuncerà totalmente alla musica e si dedicherà alla fotografia (Fettleys papers)
- **Alvin Curran** ->
- **Richard Teitelbaum** -> scomparso lo scorso anno
- **Ivan Vandor** -> sassofonista tra i pochi specialisti di tecniche estese, che suonava prevalentemente in orchestre da ballo, con una solida base accademica, e conobbe in tarda età un pentimento, divenendo un musicista conservatore, con un percorso simile a quello di Henri Posseur, mentre Vandor ebbe prese di distanza dalla sua esperienza giovanile. Di origine ungherese, ma italiano a tutti gli effetti, unico italiano in MEV.

Jewsky, Teutelbaum e Curran erano i tre musicisti che continuavano a dire che MEV esisteva ancora...

In realtà MEV dopo il 1970 è stata un'altra cosa, gruppo di ricerca tra il free jazz e musica elettronica che imbarcò altri soggetti e che ebbe un giro nelle università americane.

Questo magma di personalità non necessariamente legate all'avanguardia e alla musica, si addensa nei festival di avanguardia musicale nell'autunno 1966 e autunno 1967.

(lettura da pagina 37)

Mentre Avanguardia Musicale I, cerca di portarsi a casa una serie di concerti, Avanguardia Musicale II, è piú forte e organizzato ed invita altri gruppi ed altri.

### Luoghi della musica elettronica romana
I luoghi della musica contemporanea a Roma sono conservativi:
- sala Casella della filarmonica romana
- chiesa di San Paolo fuori le mura
È in qualche modo confortante, che esista una tradizione di luoghi che sono dediti alla musica contemporanea, come una rocca forte della musica contemporanea.

Altro luogo della musica contemporanea di roma è la chiesa di San Paolo fuori le mura, essa è la chiesa americana a Roma.

### Evento fondativo di MEV

Sono le due parti dello stesso festival "Avanguardia Musicale", che vede una concretizzazione del gruppo MEV, e si puó dire che esso sia quel gruppo di 5 persone.

### Il nome Musica Elettronica Viva

Il nome è frutto della fantasia di Vittorio Gelmetti.

Il gruppo MEV è l'avventura di un gruppo di ragazzi prima di un gruppo d'avanguardia.

Dunque il nome è un gruppo lessicale, e lo stare insieme è costruttivo in se...

Il titolo è interessante perchè non traduce Live Electronics Music, perchè essa si traduce in musica elettronica dal vivo, mentre Musica Elettronica Viva, è musica elettronica vivente.

Dunque non era importante avere struemnti elettronici, ma recuperare la musica elettronica a una pratica estemporanea rispetto a una percezione iper scentifica di Stockhausen.

A questa dimensione che verrà definita tecnocratica(Cornelius Cardew è autore di un libro, Stockhausen serve l'imperialismo), vi è in contrapposizione MEV.

Intorno alle idee di Roma come il Synket, a Milano vengono derise le idee romane.

### A cosa serviva MEV?

- contro lo scienitificismo
- contro l'istituzionalizzazione  

Per MEV gli strumenti non sono fondamentali per portare alle estreme conseguenze la tecnologia, ma constatare che una tecnologia può portare una teconologia all'estremo, come i microfoni a contatto portati allo stesso piano di altri elementi sonori.

Equiparando le forme sonore in modo eterogeneo senza pensare a frequenze, hertz etc...

Eppure accanto a questa idea, si affacciano struemnti come il primo MOOG modulare, e si sviluppano delle ricerche sull'esecuzione musicale basate sulle onde cerebrali. Ma a MEV questa ricerca interessa molto poco ma interessa produrre suono, creare situazioni e istanze sonore...

Alvin Lucier nel 1965 aveva già creato uno strumento per utilizzare le onde cerebrali, con "Musica er solo performer"...

Non è interessante l'aspetto scientifico da portare alle estreme conseguenze, ma è importante l'aspetto percettivo e sonoro.

### Stagione del 1967

Passare la propria esistenza suonando un oggetto, facendolo nel chiuso della propria stanza. Ciò ci porta alla stagione del 1967.

(spesso in questo periodo era difficoltoso distinguere i pezzi scritti da quelli improvvisati)

Nel 1967 inoltrato MEV, mette su una improvvisazione collettiva definita **spacecraft**, immaginario fantascientifico POP, che sottende e serpeggia e fa realizzare questa astronave.

L'astronave è la metafora del viaggio, anno in cui esce il primo disco dei Pink Floyd.

Questa metafora è presente in molti contesti artistici...

#### Spacecraft

È una composizione che si basa su alcune procedure di impprovisazione(anni del free jazz Anthony Braxton, Art Ensemble of Chicago) in che modo ci si può mettere in relazione:
- contrasto
- ignorare
- risposta

Progressivo accordarsi dell'ensemble per raggiungere un suono collettivo dotato di una sua distinta impronta.

Cercano un "sound" per raggiungere caratteristiche di soddisfazione, e questa interazione puó verificarsi oppure no...

(Braxton è uno dei maestri del '900)
(viste le partiture di Anthony Braxton)
(Cornelius Cardew, il grafismo e le partiture nel Treatise -> strutture grafiche in cui ogni elemento può essere funzionalizzato, volendo si possono funzionalizzare gli aspetti strutturali e dare un significato ad ognuna di queste immagini che ogni tanto sconfinano nella grafia musicale e non necessariamente)

(Notazione musicale contemporanea di Valle)
(Semiografia della nuova musica di Donorà)

#### Improvvisazione

Essa è un fenomeno altamente significativo per chi la compie, ha dei caratteri di ritualità come la messa che un prete puó anche far da solo.

In altre parole l'improvvisazione è un "rituale significativo" per chi improvvisa, ed è importante l'esempio di chi si chiude in camera e comincia a suonare, e ciò è significativo per se, ed è gratificante per l'armonia compiuta che si raggiunge, partendo ognuno dal suo labirinto e si raggiunge l'armonia quando si raggiunge ognuno il labirinto dell'altro.

Ed i luoghi comuni, i pregiudizi costruttivi non aiutano ad entrare nel labirinto dell'altro e liberandosi delle proprie tradizioni personali, si raggiunge un qualcosa di diverso.

(Scelsi è una figura fondamentale di questi musicisti)

### Crisi di Spacecraft e come si arriva a Zuppa

I confini dell'opera, vengono a mancare.

Spacecraft non era stato capito dal pubblico e il pubblico diceva che non era divertente per loro.

Rzewsky allora va in crisi, ed il problema non è piacere al pubblico ma pensare ancora sulla liceità della nozione di linguaggio musicale, e ci si chiede anche una suddivisione di chi ascolta e di chi esegue.

Lo stesso termine _esecuzione musicale_, toglie valore all'esperienza spirituale, all'esperienza personale del musicista e dell'ascoltatore stesso.

Dal momento che viene criticato tutto l'apparato borghese:
- concerti
- sala
- silenzio in sala
- divo
- virtuoso
- commissione

Per l'attività che è l'improvvisazione collettiva con mezzi sonori di qualsiasi competenza e di livelli sonori di qualsiasi livello, c'è da chiedersi se non bisogna mettere in discussione la distinzione fra pubblico e musicista?

Anzichè fare una musica con un linguaggio piú gradevole restando nel territorio dei miti e degli stereotipi, in cui non è il pubblico che va in contro all'esecutore, ma il contrario.

La traduzione zuppa, non ci da la stessa idea di _soup_ in inglese.

#### Zuppa

- 1 steeldrum grande
- vecchio pianoforte verticale con musicista annesso
- moduli di synth moog con musicista annesso
- due lastre di vetro risonanti
- una latta di olio vuota
- un kalimba
- pianoforte giocattolo
- set di campane di vetro
- antenne televisive di varia lunghezza
- piatti sospese
- piatti indiani
- campanacci da cammello
- woodblock
- set di campane
- xylofono giocattolo
- ...

Ingredienti della zuppa...

Loro aprono per 6 settimane di fila il loro studio al pubblico, accogliendo un numero di persone con una media costante di 30/35 persone.

Gli strumenti con esecutore sono quelli che suonano il gruppo, il resto, sono strumenti che suona il pubblico.

Vi è una figura di facilitatore per far suonare il pubblico.

Il pubblico deve entrare in un insieme armonico con gli altri, in questa specie di giardino con i dovuti linguaggi.

>Zuppa non è una performance, non è un pezzo, non è un'improvvisazione collettiva fatta dal gruppo, non è un happening.

**Questa è una modalità del tutto nuova, in cui anche il contributo del pubblico possa dare un qualcosa.** Rispetto agli happening americani cagiani, vi è una gerarchia del musicista.

>Ciò deve essere un qualcosa che invoglia ad armonizzare.

Questa modalità oltre ad essere un'apertura molto democratica e concreta del farsi capire, allora se vogliamo farlo capire veramente, lasciamo che il pubblico lo faccia insieme ai musicisti.

In tutto ciò vi sono degli embrioni di rovina, questo tipo di scelta, va verso la distruzione di ogni valore di costruzione e di valore musicale.
Ma per dare una coerenza a quello che stai facendo, si va verso la progressiva emarginazione del professinismo, ed è per questo che Fettleys va via.

(Phetteplace capisce che prima o poi i compositori torneranno all'ovile nelle accademie americane ed egli doveva costruire con fatica il suo professionismo...)

Questo atteggiamento di radicalismo antiprofessionale fa andar via Phetteplace e Bryant.

MEV ha segnato il suo suicidio preordinato.

### Soundpool

La fase successiva di MEV, nel 1969 è il Soundpool, e portarsi dietro qualche cosa per suonare.

Ciò è un momento di estrema radicale ribellione a degli schemi musicali e sociali che risalivano all'epoca di Bheetoven ed Haydn.

(trasformazione di ogni pratica in un genere, che nasce dal fatto che quando ci mettiamo su un percorso artistico di qualsiasi genere, il raggiungimento di una credibilità è l'obbiettivo finale -> Rino Zurzolo)

Si è compiuto quini un sacrificio rituale del gruppo, poichè vi è stata una dissoluzione del gruppo in un soggetto musicale plurimo indeterminato, come si fa a ritrovare il linguaggio. Come si fa a tornare al figurativo dopo che vi è stato l'informale.

### Hippie o Tribù

Lo stesso gruppo MEV nel tempo aveva sviluppato una tale idiosincrasia nei confronti del professionismo, ed erano coinvolti, amici, famiglie etc... era una sorta di comune hippie e correva il rischio per aver voltato le spalle ad un mondo borghese, abbracciando un etica di tipo tribale. Rischio che Curran e Rzewsky descrivono in maniera chiara, tribù in cui vi sono capi, regole, che rischiano di essere piú pressanti nel passato i vincoli borghesi.

Il gruppo non ha piú niente a che vedere con la musica, ed il gruppo non è piú un gruppo di musicisti. Le performance diventa un soggetto collettivo infinito che esplode di energia, ed il disco con dentro Soundpool è come una registrazione delle manifestazioni di piazza, ma ci si perde l'aspetto piú importante, ovvero la percezione sonoro.

La musica è l'ultima delle preoccupazioni in quel contesto.

### La rinascita di MEV

MEV quando rinasce non può tornare piú come prima.

### Interfacce

Vi è un ragionamento sulle interfacce e sulla creazione di nuove interfacce.
Come la realizzazione di un mixer con fotoresistori, con interfaccia in quanto modo di gestire, compiere gesti che abbiano la complessità del suono.

Gestualità sull'interfaccia sonora.

### Ricerca sulle onde cerebrali
Ricerca spettacolare sulle onde cerebrali fatte da Alvin Lucier, che voleva capire le conseguenze del mappamento delle onde su strumenti acustici, mentre per Teitelbaum era un qualcosa di spettacolare.

#### Biofeedback

Sonificazione dei segnali basati sul corpo umano.


Ricerche tecnologicamente cosí evolute venivano messe sullo stesso piano della tromba rotta e della latta vuota.

(Di Scipio ricerca del suono nel suono, andare a cercare il suono lí dove usualmente non vi è suono.)

__________
Trevor Wishart e la nozione di continuum in musica
