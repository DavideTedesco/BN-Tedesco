# Appunti della lezione di Sabato 12 Dicembre 2020

[Mark Fisher](https://it.wikipedia.org/wiki/Mark_Fisher_(teorico))

Termine: Subcultura -> cultura privata di alcune parti riducendo tutto all'interfaccia grafica

Perchè la cultura anglosassone continua ad esprimere dei punti di cultura che continua ad esprimere.
_________
## Argomenti del corso

Due argomenti musicali molto diversi con tempi e situazioni diverse.

1. Il panorama della musica elettronica romana (bibliografia di Pizzaleo)
2. Sonic Art di Trevor Wishart

Produrre un elaborato scritto partendo da Sonic Art e opere di Trevor Wishart, con le 6 composizioni di Vox.

Stendere un elaborato di 4/5 cartelle, spiegando chi è Trevor Wishart, cosa ha fatto e perchè è importante.

Importante esercitarsi e confrontarsi con una critica musicologica, imparando a far qualcosa di concreto. Con la presentazione di un autore con un testo di accopagnamento che presenti l'autore.

Trevor Wishart è un uomo delle periferie, della classe operaia e testimone della cultura inglese.

### Trevor Wishart
- grande compositore
- grande teorico (On Sonic Art) -> rispetto a una teoria della musica elettronica che si appiattisce sugli strumenti elettronici ("La filosofia di Minerva canta al calar della sera."e La musicologia canta ora per l'elettronica con musica per tape.)

Come frontiera di ricerca, l'acusmatico ha già detto quello che doveva dire, e ci si concentra su altri punti di ricerca.

(Guardando le conferenze NIME, sulla nuova liuteria e le nuove interfacce. Non potrebbe essere che il senso delle operazioni è nel modo in cui sono prodotte?)

(Convegno a Siviglia in cui vi era anche Jean-Claude Risset -> quale musica per queste nuove interfacce -> non è che il mezzo prende il sopravvento, è sempre leggittimo il primato del contenuto al mezzo)

## Che senso ha stabilire un criterio geografico culturale per stabilire un ambito musicale

Esso è un criterio sovraimposto, ha una sua ragione intrinseca?

(Musica elettronica a Rieti, con Composint, Rieti Elettroacustica, Sabina Elettroacustica, Classi di Musica Elettronica alla sede distaccata del Conservatorio)

C'è qualcosa che distingue su un piano storico, culturale e stilistico la musica elettronica Romana da tutto il resto?

### Il carattere eccentrico della musica elettronica romana

Testi che parlano di uno studio elettronico della Filarmonica Romana, attrezzature di Marinuzzi, ma esse erano in un sottoscala della filarmonica Romana.

Vi è lo studio R7 che sembra vada in opposizione allo studio di Fonologia, ma che non lo fu proprio.

### Huge Davies, il censimento

[testo di Davies](https://archive.org/details/InternationalElectronicMusicCatalog)

Produsse un catalogo monumentale di tutta la musica e gli studi di musica elettronica al mondo, e benchè esso sia un volume ponderoso.

In passato era tutto visto in uno spazio catalogato di musiche, in cui scegliere se indossare un autore è una scelta di stile e non piú una scelta estetica.

Oggi ogni stile è a nostra disposizione...
É normale che invece di rivivere i ricordi si possano avere tutte a disposizione.

La musica contemporanea è diventata genere(in alcuni database è definita come Non-music)

La dialettica di Adorno torna sempre, perchè in un rovesciamento dialettico diviene negativa.

Come è possibile che certe linee siano nuove e poi diventino vecchie.

### Il catalogo di Davies

In questo catalogo veniva suddiviso in città, ed era possibile constatare delle differenze tra la realtà romana e milanese.

Poichè lo Studio di Fonologia di Milano era in linea con il mainstream con la musica d'avanguardia contemporanea, e lo studio di Fonologia di Milano era avanguardia ed aveva da dire qualcosa agli altri.

#### Che differenza c'è tra Milano e Roma quindi?
Vi era un'asse Milano-Germania e Roma era completamente periferica.

##### Studi di Milano
1. Studio di Fonologia di Milano (80 pezzi)
2. Casa privata di Giorgio Gaslini(jazzista e compositore) -> che scrisse un paio di pezzi con l'elettronica (2 pezzi)

Vi era un bisogno di completezza che portava a censire tutto.

A Milano veniva prodotta essenzialmente musica per nastro magnetico.

Si andava alla Studio di Fonologia per scrivere pezzi per nastro magnetico.

Inoltre il fatto spesso descritto come superamento del dualismo tra concreto e sintesi non è vero totalmente, poichè si realizzava piú che altre il nastro magnetico.

Milano voleva avere una dimensione storica di superare Parigi, principalmente musica su supporto magnetico e che esaurisce nel nastro magnetico il suo testo.

##### Studi di Roma

Vi sono 14 studi di musica elettroacustica.

Come si spiega tutta questa presenza?

Quali sono le peculiarità di questi studi?

1. studio di Marinuzzi recensito 3 volte
2. vari studi recensiti come film studio -> studi di post-produzione cinematografica
3. studio dell'accademia americana in Roma(che tuttora è al Gianicolo)
4. studio R7 -> era fatto da 7 persone tutte animate da motivazioni e spinte diverse che si sono aggregate per guadagnare un po' di soldi -> l'attività di R7 nasce nel Febbraio del 1968 e nel 1970 era già finita -> è importante per ragioni storiche e non per la produzione
5. unici studi statali ed istituzionali, ovvero la Discoteca di stato (ICBSA) -> che produsse un disco di Gelmetti e De Blasio
6. altro studio istituzionale, associato allo Poste e Telecomunicazioni che era in viale Trastevere e vi era un Laboratorio di Ricerca Acustica dotato di camera anecoica, microfoni etc... Una realtà che aveva uno scopo di ricerca tecnica con un formidabile bollettino che usciva semestralmente e che conteneva tutti i dati sulle ricerche, ovvero la testimonianza di un'epoca di grande fermento civile. C'era l'idea che un laboratorio di stato che doveva produrre ricerca. Anche le collaborazioni prodotte da Gelmetti non erano istituzionalizzate. (Visitare la biblioteca di Poste e Telecomunicazioni)

Molte di queste composizioni sono descritte come MEV(Musica Elettronica Viva)

7. Davies recensisce anche MEV che era un gruppo di americani che erano in Italia per studiare con Petrassi. Con generose borse di studio. Provenienti dalle grandi università americane come Harvard, Yale etc... Studio in Via Poretti a Trastevere.

Alvin Curran arriva a Roma dopo un corso con Stravinskij e con il suo maestro Elliot Carter

##### Conclusioni sugli studi di Roma e Milano
Soltanto lo studio di Poste e Telecomunicazioni produsse qualcosa, e non vi era una produzione continuativa, ma erano compositori che cercavano di mettere il naso realizzando qualcosa.

Non vi è un "castello" a Roma, ovvero uno studio di Fonologia che detta le regole, e che è abbondantemente foraggiato dallo stato, ma non vi è neanche uno studio che detta la linea a Roma. Allo studio di Fonologia di Milano si poteva accedere solamente con certe credenziali ed intenti.

A Roma non vi è l'approfondimento tecnico come vi era a Milano. Poichè Gelmetti scrive per film o ad esempio musiche per il quattrecentesimo anniversario per la morte di Michelangelo(Rotondo) come musica drone di sottofondo per una mostra.

Vi è una musica indipendente da una linea, come quella milanese, non vi è un castello.

(Scelsi non era della generazione di Berio e Maderna, ma Scelsi era della generazioen di Petrassi ed era un outsider che ragionava in termini estetici come quelli di Varése, intrisa di esoterismo e simbolismi. Questi esoterismi hanno finito di danneggiare la figura di Scelsi.)

#### Roma e America

La musica elettronica romana ha un contatto privilegiato con la realtà degli stati uniti.

I musicisti di MEV erano tutti americani tranne Ivan Vandor, tutte persone transitate per l'accademia americana.

John Eaton era fra questi musicisti, che usò fra i primi il Synket. Uno dei primi sintetizzatori al mondo realizzati per l'accademia americana che fu realizzato da Paolo Ketoff. Ketoff si definiva liutaio elettronico.

Il Synket suonava già di suo e dunque un po' come Marino Zuccheri a Milano esso era uno strumento che faceva realizzare...

##### I compositori americani a Roma

Essi si possono suddividere in due grandi classi:
1. Compositori di matrice americana accademica(che hanno origini da Copland e Barber)
2. L'altro è l'avanguardia estrema che "decisero di scendere dal treno camminare sui binari" -> dedicarsi a una ricerca libera -> progressiva deleggittimazione dei saperi acquisiti in favore della totale libertà espressiva, con Cage e Tudor, massimi esponenti. (Per Cage i suoni prodotti come musica elettronica sono suoni tra gli altri, non vi è un primato elettronico.) **Acquisire tutti i suoni possibili**
  - hackerare organini elettronici
  - mito del microfono a contatto(cartridge music)

##### Cinecittà
Il rapporto con l'America ci porta anche ad altro, ovvero a Cinecittà, ovvero tutto l'apparato che costava meno per la produzione filmica.
La produzione elettroacustica di Roma era spesso la necessità di una produzione veloce ed efficace della colonna effetti di Roma, che fece realizzare il PhonoSynth. Esso era una vera e propria stazione realizzato da Ketoff e Marinuzzi. Grande quanto un paio di armadi.

Radiodramma voci dell'infinito del 1958, scritto da Pellegara(orchestrale) e Marinuzzi(elettronica), elettronica di tutto rispetto. Ma Marinuzzi era un compositore di colonne sonore, e mentre a Milano chi faceva musica elettronica faceva musica d'avanguardia, colta; ma a Roma chi faceva musica Elettronica, non hanno nulla a che vedere con il mondo della musica contemporanea, ma hanno piú a che vedere con il mondo della musica cinematografica e televisiva.

Musica elettronica applicata viene anche prodotta da Roman Vlad.

Quando Roman Vlad ha dovuto scrivere il suo pezzo di musica elettronica da mettere in catalogo, l'ha prodotta a Milano.

##### Synket e la produzione cinematografica
A Roma si faceva sempre in fretta a produrre musica. Il Synket ce l'aveva Morricone, John Eaton, lo studio R7.

MEV e Nuova Consonanza sono realtà diverse, ma hanno punti di contatto.

Nuova Consonanza significa(GINC): Evangelisti, Branchi, Bertoncini, Morricone, vi fu anche Jewsky

R7: Evangelisti, Guaccero, Branchi, Macchi(anche produzione di musica da film), Jewsky, Marinuzzi(solo film), (tecnico) Guiducci, (tecnico) Ketoff etc...

Vediamo come i due studi hanno delle personalità diverse.

##### Le realtà romane
1. Musica cinematografica
2. Musicisti americani (con Gelmetti che conia il nome)

**Stretta contiguità con la musica applicata a Roma**, che a Milano era al massimo realizzato con i radio drammi.

I milanesi trattavano male Ketoff, e i musicisti romani.

Ma i milanesi di Synket ci si campava.

#### R7
Lo studio R7 sonorizzava cinegiornali, e ci campavano con questa cosa.

**Lo studio R7 nasce come ampiamento di una realtà preesistente fatta da Branchi e Guiducci** quest'ultimo che costruiva altoparlanti.

Questi due personaggi fondarono lo _Studio di Fonologia di Roma_.

#### Le 3 caratteristiche di Roma:
1. Policentricità(Assenza del Castello)
2. Stretta connessione dell'avanguardia accademica americana
3. Contiguità della musica con il cinema

_________

Tutto il discorso fin qui affrontato è svolto tenendo Milano come punto di riferimento. Poichè quando si pensa alla musica elettronica si pensa a Milano. Roma ha prodotto meno e con caratteristiche diverse.

Marinuzzi è uno dei primissimi musicisti a poter maneggiare il saldatore, si realizzó circuiti da soli anche se per quelli piú complessi chiedeva a Ketoff e Savina.

La stilizzazione del DIY di Roma e cristallizzazione, quasi un'etica.

MEV facevano musica con High Tech e Low Tech. Anche se uno degli strumenti più usati da Alvin Curran era una latta d'olio con i microfoni a contatto.

**Roma diviene una realtà con una sua fisionomia ben precisa.**


4. Improvvisazione(Molti pezzi dal catalogo di Davies sono improvvisazioni.)
5. Musica per sonorizzazione di mostre (Water Color Music, di Alvin Curran per sonorizzare mostre)
6. musica per strumenti elettronici ed acustici (ibridazione dei mezzi elettronici ed acustici)
7. musica per teatro
8. nastro magnetico come una tra tante possibilità(che diviene un aspetto teatrale come Macchi e Guaccero-> Combinat Joy).

### I risvolti della musica elettronica romana

Queste sono tutte varianti e diffrazioni del bisogno di evasione e variante dallo schema, ovvero la spettacolarizzazione della musica da messaggio a evento -> da cosa che viene comunicata a cosa che accade.

**A Roma il bisogno di esplosione della comunicazione di un messaggio**, ovvero da musica come messaggio a musica come qualcosa che avviene intorno a te. Quindi gli inizi della musica nello spazio. Con un'allarme che ci fa mettere in guardia da quello che sta succedendo intorno.

Capiamo che vi è una diffrazione, ovvero la musica esplode in una quantità di manifestazioni.

Tutto ció si scontra con la musica elettronica milanese, che era puramente su nastro magnetico.

La musica milanese guarda i romani come i peracottari, un po' come Nono che prende distanza da Darmstadt ed i tecnicismi. Ed anche nei confronti di Cage vede il disimpegno.

La sostanziale incomprensione si riflette su una figura come Mario Bertoncini.

Bertoncini aveva una passione per Bartok, aveva una mente lucida.

Bertoncini era un grande compositore d'avanguardia ma pianista.(Jewsky non suonava piú il pianoforte perchè era borghese(Scratch symphony di Jewsky))

Il pianoforte era il simbolo della musica borghese che andava rifiutata.

A Roma il nastro magnetico è una delle evoluzioni ed attraverso l'avanguardia sviluppa la stessa.

#### Domenico Guaccero

Guaccero nelle sue forme ludiche ha alcuni dei risvolti della musica romana. Questa idea rimanda forse a Plus Minus Stockhausen.

#### Caratteristiche estetiche

Abbiamo parlato di caratteristiche circostanziali, ma l'aspetto della _grande diffrazione_, si inizia ad andare su una identità estetica che è in parte connessa con quello che avevamo prima.

Scratch Symphony di Jewsky (in memoriam a Cardew)

Roma puó vantare una fisionomia estetica che rispecchia le identità del tempo.

A Roma le parlate sono differenziato dai luoghi.

(capire se lo sfilacciamento lo si può trovare in altre realtà)

#### Aspetto antropologico: le persone fanno la differenza

Fatto che il musicista romano sia un po' abbandonato a se stesso come la madre Rai di Milano ed in tutti gli aspetti della musicale romana, Roma è la patria dei sodalizi, un continuo aggregarsi di gruppi.

Spesso nella realtà romana sono le persone che fanno la differenza.

(Musica Ex Machina, R7, Nuova Consonanza)

Piú libertà nella musica.

Studio 29A -> prosecuzione di R7.

MEV -> che nasce come una specie di laboratorio permanente.

##### Breve storia concerti di MEV

- concerti di musica contemporanea
- Spacecart
- Zuppcart -> improvvisazione collettiva aperta a tutti -> estetica dell'improvvisazione (6 settimane)
- SoundPool -> centinaia di persone in cui la bravura del musicista si va a perdere.

##### L'aggregarsi come pretesto musicale
La necessità ed i sodalizi nascono per fondare gruppi, associazioni, situazioni collettivi il cui scopo è produrre, vendere, sperimentare forme nuove.

Guaccero dice che la musica nuova deve avere un pubblico nuovo, con un nuovo blocco sociale nell'interesse di questa nuova musica con un atteggiamento utopistico(con Musica Ex Machina), che cercava di poter creare un blocco sociale che superasse le vecchie logiche tra la borghesia colta e un popolo incolto.

#### Romolo Grano

Figura emblematica della musica elettronica romana emblematica in cui negli anni '60 spiegava la musica elettronica.

Autore di un bel disco di musica elettronica molto vario(non paragonabile a quella di Milano e Parigi), ma è una musica elettronica molto vivace.

Gran parte delle musiche confluiscono nel cinema.

Romolo Grano aveva studiato con Maderna.

(William Smith aveva studiato con John Eaton)

Romolo Grano aveva studiato insieme a Luigi Nono con Maderna.

Colonna sonora di uno sceneggiato televisivo -> il cenno del comando(esoterica romana)

Canzone cento campane è entrata a far parte del repertorio folcloristico romano cantata da un cantante beat, che divvene patrimonio del repertorio folklorico romano.

Chi faceva musica elettronica a Roma faceva anche Cento Campane.

## Bibliografia

- Trevor Wishart - On Sonic Art
- Luigi Pizzaleo - 3 libri -> pdf

_________

- Quindi sono importanti si i personaggi, ma i luoghi? contavano molto le persone

- perchè lo studio di fonologia a Milano e non a Roma?

- Cosa significa che un Berio diviene direttore dell'Accademia di Santa Cecilia?
