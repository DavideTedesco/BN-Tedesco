# Appunti della lezione di Sabato 27 Febbraio 2021

Oggi inizia il ciclo delle ultime 3 lezioni su Trevor Wishart.

## Chi è Trevor Wishart?

Musicista importante e forse sottovalutato.

Wishart e Smalley sono da considerare gli ultimi classici della musica elettroacustica.

Di Wishart è importante conoscere il ciclo Vox.

**Ascoltare tutto ciò che vi è nella cartella di Wishart.**

Wishart è autore di un libro che è una raccolta di scritti, riflessioni e conferenze che partono dal 1970.

(De Artibus Sonoris traduzione latina di On Sonic Art)

## On sonic Art

Questo trattato che giunge a una certa sistematicità, costituisce uno dei pochi autentici trattati che cercando di costituire un'estetica della musica contemporanea elettroacustica, mediata attraverso l'esperienza elettroacustica.

La musica elettroacustica è un banco di prova dei valori che vanno oltre la musica elettroacustica, ed a fronte di uno strumentario tradizionale per la musica tradizionale(sottoinsieme della musica contemporanea).

Wishart fa un discorso di tipo diacronico, dicendo che la musica reticolare, tradizionale è un sottoinsieme storico delle possibilità del suono articolato ma non esaurisce tutte le possibilità che si aprono alle nostre attuali possibilità.

L'elettronica è un'apertura delle possibilità totali di realizzazione del suono.

Si parte dal dominio della musica discreta e la musica del cosidetto continuo, una musica ambiziosa, tentativo di sistematizzazione estetica, molto profondo che attinge al suono.

Il continuo di Wishart è un'intuizione del tutto, concetto Spinoziano.

La musica occidentale è un sottoinsieme come per esempio i numeri razionali, come se fino a un certo livello mantenessero una distanza benchè a livello infinito.

Il tipo di rappresentazioni insiemistiche numeriche offrono un buon repertorio di metafore.

### Composers’ Desktop Project

Software con cui mette in pratica l'idea del continuo, in maniera molto anglosassone.

[Composers’ Desktop Project](https://www.composersdesktop.com/)
### Il termine musica del graticcio o musica del reticolo

(Wishart riesce a sommare in una sola figura coerente, esperienze che siamo tradizionalmente abituati ad associare a mondi differenti.)

>Tutto ciò che è squadrettato...

Da _lattice music_, termine volutamente non matematizzato per indicare qualcosa che noi indichiamo come discreto.

### Cos'è l'arte sonora?

In maniera molto inglese, dice che se non vi piace chiamare un arte che si fa con i suoni, si chiama musica.

Sgrombando il campo dalle polemiche, si parla quindi di arte sonora, e secondo lui la disputa su cosa sia musica, è una disputa inutile.

Ci svincoliamo quindi dalle domande inutili...

Non esiste domanda piú oziosa di cosa sia o non sia musica, come facciamo a dire che un marchio acustico come firma di un brand siamo musica?

La musica per gli stacchetti di un programma televisivo è musica?

Un pensiero compositivo ha sempre una causa finale?

>Con insensato orgoglio diciamo che la musica basta a se stessa...

Lo stesso discorso di chi si atteggia da anticlericale ma aderisce a qualsiasi proposta di campo new age...

Noi ci offendiamo se qualcuno dice che la nostra non è musica. L'eurocentrismo è sbagliato come il suo opposto, e non c'entra nulla applicare categorie etiche a cose che non hanno etica.

>Com'è musicale l'uomo?

Quando non riconosci che ciò che faccio non è musica, è come se fosse un marchio con un disciplinare...

Ciò ci interessa poco, e Wishart ha un approccio molto esperienziale alle cose.

Questa questione non è nuova, e questa questione viene posta in maniera diversa da Schaeffer, che dice che vi sono suoni che rappresentano un lavoro che esaurisce in se stesso uno scopo ed un ruolo.

#### Suono organizzato

La questione musica/non musica va a fondo nel discorso compositivo e arriva fino al Suono Organizzato di Varese.

Ci da una descrizione ed una prescrizione:
- non vi è suono se non vi è un'idea di organizzazione sonora

#### Cage e 4'33"
>4'33" di Cage è uno dei pezzi piú reazionari e conservatori, in cui dice che la musica, non è ciò che viene detto, ma la sovrastruttura che vi è dietro.

Differenza tra autenticità e inautenticità della musica...

Il discorso di Cage ha un po' un ponteggio per capire cosa sia la musica.

Da un punto di vista etico chi ci autorizza dal tagliare l'oggetto sonoro da un contesto??

4'33" è passato alla storia come se con ironia ci passasse ad ascoltare i suoni...

Ma il sarcasmo è una cosa alienata da Cage, le sue prese di posizione non sono mai aggressive, non vuole sbugiardare nessuno, e questo fa imbestialire Nono, perchè Nono aveva capito che l'approccio di Cage era ludico al musicale che per Nono era inammissibile.

In 4'33" Cage ci dice che musica è ciò che appendiamo nella cornice...

(Banksy e i suoi inscatolamenti, è come mettere su CD musiche concepite per spazi di ascolto non stereofonici. Nella sceneggiatura l'autore è contrario.

Come viene a mancare il pezzo, l'apparto produttivo riproduce tutto ciò che viene fatto per la musica.

Mentre per Bansky la pubblicazione è ...)

#### Ancora sull'arte sonora
La concezione del musicale che tradizionalmente traspare dall'impostazione di Cage, è che la musica è nelle orecchie di chi ascolta.

In Wishart dato che non si vuole discutere di cosa sia musica, si passa avanti e si tratta l'arte sonora.

### Come si pone Wishart?

- arte sonora è il prodotto del suono organizzato
- da un altro lato, Wishart si preoccupa di farci introiettare l'idea che il campo di azione del musicista sia tutto il sonoro, in quanto il sonoro è continuo

Questa nozione di _continuo_ è contrapposta alla _lattice music_.

Il continuum sonoro è l'insieme della totalità delle cose e l'impossibilità di discretizzare le cose.

E può anche riaffondare in un'indeterminatezza.

Il continuum è anche il suono del continuum della realtà, vi possono essere delle fratture, delle pause. Il continuum è un concetto metafisico, ed è oggetto della composizione musicale, il sonoro nella sua totalità.
______
### _Lattice Music_

Essa esprime che siamo abituati ad avere una percezione di nodi nella pratica musicale che non hanno particolare importanza musicale.

Questa presenza di nodi, in cui tendono ad accadere le nostre percezioni, costituiscono le maglie del reticolo.

#### Altezze

Esso è il primo dei parametri, che indica che la nostra percezione è radicata profondamente nei nodi della scala temperata. Ciò che è interessante nella nozione di nodo, ci dice che:
>È vero che gli intervalli sono importanti e sono dei nodi, ma la percezione degli intervalli non è legata alla naturalezza

(3:2 è una dodicesima 2:3 è una quinta)

Quello che ci dice Wishart è che se ci si complica l'intervallo, il nodo rimane un'attrattiva; ed il nodo è un centro, un punto di accumulazione in cui andiamo ad assimilare concetti molto complessi...

Anche un rapporto molto complesso fa precipitare un rapporto complicato su un rapporto più semplice, e si tratta di **nodi coltivati** nella nostra percezione, che non dipende da aspetti naturali.

Wishart ci dice che è vero che esiste nello spazio percettivo un approssimarsi negli spazi percettivi a discorsi semplici.

Siamo infatti a nostro agio in parametri che rispetto a quelli di pitagora...

Il sistema delle altezze tradizionali è una selezione di un sistema finito di dati che ci ha permesso di semplificare, permettendoci di realizzare forme molteplici.

Wishart non ci sta dicendo che il sistema temperato o pitagorico sono banali e la contemporanea è meglio... Ma Wishart ci sta dicendo che lo stadio storico necessario ci ha fatto arrivare alla musica...

(Ancora negli anni '50, un Luc Ferrari, non poteva suonare Bartok in conservatorio, vi era dunque necessità di giustificare)

Vi è un qualche cosa oggi del risentimento pilotato nei confronti di tutto ciò che è chiamato accademia...

Per Wishart, diviene una questione politica la musica fatta con i dodici suoni, chè la musica del reticolo.

Wishart è il nemico dell'utilizzazione di strutture reticolari quando si ha disposizione altro, ma non viene gettato tutto come se fosse inutile ma viene tenuto come fecondo storico.

#### Metro

La continuità numerica all'interno di un singolo parametro, non esaurisce il continuum sonoro, ed è la complessità della realta, ed il continuum è rappresentato bene...

Esso ci fa capire ancora di piú la differenza tra una continuità concettuale ed una discretizzazione.

La continuità sul piano del metro, ci fa capire che a partire da un determinato suono, l'evento successivo puó accadere ad una certa distanza rispetto ad un'altra.

Questa possibilità di eventi è basata anche sulla grandezza della finestra che si osserva.

La sospensione della durata(estesa indefinitamente come le "fasce sonore")...

Il fatto che possiamo estendere da un infinito a uno zero, e che le durate si facciano ritmo, non possiamo realizzare glissati di ritmi, vi è la caratteristica del metro, che ci porta a suddividere delle unità con 2 o 4 movimenti.

Una battuta è un qualcosa che ha i suoi movimenti, una sua cultura ed una storia, vi è un microcosmo che articola la battuta che è articolato ancora in se stesso.

Ciò che ci interessa è il fatto che questo sistema musicale sia durato per 700 anni, a partire dall'organum della scuola di Parigi, che è il motivo che permette di memorizzare e sincronizzare, meccanismo di esclusione di tutti i parametri altri dall'altezza e dal metro.

(Musica di Francesco Landini e Francesco Chiconea)

Le musiche di altre culture propongono altri tipi di gabbie, per capire in che modo il crearsi delle gabbie sia sostanziale all'esperienza musicale per la comprensione musicologica dell'altro.

(Lavori etnomusicologici di Bartok)

Nelle lingue europee vi è un legame con la metrica linguistica di battere e levare che lega a tempi binari e ternari.

Tutto ciò funziona per realizzare i semilavorati, che nel Novecento mostra di collassare, la febbre della notazione timbrica degli anni '50, basata sul metro(costrizione), significa costrizione dal punto di vista politco nel metro.

#### Timbro

Arriviamo alla dimensione piú controversa, ovvero quella del timbro.

In cosa si manifesta l'età del reticolo?

Il timbro apparentemente sembrerebbe uno degli spazi meno soggetti a ciò.

Articolazione dello spazio timbrico: esistono dei nodi dello spazio timbrico?

è molto difficile gerarchizzare cosa viene prima o dopo, anche se la musica occidentale riesce a farlo...

Questa discretizzazione è derivata dallo strumentario e dall'utilizzo dello strumentario e la partitura è definizione della discretizzazione timbrica.

Un buono strumento conserva la sua capacità timbrica su tutto il registro, lo strumento deve quindi rispettare un principio di identità.

Per apprezzare una melodia devo avere un'uniformità, ed il timbro è una strada stretta da cui lo strumento non può uscire.

##### La voce ed il flusso

Essa è un concetto derivato dalla realtà, in cui una voce rimane tale e non puó divenire flusso.

Il principio della voce deve mantenere la sua identità ed il reticolo bidimensionale tempo frequenza si arricchisce di una terza dimensione ovvero il timbro.

(Il corno e la sua differenza nel modo di suonare ad esempio sempre in evoluzione.)

L'evoluzione degli strumenti è sicuramente fondamentale, e le incrostazioni culturali che hanno portato ad un timbro.

>A noi interessa dunque che tutte le vene e venature storiche che formano gli elementi del reticolo, producono un sistema privo di connotazioni storiche in cui le note sono 12 per ogni ottava, equamente distribuite; con un metro ed un timbro.

#### Cosa produce lo sfaldamento del timbro?

Varése è il primo e piú convinto utilizzatore delle percussioni, che portano il bisogno di infrangere la barriera timbrica.
Il brano Ionisation toglie alla musica ciò che viene considerata la muscolatura della musica.

Aspetto importante di Ionisation sono le sirene, che identificano il riappropriasi del canto attraverso le sirene, che saltano all'orecchio rispetto alle sole percussioni.

Sentire un brano per sole percussioni si sente molto preso in giro...

Varése è l'unico compositore della vecchia scuola che si è permesso di realizzare brani di musica elettronica...

(Duncan e Cowell sono i geni americani che sono i veri fratelli di Cage)
______
La cultura della koinè diffusa delle ultime simulazioni di amplificatori o VST da vita alla nostra musica oppure no?
______

## Differenza tra Varése e Stravinsky

Essa è nel linguaggio metrico-differenziale, Stravinsky è molto progressivo(ma rappresentativo), che evoca degli immaginari.

Varése invece fa un lavoro diverso...

## Epilogo

Con questi esempi siamo al momento di crisi in cui o si cambia musica o si finisce negli automatismi.

________
Da ascoltare tutte le composizioni Vox

Sulla voce si giocano tanti dei concetti descritti da Wishart.

(spazio timbrico come spazio non topologico e frattura di faglia nello spazio timbrico, le durate e le altezze hanno una rappresentazione nello spazio numerico, mentre lo spazio timbrico; constatazione di una diversità lessicale, si potrebbero discretizzare le realtà che non possono essere però ricondotte le une alle altre.)

Ragioneremo quindi approfonditamente sul ciclo Vox e sulla musica di Wishart.
_______
Tanti concetti dobbiamo scovarli in Wishart, poichè egli non è un saggista, non è divulgatore, ma è un musicista.

A volte Wishart è molto diretto e semplice, a volte è molto scientifico, vi è una disparità di registro...

Esiste un misterioso appunto/dispensa di commento
