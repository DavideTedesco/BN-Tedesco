# Appunti della lezione di Sabato 16 Gennaio 2021

## Il Synket e i sintetizzatori

>Vi è in atto una Synket Renessaince con attori fondamentali della scena romana, con il CRM che da il contributo.

L'EMS si poneva come il sintetizzatore per tutti, ma l'EMS non realizzava solamente il VCS3, ma il Synthi100.

Anche il VCS3 non era un giocattolo da poco prezzo, ma per studi miliardari.

Il fatto che Guaccero volesse realizzare una civiltà della musica elettronica fuori dalle sale da concerto, è un po' come il VCS3 che è una metafora di tale pensiero.

Il progetto dei Synthesizers è costretto ad arenarsi, per via dei pochi concerti e della poca popolarità, ma anche per un discorso legato alle interfacce.

Le possibilità del sintetizzatore si estrinsecano se la pensiamo come una macchina divisa in 2:
1. sezione di pre allestimento del timbro
2. sezione di interfaccia come la tastiera

È chiaro che dopo 20 anni di musica elettronica, dopo le esperienze di Nuova Consonanza.

**Improvvisare sulla tastiera è portarsi dietro tutto il retaggio musicale della tastiera.**

Branchi -> tutta la musica elettronica venne pensata per pianoforte
Risset -> comporre dentro il suono
Wishart -> continuum sonoro (non giustificate opere musicali composte sulle note)
Jewsky -> il pianoforte è uno strumento borghese

Il settore in cui l'idea delle note è piú persistente è la composizione assistita perchè ha bisogno di unità -> che tende a generare enunciati -> grammatiche generative (a partire da un set di regole)

Il concetto di nota nella composizione assistita con OpenMusic o Opus Modus, che si basano sul fatto che la musica si faccia con le note.

I compositori si trovano al di là del guado, e che in brani come Atmospheres, non è la singola nota a fare il suono, ma è l'insieme.

>Non è possibile creare il collegamento tra ciò che dice e fa Xenakis. Vi è un punto cieco tra la teoria matematica e la pratica musicale di Xenakis.

Perchè dobbiamo limitarci alla musica discretizzata quando vi è tutto il continuum sonoro a nostra discrezione?

Ed il sintetizzatore ha una tastiera ed una serie di controlli per impostare il timbro.

Sin dall'inizio chi aveva usato il Synket non usava la tastiera, poichè esso non era affidabile poichè non realizzato dal punto di vista industriale.

Se voglio sfruttare il VCS3 nei miei interessi dovrei simulare tutti i controlli simultaneamente!

NIME -> nascono le conferenze delle nuove interfacce degli strumenti musicali.

Noi produciamo una fisionomia dell'improvvisazione sul modulo degli strumenti.

>Branchi: i sintetizzatori hanno rovinato la musica elettronica.

>Pizzaleo: i sintetizzatori limitavano la composizione.

>Doati: il sintetizzatore rappresenta un grande risultato della ricerca e della tecnica.

Il problema delle interfacce sulla musica elettronica, parte nel momento in cui tutto viene virtualizzato e si cerca di avere dei controlli di alto livello su certe parametriche.

Il VCS3 non ha controlli di alto livello che agiscono su vari parametri.

Ma se si riuscisse a fare qualcosa del genere si sarebbe a un livello di utilizzo della macchina a una potenzialità maggiore di quella possibile.

### La fine del CMS
Le esecuzioni che era in pezzi con parti improvvisate, sembra che si facesse sempre la stessa cosa per tutto il tempo.

Se il progetto del CMS non ha funzionato, è perchè vi era un fattore interno di logoramento per l'impossibilità di ottenere una varietà che per fare un concerto, e poi altri concerti che avessero un timbro vario.

In cui non si potessero distinguere pezzi scritti e improvvisazione.

(Per MEV il sintetizzatore era un oggetto che produceva suono tra 100 altri e non vi era una gerarchia fra i suoni.)

>La varietà timbrica in se non è un valore, ma un aprirsi del suono a tutte le cose rende la varietà timbrica un valore.

Uno strumento musicale è un agglomerato di idee, esperienze, pratiche, miti, che in MEV si considera tutto ciò.

L'atteggiamento di Guaccero è quello del GINC e non si riesce ad andare oltre dei limiti degli strumenti.

In un sintetizzatore vi è la divisione tra:
1. sezione performance
2. sezione timbro

Il libro di Sylvia Goehr, _Il museo immaginario delle opere musicali_, discute proprio di questi punti.

### Perchè non si può usare uno strumento elettronico non può essere nel GINC

Lo strumento elettronico è troppo o troppo poco?

Perchè esso non viene utilizzato dal GINC?

Quando Cage scrive brani per pianoforte preparato -> vuole la convergenza della pratica strumentale con un'intrusione che altera quella pratica. L'idea è che suonando la stessa cosa, si ottenga una cosa diversa.

- atteggiamento molto elementare di stupore
- operazione di alterazione del timbro del pianoforte, con una sovrapposizione di un imprevisto a quello del suono tradizionale

**MEV si spinge verso la sovrapposizione dell'imprevisto e del tradizionale**

Dagli scritti di Jewsky percepiamo un interesse verso cose della tradizione musicale non verso il timbro, ma un'espansione del musicale. Mentre l'esperienza del GINC è intensiva.

(Sezione analitica del libro che è una sezione filosofica.)
_______
Teoria di Lydia Goher -> concetto di opera musicale è un concetto dal tardo '800, e '700 in poi, e dunque quando Bach scriveva non sapeva di scrivere opere musicali, ma dall'età classica in poi.

L'implicito, è che il fatto di essere un'opera musicale è nobilitante, ma se non scrivi opere musicali realizzi un lavoro di serie b.

Negare lo statuto di opera musicale è quasi un sacrilegio, ma quanto c'è di ripetitivo in termini di formule, progressioni, figure nelle opere di Bach.

Guardando Invenzioni, partite, etc... Vi è un formulario musicale molto ripetitivo.

Mozart è anche molto ripetitivo...

Beethoven scrive ogni opera che debba essere identificabile con percorsi di sviluppo unico con forme di risalto che sono la forma Sonata.

L'opera per Beethoven deve avere un'individualità assoluta.

Brahms 4 sinfonie, Haydn 105 sinfonie.

Più è abbondante la quantità di opere, piú vi è un travaso di forme tra un'opera e l'altra.

>Ma non è che Bach non dobbiamo prenderlo cosí sul serio?

La preoccupazione funzionale di Bach è importante.

Beethoven scrive delle opere che devono essere pubblicate da un editore -> opere per dilettanti di lusso.

Perchè le opere iniziano ad avere un titolo?

Per Bach l'aspetto d'uso è fondamentale ed il congelamento in opera del singolo aggregato musicale in Bach rispetto a Beethoven.

Cosa è successo da Bach a Beethoven?
1. Beethoven scrive opere che devono essere pubblicate
2. Beethoven non ha un padrone
3. Beethoven scrive la musica che pare a lui
4. Beethoven non può scrivere brani uguali ogni anno

Un'opera musicale beethoveniana, ovvero quello che vi è in partitura e le diverse esecuzioni, sono ancora valide?

Si puó uscire dalle regole del gioco?

La partitura è normativa!!!

(Nei musicisti vi è una voglia di fare e non solo di dirle)
_______

In una pratica elettroacustica come quella dell'improvvisazione al sintetizzatore, di Nuova Consonanza, del Live Coding, e dei grandi setup di sintetizzatori analogici, come facciamo ad escludere da queste pratiche una componente forte come la suggestione dell'oggetto strano, meraviglioso, la scatola sonora?

Il Synket sembra un centralino telefonico...

Il regista Aldo Trionfo che realizza le sinfonie Synket, nel cabaret Synket, vi è una figura di Justin ...

Ad Aldo Trionfo, interessava dunque l'oggetto enigmatico e monolitico che produce suoni assurdi.

La componente che realizza un suono esagerato, strano -> come gli stenografi della camera dei deputati.

Come il laptop ensemble, ed il suono proviene dai soggetti seduti e siamo stupeffati...

Quando si suona Per Elisa, un semitono sotto è una sorta di numero da circo.

"La toccata di Paradisi" suonata in mezzo a un concerto -> numero da circo

Vi è una componente spettacolare che non può essere aggirata.

Uno spostarsi del senso complessivo dell'operazione musicale sull'oggetto anzichè sull'operato musicale, fa sbriciolare l'idea di opera.

Quando i Synthesizers suonavano un brano con il titolo o un'improvvisazione, si sentivano cose uguali o simili.

Il concetto di **opera come oggetto normativo** non funziona piú ed alla base della crisi del gruppo di Guaccero, vi è il desiderio di costringere ad un concetto di opera un qualcosa che non è piú opera.

Pensando alle pratiche di oggi, della musica nei club, e la polemica dei musicisti professionisti contro i dj.

Sarà anche vero che fare il dj non è suonare.

Un conto è scrivere l'opera musicale, un conto è la musica stessa.

Output naturale sia il disco -> è che si perde la percezione di come un qualcosa sia riprodotto e si ha la sola percezione acustica rappresentata su pellicola.

**C'è una comprensione dettata da esigenze di profitto che comprime il massimo risultato con il minimo delle risorse.**

Guaccero voleva uscire dai limiti della sala da concerto e creare situazioni per eliminare gli elementi semantici e l'esclusività classista del fare musica di ricerca, in contesti che potessero essere di interesse piú ampio.

I giovani degli anni '70 sono spesso a caccia di nuove esperienze compositive.

L'ideale sarebbe avere un atteggiamento di apertura!

(Anche i fallimenti di Adorno ci insegnano qualcosa -> la dialettica di Adorno ci ha insegnato proiettarci su ambiti diversi)

È necessario avere il rigore Bernardiniano per vedere le cose da un altro punto di vista.

Gli elementi semantici sono anche gli elementi:
- politici
- identitari
- umani
- etc...

Abbiamo il dovere di essere nani sulle spalle dei giganti. Il titanismo di un Prometeo di Nono, non è solo il rigore della ricerca compositiva, ma sono anche fattori di tensione sociale, per valutare la portata di certi fenomeni.

Violenza del mercato che non vede un ritorno economico del guardarsi intorno o fare altro.

(La prepotenza assoluta del mercato e del profitto -> capacità di pervasione onnicomprensiva in tutti i settori della vita -> non bisogna avere atteggiamenti relativistici)

Riguardo il concetto di opera, **i concetti sussidiari all'opera**, come:
- partitura
- esecuzione
- contesto esecutivo
- scrittura

Guardare un manoscritto di Ferneyough, Guaccero, Bussoti, vi è un'espansione della scrittura per i compositori; ma che fine fa nell'epoca del digitale.

Cose che richiedono talmente tanti trucchi per essere riportate che finisco per rinunciarci.

Concorso di composizione -> scrivo un brano che non puó essere immediatamente valutato e ascoltato mentalmente dai giurati e non ho la possibilità di farlo valutare con una registrazione, non avró nessuna possibilità per il concorso.

Il giurato deve valutare in base a ciò che legge.

(Mito di Procust)

Non essendo in grado di valutare soluzioni inedite o strumenti strani, le escludo dal giudizio.

Lasciamo dentro la musica, i criteri che possono essere valutati e giudicati con la partitura tradizionale.

La pratica del concorso di composizione è uno dei motori piú forti della musica contemporanea.

Perchè non si scrivo più pezzi per orchestre, cori o grandi orchestre? Poichè non vi sono le condizioni economiche!!!

Progressiva diminuzione di opere nuove per ensemble e orchestre, ma nel 2020/2021 vi sono quasi solamente opere per strumenti solisti e 2/3 strumenti.

Allargare il concetto di fatto musicale a scopi, concetti musicali...

(Stockhausen, era fuori dalla storia e non aveva una presenza storica, ed in questo Cardew scrisse Stockhausen serve l'imperialismo. Perchè festival italiani che non avrebbero fatto suonare Berio, Nono etc... spalancano le porte a Stockhausen, poichè si diceva che egli non facesse politica -> tratti poi spunti e suggestioni new age)

_______
Vi erano dei limiti tecnici della vita di Guaccero e cosí come i limiti del concetto di opera esplodono, dovrebbero esplodere anche i luoghi, il pubblico etc...

Il fatto che questa iniziativa dovesse nascere nel CMS, ci faceva capire che l'idea di Guaccero tralasciava di gran lunga il feticcio tecnologico, ponendosi su diversi piani di linguaggio, su un plurilinguismo che crea teatro.

Ed abbiamo esplorato il concetto di opera e cercato di capire se le opere classiche:
- economia musicale
- carattere di appartenenza del compositore
- etc...

Che hanno risvolti sulla scrittura.

Ci siamo chiesti se questi criteri non siano tornati a cambiare in maniera tale da rendere inattuabile ed inoperabile...

_______
**Esiste un movimento per il recupero delle opere del recente passato tecnologico**.

Della ricodifica di opere del passato con strumenti che ne permettano la trasmissione e la riproposizione.

>Siamo sicuri che siano necessario recuperare e creare queste mummie di pezzi musicali nati e cresciuti e per cui l'opera musicale non ha quel valore che aveva in epoca '800 e novecentesca.

Quanti pezzi sono stati scritti con la Kyma Capybara? Ci sono buoni motivi per conservarli tutti?

Relazione di tipo conservativo, archivistico, culturale, cosa è importante?

Tutto va conservato, per l'archiviazione!

Nelle pratiche non è detto che l'archiviazione abbia senso!

(partitura giustificante della partitura di Nono della Foresta)
