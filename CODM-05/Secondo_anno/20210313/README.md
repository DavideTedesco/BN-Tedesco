# Appunti della lezione di sabato 13 marzo 2021

Lacatoche 1973 articolo sulle evidenze scientifiche...

## Trevor Wishart

Convergenze di Wishart con il resto del corso, ma cosa ha in comune Wishart con la musica romana?

### Walter Branchi
C'è chi parla male di Walter Branchi che nasce come contrabbasista di Jazz, con 300 serate nei club romani, che si appassionò alla tecnologia del suono e fondò con Guiducci lo Studio di Fonologia di Roma.

Guiducci costruiva altoparlanti e Walter Branchi imparava, Branchi è del 1940...

Branchi aveva 21 o 22 anni quando si associò a Guiducci e divenne un utente lavoratore del Synket.

Branchi divenne allo studio R7, membro chiamato da Evangelisti nel gruppo di Nuova Consonanza.

Brano Thin di Branchi(analisi del brano di Pizzaleo).

Poi andò a Princeton a studiare.

Poi scrisse la volontà di un modo di scrivere con un **sistema sonoro** che libera solo alcune possibilità, che è un'opera aperta perchè non esaurisce mai le sue possibilità.

>L'opera aperta è una statua molto complessa di cui non è possibile visionare tutti i punti di vista. -Umberto Eco

Quello che descrive Eco è difatto diverso da quello che è descritto nell'opera di Posseur.

Il significato di opera consiste nel fatto che un'opera non riesce mai ad essere osservata nella sua completezza.

Opera scultorea molto complessa che presenta una fisionomia diversa da ogni punto di vista che se ne può avere, e l'opera non finisce mai di essere fruita.

Se l'opera è raffigurativa, combacia con alcuni canoni, mentre se l'opera è astratta, se non vi è uno schema di verifica, l'opera aperta non cessa mai di dire qualcosa.

Tutta l'opera di Bertoncini va nella direzione di opera aperta, con una fruibilità indefinita.

Il brano Thin assomiglia in apparenza a Mikrophonie I, ma non c'entra nulla.

Con 4 microfoni che corrispondono ai quattro angoli della stanza per riprodurre lo spazio sonoro del sistema.

Sostanzialmente bisogna immaginarsi un sistema sonoro.

Branchi:
- sistemi di altezze -> selezionando una serie di rapporti -> con una rilevanza

Branchi non opera mai all'interno di un sistema casuale.

>Esempio del casuale, rispetto ai rapporti...

Programmino in Visual Basic che generava partiture di CSound per generare fino a 512 oscillatori, per selezionare:
- finestrati
- inviluppi
- frequenza centrale

Sistema con molta potenza, che però generava white noise.

E non bastava il tanto, ma serviva anche il come.

Suddivisione temperata dello spazio e delle sequenze come avviene in studio II con un suono piú caratteristico e personale, con un sistema selettivo.

Branchi adotta sempre un sistema selettivo con rapporti sonori ben precisi.

Branchi, andando in una direzione filosofica, che vanno in una direzione del paesaggio sonoro, mentre il prodotto della musica serve a integrare i prodotti del paesaggio sonoro, strato intermedio tra suoni della realtà e silenzio.

Branchi considera Evangelisti e Ligeti i suoi grandi maestri, relazione sentimentale e nostalgica.

Branchi nacque come contrabbasista da una famiglia di musicisti classici e sviluppa l'interesse per la tecnologia, dopo aver studiato contrabbasso, composizione etc...

### Il sistema sonoro

Esso è un insieme di virtualità. Possibilità di estrarre...

Sacrificando la complessità di un discorso musicale(con sole sinusoidi), con tessuto sempre cangiante di texture sinusoidali basato su complessissime strutture scritte **punto che lega Branchi a Wishart**, presa di distanza dalla Live Electronics e dai suoni riprodotti dal vivo. E le motivazioni che allontanano Branchi e Wishart sono racchiuse nell'articolo "[TREVOR WISHART: LA CHIMICA DEL SUONO](http://digicult.it/it/digimag/issue-041/trevor-wishart-chemistry-of-sound/)".

>USO: Potresti spiegare la tua preferenza per la trasformazione del suono in modalità offline, anziché in realtime?

>Trevor Wishart: Offline e realtime sono differenti sia da una prospettiva di programmazione, che musicale. Con il lavoro in modalità offline, puoi prenderti tutto il tempo per produrre un buon risultato, riflettere e decidere sul da farsi, eseguire un processo una seconda volta, e sulla base di questo eseguirne un terzo e così via. Quando le macchine e i programmatori si evolveranno sarà possibile aggirare la maggior parte di questi problemi. Ma la principale differenza è estetica. Se stai processando un evento live, devi accettare ciò che entra nel microfono o in qualsiasi altro dispositivo di ingresso. Per quanto sia precisa la partitura, i suoni saranno sempre diversi in ogni performance. Perciò il processo che si usa deve essere in grado di lavorare con svariati potenziali input. Il problema principale con la musica dal vivo, è che devi essere il tipo di persona che ama andare a concerti, e molte persone non lo sono. Da un lato, questo può essere cambiato attraverso l’istruzione, ma, più significativamente, rendendo il mondo dei concerti aperto a differenti gruppi di persone, ad esempio organizzando eventi in luoghi insoliti.

>Lavorando offline, è possibile manipolare le caratteristiche uniche di un particolare suono che non può essere riprodotto a piacimento in una performance. Perdipiù, l’assenza degli artisti, interpreti o esecutori sul palco potrebbe sembrare una debolezza, ma ha i suoi lati positivi: paragonate per esempio teatro e cinema. Nel puro evento elettroacustico si può creare un mondo “dei sogni” che può essere realistico, astratto, surreale o tutte queste cose in momenti diversi – un teatro dove le orecchie ci possono trasportare lontano da qui, in un luogo immaginario, evocato dal suono, al di là delle convenzioni sociali della sala da concerto. La musica elettroacustica non è diversa dal cinema nella sua ripetibilità, ad eccezione del fatto che la diffusione del suono, nelle mani di persone qualificate, possono rendere ogni spettacolo unico nel suo genere, dall’interazione con il pubblico alla gestione dello spazio acustico.


Vi è una differenza estetica tra ciò che entra nel microfono in Live rispetto a ciò che è scritto in partitura.

>Possiamo dire che la nozione del sistema sonoro di Branchi è una virtualità che precede il suono. Mentre il continuum sonoro è la totalità del suono.

Vi è quindi una nozione infinita di questa virtualità.

Vi è un'istanza di totale controllo da parte del compositore e della materia sonoro.

Vi è l'idea che vi è un continuum tra silenzio e stato sonoro di cui si occupa il compositore.

### Continuum sonoro

Il continuum sonoro di Wishart viene messo in scena con il suo contributo sul morphing.

#### Morphing
Il morphing permette di realizzare la rivendicazione sul suono.

Il morphing è slegato da Branchi...
Scritti tecnici di Branchi meno aforistici, che si sono fatti piú meditativi.

>Quando Branchi scrive che non comprende perchè i compositori continuano a scrivere musica elettronica come se fosse scritta al pianoforte, perchè la musica del graticcio è superata...

In alcuni punti Branchi si avvicina a Wishart, mentre in altri si allontanano.

La tradizione della registrazione dei materiali di Wishart è molto anglosassone.

Il suo approccio alla ricerca è molto anglosassone: non ho lo strumento -> me lo costruisco.

### Rapporto specifico con la voce di Wishart

Cosa può fare un compositore:
- lavorare con la voce
- evitare la voce

Wishart sembrerebbe uno Jacopo da Bologna contemporaneo, la voce ha un portato emotivo mitico.

### Branchi ed il flusso

In Branchi non ci bagniamo mai con la stessa acqua(riferimento eracliteo), l'idea di flusso come essenza della realtà che caratterizza il flusso di intero di queste composizioni di Branchi.

Mettendo in mezzo delle pietre nel flusso si crea quell'arco automatico e formale che mi riporta all'indietro.

Non come in Poeme electronique che vi è una ripetizione.

Le composizioni di Branchi con un sistema sonoro che viene attivato da un compositore, ed il compositore è un attivatore all'interno di questo sistema finito, in queste virtualità c'è spazio solamente per flussi continui e non vi è spazio per oggetti da trattenere nella memoria.

Differenza fra lo stesso suono ascoltato nel suo accadere incredibile, mentre se riproduco l'immagine identica in serie...

Cogliere una realtà tipica, perfetta o cosí com'è...

Non si vuole una realtà come una cartolina, perchè diviene ripetibile e mi costringe a raccontarmi sempre la stessa favola.

Un elemento cessa di essere circostanza per ridiventare messaggio, comunicazione codifica.

### Wishart ed il flusso

Riproduco il continuum strutturale che nella realtà è soltanto metafisico, con quella parentela che nella realtà è solo una continuità spaziale, culturale, mentre la continuità diviene acustica.

Da On Sonic Art a Audible Design.

>La musica di Branchi è musica da ascoltare e non musica per ascoltare.

Realtà che ha il vantaggio di essere fatta di oggetti, messaggi e segnali non imposti dal compositore...

Cercare di salvare la composizione episodica fatta di suoni reali con la composizione astratta sinusoidali, puramente elettronica, il cui apporto è dato dal compositore con fasci sinusoidali sempre diversi.

Software scritto da Eugenio Giordani per la realizzazione...

(Far scontrare e decollare sulla pista di atterraggio Wishart e Branchi sollevando ordini di problemi simili, concezione simile della musica del reticolo)

(Cantor -> infiniti numerabili)

Il continuum sonoro di Wishart ci ricorda la sostanza spinoziana, ogni apparizione di intero è uno sguardo su un infinito di molecole che possono illuminare il cielo.

Continuum sonoro che ci ricorda con terminologia empirica Wishart, stato metafisico sui compositori.

Differenze tra:
- tempo reale
- visione della realtà

Opera aperta, suono mobile, teatro musicale, composizione nel suono come Risset che supera la barriera della nota, discorsi affrontati da diversi personaggi con concetti che tutti i compositori attivi negli anni '80.

Wishart e Branchi sono coetanei con temi che ritornano per i compositori.

### Vox

_Ascoltiamo Vox I_

Di Vox ci sono delle partiture di esecuzione, e dice che vi è tutto descritto e spesso si tratta di testi specifici del software che lo ha prodotto.

Computer desktop project di cui urge un recupero.

Wishart proviene dalla classe operaia.

Concezione della musica come mitologia...

#### Commenti su Vox I

Dal Caos al Kairos -> con un ordine finale?

Il percorso dal caos all'ordine, o dal caos al logos, dal kronos al kairos... Un'idea abbastanza comune e corriva...

Brano del 1980, di un'epoca eroica, anche se è vero che nella performance vocale Wishart si senta piú libero, mentre nel nastro non è goffo.

La performance vocale segue i primi esperimenti, e dunque deve qualcosa a questo tipo di esperimenti.

Lettura di una progressiva conquista di spazi di logos e razionalità, ci fa capire come Wishart non esista la musica dell'avanguardia o politicamente schierato contro la tradizione in maniera iconoclastica.

Wishart non si spaventa davanti alla possibilità che i suoi cantori possano realizzare un accordo:
>come se essi cercassero un'evoluzione

È evidente il progressivo rendersi autonomo dell'aspetto verbale, comunicativo rispetto a quello semplicemente acustico. Come se gli elementi acustici lascino ben separati gli elementi puramente lessicali.

Parte del primo pezzo magmatica e continuità di voce e elettronica e nel finale vi è una netta differenziazione tra la voce come suono naturale e la voce culturale.

Dalla natura alla cultura, con un'indifferenziata unità drammatica che progressivamente si sviuluppa.

Impressione dell'ascolto multicanale sarebbe stata molto diversa.

L'estrema compattezza del tessuto è dat anche dalla riduzione dimensionale.

Lettura della voce come una culturalizzazione e progressiva acquisizione del linguaggio e della storia, sviluppo di organismi...

Quando le metafore divengono lettera, tutto cambia.

_Narrazione degli organismi_ e non musica degli organismi.

Cartone animato di quello che si vuole fare -> Marco Giordano -> idea di una progressiva conquista di un'evoluzione -> interpretazione di organismo.

##### Perchè finisce il brano?

Difficilmente un brano è portatore di una logica che ci fa intendere che un qualcosa finisce.

##### Percorso

Arrivati alla culturalizzazione ad un certo punto si puó finire, percorso dal kronos o dal kaos per arrivare al kairos.

Krono è stato assimilato a chrono in termini di tempo, anche se sono due parole diverse.

Il percorso è una sintesi abbastanza pregnannte di ciò che Wishart ci propone.

##### Voce genera mostri

Scarto fra l'intenzione morfologica e costruttiva e l'impossibilità di lasciare fuori dalla porta le connotazioni.

>Smalley e Wishart sono gli ultimi classici

_Ascoltiamo Vox II_

#### Commenti su Vox II

Cambia la morfologia della voce...

Bunraku -> il teatro delle marionette giapponese

Perchè il teatro giapponese e poi perde la connotazione etnica?

Questo brano qui è forse il piú bello dei sei brani.

Voler raccontare una storia -> storia che va incontro ad un'accumulazione

Pasaggio da Vox 1 a Vox 2 ricorda il passaggio da Thema a Visage per Berio...

1. È bello, ma perchè mi trova già pronto e ben sicuro nel mio ambiente!
2. carrellata esotica che tradisce un punto di vista molto eurocentrico -> tradire un orientalismo, come se le voci da ambienti culturali diversi, finissero per divenire ornamentali e decorative. Il rischio è accomunare certe voci ad altro. Rischio di un residuo decorativismo dell'esotico.
3. Che differenza c'è tra questo uso di suoni naturali degli insetti(ispirazione, presenza acustica, come significato), rispetto a un brano di Risset ovvero Sud?!?! Riflettere molto su questo aspetto, insetti che appartengono a due pianeti diversi.

_________

Data della prossima lezione da decidere...

- che legame c'è tra chi commissiona ed il materiale utilizzato nei brani?

## Biografia

- On Sonic Art estratti
- 3 o 4 articoli sulle questioni di Wishart
- libri di Pizzaleo
_______
- Dolls di Takeshi Kitama
- Tutto di Mishima
