# Appunti della lezione di Sabato 30 Gennaio 2021

## Importanza di Evangelisti nella musica elettronica romana
Presenza alla lezione di sabato scorso poichè vi è del materiale registrato ovvero una lezione a tutti gli effetti che riguarda una lettura del capitolo dedicato a Franco Evangelisti, nel volume Fase Seconda di Bortolotto.

Il testo di Bortolotto è un testo difficile, in un italiano letterato con metafore stringate, che necessitano alle volte una reidratazione per essere comprese al massimo.

Evangelisti arriva al bello attraverso il rigore, arrivare ad una grazia elevata con il rigore(come il brano _quattro fattoriale_). Un po' come il brano _Incontri di fasce sonore_.

Il piú grande insegnamento di Evangelisti è stato il rigore, egli aveva legami forti con la Germania ed era una di quelle figure (come Romolo Grano, autore molto poliedrico)  che aveva un contatto con la realtà europea.

Romolo Grano aveva suonato le percussioni in Musica su due dimensioni di Maderna, ed aveva studiato con Nono.

Evangelisti aveva studiato a Darmstadt, ed aveva lavorato allo studio di Colonia. Egli era un compositore affermato, talmente tanto, che quando Bortolotto decide di fare una panoramica sulle nuove generazioni di compositori, realizzo _Fase seconda_, dopo la prima ondata weberniana; poichè Evangelisti venne riconosciuto fra le voci piú pure.

Evangelisti poteva quindi campare di rendita, ed invece decise che non vi era piú niente da scrivere, e decise che vi era qualcosa che si era bloccato "la fine della musica come linguaggio" e l'inizio "della ricerca di nuovi linguaggi".

La soluzione di Evangelisti è non scrivere piú, e dice bisogna fare un bagno salutare e purificatore del silenzio("Dal silenzio a un nuovo mondo sonoro" libro di Evangelisti che si distacca dalla _musica come linguaggio_), per arrivare ad una nuova musica.

Evangelisti parla di un nuovo mondo sonoro, con una poetica dell'immersione con il sonoro che non è piú uno strumento di comunicazione, ma un nuovo sistema.

Evangelisti grazie al silenzio purificatore, cerca di arrivare ad un nuovo mondo sonoro, lasciando le formule linguistiche alle spalle.

Si approda ad un improvvisazione del timbro ed ad un nuovo modo di fare musica.

Esso è stato pubblicato postumo, ed

(Ascoltare il brano di Petrassi _Noche Oscura_ ed _I concerti per orchestra_)

Petrassi era il maestro di Guaccero, Bertoncini...

Bertoncini era già molto avanti dal concetto compositivo del suo maestro, anche se Petrassi gli chiese per i commissari di inserire la serie nei suoi brani, poichè i commissari lo richiedevano.

(Succedeva un po' il fatto della scrittura in stile poichè gli studenti erano piú avanti nel pensiero.)

Vi erano 2 differenze:
1. composizione in stile -> diversa da scrivere contrappunti
2. lingua franca ancora viva -> illudersi di scrivere con la lingua del presente


Il trauma non è lo stesso discorso di Bertoncini che ragionava su temi che trascendevano il presente del serialismo. Lo studente che si accosta alla scuola di Bernardini e Lupone, è che lo studente non è portatore di istanze progressive, ma di istanze regressive(la musica è ciò che faccio io).

Quando il contemporaneo diviene un genere, si arresta la spinta progressiva illuministica, e quando si diviene possibilisti sul reintegro del passato nel presente, ed il fare cose diverse diviene una questione di essere altrove. Ciò inizia con gli anni '80 con l'influsso in politica, con la fine dell'idea collettiva.

Un collettivismo alla Guaccero negli anni '80 non era piú concepibile...

Vi saranno quindi dei gruppi che mettono insieme risorse individuali per unire le forze che con il contributo avessero una forza maggiore.

Il CRM nasce per portare avanti progetti di 2 o al massimo 3 musicisti, risorse messe insieme per realizzare qualcosa.

Joint venture per fare massa e forza.

Tutto ciò ci riporta al discorso iniziale, e dunque Evangelisti ha il coraggio di voltare completamente pagina. Si getta a capofitto in vari progetti, con R7, poi con lo studio di Santa Cecilia, poi fa nascere la cattedra di musica elettronica a Roma, e poi nel GINC, etc...

(Anche se R7 non ha dato molto dal punto di vista delle opere, ma è stata piú una commistione di idee, R7 non ha funzionato perchè vi erano troppe individualità di alto livello.)

>Evangelisti è un agitatore, un movimentatore della musica.

Nell'ultima sua opera _Die Schactel_ si trovano queste idee come la musica concreta realizzata con gli strumenti.

>Bortolotto non coglie lo snodo "tragico" della storia, poichè l'improvviso silenzio è un qualcosa di sconvolgente. Bortolotto aspetta al varco Evangelisti sul suo terreno.

Eclissi della critica è parallela all'eclissi del movimentismo e dalle crisi musicali.

"Problema della critica come saggistica", ci si ferma nell'esprimere un'idea e non di criticare.

[Intervista a Nottoli di Giovanni](https://www.nucleoartzine.com/giorgio-nottoli-intervista-ad-emufest/)

## Synket

È uno strumento che è ritornato in vigore, per la mostra su Ketoff e per il testo di Pizzaleo.

(brand felliniano a Rimini, identificare che egli divenisse un marchi della città -> rischio è che ciò non abbia spinto nessuno a guardara il cinema di Fellini -> i riminesi non conoscono Fellini, poichè egli è di cultura felliniana)

Vi sono alcuni luoghi comuni sul Synket portati avanti da Davies nel Groove, o nel libro di Jenkins sui sintetizzatori analogici.

Ketoff era figlio di un immigrato russo, scappato da un gulag zarista.

Egli era figlio di due genitori russi. Il cognome era un cognome russo-tedesco che venne poi cambiato in Ketoff.

Il Synket non esisteva solo in una variante ed erano tutti diversi per:
- interfaccia
- architettura
- tecnologia con cui furono costruiti

I primi Synket erano realizzati a valvole, poi vengono realizzati con un transistor.

Il Synket è uno strumento che di fatto non esiste...

>Il Synket nasce con un'idea diversa da quella di un sintetizzatore modulare.

Il Synket non è tuttavia chiuso come uno strumento(patching), ed il patching è in grado di misurare ciò che è buono oppure no.

Questo schema del rapporto con il Moog

### Le 3 fasi del Synket

#### 1965-1968

Strumento a tecnologia valvolare, con significative modifiche dal primo al secondo esemplari

#### 1968-1972

Modifiche che implicano la tecnologia a stato solido come i transistor.

(Vi è una lettera tra Ketoff e Eaton che attesta questa versione)

#### 1972-1976

Realizzazione del Synket con tesi dettagliata che integra molte piú funzionalità, con una tesi di abilitazione che dovesse sostenere per accedere al centro di cinematografia sperimentale.

>Unico acquirente certificato era John Eaton di questa versione.

### Synket: strumento con dei limiti

Non bisogna cadere nell'apologia, esso aveva anche dei limiti.

#### Limiti: essere artigianle

Esso costava molto per costruirlo e molto per comprarlo, con 2 milioni di lire. L'accademia americana si poteva permettere di pagare queste cifre.
>Commisione: macchina che facesse un po' le stesse cose che faceva il phonosynth, sulla falsa riga della console attrezzata realizzata da Marinuzzi per la rapida sonorizzazione del cinema. L'accademia americana aveva affidato a Ketoff la ristrutturazione degli studi dell'accademia americana(da Otto Leuning). Realizzò Ketoff uno strumento molto piú versatile per molti versi che conosce la vita nel Maggio del 1965.

Esso era un qualcosa di artigianale ed ogni pezzo era costruito a casa sua per sua mano.

#### Manutenzione: difficile da riparare
Dato che Ketoff non rilasciava schemi dei suoi dispositivi, egli doveva manutenere lo strumento ogni qualvolta serviva.

>Il Synket fece la fortuna di John Eaton!

### Per cosa è pensato il Synket?

Esso è progettato per un'esecuzione ed un utilizzo immediato. Esso va usato cosī com'è, in cui ogni componente è studiato per funzionare con gli altri.

Le combinazioni che puó realizzare il Synket sono moltissime.

Ketoff fa vedere il Synket a Moog, che gli dice che può realizzarlo su scala industriale. E dovrebbe trasferirsi a Thrumansburg, e Moog fa un elenco delle spese che dovrebbe avere trasferendosi.

Ketoff però che vive a Roma in Via del Corso, alla fine non si vuole spostare e rimarrà a Roma.

Ketoff aveva a che fare con il mondo del cinema romano ed italiano e non si volle spostare.

Ciò ci dice che il Synket un suo fascino doveva averlo.

Robert Moog fonda "Electroni Music Reviem", in cui vi è una recensione del Synket di Ketoff, ed in cui vi è una recensione di Chadabe dei concertini sul Synket.

"Versione della sagra della primavera realizzata dagli ELP", invece se l'orchestra si sposta sul terreno della musica elettronica, il discorso si fa piú interessante.

Le matrici sono piú seriali, perchè il Concert Piece è realizzato con due orchestre scordata una di mezzo tono dall'altra.

Si va infatti nel continuum del mondo analogico.

### Pannello frontale del Synket

![synket.jpg](synket.jpg)

Esso è uno strumento con 3 manuali ed è trifonico.
Esso ha 3 sintetizzatori identici messi uno sopra l'altro: _Sound combiners_ o moduli di sintesi.

Troviamo quindi i controlli di 3 modulatori:
- 4 manopole
- 2 modulatori
- jack per segnali esterni

Da notare la differenza tra i 2 strumenti, quello sopra e sotto.

![eaton_synket.jpg](eaton_synket.jpg)

Lo strumento descritto da Ketoff sul Music Electronic Review è il primo.

Lo strumento di Eaton è invece molto diverso e fu quello prodotto per l'accademia americana.

- Vi è un pannello, il secondo, con funzione di equalizzazione.

(pagina 40 del libro)

- vi è un pannello con 26 ingressi per il patching e la gestione dei segnali esterni ed una manopola per il controllo generale

- vi è una manopola inoltre per il riverbero in alcuni modelli

- inferiormente vi sono le 3 unità di sintesi identiche del Synket

### Virtuosi del Synket

#### Jane Schoonover

![scho.jpg](scho.png)

Vediamo un modello precedente rispetto a prima

#### John Eaton

Egli fu il primo a commissionare la realizzazione di un Synket.

### Vita di Ketoff

Rapporto stilato dai soldati americani del comando di Benevento, che attesta l'arrivo di 3 soldati inglesi e un certo Paolo Ketoff che decise di unirsi alla linea del fronte e unirsi agli alleati.

Ketoff era un tecnico e si era appassionato all'elettronica.

Egli inizia una breve ed intensa carriera con l'esercito americano nella ricostruzione degli studi radiofonici e degli impianti, con strutture radiofoniche telegrafiche etc...

Egli fa un lavoro di ripristino di impianti alla base del suo mestiere, anche se egli fa l'audio del cinema e della televisione come mestiere di base.

Uscire da un istituto tecnico nel 1939 era molto formativo e si riusciva veramente a fare il mestiere.

Egli aveva iniziato una carriera da attore in "Ebrezza del cielo" in un film propagandistico.

Egli voleva anche fare l'università ma scoppiando la guerra non aveva avuto la possibilità.

Ketoff era la figura piú importante della musica elettronica romana, _Io sono un liutatio elettronico_. Dove vi era musica elettronica a Roma vi era Ketoff. Non vi è un mondo dove Ketoff nella musica elettronica non abbia avuto a che fare.

Quale è l'esigenza di fondo che fa sentire l'esigenza di questo genere di film in cui vi potrebbe essere Ketoff come protagonista?

(Non vi sono film di questo genere, perchè sarebbe cercare in un genere musicale che esso non ha, capaci di generare immaginario che esso non ha.)

Constantine Ketoff, era un rivoluzionario russo che scappa da un Gulag con l'aiuto delle popolazioni locali.

### Schema generale del Synket

Schema rappresentativo.

![schema.png](schema.png)

Ora si tratterà di capire cosa sia il modulatore e come funziona all'interno la macchina.

Vediamo che la tastiera o la manopola gestiscono un generatore di onda quadra.
![schema1.png](schema1.png)

Dopodichè vediamo degli switch (FLIP-FLOP) che servono per avere un segnale consistente di varie frequenze.

Si passa poi attraverso un controllo di ampiezza ed un filtro che è l'anima della macchina(esso produce il timbro dello strumento). La frequenza del filtro è gestito da manopole 3 e 4.

Quando il frequency modulator è attivato si attiva la modulazione della frequenza del filtro.

Il white noise entra nel circuito può arrivare:
1.  ai filtri -> chiudendo l'interruttore 9
2. chiudendo l'interruttore 10 possiamo far entrare il rumore bianco

![schema2.png](schema2.png)

Controllo del filtro passa basso è il 2.

![contr.png](contr.png)

I jack consentono l'introduzione di segnali esterni, mentre i pulsanti permettono di fare ciò che si fa con i jack in un sintetizzatore modulare.

Il routing si gestisce quindi attraverso 14 pulsanti.

La sorte dei 15 segnali non si limita ai filtri, ma tutto arriva attraverso un modulatore di ampiezza triplo.
![tri.png](tri.png)

Vediamo dei simboli con tutti i jack del pannello per il patching esterno.

Ed alla fine tutto viene filtrato dal filtro ad ottave.

Capiamo anche che da questo sistema vi sono degli ingressi che non sono assegnati per ulteriori sviluppi.

Le frequenze del filtro ad ottave.
![ott.png](ott.png)

L'intonazione del Synket faceva disperare.

![dim.png](dim.png)
Prima dimostrazione del Synket.

### Reale portata del Synket nella storia della musica elettronica?

Esso fece la fortuna di John Eaton, che fu l'unico che si mise a studiarlo.

L'unico che lo sviscerò, anche per la gestione dell'interfaccia fu proprio Eaton che ne fece la sua fortuna.

Nel 1968 uscí un articolo sul Times sul Synket e Eaton, e sono state trovate lettere sul Synket che richiedono questo strumento.

Primo utilizzo del Synket è nell'autunno del 1967 a Boston e poi la prima tournee con il Synket nel Maggio del 1968.

Eaton ne fece la sua fortuna perchè fu l'unico che riusci a dedicarsi totalmente allo strumento per fare qualche cosa che nessun altro fece.

L'idea era che il Synket potesse entrare in un futuro in orchestra. E fu una cosa che venne realizzata e non fu una cosa cosí innovativa.

Grazie alla sua scarsa affidabilità come tastiera temperata, venne utilizzato per realizzare gli "effetti". Ma di avere il Synket in orchestra per fare concerti neoromantici non se ne poteva parlare.

(Tra le le lezioni della lezione 5 vi è un riassunto di Guaccero)

Analisi su Incontri di fasce Sonore fatta come seminario.
____________
Questa settimana vi saranno ascolti tutti legati al Synket per iniziare con una riflessione di carattere generale sulle caratteristiche dello strumento e su cosa non ha funzionato.

E poi una disamina su Musica elettornica viva.

infine vedremo Wishart che è una parte totalemente diversa e che approfondiremo nelle ultime lezioni.
____________
Synth puri digitali non si trovano più
