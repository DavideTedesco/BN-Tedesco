# Appunti della lezione di Sabato 23 Gennaio 2021

## Introduzione a Fase Seconda di Bortolotto

Essi sono i compositori trattati da Bortolotto in "Fase seconda"
- Nono
- Berio
- Castiglioni
- Evangelisti
- Bussotti
- Donatoni -> post modernismo

>Capire ciò che è giusto fare o cosa sia giusto non fare in musica secondo il punto di vista musicologico.

L'attitudine musicale era prima un vettore puntato verso il futuro, mentre oggi vi è un'estraneazione ed una assenza di futuro, una sorta di lago stagnate in cui le carcasse di pesci affiorano, con fenomeni neo-romantici che affiorano, etc...

Bortolotto ci da la grande lezione di una grande riflessione sul significato del fare musica e di scrivere.

## Evangelisti visto da Mario Bortolotto: Evangelisti "Insula dulcamara"

Il titolo, è tutto da interpretare, poichè Insula dulcamara è anche il titolo di un quadro di Paul Klee, in che senso Dulcamara? Dulcamara= dolceamaro?
Dulcamara come venditore di fumo?

![Paul_Klee_Insula_dulcamara](Paul_Klee_Insula_dulcamara.jpg)
Come se Bortolotto non volendo dirlo esplicitamente, insinua che ci sia qualcosa che non va...

A Bortolotto forse non andò a genio la svolta improvvisativa e meno strutturalista, insomma la svolta romana.

Vi è un _senso inquietante in una musica che continua a usare moduli compositivi in analogia con quelli del linguaggio_, ma senza retorica e vissuto dei significati tradizionali, come una musica che parla ma non dice niente...

Strada senza uscita che sfocia in varie soluzioni...

Stockhausen con la Momentforme, sembra risentire di questa crisi, come Evangelisti, che conduceva alle estreme conseguenze le esperienze dell'avanguardia, chiedendosi "il se ci sia ancora qualcosa da scrivere".

Evangelisti si stacca andando verso posizioni informali, come l'improvvisazione, che per quanto possa essere imbrigliata in schemi, procedure, etc, trasmette l'idea di una pratica informale, con l'esplorazione timbrica per idee sonore.

Il giudizio di Bortolotto su Evangelisti è positivo!

(pag 259 di Fase Seconda)

Evangelisti comincia a scrivere musica combinatoria, quando questa tipologia di pratiche stanno nascendo, e chiude le porte anche a un rapporto di sudditanza estetica anche di Webern, Evangelisti è uno di quelli che parte da zero.

Berio dialoga molto con la tradizione al contrario di Evangelisti...

Se come osservatori, abbiamo l'illusione di studiare una linea di progresso della musica contemporanea, abbiamo compositori che presentano opere convenzionali, con Roman Vlad, Chailly, che non rappresentano una avanguardia.

Evangelisti non si fa tentare dalla musica propriamente detta e dai blasoni accademici e conservatoriali fuori dai circuiti ristretti dell'avanguardia.

(Luc Ferrari che suona Bartok e viene sanzionato.)

Ignobili blasoni conservatoriali: insegnanti che dicono che la musica è finita con Strauss, reazione e nemico da combattere, l'istituzione.

Accademia -> intellettuali e musicisti d'avanguardia come disonesti...

Oggi la letteratura non ha figure paragonabili alle figure che operavano negli anni '60, poichè la stagnazione conviene di piú al mercato che si è imposto.

(Jameson 1990, Lyotard filosofi e ricerche sulla post-modernità, postmodernismo)

**Scrittura come archittetura di Evangelisti** -> strutture dotate di autonomia e funzioni, non vi è bisogno di dotare un senso, dalla narrativa.

_Gocciate di luminosità strana_ gocce luminescienti che ci fanno capire che siamo di fronte a un'idea nuova e coerente di musica.

Evangelisti non cerca di dire cose vecchie in modo nuovo, ma cose nuove in modo nuovo...

Evangelisti guarda con tutti e due gli occhi verso avanti secondo Bortolotto, e i musicisti dovevano combattere molto, in cui vi erano episodi in cui il pubblico aveva scontri con i compositori.

>Evangelisti era uno che doveva lottare.

Franco Piersanti -> animazione di Corto Maltese, maestria orchestrale assoluta, con echi del Pelleas e Melisande di Debussy, una sorta di dialogo con il passato, una sorta di rinuncia con cui si rifà al passato.

Evangelisti si stacca dalla tradizione musicale, ed è quasi instancabile a differenza di Boulez e Stockhausen.

(Stockhausen schiavo dello ieratismo, troppo visionario nei suoi atteggiamenti, troppo ammantato di visionarietà a fronte di una musica schiava dei moduli del passato recente...)

Primo saggio importante di Stockhausen è del 1975, a cura di Jonathan Harvey con raccolta di contenuti di vari autori.

>Fase seconda: sguardo della musicologia piú accorta, piú avvertita italiania, nei confronti di un momento in cui si tenta di tirare le somme, e di avere una visione piú complessiva.

Bortolotto dice che Evangelisti è un compositore totalmente nuovo e intransigente e la sua musica non ha legami ne con la tradizione antica, ne con la tradizione recente, e vi sono legami limitati. Evangelisti non si lascia andare ad eccessi, la sua musica è di grande purezza e mira ad un mondo musicale nuovo e inaudito, mettendolo in un piano di superiorità rispetto a Boulez, Stockhausen e Berio.

>Ciò che Evangelisti deve dire, è la struttura in se, con insegnamento di economia weberniano, dove altri ammucchiano Evangelisti scava, lavorando sui vuoti piú che sui pieni.

Come le strutture scultoree di Calder, con lo spazio che la materia modula e quando una determinata struttura è finita, finisce il brano.

Stockhausen è bersaglio di questa critica, perchè il passaggio da punti a gruppi alla Momentforme è un passaggio concettuale.

Nota è elemento dell'enunciato, gruppi come motore e generatore di forma, come il granturco che scoppia e diviene un pop-corn. Elemento liofilizzato che ha espansioni metaforiche.

Idea di gruppi che poi fa passare alla Momenteforme...

Evangelisti non sovradimensiona mai.

>Capacità di Evangelisti è far convergere l'esigenza strutturale di coerenza con l'esigenza timbrica di varietà e gradevolezza.

Evangelisti non è mai ottuso nel suo strutturalismo e le scelte timbriche garantiscono la gradevolezza della sua dimensione.

Spesso l'idea di orchestrazione è totalmente assente... (similare a colorare un disegno in bianco e nero con i colori.)

La musica di Evangelisti la musica funziona come musica, e non è mai cercare di teorizzare qualcosa...

Senza "puntelli" la musica di Nono non si può capire, Evangelisti non ha bisogno di ciò.

Musica che ci fa percepire questioni spaziali piú che timbriche.

La musica di Evangelisti è una musica minerale, sta al posto suo...

Al contrario di Urlo di Munch, Hola di fuerza e y luz di Nono, la musica minerale di Evangelisti ha una sua bellezza da se...

Nell'estroverso di Varèse, esso rimane illuminista, progressista, idea della musica come continua aggiunta, l'insegnamento di Webern è _continua sottrazione_. Le percussioni in Webern sono quasi nulle, mentre il mondo nuovo sonoro che si crea per aggiunta di Varèse con le percussioni.

Per quanto possa essere condotta con atteggiamento scientifico di ricerca, di rigore, non possiamo ridurre la ricerca musicale al rigore scientifico, vi è la fatalità dell'opera, l'artista alla fine è individuale.

Non viene esclusa la fatalità dell'opera, ed essa non puó essere ridotta a mero determinismo e dopo aver detto tutto ciò che distingue Evangelisti dai suoi contemporanei, si finisce per sciogliere le differenze in una sintesi in cui si ritrova una matrice romantica anche nell'operare di Evangelisti.

Limitare i nomi dei pezzi a nomi scientifici, per sfuggire alla retorica romantica, questo problema per Evangelisti si limita però solamente ai titoli, e non alla musica stessa.

Punto di vista di Bortolotto nel 1969 e ne risente il brano Proiezioni con passaggi troppo diatonici che fanno affiorare chiazze di colori.

>Non criticare superficialmente, ma cercare di approfondire la critica a qualcosa di piú profondo.


## Analisi di "Incontri di fasce sonore" di Evangelisti

![analisi](analisi.png)

3 livelli di analisi:
1. suddivisione in sezioni caratterizzate da andamenti particolari o apparizione di strutture particolari o dal residuo di queste zone
  - zona di dichiarazione delle tipologie spettrali, di sintesi additiva
  - sezione di rarefazione con minore densità di accadimenti
  - intensificazione accadimenti
  - morfologia particolare con geometrie particolari con strutture a losanga o triangolo -> strutture in crescendo e diminuendo come se mancasse qualcosa
  - momento caotico di sostanziale impossibilità di impostare i singoli flussi
  - sezione finale di definitiva rarefazione con presenza maggiore del riverbero come se tutte le spettromorfologie dovessero allontanarsi nello spazio
2. marker seguono tutti gli accadimenti percepiti -> anche accadimenti per sottrazione
3. riquadri della sezione intermedia circondano tutti gli oggetti e gli spettri

Criterio di analisi ad albero con livelli intermedi, ad una cifra, a 2 e 3 cifre.

Le lettere dell'alfabeto seguono un criterio personale raggruppando con lo stesso indice ma con lettere diverse dell'alfabeto.

I cerchi rossi segnalano delle strutture ribatutte che fanno anche da legante.

Vi sono movimenti spettrali verso l'alto e verso il basso con frecce blu, vi è una frequenza insistente nella sezione quinta, sequenza caotica.

La distribuzione dei marker è abbastanza omogenea, segnala un andamento ed una concezione strumentale.
Concezione di note e cellule e di musica come linguaggio, in forme sostanzialmente sintattiche, i quadri rossi posso essere di fatto dei sintagmi. Questa definizione è stata applicata da Francesco Giomi, che fa un uso scientifico di questa terminologia Schaefferiana.

## Guaccero - Musica ex Machina

Lettura dal testo: "Musica elettroacustica a Roma: Gli anni Sessanta" di Pizzaleo

Guaccero è fra i piú grandi animatori musicali e culturali degli anni '60 e '70, ed uno dei piú convinti entusiasti di fondare il gruppo R7, anche se Guaccero si deve esprimere in questi termini:
(pag. 150)

### Disgregazione di R7
> a)Situazione e soci

...
>3. _Interesse verso lo studio_. A parte i nuovi soci, che non hanno preso ancora contatto con lo Studio, la situazione per i vecchi soci è la se- guente: Marinuzzi, Ketoff, Evangelisti e in una certa misura Guiduc- ci hanno perso interesse per la vita e l'attività dello Studio. Marinuzzi perché lavora a parte alla sua apparecchiatura (e ha sempre detto di voler andarsene); Ketoff, che si dedica ormai ad altre attività e dopo le note questioni di regolamento il suo lavoro lo fa fuori; Evangelisti, perché ha dato il via allo Studio di S. Cecilia, non ha fiducia per le capacità “artistiche” dello Studio e non ha interesse per quelle “commerciali”; Guiducci, che da mesi si occupa poco dello Studio e ora preferisce dedicarsi ad altro lavoro. Restano Branchi, Guàccero e Macchi. Branchi e Macchi hanno effet- tuato dei lavori (portando anche del materiale allo Studio); Guàccero, pur apportando del materiale, non ha realizzato lavori, se non di minore mole. Questa situazione influisce negativamente e sulle strutture e sulle at- tività dello Studio, come sulla psicologia dei soci, che oscilla fra disinteresse, superficialità, disagio e impotenza.
b) Prospettive dell'attività commerciale

>1. È noto che una parte dei soci crede di poter sostenere la spesa di gestione con la sola attività elettronica commerciale, altra parte crede che occorra trovare uno sbocco nell'attività di registrazione (sala di registrazione). C'è ancora l'opinione molto netta (Evangelisti) che lo Studio non possa oggi e domani che dedicarsi ai lavori di elettronica commerciale, escludendo la parte artistica: questo perché l'attività ar- tistica dovrà essere espletata dal costituendo Studio di S. Cecilia, nel quale Evangelisti è primariamente e direttamente, se non esclusiva- mente interessato. Lo Studio R7 servirebbe a Evangelisti e a S. Ceci- lia, per il momento per quello che può offrire (nonostante il bluff), per poi essere lasciato al suo destino non appena lo Studio di S. Ceci- lia sia pronto. In quest'ultima prospettiva, della pura attività elettroni- ca commerciale fine a se stessa (cioè non allo scopo di sostenere la crescita di uno studio a livello artistico e di ricerca), la attuale struttura dello Studio è pletorica, bastando un capitale abbastanza limitato (un Moog, alcuni magnetofoni e altoparlanti e un mixer), che addirittura un privato può mettere a monte, o per al massimo due o tre persone, con ovvia maggiore agilità e ripartizione degli utili.

Vi è una grande disilussione di Guaccero su ciò che sta accadendo ad R7, il destino di R7 non era propriamente dei migliori e Guaccero si rivolge a qualcosa di nuovo.

### Centro della musica sperimentale

A Guaccero non interessava il commerciale, ma piú la ricerca e nasce l'idea del centro della musica sperimentale:
- associazione senza finalità di lucro
- non ha l'ossessione della finalità commerciale
- presenza di troppe personalità(Evangelisti, Guaccero, Macchi, Ketoff)

R7 non nasceva sotto una buona stella, mentre questo centro con:
- Schiaffini
- Curran
- Scarlato
- Luca Lombardi
- Sergio Rendine

Si tratterà di compositori molto giovani ed alla corte di Guaccero e la situazione a livello collettivo sarà molto diversa.

La prima preoccupazione di Guaccero in questo gruppo era il cercare una formula che superasse la formula borghese dei concerti, cercando una **nuova allenza** tra la musica sperimentale ed un pubblico nuovo, ponendosi in maniera antagonistica alla musica borghese. In quel momento un compositore non puó fare ciò che vuole, ma vi era un'urgenza politica stabilendo un pubblico per non la sola entrata economica o la sola possibilità di ascolto delle composizioni.

Lettera di Guaccero a Luca Lombardi:
>A questo punto c'è una scelta da operare, sulla quale chiedo la tua opinione. Si tratta di “dove” impiantare questo locale. Se avesse dovuto servire quale luogo di prova o da studio elettronico l'ubicazione sarebbe stata indifferente. Se invece si vuole fare attività pubbliche la cosa cambia. Che tipo di pubblico vogliamo? Di quale estrazione sociale? E, meglio, non sarebbe il caso di non pensare più al “pubblico”, ma a “comunità”? È chiaro che per noi “comunità” può significare comunità a sinistra: quindi rapporto con altra gente che non siano gli abituées della Filarmonica o di S. Cecilia. La conclusione pratica potrebbe essere la seguente: esclusio- ne di zone borghesi (e fasciste) come i Parioli, il Nomentano, certa parte del Centro, Prati ecc. Si può provare o con una certa periferia (ad es. Cinecittà, v. il precedente della Maraini che ha aperto un locale a Centocelle) o certe zone più popolari del “centro” (Trastevere, magari Testaccio). Si può anche vedere dal modo di votare il “colore” dei vari quartieri. Che ne dici?

Collocarsi in un tessuto sociale che abbia una ben determinata esigenza politica, per il fatto che Guaccero era un convinto socialista di sinistra.

Tutto ciò si scontra con una concezione musicale, ed il gruppo che doveva chiamarsi _Synthesizers_ e invece si chiamò poi _Musica Ex Machina_.

Una delle prime attività che si poneva come centro musicale di creazione e realizzazione, ci fa pensare a quella convergenza fra teatro e musica elettronica che conferma la natura complessa e multimodale, plurilinguistica e translingustica.

A Roma vede la fioritura di musica di vario genere: musica di commento, improvvisazione etc...

Definizione di musica elettronica polimorfica romana, l'idea che teatro musicale ed elettronica seguino uno stesso destino ad esempio.

#### Evoluzione dell'elettronica

Vi è stato il Synket, ma vi è stato anche il MOOG, il Buchla ed i sintetizzatori dell'EMS, che rilasciano uno dei sintetizzatori piú influente della musica elettronica ovvero il VCS3.

Questo sintetizzatore è molto agile, stabile, affidabile e incarna l'idea che il sintetizzatore aveva di: studio portatile.

L'azione del compositore elettroacustico, poteva essere democratizzata.

Il VCS3 costava anche 6000 euro...

(Lombardi acquistó un VCS3 da Guaccero)

Il VCS3 portava a diretto contatto la percezione della musica elettronica.

Il compositore acquisirà la responsabilità dei propri suoni senza farsi aiutare da un tecnico.

Ma ci interessa il fatto che la pratica del sintetizzatore porta con se alcuini problemi:
1. questi compositori non usavano la tastiera, ed andavano alla ricerca del suono, di masse, di impulsi e di gestualità, ed è il motivo per cui un brano per elettronica dal vivo non usa la tastiera
2. questi sintetizzatori, potevano fare molte cose, ma andando in genere verso un collo di bottiglia, una strozzatura:
  1. tempo varianze limitate dalle 2 mani del compositori, rischiando di avere dei clichè, non vi era bisogno del virtuosismo meccanico
  2. dimensione dell'improvvisazione, non sapendo cosa improvvisare(schemi, percorsi già stabiliti), e dei pezzi determinati con largo spazio all'improvvisazione, il limite fra queste due dimensioni quale sarà?


Vi sono problemi teorici per l'utilizzo di questi sintetizzatori, perchè alla fine le possibilità realizzabili sono limitate dalle possibilità della performance.

#### Che fine fece l'alleanza?

Nell'idea di Guaccero si doveva formare una sorta di allenza tra la comunità e il gruppo di musicisti che la esprime. Non doveva essere una proposta autoritaria di un musicista sul pubblico. Vi doveva essere una condivisione di idee sociali con condivisione con la comunità.

Alla fine per fare ciò serviva degli spazi che non riuscirono a ottenere, e la possibilità di farsi ascoltare in contesti che non erano interessati a loro.

Storia di frustrazione...

#### Come si finanziavano?

Vi era la necessità di finanziarsi, poichè non vi era un'attività commerciale.

Vi era una legge: 800/1967 per le associazioni senza scopo di lucro all'art.37:
>          Art. 37. Concorsi, attività sperimentali e rassegne [23]
     [Sul fondo di cui all'art. 2, lettera b), sentita la Commissione centrale per la musica, possono essere assegnate sovvenzioni a enti, istituzioni ed associazioni non aventi scopo di lucro che, al fine di promuovere la cultura musicale, di stimolare la nuova produzione lirica, concertistica e di balletto, e di reperire nuovi elementi artistici di nazionalità italiana, effettuino concorsi di composizione ed esecuzione musicale, corsi di avviamento e perfezionamento professionale, stagioni liriche sperimentali e rassegne musicali.]

Realizzarono un corso di composizione presso l'istituto latino-americano con 3 classi per finanziarsi:
1. Synthesizer patching -> Mauro Bortolotti
2. Elementi di elettroacustica e sintesi del suono -> Fabrizio Zannini
3. Composizione e improvvisazione con il Synthesizer -> Guaccero

Essa fu un passo importante e si ebbe l'occasione di diffondere nella comunità dei musicisti, quello che stavano facendo questi musicisti, ricordandosi che questi corsi erano anche per persone non interessate alla musica colta.

#### I concerti

Alla fine non si ebbe quella risonanza cosí grande da poter creare un pubblico.

Fecero di fatto concerti in luoghi rinomati e realizzando di fatto un paradosso rispetto al pubblico a cui volevano avvicinarsi.

Lombardi sostiene che questo gruppo fece 3 concerti...

Il programma era fatto con alcuni brani con titoli predeterminati.

Recensione del concerto dei _Synthesizers_ a Padova:
>Bisogna servirsi del suono della macchina, delle sue possibilità, della sua estrema malleabilità: ma anche delle sue capacità provocatorie. E tanto più la provocazione è efficace, quanto più i suoni sfuggono ad ogni preordinata logica riconoscibile e ripetibile: casualità, più ancora che improvvisazione. Provocato, l’ascoltatore non resta più passivo: muta il rapporto tra soggetto e oggetto. Questa tematica era tutta sottesa nel concerto tenuto alla Sala dei Giganti dal Synthesizer Trio, formato da Domenico Guàccero, Alvin Curran e Luca Lombardi. (...). Ritto dietro la sua macchina il compositore sembrava aver rinunciato al “gesto”, momento tipico della sua musica precedente. In realtà, l'elemento “teatrale” era tutto trasferito nelle violente contrapposizioni dei suoni elettronici, tra fasce concitate e urtanti, e zone pacate e gradevoli. Un dialogo tutto umano, fra stati d'animo immediatamente rappresentati. Perché i suoni di Guàccero saranno anche casuali, ma mai gratuiti. L'amore al suono, a un suono più “trovato” che inventato, per Guàccero diviene irresistibile tendenza all'onomatopea, alla ripetizione dei rumori della vita: il battito cardiaco, il respiro, il grido d'angoscia o di rabbia, il sospiro della tristezza (FRANCESCO CAVALLA 1972).

Cavalla aveva capito qualcosa, ma non proprio tutto, Guaccero non vuole essere visto come gestore della casualità. Questa recensione da un'idea di quello che era la ricezione di questa musica.

#### Il linguaggio

Altro problema, oltre degli spazi che mancavano, era il linguaggio, ovvero la difficoltà di indipendenza rispetto al mezzo. Problema che si presenta quando noi creiamo nuove interfacce, difficile equilibrio tra ciò che posso fare e non fare, magari ripetendo sempre piú cose.

Andò in fumo il gruppo _Musica ex machina_, il CMS non andò in fumo e culminerà verso la fine degli anni '70, nello studio 29A.
Guaccero continuò a lavorare al suo gruppo di ricerca teatrale.

**Vi era quindi un collo di bottiglia espressivo**...


#### Fallimento del progetto sociale

Anche il passaggio del nome, da The Synthesizers a Musica ex machina, formula di Fred Prieberg, e titolo di trasmissioni radio, questa fu una virata verso il mondo delle sale da concerto e della filarmonica, allontanandosi dalle persone.

### Perchè ci interessa Guaccero e le sue esperienze?

Perchè ci fa riflettere su problemi universali nel mondo della musica elettronica:
- problema della comunita
- contrapposizione tra gruppo e individuo
- problema della comunicazione tra individuo e pubblico
- problema dell'interfaccia
- problema dell'evasione dai linguaggi
- problema di suonare cosa per chi
- problema di conciliare il principio di determinazione(improvvisazione e/o studio?)

Vi è un segnale in un appunto di Guaccero, che non riesce a sintonizzarsi sul progressive rock e l'espansione esponenziale del MOOG come espansione della sintesi. Disco Beaches Brudy di Miles Davis, in cui l'elettrico arriva a dimensione nuova, mai citata, come altri gruppi di improvvisazione in inghilterra e America, ed erano importanti anche gruppi di sperimentazione.

Miopi di questi musicisti nei confronti del rock e jazz, che li costringe a una dimensione elitaria, come se ci si pone di fronte a un qualcosa di elitario che si puó superare con strumenti elitari...

Il gruppo di Guaccero non aveva una visione dialettica di se stessi, cercando di collocarsi, cercando di essere ciò che altri facevano meglio di loro, e noi lo riusciamo a cogliere solo nella postmodernità stagnante.

**Grandi sollecitazioni** che ci ritroviamo fra i piedi e con cui dobbiamo fare i conti.
