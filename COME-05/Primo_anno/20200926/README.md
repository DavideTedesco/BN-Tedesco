# Appunti della lezione di Sabato 26 Settembre 2020

Richard Stallman -> hacker molto attento alla sicurezza

__________
## Cosa non viene insegnato e che serve, da richiedere:

- spazializzazione
- machine learning
- convoluzione
- tecniche nel dominio della frequenza
__________  
## Programma nei corsi d'insegnamento

come un docente intende un insegnamento, programma di studi d'informatica musicale

buchi formativi d'istruzione -> rendendosi conto di quale dovrebbe essere l'universo d'informatica musicale legato all'esperienza musicale compositiva

__________
Esame d'informatica -> composizione per l'esame -> non valutata dal punto di vista compositivo ma dal punto di vista informatico
__________
**Sessione d'esame del 30 Ottobre**

Commissione compositiva (Bernardini, Silvi, Giordano)

Commissione non compositiva (Scalas, Citera, Pizzaleo)
__________
Criteri per il giudizio in sede d'esame:
- se lavoro è andato benissimo -> il massimo è 28, l'integrazione è data dagli altri docenti e da come viene presentato il lavoro puó ribaltare o confermare il risultato.

Si pone la decisione dunque su due punti chiave:
1. Attenzione al linguaggio per presentare un lavoro
2. Possibilità di decisione della commissione
__________
Parte importante oltre la realizzazione sarà il modo in cui sono spiegati gli algoritmi dal punto di vista informatico.

Stiamo parlando ad una platea di specialisti, far vedere un nostro punto di vista. Il punto di vista è importante che è determinato da scelte algoritmiche, scelte compositive etc...

- algoritmico
- organizzazione logica
- compositiva
__________
## FreeVerb

Riverbero che tiene presente ancora solo la coda del Riverbero

#Struttura

Essa prevede 8 comb e 4 allpass che esce in mondo, per avere un riverbero stereo potrò fare in 2 modi:
1. copia identica con numeri diversi
2. oppure defasizzare tutto in un altro canale

![struttura freeverb](struttura_freeverb.png)

I ritardi sono in campioni, .84 è il valore di gain e .2 è il coefficente di filtraggio espresso in valori assoluti e non in radianti.

Sugli allpass ci sono infatti due valori molto piccoli e manca il coefficente di filtraggio (abbiamo coefficente di retroazione e ritardo), il coefficente di retroazione dell'allpass non si lega al T60, ed è inferiore a quello che metteremmo se scegliessimo un tempo di riverbero molto piccolo, inferiore al piú piccolo valore che metteremmo sul T60, non influisce e non crea la coda l'allpass, smussa i transienti, crea la coda e confonde le fasi.

Scopo dei 4 allpass di fila sono le 3 caratteristiche espresse sopra.

Freeverb nacque alla fine degli anni '70.

[libro di JOS](https://ccrma.stanford.edu/~jos/filters/)

Dopo aver implementato l'allpass di primo ordine, possiamo vedere l'allpass di secondo ordine.

![allpass_second](allpass_second.png)

Dopo aver realizzato un allpass, possiamo dunque pensare alla realizzazione di varie tipologie di filtri che nascono dall'implementazione.

Quando l'eccitatore non di un comb low pass non è una sorgente ma un impulso diviene un algoritmo simile al Karplus strong.

## Implementazione del Freeverb

Quanti ms servono invece di mettere i campioni?

![freeverb](freeverb.png)

Potremmo iniziare a considerare tutti i parametri per la descrizione del riverbero.

Parametri di un ambiente come:
- grandezza
- densita

#### Formula di sabine
Da volume(Lung * larg *altezza) e  coefficenti di assorbimento stanza ricaviamo il T60
Abbiamo quindi:
- T60
- filtraggio complessivo

#### EDT
Ci serve anche la determinazione della prima riflessione(EDT)(distanza tra noi e il primo ostacole) ed il ritardo minimo del comb.
Quando parte la coda ci interessa sapere il primo comb quanto ritarda. Ovvero sapere l'inizio della coda che ritardo ha.

Esempio del primo riverbero di Moorer, ci sono 6 comb, un solo allpass ed un ritardo, che è pari al ritardo del multitap delay che fa le Early Reflections

![moorer_reverb](moorer_reverb.png)
Le Early reflections vengono realizzate da Multitap delay con N valori, importante che il suono originario vada nella linea di ritardo e si sommi ad ogni linea, cosí da far viaggiare il suono originario e tutte le riflessioni. Quando MN finisce(finiscono early reflections, inizia la coda)

La distanza tra segnale diretto e la prima riflessione(M1) è quanto siamo distanti dal primo ostacolo, e C1 è coerente con esso (con la stessa distanza) come se fosse la riflessione multipla della prima linea di ritardo.

I ritardi nei comb li scelgo con numeri primi successivi.

(trovare logiche personali per riverveberi)

#### Formula per il ritardo in ms del suono avendo la distanza

distanza/costante = t(s) ->*1000 = ms
6 metri/343,3 = 0,01749 -> *1000 = 17,49 ms

## Integrazione di Moorer
L'integrazione di Moorer introduce le Early Reflections del riverbero.

Da fare è con early reflections e ritardi calcolati del multitap delay e del comb.

__________

## Nella prossima lezione
- Vedere la distanza tra il freeverb e l'integrazione di Moorer

- esperienza di Chowning -> concetto di riverberazione e rapporto col segnale riverberato, suoni riverberati e suoni diretti in che proporzione stanno, come trattare un riverbero in base al segnale.

- partire dunque dal punto piú semplice

- Chowning sorgenti in movimento, filtraggio ad alte e basse frequenze ed effetto doppler, riververo locale e globale

- strumenti base per poter realizzare sorgenti in movimento
