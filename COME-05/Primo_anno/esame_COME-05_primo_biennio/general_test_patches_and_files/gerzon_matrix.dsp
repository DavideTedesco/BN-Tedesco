import("stdfaust.lib");

gerzon_matrix(L,R) = F,L,B,R
  with{
    F = (1/sqrt(2))*(L+R);
    B = (1/sqrt(2))*(L-R);
};

process = gerzon_matrix;
