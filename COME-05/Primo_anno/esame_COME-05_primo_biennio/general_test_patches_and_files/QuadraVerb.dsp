declare name "QuadraVerb";
declare version "0.1";
declare author "Davide Tedesco";
declare license "CC3";

import("stdfaust.lib");

//QuadraVerb - a quadraphonic reverbertor for the Surround Sound from 2-channel stereo by Michael Gerzon
//
//--------------------------------------
//GUI
group(x) = vgroup("Reverb controls",x);
knobsGroup(x) = group(hgroup("[0]",x));

//Combfeed control
origSR = 44100;
scaleroom = 0.28;

combfeed = knobsGroup(hslider("[0]Room Size [style:knob]",0.5,0,1,0.01)*scaleroom*ma.SR/origSR + offsetroom) : si.bsmooth;

//Damping control
scaledamp = 0.4;

damping = knobsGroup(hslider("[2]Damping [style:knob]",0.7,0,1,0.001)*scaledamp*ma.SR/origSR: si.bsmooth);

//Early reflections quantity
rev_quantity = knobsGroup(hslider("[0]Dry/Wet [style:knob]",.42,0,1,0.001) : si.bsmooth) ;

tail = knobsGroup(hslider("[1]Reverb Tail [style:knob]",0.4,0,0.95,0.01)*ma.SR/origSR: si.bsmooth);

room_size = knobsGroup(hslider("[2]Room size",0.0004,0,0.033,0.0000001)) ;

general_volume = (hslider("[3]Volume",0.9,0,1,0.01): si.bsmooth);

//--------------------------------------

//Gerzon matrix
//
//From the article of Michael Gerzon: "Surround Sound from 2-channel stereo", 1970
//
//
//Gerzon matrix in sum-difference configuration
//         F
//      •  |  •
//   •     |     •
// L ----- + ----- R
//   •     |     •
//      •  |  •
//         B

gerzon_matrix(L,R) = F,L,B,R
  with{
    F = (1/sqrt(2))*(L+R);
    B = (1/sqrt(2))*(L-R);
};

//Multi-tap delay

n_er = 100;

room_early_delay = ba.sec2samp(room_size);
early_reflectionsF = _ <: par(i, n_er, @(i+room_early_delay+3) ) :> _/n_er ;
early_reflectionsL = _ <: par(i, n_er, @(i+room_early_delay) ) :> _/n_er ;
early_reflectionsB = _ <: par(i, n_er, @(i+room_early_delay+4) ) :> _/n_er ;
early_reflectionsR = _ <: par(i, n_er, @(i+room_early_delay+1) ) :> _/n_er ;

//Comb filters

lpcomb(dt,damp) = (+:de.delay(4096,dt))~ (*(1-damp) : (+ ~ *(damp)) : *(tail));

comb_filtersF = _  <: lpcomb(4001,damping),lpcomb(3001,damping),lpcomb(2003,damping),lpcomb(1009,damping),lpcomb(503,damping),lpcomb(307,damping),lpcomb(103,damping),lpcomb(41,damping) :> _ ;
comb_filtersL =  _  <: lpcomb(3989,damping),lpcomb(2999,damping),lpcomb(1999,damping),lpcomb(997,damping),lpcomb(499,damping),lpcomb(293,damping),lpcomb(97,damping),lpcomb(37,damping) :> _ ;
comb_filtersB =  _  <: lpcomb(3967,damping),lpcomb(2971,damping),lpcomb(1997,damping),lpcomb(991,damping),lpcomb(491,damping),lpcomb(283,damping),lpcomb(101,damping),lpcomb(43,damping) :> _ ;
comb_filtersR =  _  <: lpcomb(3947,damping),lpcomb(2969,damping),lpcomb(1993,damping),lpcomb(983,damping),lpcomb(487,damping),lpcomb(281,damping),lpcomb(89,damping),lpcomb(29,damping) :> _ ;


//Allpasses

fb_comb(t,g) = (+ : de.delay(ma.SR/2, int(t-1)))~*(g) : mem;

allpass(t,g) = _ <: *(-g)+(fb_comb(t,g)*(1-(g*g)));


allpasses = allpass(37,.9) : allpass(19,.9) : allpass(53,.9) : allpass(31,.9) : allpass(67,.9) : allpass(59,.9);


//Unit reverberators

unit_reverberatorF = early_reflectionsF : comb_filtersF : allpasses;
unit_reverberatorL = early_reflectionsL : comb_filtersL : allpasses;
unit_reverberatorB = early_reflectionsB : comb_filtersB : allpasses;
unit_reverberatorR = early_reflectionsR : comb_filtersR : allpasses;


unit_reverberators = unit_reverberatorF, unit_reverberatorL, unit_reverberatorB, unit_reverberatorR ;

direct_sound = 1;

dry_wet = rev_quantity-direct_sound;

quadra_verb = gerzon_matrix <: par(i, 4, dry_wet*_),_,_,_,_ : (_,_,_,_),(unit_reverberators):>  _,_,_,_;
//--------------------------------------

process = quadra_verb;

//to use the reverb
//process = _,_ : quad_verb : par(i,4,_*general_volume) : _,_,_,_;
