import("stdfaust.lib");

fb1 = hslider("fb1",0.001,.001,0.9998,0.001);
fb2 = hslider("fb2",0.001,.001,0.9998,0.001);
damp = hslider("damp",0.8,.001,0.9998,0.001);
spread = hslider("spread",1,0,8192,1.);
drive = hslider("drive",0.05,.001,0.9998,0.001);
offset = 0;
maxdel = 2048;
intdel = 1024;
aN = 0;

w = hslider("window size in samples",8192,0,8192,1);
x = hslider("cross fade dur in samples",8192,0,8192,1);
s = hslider("semitones",5,-24,24,.5);

pitch_shifter = _ <: ef.transpose(w, x, s)+ef.transpose(w, x, -12)+ef.transpose(w, x, -24)+ef.transpose(w, x, -36)+ef.transpose(w, x, -10) : _/5;

volume = hslider("volume",0.1,.001,0.9998,0.001);

lrfb(L,R) = L,R,F,B
  with{
    F = (1/sqrt(2))*(L+R);
    B = (1/sqrt(2))*(L-R);
};

end_module = fi.allpass_comb(maxdel,intdel,aN),fi.allpass_comb(maxdel,intdel,aN),fi.allpass_comb(maxdel,intdel,aN),fi.allpass_comb(maxdel,intdel,aN) : fi.lowpass(6,8000),fi.lowpass(6,8000),fi.lowpass(6,8000),fi.lowpass(6,8000) : fi.highpass(6,50),fi.highpass(6,50),fi.highpass(6,50),fi.highpass(6,50) : _*volume,_*volume,_*volume,_*volume;

process = _ : ef.cubicnl(drive,offset) <: pitch_shifter,_ : lrfb : re.stereo_freeverb(fb1, fb2, damp, spread),_,re.mono_freeverb(fb1, fb2, damp, spread) : end_module;
