# Appunti della lezione di Sabato 3 Ottobre 2020

- Stockhausen si è trovato nel passaggio del cambiamento da acustico a elettronico e si è dunque trovato a disconoscere alcuni suoi brani piú vecchi.

- tante persone non fanno troppo sforzo per l'ascolto, ma certa musica si realizza per gente che ha passione -> musica per persone appassionate (che hanno un organo caldo)

- scegliere il proprio percorso musicale e compositivo in base ai propri pensieri e da quello che si sa fare e che si vuole

- non per forza la realizzazione passa dalla complicazione, magari passando per qualcosa di piú semplice riusciamo a rendere piú l'idea
_________
## Sintesi sottrattiva

Usare sintesi sottrattiva nel brano se vogliamo -> andiamo in sottrazione da un segnale di origine. In genere le tecniche di sintesi funzionano tutte per addizione o distorsione. DNL crea un artificio ovvero una distorsione dello spettro. FM modula la frequenza con addizione di parti di spettro.

I filtri sono uno strumento per realizzare la sintesi sottrattiva. Dunque la sintesi sottrattiva è creare timbri da un timbro piú complesso.(ovviamente non si possono usare le sinusoidi per fare sottrattiva).

Non trattati i filtri ellittici o di Chebychev. Capito il meccanismo della realizzazione dei filtri si puó realizzare qualsiasi filtro. Ad esempio usando strumenti come [micromodeler DSP](https://www.micromodeler.com/dsp/)

Primo acchitto con la sottrattiva che si puó avere è ritrovare le sinusoidi con la sottrattiva.

Tanti sintetizzatori analogici e digitali funzionano in sottrattiva, dato che sono già ricchi in partenza che variazione di ampiezza.

Riuscire a capire dalle schede come realizzare i primi modelli fisici in cui vi è molta sottrattiva.

Tutti i modelli fisici sono tutti con un eccitatore di base(impulso, treno di impulsi o noise). L'eccitatore anche per gli strumenti fisici è un impulso o un rumore. Sempre eccitatori di tipo complesso a livello di frequenze. Nel flauto spesso si usa il rumore bianco per simularlo.

In ogni caso si realizzerà sintesi sottrattiva.
_________

## Riverbero di Moorer

- parte di multitap dealy per EDT
- tail inizia con ultima Early Reflection
- l'allpass

![moorer_rev](moorer_rev.png)

Seguendo lo schema realizzare il riverbero di Moorer, ovviamente questo schema è un solo canale, per avere riverbero stereo bisogna duplicare il riverbero.

M1 e C1 hanno piú o meno lo stesso tempo, il resto di M variano in base alla stanza, mentre C1, C2, C3 etc... sono numeri primi.

7/8 early reflection a destra e lo stesso numero a sinistra.

## Esperienza di Chowning per la spazializzazione

Esperienza efficace e semplice da implementare. Fece il ragionamento del che cosa succede quando un segnale si allontana, perchè abbiamo una distanza minima. Chowning fa un ragionamento per lo spazio quadrifonico.

Il ragionamento del suono è sempre dalla linea tra gli speakers in poi. Consideriamo gli altoparlanti come adiacenti alla testa dell'ascoltatore. C'è solo la possibilità di far muovere il suono al di fuori.

Quasi tutti i meccanismi di spazializzazione hanno come spazio minimo la distanza tra l'altoparlante e lo spettatore.

La wavefield synthesis si basa sul concetto del fronte d'onda. Una serie di altoparlanti messi in serie ricostruiscono una sorgente di un fronte d'onda , riuscendo a costruire anche la posizione interna allo spazio, riuscendo anche a costruire la posizione interna dell'oggetto nel suono.

(Sicomoro, sistema di array di altoparlanti che funzionavano con i fronti d'onda)

![wfs](wfs.png)

Idea perfetta di diffusione del suono in cattedrale o chiese grandi è l'utilizzo di una wavefield synthesis. Come la diffusione realizzata dall'organo con tutte le canne che sono come se fossero tutti piccoli diffusori.
L'organo sfrutta dei corpi agglomerati per la realizzazione del timbro.

Scopo dell'organo non è mai da sentire come un qualcosa di puntiforme, ed è uno strumento con possibilità timbriche e potenze sonore notevoli.

Organo è qualcosa fatto in modo pervasivo dello spazio

Tradizione bachiana veloce e per chiese costruite in legno, mentre la tradizione italiana è realizzata ascoltando i luoghi poichè le chiese sono realizzate in materiali diversi.

Al variare della distanza il segnale diretto decade molto rispetto al segnale riflesso.

Il riscalamento di ampiezza del segnale diretto è 1/d mentre segnale riverberato è 1/sqrt(d). Dunque al variare della distanza vengono riscalate in modo diverso le ampiezze.

## Riverbero globale e riverbero locale

Quando un oggetto o un segnale audio produce suono in uno spazio lo spazio si comporta in 2 modi diversi. Percepiamo la riverberazione in 2 modi diversi.
1. Lo spazio si comporta in un modo a prescindere dalla nostra posizione, lo spazio risuono nello stesso modo in un senso -> riverbero globale
2. da un secondo punto di vista invece spostandomi lo spazio risuona in modo diverso da un altro punto -> riverbero locale -> componenti di direttività in se

Se dunque sono in un punto dello spazio, esso risuonerà in un certo modo con una parte

Piú la fonte è vicina piú il riverbero è omnidirezionale, piú la fonte si allontana, piú il riverbero è localizzato, ci deve essere un modo per passare da un riverbero globale, a un riverbero locale. Allontanandosi la fonte sonora prende una direttività anche il riverbero, che non è la stessa che il suono ha quando sta a distanza 1.

L'implementazione di Chowning era sempre sulla quadrifonia. La sorgente sonora viene riverberata in 2 vie con 2 mandate:
1. riverbero globale -> stesso riverbero con stessa quantità ed intensità su **tutti gli altoparlanti**.
2. riverbero locale -> sparisce da una cassa e appare su di un'altra, e funziona dunque a con **2 speaker**.

Avere la possibilità di un riverbero omnidirezionale che suona ugualmente a prescindere dalla posizione ed avere invece un riverbero locale in una posizione tra due altoparlanti.

Per gestire le due tipologie di riverberi le proporzioni sono molto semplici:


Suono diretto scalato = 1/d
Suono riverberato scalato = 1/√d

Ma il riverbero deve essere riscalato con la suddivisione in riverbero locale e globale.

![rev_glob_loc](rev_glob_loc.png)

Aumentando la distanza il riverbero locale aumenta(poichè si localizza di piú), mentre il riverbero globale diminuisce(poichè si allontana la sorgente).

Potremo in un mixer realizzare il riverbero locale e globale.

Dal nostro canale possiamo aggiungere due send, uno locale e uno globale.

Il riverbero globale è pre-pan mentre il locale è post-pan tutti e due con riscalamente visto prima.

Un riverbero è localizzato, l'altro no.

![rev_glob_loc_mixer](rev_glob_loc_mixer.png)

Una parte del riverbero (quello globale) dato che è pre-pan suona dapertutto, mentre la mandata locale tiene conto del posizionamento (è post-pan).

Il riverbero globale si riscala di 1/d, mentre il riverbero locale si riscala 1-1/d.

Ad un metro abbiamo quasi solo riverbero globale, mentre allontanandosi la fonte, sentiremo il riverbero sempre piú localizzato.
![rev_glob_loc_stereo](rev_glob_loc_stereo.png)

### Riepilogo sul riverbero locale e globale

Il segnale viene riscalato e poi spazializzato L e R, decidendo una posizione del segnale. Il segnale diretto va poi nel riverberatore, che ha 2 mandate, la prima globale che esce nello stesso modo su L e su R, ed una locale che segue la stessa spazializzazione del suono diretto, ciò viene riscalato in uscita secondo la proporzione suono diretto-suono riverberato. Ed il locale e globale hanno via via proporzioni diverse.

![rev_glob_loc_riscalamenti_pan](rev_glob_loc_riscalamenti_pan.png)
Le formule sono speculari e l'out del riverbero locale è legato al pan.


Riverbero a 6 uscite, due per segnale diretto, due per riverbero locale e due per riverbero globale.

_________

Libri consigliati di Sebastian Foer

- se nulla importa...
- lontano vicino...
