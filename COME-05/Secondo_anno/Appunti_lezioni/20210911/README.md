# Appunti della lezione di sabato 11 settembre 2021

## Deterministica e statistica

Dopo aver visto un po' di argomenti, si lascerà spazio all'applicazione di ciò che vedremo oggi, con ad esempio delle tesine.

Incontro basato in parte sui modelli fisici, e poi sulle implementazioni dell'attrattore di Lorenz.

Per caos si intende un insieme di valore, dove il valore successivo è legato al valore precedente su regole non determinabili a priori.

![c](c.png)
- Caos deterministico, esso sembra non avere legami fra i vari valori.

- Caos stocastico (anche detto caos statisco), esso è basato su delle leggi probabilistiche.


### Caos deterministico

![cd](cd.png)

So in che punto del tempo so dove sia la massa o la molla.

Ma vi sono dei sistemi che variano in base alla posizione iniziale varia la posizione finale.

![ca](ca.png)

Abbiamo descritto il pendolo nello spazio delle fasi con:
- ascisse
- ordinate

Un pendolo senza attrito descrive perfettamente un cerchio con massima accelerazione ed il pendolo era fermo e si descriveva una sorta di cerchio.

Se vi è dell'attrito il cerchio spiraleggia e va verso il centro del piano delle fasi.

#### Modelli di simulazione del caos deterministico

Siamo abituati a pensare ad un'applicazione con un qualcosa generato da un dominio x che si rispecchia nell'immagine o codominio:
![x](x.png)

Il grafico è la traccia dell'applicazione che realizzo:
![g](g.png)

Si considera nel caos deterministico un unico punto del dominio, si prende la funzione, si vede cosa succeda nell'immagine e poi lo riporto nel dominio e ci riapplico la funzione.

Abbiamo quindi un meccanismo di retroazione con un feedback in cui un insieme dei valori dell'immagine ritorna sul dominio:
![p](p.png)

Non si studiano quindi tutti i punti del dominio, ma alcuni valori particolari.

(Le funzioni interessanti saranno quelle non lineari)

#### Esempio

![r](r.png)

1 è detto punto fisso dell'iterazione.

Cosa succede se mi sposto di poco:
![1.3](1.3.png)

Gradualmente vado vero il punto fisso...

![0.9](0.9.png)

Vediamo il programmino in Python:

![pr](pr.png)

#### Bacino di attrazione

Esso è l'insieme di tutti i valori del dominio che vanno verso un punto fisso:

![ba](ba.png)

Se comincio ad iterare vado verso il punto che è definito come bacino d'attrazione che è in genere un punto unico.

![or](or.png)

Tutti i valori dell'insieme che avevamo visto, convergono ad un valore, tutti questi valori vengono chiamate orbita.

Abbiamo anche delle funzioni cicliche.

#### Verificare i punti fissi: analisi grafica

Facciamo coincidere l'immagine con il dominio:
![xy](xy.png)

Abbiamo una coincidenza con una copia del dominio

Preso un punto iniziale sulle ascisse prendiamo un valore corrispondente sull'asse delle coordinate, se voglio che f(a) riappartenga al dominio della funzione lo devo riportare sull'asse delle ascisse:
![el](el.png)

Generiamo quindi una sorta di direzione di segmenti, come se ogni volta mi spostassi su quest'asse ed esso fosse il punto di partenza per spostarsi sull'asse:
![sp](sp.png)

Vedendo l'esempio della radice:
![rad](rad.png)

Vediamo che 1 è un punto fisso.

Vi può essere l'indagine grafica:
![cos](cos.png)

Il coseno ha un punto di attrazione 0.73908

Mentre per le funzioni dispari vediamo che abbiamo 2 punti di attrazione.

I punti della x^2 vengono detti punti repulsori...

Vediamo che abbiamo funzioni non lineari:
![nli](nli.png)

![rep](rep.png)

 In base al punto di partenza, cambiano molto le condizioni, il sistema è caotico quando cambia di molto il comportamento con punti iniziali diversi.

 Tutti i valori che uso avremmo convergeranno verso:
 ![punti](punti.png)

 ![it](it.png)

Prendendo l'unico punto repulsore avremo invece.

#### Equazione logistica

Essa è un modello rudimentale che descrive l'evoluzione di una popolazione in un ambiente chiuse, abbiamo una popolazione:
![eql](eql.png)

Questa equazione è del secondo ordine:
![pn](pn.png)

Se abbiamo 500 individui vivi e 1000 individui possibili, avremo 0.5 come possibili nuovi individi.

Questa equazione prevede la popolazione della popolazione successiva in base a quella attuale.

c è una costante importante che dipende dalle condizioni dell'ambiente in cui la popolazione deve vivere.

L'equazione fa parte delle equazioni quadratiche.

Nonostante il modello sia rudimentale esso è descrittivo per le colture dei batteri.

Abbiamo una forte dipendenza dalle condizioni iniziali, partendo da una popolazione con il 50% di popolazione come punto iniziale:

Avendo un coefficiente di 2.7, abbiamo ad ogni generazione successiva il 50% di individui.

![2.7](2.7.png)

Avendo delle risorse basse, andiamo a finire ad un numero piccolo.

Con risorse molto grandi, abbiamo una popolazione che oscilla.

Il parametro _c_ determina cosa succede alle generazioni successive.

>_Possiamo applicare questi ragionamenti alla densità di eventi in un brano musicale._

#### Diagramma delle orbite

![do](do.png)

Teniamo fisso il valore che esce fuori dalle iterazioni e studio come cambia il parametro c.

Vediamo il grafico delle orbite dove fissato un valore di c vado a studiare la funzione...

Vediamo la biforcazione con un periodo di ciclo 2:
![bif](bif.png)

Questo diagramma viene detto diagramma delle orbite e lo moltiplico per un certo fattore, ottengo un grafico uguale a quello iniziale.

Riepilogando il diagramma delle orbite, vedremo un nuovo valore di c indipendentemente dal valore x di partenza.

[video sul diagramma delle orbite](https://www.youtube.com/watch?v=ovJcsL7vyrk&ab_channel=Veritasium)

Avevamo visto come tenendo un valore ci spostavamo sul grafico.

#### Iterazione nel piano complesso

![comp](comp.png)

[libreria Python per grafica](https://docs.python.org/3/library/turtle.html)

Sui numeri complessi dobbiamo ricordarci che essi possono essere rappresentati in forma cartesiana o in in forma polare.

Elevando al quadrato, il valore al quadrato fa realizzare una rotazione del vettore.

Fare la somma dei due vettori, vuol dire applicare la regola del parallelogramma.

![s](s.png)

È interessante notare che facendo il modulo avremo un valore piú piccolo del modulo di z e del modulo di v.

Spazi metrici...

Vediamo questa prima iterazione del con z^2, ovvero:
![mo](mo.png)

Tutti i punti z il cui modulo è minore di 1 convergono al punto 0 mentre gli altri punti divergono ad infinito.

Cosa succede quando |x+jy|=1 ?

Tutti questi punti saranno quelli che appartengono alla circonferenza di raggio 1.

L'insieme di questi punti è detto insieme di Julia, e che descrive che tutti i punti appartengono alla circonferenza goniometrica ma nel piano complesso.

Se mi spostassi dalla circonferenza unitaria, leggermente piú in avanti, genereremo valori che vanno all'infinito.

Se sono all'interno del cerchio vado a 0, se sono sulla circonferenza mi sposto su di essa, all'esterno tutti i punti fuggono all'infinito.

Possiamo studiare i punti dell'insieme di Julia con le differenti velocità di fuga colorati in base alle loro velocità:
![jul](jul.png)

I punti che rimangono nel contorno dell'insieme di Julia: ![fu](fu.png)

La c è fissa e la z variabile

#### Insieme di Mandelbrot

Ora invece analizziamo l'orbita z fissa e la c variabile.

![ma](ma.png)

#### Proprietà dei set e dei frattali

![fra](fra.png)

Sono:
- auto-similari
- hanno dimensione frazionaria

#### Triangolo di Sierpinski

![sie](sie.png)

Abbiamo una generazione infinita di rimozioni di triangolo da l/2 del triangolo precedente.

Il triangolo di Sierpinski ha un perimetro ma area 0, ed abbiamo quindi un paradosso, poichè l'area di un triangolo non può essere nulla.

##### È una figura autosimilare?

![as](as.png)

Si

##### Dimensione?

![di](di.png)

il triangolo di Sierpinski che dimensione ha?

###### Dimensione frazionaria
![ddd](ddd.png)

Per capire la dimensione sappiamo che:
- un segmento è una figura auto-similare -> prendendo una parte di un segmento, moltiplicandola per n ottengo il segmento iniziale.

![dime](dime.png)

Il segmento prima per poter ritornare al segmento originario era n.

>La dimensione è il logaritmo del numero dei pessi, diviso il logaritmo dell'ingrandimento.

Per il triangolo di Sierpinski avremo una dimensione:
![dimes](dimes.png)

Non avremo quindi una dimensione come un numero intero, ma siamo a metà strada tra una figura a 2 dimensione ed una ad una dimensione.

La dimensione non è un numero intero ma è un numero frazionario(da ciò frattali).

##### Il fiocco di neve

Esso è un altro frattale in cui rimuoviamo delle parti:
![f](f.png)

Ora invece di rimuovere l'area, rimuoviamo una parte di perimetro.

Vediamo che questo fiocco di neve ha un'area finita ma perimetro infinito, perchè posso frazionare quanto voglio.

>Quanto è lungo il perimetro di una costa?

Ciò dipende dalla scala utilizzata...

Abbiamo nuovamente una dimensione frazionaria.

Avremo quindi una figura che si avvicina ad una dimensione 1.

### Caos e memoria associativa

Memorie associative e circuiti di retro azione non lineare.

Visione del video:
[Caos, ordine e memoria associativa](https://www.youtube.com/watch?v=_b4wltMg7As&ab_channel=studiodelbianco)

___

Caos stocastico
