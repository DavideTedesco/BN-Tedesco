# Appunti della lezione di mercoledì 12 Maggio 2021

Iniziamo facendo un piccolo ripasso sul modulo `gen~` di Max.

Le spiegheremo in CSound e poi faremo il porting Max.

## Ripasso su `gen~`

`gen~` ci permette di simulare codice al livello di linguaggio macchina.

Dovremmo aspettarci dei modelli dove l'aspetto dell'eccitazione ritorna su se stesso, e l'energia iniziale viene sostenuta da un sistema che inizia a vibrare per mezzo di questa energia.

### Differenza dal modello matematico

La sintesi additiva, l'FM, la waveshaping lavorano a livello matematico. Nei modelli fisici vado a generare gli oggetti per realizzazare un modello fisico.

L'eccitazione è in genere per il modello fisico realizzata per mezzo di un impulso o di un treno di impulsi:

![in](in.png)

### `gen~`

Il codice realizzato con `gen~` può essere esportato in C++:

![ge](ge.png)

Il minimo feedback, in Max è su 64 campioni, con `gen~` possiamo ritardare anche 1 campione.

### Codebox

Abbiamo un form per scrivere in un linguaggio simile a Javascript:
![cb](cb.png)

Questo linguaggio è denominato genexpr.

### Caricamento di dati in `gen~`
![sa](sa.png)

Per caricare il campione usiamo l'oggetto sample.

Per riempire il buffer, possiamo individuare la casella, l'indice che scorre e il valore

![i](i.png)

### Parametri e inizializzazione
Dovremmo ricordarci che i parametri dovranno essere scritti prima con la parola chiave `Param`.

![pa](pa.png)

### Priorità nell'output

Soltanto l'ultima variabile puó dare valore multipli, esempio del cartopol

### Funzioni in genExpr

Le funzioni sono uguali agli altri linguaggi di programmazione:

![f](f.png)

È tutto in audio rate!

### Da oggetti alle righe di codice

![cod](cod.png)

### Cicli decisionali

Posso utilizzare dei cicli decisionali per cambiare la forma d'onda:
![de](de.png)

Posso in quest'esempio scegliere fra piú forme d'onda.

![de1](de1.png)

Si possono inventare forme d'onda particolari, sentendo sempre la frequenza fondamentale, ma piuttosto che tenere dei parametri bloccati, posso dare dei parametri che possono cambiare ad esempio in una situazione di Live Electronics.

![de2](de2.png)

### Cicli iterativi

![ci](ci.png)

Usiamo le parole chiave `for` e `while`.

Vediamo un esempio di accumulatore.

Cicli iterativi con variabile:
![ci1](ci1.png)

Si possono realizzare anche cicli annidati come tutti i linguaggi di programmazione.

#### Interruzione dei cicli

Per mezzo delle keyword `break` e `continue`:
![int](int.png)

Con `break` si ferma il ciclo, mentre con il `continue` si usa per ad esempio realizzare una funzione discontinua.

### Derivatore in `gen~`
![der](der.png)

Prendiamo il valore attuale e il valore precedente, in un certo tempo, vedendo se vi sia stata una variazione o cambiamento per la posizione.

![de1](de1.png)

Posso considerare il campione precedente con il modulo `history` che mi restituisce il campione precedente.

![deri](deri.png)

Il tempo per default viene indicato come unitario con: "valore attuale che ho meno il valore precedente"

>La derivata di una funzione, è la variazione di una funzione rispetto ad una variabile.

![der1](der1.png)

>La variazione di una funzione costante è nulla, la derivata sarà nulla.

>La variazione di una funzione lineare è sempre analoga ad x.

È importante la variazione perchè la velocità è la variazione nel tempo dello spostamento...

![v](v.png)

#### Esempio di derivata

Abbiamo il rapporto incrementale, per poter passare a t che tende a t_0:
![ri](ri.png)

Avremo per mezzo del limite la derivata nel tempo della funzione.

Vi sono diversi modi per esprimere le derivate:

- x con il punto -> derivata di una grandezza rispetto al tempo
- derivata di una grandezza rispetto allo spazio, come ad esempio una variazione della temperatura in punti diversi di una stanza
  - potremmo studiare la grandezza temperatura in funzione di due variabili:
  1. temperatura
  2. spazio
- Se tengo una delle due grandezze costanti, realizzo uno studio alle derivate parziali, studiando le variazioni lungo le 3 direzioni, la derivata parziale si indica con una d diversa:![par](par.png)

>Per studiare una funzione di 4 variabili, possiamo tenere delle grandezze fisse e studiare rispetto al tempo cosa succeda nel tempo.

##### Derivata seconda e funzioni infinitamente derivabili

Se derivo rispetto a una derivata ottengo una derivata seconda.

![deriva](deriva.png)

Tra le funzioni infinitamente derivabili abbiamo ad esempio la sinusoide, che dopo 4 derivate il sendo torna a se stesso.

Le funzioni infinitamente derivabili fanno parte della classe C infinito.

>È importante studiare una variazione...

>Un'equazione differenziale, vi è una funzione legata a una variabile e le variazioni nel tempo.

#### Dimostrazione derivata

![dim](dim.png)

Ogni volta che ho una forma polinomiale, avrò funzioni che dopo un certo numero di derivate diverranno nulle.

Non dimenticarsi mai:
![dim1](dim1.png)

>Le funzioni C infinito, continuando a derivare hanno sempre la stessa formula.

Per dimostrare le derivate di seno e coseno conviene passare nella notazione di eulero.

### Derivate in Max

![mad](mad.png)

Osserviamo come si possa fare il derivatore aggiustandolo con una moltiplicazione avremo ciò che vi è in figura.

### Cosa fa una derivata?
Ci segnala solo dove vi è una variazione, per un'onda quadra avremo segnati solo i punti in cui vi è una variazione, ovvero in un istante, possiamo usare la derivata di un onda quadra come impulso.
![qua](qua.png)

se abbiamo un dente di sega potremmo realizzare qualcos'altro...

#### Esempi

![es](es.png)

>Mettere nei modelli fisici volumi bassi.

![es_](es_.png)

Vediamo che in codebox è più semplice realizzare modelli fisici.

## Lo spazio delle fasi

![sf](sf.png)

Rappresentazione dei moti periodici nello spazio delle fasi, con:
- x è la posizione della particella
- x_con_punto è la velocità relativa della particella

Spostando e tirando una molla, potremmo spostarla verso l'alto o il basso e spostandosi la massa potrà aumentare la velocità, e la massa potrà ritenere la massa, la velocità si fermerà e si inverterà il moto.

Un moto periodico sinusoidale, lo possiamo cosí rappresentare, osservando che il moto diviene a spirale e il punto su cui si ferma è il punto attrattore:
![spi](spi.png)

Il punto attrattore in un oscillatore armonico è l'origine:
![spi1](spi1.png)

Un sistema potrebbe osccillare tra due attrattori diversi.

>Le figure di Lissajous, mi fanno intendere che relazione abbiamo due segnali ad una certa frequenza:
![lissajous-picture.png](lissajous-picture.png)

Ogni sistema fisico ha alemno un attrattatore che è il punto di attrazione del sistema.

Avendo più attrattori, avrò differenze nel tempo, spiegandoci perchè due sistemi oscillino tra due stati differenti.

>Si può traslare l'idea dell'attrattore alle toniche percettive nella tonalità.

Gli iperpunti sono punti a n dimensioni, avendo un complesso punto di eventi percettivi, possiamo legarlo ad un iperpunto percettivo, facendolo divenire un attrattivo percettivo.

### Attrattore di Lorenz

Attrattori strani -> li vedremo in seguito

## Modelli fisici

### Modello massa-molla

![mm](mm.png)

Abbiamo i valori dell'elongazione o deflessione.

![mmm](mmm.png)

Essa è la rappresentazione ideale nel vuoto, e posso fare la differenza tra a_0 ed a_1.

Abbiamo quindi un grafico e potrò dire che la velocità sarà la differenza delle posizioni nel tempo.

Il valore del terzo stato sarà dato dal termine a0, inserendo il termine di a1...

Quindi a2=a1+(a1-a0)

![mmmm](mmmm.png)

Il movimento della massa, dato che ha un fattore di ritenzione, mano mano che vado avanti sento che la molla ha un fattore di ritenzione verso il basso.

Mano mano che aumenta l'ampiezza si sentre il peso di c che è un fattore moltiplicativo.

### Massa-molla in CSound

Vediamo in CSound la realizzazione del sistema:
![cs](cs.png)

In `k2` mettiamo la formula vista precedentemente aggiornando i parametri.

![cs1](cs1.png)

k0, k1 e k2 assumono valori negativi oltre che positivi.

![cs2](cs2.png)

#### Applicazione della formula a CSound

![app](app.png)

Vediamo un'applicazione al sistema massa-molla.

#### Applicazione con c randomizzato

Il c è legata alla costante di elasticità della molla, ed alla massa.

![app1](app1.png)

#### Applicazione con forza d'attrito

Conversione dal coefficiente c alla frequenza.

![con](con.png)

Per mezzo delle equazioni alle differenze leghiamo una variabile alla variazione nel tempo.

Il metodo di Eulero permette di passare...

### Integrazione della traiettoria

Se conosciamo la posizione e la velocità in t-1 potremmo ottenere quella in t:
![inte](inte.png)

#### Esempio di traiettoria balistica

![ba](ba.png)

#### Esempio di accellerazione su una molla con Eulero

Rispetto a prima ricavavamo la posizione, ed ora usando l'approccio di Eulero diciamo che l'accellerazione è legata alla velocità, mentre la velocità è legata a velocità e accellerzione.

![eq](eq.png)

c è come se fosse la costante di elasticità della molla.

Avendo misure eterogenee, in fisica ciò può essere scomodo, ma possiamo ridurre allo stesso gruppo se esse hanno tempo unitario.

![eqd](eqd.png)

Questa approssimazione di Eulero fino a 4000/5000 Hz funziona bene.

Metodo di Eulero migliorato o metodo di Runge-Kutta.

>L'idea è tenere il kc non più grande di 1, perchè è come se ogni volta si mettesse una velocità che va ad espldere.

Questa equazione va bene quando il c è più piccolo di 1.

### Cosa succede se smorzo il sistema?

Lo smorzamento è proporzionale alla velocità:
![smo](smo.png)

Ora moltiplico _v_, per un coefficiente _d_, la _v_ sarà pesata dal termine 1-d.

Non avremmo un inviluppo ma un attrito prodotto dal sistema:
![smo1](smo1.png)

Abbiamo una versione più semplice del Karplus-Strong, applicando posizione e velocità.

Ci si puó inventare qualcosa con uno smorzamento che cambia nel tempo, facendo valutazioni con modelli fisici degli strumenti impossibili da cambiare in natura per la d.

### Sistema fisico posta ad una forza di eccitazione

Posso prendere una quantità di energia e sommare una quantità random...

![ecc](ecc.png)

Posso dare ogni volta un contributo energetico.

Potremmo eccitare un sistema in continuazione, ed i dati diventeranno sempre maggiori fino ad esplodere.

>Attenzione agli spike...

#### Forza di eccitazione

Posso porla nello score, facendo uno strumento in CSound con un UDO:
![ecc1](ecc1.png)

Faccio in modo che il kr sia nel lin_reson=44100...

In questa formula teniamo conto della frequenza e dello smorzamento.

Questo modello è piú comodo per usare il mio opcode...

***
Parleremo del caos deterministico e degli attrattori la prossima settimana.

***

Riuscire ad implementare su codebox ciò che abbiamo realizzato a lezione...

- Modelli fisici
- DSP
- caos deterministico e caos statistico(modelli stocastici)

***
All'esame tesi su uno degli argomenti che affrontiamo.
