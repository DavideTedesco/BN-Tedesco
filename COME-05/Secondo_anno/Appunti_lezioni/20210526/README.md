# Appunti della lezione di mercoledì 26 maggio 2021

Ci sbizzarriamo a cambiare il modello fisico...

Il reson~ è un IIR del secondo ordine

>Il sistema caotico come indice di modulazione per la realizzazione di sistemi reali...
***

## Realizzazione di un oscillatore armonico: varianti

Avevamo scritto attraverso il metodo di eulero lo smorzamento dell'oscillatore armonico:
![a](a.png)

Avevamo moltiplicato la velocità per un fattore di smorzamento, dall'inglese "damping" per avere ogni volta una velocità diversa, e la posizione _x_ dato che la velocità è lo spazio sul tempo, la nuova posizione, è la posizione della molla piú la velocità.

Abbiamo una simulazione ottima e non abbiamo bisogno di inviluppo:
![c](c.png)

In base al valore _kc_ capiamo quale sia la frequenza di risonanza, ed attraverso la formula:
![aa](aa.png)

Possiamo ad esempio mettere 100 oscillatori con frequenze random a 2000 Hz che si smorzano per mezzo del coefficiente d.

Si è realizzato un opcode in CSound per realizzare un risonatore lineare:
![ris](ris.png)

Alla velocità sommiamo una forza in ingresso...

`dust2` è per la realizzazione di impulsi...

Abbiamo praticamente visto l'esempio con lo smorzamento precedente...

### Crescita non controllata del sistema

![co](co.png)

Vediamo che viene sommata una quantità casuale, il sistema esploderà.

>Come se su un pendolo aggiungo in modo casuale una velocità di oscillazione che fa perdere

![fe](fe.png)

**Tenere basso il volume sui modelli fisici...**

### UDO
![udo](udo.png)

![udo2](udo2.png)

>Avevamo visto che anche un sistema di massa-molla può farci implementare un tratto vocale.

Questo modello è ancora a feedforward, per il feedback dovremmo mandare un po' dell'uscita in ingresso.

### Accellerazione non lineare

Vediamo che in un pendolo l'accellerazione sarà massima quando cambia la direzione del pendolo:
![anl](anl.png)

>L'accellerazione di un oscillatore armonico dipenderà dalla velocità e dalla massa della molla.

Se facciamo passare l'accellerazione attraverso una tabella di distorsione...

![anl1](anl1.png)

Vediamo che abbiamo una componente continua

![f0](f0.png)

#### Corrente continua in elettronica

![cc](cc.png)

Vediamo che mettendo un filtro RC, possiamo sottrarre la componente in continua.

Di solito si mette un filtro a 1 Hz per togliere la componente in continua.

### Accellerazione e risonatore non lineare
![tab](tab.png)

Abbiamo una tabella con origine il centro, che all'interno ha dei valori lineari o non lineari.

![t](t.png)

Realizziamo la tabella lineare a tratti, ogni volta che si passa intorno allo 0 cambia il coefficiente k/m.

![tt](tt.png)

Abbiamo ora un'accellerazione non lineare:
![nl](nl.png)

>Ascoltare il prodotto di CSound.

[dcblock2 di CSound](http://www.csounds.com/manual/html/dcblock2.html)

***

Studieremo la variazione dei parametri con derivate...

## Derivate

Variazione nel tempo di un qualche parametro

![de](de.png)

Usavamo l'opcode `init` in CSound, mentre in gen~ usiamo `history` per avere il campione precedente, vediamo un esempio di derivazione:
![de1](de1.png)

Vediamo un la derivata in uscita:
![de2](de2.png)

![deri](deri.png)

Vi sono dei sistemi in cui le equazioni differenziali descrivo un moto.

Nello spazio delle fasi abbiamo le velocità(variazioni di x nel tempo)

Abbiamo in questo esempio un attrattore...

l'1 è ad esempio un attrattore della funzione radice.

Ho dei sistemi in cui potrò avere alternativamente piú attrattori

![att](att.png)

>Quando descrivo un sistema lo descrivo con le sue equazioni in cui metto la relazione tra grandezza derivata e non derivata, nello spazio delle fasi.

Potrei avere degli spazi generici in cui leghiamo una grandezza rispetto all'altra.

### Attrattori di Lorenz

Vi sono dei sistemi in fisica con piú punti delle attrattori, allora la traiettoria del sistema andrà su piú punti del sistema.

Vediamo che in questo caso abbiamo 2 attrattori ed ogni volta il punto materiale non passa mai sulla stessa orbita, ed ogni volta è come se vi fosse una sorta di zoom che riporta su un'orbita diversa.

L'attrattore di Lorenz è un attrattore strano che descriveva un fluido come si muoveva in base ad esempio al crescere della temperatura.

![lo](lo.png)

Abbiamo in realtà nel mondo reale applicazioni in 3 dimensioni quindi su 3 piani.

**Nello spazio delle fasi si ottiene un'orbita che non ripercorre un orbita dello stesso punto.**

![lo1](lo1.png)

#### Attrattore di Lorenz in gen~

Ne vediamo la realizzazione in gen~ per mezzo del codebox.

![lo2](lo2.png)

Osserviamo la realizzazione di questo caos deterministico.

![lo3](lo3.png)

[attrattore di Lorenz in Processing](https://www.youtube.com/watch?v=f0lkz2gSsIk)

#### Attrattore di Lorenz in CSound

Vediamo l'attrattore di Lorenz in CSound

![lo4](lo4.png)

Dobbiamo realizzare la figura...

![lis](lis.png)

### Attrattore di Rossler

![ro](ro.png)

### Sistemi deterministici

![dete](dete.png)

Molti sistemi caotici possono essere associati al controllo di parametri di modulazione e di pan, come ad esempio il controllo di un FM:
![fm](fm.png)

***

Implementare l'attrattore di Lorenz e Rossler

- attrattori strani(Lorenz e Rossler)
- attrattori classici(sistema terra-sole)
