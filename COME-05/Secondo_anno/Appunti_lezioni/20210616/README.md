# Appunti della lezione di mercoledì 16 giugno 2021

Abbiamo precedentemente visto il modello a guide d'onda:

![ri](ri.png)

>Una guida d'onda è sostanzialmente un modo per poter rappresentare la propagazione dell'onda di pressione o onda meccanica che si instaura in uno strumento.

La guida d'onda è legata all'aspetto della risonanza dell'oggetto e simula la particella di massa o aria.

Il comportamento della guida d'onda, è come un ritardo realizzato con delle linee di ritardo.

Vedremo oggi il comportamento di un flauto.

Usiamo le guide d'onda per non usare le equazioni differenziali con equazioni differenziali di D'Alambert, in piú dimensioni difficili da risolvere e calcolare.

Possiamo quindi vedere come è realizzata una guida d'onda elementare per mezzo di un modello:
![mo](mo.png)

Da esso è stato sviluppato anche il modello di Karplus-Strong con un feedback che si puó attenuare per mezzo di un moltiplicatore...

>Quando la perturbazione raggiunge il vincolo, una parte di energia viene persa, mentre un'altra parte di energia si pone in ricircolo nella corda. In genere le frequenze piú acute vengono assorbite piú velocemente delle frequenze piú basse; per questo motivo viene utilizzato un filtro passa basso.

(i servomeccanismi sono meccanismi con una sola amplificazione senza delay e filtro)

Avevamo poi visto che avendo un certo tipo di input con una componente in continua avremo un offset dannoso, con una tensione in continua in costante salita con una saturazione del materiale:
![go](go.png)

Introduciamo quindi alla fine del circuito un filtro che blocca una continua...

Possiamo quindi fare la derivata, poichè derivando il segnale togliamo la componente costante.

![cr](cr.png)

Avevamo poi visto un listato per realizzare un modello per mezzo di una guida d'onda del basso.

![dcb](dcb.png)

Realizzare una sola operazione matematica è utile per ottimizzare senza usare uno specifico opcode.

## Disadattamento di impedenze e giunzioni

L'impedenza è un'impedimento o una resistenza che presenta un sistema al passaggio di energia.

Ho un mezzo in cui passa una forma di energia e posso misurare l'impedenza in Ohm (acustici, meccanici ed elettrici); essi sono legati all'impedimento per la trasmissione di un'energia in un certo mezzo.

>Z, ovvero l'impedenza è in funzione della frequenza.

![z](z.png)

Lo stesso sistema offre resistenze ed impedimenti diversi al variare della frequenza dell'energia.

Un filtro passa basso ha una grossa resistenza alle frequenze gravi e un'alta resistenza alle frequenze elevate.

>L'impedenza acustica ha un corrispondente con l'impedenza elettrica...

Vediamo quindi la correlazioni con corrente e tensioni.

Se la corrente è costante, dove avrò un'impedenza costante avrò una tensione costante.

A parità di corrente che scorre abbiamo comportamenti diversi al variare dell'impedenza.

Se abbiamo invece una tensione costante, in base all'impedenza la corrente sarà maggiore o minore.

### Analogia con le equazioni di Maxwell(impedenza e circuiti)

Nasce da qui l'analogia di Maxwell...

![an](an.png)

Ovvero che un sistema massa molla può vedersi come un circuito RLC.

Vediamo che la frequenza di tale circuito è calcolabile come:
![eq](eq.png)

Ricordiamo che la frequenza di risonanza di un circuito RC si calcola quando la parte resistiva eguaglia la parte capacitiva.

Un condensatore sfasa la tensione in ritardo e si stabilisce poi la tensione.

Se la variazione di tensione è molto veloce ed il condensatore non riesce a caricarsi... Se invece la tensione è lenta allora il condensatore si caricherà del tutto.

Arrivando +3 volt ad esempio su un condensatore sommandole con 3 volts opposti dal generatore, avremo che scorrerrano 0 volts nel resistore.

Mettendo una frequenza lenta(bassa) facendo caricare il condensatore...

Se invece faccio passare un segnale a frequenza elevata il condensatore non si caricherà facendo passare tutte le frequenze elevate invariate.

In questo esempio di circuito elettrico abbiamo:
- generatore
- resistore
- condensatore

![ci](ci.png)

>Un condensatore è come un silos che viene riempito via via, arrivati al condensatore completamente carico non potremmo piú caricarlo...s

Se le cariche fluiscono nel condensatore e vi sono 4 cariche positive sopra il condensatore, vi saranno 4 cariche negative sulla lamella sottostante.

Vi quindi il condensatore che si carica via via arrivando quindi al massimo alla carica del generatore ed avremo che:
![eq1](eq1.png)

Abbiamo che la costante di tempo, legata alla relazione tra capacità a resitenza calcolabile come tau:![tau](tau.png)

Abbiamo che la curva di caricamento del condensatore è legata alla costante di tempo, maggiore è Tau(tempo di carica), piú tempo ci metterà per caricare il condensatore:
![cond](cond.png)

La corrente invece all'inizio scorre tutta poichè non vi è un condensatore, mentre andiamoo avanti la corrente scenderà sempre di più, fino ad arrivare a 0 ovvero quando il condensatore è carico.

Abbiamo quindi un esponenziale decrescente.

Con la formula:![ee](ee.png)

Quando t=RC, abbiamo che viene e alla meno 1, ovvero v/R diviso e.

Si considera quindi carico il condensatore quanto t è maggiore di 3 volte RC.

Questò poichè quando il condensatore è carico abbiamo che:
![q](q.png)

>Vedendo quando v su si riduce di 1 su e, sto nel mio tau.

Il condensatore si considera carico quando il tempo abbiamo 3RC.

Tutto ciò implica che il circuito è sollecitato da una tensione che precedentemente era nulla e al momento di chiusura dell'interruttore e la tensione ai capi del circuito inizia ad essere vin.

La tensione ai capi del condensatore sarà prima nulla e ai capi del condensatore sarà:![cond1](cond1.png)

Dopo aver caricato il condensatore, prendiamo e mettiamo un filo al posto della batteria:
![c](c.png)

Mettendo un circuito chiuso è come se avessimo una batteria che si scarica sulla resistenza, non avendo la possibilità di avere una carica, il condensatore si scarica e va a zero.

Equilibrando le cariche positive e negative, le cariche si eliminano via via e avremo che all'inizio abbiamo una tensione di 9v e nel tempo tau la tensione scenderà ed andrà a 0.
![fi](fi.png)

Anche la corrente scenderà man mano...

Invertendo invece i poli del generatore avremo che se la variazione è molto veloce, il condensatore non riesce a seguire le variazioni.

Se ad esempio abbiamo una resistenza da 1000 Ohm e un condensatore da 1 microfarad, avremo almeno bisogno di 1ms per far caricare un condensatore:
![n](n.png)

Ma se abbiamo un condensatore che si carica più velocemente di 1ms allora il condensatore non riuscirà a caricarsi:![fff](fff.png)

Dato che in un filo non vi è differenza di potenziale e la variazione è troppo veloce non avremo il modo di caricare il condensatore.

>Se volessimo vedere la tensione elettrica nel tempo, sul condensatore la tensione arriverà in ritardo rispetto alla tensione d'ingresso, perchè il condensatore ha bisogno di un tot di tempo per caricarsi.

Il condensatore ritarda quindi il segnale di uscita rispetto alla tensione d'ingresso.

Il ritardo di fase del condensatore viene standardizzato a 90 gradi dal segnale d'ingresso.

Avremo quindi che il condensatore inizierà a reagire dopo 90 gradi della tensione di ingresso del condensatore.

Possiamo quindi rappresentare la fase del segnale di uscita rispetto al segnale di ingresso rispetto ad un grafico in cui metteremo da una parte il segnale di ingresso sull'asse x dei numeri reali, sull'altro asse i numeri immaginari.

Abbiamo quindi la rappresentazione di elementi che sfasano o non sfasano.

Se gli elementi non sfasano, come dei resistori allora non avremo sfasamento tra ingresso ed uscita.

Mentre se avremo dei componenti che sfasano ingresso ed uscita allora dovremmo segnalarli sul grafico delle fasi:
![gra](gra.png)

>Se la frequenza è molto lenta il condensatore riesce a caricarsi, altrimenti esso non riuscirà a caricarsi...

Se abbiamo una resistenza dipendente dalla frequenza(ad esempio per i condensatori) allora se la resistenza è bassa il condensatore non riesce a caricarsi, poichè 0xi=0

Quando le frequenze sono lente, si offre una resistenza e si crea una tensione; se la resistenza è bassa la tensione in uscita non vi è e non registro nulla.

>La frequenza dipendente dalla resistenza prende il nome di _Reattanza_ X.

X_c prende il nome di reattanza capacitiva che è inversamente proporzionale alla capacitanza:
![xc](xc.png)

>Scelta una certa frequenza avremo Xc che per ricordarmi che sfasa di 90 gradi lo metto:
![gra1](gra1.png)

Abbiamo quindi lo sfasamento di 90 gradi puri de condensatore e lo sfasamento nullo della resistenza.

Supponendo che c sia 1 microfarad e omega è 100Hz, allora dovremo calcolare z, ovvero fase e modulo dell'impedenza trovata:
![gra2](gra2.png)

Per calcolare l'impedenza dovremmo quindi calcolare modulo:
![m](m.png)

E fase:
![fa](fa.png)

>Possiamo sapere quanto sia la fase, avendo in base ad omega quanto puó sfasare il segnale di ingresso e di uscita.

Quando omega è molto grande avremo allora la mia impedenza tenderà ad essere pari alla resistenza:
![gra3](gra3.png)

Per omega molto piccola avremo invece:
![gra4](gra4.png)

Abbiamo quindi un vettore particolare, ovvero un fasore che ruota da -90 a +90 gradi.

>Vi è un valore particolare della frequenza, ovvero quando R=Xc

Ovvero in tale occasione avremo che l'impedenza è pari al modulo di Z e angolo o fase è di 45 gradi, questo valore si chiama pulsazione o f di taglio:
![ta](ta.png)

La frequenza di taglio è quel valore reale che uguaglia la reattanza capacitiva.

Se volessimo sapere la frequenza di taglio dovremmo realizzare la formula inversa, trovando che la frequenza di taglio sarà:
![tag](tag.png)

Se volessimo quindi sapere la frequenza di taglio di un circuito simile a questo:
![1](1.png)

Avremo a frequenza di taglio il segnale attenuato di modulo:

E sfasato di un tot.

Se ho quindi una tensione di 10 volt ho un'attenuazione che porta 7 volt.

Realizzando il logaritmo in base 10 e moltiplicando poi per 20 avremo un'attenuazione del 30% a frequenza di taglio:
![at](at.png)

Che ci darà circa -3, ovvero l'attenuazione in dB.

### Analogia con la frequenza di risonanza e la frequenza di taglio

Una volta dato un impulso, il condensatore si caricherà e scaricherà...

Mentre il condensatore si carica al flusso di corrente elettrica, la bobina si oppone a tutte le variazioni ed ogni volta che vi è una tensione si cerca di bilanciare il nuovo stato per mantenere lo stato precedente.

La variazione di tensione sarà quindi legata anche all'induttore:
![va](va.png)

Variando quindi la corrente nel circutio, la bobina farà in modo di rimanere nello stato che è.

#### Differenza tra condensatore e induttore

Il condensatore accetta tutto fino al massimo della capacità...

La bobina invece all'inizio si oppone estremamente alle variazioni, e poi si adatta alle variazioni.

Il comportamento di bobina e condensatore sono uno antagonista all'altro.

#### RLC
(carichi induttivi e capacitivi)

Il circuito RLC oscilla continuamente caricando e scaricando il condensatore.

Ad una certa frequenza l'oscillazione sarà:
![om](om.png)

Se volessimo sapere l'impedenza generale dovremmo fare la differenza fra le reattanze del condensatore e dell'induttore che vediamo come differenti sul piano di Gauss...

![dif](dif.png)

La fase sarà analogamente calcolabile...

#### Risoluzione di circuti elettrici complessi

Risolvendo circuiti complessi di condensatori e bobine, si riesce a padroneggiare un circuito; ma come risolvere un qualcosa realizzato con molle e pesi?

### Circuiti e sistemi massa molla
![mm](mm.png)

In questo caso:
- L corrisponderà alla massa
- la resistenza elettrica legata al coefficiente di attrito
- inverso capacità elettrica legata all'elasticità
- la corrente è la velocità di vibrazione della massa
- la tensione elettrica è la forza eterna o pressione efficace

Parlando di impedenza, possiamo parlare di impedenza elettrica = tensione fratto la corrente.

L'impedenza meccanica è invece la forza diviso la velocità (u) dell'oggetto che sta vibrando.

L'impedenza acustica è la pressione dell'esecutore, diviso la portata(quantità di aria o gas che è possibile spostare in un secondo).

Esse sono:
![zz](zz.png)

### Impedenza acustica

È l'opposizione di un fluido al passare delle onde sonore.

![ia](ia.png)

La portata è il volume di fluido che viene spostato nell'unità di tempo...

Avremo quindi che la portata è esprimibile in funzione della superficie:
![s](s.png)

Avendo la stessa velocità del suono con 2 superfici di affaccio diverse, avremo un cambiamento di impedenza, con una superficie di scambio avremo un disadattamento di impedenza.

Ciò è come se avessimo 2 materiali differenti...

#### Esempio del flauto

Avendo due materiali differenti, avremo una sezione che si affaccia all'esterno con una superficie esterna molto piú grande dell'imboccatura del flauto.
![fla](fla.png)

Avendo due superfici diverse, avremo uno svuotamento, o sottopressione rispetto alla pressione di equilibrio, percorrendo il tubo nel modo opposto.

Avendo una sottopressione sul capo del tubo ritornata indietro ed avremo un tubo sonoro che fa oscillare un onda stazionaria, e grazie al meccanismo del disadattamento di impedenza si crea un'onda stazionaria.

>Una parte dell'energia è trasmessa all'esterno, mentre un'altra parte è riflessa.

Un adattatore di impedenza è ad esempio un cono, come gli ottoni, o le campane...

Nel flauto non avendo grandi trombe o strombature avremmo non si creeranno molte onde stazionarie.

![dis](dis.png)

>Nel registro centrale il flauto è sostanzialmente una sinusoide...

### impedenza meccanica

![im](im.png)

Prendendo un pendolo e facendolo oscillare a 1000Hz, mi basterà pochissima energia per farlo vibrare a 1000Hz poichè è la sua frequenza naturale, ma farlo vibrare ad un'altra frequenza sarà qualcosa di piú difficile.

### Impedenza di una corda

![cor](cor.png)

Vediamo che possiamo sostituire al posto di c, \sqrt(T/M)...

Posso sapere tutte le informazioni che mi servono per poter calcolare l'impedenza della corda.

Sapendo che la massa è pari al volume per la densità.

Avremo quindi la densità per unità di lunghezza, con il Volume che sarà area di base per l'altezza.

Avremo che una corda dell'esempio fa 113 metri al secondo.

Per saperne l'impedenza prendiamo la massa e moltiplicandola per la velocità di propagazione della corda.

L'impedenza risultante sarà quella in ohm.

Per adattare l'impedenza della corda all'aria usiamo un adattatore di impedenza per la corda, per mezzo della cassa armonica, per evitare di perdere energia.
Ed avendo un maggiore trasferimento di energia per mezzo di un adattatore di impedenza come i ponticelli(per le corde) o le trombe(per i tubi sonori).

Adattamento di impedenza con i fluidi è visibile con un tubo pieno d'acqua ed un restringimento:
![re](re.png)

Una part di energia torna indietro ed una parte di energia continua ad andare avanti:![ene](ene.png)

Avremo quindi che una parte trasferita ha una fase, mentre quella che torna indietro ha fase inversa.

L'interfaccia centralè è il ponticello o la tromba.

Si dirà R il coefficiente di riflessione fra 0 ed 1:![coer](coer.png)

Il coefficiente di riflessione è scritto in modo tale che non arriverà mai a 1.

Abbiamo qui il coefficiente di riflessione espresso per mezzo delle superfici.

Guardando i modelli fisici reali dovremmo osservare la nostra giunzione e osservare i disadattamenti di impedenza di ogni oggetto come un flauto.

I fori in generale realizzano un disadattamento di impedenza sempre, come ad esempio un flauto.

>Vedremo un modello di flauto con un solo foro che poi si potrà replicare in varie posizioni del tubo.

## Modello del flauto

Utilizziamo 2 linee di ritardo...

![flauto](flauto.png)

Entra l'aria e si forma l'onda progressiva e l'onda regressiva, abbiamo il disadattamento dell'impedenza e una somma dell'aria dall'esterno con fase esterna.

Abbiamo quindi vari disadattamenti di impedenze.

Abbiamo una serie di interferenze distruttive e costruttive che permettono di realizzare il suono...

Possiamo dire che il segnale che sta passando è una sorta di distorsione non lineare del terzo ordine...

Posso dire che il segnale è un qualcosa che sommato a del noise del soffio mi da il suono del flauto.

realizziamo quindi una waveshaping, il segnale distorto entra nel filtro passabasso che simula la dispersione all'esterno.

Una parte del segnale ritorna nella zona del foro...

Una parte del segnale è riflessa mentre un'altra parte ritorna all'imboccatura con il delay dell'imboccatura, abbiamo quindi un modello con 2 delay con un solo foro.

![flo](flo.png)

Potremmo ad esempio modificare il flusso con un vibrato con un flusso d'aria che viene modificato:

![floo](floo.png)

### Simulazione del flauto in CSound

Possiamo simulare il flauto in CSound con vari opcode:
![oc](oc.png)

![sim](sim.png)
_____

Il buffer circolare per il ritardo lo vedremo la prossima volta, insieme alla gestione dei feedback per delay read e write, prelevando il segnale da punti diversi del flauto.
