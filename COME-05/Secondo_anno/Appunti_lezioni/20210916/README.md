# Appunti della lezione di giovedì 16 settembre 2021

Oggi finiamo l'aspetto del caos parlando del caos statistico e poi parliamo di alcune tecniche e tematiche legate al DSP.

La scorsa volta abbiamo visto una serie di strategie per generare dei dati con dei motori deterministici, con equazioni ed una serie di equazioni.

Abbiamo visto:
- insieme di Julia
- insieme di Mandelbrot
- equazione logistica

Oggi vediamo un altro approccio, ovvero quello in cui non conosciamo la macchina che crea i dati.

## Caos stocastico

Il caos è definito quando non si riesce a trovare una relazione tra i dati che stanno pervenendo e quelli che devono essere analizzati.

![cs.png](cs.png)

Abbiamo l'insieme di tutte le possibili realizzazioni che viene chiamato _Sample space_.

Possiamo dare un peso alle ricorrenze di ognuno di questi eventi, e si parlerà di probabilità di certi eventi.

La probabilità va da 0 ad 1 e possiamo dare per ogni evento un certo indice.

Se ho piú eventi che si possono verificare nei quali un evento è complementare di un altro, la somma totale delle probabilità dovrà essere pari ad 1.

![ep.png](ep.png)

Potremmo avere anche degli eventi in cui si verifichino entrambi degli eventi, e non soli eventi disgiunti:
![ep2.png](ep2.png)

Quale è la probabilità che ognuno dei clienti abbia tutte e due le carte?

Attraverso la formula vedremo quale sarà la probabilità che il cliente abbia almeno una delle due carte.

Quale sarà la probabilità che dopo un lancio di un dado vi sarà al lancio successivo.

### Probabilità condizionata

Se abbiamo alcune informazioni riguardo l'esito di un esperimento disponibili:
![pc.png](pc.png)

Mi chiedo la probabilità che la somma sia 10, sapendo che il primo dado sia 4, andando a vedere, controllerò quale sarà la probabilità di avere un secondo numero che arrivi a 10.

>Il processo diviene stocastico quando viene articolato nel tempo.

![pc2.png](pc2.png)

### Eventi dipendenti e indipendenti

Se il lancio di una moneta, ad esempio, ed un altro evento non cambia, abbiamo due eventi indipendenti.

Altrimenti se abbiamo un legame fra questi lanci avremo delle probabilità dipendenti fra loro.

![ind.png](ind.png)

### Variabili aleatorie


![va.png](va.png)

### Densità di Probabilità
Una variabile x ha una sua distribuzione, che si chiamerà funzione di probabilità, ovvero un sottoinsieme dei possibili valori di x.

![dp.png](dp.png)

La distribuzione di probabilità se costante come quella in figura, significa che la distribuzione di probabilità sarà uniforme.

Se una distribuzione ha densità f(x)dx, allora avremo che la distribuzione sarà pari all'area sottesta dal segmento.

![20.png](20.png)

La distribuzione darà come gli eventi sono stati generati, se volessi studiare ad esempio tonica e dominante e la loro distribuzione di probabilità vedrò dei comportamenti e degli aspetti interessanti.

### Valor medio

![vm.png](vm.png)

Esso sarà la somma dei possibili valori dati dalle variabili ciascuno moltiplicato per il suo peso.

Il valor medio è ciò che succede quando analizzo una serie di dati e mediamente dei valori si disporranno in un certo modo...

Sapendo ad esempio il valore che uscirà con maggiore frequenza per il lancio di un dado, il valor medio sarà la somma dei valori.

Nel caso in cui la variabile assumi valori continui, al posto della somma dovró inserire un integrale con una funzione di probabilità vera e propria.

Pensando alla distribuzione uniforme vedremo che:
![du.png](du.png)

Vedremo quindi il valor medio, ovvero la speranza matematica del lancio del dado.

Facendo tanti lanci, sarà quasi certo che il numero maggiore di occorrenze si verifichi per il valore 3 ed il valore 4.

Vediamo che se la distribuzione è continua, l'area totale dovrà valere 1:
![duu.png](duu.png)

Avremo che in questo caso il valor medio sarà il valore 0.5

### La densità spettrale

Abbiamo visto che nell'ambito delle frequenze, lo spettro mi fa visualizzare quante volte vediamo un elemento ciclico:
![ds.png](ds.png)

Essa mi aiuterà a capire le periodicità che sono insite nel processo stocastico.

Le periodicità potranno quindi essere molto importanti, ciò lo potrò vedere osservando la densità spettrale.

>Essa è una trasformata di Fourier da applicarsi ad un segnale aleatorio.

Lo spettro mi mostrerà se vi sono periodicità nei dati che stiamo analizzando.

Se non vi sono periodicità, lo spettro mostrerà per ogni valore le frequenze di occorrenza.

### Esempi di distribuzione uniforme in CSound e Max

![edu.png](edu.png)

### Altri tipi di distribuzione

### Applicazione della distribuzione lineare

![a.png](a.png)

#### Distribuzione lineare

Vi possono essere ovviamente altri tipi di distribuzione, ma l'area totale dovrà sempre essere pari ad 1:
![lin.png](lin.png)

Vediamo che la densità di probabilità sarà data dall'equazione.

Vediamo che usiamo due distribuzione uniformi ed ad ogni distribuzione diamo un numero casuale.

Il valor medio sarà 0.33...


#### Distribuzione triangolare

![dt.png](dt.png)

Possiamo vederla in CSound:
![ddd.png](ddd.png)

#### Distribuzione esponenziale

![de.png](de.png)

Se lambda è grande aumenta la probabilità di avvenimenti mentre avvicinandosi all'asse delle x...

Studiando musica tonale, osserviamo dei fenomeni come la funzione di Levy, o funzione della pulce.(soluzione efficace per poter distribuire valori ed intervalli musicali).

Potremmo fare in modo di scrivere battute con 5 eventi al secondo, poi con 11 eventi al secondo e via via...

Ora analizziamo le funzioni ma nulla ci vieta di interrogare ed implementare queste funzioni che stiamo vedendo.

![ma.png](ma.png)

#### Distribuzione di Poisson

Essa descrive lo studio dei valori per cui la variabile x assume dei valori interi o 0:

![po.png](po.png)

Essa non ha un limite superiore ed è controllata dal parametro lambda.

Ci chiediamo quale sia la probabilità che si verifichi un certo evento dato il valore di lambda.

![po2.png](po2.png)

Poisson è importante perchè lega il tempo alla probabilità, in 20 minuti avró la probabilità che un certo evento accada nel tempo.

Il valor medio della distribuzione sarà proprio lambda.

##### Elenco di applicazioni della distribuzione di Poisson

![el.png](el.png)

##### Distribuzione degli eventi rari

![rari.png](rari.png)

##### Poisson in CSound

![css.png](css.png)

#### Distribuzione gaussiana

Ogni fenomeno naturale dovrebbe attestarsi su una distribuzione uniforme, ma sommando un congruo numero di distribuzioni uniformi ci uscirà fuori una gaussiana.

![ga.png](ga.png)

La maggior parte dei valori si concentrerà sulla zona centrale della curva gaussiana.

Si puó approssimare una gaussiana con 12 distribuzioni uniformi:
![gaa.png](gaa.png)

Vediamo una distribuzione normalizzata al valore 0.

Essa è simile a quella esponenziale solamente che il grafico riporta le distribuzioni non nel tempo...

### Mapping

![mapp.png](mapp.png)

Esso sarebbe un riscalamento verso un range di valori diverso dai valori originali.

Il riscalamento puó essere attuato non solo linearmente, ma anche logaritmicamente o esponenzialmente, realizzando dei mapping non lineari.

### Autocorrelazione

Essa segnale se un segnale contiene dei pattern che si ripetono:
![ac.png](ac.png)

Prendendo ad esempio il valore di x nel punto t e x di t+tau, avremo un valore maggiore di zero.

Nel secondo esempio avremo un valore prossimo allo zero.

Se ho un valore prossimo allo zero, vi è una correlazione, ovvero i valori si somigliano, mentre se non si somigliano avremo valori maggiori di zero.

Se il segnale varia in modo monotono, l'indice di correlazione sarà >0.

L'indice è chiamato indice di correlazione.

![ic.png](ic.png)

Facendo la trasformata inversa dello spettro di probabilità, vedremo la densità di autocorrelazione.

![auto.png](auto.png)

Quando ascoltiamo delle forme d'arte nel tempo, confrontiamo le sensazioni attuali con quelle precedenti per poterne fare dei confronti con tutta una serie di informazioni.

#### Processi di memoria

![pm.png](pm.png)

In base all'indice del processo di memoria abbiamo quindi delle correlazioni differenti, l'indice di correlazione realizza legami con dei processi che si sviluppano nel tempo, ed avremo dei processi con memoria di quello che è avvento precedentemente.

>Articolo di Santoboni sui rumori frazionari


![pmm.png](pmm.png)



#### Processi markoviani

![mark.png](mark.png)

Una distribuzione uniforme viene rimappata sul sistema che stiamo utilizzando.

Esso è ciò che è implementato nel `drunk` di Max.

Abbiamo delle barriere diverse:
- barriera assorbente
- barriera riflettente

Il processo continua a reiterarsi nel sistema.

Se analizzo un processo frazionario vedo che avró la dipendenza dal solo stato precedente ovvero con una distribuzione 1/f^2.

![marko.png](marko.png)

Partiamo da uno stato e poi attraverso una matrice delle occorrenze, capiamo come possiamo passare da uno stato ad un altro, possiamo però segnare le probabilità per andare da un gradino ad un altro, avremo quindi una matrice di probabilità con la probabilità di passare ad uno stato successivo con caselle P_ij.

![mo.png](mo.png)

Avremo quindi una matrice:
![matr.png](matr.png)

La matrice che si ricorda il solo stato precedente viene detta del primo ordine

>Una matrice che rappresenta le possibilità compositive della dodecafonia, sarà del dodicesimo ordine.

Pensando a cosa succede su un segmento, al prossimo passo potremmo andare solamente in una delle due direzioni, ovvero in un intorno prestabilito che dipende sempre da noi.

![bro.png](bro.png)

Se ho un liquido che bolle, la particella andrà in un intorno rispetto al punto di partenza, ed il modo di distribuire la particella su uno spazio è realizzabile in 3 dimensioni con 3 matrici di Markov.

È molto interessante truccare una matrice per indirizzare un elemento verso una specifica direzione, cambiando anche la particella facendola tempovariante.

Un processo stocastico è un processo probabilistico legato al tempo.

>Cerchiamo di fare in modo che una matrice non abbiamo un moto browninano, ma qualcosa di piú complesso.

Possiamo anche analizzare tutte le occorrenze di una tipologia di repertorio, generando una matrice, che diviene una sorta di estratto degli andamenti delle altezze.

>Estraiamo delle caratteristiche di un pensiero compositivo visto nell'ambito probabilistico.

#### Implementazione del processo di Markov

![markov.png](markov.png)

![es.png](es.png)

Scegliamo R=0.7

0.2+0.3=0.5 vado avanti e allora arrivo al solo e faccio 0.2+0.3+0.1=0.6 -> allora sceglierò la quinta casella che mi realizza probabilità 1>0.7.

Andrò ora sul La, genero ora un numero casuale:
R=0.2 e confronto tutte le probabilità che ho, finchè non supero il valore di R.

##### Esempio in Python
Vediamo un'applicazione in Python con barriere riflettenti.

![py.png](py.png)

Particella in uno spazio tridimensionale.

![tra.png](tra.png)

##### Applicazione alla composizione

![app.png](app.png)

Possiamo attribuire dei pesi agli eventi complessi ed estendere la matrice di probabilità.

![matpro.png](matpro.png)

Possiamo osservare che la diagonale ha tutti zero tranne il Re.

La matrice appena vista è del primo ordine e ricorda lo stato precedente.

Maggiore è l'ordine di una matrice, maggiore sarà l'opportunità di avere correlazione fra dati in ingresso ed in uscita.

###### Esempi in CSound

![matpro1.png](matpro1.png)

#### Matrice di Markov del secondo ordine

![secon.png](secon.png)

Vediamo che per la realizzazione di una matrice di secondo ordine consideriamo la probabilità che si verifichi un evento dopo due eventi che sono avvenuti, ed il numero di eventi mi darà l'ordine della matrice.

![second.png](second.png)

#### Implementazione della catena di Markov in Max di primo ordine

![max.png](max.png)

##### Implementazione del secondo ordine

Con lo shift dei bit a sinistra di 7 bit.

#### Matrici di Markov tempo varianti

Posso cambiare le matrici via via che le uso per indirizzarle verso una direzione specifica che desidero:
![tv.png](tv.png)

___

Abbiamo concluso la sezione sul caos...

Per la prossima volta:
- convoluzione
- trasformata di Hilbert -> pitch-shifter
- time-stretching
- filtri FFT->filtraggio bin per bin
