# Appunti della lezione di mercoledì 23 giugno 2021

CharlieVerb di Nottoli -> protocollo di segreto industriale...

## Previously
Avevamo visto un modello fisico con massa molla e piú comprimevamo la molla, maggiore sarà la forza con cui il sistema tornerà indietro.

Abbiamo poi realizzato una serie di variazioni per sistemi massa molla e abbiamo visto tutto l'aspetto legato all'impedenza.

Avevamo visto cosa succedeva nei tubi risonanti...

Avevamo visto l'analogia di Maxwell per l'impedenza...

Pensare un circuito meccanico trasformato in un circuito elettrico è molto utile per pensare un circuito meccanico e trasportarlo nel mondo dell'elettronica.

Il discorso realizzato era sempre monodimensionale, mentre a breve passeremo ad un discorso bidimensionale.

![an](an.png)

Possiamo mettere al posto di L la massa e al posto di C la cedevolezza.

### Esempi di analogie meccaniche e circuitali

![me](me.png)

Vediamo le analogie fra circuiti elettrici e circuiti meccanici:
![e](e.png)

![ee](ee.png)

Avendo la stessa forza che agisce su due molle, se ho due molle, è come se avessi di 2 condensatori etc...

### Come poter codificare l'equazione che dall'accellerazione estrae lo spostamento

Calcolo differenziale per mezzo di accellerazione:

![a](a.png)

Possiamo in modo analogo scrivere lo spostamento nello spazio x...

Abbiamo generato dei sistemi massa molla con anche coefficiente di smorzamento.

Poi abbiamo visto un modello di oscillatore che cambia in modo vario...

### Equazioni differenziali e modelli di Gauss deterministico

Abbiamo visto applicazioni in Max e CSound di come poter generare valori tra loro decorrelati per mezzo di equazioni differenziali.

Abbiamo visto quindi gli attrattori come quello di Lorenz per descrivere cosa succedeva in un sistema caotico:
![ee](ee.png)

Possiamo usare ciò come insieme di dati per modificare una tecnica di sintesi.

## Modello del flauto

![fl](fl.png)

Quando dal tubo viene trovata un'impedenza diversa, vi è disadattamento di impedenza, quindi una parte della sovrapressione si espande, mentre la maggior parte della pressione torna indietro, trovato un altro foro abbiamo un altro disadattamento di impedenza.

Avremo quindi:
- onde regressive
- onde progressive

Troviamo quindi ad ogni foro un disadattamento di impedenza...

Abbiamo quindi tutti i disadattamenti di impedenza di tutti i fori e gli estremi del flauto.

Abbiamo quindi sezioni di feedforward e feedback.

Abbiamo quindi il modello del flauto con aggiunta di rumore per simulare il respiro.

La sezione di feedback sono 2 linee di ritardo:
1. prima linea di ritardo che modellizza l'imboccatura
2. seconda linea di ritardo modellizza il foro

Con la guida d'onda simulo dei ritardi del segnale...

Abbiamo un interazione fra eccitazione e foro per mezzo di una sorta di distorsione non lineare:
![int](int.png)

Abbiamo quindi il modello fisico e uniamo i vari pezzi del flauto per mezzo del +:
![m](m.png)

Le distorsione non lineare si puó introdurre in vari punti...

### I vari tipi di delay

![cs](cs.png)

Delay multitap, prendo un frammento in diversi punti della memoria avendo tanti istanti differenti di quel suono che avevo realizzato.

Abbiamo quindi vari opcode che possiamo usare.

#### `delayw` e `delayr`

Si scrive con `delayw` e si legge con `delayr`:

![d](d.png)

#### `deltap`

Abbiamo un delay che mi cambia ogni 0.1 secondi:
![pi](pi.png)

![pii](pii.png)

![piii](piii.png)

#### `deltapi` e flauto

Con il `deltapi` dentro il modello del flauto possiamo avere un tot di delay...

Abbiamo il modello del flauto:
![1](1.png)

Segnale che sta entrando con funzione di distorsione e `deltapi` siamo all'uscita dopo il filtro:
![2](2.png)

Abbiamo l'uscita dal delay e la componente in continua:
![3](3.png)

## Modello del clarinetto

![c](c.png)

Vediamo che nel modello del flauto abbiamo un feedback che rientra in ingresso e va ad agire sull'ancia.

Abbiamo a destra il tubo sonoro e a sinistra la sezione dell'ancia.

L'ancia distorce il segnale che sta arrivando e rispetto al livello del flauto dove non avevamo previsto distorsioni a livello di eccitazione, ora proponiamo una distorsione...

Sulla x vi è il segnale d'ingresso mentre sulla y vi è l'uscita.

Soffiando troppo forto l'ancia si attacca al barilotto e il clarinetto non suonerà piú.

Gli esponenti pari danno anche un coefficiente costante(la costante x^0) mentre quelli dispari no...

>Si potrebbe realizzare un modello fisico del clarinetto con tubo del flauto...

### Clarinetto in CSound

![cla](cla.png)

Un modello a sintesi non si comporta in modo diverso se cambia la pressione sonora, mentre un modello fisico cambiando la pressione sonora in un modello fisico cambia anche il timbro.

## Introduzione del modello a membrana

Abbiamo ora un modello a guida d'onda a piú dimensioni rispetto a prima che il modello a guida d'onda era ad un'unica dimensione.

Ora abbiamo n corde e piuttosto che simulare n delay, possiamo simulare invece avendo solamente alcuni ritardi e non n...

![del](del.png)

Abbiamo quindi delay a 2 linee con segnale che è combinazione a pochi delay.

![im](im.png)

Possiamo modellizzare la membrana come quadrata con solo 6 linee di ritardo...

Possiamo cambiare i delay per fare dimensioni differenti di membrane...

Abbiamo quindi una rappresentazione dei ritardi come linee di ritardo da un punto ad un altro e l'inverso.

![gom](gom.png)

Possiamo usare sistemi di risonanza diversi come anche per gli altri strumenti.

![gomma](gomma.png)

La cassa di risonanza è cosī realizzata:
![cos](cos.png)

Il dcblock possiamo realizzarlo in vari modi.

L'eccitazione per mezzo dello stick è cosī realizzata:
![stick](stick.png)

Abbiamo diverse dimensioni di membrane ed implementazioni.
