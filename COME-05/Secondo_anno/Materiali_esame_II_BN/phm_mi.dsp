declare name "Physical Model of a String made w/ mi.lib";
declare version "0.1";
declare author "Davide Tedesco";
declare license "CC3";
declare year "2022";

import("stdfaust.lib");

/*
specific force input signal operation here
*/
pluck = button("[06]Pluck")  : ba.impulsify * 0.1;

OutGain = hslider("[05]Volume", 0.4, 0, 1, 0.001) : si.smoo;

/*
graphical objects to control the parameters
*/
m = hslider("[01]m", 1.0, 1, 20, 0.001) : si.smoo;
/*
beware of the relationship between k and z,
if the sum is greater than 1, the phm stop working
*/
k = hslider("[02]k", 0.1, 0.007, 0.988, 0.001) : si.smoo;
z= hslider("[03]z", 0.007, 0, 0.009, 0.00000001) : si.smoo;
zo = hslider("[04]zo", 0.0, 0, 0.009, 0.000001) : si.smoo;

k_value = k : max(k-z);
z_value = z : max(z-k);

/*
beware of the relationship between m and k,
if k_value is higher than m and m is lower than k_value,
the phm stop working
*/

//mass value
str_M = m;
//stiffness
str_K = k_value;
//viscosity
str_Z = z_value;
//friction
str_Zo = zo;

model = (
	mi.ground(0),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.oscil(str_M, 0, str_Zo, 0, 0.00, 0.00),
	mi.ground(0),
	par(i, nbFrcIn,_):
	RoutingMassToLink ,
	par(i, nbFrcIn,_):
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	mi.springDamper(str_K, str_Z, 0.00, 0.00),
	par(i, nbOut+nbFrcIn, _):
	RoutingLinkToMass
)~par(i, nbMass, _):
par(i, nbMass, !), par(i, nbOut , _)
with{
	RoutingMassToLink(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20, m21, m22, m23, m24, m25, m26, m27, m28, m29, m30, m31) = /* routed positions */ m0, m1, m1, m2, m2, m3, m3, m4, m4, m5, m5, m6, m6, m7, m7, m8, m8, m9, m9, m10, m10, m11, m11, m12, m12, m13, m13, m14, m14, m15, m15, m16, m16, m17, m17, m18, m18, m19, m19, m20, m20, m21, m21, m22, m22, m23, m23, m24, m24, m25, m25, m26, m26, m27, m27, m28, m28, m29, m29, m30, m30, m31, /* outputs */ m28, m4;
	RoutingLinkToMass(l0_f1, l0_f2, l1_f1, l1_f2, l2_f1, l2_f2, l3_f1, l3_f2, l4_f1, l4_f2, l5_f1, l5_f2, l6_f1, l6_f2, l7_f1, l7_f2, l8_f1, l8_f2, l9_f1, l9_f2, l10_f1, l10_f2, l11_f1, l11_f2, l12_f1, l12_f2, l13_f1, l13_f2, l14_f1, l14_f2, l15_f1, l15_f2, l16_f1, l16_f2, l17_f1, l17_f2, l18_f1, l18_f2, l19_f1, l19_f2, l20_f1, l20_f2, l21_f1, l21_f2, l22_f1, l22_f2, l23_f1, l23_f2, l24_f1, l24_f2, l25_f1, l25_f2, l26_f1, l26_f2, l27_f1, l27_f2, l28_f1, l28_f2, l29_f1, l29_f2, l30_f1, l30_f2, p_out1, p_out2, f_in1) =
/* routed forces  */
l0_f1, l0_f2 + l1_f1, l1_f2 + l2_f1, l2_f2 + l3_f1, l3_f2 + l4_f1, l4_f2 + l5_f1, l5_f2 + l6_f1, l6_f2 + l7_f1, l7_f2 + l8_f1, l8_f2 + l9_f1, l9_f2 + l10_f1, f_in1 + l10_f2 + l11_f1, l11_f2 + l12_f1, l12_f2 + l13_f1, l13_f2 + l14_f1, l14_f2 + l15_f1, l15_f2 + l16_f1, l16_f2 + l17_f1, l17_f2 + l18_f1, l18_f2 + l19_f1, l19_f2 + l20_f1, l20_f2 + l21_f1, l21_f2 + l22_f1, l22_f2 + l23_f1, l23_f2 + l24_f1, l24_f2 + l25_f1, l25_f2 + l26_f1, l26_f2 + l27_f1, l27_f2 + l28_f1, l28_f2 + l29_f1, l29_f2 + l30_f1, l30_f2,/* pass-through */ p_out1, p_out2;
	nbMass = 32;
	nbFrcIn = 1;
	nbOut = 2;
};
process = vgroup("Physical Model of a String made w/ mi.lib ",
					(
					pluck :
					fi.dcblocker :
					model:*(OutGain), *(OutGain) :
					fi.dcblocker,fi.dcblocker <:
					attach(_, ba.linear2db : hbargraph("[99]L",-60,0)), attach(_, ba.linear2db : hbargraph("[100]R",-60,0))));
