<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

sr = 44100
kr = 32
nchnls = 1
0dbfs = 1.0


opcode linear_reson, a, akk ;name, outtypes, intypes usage at:http://csounds.com/manual/html/opcode.html
setksmps 1
avel init 0
ax init 0
ain, kf, kdamp xin
;kc is the frequency
kc = 2-sqrt(4-kdamp^2)*cos(kf*2$M_PI/sr)
aacel = -kc*ax
avel = avel+aacel+ain
;introduction of the damping coefficient
avel = avel*(1-kdamp)
ax = ax+avel
xout ax
endop

instr 1
aexc rand p4
aout linear_reson aexc, p5, p6
out aout
endin

</CsInstruments>
<CsScore>
;instr_number start_point end_point excitation frequency damping
i1 	      0 	  5         0.01       330	 0.01

</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
