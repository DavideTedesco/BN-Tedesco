<CsoundSynthesizer>
<CsOptions>
-odac -
</CsOptions>
<CsInstruments>

sr = 44100
kr = 44100
nchnls = 2
0dbfs = 1.0

#define PI #3.14159265358979323846264338327950288419716939937510#

instr 1

ax init 0
av init 0.1
kc = 2*(1-cos(2*3.1415926535897*p4/sr))

outs ax, ax
aacc = -kc*ax ;accelerazione
av = av+aacc
ax = ax+av

endin



</CsInstruments>
<CsScore>
i1 0 5 333

</CsScore>
</CsoundSynthesizer>