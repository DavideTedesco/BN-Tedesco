<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>
sr = 44100
kr = 44100
nchnls = 2
0dbfs = 1

instr MassaMolla
a0 init 0
a1 init 0.05
ic = 0.001

a2 = a1+(a1-a0)-ic*a1
outs a0, a0
a0 = a1
a1 = a2
endin

</CsInstruments>
<CsScore>
i "MassaMolla" 0 10
</CsScore>
</CsoundSynthesizer>
