<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>
sr = 44100
kr = 44100
nchnls = 2
0dbfs = 1

instr 1
ax init 0
av init 0.1
kc = 0.3

outs ax, ax

aacc = -kc*ax
av = av+aacc
ax = ax+av

endin

</CsInstruments>
<CsScore>
i1 0 5 440
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
