# Bibliografia

1. Federico Avanzini - _Sintesi per modelli fisici_ - dalle dispense del Corso di Sistemi di elaborazione per la musica

# Sitografia

1. [Definizione di sintesi per modelli fisici - da sintetizzatore.com](http://www.sintetizzatore.com/index.php/definizione-di-sintesi-per-modelli-fisici-)


# Videografia

1. [Julius Smith - History of Virtual Musical Instruments Based on Physical Modeling](https://www.youtube.com/watch?v=I64y40EIPaM)

2. [Julius Smith - Sound synthesis based on physical models](https://www.youtube.com/watch?v=dUcNzPhZdwk)

3. [Physical Modelling Synthesis | Explanation and Tutorial](https://www.youtube.com/watch?v=C3a55XjLptM)
# VST commerciali

- Kaivo
- Chromaphone
- Substantia
