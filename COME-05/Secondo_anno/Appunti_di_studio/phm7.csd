<CsoundSynthesizer>
<CsOptions>
-odac -
</CsOptions>
<CsInstruments>

sr = 44100
kr = 44100
nchnls = 2
0dbfs = 1.0

instr 1

ax init 0
av init 1
aacc init 0

kd = 0.0002
kc = 2*(1-cos(2*$M_PI*p4/sr))
outs 0.5*ax, 0.5*ax

aacc = -kc*ax ;accelerazione
av = (av+aacc)*(1-kd)
ax = ax+av

endin



</CsInstruments>
<CsScore>
i1 0 5 333

</CsScore>
</CsoundSynthesizer>