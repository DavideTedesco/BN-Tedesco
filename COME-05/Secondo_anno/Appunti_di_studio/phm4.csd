<CsoundSynthesizer>
<CsOptions>
-odac -
</CsOptions>
<CsInstruments>
sr = 44100
kr = 44100
nchnls = 2
0dbfs = 1.0

instr 1

ax init 0
av init 0.1
kc = 0.3

outs ax, ax
aacc = -kc*ax ;accelerazione
av = av+aacc
ax = ax+av

endin



</CsInstruments>
<CsScore>
i1 0 5 440

</CsScore>
</CsoundSynthesizer>