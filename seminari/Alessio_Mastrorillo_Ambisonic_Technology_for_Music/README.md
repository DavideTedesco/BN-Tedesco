# Appunti dell'incontro con Alessio Mastrorillo: Ambisonic Technology for Music

Introduzione all'Ambisonic e realizzazione di un decoder per Ableton Live.

## Ambisonics

Scrittura della musica partendo dal missaggio e la realizzazione di strumenti per cinema, musica e VR/AR.

Come sfruttare l'Ambisonics su sistemi stereo domestici?

La risposta è stata:
- binaurale -> solo per cuffie
- stereo
- UHJ -> raccolta dell'audio sulla superficie planare, immagine molto ampia in cuffia

Con l'UHJ si perde l'elevazione rispetto al piano orizzontale.

Si parla di UHJ su due canali...

L'UHJ non è stato utilizzato molto...

[Envelope for Live]()

## UHJ, streaming due canali e ricostruzione

Si può ricostruire il segnale ritornando all'E-format.

### Quadratura del segnale

La trasformata di Hilbert

È stato realizzato un filtro, per poter cambiare la fase del segnale senza modificare l'ampiezza, con filtri allpass.

Si può tarare il filtro con un banco di oscillatori e un banco di filtri.

Il filtro è  piatto da 18 Hz a 20KHz, ma cosa succede sopra e sotto tali frequenza?

[Versione standalone](https://undecoded.gumroad.com/l/SSAdecoder)

Lavoro su Max MSP in gen...

## Diffusione dell'UHJ

Si è sviluppato un utilizzo...

Il fatto di poter realizzare lo streaming di 2 canali e realizzare in casa il 5.1, si può veicolare il segnale.

Fratelli Cohen: film _Non è un paese per vecchi_

## Ambisonic e mix scalabile

Possibilità di utilizzo di un numero di canali illimitato...

## Comporre in Ambisonic

UHJ è flat e riesce a conservare le basse frequenze...

## Brani in Ambisonic con l'utilizzo del decoder UHJ

### Trippin' on the edge of time

Elementi che si contrappongono, esso è un brano di computer music.

Vi sono orchestre in CSound e algoritmi realizzati in Wolfram Mathematica.

_Ascolto di Trippin' on the edge of time_

basso -> sintesi in Feedback modulation

Elementi di alea, e scelte degli algoritmi.

Materiale con orchestre di CSound, il materiale è poi montato in Ableton.

>Tutti i grandi musicisti sono in tempo differito.


### Emptiness of the hanging

Touissant e la clave...

_Ascolto di Emptiness of the hanging_
