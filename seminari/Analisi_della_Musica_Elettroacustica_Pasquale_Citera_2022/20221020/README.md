# Appunti della lezione del 20 ottobre 2022

## Post-internet e multimedialità

Vi è più materiale analitico ed estetico, sulla multimedialità attuale.

### Custom One - Oscar Escudero (2017)

Brano che deve essere customizzato ogni volta, dal cantante. Vi è nella parte video tanto di personale.

>Questo brano è solo arte multimediale, ma usa 3/4 di quello che abbiamo sentito proveniente da internet.

Somiglia al brano di Schubert _Star me kitten_, simile come impianto (con ensemble componibile, video e voce).

Questo brano di Escudero viene dopo il brano di Schubert.

>Osserviamo che le esigenze artistiche sono diverse...

Questo brano ha un'idea di estetica comune a Schubert e tutti e due si trovano comuni come idea in un ambito comune.

Questo brano prende piede, ma solo poichè è un'estetica che parla del tempo.

### Sconvolgimenti artistici
>A uno sconvolgimento di natura sociale segue uno sconvolgimento artistico, ad esempio per i grandi cambiamenti di natura tecnologica.

Possiamo osservare tali caratteristiche osservandole in varie branche dell'arte.

Sappiamo ad esempio che Guido D'Arezzo ha inventato delle tecniche per insegnare la musica ai suoi allievi.

La performance e la sola interpretazione, ad esempio non permetteva monofonicamente di scrivere a più voci.

Il compositore era indirizzato a comporre in base alla tecnica e tecnologia degli strumenti.

Vi sono quindi sconvoglimenti tecnologici/fisici degli strumenti che sconvolgono l'esigenza artistica...

>La musica elettronica nasce quando nasce l'elettricità.

L'elettricità fa trasferire il fenomeno acustico per analogia attraverso l'elettricità, su un supporto.

### Cosa è accaduto negli ultimi 40 anni?

Digitale come evoluzione dell'elettricità?

Internet è forse stato lo sconvoglimento più grande degli ultimi 40 anni.

Quanto ha sconvolto a livello di possibilità artistica, l'avvento di internet?

Internet ha portato ad uno sconvolgimento sociale, quindi l'arte a seguire ha portato ad altri sconvoglimenti.

Vi sono stati ragionamenti e prodotti di carattere artistico.

### Post-internet

Post non significa alla fine di, ma assimilazione di un elemento nella società tale che qualcosa si dia per scontato.

Quando qualcosa diventa normale, siamo in un'era post, dato che quel qualcosa è assimilato.

Gli strumenti di internet hanno sconvolto le nostre vite, cosa siamo divenuti ora?

### Utilizzo di internet per tutto ciò che significa

Artisti ed artiste usano in modo estensivo internet, perchè la via è una sorta di contatto diretto con la realtà.

I brani post-internet danno un legame molto diretto con la realtà.

Il movimento degli autori, che creano opere di questo tipo è denominato Zeitgeist, e come descritto da Jennifer Walshe: _Nuova disciplina_

Con _Nuova disciplina_ si intende una macro area, che usano linguaggi che usano o arrivano da internet.

### Digitalizzazione del reale

Discretizzazione del mondo reale nell'ambito del post-internet, con una copia di se stessi su internet.

Nei primi anni duemila esisteva Second Life, dove si poteva realizzare una seconda vita.

Rapporto tra parte virtuale e parte del reale, è il leit motif di tutto il movimento della _Nuova disciplina_.

Sappiamo che il reale ed il virtuale fino ad ora erano separati e non vi era un legame fra i due.

Fra virtuale e reale oggi vi è un legame diretto...

>La parte negativa della persona viene usata in arte per poterla trasfigurare...

### Elementi post-internet di Schubert

Schubert fa una lista degli elementi post-internet, nel brano di Escudero abbiamo visto:
- adaptation of internet-specific art forms

### Wikipiano.net di Schubert

Brano collaborativo realizzato via una pagina wiki.

### Gaming e comunità di gioco
I videogame si possono giocare in locale o in remoto.

Delocalizzazione del gioco e serietà del giocare...

In inglese sappiamo che vi è la parola "play" che tende ad avere meno pregiudizio sul giocare ai videogiochi.

Con i videogiochi vi sono implicazioni interessanti, e il comporto musicale sta ai videogiochi, come lo sviluppo tecnologico sta all'armamento di guerra (come impianto industriale).

>Industria discografica, industria del cinema con la musica, industria del videogioco con la musica...

### Ascolto di Generation Kill - Stefan Prins

Per ensemble variabile, nella versione ascoltata vi sono diversi esecutori:
- violoncellista
- percussionista
- proiezioni di se stessi
- esecutori al Live Electronics gestito da console PS3
- live video
- regia luci

Si comprende che vi è l'alter ego solo perchè vi è il video con errori.

Rapporto virtuale con la guerra e joystick che fanno vedere il rapporto virtuale con le persone che vengono uccise.

>Vedere altra versione a quattro esecutori.

### Visione e ascolto di Genesis di Schubert

Virtual Real Life Computer Game, l'ascoltatore e chi vede è una realtà virtuale che comanda delle cose reali.

### Avery è l'avatar virtuale in play su youtube di Alexander Schubert

### 
