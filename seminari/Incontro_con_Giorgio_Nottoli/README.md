# Appunti dell'incontro con Giorgio Nottoli di lunedì 18 ottobre 2021

## La storia di Giorgio Nottoli

Nottoli studiò composizione con Guaccero e Musica Elettronica con Walter Branchi.

Nottoli fece molto il progettista e compose poco nei 30/40/50 anni.

All'età di circa 45 anni riprese a comporre, componendo con una sua macchina, da lui progettata.

Molti brani di Nottoli vennero influenzati dalla realizzazione di sue macchine.

Si dedicò molto alla sintesi del suono, ma non tutti.

Prendendo una data cominciamo a osservare brani di varie epoche: dal 2010/2011 fino ad oggi.

>Essere sensibili al cambio di prospettive, come compositori.

### Ascolto di _Ordito polifonico_

Tutto ciò che utilizza nel brano è realizzato da Nottoli:
- plugin
- riverbero

Vi è stato un passaggio dall'hardware al software, questo brano venne realizzato con il software.

Si usa un algoritmo di granulazione su campi spettrali, ed esso prende grani di spettro.

Questo brano ha la forma del continuo, questa tipologia di brano è un qualcosa che Nottoli predilige; questa tipologia di forma fa concentrare sul timbro e sulla parte armonica.

E gli accordi fanno parte del processo timbrico.

Questa fascia sonora passa attraverso dei processi timbrici.

Viene utilizzata la formula di McAdams

La durata è di 7 minuti, e fa parte di un ciclo di brani richiesti dal Conservatorio di Milano con una durata di 7 minuti.

A volte i 10 minuti sono un qualcosa di normale, mentre 7 minuti sono qualcosa di conciso.

__Comporre musica elettroacustica__ fu l'occasione dei nuovi brani, dal 2008 fino al 2013, organizzato da Luigi Pestalozza, presidente della rivista Music/Realtà.

Brano eseguito per al [Teatro del Verme](https://www.ipomeriggi.it/il-teatro/la-storia-teatro/)

Brano suonato al festival [Angelica](https://www.aaa-angelica.com/aaa/festival/angelica-28-giorgio-nottoli-soffio-battito-lelettrico-policromo-italia/) di Bologna nel 2018 con sei altoparlanti.

![1](1.png)

#### Aldo Clementi
Aldo Clementi era un compositore italiano e si dice che fosse uno degli unici esponenti dell'informale in musica, ovvero la scrittura di fasce sonore da cui emergono detriti, ovvero degli oggetti trovati.

#### Trame o sfondo

Il lavoro fa parte di un ciclo su trame e sfondi, brano che viene subito dopo il brano tessitura.

#### Trama

Una trama simile ad un chime metallico si sente nel registro acuto.

Sintetizzatore _Texture_ che è stato realizzato da Nottoli sia in modo Hardware e Software.

#### Tipologie timbriche
Questo brano mette insieme le tipologie timbriche che riguarda Napoli e Parigi, partendo dagli insegnamenti di Giuseppe Di Giugno.

Di Giugno è un fisico che progettava come un ingegnere.

Giuseppe Di Giugno, aveva le sue idee musicali, pensando da fisico.

Di Giugno otteneva un qualcosa di appetibile per i musicisti, fuori dal contesto della musica.

#### Da Wessel al brano di Nottoli

Wessel faceva 9 glissandi in Antony, esplorando lo spazio tipico in modi diversi.

Il sistema di generazione è simile a quello di Wessel.

Si crea una pasta inarmonica del risultato con una massa...

David Wessel, psicoacustico trasse le sue conclusioni sulle intuizioni di Di Giugno.

Anche se questo brano è di sintesi, vi è comunque un qualcosa di mimetico, con riconoscimenti di archi e voci.

Ascoltiamo esempi di varie texture realizzate con il vst.

#### Scheletro di altezze

Abbiamo 5 accordi creati con la formula di McAdams.

Vi è una coerenza di principio che usa, e cerca di nascondersi a iò che si percepisce.

#### Forma del brano

Esso ha 6 sezioni con  una coda che ritorna al primo passaggio:
![3](3.png)

![4](4.png)

Vi sono vari parametri per generare delle granulazioni più intelligenti.

I grani vengono indirizzati casualmente su una delle componenti spettrali.

Si possono rappresentare spettri con un movimento dovuto alla granulazione.

![5](5.png)

#### Metodo di sintesi

Sintesi granulare su campo spettrale.

#### Il sintetizzatore Texture

[Link al Sintetizzatore Texture](http://mastersonicarts.uniroma2.it/ricerca/texture-2/)

### _Trama filante_ per Enzo Filippetti

Da ascoltare...

___

Differenziazione tra raccolta dei materiali e stesura che avviene in modo concitato...
___

### Attività di progettista di Nottoli

Nella fase in cui l'attività di progettista(anni '80 e '90), vi era la battaglia per i chip degli strumenti musicali.

nel 1983 nacque il DX7, allora le aziende italiane dislocate nelle Marche.

In quegli anni uscì il TMS320, e si andò:
- Lupone
- Galante
- Sani

Si fondò una società che fece i motori per alcuni strumenti.

Il circuito più forte fu quello realizzato da Nottoli.

La ORLA e Bohem e la Honer utilizzarono il circuito integrato.

Il circuito si chiama __Orion__.

![orion](orion.jpeg)

Il sequencer di Nottoli del 1974 è tenuto in maniera straordinaria.

### Traiettoria tesa

Abbiamo ascoltato un brano acusmatico di sintesi, e ora scoltiamo un pezzo acusmatico concreto, cominciando da questo...

Il brano è per flauto e dura 15/16 minuti.

C'è uno stato di grazia in cui se il valore è proporzionato dal punto di vista tecnico ed espressivo, non si privilegia la parte espressiva e quella tecnica.

#### La parola traiettoria

Essa è inerente al concetto di percorso.

L'immagine generale è quella di un lungo processo con una sequenza di grandi archi.

#### Aggettivo tesa

Richiama a uno stato emozionale che puó caratterizzare l'intero lato formale dell'opera che spinge in avanti il tempo.

Un pezzo che funziona cammina, un pezzo che non funziona sta fermo.

>La musica è tempo percepito attraverso il suono.(Karlheinz Stockhausen)

Il tempo passa in modo diverso a seconda di come si tratti il suono nel tempo stesso.

#### Prima del pezzo

La traiettoria sarebbe potuta essere verso l'alto o verso il basso, per massimo e minimo di tensione.

Brano che ha avuto un riconoscimento immediato al primo ascolto.

#### Materiale sonoro

Materiale sono scelto insieme a Trovalusci.

La prima fase è imparare il flauto di nuovo con lo strumentista.

Abbiamo una carrellata di suoni realizzati da Trovalusci.

- martellato
- whistle tone intonato
- Whistle tone interno, coprendo il foro con le labbra
- colpo di diaframma
- selezione di armonici ascendenti

(non vengono usati multifonici)

#### Sound processing

L'elettronica dal vivo espande e moltiplica il suono dello strumento acustico.

Dialogo fra lo strumento e 3 copie di se stesso con 3 copie strumenti chiamate specchi. Essa è un'idea di Boulez e dell'IRCAM e il suono dello strumento deve provenire da dov'è lo strumento.

#### Spazializzazione

![6](6.png)

EMUpanner, external per Max MSP.

#### Interazione fra flautista e processo

Il processo è composto da delay multipli.

Il processo è guidato dalla partitura elettronica.

Vi sono molti elementi voiced e unvoiced e se devo fare una trasposizione di altezza, la faccio su un suono determinato.

#### Analizzatore

Usiamo un analizzatore:
- basse
- medie
- acuto/rumore

Ciascun aspetto seleziona il segnale sul quale sviluppare il suo processamento.

#### Filtraggio complesso

Vi è una nuvola di filtri con un punto che viene dato nel tempo che dipende dalla scena e dal flauto dipende molto la densità e la tipologia dell'attacco.

Chiamata da Nottoli filters cloud...

#### Composizione strutturalista

Vi è una visione strutturale ma non nel senso seriale e si cerca un ordine interno alla composizione con l'obiettivo espressivo.

#### Utilizzo di Open Music

Algoritmo con 3 funzioni per realizzare un automatismo e volontà improvvisativa.

_Ascolto di Traiettoria Tesa_

___
## Domande

Aspetti importanti sono la comunità...

Brano sette isole con 7 brani con una durata di circa 20 minuti.

Creazione di una comunità viva...

Il discorso deve essere fluido e non bisogna quasi accorgersi andando verso una trasformazione.

>La proprietà del brano è per il 50% dell'esecutore.
