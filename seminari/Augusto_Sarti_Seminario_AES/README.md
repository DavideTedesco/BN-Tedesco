# Seminario: L’ascesa dell'audio spaziale: nuovi strumenti matematici per l’acustica interattiva, intelligente environment-aware

Tenuto da: Augusto Sarti

## Dalla grafica computazionale all'audio spaziale

Dal ray tracing all'audio
![1](1.png)

Dal beam-tracing all'audio -> capire come un fascio acustico si spezzetta man mano che incontra oggetti fisici.

![2](2.png)

Cercare di fare il ray tracing in tempo reale, bisogna fare i calcoli del beam-tree.

![3](3.png)
Vediamo i beam acustici generati da una particolare sorgente, e possiamo calcolare i raggi corrispondenti a quella sorgente.

Se invece di concentrarsi sul calcolo veloce dei raggi, ci si concentra sul beam-tree.

### Lookup di una funzione di ordine superiore, ovvero il complesso di visibilità.

![4](4.png)

Abbiamo un proiettore in geometrica euclidea, a questo punto possiamo partire dallo spazio geometrico e vediamo cosa succede nello spazio dei raggi.

A destra vediamo lo spazio dei raggi.

Qual'è la visibilità attraverso questa finestra.

Cosa vede una qualunque sorgente immagine?

Vogliamo vedere come tutti i raggi che attraversano la finestra a sinistra, vanno a coprire la strisca che si vede a destra.

Abbiamo la visibilità attraverso il riflettore a sinistra.

![5](5.png)

Otteniamo delle geometrie che descrivono cosa vediamo della scena a partire da un riflettore dato.

![6](6.png)

In  questo nuovo spazio dei raggi c'è una sorta di dualità, e abbiamo un punto, rette e segmenti in base a cosa proiettiamo nello spazio dei raggi.

### Zona con tanti riflettori

![7](7.png)

Vediamo i segmenti incontrati durante questa esplorazione angolari, spezzetandosi in tanti beams.

Riusciamo quindi a costruire il beam tree che contiene tutte le informazioni per costruire un campo acustico completo.

![8](8.png)

### Perchè non complicarsi la vita con i beam-tree?

Vi sono dei punti cechi, e dunque come si fa a gestire tutti i raggi?

Per gestire tutti i possibili i raggi è quello di produrre uno spazio dei raggi proiettivo.

L'equazione di una retta è realizzata da tre coordinate proiettive in uno spazio tridimensionale.

![9](9.png)

(non c'è il concetto di prossimità e le distanze)

![10](10.png)

La definizione di uno spazio proiettivo mantiene molto ampia la classe di operazioni effettuabili sullo spazio proiettivo(come l'image processing).

Si risolvono operazioni in modo lineare attraverso le omografie.

I raggi sono una retta orientata, e fu necessario introdurre lo spazio proiettivo orientato, per descrivere una sorgente acustica al meglio.

Tutti questi passaggi hanno fatto in modo di trattare i punti cechi, la direzione e tutte le differenze che ci sono emesse da fasci acustici.

![11](11.png)

Nel caso 2D, un raggio nello spazio ha bisogno di almeno 4 parametri: 2 intercetta, angolo e direzione; possiamo caratterizzare un raggio nello spazio proiettivo.

Passiamo da 2 parametri a 4 parametri per passare al 3D.

La generalizzazione di una retta su un piano è un piano nello spazio in 3D.

L'unica rappresentazione che riesce a dare una rappresentazione chiara è quella attraverso le coordinate di Plucker.

![12](12.png)

Passiamo ad uno spazio proiettivo in 5 dimensioni.

_Prendendo una serie di spaghetti e mettendoli in una pentola vediamo che crea una forma come a quella a destra_

Vediamo una "rigata", ovvero rule of quartet è realizzata da linee.

Immaginiamo ora i vari piani come delle lasagne, ovvero serie di piani proiettivi che formano un'iperboloide sul piano proiettivo.

Di fatto possiamo realizzare delle cose interessanti, realizzate dalla quadrica multidimensionale, come se lavorassimo in piani proiettivi che lavorano in un iperquadrica proiettiva.

Vediamo che possiamo vedere come si intercettano i piani con la quadrica proiettiva.
![13](13.png)

I piani delimitano le regioni come nella versione 2D.

Tutte le regioni in mezzo generano la regioni di visibilità.

Vediamo che tutto torna lineare come operazioni e salta fuori una tecnica che consente di costruire una tecnica di beam tracing.

Saliamo in dimensionalità per calcolare in tempo lineare.

![14](14.png)
![15](15.png)

(Fabio Antonacci e Dean Markovic, tesi di dottorato su questi strumenti)

### Demo
_Demo su beam-tracing con calcolo dell'intero campo acustico_
![demo](demo.png)

Posso aumentare l'ordine di riflessione come mi pare, avendo 8 ordini di riflessione.
![demo1](demo1.png)

Spostando sorgente o microfoni i calcoli avvengono allo stesso modo.

Attraverso un meccanismo di questo tipo si può realizzare "Acustica virtuale ed interattiva".

![16](16.png)

Visitare un labirinto tridimensionalmente.

(spazi a Cremona per la realizzazione e il test di questi ambienti tridimensionali)

È anche possibile trattare fenomeni come la diffusione e la diffrazione.

### Come trasformare un problema di sintesi in un problema di analisi?

![17](17.png)

Il complesso di radiosità...

Cosa vuol dire parlare di cattura di un campo acustico?

![18](18.png)

Invece di trattare la pressione acustica come uno scalare, si è deciso di trattare i descrittori come dipendenti dai raggi. Ciò è un retaggio dell'illuminotecnica. La visibilità è concetto invariante rispetto al raggio e la posso esprimere come funzione del raggio.

La radianza acustica, è la componente della pressione acustica lungo una data direzione.

Possiamo esprimere la radianza in funzione del raggio o dei suoi parametri.

Possiamo ora gestire i campi acustici rispetto alla loro riflessività.

Possiamo ora definire una finestra di osservazione.

Discretizziamo il segmento per campionarlo con i microfoni(schiera di microfoni).

Parliamo di una camera acustica, composta tante microschiere animate

![19](19.png)

Vediamo una tencnica di beam forming che cambia a seconda dell'angolo.

Considerando una sottoschiera, possiamo avere tante microcamere acustiche allineate e disposte in modo a piacere.

Questo è il concetto di camera plenoacustica(camera Lytro)

![20](20.png)

Come ciò che è stato fatto in Matrix con varie camere.

![21](21.png)

Vediamo come tutte le camere realizzino varie strisce, otteniamo un immagine di questa tipologia.

Questa immagine soundfield ha una somiglianza strettissima rispetto alla modellazione acustica precedente.

Possiamo allineare questi parametri lungo una retta.

Vediamo degli artefatti quando la risoluzione cala osservando un oggetto rispetto alla direzione della sorgente.

### Cosa succede osservando 2 sorgenti in movimenti?

Osserviamo una retta per le due sorgenti.

![22](22.png)

Le rette sono sempre distinguibili.

E la situazione diviene piú interessante anche inserendo un ostacolo nel mezzo.
![23](23.png)

Allora posso spostare la sorgente, posso capire qual'è la forma dello specchio.

È anche possibile cambiare la risoluzione dei microfoni.
![24](24.png)

Riusciamo anche a riconoscere gli artefatti.

La camera soundfield genera delle immagini plenoacustiche.

![25](25.png)

![26](26.png)

### Profondità di campo dell'array

![27](27.png)

### Microfoni MEMS

![28](28.png)

![29](29.png)

### Cosa si fa con tutto ciò?

Si possono generare dei proiettori plenoacustici.

![30](30.png)

### Applicazioni

![31](31.png)

Wave field synthesis in tempo reale

![29](29.png)

Vediamo come il risultato è formidabile come lo è la risoluzione spaziale.
![32](32.png)

![r33](r33.png)
![r33](r34.png)
![r33](r35.png)
![r33](r36.png)
![r33](r37.png)
![r33](r38.png)
![r33](r39.png)
![r33](r40.png)
![r33](r41.png)
![r33](r42.png)
![r33](r43.png)
![r33](r44.png)
![r33](r45.png)

### Ulteriori Applicazioni

Trasformata invertibile superando i limiti dell'acustica geometrica

Tecniche di reti neurali convoluzionale per ricostruire il ray space partendo da ricostruzioni sparse di microfoni, arrivando a una ricostruzione dettagliata del rayspace.

È anche possibile le tecniche di DIP, per ricostruire i bassi ordini statistici dell'immagine per costruire un immagine reale ad alta risoluzione per costruire delle immagini plenoacustiche con elevata correttezza. Utilizzando i meccanismi senza un'attuale copertura dello spazio


### Domande

Scrivere al professore Augusto Sarti per i materiali citati durante la presentazione.

- Frequenza di Schroeder e tecniche di sintesi dello spazio? Per ora si realizza tutto attraverso le early reverberations. Il problema oltre la quale il problema diviene statistico, non viene affrontato.

- Come mai alcuni software di riproduzione acustica ci mettono di piú del programma presentato? Perchè quei software sparano raggi a caso, essa è un'operazione dispendiosa perchè devo ricalcolare tutto. Il beam-tracing è molto veloce quando sposti il ricevitore, perchè ha fatto il pre-calcolo dei beam-tree. C'è una fase di precalcolo fatta a monte costruendo le regioni di visibilità. (ray space transform pubblicato dal PoliMi)

- Si possono modulare in maniera simile gli ordini delle schiere plenoacustiche.
- la generalizzazione a forme non planari, si puó fare con microfoni sparsi in una stanza, usando il teorema della tomografia(articoli sulla **sparsità** con l'università di Sydney)
- Tuchare Bayapala, difensore di microfoni di ordini superiori e del plenoacustico.

- Cheating Shannon

- Tecniche di complessive sensing(tecniche adattative) -> la sparsità da un'accelleratore acustico
________

Repository di dati ambisonici utilizzando facendo un'acquisizione di aule. Indoor e outdoor.
