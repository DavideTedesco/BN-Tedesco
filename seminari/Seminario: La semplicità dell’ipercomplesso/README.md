# Seminario: La semplicità dell’ipercomplesso: il deep learning per l’audio immersivo

## Introduzione

Modi per la registrazione e riproduzione di uno spazio acustico.
![frame](frame-3.png)
![frame](frame-12.png)
![frame](frame-12.png)
![frame](frame-16.png)
![frame](frame-27.png)
![frame](frame-35.png)

## L'ambisonics
![frame](frame-41.png)

Creare microfoni virtuali

aumentare microfoni per aumentare la definizione.
![frame](frame-42.png)
![frame](frame-48.png)
![frame](frame-58.png)
![frame](frame-72.png)

Rotazione
![frame](frame-80.png)

![frame](frame-89.png)

Virtual miking
![frame](frame-93.png)

## Algebra ipercomplessa
![frame](frame-95.png)

Come ternioni e quaternioni
![frame](frame-99.png)

### Algebra quaternionica
Introdotta da Hamilton
![frame](frame-112.png)

Per una rappresentazione di tempo e spazio per la rappresentazione di tempo e spazio.
![frame](frame-123.png)


#### Proprietà dell'algebra quaternionica

![ijk](ijk.png)

- non commutatività
![ham](ham.png)

La moltiplicazione tra 2 quaternioni è costruita in modo diverso.

Il prodotto di Hamilton rappresenta il cuore delle operazioni.

![ham2](ham2.png)

Il prodotto di Hamilton influisce anche per le rotazioni e ciò in ambito quaternionico significa utilizzare una dimensione aggiuntiva.

![rot](rot.png)

![gimb](gimb.png)

[Blocco Cardanico](https://it.wikipedia.org/wiki/Blocco_cardanico)

![inv](inv.png)

Le involuzioni hanno implicazioni importanti poichè sarà possibile derivare da possibilità statistiche per usare dal punto di vista dell'audio. capendone i vantaggi.

![inv](inv.png)

![stat](stat.png)

La matematica può essere molto sfruttata per avere applicazioni di comune utilizzo.

## Algebra quaternionica per i segnali 3d

![squat](squat.png)

![ambqu](ambqu.png)

Possiamo considerare 4 segnali ambisonici come un unico segnale ambisonico.

Quando elaboro un segnale 3d posso andare a sfruttare determinata feature.

![feat](feat.png)

Possiamo sfruttare le caratteristiche legate ai segnali acquisiti.

![acu](acu.png)
![acu2](acu2.png)

Posso considerare gli spettrogrammi di parte attiva e reattiva con numeri reali e immaginari.

Posso trattare 4 tipi di informazioni diverse con 2 quaternioni.

## Quaternioni e Network Neurali

Usando il Deep Learning creaiamo le reti neurali quaternioniche(nate in Italia tra un gruppo di Siciliani)

![deep](deep.png)

Operazioni chiave nel dominio quaternionico legate ai prodotti.

Ridefinendo l'operazione di convoluzione:
![conv](conv.png)

Ora invece di avere un blocco di ingresso e matrice di pesi sfrutto le correlazioni fra i vari input e la matrice dei pesi.

![pesi](pesi.png)

Se i segnali d'ingresso sono abbastanza correlati, la distribuzione delle matrici sfrutta le correlazioni tra i segnali.

![pws](pws.png)

![convo](convo.png)

![rico](rico.png)

![convo2](convo2.png)

Schema di una rete neurale di quaternioni

![schema](schema.png)

Sfruttiamo le potenzialitàà di segnali correlati

## Come arrivare da una teoria che abbraccia la parte acustica a quella dell'algebra ipercomplessa ad applicazioni audio 3D

![3d](3d.png)

![dnn](dnn.png)

La memoria dei dati viene sfruttata per alcuni tipi di applicazoni.

![3ddnn](3ddnn.png)

Realizzare il pre-processing nel dominio quaternionico.

![qcnn](qcnn.png)

Architettura utilizzata per sound event detection e localization.

Con riconoscimento del segnale e stima della direzione di arrivo stimando le coordinate di arrivo del segnale

![qssl](qssl.png)

![qnn](qnn.png)

![datasets](datasets.png)

### Esempi di applicazioni

![noise](noise.png)
Come il microfono dei piloti di aereo.

![virtu](virtu.png)

![simu](simu.png)

## Conclusioni

![conclu](conclu.png)

![ambi](ambi.png)

"Non c'è nulla di piú pratico che una buona teoria"

L'algebra quaternionica è molto complessa dal punto di vista teorico, ma che ha poi applicazioni audio.

_______
![info](info.png)

Reti neurali complesse sono state sviluppate ad Ancona, San Diego e Catania

[Paolo Arena](https://www.dieei.unict.it/docenti/paolo.arena) a Catania
