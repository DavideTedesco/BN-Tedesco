# Incontro con Emiliano Turazzi

[Link alla videolezione](https://www.youtube.com/watch?v=8-Amgvivz1A&list=PLxcqYDjKDVLSrN1mHzlNi0huZAS5maHHf&index=6&ab_channel=PasqualeCitera)

## Registrazione

>La registrazione è una forma di scrittura!

Prendere tutto come un qualcosa di provvisorio, e tutto quello che si dice rimane anche una domanda.

Contributo del 2012, dal quale egli è lontano.

Fasi della vita in cui si è costantemente lontani da se stessi.

Momenti di crescita in cui non si capisce dove si è!

>Perchè Pasquale Citera ha chiamato Emiliano Turazzi, sulla rappresentazione reale e fertile. Poichè nelle lezioni che facevamo, e la domanda importante era: ma come scrivono i compositori, che pensiero c'è dietro la realizzazione pratica, essi usano lo stesso metodo o metodi diversi.

## Emiliano Turazzi come musicista poco sistematico

Fatica nel trovare un proprio metodo.

Brano più antico è del 1996, ovvero scritto a 26 anni.

Il problema del metodo è importante, perchè si corre il rischio di farsi imprigionare dai metodi degli altri.

C'erano a Milano vari approcci di composizione:
- allievi di Sciarrino
- allievi di Donatoni
- allievi di nessuno

Alcuni allievi avevano approcci diversi, gli allievi di Donatoni erano ad esempio strutturalisti, con meccanismi automatici e il controllo del compositore era all'origine e l'interesse per il suono era molto relativo e gli strumenti erano indifferenti.

Donatoni non aveva un'inclinazione leggittima, non era interessato ai processi, e la forma era un risultato di pannelli accanto agli altri.

Vi erano in Donatoni tecniche compositive astratte

Sciarrino aveva un'approccio più intuitivo e uno dei metodi principali i piani grafici, la forma era molto importante e la forma veniva controllata attraverso dei grafici che permettevano di staccarsi dal lavoro materiale e di guardare lo sviluppo tenendolo sotto controllo.

Vi erano già nella fine degli anni '80, influenze della scuola spettrale francese.

>Oggi la circolazione del sapere è molto rapida.

Nel 1991, gli studenti non sapevano di Lachenmann, di Lucier e di altri. Cage era un pagliaccio, Nono un dilettante etc...

Milano era una realtà provinciale.

Il problema del metodo era per Turazzi il fatto che era vicino a Sciarrino e le persone che stimava erano:
- Netti -> vicino allo strutturalismo
- Billone -> grafici vicini ai grafici di Sciarrino

Turazzi che aveva un approccio orientato a una musica che si sostanziasse di suono, si immaginava che il metodo migliore fosse fare una realizzazione grafica per realizzare la continuità. Tale opzione non era a lui congeniale perchè il disegno in bianco e nero è ciò a cui si lega.

Turazzi si è accorto sempre di più che i grafici erano pieni di commenti, descriveva dando nomi alle cose, e dare nomi a una cosa fa divenire una cosa a un oggetto.

Sciarrino suddivide e categorizza il suono e lo puó trasformare in oggetti e figure, ed una certa natura delle cose scelte impone di relativizzare il senso della musica.

>La verbalizzazione è un qualcosa in cui Turazzi si riconosce come metodo. Egli realizza dei diagrammi di flusso.

Finchè Turazzi ha pensato di realizzare grafici, egli ha avuto dei problemi di gestione del materiale musicale.

>Un grafico potrebbe essere un qualcosa come: ![1](1.png)

Le partiture di Sciarrino sono anche traducibili da un'assistente di Sciarrino.

Il fatto di poter affidare un sistema di composizione che possa affidare ad un terzo di realizzare il lavoro di bottega per avere la possibilità di tradurre un brano.

## Emiliano Turazzi nasce come compositore interessato al suono

Turazzi nasce come sassofonista e jazzista Jazz, non ha smesso di praticare Jazz e improvvisazione.

La cultura dell'improvvisazione di Turazzi nasce anche dal legame con il Jazz.

Nella cultura afro-americana, il musicista deve trovare una sua propria via, i grandi jazzisti devono essere riconosciuti immediatamente, come Armstrong, Parker o Davies.

Vi è una cultura di appropriazione dello strumento, esplorando lo strumento, esplorandone le risorse.

Ciò vale a diversi livelli e permette di avere un suono diverso rispetto a due individui diversi.

Ornette Coleman è il musicista preferito di Turazzi e racconta di fare una nota con tutte le posizioni possibili.

Turazzi ascolta la musica dove si fa il suono davanti a noi e spesso si ascolta in maniera fluida.

>Il multifonico è un punto di una serie continua di trasformazioni.

Il multifonico è un punto è un momento che un suono fa divenire un qualcos'altro.

Turazzi ha incomiciato ad approfondire il suono dall'interno degli strumenti, e dagli strumenti che era in grado di suonare.

Ci troviamo davanti l'universo guardando lo strumento musicale da vicino.

Turazzi, essendo un clarinettista, gli viene molta voglia per scrivere il clarinetto. Turazzi guarda troppo dal punto di vista del clarinettista che fanno guardare lo strumento con i propri occhi.

## Strumento come labirinto

Uno strumento è un labirinto e serve una chiave d'accesso per labirinto.

Il sassofono lo ha studiato sistematicamente, con una catalogazione sistematica.

- armonici naturali della fondamentale
- tutte le coppie di armonici naturali
- ogni diteggiatura dello spettro
- possibilita1 di piegare l'intonazione
- voce

Mappa gigante per la catalogazione, e si maturava un piano formale per il brano per le categorie.

Con la mappatura Turazzi non riusciva a farne nulla.

Turazzi aveva una sua mappa e nonostante questo bisognava tornare a studiare lo strumento.

Come va il suono da una qualità ad un'altra?

Ogni volta che doveva fare qualche cosa, bisognava riprendere lo strumento in mano.

Capendo cosa non serva, è un modo per capire di cosa si abbia bisogno.

Essere vicino allo strumento...

Il metodo nel caso di Turazzi è plurale.

>La musica di Bussotti si può analizzare con una poesia a livello grafico. Analizzando _De Rara Requiem_ ed _I cinque frammenti all'Italia_, in un singolo pezzo, Bussotti mette assieme un'esposizione di un totale cromatico e un moviemento cadenzale con una sostituzione di tritono. Ovveor con due tecniche diverse, vi sono dei grafismi ed alcune note sono scelte in maniera grafica con una pletora di tecniche.

Quale metodo fa a proprio caso?

Quali tecniche fanno al proprio caso? Anche se le tecniche si vanno a studiare.

Analizzando _De Rara Requiem_, l'analisi non è riconoscibile alle note, ed è fertile e stimolante vedere tale partitura che è strutturata a ritornelli, come guardare lo stesso oggetto in maniera diversa. Vedendo la forma in altri termini, ovvero una sorta di approfondimento di strati di attraversamento.

Uno strumento solista entra nel coro, viene assorbito dal coro, ed esce come un violoncello...

La partitura ci induce a concentrarci su diverse cose.

Anche i processi non sono la stessa cosa...

## Turazzi lontano da alcune cose da egli stesso realizzate

La grande enfasi sul suono e sul suono attraverso lo strumento guidato all'inizio era una cosa di grandissima rilevanza e portava all'attenzione della musica cose un po' neglette.

>Alla fine, non è il suono che è il centro della musica, ma quello che si fa accadere attraverso il suono, ovvero l'esperienza che si fa fare agli altri attraverso il suono e la musica.

Se mi metto in crisi il senso della ricerca del suono, non è evidente avere suono A e B, perchè ciò non rende immediatamente importante un suono e capace di far accadere qualche cosa ed il suono in se si relativizza un po'.

>La musica è importante quando crea un qualche tipo di condizione d'ascolto. Ovvero che essendo in un luogo, siamo presenti in un accadere musicale, e tale accadere ha una sua unicità, una sua fondamentalità, con cose diversamente fra di loro.

Steve Reich fa questa cosa tantissimo, ovvero ad esempio tutti i pezzi con lo sfasamento creano brani musicali che dal vivo fa accadere delle cose nello spazio acustico, con cose molto interessanti.

L'esperienza dei brani dipende non solo dal brano e dallo strumento, ma anche dall'acustica.

L'esperienza della musica ci mette in un ascolto particolare, che ci mette in relazione con gli ascolti pregressi.

Un concerto è un rituale, e si ascolta in modo diverso, quando si ascolta da soli. Si ascoltano delle componenti a cui non siamo attenti in altri momenti.

Concerto per orchestra di Bartok, ascoltato nel loggione della Scala, che è un inferno acustico. All'inizio del concerto dell'orchestra, si sente lo spazio che si riempie di suono e Bartok si lega con la musica fatta e reale, non con quella astratta.

Abbiamo un patrimonio gigantesco di ascolti ma solo registrati e non dal vivo.

Manca la componente visuale e come si ascolta.

Vi è della musica che nasce solo per essere registrata, pochi fanno musica per essere registrata o non registrata.

Le componenti acustiche e spaziali sono dominanti in un ascolto e vi può essere una musica non buona, e vi può essere una musica accettabile che fa avvenire qualcosa nello spazio.

Turazzi è attratto sempre di più a una dimensione della musica che fa accadere qualcosa nel proprio luogo.

Turazzi è attratto da Lucier, che fa musica sulla base dei fenomeni acustici ed a seconda dello spazio, la sua musica fa qualcosa di diverso nello spazio.

Ma Turazzi rimane quello che è, musicista europeo con una formazione alla musica che da un'enorme rilevanza alla forma.

Oggi Turazzi da importanza alla forma, ma non considera la forma come una discrimimanante.

Turazzi segue fenomeni compositivi molto forti.

Una musica che mette al centro la forma pesa molto...

Turazzi è astratto da una musica esclusivamente nuda ed in questo momento preferisce di ascoltare 20 minuti di Lucier per via dell'esperienza, rispetto a ciò che fa Bach.

>Durante il primo lockdown Turazzi ha lavorato moltissimo, con una discussione molto forte con sua moglie. Ella disse che il brano era molto lento, e ebbe un'epifania su cosa vorrebbe fare sulla musica, ma ha subito dopo perso tale epifania.

A Turazzi interessa molto immaginare un tipo di musica che possa assomigliare all'arte di Anish Kapoor.

Vi sono degli specchi convessi o concavi che deformano le immagini, ciò che succede è che si può vedere una fotografia dello specchio, ma la presenza dello specchio può curvare, e tali specchi danno una componente acustica.

>A Turazzi interessa una musica che diventa parte di uno spazio.

Un'altra musica:
>musica che non ha una forma definita.

Un'esecuzione con degli errori non è ancora una cattiva esecuzione. Il brano non è la partitura e neanche l'esecuzione ideale, ma esso è il qualcosa che accade quando accade.

Citando Emilia Fedini:
>La musica esiste soltanto nel momento in cui viene suonata.

  Questa citazione è valida per Scarlatti per alcuni brani, ma non per Kontakte o Hymnen di Stockhausen...

L'esecuzione realizzata male era un collage di cose migliori delle varie esecuzioni.

## _Passa attraverso il corpo_

Sulla partitura si hanno in verde gli interventi della voce. Si ha un organico uguale a quello di un ottetto di Schubert:
- corno
- fagotto
- clarinetto basso
- due violini
- viola
- violoncello
- contrabbasso

Gli archi sono fortemente scordati, ed esso è un brano pensato secondo modalità del Turazzi di allora con un grande interesse per la voce.

Turazzi pensava che la voce parlasse accostandosi allo stile Jungle di Duke Ellington con le sordine delle trombe wah wah prese da Stockhausen e Sciarrino.

L'interesse dello strumento che è anche voce, e tale cosa attrare Turazzi molti.

Il brano è pensato con un corno solista, ed il corno usato in un certo modo è inteso con una vocalità chiusa, mentre il fagotto è pensato come una voce femminile.

Il corno fa le veci della componente vocalica, gli altri strumenti fanno emergere la componente consonantica con degli elementi percussivi.

Emerge il corno, poi gli elementi percussivi.

Ne sentiamo l'inizio e pou una parte.

**Ascolto di _Passa attraverso il corpo_ fino a 4'15"**

Abbiamo sentito un grattato degli archi, ovvero una sorta di gigantesca somgilianza con la voce.

Alcune cose erano pensate con la voce sotto.

Si riconoscono qui 2 categorie:
- suoni rischiosi -> instabili in cui vi è un margine di indeterminazione di ciò che può succedere -> suono che può cascare, suono legati al quotidiano
- suoni molto rotondi -> suono grave del clarinetto basso, cordiere degli archi -> suono che fa risuonare lo spazio in maniera diversa

Esse sono due poli dei suoni di Turazzi.

Esse sono caratteristiche di questa composizione e della musica.

>Suono con la relazione con lo spazio.

La disposizione degli strumenti è pensata con la necessità di far sentire gli archi.

Turazzi con movimenti dal vivo, pensava di avere distrazione dai movimenti...

Il suono come qualità è molto importante...

I solisti sono importanti nella musica di Turazzi.

Lo spazio è pensato internamente ed il suono è pensato come uno spazio in se, e lo si può ascoltare in tanti modi diversi.

Il suono ha una sua densità spaziale e l'ideale di ascolto di un brano e1 uno spazio piccolo con un ottetto da vicino.

Dal vivo con una sala adeguata un brano.

>Nel 2001 scrisse un brano per suoni attaccati al compositore, senza pensarli troppo in una sala.

Esperienza palpabile del suono in spazi piccoli è importante.

Ora ascoltiamo un punto dove vi è un testo frantumato, e dovrebbe riecheggiare un testo che dovrebbe essere detto e deformato, con un verso di Dylan Thomas (Time to catch your boys), abbiamo un testo frammentato tra sillabe e voci.

Dopo alcuni frammenti emerge un testo frammentato e poi vi sono degli errori compositivi, in cui la voce è troppo staccata dalla musica.
Gli elementi delle voci sono meno presenti di quanto dovrebbe essere presente.

Dare indicazioni all'atto pratico, più indicazioni si danno, meno si fanno.

Avere bisogno di grandi tempi per le prove non è un capriccio della musica d'oggi.

Le esecuzioni di Webern di Boulez, hanno molti errori.

Si prende in considerazione degli errori che vi possono essere, e vi è una misura di tolleranza degli errori.

Vi sono esecuzioni intollerabili, ma rivelatrici.

Non è stato istruito lo strumentista che la voce deve entrare nelle qualità degli strumenti, rigida come scrittura, scrivere di inserire la voce.

Succede poi una cosa interessante(una cosa bella in musica), e poi vi è un'esplosione strumentale...

Noi restiamo figli di tutti i nostri ascolti.

**Ascolto di _Passa attraverso il corpo_ dal minuto a 15'14"**

Si sente in questo brano che vi è un grande interesse nella forma, e ciò oggi interessa Turazzi molto di meno.

Vi è una grande complessità, e la complessità non è per forza un valore assoluto che la musica deve avere.

In questo brano voleva spingersi verso un qualcosa di difficile strumentalmente.

A Turazzi non piace il termine tecniche estese, perchè sposta l'attenzione dal suono a quello che si sta facendo, la tecnica porta a mettere dentro categorie di un qualcosa che è in realtà fluido.

Ma dobbiamo pensare in termini diversi e non di tecniche estese, non restringendo un campo.

Oggi è normale parlare di tecniche estese che si danno per scontato, mentre 20 anni fa non si usava il termine tecniche estese.

La tendenza a spingere i limiti della strumentalità a Turazzi non interessa più.

A Turazzi non interessa che una composizione abbia tra i suoi motivi di interesse, aver spinto sul piano strumentale.

All'epoca interessava a lui che vi fosse una forte spinta strumentale.

Turazzi viene da una tradizione strumentale di anti-virtuosi.

>Il virtuosismo non è il centro, ma deve essere un mezzo.

Se vi sono delle difficoltà e vi devono essere...

## Momento di transizione di Turazzi

Spostamento dalla scrittura con un brano del 2011/2012.

Brano per violino e nastro, con un'elettronica brutta, se non per il Live Electronics.

L'IRCAM ha un'estetica precisa, con dei marchi, come per l'ECM è più importante il suono con cui registrare la musica che la musica registrata.

A Milano si sentivano molti concerti di musica elettronica in cui succedeva tutto e non gli interessava la staticità del nastro.

>Il nastro ha scoperto poi che non è mai stato statico.

Il Live Electronics, eccetto nella forma primitiva del feedback, l'elettronica l'ha sempre interessato.

Il nastro Turazzi l'ha sempre utilizzato con Wavelab 6.

Turazzi usa l'elettronica con:
- equalizzazione
- compressione
- trasposizione
- generare rumori
- generare le sinusoidi

## _Da qui si deve passare_

Questo brano tiene insieme un paio di concetti di Turazzi.

Il brano è stato suonato con un'amplificazione non adeguata, senza click.

Il brano si può leggere in maniera cerebrale, realizzando un pezzo di musica fatto sulla musica.

Il tentativo è quello di realizzare un dialogo tra qualcosa(pezzo per chitarra elettrica e sassofono è stato pensato come conversazione tra Nono, Albert Hyder e Jimi Hendrix).

Il brano in oggetto è pensato come un tentativo di mettere insieme tendenze di stato, di andare di lato ad andare verso musiche fatte di forme, avvicinandosi all'estetica più nuda di Lucier e Penny. Mantenendo un piede nella tradizione formante, con un approccio con lo strumento come visto prima.

Il brano ha una sua graniticità e sloennità.

IL brano è realizzato in 3 parti, tutte e tre nello stesso modo, come una torsione ideologia del pezzo di Lucier, I am sitting in a room.

Ciò che avviene sono persone che parlano prima del concerto, con loop filtrato per dargli una direzione.

Vi è quindi una differenza importante che potrebbe dare fastidio a Lucier.

Turazzi filtra fino ad una dimensione che fa entrare il suono nel violino, con 3 cose piuttosto diverse.

Ogni sezione del violino è realizzata su corde diverse.

Il violino entra sul re e poi esce dallo strumento, con articolazioni strumentali che rimandano alla voce.

- La prima sezione è sul Re.
- La seconda sezione è sul La, le voci divengono una sinusoide. Succede che il violino entra dal ponte e poi si gonfia il suono verso il la, il rumore bianco si stacca dal suono e ha un ruolo diverso.

Il rumore bianco diventa via via degli impulsi.

Abbiamo

- La terza sezione ha voci verso il basso, e poi si va in saturazione e avviene un casino, e vi è un altro elemento di Cry Baby di Janis Joplin

Il video di Janis Joplin è una performance che acquista tantissimo il video e quindi la visione della performance...

Vi è un continuo trascolorare, entrando ed uscendo dalla vita ordinaria e straordinaria di entrare e uscire dalla musica.

Vi è un momento del fare della musica che ha a che fare con il rock.

>La sezione aurea è un qualcosa che dimostra che abbiamo ascoltato tanta musica, e non che è un qualcosa di connaturato.

Il brano è con una forma anche solenne a suo modo, con un titolo _Da qui si deve passare_, ovvero dalla morte.

**Ascolto di _Da qui si deve passare_ da a 3'18" a 3'29"**

Ascoltiamo un punto di trasformazione avanzata delle voci.

Abbiamo una sinusoide con il dialogo del violino, facendo fare alla sinusoide un battimento, come una condizione particolare del suono, come un'ultima condizione di quelle voci viste fino ad ora, con un eco remota del movimento.

Lucier ebbe un'epifania italiana, arrivando in Italia che era un compositore neoclassico, e andò a studiare da Petrassi, che gli disse che faceva neoclassico.

Tornato in America Lucier, non sapeva e non voleva imitare la musica contemporanea che aveva sentito.

**Ascolto di _Da qui si deve passare_ da a 7'30" a  9'**

Esso è uno dei primi brani in cui gli interessa lo spazio.

Il brano andrebbe suonato con 2 diffusori davanti al pubblico e un violinista in mezzo al pubblico, e dovrebbe partire la voce mentre il pubblico sta chiaccherando.

**Ascolto di _Da qui si deve passare_ da a 10'53" a 12'11"**

Il violino entra concettualmente dal rumore bianco e poi entra ed esce dalla sinusoide.

**Ascolto di _Da qui si deve passare_ da a 17' a 18'42"**

Sentiamo che si ritorna alle voci.

**Ascolto di _Da qui si deve passare_ da a 24' a 29'**

Vi sono in questo brano diversi livelli di lettura con diversi mondi si confrontano.

Il brano non è intelletualistico e l'ambizione è quella di trasformare tutte le cose facendone un'altra.

Il brano non è intelletualistico perchè non deve arrivare a dire che si sta ascoltando un qualcosa di Lucier, Xenakis, o altri.

>Giuseppe: si è partiti da un approccio con la forma, poi il rapporto con Lucier, verso un processo vicino al suono, con la naturale produzione fisica, con la potenzialità di capirne il Live Electronics.

Brano con potenziali concertistici, con grande impressione sul pubblico.

>Adesso il nastro interessa Turazzi con delle esperienze vive con delle cose che non sono vive.

Turazzi ha un ascolto del nastro come una memoria: __il nastro è una memoria__.

Cercare una sintesi tra musica e nastro.

Attraverso la musica il nastro deve essere qualcosa di presente e non concettuale.

## _Lo sguardo fermo, senza sforzo_

Esso è un brano realizzato con il feedback, ed è arrivato al feedback cercando con un brano per flauto dolce ed un risuonatore attivo, ovvero un altro flauto dolce, ovvero un microfono dentro il flauto dolce, e l'altro modificando delle risonanze.

>Bisogna amplificare molto il segnale senza avere un feedback.

Turazzi aveva scritto un brano mai realizzato in cui vi erano degli strumenti dentro dei microfoni che devono amplificare trasformando attraverso risonanze quello che fanno gli altri strumenti.

Appena Turazzi sentì il feedback decise di usarlo compositivamente e incuriosí Turazzi alcune caratteristiche del feedback.

Modificando le diteggiature del flauto si aveva un modifica del feedback.

Il clarinetto è uno strumento simile al feedback?!?

Interessa a Turazzi imbrogliare le cose, cercande di aumentare...

A secondo dello strumento in cui tu metti il microfono sarebbe come se il feedback fosse l'impronta di uno strumento.

Ogni strumento risponde al controllo in maniera diversa.

Un qualsiasi stimolo strumentale, azzera il feedback e puoi far entrare ed uscire il feedback.

Per avere un feedback puro si può utilizzare solo microfoni ovviamente...

>Uno dei problemi con l'elettronica pura è che il suono elettronico si data molto in fretta!

Pensando anche alla musica non colta...

Con gli strumenti acustici è più difficile che il suono sia datato e vi è una possibilità di sguardo storico diverso.

Non si aspettano nuovi strumenti acustici che entreranno nella musica.

La nuova strumentalità andrà in altre dimensione...

Vi sono tanti che costruiscono strumenti particolari, ma non interessa Turazzi perchè introducono nuove gabbie.

Alois Hába fece un'opera con quarti di tono che sembra però stonata, essa è un'opera normale  rimane un'estetica di armonia funzionale super satura.

L'ultima componente spettacolare del feedback, ha a che fare con lo spazio e si ha una componente dello spazio acustico che va a influenzare il feedback e quindi lo spazio controlla il risultato.

Aprí ciò in un teatro musicale astratto in cui vi sono i movimenti e la gestualità totalmente non casuale. La gestualità attrae l'attenzione, perchè non è decorativa(come nel teatro NO).

Nel teatro No, l'apertura del ventaglio mostra il paesaggio e i gesti sono necessari e non arbitrari.

Andiamo quindi in un terreno in cui rendere riproducibile la musica è sempre più difficile.

Vedendo un video manca un po' la componente di gestualità.

Abbiamo un brano con una forma costruita a posteriori, vi sono dei momenti(orizzonti) dove il suono è piú fermo, modulato prevalentemente da uno spazio e vi sono diversi glissati discendenti, ed al terzo momento discendente si apre uno spazio grave e vi è un glissato delle dita ed il feedback tende a rimanare grave.

Il feedback è instabile e non è sempre prevedibile e vi è uno scarto che non compromette la forma o la validità dell'evento.

Il brano è stato eseguito 3 volte da:
- [Elena D'Alò](https://www.youtube.com/watch?v=s7x5mlTMEzc&ab_channel=emilianoturazzi)
- [Manuel Zurria](https://www.youtube.com/watch?v=1vdSwbpavtI&ab_channel=zurriaman)
- Teatro Saronello di Bologna eseguita da Manuel Zurria

Esecuzione con Manuel era prettamente acustica.

Mentre l'esecuzione con Elena era gestita da Marco Di Martino e non vi fu uno scarto grandissimo passando dal mixer a qualcosa di complesso fa tenere al feedback la sua tipologia.

Vi è un altro brano per flauto basso e per clarinetti bassi.

___
## Domande

Il feedback è qualcosa di difficile per lo strumentista e il feedback rappresenta il luogo sonoro più complesso più difficile da saper stimare in termine di tempo?!?

Vi è quindi una condizione di ascolto dello strumentista con il feedback.

Non si può cogliere dal video la presenza dell'esecutore(di Elena).

Alla fine vi è un rilascio di tensione ed avere un flauto basso ha un suo peso.

Elena arrivata al grave si è lasciata andare anche forse per via della maggior certezza del feedback sulla nota grave.

A Caprarola il brano veniva da un brano per clarinetto solo e da Scelsi.

___

Partiture e materiali online

[Emiliano Turazzi - Dopo le ore del sonno - 2002](https://www.youtube.com/watch?v=2t6I9XVhYxs&ab_channel=emilianoturazzi)
