# Appunti dell'incontro "Tecniche estese per la musica elettronica"


La composizione musicale che utilizza gli strumenti elettronici per la generazione, l’elaborazione del suono e per le tecniche di scrittura assistita dal calcolatore.

```
WEBINAR
Mercoledì 24 marzo ore 11:00       
TECNICHE ESTESE PER LA MUSICA ELETTRONICA
In live streaming dall‘Auditorium Parco della Musica
https://us02web.zoom.us/j/86031100510
ID webinar:
86031100510
Quarto appuntamento che chiude il ciclo di Webinar organizzati dall’Accademia Nazionale di Santa Cecilia in collaborazione con il Centro Ricerche Musicali con l’obiettivo di promuovere e divulgare gli aspetti innovativi introdotti dalla Musica elettronica nei diversi generi e stili musicali. Un’ampia panoramica delle possibilità offerte dalle tecnologie analogiche e digitali utilizzate per la produzione, la ricerca e la formazione musicale. Ogni incontro ha presentato uno degli aspetti applicativi fornendo testimonianze e contributi di importanti personalità dei diversi settori, artistici e tecnici.
L’argomento del Webinar è la composizione musicale che utilizza gli strumenti elettronici per la generazione, l'elaborazione del suono e le tecniche di scrittura assistita dal calcolatore.
INTERVENGONO:
Michele Dall’Ongaro,
Presidente-Sovrintendente Accademia Nazionale di Santa Cecilia
Daniele Pittèri
Amministratore Delegato Musica per Roma
Michelangelo Lupone
Direttore Artistico Centro Ricerche Musicali
Ivan Fedele
Compositore e docente di Composizione dei Corsi di Perfezionamento
dell’Accademia Nazionale di Santa Cecilia
Yann Orlarey
Direttore scientifico del Centro di ricerca e creazione musicale
GRAME di Lione
La partecipazione del pubblico ai Webinar è gratuita.  
I partecipanti possono interagire rivolgendo domande o lasciando note a commento tramite chat. Gli interessati possono lasciare il proprio nominativo e recapito di posta elettronica per essere informati sulle future attività dello Studio Paolo Ketoff .
A breve l’inaugurazione dell’innovativo Studio presso l’Accademia Nazionale di Santa Cecilia che si avvale delle più avanzate competenze multidisciplinari per la creazione e produzione musicale
```

## Ivan Fedele

Tecniche estese: diverse tipologie di espansione di uno strumento.

Ciò avviene grazie all'utilizzo di tecniche alla base di realizzazione

### Tipologie di tecniche estese
1. tecniche sullo strumento per cambiare il timbro
2. nuova liuteria  -> CRM e Lupone
3. strumento elettronico -> grande luogo strumentale del trattamento del segnale
  - Capatation -> 2005
  - La pierre e le temps ->

    Interpretazione dei segnali è gestita da sensori ai polsi degli strumentisti. Ciò ha

Brano "Leading Lines" del 2019 che prevede l'uso dell'elettronica, commissianata dal GRAME ed eseguita al Centre Pompidou.

## Momento elettronico nell'acustico
Ivan Fedele nei periodi, momenti elettronici che sono alla base della scrittura post-elettronica.

Nuova musica con strumenti nuovi...

Dal trattamento digitale del suono, arrivo all'acustico ho quindi

## Leading Lines

### Primo movimento
Granulazione con figura random su processo random, filtro passa alto etc...

![1](1.png)

Abbiamo processi acustici enfatizzati

_Ascolto di parte del primo movimento di Leading Lines di Ivan Fedele_

### Secondo movimento

Linguaggio spettralista con concentrazione sulle armoniche enfatizzate dallo strumento elttronico:
- enfatizzazione attraverso l'elettronica
- ritorno verso l'elettronica

_Ascolto di parte del secondo movimento di Leading Lines di Ivan Fedele_

![2](2.png)

### Terzo movimento

Discorso di distorsione e delay variabili e traslazione verso l'alto, lineare ed esponenziale.

![3](3.png)

_Ascolto di parte del terzo movimento di Leading Lines di Ivan Fedele_

### Ultimo movimento

Tecniche di granulazione diverse...

![4](4.png)

Con tecniche di frammentezione diverse del suono...

Al posto di un quartetto abbiamo a disposizione allomorfi degli strumenti:
![5](5.png)

_Ascolto di parte del quarto movimento di Leading Lines di Ivan Fedele_


**Chiunque abbia avuto anche una sola volta l'esperienza di musica elettronica, permette di ripensare ciò che il compositore ha sempre fatto.** Avendo ad esempio una visione sincronica degli elementi(accordo), ma lo si pensa come un harmonizer, si esce da una logica "tradizionale" e si esce dai vincoli.

## Daniele Pitteri

Amministratore delegato di Musica per Roma, che ha voluto realizzare una mostra!

### Paolo Ketoff

Raccontare il tecnicismo delle invenzioni di Ketoff, con ambito divulgativo,

Data di inaugurazione della mostra forse per Giugno.

## Foto dello studio Paolo Ketoff

![6](6.png)

## Yann Orlarey

Inventare interfacce che mettano in relazione il compositore con il pensiero musicale.

Yann inventò FAUST.

![7](7.png)

Lavoro di ricerca in direzione di linguaggi di programmazione per la musica.

In particolare FAUST.

**Perchè i linguaggi di programmazione usati per la musica sono mezzi per inventare le idee?**

### Introduzione ai linguaggi di programmazione
Primi suoni realizzati alla fine degli anni '50...
![8](8.png)

Visione fenomenale per poter capire come fare la musica con computer.
![9](9.png)

Mathews è inventore di molti linguaggi di programmazione per far musica
![10](10.png)

Music III introdusse il concetto importante di unit generator:
![11](11.png)

>La parte grafica era usata per disegnare una patch e poi il programma era implementato!

Manoscritto di Risset che spiegò il concetto di FM a Chowning.

_Tones des increments negative_

Chowning scoprí che usare un linguaggio di programmazione come Music, come un mezzo di invenzione.

### Creative programming

- intenzione
- estensione
![12](12.png)

Relazione tra computer musicista e computer:
- cogitare -> muovere idee per farle muovere
- computer rapido a descrivere
- scegliere
![13](13.png)

La ricerca su linguaggi di programmazione è:

![14](14.png)

### FAUST

![15](15.png)

FAUST è stato il primo linguaggio ad essere compilato:
- strumenti aumentati
- fare schede elettroniche

![16](16.png)

**Oggi vogliamo programmare molte cose e non solo computer, potenzialmente usando lo stesso linguaggio, senza dover riscrivere lo stesso algoritmo. FAUST è un linguaggio comune a molti aspetti, senza dover riscrivere il codice molte volte.**

![17](17.png)

Realizzazione di una proposta virtuale del Synket di Ketoff.

![18](18.png)

Esempio di realizzazione di un modello fisico:[Moforte Guitar](https://www.youtube.com/watch?v=_ZvbxjZNlRg&ab_channel=moForte)

### Musica come mezzo di imparare molte cose

![19](19.png)
![20](20.png)
![21](21.png)
![22](22.png)
![23](23.png)

Poco tempo per realizzare molti strumenti.
![24](24.png)

![25](25.png)

MDO: soddisfazione usata per fare musica con un linguaggio di programmazione è diversa da comporre?

YO: non si pensa al codice, ma si pensa alla musica

MDO: futuro immagginiamo un percorso in cui vi sono figure tecniche e compositive che si sdoppiano da (Di Giugno e Maderna, da Zuccheri e Berio a Lupone, fino ad un'unica figura che si sdoppia).

## Michelangelo Lupone

Mostra su Ketoff, importante per la focalizzazione sul personaggio importante.

Esemplificazione di un'acquisizione di strumenti di pensiero nell'approccio alla musica elettronica.

Acquisizione dalla musica elettronica che soddisfa sia la composizione che la realizzazione nella programmazione.

Erudizione sugli strumenti, insieme alla creatività sugli strumenti stessi.

![26](26.png)
![27](27.png)
![28](28.png)


Ripercorrere le questioni che si
![29](29.png)

### Esempio di studio sugli strumenti a percussione: Feed Drum

![30](30.png)

Analisi delle forme di risonanza, con uno strumento con potente con i modi vibrazionali della membrana, che la dividono in cerchi con potenzialità di vincolo che rendono nuova l'osservazione compositiva e d'ascolto di un tamburo che ha una storia ed una tradizione caratterizzata da aspetti del crecendo orchestrale. E diviene quasi un quartetto d'archi.
![31](31.png)

Gli sviluppi hanno dato vita allo SkinAct

![32](32.png)
![33](33.png)

_Visione di un esempio di Feed Drum con Ars Ludi_

_Visione di un esempio di SkinAct con la tromba_
![34](34.png)

**L'elettronica e le generazioni delle vibrazioni sono generate dalla membrana e dalle percussioni!**

________

Materiali dello studio Ketoff messi a disposizione della BiblioMediateca

![35](35.png)
