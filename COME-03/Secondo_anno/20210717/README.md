# Appunti della lezione di venerdí 17 luglio 2021

Introduzione e discorso sulla masterclass di Xavier Serra...

>È importante capire come funzionano le tecnologie del prossimo millennio...

Contenuti utili sul Team di WebEx...
___

## Cose di cui abbiamo parlato

- [Seymour Papert](https://en.wikipedia.org/wiki/Seymour_Papert)
- [Lego Mindstorms](https://www.lego.com/it-it/themes/mindstorms?ef_id=CjwKCAjw3MSHBhB3EiwAxcaEuxAMc29cZJoPx6-5FVXEcc5E0vsahPoHq8fY65JVxYHwFjf8wpHt6hoC60AQAvD_BwE:G:s&s_kwcid=AL!933!3!377775028262!p!!g!!%2Bmindstorms&cmp=KAC-INI-GOOGEU-GO-IT-IT-RE-PS-BUY-EXPLORE-MINDSTORMS-SHOP-BC-PH-RN-BRAND)
- [Logo Programming Language](https://el.media.mit.edu/logo-foundation/what_is_logo/logo_programming.html)
- [Jean Piaget](https://it.wikipedia.org/wiki/Jean_Piaget)
- fino ad ora abbiamo progettato dei sistemi complicati e non complessi
- supervised learning -> educazione con un'etichetta o con proprietà
- unsupervised learning -> deep learning
- omeostasi -> equilibrio complesso -> importante per la percezione -> il nostro obiettivo è conservare omeostasi
- bioma...
- [COMPOSIZIONE ANALISI MUSICALE E TECNOLOGIA NELLA SCUOLA PRIMARIA
I bambini compongono, raccontano, analizzano, riflettono](http://www.edizioniets.com/scheda.asp?n=9788846754851)
- docenti di musica e reazionarità
- Scuola di Barbiani di Milano
- [Lifelong Kindergarten: Cultivating Creativity through Projects, Passion, Peers, and Play](https://www.amazon.it/Lifelong-Kindergarten-Cultivating-Creativity-Projects/dp/0262037297)
- Bruno Munari, non trattare i bambini come bambini, prendere i bambini sul serio
