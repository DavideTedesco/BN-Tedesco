# Appunti della lezione di martedì 14 settembre 2021

Partecipare alla revisione degli appunti da parte di tutti.

In modo tale da intervenire per una seconda revisione.

[Libreria di Max per l'estrazione dei descrittori in tempo reale](http://www.e--j.com/index.php/what-is-zsa-descriptors/), essa è una collezione di moduli Max per l'analisi in tempo reale del suono e per l'estrazione di una serie di descrittori.

Vi possono essere delle definizioni un po' diverse e quando si scrive una libreria poi si adottano delle caratteristiche diverse.

Dei descrittori un po' piú complessi vengono in genere corredati da una bibliografia.

## Piattezza spettrale

Essa è il rapporto tra la media geometrica dei moduli dei bin e la media aritmetica dei moduli del bin.

>Questo descrittore da informazioni sulla presenza di parziali armoniche.

La media geometrica dei moduli dei bin da 0 se anche solo una parziale è nulla.

La media geometrica è un oggetto molto sensibile alla nullità degli elementi al suo interno.

Di fronte ad uno spettro rigato, ovvero uno spettro fatto di tante parziali sovrapposte.

Un rumore è uno spettro complementare a quello rigato.

Avendo uno spettro rumoroso, avremo che la media geometrica e la media aritmetica, tendono ad essere uguali.

Per riconoscere le regioni vocaliche da quelle consonantiche, si utilizza infatti questo descrittore, che oscilla tra zero ed uno.

Per i segnali rigati, ovvero quelli vocalici o armonici, avendo un po' di storia del campione, possiamo cercare di capire come si potrebbe esso evolvere successivamente nel tempo.

(Anche nella crittografia si cercano sempre delle fonti di rumore reale e non pseudo-random.)

### Media geometrica

Essa si calcola attraverso i prodotti, con il prodotto di tutti i moduli e se ne fa la radice(non quadrata) di tutti i valori.

### Media aritmetica

Essa è una somma di tutti i valori e la si divide per il numero dei valori che si sono sommati.

## Flusso spettrale

Esso è un descrittore che ha uno scopo piú dinamico e da un'idea della differenza che c'è tra una fotografia dello spettro ad un certo istante, ovvero ad una certa finestra, e lo stesso spettro alla finestra successiva.

Guardando in sequenza i moduli, vedendo le fettine dello spettrogramma messi in fila, o vedendo l'animazione dello spettro man mano che il segnale scorre nel tempo, ci possiamo immaginare un descritto che ci da la differenza tra una frame e la frame successiva.

Esso ci dice la dinamica o mobilità spettrale.

Nella fase di attacco avremo quindi una differenza algebrica maggiore e il flusso spettrale indicherà che vi è appunto una grande differenza tra una fremo e l'altra.

Esso tra una frame ed un'altra ci dice quant'é la differenza tra uno spettro e l'altro, ovvero tra un fotogramma e il successivo.

Sottraiamo i bin della frame attuale e di quella precedente e di ció ne viene fatta la norma di secondo ordine, elevando al quadrato e facendone la radice e poi sommandone tutti i valori.

![2norma](2norma.png)

(Dal testo di Park a pagina 81)

>Avremo quindi la distanza in termini geometrici cartesiani che vi è fra questi due punti in questo spazio ideale che ha tante dimensioni quanti sono i punti della finestra.

Se lo spettro si muove poco, i due punti saranno vicini.

Se lo spettro si muove tanto i due punti saranno lontani.

Lo Spectral Flux, serve ad avere un idea per capire se sono nella fase di attacco di un suono, se sono nella fase di sustain, lo spettro sarà abbastanza stabile.

## Aggiungiamo la rappresentazione del segnale

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

fig, ax1= plt.subplots()

color = 'tab:green'
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Segnale', color=color)
ax1.plot(time, x, color=color)
ax1.tick_params(axis='y', labelcolor=color)

color = 'tab:red'
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Scala SpectralFlatness', color=color)
ax1.plot(t, dsf, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dsx, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()
```
![mod](mod.png)

### Plottiamo in modo differente

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

plt.subplot(3,1,1) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(time,x)
plt.xlabel("Tempo")
plt.ylabel("Segnale")

plt.subplot(3,1,2) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t, dsf)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")

plt.subplot(3,1,3) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t,dsx)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flux")

plt.show()
```

Ed avremo quindi:
![3plot](3plot.png)

Possiamo prendere il logaritmo della flattness per poter amplificare le variazioni della Flattness e poterle visualizzare meglio.

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

plt.subplot(3,1,1) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(time,x)
plt.xlabel("Tempo")
plt.ylabel("Segnale")

plt.subplot(3,1,2) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t, np.log(dsf))
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")

plt.subplot(3,1,3) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t,dsx)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flux")

plt.show()
```
![log](log.png)

All'interno delle oscillazioni dei valori possiamo rintracciare l'alternarsi di consonanti e vocali ed i picchi della flattness sono quelli che corrispondono alle consonati mentre le zone statiche alle vocali.

Lo spectral flux, oltre i momenti di silenzio, ci dice che abbiamo un segnale con una variabilità elevata, e stiamo praticamente dicendo che abbiamo un segnale non stazionario; sappiamo infatti che il parlato è un segnale estremamente tempo-variante e quindi non stazionario.

Le variazioni piú marcate del flusso le abbiamo con la transizione tra consonante e vocale.

### Come faccio a segmentare il parlato?

Lo faccio con algoritmi di segmentazioni che usano questi descrittori.

Google translate passa ad esempio un segnale in:
- un fitro che lo rende a fettine
- ogni fonema viene passato nell'algoritmo di riconoscimento dei fonemi -> supponendo di avere l'elemento piccolo del fonema, se voglio addestrare l'algoritmo a riconoscere il fonema, lo faccio esercitare e gli attacco un etichetta -> avendo imparato a distinguere i fonemi
- algoritmo che mette insieme i fonemi
- algoritmi che mettono insieme i risultati e cercano di indovidare quale sia la parola e cercano di indovinare la parola successiva

### Individuazione dei fonemi

Quando ho riconosciuto un fonema, ogni descrittore avrà un suo valore per quel fonema, e quei 10 descrittori e valori sono come un punto in uno spazio a 10 dimensioni e posso valutare quale sia la distanza tra un fonema e tutti quelli che ho in memoria e che corrispondono al mio bagaglio di apprendimento.

Se prendo ad esempio 200 esempi e li catalogo come un preciso fonema, avrò un insieme di 10 valori per quei fonemi, in uno spazio a 10 dimensioni, avremo quindi un cluster di punti.

Un altro fonema, sul quale abbiamo calcolato gli stessi descrittori, ci darà un altro luogo ed avremo uno spazio a 10 dimensioni occupato da questi fonemi.

Fatta questa classificazione, abbiamo lo spazio che corrisponde alla sua esperienza.

Quando gli si introduce un nuovo fonema, si utilizzano i calcoli precedentemente svolti per gli altri fonemi per cercare di individuare il fonema nuovo da riconoscere, individuano la distanza minima del fonema dal cluster precedentemente individuato.

## Analisi dei descrittori per l'oboe

![oboe](oboe.png)

Vediamo nella spectral flattness che abbiamo dei picchi sull'attacco e sulla release.

Vediamo sul flusso spettrale abbiamo picchi sull'attacco e un assestamento successivo e una piccola gobba nella fase di release, con la perdita progressiva di diverse armoniche, avremo quindi delle variazioni dello spettro.

La flattness è ora visualizzata in modalità logaritmica.

>Osservando le analisi in relazione tempo-frequenza, ho bisogno di una certa relazione temporale, sennó i fenomeni non riesco neanche ad osservarli.

Osservare con vari tipi di setacci lo stesso segnale per capire se abbiamo analisi artificiose o meno.

---
## Per la prossima lezione

Far girare gli scripts con diversi strumenti.

---
[Nomi dei colori in matplotlib](https://matplotlib.org/stable/gallery/color/named_colors.html)
