import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "roar_tiger_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

time = np.arange(0,np.size(x))/fs

#calcolo dei descrittori
N = 1024
hopSize = 512
#RMS
[dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=N, iHopLength=hopSize)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=N, iHopLength=hopSize)
#pitch tracker
[vP, tP] = pyACA.computePitch("TimeZeroCrossings",x,fs,iBlockLength=N, iHopLength=hopSize)

#plottiamo

#mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
fig, axs = plt.subplots(3,1)

#configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
color = 'silver'
axs[0].grid()
axs[0].set_xlabel("Tempo", color=color)
axs[0].set_ylabel("Segnale", color=color)
axs[0].plot(time, x, color=color)

#clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()

color = 'tab:red'
ax2.grid()
ax2.plot(t,dsrms)
ax2.set_xlabel("Tempo", color=color)
ax2.set_ylabel("RMS", color=color)

#configuro il grafico della seconda finestra
#plot del flusso spettrale
color = 'tab:blue'
axs[1].grid()
axs[1].plot(t,dsx)
#axs[1].set_xlabel("Tempo", color=color)
axs[1].set_ylabel("Spectral Flux", color=color)

#configuro il grafico della terza finestra
#plot del pitch tracker
color = 'tab:green'
axs[2].grid()
axs[2].plot(tP,vP)
#axs[2].set_xlabel("Tempo", color=color)
axs[2].set_ylabel("Pitch", color=color)


plt.show()
