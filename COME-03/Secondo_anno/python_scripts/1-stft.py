import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

#leggiamo un file audio(file audio mono)
#[fs, x] = read("daniel.wav")
[fs, x] = read("oboe.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcoliamo la matrice stft
#nperseg sono il numero di campioni della stft
#noverlap serve per realizzare un salto di 1000 campioni, riconsiderando campioni gia precedentemente considerato
#f, t, STFT = signal.stft(x, fs, nperseg=2048, noverlap=1024, window='blackmanharris')
#f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=1024, window='blackmanharris')
f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

#plot
plt.figure(1)
plt.ylabel('segnale')
plt.grid()
plt.plot(x)
plt.show()

plt.figure(2)
plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=np.max(abs(STFT)), shading='auto')
plt.xlabel('Tempo [s]')
plt.ylabel('Frequenza [Hz]')
plt.show()
