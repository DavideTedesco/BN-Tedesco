import numpy as np
from scipy import signal
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

#leggiamo un file audio(file audio mono)
#[fs, x] = read("daniel.wav")
[fs, x] = read("oboe.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcoliamo la matrice stft
f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

#mi calcolo il centroide spettrale
specCent = pyACA.FeatureSpectralCentroid(np.abs(STFT),fs)
print("dimensioni del vettore del centroide spettrale: ",specCent.shape)

#plottiamo il tutto
plt.subplot(2,1,1)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.plot(x)

plt.subplot(2,1,2)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Centroide spettrale')
plt.plot(t, specCent)

plt.show()
