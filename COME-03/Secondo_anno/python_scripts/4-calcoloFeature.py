import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcolo del descrittore
#centroide spettrale
[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
#[dsf, t] = pyACA.computeFeature("SpectralFlattness",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flux
#[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale

#plottiamo
plt.plot(t,dcs)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.show()
