# Appunti della lezione di venerdì 17 settembre 2021

## Visione delle analisi dei descrittori sui vari strumenti

Le immagini sono disponibili [qui](/COME-03/Secondo_anno/20210917/Spectral_flux_flatness_various_instruments)

Non dimenticarsi degli altri descrittori, poichè sovrapponendo il grafico con altri descrittori potremo capire cosa avviene veramente.

Script in Python con una sorta di gate per tagliare il segnale, usando dei descrittori per individuare delle zone da tagliare.

Nella chitarra acustica ci sono anche dei meccanismi di feedback.

Anche il descrittore della brightness da informazioni sullo spostamento progressivo delle armoniche.

Il carattere di uno strumento come strumenti impulsivi è caratteristica.

La curva di decadimento della brightness ogni volta sarà diversa.

L'analisi umana ci dovrebbe mettere in evidenza proprio le differenze di analisi fra gli strumenti.

Come noi abbiamo dei pattern per quanto riguarda l'attacco di uno strumento, abbiamo anche dei pattern sulla release del suono.

La Spectral Flatness rimane sempre molto alta sul tam-tam, poiché il suono é sempre vicino al rumore.

Nel tam-tam vi sono dei meccanismi di ricircolazione dell'energia in cui lo spettro é pieno di...

Se abbiamo un artefatto che é dovuto alla risoluzione temporale, aumentiamo la risoluzione della finestra, diminuendo la dimensione della finestra, ed esploriamo fenomeni con una tempo varianza più alta.


Vi é una correlazione fra l'esito timbrico ed il gesto, usando quindi alcune parti dell'archetto vi sono delle questioni fisiche che non si possono non tenere da conto.

Si vede come il contributo del rumore sia presente nelle parti iniziali e finali del segnale del violino.

## Algoritmi adattivi

Che fanno una analisi preventiva dei cambiamenti del segnale, ed in base ad essa adottano una risoluzione temporale piú elevata.

In SPEAR si può ad esempio segnalare se usare le finestre adattive.

## Correzione di ciò che avevamo visto la scorsa lezione

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

fig, ax0 = plt.subplots() #sovrappore grafici con assi y diversi

#plot del Segnale
color = 'silver'
ax0.set_ylim(-1,1)
ax0.set_xlabel('Tempo')
ax0.set_ylabel('Segnale', color=color)
ax0.plot(time, x, color=color)
ax0.tick_params(axis='y', labelcolor=color)

ax1 = ax0.twinx()  # instantiate a second axes that shares the same x-axis

#plot della Spectral Flatness
color = 'tab:red'
ax1.set_ylim(-6,0)
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Scala SpectralFlatness', color=color)
ax1.plot(t, np.log(dsf), color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

#plot dello Spectral Flux
color = 'tab:blue'
ax2.set_ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dsx, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()
```

![col](col.png)

## Come compito

Tagliare un file normalizzarlo:
- in sovraimpressione la RMS in un primo grafico con il segnale dietro
- altro descrittore tra i descrittori visti

[Link a matplotlib](https://matplotlib.org/stable/gallery/lines_bars_and_markers/cohere.html#sphx-glr-gallery-lines-bars-and-markers-cohere-py)

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "roar_tiger_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#RMS
[dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

#mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
fig, axs = plt.subplots(2,1)

#configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
color = 'silver'
axs[0].grid()
axs[0].set_xlabel("Tempo", color=color)
axs[0].set_ylabel("Segnale", color=color)
axs[0].plot(time, x, color=color)

#clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()

color = 'tab:red'
ax2.grid()
ax2.plot(t,dsrms)
ax2.set_xlabel("Tempo", color=color)
ax2.set_ylabel("RMS")

#configuro il grafico della seconda finestra
color = 'tab:blue'
axs[1].grid()
axs[1].plot(t,dsx)
axs[1].set_xlabel("Tempo")
axs[1].set_ylabel("Spectral Flux")

plt.show()
```

## Per l'esame

Ci studieremo un descrittore fra quelli che non abbiamo visto fino ad ora.

Studiarsi un descrittore da soli, e poi fare un'analisi comparata tra due strumenti diversi o lo stesso strumento con modalità di eccitazione diverse, o strumento reale e strumento sintetico.

Realizzare una tesina che descrive il lavoro e lo studio fatto.

## Bibliografia del corso

- Alexander Lerch, su cui troviamo dal capitolo 3 una presentazione dei descrittori suddivisi tra audio pre-processing, ovvero delle feauture istantanee, ovvero descrittori che fanno la prima analisi del materiale, ció é usato dai descrittori di livello più alto che sono affrontati successivamente, tutto il libro é orientato agli algoritmi automatici, e nel capitolo 3.5 abbiamo la descrizione di algoritmi usati nella fase di apprendimento(come la Feature Dimensionality Reduction -> quando ho tanti descrittori, c'é la possibilità che io stia sovracampionando lo spazio delle fasi del segnale, e quindi avrò che la descrizione sarà ridondante), anche capitolo 2 e 4 da vedere
- tesi di Park
- libro sull'MPEG7

## Altri descrittori

Descrittori come gli MFCC sono molto potenti ma danno un forte indirizzo sulla distinguibilità dei segnali

## Domanda per la prossima lezione

Dove in Italia si fa Feature extraction e si studia e si fa ricerca nel campo da noi affrontato?
