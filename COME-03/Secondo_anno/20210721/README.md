# Appunti della lezione di mercoledì 21 luglio 2021

Per acustica:
- fonometro
- microfono per misurazione

## Cosa portare all'esame

Esercizio simile a ciò che realizza un computer nel caso del riconoscimento del timbro, ma condotto in modo umano, con un'analisi del timbro di uno strumento condotta con piú descrittori, individuando le carattaristiche peculiari di uno strumento.

Quali sono i descrittori e i valori di descrittori che consentono di individuare lo strumento con una certa efficacia.

Capire ed intendere le tecniche di analisi automatica del timbro.

## Le puntate precedenti

Avevamo realizzato un primo script per calcolare la matrice STFT e ne visualizzavamo lo spettrogramma, vedendo come il vettore del campione del segnale viene fatto a fettine ed ogni fettina costituisce una frame di analisi, con risoluzione piú o meno grande in frequenza e tempo.

Le strutture dati con cui stiamo lavorando sono quindi due matrici:
- matrice nel dominio del tempo
- matrice nel dominio della frequenza

Esse hanno come riga la frame i-esima:
- per il tempo -> i campioni
- per la frequenza -> una frame

Esse hanno come colonne la frame i-esima:
- per il tempo -> l'indice dei campioni all'interno della frame
- per la frequenza -> le colonne sono i bin, ovvero la banda di frequenza che rappresenta la risoluzione in frequenza dell'analisi STFT

Alla base di tutto ció c'è il principio di indeterminazione.

Lo spettrogramma è la visualizzazione dei moduli degli elementi della matrice frequenze.

_Esempio di analisi del segnale di un parlato_

Per studiare la tempo-varianza di un parlato, facendo speech recognition mi servirà una risoluzione temporale che mi consente di riconoscere correttamente tutti i fonemi.

In genere nei processi di analisi(anche SPEAR lo realizza), si può cambiare la risoluzione di analisi nel tempo, realizzando una risoluzione adattiva.

A meno di adottare dei sistemi di analisi multi-risoluzione che realizzano un'analisi a diverse risoluzioni contemporaneamente.

[Campioni di strumenti acustici](http://theremin.music.uiowa.edu/MIS.html)

Avevamo poi realizzato uno script realitivo a un descrittore temporale ovvero quello che realizzava i dB RMS.

>Bisogna essere attenti agli artefatti senza farsi ingannare da pattern strani.

## Brightness o centroide spettrale o baricentro spettrale

Esso è uno dei descrittori piú usati nella MIR.

La _brightness_ è un descrittore scalare calcolato nel dominio della frequenza.

Esso è quindi espresso attraverso un solo numero.

Per ogni frame di analisi calcoleremo un solo numero che ci indica questa specifica proprietà del segnale.

### Cos'è da un punto di vista intuitivo?

Essa è il baricentro dello spettro.

Essa è una centroide spettrale, ovvero nel dominio della frequenza(da non confondere con la centroide temporale e quindi calcolata nel dominio del tempo).

La brightness è invece un descrittore scalare nel dominio della frequenza.

#### La centroide spettrale

Avendo una matrice della STFT che ha 1024 righe e 3500 frame di analisi, avremo per ogni frame di analisi(ovvero le colonne della matrice), un valore.

Avremo come vettore della brightness un oggetto che avrà 1 riga e 3500 colonne:

![bri](bri.png)

Avendo quindi un rettangolo alto R e largo L, se dovessimo tenere in equilibro il rettangolo, su un dito, terremmo in equilibro sul dito il rettangolo nel punto medio.

![re](re.png)

Per un triangolo per metterlo in equilibro avremo il punto cosí descritto:
![tr](tr.png)

Pensanso che il rettangolo sia un white noise, il suo centro di massa sia la metà, mentre per un spettro triangolare avremo che esso avrà centro di massa spostato.

Per una figura piú complessa è calcolabile un centro di massa o baricentro :

![se](se.png)

Con la formula descriviamo come calcolare il baricentro:
![f](f.png)

k è l'indice del bin,  x[k] è il modulo di quell'indice del modulo della STFT.

Avremo quindi N elementi della frame, di cui ne facciamo la somma, sopra moltiplicata per k e sotto invece no:
![sp](sp.png)

Ciò da un punto di vista dimensionale fa ottenere un indice di bin, che potrebbe essere anche frazionario, ma ció non ci interessa perchè stiamo calcolando la frequenza che corrisponde al baricentro spettrale.

Ció corrisponderà avendo una rappresentazione dello spettro per mezzo di rettangoli, moltiplicando l'altezza dei rettangoli per l'indice e poi dividerli per la somma di tutte le altezze dei rettangoli, realizzando una media pesata. Essa è pesata con l'indice dei bin, ed essa ci serve pesata per indicare l'indice preciso in cui individuare il baricentro.

![see](see.png)

Nel caso di un rumore avremo invece:

![ru](ru.png)

E quindi:

![rrr](rrr.png)

La somma dei primi N numeri diviso N.

Quindi per sapere quanto vale la somma dei primi N numeri, con la formula di Gauss.

Tutto ciò va diviso per N, rimanendo poi (N+1)/2, ed il baricentro di un rettangolo uniforme sarà a metà della sua larghezza.

![sem](sem.png)

>Da un punto di vista percettivo si dice se l'energia è spostata verso destra o verso sinistra.

L'indice del bin di fatto rappresenta quindi una banda e scegliendo un frame di 1024 campioni, un bin rappresenterà  44100/1024.

#### Il centroide temporale

Essa è calcolata sui campioni del tempo, ovvero mi calcola per vedere se la centroide è individuata su destra o sinsitra della frame, essa permette di individuare le fasi di attacco.

In una fase di sustain i campioni sono simili e il centroide è al centro.

Spostato verso destra, i campioni stanno crescendo, quindi in fase di attacco; verso sinistra i campioni decrescono e siamo quindi in fase di release.

Se abbiamo un centroide al centro siamo in una fase di sostegno.

### Esempio di applicazione della ricerca del centroide spettrales

```
import numpy as np
from scipy import signal
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

#leggiamo un file audio(file audio mono)
#[fs, x] = read("daniel.wav")
[fs, x] = read("oboe.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcoliamo la matrice stft
f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

#mi calcolo il centroide spettrale
specCent = pyACA.FeatureSpectralCentroid(np.abs(STFT),fs)
print("dimensioni del vettore del centroide spettrale: ",specCent.shape)

#plottiamo il tutto
plt.subplot(2,1,1)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.plot(x)

plt.subplot(2,1,2)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Centroide spettrale')
plt.plot(t, specCent)

plt.show()
```

![vi](vi.png)

Se il suono è fermo e stabile, sia come pitch che come timboro a sua volta il suono rimane stabile.

Visualizzando invece che l'oboe, il parlato avremo:

![vi2](vi2.png)

Osserviamo che un file di parlato, e con situazioni di silenzio abbiamo il rumore di fondo del segnale e con lo zero teoricamente avremmo il `nan`, in questo caso abbiamo invece che la centroide spettrale schizza verso l'alto.

Il fonema finale è una s e corrisponde al valore finale del centroide spettrale che va verso l'alto.

In tutta l'ultima frase vi è un'alternanza fra consonanti e vocali, e l'oscillazione della brightness da un'indicazione dell'oscillazione fra consonanti e vocali.

Le vocali sono componenti armoniche bilanciate verso il basso.

Le consonanti quando arrivano fanno registrare un picco della brightness, poichè esse sono componenti rumorose che sono filtrate attraverso i risuonatori vocali.

Quindi già facendo una discriminazione di soglia sul valore della brightness, essa la posso usare per capire quali frammenti del parlato sono rumore e quali sono vocali.

#### riepilogo
In questo file abbiamo calcolato la matrice STFT e poi l'abbiamo mandata in pasto al metodo `FeatureSpectralCentroid()`.

E calcolo la feature piú semplicemente in questo modo:
```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcolo del descrittore
[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale

#plottiamo
plt.plot(t,dcs)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.show()
```

### Domande su ciò che abbiamo fatto dal docente
![o](o.png)

1. Come mai nella fase di fine il centroide cresce per l'oboe?


### Altri due descrittori

Riprodurre lo stesso tipo di istruzione qua descritto usando altri due descrittori:
- spectral flattness
- spectral flux

Nel caso di strumenti che possiamo confrontare a nostro piacimento:
- violino
- pianoforte

Strumenti con caratteristiche di inviluppo diverse, questi due nuovi descrittori danno indicazioni sul tempo.

### Descrittore flattness spettrale

Esso è il rapporto tra media geometrica e media aritmetica, esso è un descrittore scalare.

Esso comporta che facendo la media geometrica ovvero sui prodotti fratto la media aritmetica, ovvero con le somme.

Invece della somma o della divisione si usano prodotto e radice...

Intutivamente ciò significa che se c'è qulalche bin che vale zero, tutto il prodotto della media geometrica vale zero.

Se ho quindi uno spettro rigato con sinusoidi o somma di sinusoidi, avrò una piattezza spettrale pari a zero.

>Questo descrittore restituisce un valore che descrive se lo spettro è piú simile a una sintesi additiva o a un rumore.

Nel rumore bianco tutti i bin hanno un valore diverso da zero.

Nel caso del rumore bianco la media geometrica e aritmetica coincidono, e vale 1.

>Avremmo quindi un valore che identifica se un suono è fatto di parziali o è un rumore.

Questo descrittore serve per la discriminazione tra parte vocalica e parte consonantica.

In generale esso lo posso usare per distinguere le parti di attacco di un suono, poichè le parti di attacco sono carattarizzate da uno spettro denso o complesso.

### Descrittore SpectralFlux

Il flusso spettrale è un indicatore della differenza tra lo spettro di una frame e lo spettro di una frame successiva e da un'indicazione di quanto varia lo spettro tra una frame ed un'altra.

Ad esempio il flusso spettrale di una nota lunga di oboe abbiamo variabilità del flusso pari a zero.

Mentre applicandolo al parlato avremmo una differenza di flusso ampia.

**Quale range è posto per questo descrittore.**

_Provare a tirar fuori finestre grafiche per visualizzare contemporaneamente i descrittori_.

___
Si possono visualizzare piú descrittori in un unico grafico stando attenti alle scale...

La flattness e il centroide spettrale ad esempio oscillano su scale diverse e non si vedono bene...

Il consiglio è usare i subplot.

___

## Per approfonidre
Per approfondire nella parte di WebEx nella parte dei contenuti vi è la tesi di dottorato di Park a Princeton, abbiamo una definizione matematica di tutti i descrittori con il loro significato e come vengono realizzati.

Altro file dal libro di Alexender Lerch.
