# Appunti della lezione di lunedí 5 luglio 2021

Indice stringato su PBworks e appunti completi su gitlab.
___

## Nell'episodio precedente...
Avevamo parlato di cosa volesse dire estrarre dei descrittori nel dominio del tempo e nel dominio della frequenza, ed avevamo visto il tema della risoluzione dell'analisi nel dominio tempo-frequenza.

E se possibile cercheremo di vedere tecniche di analisi multi-risoluzione come Haar, Gabor ed in generale wavelet.

Abbiamo quindi capito i limiti della STFT ed abbiamo realizzato il primo esempio del calcolo di uno spettrogramma di un file audio.

>Il tipo di analisi che useremo per ricavarci dei descrittori si basa sulla segmentazione del file audio in finestre, chiamate _frame_, ciò porta a definire due matrici:
- una nel dominio del tempo
- l'altra nel dominio della frequenza

Esse fanno si di avere descrittori nel dominio del tempo e descrittori nel dominio della frequenza(dominio di Fourier).

Usiamo delle matrici perchè quando facciamo a fettine il segnale, ricostruiamo il segnale non piú in una struttura dati lineare come un array, ma riogranizziamo il segnale con:
- righe in cui troviamo i frame
- nelle colonne troviamo i time stamps -> inizio o punto iniziale di ogni frame

>Avendo una frame con una durata di 20ms, avremo su ogni riga diversi time stamp nel dominio del tempo...


Nel dominio della frequenza avremo un analoga struttura ma con DFT di ogni finestra analizzata in ogni spazio della matrice.

>Nel calcolo della DFT si inviluppa la finestra per una funzione per evitare ringing ai bordi della finestra(nel caso fosse quadrata); inoltro non si realizzano finestre secche per tagliare il segnale ma avremo tagli di porzioni delle finestre di circa il 50% delle finestre.

Dal punto di vista matematico questo tipo di taglio con finestre sovrapposte si puó ricavare una ricostruzione del segnale sotto certe condizioni, con _Overlap and add_.

## Descrittore nel dominio del tempo: RMS

Ricavare la matrice delle frame temporali e da essa ricavarne gli elementi RMS di un segnale.

>Ciò ci è utile per capire come costruire la matrice delle frame temporali e per capire la libreria che useremo in seguito...

Tutte queste cose potremmo infatti scriverle a mano, ma abbiamo anche una libreria già pronta o comunque per implementare da zero si potrà utilizzare il corso di Xavier Serra su Coursera.

Useremo noi delle funzioni della libreria `signal` di `scipy` che ci fornisce la STFT già pronta.

Ora useremo un'altra libreria che si trova in un altro luogo e che fa riferimento a [https://www.audiocontentanalysis.org/](https://www.audiocontentanalysis.org/) corrispondente anche ad un [libro](https://www.wiley.com/WileyCDA/WileyTitle/productCd-111826682X,descCd-buy.html) che parte dai descrittori di basso livello ed arriva ai descrittori di alto livello.

[Repository](https://github.com/alexanderlerch/pyACA) del codice e script di python implementati da Wiley.

## Realizzazione dello script RMS

Decisa la grandezza della finestra e l'hopSize, avrò sempre un avanzo alla fine perchè alcuni campioni ce li perderemo...

La versione in `pyACA` taglia brutalmente la parte finale del file, per evitare il taglio della parte finale del file, realizziamo uno zero padding del segnale alla fine per far si che quando lo tagliamo a fettine comprendendo anche il segnale utile alla fine, senza avere tagli artificiosi...

Reaizziamo uno zero padding sempre quando si ricava la matrice delle frame frequenziali, quando abbiamo frame che non ha potenze di due, possiamo aggiustarci la frame con numero di campioni che non è una potenza di due; capisco quale sia la potenza di due immediatamente piú grande e aggiungo un buffer della grandezza necessaria.

>Modo in cui il campione viene copiato nella frame di analisi è elaborato e bisogna spaccare il campione in modi diversi per avere la coerenza di fase.

Nel nostro caso non ci ricaviamo la matrice delle frame frequenziali ma delle frame temporali...

La funzione per il padding in `numpy` è `.pad()`...

la proprietà `.shape[0]` restituisce il numero di elementi di una matrice, e quindi avremo il numero di righe.

```
import numpy as np
import sys
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
from funzioni import rms

#leggiamo un file audio(file audio mono)
[fs, x] = read("daniel.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#generiamo la matrice dei frame temporali(segmentazione del campione nel dominio del tempo)
#lunghezza della frame di analisi in campioni
frameLength = 1000
#hopSize in campioni -> di quanto ci spostiamo in ogni finestra di analisi, mentre l'overlap e' il complemento dell'hopSize
hopSize = 500

#zero padding alla fine del file(non necessario in questo caso)
x = np.pad(x, (0,frameLength), 'constant', constant_values=(0,0))
print("\ndimensione del file audio post padding: ", np.size(x))

#costruzione della matrice delle frame temporali
#(array, dimensione, hopSize)
xFrames = pyACA.ToolBlockAudio(x, frameLength, hopSize)
#numero di frame di analisi
nFrames = xFrames.shape[0]
print("\ndimensioni della matrice delle frame temporali: ", xFrames.shape)

#costruzione della base tempi(esso e' un array con istanti di tempo corrispondenti alle varie frame di analisi)
#per avere i valori dell'asse x in unita' temporali
t = (np.arange(0, nFrames)*hopSize + (frameLength/2))/fs

#estrarre l'RMS
RMS = rms(xFrames)
#RMS =20*np.log(rms(xFrames))

print("\ndimensioni del vettore RMS: ", RMS.shape)

#plot
plt.subplot(2,1,1)#ci interessa il primo subplot dove mettiamo il segnale
plt.grid()
plt.plot(x)
plt.ylabel('segnale')
plt.xlabel('Tempo')

plt.subplot(2,1,2)#ci interessa il secondo subplot dove mettiamo il valore RMS
plt.grid()
plt.plot(t, RMS)
plt.ylabel('RMS')
plt.xlabel('Tempo')

plt.show()
```

Abbiamo ricavato un descrittore scalare temporale.

![rms](rms.png)

Vi sono anche descrittori vettoriali come lo spettrogramma che per ogni analisi mi ricavo una serie di elemnti che sono i moduli della DFT.

Altri descrittori vettoriali sono ad esempio quelli ricavati da una DFT e poi il calcolo delle parziali di un segnale oltre una certa soglia, per ogni frame si avrà un certo numero di parziali con la loro posizione e la loro ampiezza, avremmo quindi un certo numero di descrittori di parziali e vettoriali.

>La differenza fra i descrittori scalari e vettoriale, è che per quelli vettoriali si deve usare un diagramma waterfall o dei colori.

## Cosa fare con le due matrici? Come usare i vari descrtittori?

Suppore di avere la descrizione del segnale in diversi tipi di descrittori.

Supponendo di avere un segnale diviso in due matrici, come il dominio del tempo e della frequenza e da queste matrici ci possiamo ricavare dei descrittori.

Facendo ciò possiamo avere dei vettori enormi con 200 dimensioni che descrivono il segnale interno in termini delle 200 variabili come descrittori o pezzi di descrittori.

>Un elemento a due dimensioni è un punto nel piano, un elemento in 200 dimensioni è molto difficile da rappresentare.

Immaginando che un timbro ed una nota suonata da uno strumento equivale ad una successione di punti in uno spazio a 200 dimensioni.

Rappresentando idealmente questa successione di punti potrei quindi definire una traiettoria.

Potrò quindi in qualche modo ricondurre in maniera univoca una traiettoria a quel tipo di strumento e timbro in un modo specifico, e abbia io un qualche modo di riconoscere quel punto.

>Il computer potrebbe essere in grado di riconoscere la successione di punti descritti in uno spazio generalizzato...

Posso definire la distanza fra due punti in uno spazio in quante dimensioni voglio.

Potrò quindi analizzare una nuova traiettoria cercando di capire cosa sia...

Potrò avere un riconoscimento del timbro descritto in percentuale...

Tutto ció che è in mezzo tra il riconoscimento del timbro e tutto ciò che si descritto da parte nostra; questa sezione del Machine Learning si chiama "classificazione".

Dopo aver dato una serie campioni abbiamo una macchina in grado di classificare...

>Prendere una singola frame di un suono non ha senso, ma dovremmo avere una evoluzione del suono che da caratteristiche ad un timbro e lo identifica.

Noi non sappiamo di fatto quali sono i descrittori che usa il nostro sistema cognitivo.

>Ora si capisce perchè il timbro è la proprietà piú complessa che si possa descrivere, poichè è un insieme di proprietà cognitive che si riferiscono alla percezione del timbro.

Fin da piccoli realizziamo un apprendimento che lega delle mappe cognitive.

>Ascolto di uno strumento come scarica elettrica nel tempo con mappe spaziali che si immagazzinano nella nostra storia, avendo un confronto velocissimo e molto efficiente con tutto il catalogo di mappe nella nostra testa.

Il Machine Learning e le Intelligenze Artificiali cercano di ricostruire un modello di timbro che cercano di imitare i descrittori che abbiamo cablati dentro di noi.

Il fatto che i descrittori funzionino sta nel fatto che devo scegliere come ricavare i descrittori.

Quali sono i descrittori che devo scegliere?
Quali segnali scelgo?
Quali algoritmi scelgo?
Etc...

Abbiamo tante domande da porci per capire i descrittori quali devono essere e come apprendere le macchine.

## Descrittori di piú alto livello

Quando descriviamo un genere musicale, lo descriviamo in base a dei descrittori precisi.

La MIR cerca di definire descrittori di livelli diversi per arrivare a realizzare analizzatori di descrizione di piú alto livello.

### Il problema della "corenza dello stile"

Cercare di variare in qualcosa che è una costante ovvero lo stile.

Già quando si ha una musica tonale del 600 con uno stile e si ha ogni autore che si muove in un certo set di regole.

Avremmo quindi una pluralità di stili molto diversi...

>Se riuscissi ad accumulare molti descrittori che mi danno molte informazioni...

Se abbiamo molte informazioni e cosa mi interessa, allora se voglio costruire una composizione basandomi sulle informazioni accumulate...

>L'intelligenza, la capacità e la probabilità di riconoscere la bellezza non dipende da diagrammi cartesiani ma ciò che è cablato nella mente e nel corpo.

___

>Cosa vuol dire godere per un'opera d'arte...

[Neuroscienze e arte, articolo consigliato.](https://www.wired.it/scienza/lab/2021/06/25/neuroscienze-emozioni-arte-gallese-spoleto/)

>Tutto ciò che sta venendo fuori è che muovendoci stiamo pensando, che ci servono proprio come elementi di astrazione.

Analizzare i sistemi complessi e cosa sono, puó avvicinarci a capire come funziona il sistema umano...

**Sistemi esperti** termine utilizzato prima per parlare delle intelligenze artificiali...
