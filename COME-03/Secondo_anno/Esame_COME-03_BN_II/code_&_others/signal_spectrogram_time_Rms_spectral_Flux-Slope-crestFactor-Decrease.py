import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pyACA
import os

def save(path, ext='png', close=True, verbose=True):

    # Estraiamo la directory ed il nome del file dal path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '.'

    # Se la directory non esiste la creiamo
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Percorso finale dove salveremo i grafici
    savepath = os.path.join(directory, filename)

    if verbose:
        print("Saving figure to '%s'..." % savepath),

    # Salviamo il grafico
    plt.savefig(savepath, bbox_inches='tight')

    if close:
        plt.close()

    if verbose:
        print("Done")
################################################################################

def plotter(file_name):
    # Leggiamo un file audio(file audio mono)
    my_path = (file_name)
    [fs, x] = pyACA.ToolReadAudio(my_path)

    # Normalizziamo il file audio(a 0 dB)
    x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

    time = np.arange(0,np.size(x))/fs

    # Calcolo dei descrittori
    # RMS
    [dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=1024, iHopLength=512)
    # Spectral flux
    [dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)
    # Spectral slope
    [dss, t] = pyACA.computeFeature("SpectralSlope",x,fs,iBlockLength=1024, iHopLength=512)
    # Spectral crest factor
    [dscf, t] = pyACA.computeFeature("SpectralCrestFactor",x,fs,iBlockLength=1024, iHopLength=512)
    # Spectral crest decrease
    [dsd, t] = pyACA.computeFeature("SpectralDecrease",x,fs,iBlockLength=1024, iHopLength=512)

    # Realizziamo il plot

    # Ricaviamo gli handler della chiamata a subplots(6 righe, una colonna),
    # 1920x1080 pixel con asse x condiviso per allineare i grafici
    fig, axs = plt.subplots(6,1,figsize=(19.20, 10.8), sharex=True)

    # Configuriamo il grafico della prima finestra
    color = 'silver'
    axs[0].grid()
    axs[0].set_xlabel("Time", color=color)
    axs[0].set_ylabel("Signal", color=color)
    axs[0].plot(time, x, color=color)

    # Sovrapponiamo il grafico RMS a quello della forma d'onda
    # nella stessa finestra
    ax2 = axs[0].twinx()

    color = 'tab:blue'
    ax2.grid()
    ax2.plot(t,dsrms)
    ax2.set_ylabel("RMS",color=color)

    # Configuriamo il grafico della seconda finestra
    axs[1].grid()
    axs[1].specgram(x, NFFT=2048, Fs=fs, noverlap=1024, cmap='jet_r')
    axs[1].set_ylabel("Spectrogram")

    # Configuriamo il grafico della terza finestra
    color = 'tab:red'
    axs[2].grid()
    axs[2].plot(t,dsx, color=color)
    axs[2].set_ylabel("Spectral Flux",color=color)

    # Configuriamo il grafico della quarta finestra
    color = 'tab:cyan'
    axs[3].grid()
    axs[3].plot(t, dsd, color=color)
    axs[3].set_ylabel("Spectral Decrease", color=color)

    # Configuriamo il grafico della quinta finestra
    color = 'tab:orange'
    axs[4].grid()
    axs[4].plot(t,dss, color=color)
    axs[4].set_ylabel("Spectral Slope", color=color)

    # Configuriamo il grafico della sesta finestra
    color = 'tab:green'
    axs[5].grid()
    axs[5].plot(t, dscf, color=color)
    axs[5].set_ylabel("Spectral Crest Factor", color=color)

    path_for_image = my_path.replace(".wav","")

    # Invochiamo la funzione di salvataggio
    save(path_for_image, 'jpeg')

################################################################################

# Inseriamo la directory dei files di cui realizzare il grafico dei descrittori
dirname = '/Users/davide/gitlab/SMERM/BN-Tedesco/COME-03/Secondo_anno/Esame_COME-03_BN_II/spectral_descriptors_pyACA'

# Estensione dei file che useremo per l'iterazione
ext = ('.wav')

# Iteriamo su tutti i files con l'estensione data
for files in os.listdir(dirname):
    if files.endswith(ext):
        plotter(files)
    else:
        continue
