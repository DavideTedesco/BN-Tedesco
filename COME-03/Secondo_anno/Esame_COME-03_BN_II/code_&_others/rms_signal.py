import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "Clavicembalo_A2_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#RMS
[dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

#mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
fig, axs = plt.subplots(2,1,figsize=(12.8, 8), sharex=True)

#configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
color = 'silver'
axs[0].grid()
#axs[0].set_xlabel("Tempo", color=color)
axs[0].set_ylabel("Segnale", color=color)
axs[0].plot(time, x, color=color)

#clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
#ax2 = axs[0].twinx()

color = 'tab:blue'
axs[1].grid()
axs[1].plot(t,dsrms)
axs[1].set_xlabel("Tempo", color=color)
axs[1].set_ylabel("RMS[dB]",color=color)

plt.show()
