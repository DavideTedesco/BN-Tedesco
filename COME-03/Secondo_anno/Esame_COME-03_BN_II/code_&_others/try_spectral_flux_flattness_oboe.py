import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "oboe.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=256)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=256)

#plottiamo

plt.subplot(3,1,1) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(time,x)
plt.xlabel("Tempo")
plt.ylabel("Segnale")

plt.subplot(3,1,2) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t, np.log(dsf))
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")

plt.subplot(3,1,3) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t,dsx)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flux")

plt.show()
