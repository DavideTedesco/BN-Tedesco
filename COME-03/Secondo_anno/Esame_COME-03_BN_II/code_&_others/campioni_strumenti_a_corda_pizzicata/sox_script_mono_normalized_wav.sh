#!/bin/sh

#  xxx-script.sh
#
# use it by terminal typing:
# bash xxx-script.sh
for i in *.wav
do
  sox "$i" -c 1 --norm=-0.1 "$i"
done
