import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pyACA
import os

#from https://gist.github.com/jhamrick/5320734
def save(path, ext='png', close=True, verbose=True):

    # Extract the directory and filename from the given path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '../graphs'

    # If the directory does not exist, create it
    if not os.path.exists(directory):
        os.makedirs(directory)

    # The final path to save to
    savepath = os.path.join(directory, filename)

    if verbose:
        print("Saving figure to '%s'..." % savepath),

    # Actually save the figure
    plt.savefig(savepath, bbox_inches='tight')

    # Close it
    if close:
        plt.close()

    if verbose:
        print("Done")
################################################################################

def plotter(file_name):
    #leggiamo un file audio(file audio mono)
    my_path = (file_name)
    [fs, x] = pyACA.ToolReadAudio(my_path)

    #normalizziamo il file audio(a 0 dB)
    x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

    time = np.arange(0,np.size(x))/fs

    #calcolo del descrittore
    #RMS
    [dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=1024, iHopLength=512)
    #spectral flux
    [dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)
    #spectral slope
    [dss, t] = pyACA.computeFeature("SpectralSlope",x,fs,iBlockLength=1024, iHopLength=512)
    #spectral crest factor
    [dscf, t] = pyACA.computeFeature("SpectralCrestFactor",x,fs,iBlockLength=1024, iHopLength=512)
    #spectral crest decrease
    [dsd, t] = pyACA.computeFeature("SpectralDecrease",x,fs,iBlockLength=1024, iHopLength=512)

    #plottiamo

    #mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
    fig, axs = plt.subplots(6,1,figsize=(19.20, 10.8), sharex=True)

    #configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
    color = 'silver'
    axs[0].grid()
    axs[0].set_xlabel("Time", color=color)
    axs[0].set_ylabel("Signal", color=color)
    axs[0].plot(time, x, color=color)

    #clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
    ax2 = axs[0].twinx()

    color = 'tab:blue'
    ax2.grid()
    ax2.plot(t,dsrms)
    #ax2.set_xlabel("Tempo", color=color)
    ax2.set_ylabel("RMS",color=color)

    #configuro il grafico della seconda finestra
    axs[1].grid()
    axs[1].specgram(x, NFFT=2048, Fs=fs, noverlap=1024, cmap='jet_r')
    #axs[1].set_xlabel("Tempo",color=color)
    axs[1].set_ylabel("Spectrogram")

    #configuro il grafico della seconda finestra
    color = 'tab:red'
    axs[2].grid()
    axs[2].plot(t,dsx, color=color)
    #axs[1].set_xlabel("Tempo",color=color)
    axs[2].set_ylabel("Spectral Flux",color=color)



    #configuro il grafico della quarta finestra
    color = 'tab:cyan'
    axs[3].grid()
    axs[3].plot(t, dsd, color=color)
    #axs[1].set_xlabel("Tempo",color=color)
    axs[3].set_ylabel("Spectral Decrease", color=color)

    #configuro il grafico della terza finestra
    color = 'tab:orange'
    axs[4].grid()
    axs[4].plot(t,dss, color=color)
    #axs[1].set_xlabel("Tempo",color=color)
    axs[4].set_ylabel("Spectral Slope", color=color)

    #configuro il grafico della quarta finestra
    color = 'tab:green'
    axs[5].grid()
    axs[5].plot(t, dscf, color=color)
    #axs[1].set_xlabel("Tempo",color=color)
    axs[5].set_ylabel("Spectral Crest Factor", color=color)

    path_for_image = my_path.replace(".wav","")

    save(path_for_image, 'svg')

################################################################################

#from https://www.geeksforgeeks.org/python-loop-through-files-of-certain-extensions/
# giving directory name
dirname = '/Users/davide/gitlab/SMERM/BN-Tedesco/COME-03/Secondo_anno/Esame_COME-03_BN_II/spectral_descriptors_pyACA'

# giving file extension
ext = ('.wav')

# iterating over all files
for files in os.listdir(dirname):
    if files.endswith(ext):
        plotter(files)
    else:
        continue
