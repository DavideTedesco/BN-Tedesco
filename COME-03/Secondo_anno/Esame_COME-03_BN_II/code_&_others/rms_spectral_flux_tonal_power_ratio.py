import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "Clavicembalo_A2_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#RMS
[dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)
#spectral slope
[dss, t] = pyACA.computeFeature("SpectralSlope",x,fs,iBlockLength=1024, iHopLength=512)
#spectral crest factor
[dssp, t] = pyACA.computeFeature("SpectralSpread",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

#mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
fig, axs = plt.subplots(4,1,figsize=(12.8, 8))

#configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
color = 'silver'
axs[0].grid()
axs[0].set_xlabel("Tempo", color=color)
axs[0].set_ylabel("Segnale", color=color)
axs[0].plot(time, x, color=color)

#clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()

color = 'tab:blue'
ax2.grid()
ax2.plot(t,dsrms)
#ax2.set_xlabel("Tempo", color=color)
ax2.set_ylabel("RMS",color=color)

#configuro il grafico della seconda finestra
color = 'tab:red'
axs[1].grid()
axs[1].plot(t,dsx, color=color)
#axs[1].set_xlabel("Tempo",color=color)
axs[1].set_ylabel("Spectral Flux",color=color)

#configuro il grafico della terza finestra
color = 'tab:orange'
axs[2].grid()
axs[2].plot(t,dss, color=color)
#axs[1].set_xlabel("Tempo",color=color)
axs[2].set_ylabel("Spectral Slope", color=color)


#configuro il grafico della quarta finestra
color = 'tab:cyan'
axs[3].grid()
axs[3].plot(t, dssp, color=color)
#axs[1].set_xlabel("Tempo",color=color)
axs[3].set_ylabel("Spectral Spread", color=color)

plt.savefig('a.pdf', dpi=500)
