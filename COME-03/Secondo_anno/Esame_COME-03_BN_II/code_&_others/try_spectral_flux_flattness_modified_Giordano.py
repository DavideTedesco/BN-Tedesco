import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

fig, ax0 = plt.subplots() #sovrappore grafici con assi y diversi

#plot del Segnale
color = 'silver'
ax0.set_ylim(-1,1)
ax0.set_xlabel('Tempo')
ax0.set_ylabel('Segnale', color=color)
ax0.plot(time, x, color=color)
ax0.tick_params(axis='y', labelcolor=color)

ax1 = ax0.twinx()  # instantiate a second axes that shares the same x-axis

#plot della Spectral Flatness
color = 'tab:red'
ax1.set_ylim(-6,0)
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Scala SpectralFlatness', color=color)
ax1.plot(t, np.log(dsf), color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

#plot dello Spectral Flux
color = 'tab:blue'
ax2.set_ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dsx, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()
