# Appunti della lezione di Mercoledì 9 Settmbre 2020

## Ripasso

Studio degli spazi acustici da diversi punti di vista e con diversi modelli di studio e analisi, percorrendo i 3 approcci diversi
### Lezione 1
1. analisi ondulatoria o modale -> scrivere le equazioni del campo scrivendo la geometria dello spazio chiuso, considerando le caratteristiche fisiche dei vincoli(coefficenti di assorbimento pareti, assorbimento, scattering) -> con equazioni differenziali si risolve  e si trovano soluzioni dell'equazioni del campo(sempre campi molto semplici)
  - Metodi FEM, analisi agli elementi finiti, scomporre struttura che racchiude il campo acustico, in una struttura tassellata, risolvendo puntualmente l'equazione del campo con metodi di analisi numerica, considerando la presenza dei vincoli
  - modi naturali
2. Approccio geometrico, considerare il campo acustico come se fosse un campo ottico, che deriva da studio dei sistemi di analisi e diffusione della luce, vedendo i limiti di questo tipo di approccio, che ha senso quando le distanze in gioco sono piú grandi della piú grande lunghezza d'onda del campo, molto spesso funziona un approccio ibrido (alcuni software usano questo approccio ibridio-> per la proggettazione di spazi)
3. Campo diffuso, considerando il campo come se fosse una specie di gas ergotico con stesse caratteristiche in tutti i punti dello spazio ed ha un'incoerenza distribuita equamente su tutto lo spazio, ovvero non è rilevante l'intervento dei modi naturali dello spazio, lontani dall'ipotesi di trattazione del campo con approccio ondulatorio o modale

### Lezione 2
risoluzione dei modi e esempi su parallelepipedi

Distribuzione dei modi con pattern armonico su parete parallela, in cui il profilo frequenziale non è esattamente impulsivo  e quindi le pareti assorbono una certa quantità di energia, e ciò le fa divantare campane.

Abbiamo poi studiato le tipologie di modi:
- assiali
- tangenziali
- obliqui

Con cammini a 1, due o tre dimensioni

Abbiamo visto la distribuzione dei modi in frequenza, e poi abbiamo notato che dopo un certo tempo le riflessioni e anche i modi naturali assumono una denistà talmente elevata che ha senso studiarli solo con un approccio complessivo(come il tempo di riverbero)

Abbiamo poi trovato la larghezza di banda di un modo naturale e il coefficente di smorzamento

### Lezione 3

Abbiamo visto che esiste la frequenza di Schroeder che è lo spartiacque tra l'approccio modale e l'approccio statistico

Abbiamo introdotto l'approccio geometrico vedendo le tecniche per implementare algoritmicamente l'apprroccio geometrico(con raytracing e sorgenti virtuali)

Abbiamo ripassato il concetto della IR, che èla risposta nel tempo che ha un sistema quando in ingresso gli viene dato l'impulso(segnale più unitario), essa ha dentro di se tutte le caratteristiche del sistema analizzato, l'impulso è breve, anche se la risposta sarà piú prolungata. In genere un campione in ingresso, e vari campioni in uscita, con un suono diretto, prime riflessioni e riverberazione diffusa.

Siamo entrati nel dettaglio dell'approccio geometrico implementato e dello scattering, vedendo che il campo acustico interagisce con le irregolarità della parete, ed il campo acustico interagisce con le asperità che sono confrontabili con le lunghezze d'onda paragonabili alle asperità, permettendoci di frammentare il fronte d'onda scegliendo il materiale, per sparpagliare l'energia, con pareti di diffusione(scattering) con superfici irregolari, che intervengono a diverse scale di grandezza (con i pannelli tipo il diffusore di Schroeder). (Vedendo il diagramma di assorbimento)
Abbiamo visto il diagramma di radiazione e diffusione.

### Lezione 4

Abbiamo introdotto l'approccio statistico introducendo delle grandezze che danno una caratterizzazione del campo, con grandezze che hanno a che fare tutte con il campo diffuso.

#### RT60
Il tempo necessario perchè l'SPL di un suono decada di 60dB rispetto al suono iniziale

#### Formula di Sabine

Per prevedere il tempo di riverberazione dello spazio, essa chiama in causa il volume dello spazio e la superficie efficace dello spazio(pesatura pareti con coefficente di assorbimento)

Abbiamo visto il miglioramento della Formula di Sabine. Ovvero la formula di Norris-Eyering, che per coefficenti di assorbimento piú piccoli di uno funziona come la formula di Sabine, mentre quando siamo in campo aperto con coefficenti di assorbimento uguale ad 1, il T60 viene 0.

### Lezione 5

Approfondimento sul progetto Echi d'acqua di Lupone, in cui ci ha spiegato le considerazioni concettuali.

### Lezione 6

#### Il filtraggio dell'aria
Filtraggio dell'aria e la legge della propagazione e sulla diminuzione delle linee di propagazione ed un _problema di incoerenza_, e se dunque prendiamo la formula classica che da l'attenuazione di intensità per propagazione ci troviamo con un assurdo fisico, poichè quando la distanza tende a zero l'intensità tende ad infinito.

Scopo di modellazione per costruire un sitema di sintesi o simulazione di uno spazio che implementa le proprietà di propagazione del campo acustico, per ogni sorgente dovrei attenuare ogni segnale di una certa intensità secondo la legge di propagazione dell'intensità. (Quando R tende a 0 l'intensità tende ad infinito). Quella legge era in realtà approssimata, mentre in tesi è stata calcolata (in maniera piú accurata), poichè quando r tende a zero, l potenza del segnale tende a p/2.

#### Attenuazione dell'aria

Essa si comporta come un passa basso dinamico dipendente dalla distanza dalla sorgente, piú è vicina la sorgente piú bassa sarà la frequenza (filtro ad un polo), con un suo effetto. Calcolando un caso particolare a 6 metri di distanza, abbiamo il filtro in banda audio e ad un km l'effetto diviene ancora piú percepibile.

Motivo per cui da lontano sento le medio gravi è dovuto al filtraggio dell'aria ed alla diffrazione.

I tuoni ad esempio hanno quel suono per via del filtraggio dell'aria.

I lampi si scatenano nel cielo e non essendoci ostacoli in cielo il filtraggio è dovuto all'attenuazione dell'aria, ed il tuono ha quel carattere molto filtrato sulle basse frequenze.

_Abbiamo poi visto le formule di Sabine, Norris etc... corrette con l'assorbimento dell'aria_

#### Tempi di riverberazione

Abbiamo definito altre grandezze per caratterizzare la risposta dello spazio acustico.

#### Norma ISO

per le grandezze e processi di misura di uno spazio acustico, in cui sono defiuniti i processi e le strumentazioni che sono realizzate per essere compliant a questa norma.

Essa è divisa in 3 parti ovvero:
1. spazi performativi
2. spazi ordinari
3. spazi aperti


Abbiamo visto esempi di tempi di riverberazione per differenti tipologie di sala

Le caratteristiche di uno spazio acustico cambiano in funzione della destinazione dello spazio

Una progettazione acustica degli spazi viene in genere destinata ad una destinazione d'uso musicale, mentre ci dovrebbe essere una maggior cura. Anche se la percezione di uno spazio fa parte dell'ergonomia di uno spazio che mi permette di usare e vivere uno spazio in maniera piacevole.

(Teoria delle atmosfere articolo)

In genere in spazi viene effettuata la correzione solamente a posteriori e non in fase di progettazione. Mentre in Europa si stanno affermando degli studi che studiano la progettazione acustica di un luogo.

Alberi vengono utilizzati con risultati molto efficaci per l'assorbimento e diffusione del suono. Diffusore che lavora bene a diverse lunghezze d'onda e quindi puó attenuare diversi meccanismi di diffusione, oltre all'attenuazione del suono.

Lavoro di Beranek (del 1962) che raccolse enorme quantità di dati fisici di sale da concerto e che prese direttori e critici musicali e gli fece mettere voti a determinate caratteristiche per giudicare degli spazi musicali, con l'obiettivo di stabilire una correlazione tra grandezze fisiche e grandezze valutate da questi individui.
Per la godibilità degli spazi acustici.

Quanto è prevedibile il risultato nella creazione di un riverbero digitale?
Nel progettare un filtro che produca una modifica di un segnale.

- progettare spazio
- progettare riverbero (parametri legati alle caratteristiche fisiche spaziali, ma i riverberi ad alti livelli agiscono su parametri che plasmano a basso livello il segnale, senza avere una relazione diretta con lo spazio che genera riverbero)
- realizzazione spazio virtuale (realtà aumentata), in simulatori quello che si sente nelle cuffie dovrebbe idealmente essere una simulazione sonora dello spazio che si vede intorno.

#### RT60

Cosa è e come si ricava, ovvero attraverso le formule previsionali o se io ho tracciato la curva della risposta all'impulso.

Con la curva tracciata logaritmicamente(in dB), e vediamo che essa è abbastanza lineare e la posso ricavare tracciando una retta tra il punto iniziale ed il punto a -60dB della risposta all'impulso, la tangente dell'angolo mi dà il valore dell'RT60.

#### T10, T20, T30

Altri parametri simili all'RT60, ovvero 3 pendenze e profili di decadimento diversi che hanno lo scopo di caratterizzare la risposta all'impulso in momenti diversi,

Ciò perchè una risposta all'impulso uniforme dovrebbe avere T10, T20 e T30 uguali, mentre nella realtà i parametri differsicono fra loro, e quanto differeiscono e quali sono i loro valori danno idea della omogeneità della IR.

#### EDT
Early deacy time, caratterizza la caduta ed il decadimento delle prime riflessioni. Esso si ricava prendendo 2 punti a 0 dB e -10dB e si traccia una linea, se c'è un'uniformità tra prime riflessioni e riposta totale all'impulso.

Se EDT e T60 sono diversi, il comportamento delle prime riflessioni è diverso dalla T60 finale, e viceversa.

Mentre il T60 è abbastanza uniforme calcolato in diversi punti dello spazio, l'EDT varia molto poichè è legato alle prime rilfessioni, ovvero dipende dalla forma dello spazio, e quindi cambia molto da punto a punto dello spazio(come se mi muovessi in punti diversi dello spazio), mentre con lo T60 vedo lo spazio da molto lontano.

### Lezione 7

Grandezze riassunte nella norma ISO,

#### ITDG

Ritardo tra suono diretto e prima riflessione(se vi sono ovviamente i suoni diretti, nel caso non ci sia copertura ottica)

Questo parametro è importante poichè nello studio di Beranek, correlazione stretta tra il parametro e l'_intimacy_, ovvero una percezione di prossimità dello spazio, sentire lo spazio come un qualcosa di vicino e prossimo e non dispersivo.

Se la distanza temporale diviene troppo grande, sembra come se sentissimo echi. Relazione tra suono diretto e prime riflessioni, per una buona relazione mai piú di 35 ms, per il parlato meglio se si è sotto i 20ms (per l'intellegibilità del parlato).

Ritardo calcolato indipendentemente dalla direzione della prima riflessione, il sistema percettivo non identifica la prima riflessione come se venisse da un altro luogo se non da quello da cui il suono diretto viene.

#### Bass ratio & Brilliance

Sono 2 grandezze.

(intorno a 100 Hz)
BR è un rapporto che definisce un bilancio energetico tra tempo di riverberazione a frequenze diverse, un range di valori bilanciati è quello che va da 1,2 a 1,25. Essa è adimensionale.

(intorno a 1000 Hz)
La brilliance è definita similmente alla BR ma in un range frequenziale medio.

A seconda del tipo di materiali, cambieranno i valori di queste due grandezze, in base al materiale delle superfici.

#### Parametri energetici

1. definition (D50)
2. clarity(50 oppure 80)

Molto simili come definizione _definition_ rapporto tra energia utile ed energia totale. L'energia utile è nei primi 50 secondi della risposta all'impulso. Perchè l'energia utile è definita nei primi 50ms? poichè una buona parte dell'energia raccolta nella prima parte della risposta all'impulso, per il parlato c'è una buona definizione.

La _clarity_ è la stessa definizione, ma al denominatore non c'è l'energia totale, ma l'energia dannosa. Ovvero l'energia tra 50 o 80 ms e infinito. Questa grandezza viene usata per caratterizzare la chiarezza dello spazio nel caso del parlato e della musica. (C50 usata nel caso del parlato C80 usata nel caso della musica, poichè nel segnale del parlato la tempo varianza è piú alta, la tempo-varianza del segnale musciale è molto piú piccola, si tollera un livello di intellegibilità piú basso nel caso della musica).

### IACC

Inter aural cross correlation, che è una grandezza che corrella la coerenza dei suoni ricevuti da destra e da sinistra, poichè un segnale fortemente coerente da dx e sx è definito sgradevole, come il segnale mono in cuffia che viene percepito come al centro della destra. Poichè un segnale mono spegne tutti i sistemi di percezione spaziale. Questo parametro viene valutato con delle dummy head, per cui anche architettonicamente, spazi che sono il piú asimmetrici possibili hanno un risultato piú gradevole. Dal punti di vista acustico l'asimmetria non è una caratteristica gradevole. In una sala simmetrica eviterei dunque le sedute centrali.

![c50-c80](c50-c80.png)

(Visto l'auditorium di Piano)
____________________________

Abbiamo introdotto questi parametri poichè sono parametri che sono stati usati per caratterizzare gli spazi acustici e vanno conosciuti poichè ci aiutano a caratterizzare la risposta di uno spazio, poichè sono legati alle qualitè che sono collegate alle qualità cha assegnamo percettivamente a uno spazio. Anche sul campionamento delle caratteristiche percettive è stato fatto ancora poco lavoro. Diverso ad esempio rispetto al campionamento sul volume percepito (curve fletcher e munson).

Queste grandezze ci aiutano a capire la fruibilità di uno spazio acustico e che sono presenti in REW che permette di fare la misurazione di uno spazio, per realizzare la ripresa di uno spazio per analizzare le caratteristiche che il software ci da.
____________________________

Per l'esame stanza da misurare con REW

____________________________

#### Il primo riverbero

Primo riverbero realizzato da Schroeder ai Bell Labs fu con comb e allpass, con una batteria di comb in parallelo con lo scopo di simulare le prime riflessioni (coppia di comb in parallelo come a presentare una coppia di pareti parallele) con valori di amplificazioni diversi(numeri primi) con elementi decorrelati, come se fossero le prime riflessioni; dopodichè se con quel segnale che esce dai comb alimento una serie di allpass, costruisco la risposta diffusa(riverberante) mescolando ciò che viene realizzato dai comb. La risposta dell'allpass serve per il guadagno in ampiezza e mescola molto le fasi. Questa cosa è vera però matematicamento solo per i segnali stazionari(stabili), per i segnali tempovarianti l'allpass suona come un comb, poichè ad esempio il parlato se confrontabile con l'allpass esso lo fa suonare come un comb.
(servono in genere una serie di allpass, poichè uno solo diviene confrontabile con il parlato)
