# Appunti della lezione di Mercoledì 23 Settembre 2020

fine di ogni lezione documento con presenze

## HRTF

![cipic_hrtf_HRTF_med](cipic_hrtf_HRTF_med.png)

Approfondimento su HRTF

L'immagine rappresenta la famiglia di curve di filtraggio che si chiamano HRTF, è un'immagine a 3 assi, con ascissa 3 frequenze, ordinata risposta in dB, ordinata a destra direzione di provenienza della sorgente(approssimativamente).

La gradazione fa riferimento ad angolo di provenienza della sorgente. Le curve mostrano il fenomeno di filtraggio dovuto alla diffrazione, per cui per effetto della diffrazione ed interferenza vi sono attenuazioni o interferenze. Che il suono (incontrando) alcune volute del padiglione. Ci sono cancellazioni dunque.

Quindi il notch che si vede nella prima curva in basso, man mano che la sorgente si muove verso l'alto cresce in frequenza e se guardiamo curve piú in alto, per alcune curve tra _front_ ed _above_ si fonde a 10kHz.

Ricavato da un diagramma _waterfall_ a 3 dimensioni. I valori delle varie curve andrebbero letti dalle varie curve.

Banda implicata è dai 4KHz fino a 16KHz che hanno a che fare con il riconoscere la nostra banda di filtraggio.

Alcuni meccansimi fanno riferimento a lateralità, altri a distanza ed altri ancora alla percezione dello spazio. Alcuni meccanismi hanno influenza in diverse meccanismi come ITDG, ritardo e intensità. Tutti i dati vengono usati insieme per costruire delle mappe confrontate con l'esperienza che danno un esito in termini di comprensione, con la _Auditory scene analysis_.

_____

Metodologia del rinforzo per imparare.

Orecchio assoluto -> associare un nome ad un tipo di percezione. Capacità di attribuire un nome ad un'altezza, è una capacità culturale, e facendo un training molto forte, fai un legame percettivo al nome che si lega ad un suono.

Differente dal linguaggio, di cui impariamo delle regole generative di cui gli output sono potenzialmente infiniti. Dunque l'orecchio assoluto è associare un nome ad un qualcosa di percettivo.

Chiaramente, se voglio costruire un sistema di rendering di una scena auditiva, potrei usare le HRTF, soprattuto per quanto riguarda il piano verticale. Anche in un ambiente di rendering binaurale multicanale(non in cuffia), per indurre una certa sensazione di luogo di provenienza di un suono. Inducendo la sensazione di provenienza di una sorgente.

Quando interpolo e sfrutto le curve delle HRTF, realizzo anche movimenti delle sorgenti nello spazio.

Auralizzazione di un segnale mono su 2 altoparlanti, se il mio orecchio percepisce dei profili di filtraggio con una cosa conosciuta come HRTF, il cervello ricostruisce una scena auditiva anche se si tratta di un qualcosa che ha di per se nessuna informazione spaziale.

Ascolto mono in cuffia e nello spazio ascolto diverso.
_____________
## Materiale da portare per l'esame

Relazione sull'analisi fatta con REW di questo spazio scelto.

Documento Google per annotare e fare la revisione
_____________
## Elaborato

- ripresa di una IR dell'ambiente scelto, da correggere, se essa ha caratteristiche particolari essa diviene piú interessante

- dotazione ripresa, altoparlanti omnidirezionali(S.T.O.N.E.) e microfono da misurazione il piú possibile piatto in frequenza, microfono OM1

- utilizzare 2 speaker fronte-retro con diffusione e senza avere driver allineati

- scheda audio

- Serve un fonometro SPL calibrato oggettivo
_____________

## Panoramica di REW

Documentazione fatta molto bene e molto esauriente

Articolo a cui fa riferimento REW -> segnali logaritmici di sweep come segnali di test per la ripresa della risposta all'impulso. Motivo per cui si usa uno sweep per l'IR è poichè la IR è la risposta di un sistema ad un impulso. Un impulso teorico è una funzione matematica che non esiste. In realtà si usano: colpo di pistola, palloncini, battito su legno...

### Perchè non usiamo questi suoni?

Ovviamente le caratteristiche dell'ambiente riprodotte dall'impulso hanno un tot di energia con che con microfono non estremamente sensibile non vengono riprese.

L'energia di uno sweep è spalmata durante tutta la durata del test, che dura di piú ma mi da complessivamente piú energia, dandomi un rapporto segnale/rumore piú alto(S/N ratio). Piú è alto il rapporto segnale/rumore indicato in dB, migliore è la IR che ottengo. Rapporto tra segnale utile e disturbo(rumore di fondo). Ad altoparlante spento abbiamo un disturbo di fondo che rimane lí (ad esempio 18dB). Se ho un segnale che è a 30 dB, il rapporo S/N R è 12 Db e dovrò avere un segnale di almeno 60/70dB. Per avere tutta quest'energia per tutte le frequenze concentrata in un'istanza di tempo, il suono dell'impulso dovrebbe essere potentissimo. Usando uno sweep posso avere un segnale di test con livello medio di 70dB che sia utilizzabile a livello di misurazione.

Quando non ho piú un impulso devo fare il rapporto tra sweep e segnale in ingresso e deconvolvere. O nel dominio della frequenza rapporto tra FFT segnale in uscita e quella in ingresso.
_____________
### Convoluzione

![applet convoluzione](https://pages.jh.edu/~signals/discreteconv2/index.html)

Convoluzione di qualsiasi impulso con con una funzione è la funzione stessa.

La convoluzione dal punto di vista computazionale è un po' pesante, e dunque il prodotto campione per campione e la somma deiv vari campioni

Funzione che viene fuori dalla convoluzione fra 2 rette.
![convoluzione](convoluzione.png)

I campioni della risposta all'impulso mi danno dunque la funzione. Mi leggo dunque campione per campione la risposta all'impulso di un sistema.
![ir_conv](ir_conv.png)

Se invece di un impulso faccio la convoluzione fatta con un'altra funzione, ottengo una terza funzione e devo fare dunque una deconvoluzione.

È difficile fare l'anti-trasformata di Fourier poichè: anche se X(f) è uguale a zero od è molto piccola ci sono dei problemi di calcolo.

REW registra, prende la ripresa e la deconvolve rispetto alla funzione di sweep.

Questi sono i motivi per cui si usa una funzione di sweep e non viene piú usato l'impulso classico.

_____________
### Set up e misure preventive

- serve un fonometro
- registrare la IR della propria scheda audio in loopback, collegando l'uscita line all'ingresso line usato per la misura e poi


- usare un canale solo -> calibrazione scheda -> salvato file di calibrazione per risposta in frequenza scheda audio, in modo tale da poterle usare per scheda audio

- la risposta della scheda audio è diversa per ogni canale che si sceglie

### Check dei livelli e scelta dei livelli

- segnale generato o su subwoofer o su altoparlante principale -> segnale di test a -12dB -> regolare il segnale di ingresso in modo tale da massimizzare l'energia senza saturare (rapporto S/N)

### Calibrazione e lettura SPL

- utilizzo del fonometro, nel momento in cui leggiamo un valore non abbiamo un riferimento assoluto sul valore di quel segnale, ho dunque bisogno di un riferimento calibrato. Quando si va sul pannello delle preferenze Mic/Meter, abbiamo 2 possibilità:
1. file Calibrazione
2. c weighted spl meter (SPL meter)

### Pronti per la misura

- connettere scheda audio ad altoparlante
- realizzazione di una misura
- importante nella misura è fare un rilievo dello spazio, ovvero le misure dello spazio (metro laser) o piantina dello spazio, poichè qualunque risposta all'impulso dipende dalla posizione della sorgente e dalla posizione del microfono
- come è fatto l'ambiente e come è arredato, con la tipologia di arredamento e strutture di assorbimento
- per ogni risposta all'impulso fatta bisogna segnare la posizione della sorgente e del microfono
- se l'ambiente misurato ha già una predisposizione d'uso
- cosa importante è documentare tutte queste cose

### Simulatore di stanza

- con grandezze della mia stanza avrò un plot della risposta calcolata dello spazio, a quel punto l'informazione mi potrebbe essere utile, poichè se trovo l'enfasi a tot Hz, ho in sostanza quali sono i modi a cui fanno riferimento alla risposta calcolata, capendo quali sono le pareti coinvolte nella generazione di quell'enfasi

- enfasi nelle frequenze gravi, sono modi semplici a uno o al massimo due dimensioni, posso decidere di mettere degli assorbitori di helmoltz, e dunque può aver senso intrappolare le frequenze basse, le lunghezze d'onda in gioco essendo grandi è difficile fare scattering, poichè dovrebbero essere grandi ed avere profili di diffusione molto grandi

_____
- utilizzo della fase -> effetti di controllo delle fasi si possono effettuare sulle frequenze gravi, andando su sulle frequenze gravi

- modi naturali si accendono in qualsiasi caso a meno che non la mettiamo nel nodo ovvero la stiamo azzittendo, facendola lavorare in controfase

- eccitare la stanza in maniera uniforme su tutto lo spettro con subwoofer
