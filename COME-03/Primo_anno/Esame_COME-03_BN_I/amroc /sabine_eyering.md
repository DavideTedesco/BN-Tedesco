Volume: **56.11 m<sup>3</sup>**

Surface: **90.23 m<sup>2</sup>**

Schroederfrequency: **207 Hz**

Critical distance: **0.55 m**

<table> <thead> <tr> <th></th><th>Sabine</th><th>Eyring</th> </tr> </thead> <tbody> <tr> <th nowrap="">Equivalent absorption area needed:</th> <td id="span_ri_sabines_area">15.24 m<sup>2</sup></td> <td id="span_ri_eyring_area">14.02 m<sup>2</sup></td> </tr> <tr> <th nowrap="">Average absorption coefficient needed:</th> <td id="span_ri_sabines_coef">0.17</td> <td id="span_ri_eyring_coef">0.16</td> </tr> </tbody> </table>
