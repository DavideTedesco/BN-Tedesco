# Appunti della lezione di Martedì 23 Febbraio 2021

Avevamo visto le variabili di environment che ci sono nella shell.

`giovanni=23` e poi richiamandola con `echo $giovanni` per visualizzare il valore.

## Variabili d'ambiente

###  PS1
```echo $PS1```

### PATH
Esse sono un elenco di cartelle separate dai 2 punti dove va a cercare i programmi eseguibili.
Possiamo vedere queste cartelle con i comandi:
```echo $PATH```

Questo è il meccanismo fondamentale, per sapere sempre dove sono i programmi.

Facendo `ls`, facciamo partire il programma che lista gli elementi in una directory. Per sapere dove sia il programma `ls` possiamo farlo scrivendo: `type ls`

Possiamo resettare le variabili, facendo una prova salvando il PATH.

```
mypath=$PATH
PATH=""
hash -r
```

Ora non funziona nulla...

Per rimettere apposto facciamo: `export PATH=$mypath`

Dentro le variabili come PATH, non vi è la directory ".", ovvero la directory corrente.

#### Come faccio a crearmi un comando nella directory corrente?

```echo "echo ciao">ciao```

Per farlo divenire veramente un comando, lo dobbiamo rendere eseguibile.

Lo rendiamo cosí eseguibile:
```chmod +x ciao```

Se ora scrivo "ciao", non essendoci il punto nel PATH, devo aggiungercelo per far girare ciao:
```export PATH=$PATH:.```

Adesso guardando il PATH vediamo il "." alla fine.
```echo $PATH```

Dunque ora funziona il `ciao`

##### Perchè il "." non vi è già da prima?

Perchè non è una buona pratica.

Il modo di eseguire un comando che si trova nella cartella corrente è scrivere `./ciao`, ovvero inserire il path assoluto.

Mettiamo quindi il punto esplicitamente, perchè mettiamo la parte che eseguiamo.

Non mettiamo il punto dentro il path, perchè se vogliamo eseguire qualcosa nella cartella corrente, lo si fa esplicitamente.

### $?

Esso serve per ritornare il valore del comando precedente.
```echo $?```

Un programma che esegue in maniera giusta, ritorna 0.

I programmi possono tornare parecchi valori diversi, che possono essere diversi e possono essere interpretati.

### $$

Variabile per vedere il PID corrente
E la uso: `echo PID`

### Variabili già predefinite

Per vedere tutte le variabili che cominciano con una lettera maiuscola
```set | grep "^[A-Z]*="```

### `ls -l`

Permessi di scrittura e lettura su file di una cartella

#### Gestire i permessi

Attraverso il comando `chmod`.

## Dare ad un file piú nomi con il comando "link"

Invece di fare la copia, voglio avere lo stesso file con due nomi diversi, faccio quindi un **link hardware**.

```ln ciao gino```

Ora figurano ciao e gino e abbiamo a tutti gli effetti lo stesso file.

```echo echo daje>>gino```
Ridirigi dentro il file ma appendendo

come vedere i caratteri all'intero del file attraverso: `od -c nomefile`



### Cosa succede se cancello uno dei due?

Funziona ancora uno dei due file linkati, precedentemente.

**Tra le partizioni del disco non si possono fare link hardware**, quindi non si possono fare link hardware di cartelle.

## Link simbolici

Il link non è mai il file, quindi se rimuovo il file, il link esiste sempre...

Ciò è utilissimo in Csound, dove rimane conveniente numerare i suoni e non si vogliono perdere i nomi.
_______
## Come funzionano i file system?

- Il disco da un tera come si indirizza?
- come si gestisce il file system?

Abbiamo molta piú memoria di quanta ne possiamo indirizzare, come si fa?

Si puó dividere il disco in settori ed indirizzare i singoli settori. Quando il disco viene formattato il disco, vengono fatte le boundries di un settore.

Un settore può avere varie dimensioni, i sistemi operativi decidono loro come sono fatti i settori, o su Linux si possono decidere le grandezze dei settori.

### Cosa succede se un file occupa piú di un settore?

![disk](disk.png)

Certi settori svuotandosi, non possono essere piú riempiti, e si deframmenta il disco.

Vi è un modo chiamato **file allocation table**, da dove parte il braccetto a riposo, denominato FAT, in essa c'è:
- il nome del file
- collezione di gruppi di settori dei quali il file fa parte

Per ritrovare le cose se si deframmenta il disco, troviamo le cose a pezzettini.

![disk1](disk1.png)

Nella FAT ci sono le collezioni di indirizzi dei settori di ciascun file e fanno riferimento a dei numeri chiamati "inodes".


_______
## Operatori logici della shell: and e or(&& e ||)

```rm ciao || echo "che vuoi da me?"```

Viene eseguito il secondo comando perchè rm fa ritornare un valore falso.

Invece se `rm ciao && echo "che vuoi da me?"`, non fa nulla perchè "&&" esegue il primo comando se il primo è giusto o se tutti e due sono sbagliati.
```ls && echo "che vuoi da me?"```

## Creiamo uno script in bash

File eseguibile come se fosse un comando.

File che conti il totale dei secondi di file audio in una cartella usando il comando `sndinfo` installato con csound.

![script](script.png)

awk è un linguaggio che fa filtri di testo complessi.

Se volessi usare altri linguaggi dovrei metterci l'hash bang:
```#!bin/bash```

Il comando env fa partire il comando che viene subito dopo, in un environment diverso dall'environment che noi abbiamo.
```#!usr/bin/env bash```

A fare gli scripts ci si mette molto poco e sono molto utili per lavorare
_______
## Terminal History

```
516  giovanni=23
517  echo giovanni
518  echo $giovanni
519  $PS1
520  echo $PS1
521  hostname
522  echo "\h"
523  echo $PATH
524  type ls
525  mypath=$PATH
526  echo mypath
527  echo $mypath
528  echo $/
529  echo $?
530  j
531  echo $?
532  rm ciao
533  echo $?
534  echo $$
535  PID
536  echo PID
537  echo $PID
538  ls
539  echo $SHELLPID
540  set/
541  echo set
542  echo $set
543  set | grep "^[A-Z]+="
544  set | grep "^[A-Z]*="
545  ls -l
546  ls -list
547  sudo ls -l
548  ls -h
549  ls --h
550  ls --help
551  man ls
552  man od
553  ls
554  sndinfo *
555  cd ..
556  ls
557  cd  ../..
558  ls
559  cd BN-Tedesco/
560  ls
561  cd COME-02
562  ls
563  cd Primo_anno/
564  sndfile *
565  sndinfo *
566  date
567  time
568  hour
569  h
570  day
571  date
572  bash --version
573  type bash
574  history
```
