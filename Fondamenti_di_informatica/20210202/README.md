# Appunti della lezione di Martedí 2 Febbraio 2021

## Introduzione

"Il computer è l'equivalente di un frigorifero per la generazione del boom economico" e non abbiamo avuto il difficile percorso che ha avuto quella generazione.

Ciò ha portato quella generazione ad usare in maniera approfondita il computer per fare tutto in maniera casuale e in modo piú diverso.

Bisogna conoscere in profondità se si vuole conoscere ed avere un approccio personale a quello che facciamo, altrimenti si rimane relegati a quello che la macchina ci fa fare.

È di fondamentale importanza farsi aiutare dalla macchina per non ingozzare la macchina e per farle fare quello che vuole far lei.

La conoscenza approfondita necessaria è paradossalmente difficile da acquisire per via di vari motivi.

## Macchine di calcolo: la nascita

### Pascal
Il primo che ebbe l'idea di inventare delle macchine di calcolo per fare i conti fu Pascal, che con ruote per realizzare dei conti realizzo delle macchine per fare conti.

### Spiccioli

I romani avevano un sistema per fare i conti(con i sassi) e di numerazione per gli anni(i numeri romani).

### Leibnitz

Pose le basi dell'informatica, Leibnitz, e disse che non vi era bisogno di un sistema di numerazione cosí complicato, e bastavano 2 simboli e con quello si poteva numerare ogni cosa.
Leibnitz prese la macchina di calcolo di Pascal _Pascalina_, e si chiese con questa macchina cosa si potesse fare.

Poichè si potevano fare somme e sottrazioni, e Leibnitz riusciva a fare moltiplicazioni.

### Universo calcolabile o no?

Si pensava a un universo tutto calcolabile, il punto è che nel 1900 i matematici iniziano a porsi il problema dell'effettiva calcolabilità dei problemi a partire da Hilberte e poi vi è il famoso teorema di Goedel che teorizza che non possiamo risolvere tutti i problemi matematici.

Goedel relativizza i matematici ed è per questo che la scienza del novecento è cosí debole e fragile.

### Turing

Turing riesce a dimostrare che con una certa macchina è possibile calcolare tutti i problemi che siano calcolabili. Questo è un fatto importante, poichè divide in due il problema:
1. se le macchine possono sostituire il pensiero umano
2. oppure esse non possono sostituire il pensiero umano

#### Macchina di Turing

Una macchina con memoria infinita(nastro su cui vi erano riportati 2 simboli), e scorrendo il nastro avanti ed indietro per la macchina si poteva calcolare qualsiasi problema.
Con una quantità infinita di bit si poteva risolvere qualsiasi problema numerico.

>L'informatica che vediamo oggi nasce da una serie di intuizioni

1. Leibnitz e la rappresentazione per mezzo di 0 ed 1
2. problema di risoluzione di tutti i problemi calcolabili

#### Sostituzione dell'uomo da parte della macchina

Le macchine possono sostituire l'uomo nei problemi di calcolo.

### Guerra e informatica

Durante la guerra vi fu una forte spinta per l'informatica.

### Von Neumann

Egli pensò le macchine per come sono adesso, ovvero che sono un'evoluzione della macchina di Turing.

Esse sono un'ottimizzazione della macchina di Turing.

## Cosa c'è dentro un computer

### Interruttori

A livello fisico sono tutti transistor, ovvero interruttori. Ciò deriva direttamente dall'intuizione di Leibnitz, ovvero usare 2 stati.

Ciò permette ad esempio di realizzare un video.

### RAM cosa è?

Vi sono dati ed indirizzi che gestiscono altri interruttori.

>Tutto è un interruttore

### Cosa è una CPU?

La CPU è una sorta di memoria in cui gli interruttori hanno delle specifiche funzioni.

In essa vi è ad esempio un registro che porta ad un indirizzo di memoria.

Istruzioni del computer:
- spostamenti

>Gli interruttori che si trovano in una certa locazione di memoria, vengono ricopiati e messi in un'altra locazione di memoria.

### Come tenere l'informazione quando va via la corrente?

#### Hard Disk

Essi sono realizzati salvando gli stati degli interruttori in forma magnetica. Come su i nastri magnetici, in cui rimanevano magnetizzati per un tempo lungo.
Questo tipo di informazione analogica, all'inizio degli anni '80 si è cominciata a chiamarla su dischi rigidi, ovvero delle pile di dischi uno sopra l'altro, che girano e permettono di magnetizzare le particelle di ferro sulla superficie in maniera tale da salvare uno stato, nel caso dei computer uno stato binario.

>Si può rappresentare ogni numero attraverso interruttori

### Rappresentazione in numeri binari

Con essi posso rappresentare tutti i numeri...

Contare i numeri con le 10 dita in binario con le 10 dita.

2^16 = 65536 -> con 16 interruttori posso calcolare molti numeri.

2^24 = 16777216

### Suono e bit

Se usiamo 24 bit possiamo fare una sinusoide che oscilla che oscilla tra 0 e la metà di 16777216.

Questo ci porta a calcolare soli numeri interi positivi.

Ma come li vogliamo leggere questi numeri?

Con questi possiamo fare solo somme e moltiplicazioni.

### Come posso fare sottrazioni e divisioni?

Usando un bit per il segno.

Il numero negativo per essere utilizzabili, se lo sommo al corrispondente positivo da zero.

#### Complemento a due

```
1  = 0001

-1 = 1111
```
Un numero sommato con il suo complemento a due da zero in uscita.

#### Numeri in floating point
- 8 bit li assegno alla mantissa
- 4 bit li assegno all'esponente

Divido in due parti con un numero che è tra 0 ed 1.

![c2](c2.png)

In questa numerazione qui vi sono dei buchi, e non ho una numerazione contigua.

### Cosa succede quando premo un tasto della tastiera

- chiudo un interruttore
- il valore arriva su un bus DATI e INDIRIZZI(dove devono andare questi dati)
- i dati vengono portati a un registro nella CPU

Attraverso una collezione di interruttori riusciamo a fare tutto.

Multiplexer che mette in fila tasti premuti insieme, e capisce quale dato viene prima o dopo.

## Problema del numero dei bit

Computer che hanno file di interruttori più o meno grandi.

Computer a 16 bit -> importante rivoluzione

Non è importante quanti numeri riesca a contare.

### Memoria limitata

se ho 16 bit -> memoria non piú grande di 64KB

![lim](lim.png)

#### Sistema DOS

Aveva 640 KB, posso caricare il bus indirizzi con 2 parti dell'indirizzo, con due parole da 2^16 indirizzi.

Questo è uno dei motivi per cui le macchine con piú numero di bit, sono piú veloci, perchè esse permettono un indirizzamenteo piú diretto.

### I bit e il loro utilizzo oltro la memorizzazione

Nell'IR della CPU i bit si usano in altro modo.
Mettere i bit giusti al posto giusto e quando si programma si scelgono questi interruttori.

#### Assembly

Con 146 che è MOVE e 5961 con il JUMP

![ass](ass.png)

>La CPU è un cuore che batte ad una certa precisa velocità
_________

Il problema del controllo si puó risolvere in vari modi

Per la prossima volta -> procurarsi di un emulatore di terminale con la shell.
