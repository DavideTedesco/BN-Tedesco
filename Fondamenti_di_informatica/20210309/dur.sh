#! /bin/bash

if [ i# -lt 2]
then
      echo "usage: $0 file [...]" 1>&2
      exit -1
fi

sndinfo $@ |& awk 'BEGIN {totdur=0}
/\.[WwAa][AaIi][VvFf]*:$/ {nome=$1}
/seconds$/ {dur=$7}
/frames)$/ {totdur+=dur;printf("%-60s %12.4f\n", nome, dur)}
END {print "gran totale:", totdur}'
exit 0
