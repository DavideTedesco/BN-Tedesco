# Appunti della lezione di martedì 16 marzo 2021

## `nc`

Comunicatore banalizzato, può funzionare sia da server che da client.

Metto un indirizzo e una porta:
`nc -l localhost 52323`

Servente

Ora si aspetta il cliente:
`nc localhost 52323`

Posso usare questo comando anche su un server...

Sto di fatto vedendo la rete in azione.

>Quando parliamo di una rete parliamo di trasmettere dei messaggi da una parte ad un'altra.

È quindi necessario realizzare dei protocolli di comunicazione per il passaggio di messaggi.

## http

Esso è l'Hyper Text Transfer Protocol, e serve per comunicare tra cliente e server.

Il protocollo http ha 7/8 comandi ha:
- `get`
- ...

Quando chiamo http...

Mentre il get è un istruzione trasparente.

Il download generale da un sito è una cosa molto sicura.

Il cosiddetto `post` è in genere molto diverso, scrivere dentro un sito web è molto più protetto e controllato di leggere solamente.

## Criptaggio
>Quando un flusso di dati è criptato è difficilissmo capire quello che si passa.

Gli algoritmi di criptaggio oggi hanno una lunghezza indipendente dalla lunghezza del messaggio stesso.

- algoritmo per traspozione dell'alfabeto

Degli algoritmi hanno un elemento segreto che va comunicato.

L'elemento segreto è l'anello debole della catena.

Sul web la questione diviene complessa...

### RSA

L'algoritmo RSA nasce dalla necessità di avere un algoritmo che non ha un elemento segreto.

Operazione che ha la massima entropia è una sequenza di numeri primi.

Se si scopre il meccanismo di funzionamento dei numeri primi, cade tutto ciò che è stato realizzato per il criptare.

Funzioni ellittiche che potrebbero servire come prossimo sistema del criptaggio.

Fare algoritmi pubblici che in mancanza di elementi non si possono decriptare.

Vi sono protocolli in cui non bisogna conoscere il destinatario.

Nella realtà della rete odierna, le ricerche sono criptate.

## Cookies

A cosa servono?

Essi sono un meccanismo di funzionamento quasi obbligatorio della rete. (dal 2012 c'è una legge che regolamenta i cookies)

Quando vado su un sito, faccio una richiesta in http, ed ho una risposta che mi ritorna piú un'altra cosa(i cookie).

Il server da delle informazioni al browser, che il browser rimanderà al server ogni volta.

### Connessione statless

La connessione http è stateless, faccio una richiesta al server ed esso mi risponde in un certo modo, quando ho chiuso la richiesta non invio piú nulla al server.

### Connessione statefull

Fino a quando entro e rimango con un `ping` attivato da terminale, rimango sempre connesso al server.

### Esempio di pagina stateless
Se apro una pagina web, faccio una connessione stateless.

Come ricordarsi in che posizione si è arrivati quando si fa una richesta?

Ciò se lo ricorda perchè ha delle informazioni di sessione che ogni volta che si connette al server si ricorda e mantiene.

Http è stato uno dei primi protocolli ad essere stateless ed aveva bisogno dei cookies.

### Cookies e scrittura

La politica si è accorta che i cookies scrivono sulla propria macchina.

Molta della profilazione sugli acquisti se la vende Google a caro prezzo.

Google ha un impatto industriale fortissimo.

## Alphabet e Google

Budget pubblici, con cifre astronimiche con milioni di voci di entrate ed uscite con miliardi di dollari...

## Come sono cambiate le applicazioni per le reti? Architettura Multi-tier

Le applicazioni sono realizzate in un modo multi-tier...

**Studio di caso: come è architettato Facebook dal punto di vista software?**

![multi_tier_architecture.png](multi_tier_architecture.png)

- Facebook ha un database
- client -> browser web
- server -> con gestore del database (db manafer)
- view

In messo al manager e visualizzatore, vi è una glue, o business logic che è fatta con applicazioni realizzate su una rete.

Abbiamo varie applicazioni staccate, con un'architettura multi-tier.

### Pure Data
Tra i nostri strumenti di sintesi, abbiamo applicazioni con architetture multi-tier:

Pure data è realizzato con un interfaccia grafica ed un motore audio, essa è una dual-tier.

Possiamo mettere un'interfaccia grafica su un'altra macchina.

Quando si fa partire un'applicazione in PD, parte l'engine audio e poi il visualizzatore di grafica.

### FAUST
FAUST non è multi-tier, esso è molto furbo, perchè il codice che si scrive ad alto livello viene realizzato in C++ e può divenire diverso cambiando la sola architettura, con un processo di compilazione.
Abbiamo quindi delle applicazioni monolitiche realizzate in FAUST che posso parlare fra di loro in una rete.

## Cosa sono dei cloud

Le cloud si basano sulla virtualizzazione delle macchine.

Con un sistema multitasking aperto sono iniziate ad apparire una serie di deviazioni sul tema.

Un sistema UNIX è un sistema multi-tasking con vari utenti.

>Ogni task è completamente isolato da un altro task.

Facendo lo switch tra un task e l'altro, ci si muove fra i task senza problemi.

Ma tutti i task usano le stesse risorse.

### Macchine virtuali

Su un task possono girare delle macchine virtuali(VM).

Si possono avere tante macchine fisiche con un architettura linux in cui la mia macchina virtuale è distribuita su piú macchine, facendo un load balance e distribuendosi su varie macchine.
### IAAS
Una cloud sta su un disco che si comporta come un disco normale 80gb, vi è una macchina di tipo storage di 500gb per backup e non per accesso continuo.

Questa è l'IAAS: Infrastructure As A Service

Tipo base di cloud.

![vms_and_clouds.png](vms_and_clouds.png)

Qualcuno mette a disposizione un pezzo di architettura, e te li fa pagare per quanto serve!

Non ci si comprano le macchine ma il tempo macchina.

## Banca o sotto al materasso: i file dove li metto?

Un cloud è una società di informazione su cui metto i miei dati.

Le cloud sono gli specialisti del salvataggio dei files.

Il fatto di tenere tutto creative commons e pubblico magari fa ritrovare tutto...

Usare un server git per lavori privati, per avere una migliore garanzia di sopravvivenza...

### Pagamento di GitHub?

GitHub è un'interfaccia grafica incollata su git.

[git.smerm.org](git.smerm.org)

Microsoft ha pagato 7,5 MLD di dollari GitHub...

GitHub ha comprato dati per 3 milioni di utenti...

Nella società industriale gli asset erano le risorse energetiche(come Emirati Arabi Uniti), l'equivalente delle risorse energetiche oggi sono i dati.

ICBSA che ha tutto il materiale discografico e fonografico dall'inizio della riproducibilità ad oggi.

[https://www.europeana.eu/it](https://www.europeana.eu/it) è di fatto un progetto fallimentare.

È significativo cercare di capire quando vengonono aggregate società tipo Facebook, Instagram e WhatsApp.

YouTube è oggi divenuta un incomprabile oggi per il suo valore.

### Huawei

Un capo di stato può determinare come usare Android...

Su Huawei non si può avere Play Store...

Android è un linux abbastanza menomato e non usabile a livelli liberi.

## Software libero
Richiard Stone: iniziatore del software libero

Software libero o open-source?

Android open-source e non software libero, perchè è open-source ma non è libero come software.
______
Nella prossima lezione si parlerà di software libero e si inizierà a programmare.
