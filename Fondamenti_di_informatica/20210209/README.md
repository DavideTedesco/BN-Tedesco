# Appunti della lezione di Martedì 9 Febbraio 2021


Avendo a che fare con collezioni di interruttori, c'è stato bisogno di standardizzare molte cose.

## ASCII

Gli standard legati ai caratteri sono ASCII.

Standard con 7 bit.

Con esso riusciamo ad avere 128 caratteri, da 0 a 127.
![ascii](ascii.png)

Le prime due colonne sono caratteri di controllo.

Dal 48 cominciano i numeri.

Abbiamo i caratteri di punteggiatura etc...

Le lettere sono una di seguito all'altra e in fondo vi sono parentesi.

Questo fa parte dello standard ASCII, che si capisce che è americano e si vede, perchè non vi sono le lettere accentate.

## Revisione degli standard

Vi sono stati altri standard con l'espandersi dell'informatica.

_Latin1_ -> _Latin15_ quando nacque l'euro.

Lo standard _UTF8_ usa 128 bit con 265 caratteri, essi sono tutti i caratteri...

Poi arrivando altre culture si sono ancora più espansi gli standard _UTF16_ che utilizza 16 bit.

>Si è mantenuta la coerenza fra i simboli dell'UTF8 e dell'ASCII


## Peso di file ASCII

La maggior parte dei linguaggi di programmazione si basano su caratteri di ASCII puro.

Vi sono quindi delle sovrapposizioni di standard e ciò significa che scrivendo con un editor di testo ASCII puro, inserendo caratteri accentati, l'editor di testo passa da 7 a 8 bit. Il problema si crea quando si passa ai 16 bit.

>Ruby scritto da un giapponese, gestisce anche UTF16 e occupa il doppio un file in UTF16.

![utf8](utf8.png)

[unicode](http://www.brescianet.com/appunti/vari/unicode.htm)

## Standard per la definizione dei colori

Essi servono per gestire i colori di uno schermo ad esempio.

La matrice di led è controllata da una pila (in genere di 24 interruttori -> 16 milioni di colori), organizzati in 3 gruppi di 8 bit:
- 8 bit definisco il verde
- 8 definisco il rosso
- 8 definisco il blu

La trasparenza è una proprietà dell'immagine e non dello schermo.

## Necessità della standardizzazione

>Necessità dunque di standardizzare le cose

Con l'IBM che realizzò inizialmente un suo standard.

Le combinazioni di numeri attraverso gli standard acquisiscono altri significati.

Standard midi -> acquisiamo anche le note musicali.

Do centrale è 60 nello standard midi.

Usiamo un altro standard nella musica elettronica e contemporanea per avere altre altezze oltre i semitoni.


## Cosa succede quando premiamo ON su un computer

La CPU che è un mucchio di interruttori che è specializzata. Vi è la memoria specializzata che si chiama program counter, che è puntata dal computer per leggere la prossima istruzione.

Quando viene azzerata la CPU sta a zero.

L'indirizzo di memoria, quando essa viene azzerata sta su una ROM che contiene un programma di diagnostica(come quando esci di casa e controlli se hai tutto ciò che ti serve), la memoria ROM sono interruttori bruciati in una certa posizione.

La CPU legge questo programma e carica un'istruzione ed ed esegue un programma di diagnostica.

Quando la diagnostica passa e parte un programma che carica il sistema operativo, ovvero il boot loader, che carica il sistema operativo che si trova in una zona specifica del disco.

![lavagna](lavagna.png)


## Sistema operativo

Esso è un programma, ovvero il programma che non si vede mai, ma è quello che fa la gestione delle risorse del computer.

Il sistema operativo fa la gestione delle varie periferiche e le orchestra tutte insieme.

>Gestisce le risorse e fa partire altri programmi, ai quali passa la mano.

### A cosa serve un sistema operativo?

>Un computer potrebbe funzionare anche senza il sistema operativo. Ovvero se esso deve gestire funzioni molto semplici o ripetitive, infatti non servirebbe avere un sistema operativo per fare ciò.

Una macchina senza sistema operativo fa il _polling_, ovvero la reiterazione ed il controllo di un parametro, una porta o un bottone.

Arduino ad esempio non ha un sistema operativo, e non ce l'ha perchè non deve gestire cose complesse.

Su un computer normale in genere siamo su circa 65.000 porte.

### Carico del sistema operativo

Il sistema operativo ha un carico sulla macchina che permette di gestire un computer come lo conosciamo.

Nel caso in cui dobbiamo gestire delle interfacce semplici per gestire standalone, si può utilizzare anche Arduino.

### Evoluzione sistemi operativi

Vi è stata un'evoluzione dei sistemi operativi.

Oggi vi sono 3 sistemi operativi molto in voga, realizzati in momenti diversi e per motivi diversi:
1. UNIX, ovvero il primo sistema operativo che è riuscito a implementare il time sharing, Linux
2. MACH, macOs è Kernel di tipologia MACH derivato da UNIX
3. DOS con storia diversa, che era operazione lanciata da Microsoft (Disk Operating System), che IBM usò per lanciare la prima linea di computer

### Time sharing e sistemi operativi

I computer erano molto costosi e divisi fra tante persone. Dunque si può lavorare su tanti terminali, ma il sistema operativo deve essere in grado di far lavorare tutti contemporaneamente.

Negli anni '60 vi fu una commissione tra General Eletric, Honeywell e Bell Laboratoraries. Avevano creato il sistema Multix(ancora esistenti sostenitori di questi sistemi), che crollò perchè la General Eletric si tirò indietro.

Ken Thompson, che aveva creato Multix, si creò una versione di Multix per giocare a scacchi UNIX.

### La nascita di Linux

Esso è uno UNIX system 5, versione Bell Labs, ed è quello che usa la maggior parte dei server.

Kernel UNIX tradizionale, ovvero Kernel monolitico, dove parte un programma _vmlinuz_ che gira costantemente nella macchina. Come era stato concepito UNIX e come funziona Linux.

### Kernel MACH

Esso è un piccolissimo programma(micro kernel) che gestisce le risorse attraverso piccoli programmi che partono e si fermano a seconda della necessità della gestione delle risorse.

I micro kernel impattano di meno, ma è molto più complesso gestirli; ad esempio.

### Kernel Windows

DOS era un sistema mono task concepito per i _personal computer_, che non si pensava dovessero servire per il multi task.

Esso poteva fare una sola cosa alla volta ed è dovuto rapidamente evolvere e cambiare, ma ha sempre avuto questa questione di essere legato ad una concezione di sistema operativo personale che non doveva essere condiviso fra varie persone.

Mentre UNIX anche se tutte le macchine sono ormai  personali lavora comunque in multi task.

### Come si lavora in multi task?

La task list, ovvero l'elenco di programmi contiene lo stato della CPU l'ultima volta che la task ha fatto qualcosa. Il time-sharing fa una serie di Task e prende lo stato della CPU e mette una serie di stati nella CPU, fa andare la CPU, risalva il dato di un dato processo.

Il processore fa una cosa per un certo periodo di tempo, salva lo stato nella RAM e poi carica un'altra task e manda avanti la CPU.

Con il comando 'ps' vediamo i processi su Linux.
![ps](ps.png)

Posso uccidere i processi in ogni momento attraverso il codice del processo.

Vediamo come è abbastanza "impressive" quello che riesce a fare un sistema time-sharing.

### Interfacce grafiche

Con l'avvento delle interfacce grafiche è comparsa un'altra necessità.

Le task avevamo visto facevano task-switching, carica e scaricare dalla CPU il cosiddetto contesto, che viene salvato e messo in RAM.

Il task-switching è pesante, per il tempo e le risorse che servono.

Vi sono delle cose che non si prestano bene al task-switching, come l'interfaccia grafica.

>Quando l'applicazione che ho è attiva, devo poter utilizzare gli elementi che ho sull'interfaccia.

Si fa dunque per alcune cose il _threading_.

### Threading

è una sorta di multi tasking legato a una task sola, in cui le varie voci vanno avanti contemporaneamente. Il problema del multi-threading è l'accesso alle variabili del programma.

Ho una gestione delle variabili che si chiama _mutexlock_. Se trovo una varibile e la trovo bloccata, la aspetto.

### Time-sharing

Esso è più complesso di così: la scrittura su disco è ad esempio molto lenta.

Il disco mi risponde, in scrittura, nell'ordine di 1/2 ms che per il computer è un tempo memorabile.

Il computer quando accede a una risorsa più lenta, manda a dormire i processi più lenti.

Microsoft a quanto pare nell'ultimo sistema operativo ha realizzato un programma time-sharing.

### Diverse modalità utilizzo sistemi operativi

1. Task-switching, durante esso abbiamo operazione atomica, ed il computer non si può fermare in mezzo, ed il computer non può pensare di rispondere a un tasto(in cui risponde in interrupt, oltre allo stato normale di funzionamento->si può interrompere elettricamente con un Interrupt da tastiera, interrompendo qualsiasi cosa stia facendo), vi sono dei momenti in cui un computer non può interrompere _kernel mode_, momenti in cui il sistema operativo deve salvare in memoria. Il _kernel mode_ in generale dura molto poco.
2._User mode_ è il modo normale in cui girano tutti i programmi che possono essere interrotti, mandati a dormire etc...

>Quando un programma deve leggere e scrivere da una risorsa, il programma deve andare in kernel mode, lettura e scrittura di singoli byte.

Lettura e scrittura di file, deve switchare di contesti e programmi varie volte.

In tutto ciò si inserisce il discorso delle cache che hanno sue fasi computazionali che agevolano queste operazioni.

I sistemi operativi sono materie complicate.

>Del kernel mode ce ne si accorge in certe situazioni, quando il computer si pianta, tipicamente è che il computer si blocca in kernel mode e non ridà piú la mano ad altri programmi. Situazione in cui il computer diventa non responsive a qualsiasi input.

Durante il boot il kernel è mono task, il kernel gira praticamente tutto in ram.

## Cosa è un Kernel?

Esso è un pezzo centrale del sistema operativo(nocciolo del sistema operativo), intorno ad esso vi sono molti ghingheri e risorse.

Si ha in Linux un nocciolo e i driver che servono a gestire le varie risorse:
![ker](ker.png)

Il kernel vede qualsiasi cosa come lettura e scrittura da un file, mentre i driver si adattano a specifiche dei vari dispositivi connessi.

Il driver video ad esempio è una parte di kernel che è specializzata su una data scheda video etc...

Praticamente hanno trovato il modo di fare una generalizzazione delle funzioni del kernel che consistono nel fare il task switching.

![audio](audio.png)

(Kernel Panic in Linux: Oops, guru meditation(programmi))

Il [Jargon File](http://www.catb.org/jargon/html/) contiene modi di dire e storielle sui programmatori.

### Dopo la partenza del sistema operativo cosa succede?

Vi sono dei sistemi operativi che non fa vedere nulla di ciò che succede quando parte il programma.

Intorno al sistema operativo parte la conchiglia ovvero un programma che gestisce tutto ciò che vi è intorno al sistema operativo.

1. Shell
2. IDE

L'interfaccia grafica è un programma che attraverso il sistema operativo prende possesso di ciò che succede al sistema.

#### Interfaccia grafica

Essa fu presa da Xerox, poi da Apple e poi divvenne di uso comune.

Mentre essa è estremamente importante nell'uso comune come appliance generalizzata, etc...

#### Shell
L'interfaccia testuale si presta bene alle applicazioni professionali del computer.

L'uso dell'interfaccia tesutale si presta all'uso del computer nella musica.

Cosa si può fare con interfaccia testuale che non si può fare con l'interfaccia grafica?

Una buona combinazione delle due interfacce consente un utilizzo abbastanza serio del computer.

#### Differenze tra interfacce
Una delle differenze tra interfacce testuali e grafiche è che è portata o meno ad un utilizzo.

Si può utilizzare un'IDE e li iniziano le guerre di religione...

#### Cosa si può fare con un'interfaccia testuale?

Il comando più usato è forse "ls", ovvero per listare i file che ci sono in una cartella.

Comando "pwd".

Quando parte il kernel in un sistema testuale fa partire un emulatore di terminale che è il driver di terminale, ovvero un software come un altro, che fa partire un programma che si chiama shell.

Il comando "ls -l" dice dove viene preso il comando lista.

La shell da comandi che da la mano a altri programmi.

#### Che cos'è la shell?

è un programma, ovvero la bash shell e si trova in una particolare cartella.

Una delle caratteristiche dei comandi da riga di comando è che i comandi sono corti da scrivere.

Per guardare il contenuto della cartella si fa una chiamata a sistema, e poi si stampa il contenuto.

"root" nei sistemi UNIX è il capo del mondo, mentre un user ha un certo numero di gruppi e può accedere ad un certo numero di operazioni come "sudo".

### I comandi da shell

I comandi da linea di comando sono molto piccoli, e seguono la strategia _divide et impera_:
1. un comando fa una sola cosa
2. i comandi si possono connettere fra di loro

Qualsiasi forma di lettura e scrittura hanno Input e Output come tre file:
- stdin
- stdout
- stderr

>Vi sono sempre questi 3 files aperti

I comandi hanno anche dei valori di ritorno, quando escono, escono con un valore di ritorno del comando:

- 0 quando il comando non ha avuto errori
- !0 se il comando ha avuto errori

![err](err.png)

2 è ad esempio l'errore "file not found"

Nella shell i programmi ritornano un solo modo con cui non avere errori e vari con cui hanno errori.

La cosa interessante delle shell intelligenti è che se voglio mandare l'Output di un programma ad un altro programma che cerca tra le stringhe gli mando.

Come tutti i comandi che iniziano con la h:
![h](h.png)

Ora stiamo ridirigendo l'output di ls dentro un altro comando che ha collegato il file di stdin ha un'uscita.

Cosa succede se cerco un file che non esiste?

![errs](errs.png)

Lo passa ad un altro file separato.

Posso mettere un errore in un file
![errs1](errs1.png)

Separo standard outoput da standard error per poter separare i file.

Se voglio mettere un output in un comando faccio:
![errs2](errs2.png)

"cat" serve per cercare i file

"rm" ruspa via un file dal disco

"rm -i" per essere coccolati dal sistema operativo

Il sistema UNIX e le shell sono fatti per i programmatori dai programmatori.


### Valori di ritorno e loro utilizzo

I valori di ritorno di un comando si possono usare per fare comandi come "echo".

Ad esempio faccio valutare un'operazione e poi:
![echo](echo.png)

### Moltiplicazione di CPU e vicinanza di componenti

Si va verso una moltiplicazione delle CPU poichè si è arrivati al limite della miniaturizzazione.

Alcune applicazioni si distribuiscono su tante CPU, le applicazioni multi-threading si distribuiscono tante CPU.

_____________


[link GitLab del corso](https://gitlab.com/SMERM/informatica_di_base)
