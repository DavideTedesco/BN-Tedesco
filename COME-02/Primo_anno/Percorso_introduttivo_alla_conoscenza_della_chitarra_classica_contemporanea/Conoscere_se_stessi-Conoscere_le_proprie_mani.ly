\version "2.20.0"
\header {
  tagline = ##f
 }

\new staff{
  \clef "treble_8"
  \arpeggioArrowUp
  <e, a, e g b e'>\arpeggio^"sopra la tastiera l.v."
  
  a e dis \bar "-." r
  
} 
