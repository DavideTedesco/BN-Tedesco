/* ------------------------------------------------------------
author: "DT"
license: "BSD"
name: "Karplus_filtrato"
version: "1.1"
Code generated with Faust 2.27.2 (https://faust.grame.fr)
Compilation options: -lang cpp -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

/************************************************************************
 ************************************************************************
    FAUST Architecture File
    Copyright (C) 2006-2011 Albert Graef <Dr.Graef@t-online.de>
    ---------------------------------------------------------------------
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation; either version 2.1 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the GNU C Library; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA. 
 ************************************************************************
 ************************************************************************/

/* Pd architecture file, written by Albert Graef <Dr.Graef@t-online.de>.
   This was derived from minimal.cpp included in the Faust distribution.
   Please note that this is to be compiled as a shared library, which is
   then loaded dynamically by Pd as an external. */

#include <stdlib.h>
#include <math.h>
#include <string.h>

/************************** BEGIN misc.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <cstdlib>
#include <string.h>
#include <fstream>
#include <string>

/************************** BEGIN meta.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual ~Meta() {};
    virtual void declare(const char* key, const char* value) = 0;
    
};

#endif
/**************************  END  meta.h **************************/

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key] = value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key] = value; }
};

static int lsr(int x, int n) { return int(((unsigned int)x) >> n); }

static int int2pow2(int x) { int r = 0; while ((1<<r) < x) r++; return r; }

static long lopt(char* argv[], const char* name, long def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return std::atoi(argv[i+1]);
    return def;
}

static long lopt1(int argc, char* argv[], const char* longname, const char* shortname, long def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return atoi(argv[i]);
        }
    }
    return def;
}

static const char* lopts(char* argv[], const char* name, const char* def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
    return def;
}

static const char* lopts1(int argc, char* argv[], const char* longname, const char* shortname, const char* def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return argv[i];
        }
    }
    return def;
}

static bool isopt(char* argv[], const char* name)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
    return false;
}

static std::string pathToContent(const std::string& path)
{
    std::ifstream file(path.c_str(), std::ifstream::binary);
    
    file.seekg(0, file.end);
    int size = int(file.tellg());
    file.seekg(0, file.beg);
    
    // And allocate buffer to that a single line can be read...
    char* buffer = new char[size + 1];
    file.read(buffer, size);
    
    // Terminate the string
    buffer[size] = 0;
    std::string result = buffer;
    file.close();
    delete [] buffer;
    return result;
}

#endif

/**************************  END  misc.h **************************/
/************************** BEGIN UI.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2020 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

template <typename REAL>
struct UIReal
{
    UIReal() {}
    virtual ~UIReal() {}
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // -- active widgets
    
    virtual void addButton(const char* label, REAL* zone) = 0;
    virtual void addCheckButton(const char* label, REAL* zone) = 0;
    virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;
    
    // -- metadata declarations
    
    virtual void declare(REAL* zone, const char* key, const char* val) {}
};

struct UI : public UIReal<FAUSTFLOAT>
{
    UI() {}
    virtual ~UI() {}
};

#endif
/**************************  END  UI.h **************************/
/************************** BEGIN dsp.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>
#include <vector>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'openTabBox', 'addButton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param sample_rate - the sampling rate in Hertz
         */
        virtual void init(int sample_rate) = 0;

        /**
         * Init instance state
         *
         * @param sample_rate - the sampling rate in Hertz
         */
        virtual void instanceInit(int sample_rate) = 0;

        /**
         * Init instance constant state
         *
         * @param sample_rate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int sample_rate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         *
         */
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = nullptr):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int sample_rate) { fDSP->init(sample_rate); }
        virtual void instanceInit(int sample_rate) { fDSP->instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { fDSP->instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
        virtual std::string getCompileOptions() = 0;
        virtual std::vector<std::string> getLibraryList() = 0;
        virtual std::vector<std::string> getIncludePathnames() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/**************************  END  dsp.h **************************/

/******************************************************************************
*******************************************************************************

							       VECTOR INTRINSICS

*******************************************************************************
*******************************************************************************/


/***************************************************************************
   Pd UI interface
 ***************************************************************************/

enum ui_elem_type_t {
  UI_BUTTON, UI_CHECK_BUTTON,
  UI_V_SLIDER, UI_H_SLIDER, UI_NUM_ENTRY,
  UI_V_BARGRAPH, UI_H_BARGRAPH,
  UI_END_GROUP, UI_V_GROUP, UI_H_GROUP, UI_T_GROUP
};

struct ui_elem_t {
  ui_elem_type_t type;
  char *label;
  float *zone;
  float init, min, max, step;
};

class PdUI : public UI
{
public:
  const char *name;
  int nelems, level;
  ui_elem_t *elems;
		
  PdUI();
  PdUI(const char *nm, const char *s);
  virtual ~PdUI();

protected:
  std::string path;
  void add_elem(ui_elem_type_t type, const char *label = NULL);
  void add_elem(ui_elem_type_t type, const char *label, float *zone);
  void add_elem(ui_elem_type_t type, const char *label, float *zone,
		float init, float min, float max, float step);
  void add_elem(ui_elem_type_t type, const char *label, float *zone,
		float min, float max);

public:
  virtual void addButton(const char* label, float* zone);
  virtual void addCheckButton(const char* label, float* zone);
  virtual void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step);
  virtual void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step);
  virtual void addNumEntry(const char* label, float* zone, float init, float min, float max, float step);

  virtual void addHorizontalBargraph(const char* label, float* zone, float min, float max);
  virtual void addVerticalBargraph(const char* label, float* zone, float min, float max);
    
  virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
  
  virtual void openTabBox(const char* label);
  virtual void openHorizontalBox(const char* label);
  virtual void openVerticalBox(const char* label);
  virtual void closeBox();
	
  virtual void run();
};

static std::string mangle(const char *name, int level, const char *s)
{
  const char *s0 = s;
  std::string t = "";
  if (!s) return t;
  // Get rid of bogus "0x00" labels in recent Faust revisions. Also, for
  // backward compatibility with old Faust versions, make sure that default
  // toplevel groups and explicit toplevel groups with an empty label are
  // treated alike (these both return "0x00" labels in the latest Faust, but
  // would be treated inconsistently in earlier versions).
  if (!*s || strcmp(s, "0x00") == 0) {
    if (level == 0)
      // toplevel group with empty label, map to dsp name
      s = name;
    else
      // empty label
      s = "";
  }
  while (*s)
    if (isalnum(*s))
      t += *(s++);
    else {
      const char *s1 = s;
      while (*s && !isalnum(*s)) ++s;
      if (s1 != s0 && *s) t += "-";
    }
  return t;
}

static std::string normpath(std::string path)
{
  path = std::string("/")+path;
  int pos = path.find("//");
  while (pos >= 0) {
    path.erase(pos, 1);
    pos = path.find("//");
  }
  size_t len = path.length();
  if (len > 1 && path[len-1] == '/')
    path.erase(len-1, 1);
  return path;
}

static std::string pathcat(std::string path, std::string label)
{
  if (path.empty())
    return normpath(label);
  else if (label.empty())
    return normpath(path);
  else
    return normpath(path+"/"+label);
}

PdUI::PdUI()
{
  nelems = level = 0;
  elems = NULL;
  name = "";
  path = "";
}

PdUI::PdUI(const char *nm, const char *s)
{
  nelems = level = 0;
  elems = NULL;
  name = nm?nm:"";
  path = s?s:"";
}

PdUI::~PdUI()
{
  if (elems) {
    for (int i = 0; i < nelems; i++)
      if (elems[i].label)
	free(elems[i].label);
    free(elems);
  }
}

inline void PdUI::add_elem(ui_elem_type_t type, const char *label)
{
  ui_elem_t *elems1 = (ui_elem_t*)realloc(elems, (nelems+1)*sizeof(ui_elem_t));
  if (elems1)
    elems = elems1;
  else
    return;
  std::string s = pathcat(path, mangle(name, level, label));
  elems[nelems].type = type;
  elems[nelems].label = strdup(s.c_str());
  elems[nelems].zone = NULL;
  elems[nelems].init = 0.0;
  elems[nelems].min = 0.0;
  elems[nelems].max = 0.0;
  elems[nelems].step = 0.0;
  nelems++;
}

inline void PdUI::add_elem(ui_elem_type_t type, const char *label, float *zone)
{
  ui_elem_t *elems1 = (ui_elem_t*)realloc(elems, (nelems+1)*sizeof(ui_elem_t));
  if (elems1)
    elems = elems1;
  else
    return;
  std::string s = pathcat(path, mangle(name, level, label));
  elems[nelems].type = type;
  elems[nelems].label = strdup(s.c_str());
  elems[nelems].zone = zone;
  elems[nelems].init = 0.0;
  elems[nelems].min = 0.0;
  elems[nelems].max = 1.0;
  elems[nelems].step = 1.0;
  nelems++;
}

inline void PdUI::add_elem(ui_elem_type_t type, const char *label, float *zone,
			  float init, float min, float max, float step)
{
  ui_elem_t *elems1 = (ui_elem_t*)realloc(elems, (nelems+1)*sizeof(ui_elem_t));
  if (elems1)
    elems = elems1;
  else
    return;
  std::string s = pathcat(path, mangle(name, level, label));
  elems[nelems].type = type;
  elems[nelems].label = strdup(s.c_str());
  elems[nelems].zone = zone;
  elems[nelems].init = init;
  elems[nelems].min = min;
  elems[nelems].max = max;
  elems[nelems].step = step;
  nelems++;
}

inline void PdUI::add_elem(ui_elem_type_t type, const char *label, float *zone,
			  float min, float max)
{
  ui_elem_t *elems1 = (ui_elem_t*)realloc(elems, (nelems+1)*sizeof(ui_elem_t));
  if (elems1)
    elems = elems1;
  else
    return;
  std::string s = pathcat(path, mangle(name, level, label));
  elems[nelems].type = type;
  elems[nelems].label = strdup(s.c_str());
  elems[nelems].zone = zone;
  elems[nelems].init = 0.0;
  elems[nelems].min = min;
  elems[nelems].max = max;
  elems[nelems].step = 0.0;
  nelems++;
}

void PdUI::addButton(const char* label, float* zone)
{ add_elem(UI_BUTTON, label, zone); }
void PdUI::addCheckButton(const char* label, float* zone)
{ add_elem(UI_CHECK_BUTTON, label, zone); }
void PdUI::addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step)
{ add_elem(UI_V_SLIDER, label, zone, init, min, max, step); }
void PdUI::addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step)
{ add_elem(UI_H_SLIDER, label, zone, init, min, max, step); }
void PdUI::addNumEntry(const char* label, float* zone, float init, float min, float max, float step)
{ add_elem(UI_NUM_ENTRY, label, zone, init, min, max, step); }

void PdUI::addHorizontalBargraph(const char* label, float* zone, float min, float max)
{ add_elem(UI_H_BARGRAPH, label, zone, min, max); }
void PdUI::addVerticalBargraph(const char* label, float* zone, float min, float max)
{ add_elem(UI_V_BARGRAPH, label, zone, min, max); }

void PdUI::openTabBox(const char* label)
{
  if (!path.empty()) path += "/";
  path += mangle(name, level, label);
  level++;
}
void PdUI::openHorizontalBox(const char* label)
{
  if (!path.empty()) path += "/";
  path += mangle(name, level, label);
  level++;
}
void PdUI::openVerticalBox(const char* label)
{
  if (!path.empty()) path += "/";
  path += mangle(name, level, label);
  level++;
}
void PdUI::closeBox()
{
  int pos = path.rfind("/");
  if (pos < 0) pos = 0;
  path.erase(pos);
  level--;
}

void PdUI::run() {}

/******************************************************************************
*******************************************************************************

			    FAUST DSP

*******************************************************************************
*******************************************************************************/

//----------------------------------------------------------------------------
//  FAUST generated signal processor
//----------------------------------------------------------------------------
		
#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <algorithm>
#include <cmath>
#include <math.h>

static float mydsp_faustpower2_f(float value) {
	return (value * value);
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif

#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	int fSampleRate;
	float fConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	FAUSTFLOAT fHslider0;
	FAUSTFLOAT fHslider1;
	FAUSTFLOAT fButton0;
	float fVec0[2];
	FAUSTFLOAT fHslider2;
	float fRec12[2];
	int iRec13[2];
	int IOTA;
	float fVec1[1024];
	FAUSTFLOAT fHslider3;
	float fRec11[3];
	float fVec2[2];
	float fConst9;
	float fConst10;
	float fRec10[2];
	float fConst11;
	float fConst12;
	float fRec9[3];
	float fConst13;
	float fConst14;
	float fRec8[3];
	float fConst15;
	float fConst16;
	float fConst17;
	float fConst18;
	float fConst19;
	float fConst20;
	float fConst21;
	float fRec7[3];
	float fConst22;
	float fConst23;
	float fConst24;
	float fConst25;
	float fConst26;
	float fConst27;
	float fConst28;
	float fRec6[3];
	float fConst29;
	float fConst30;
	float fConst31;
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fRec5[3];
	float fConst36;
	float fConst37;
	float fConst38;
	float fConst39;
	float fConst40;
	float fConst41;
	float fConst42;
	float fRec4[3];
	float fConst43;
	float fConst44;
	float fConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fRec3[3];
	float fConst50;
	float fConst51;
	float fConst52;
	float fConst53;
	float fConst54;
	float fConst55;
	float fConst56;
	float fRec2[3];
	float fConst57;
	float fConst58;
	float fConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fConst63;
	float fRec1[3];
	float fConst64;
	float fConst65;
	float fConst66;
	float fConst67;
	float fConst68;
	float fConst69;
	float fConst70;
	float fRec0[3];
	FAUSTFLOAT fVslider0;
	float fRec14[2];
	float fConst71;
	float fConst72;
	float fConst73;
	float fConst74;
	float fRec27[2];
	float fRec26[3];
	float fRec25[3];
	float fVec3[2];
	float fConst75;
	float fConst76;
	float fConst77;
	float fRec24[2];
	float fConst78;
	float fRec23[3];
	float fConst79;
	float fConst80;
	float fRec22[3];
	float fRec21[3];
	float fRec20[3];
	float fRec19[3];
	float fRec18[3];
	float fRec17[3];
	float fRec16[3];
	float fRec15[3];
	FAUSTFLOAT fVslider1;
	float fRec28[2];
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fRec40[2];
	float fRec39[3];
	float fRec38[3];
	float fVec4[2];
	float fConst86;
	float fConst87;
	float fConst88;
	float fRec37[2];
	float fConst89;
	float fRec36[3];
	float fConst90;
	float fConst91;
	float fRec35[3];
	float fRec34[3];
	float fRec33[3];
	float fRec32[3];
	float fRec31[3];
	float fRec30[3];
	float fRec29[3];
	FAUSTFLOAT fVslider2;
	float fRec41[2];
	float fConst92;
	float fConst93;
	float fConst94;
	float fConst95;
	float fConst96;
	float fRec52[2];
	float fRec51[3];
	float fRec50[3];
	float fVec5[2];
	float fConst97;
	float fConst98;
	float fConst99;
	float fRec49[2];
	float fConst100;
	float fRec48[3];
	float fConst101;
	float fConst102;
	float fRec47[3];
	float fRec46[3];
	float fRec45[3];
	float fRec44[3];
	float fRec43[3];
	float fRec42[3];
	FAUSTFLOAT fVslider3;
	float fRec53[2];
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fRec63[2];
	float fRec62[3];
	float fRec61[3];
	float fVec6[2];
	float fConst108;
	float fConst109;
	float fConst110;
	float fRec60[2];
	float fConst111;
	float fRec59[3];
	float fConst112;
	float fConst113;
	float fRec58[3];
	float fRec57[3];
	float fRec56[3];
	float fRec55[3];
	float fRec54[3];
	FAUSTFLOAT fVslider4;
	float fRec64[2];
	float fConst114;
	float fConst115;
	float fConst116;
	float fConst117;
	float fConst118;
	float fRec73[2];
	float fRec72[3];
	float fRec71[3];
	float fVec7[2];
	float fConst119;
	float fConst120;
	float fConst121;
	float fRec70[2];
	float fConst122;
	float fRec69[3];
	float fConst123;
	float fConst124;
	float fRec68[3];
	float fRec67[3];
	float fRec66[3];
	float fRec65[3];
	FAUSTFLOAT fVslider5;
	float fRec74[2];
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fConst129;
	float fRec82[2];
	float fRec81[3];
	float fRec80[3];
	float fVec8[2];
	float fConst130;
	float fConst131;
	float fConst132;
	float fRec79[2];
	float fConst133;
	float fRec78[3];
	float fConst134;
	float fConst135;
	float fRec77[3];
	float fRec76[3];
	float fRec75[3];
	FAUSTFLOAT fVslider6;
	float fRec83[2];
	float fConst136;
	float fConst137;
	float fConst138;
	float fConst139;
	float fConst140;
	float fRec90[2];
	float fRec89[3];
	float fRec88[3];
	float fVec9[2];
	float fConst141;
	float fConst142;
	float fConst143;
	float fRec87[2];
	float fConst144;
	float fRec86[3];
	float fConst145;
	float fConst146;
	float fRec85[3];
	float fRec84[3];
	FAUSTFLOAT fVslider7;
	float fRec91[2];
	float fConst147;
	float fConst148;
	float fConst149;
	float fConst150;
	float fConst151;
	float fRec97[2];
	float fRec96[3];
	float fRec95[3];
	float fVec10[2];
	float fConst152;
	float fConst153;
	float fConst154;
	float fRec94[2];
	float fConst155;
	float fRec93[3];
	float fConst156;
	float fConst157;
	float fRec92[3];
	FAUSTFLOAT fVslider8;
	float fRec98[2];
	float fConst158;
	float fRec101[2];
	float fRec100[3];
	float fRec99[3];
	FAUSTFLOAT fVslider9;
	float fRec102[2];
	FAUSTFLOAT fHbargraph0;
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("analyzers.lib/name", "Faust Analyzer Library");
		m->declare("analyzers.lib/version", "0.1");
		m->declare("author", "DT");
		m->declare("basics.lib/name", "Faust Basic Element Library");
		m->declare("basics.lib/version", "0.1");
		m->declare("filename", "karplus_filtrato.dsp");
		m->declare("filters.lib/fir:author", "Julius O. Smith III");
		m->declare("filters.lib/fir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/fir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/highpass:author", "Julius O. Smith III");
		m->declare("filters.lib/highpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/highpass_plus_lowpass:author", "Julius O. Smith III");
		m->declare("filters.lib/highpass_plus_lowpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/highpass_plus_lowpass:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/iir:author", "Julius O. Smith III");
		m->declare("filters.lib/iir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/iir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/lowpass0_highpass1", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/lowpass0_highpass1:author", "Julius O. Smith III");
		m->declare("filters.lib/lowpass:author", "Julius O. Smith III");
		m->declare("filters.lib/lowpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/lowpass:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/mth_octave_filterbank5:author", "Julius O. Smith III");
		m->declare("filters.lib/mth_octave_filterbank5:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/mth_octave_filterbank5:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/mth_octave_filterbank:author", "Julius O. Smith III");
		m->declare("filters.lib/mth_octave_filterbank:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/mth_octave_filterbank:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/mth_octave_filterbank_default:author", "Julius O. Smith III");
		m->declare("filters.lib/mth_octave_filterbank_default:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/mth_octave_filterbank_default:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/name", "Faust Filters Library");
		m->declare("filters.lib/tf1:author", "Julius O. Smith III");
		m->declare("filters.lib/tf1:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf1:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf1s:author", "Julius O. Smith III");
		m->declare("filters.lib/tf1s:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf1s:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf2:author", "Julius O. Smith III");
		m->declare("filters.lib/tf2:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf2:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf2s:author", "Julius O. Smith III");
		m->declare("filters.lib/tf2s:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf2s:license", "MIT-style STK-4.3 license");
		m->declare("license", "BSD");
		m->declare("math.lib/author", "GRAME");
		m->declare("math.lib/copyright", "GRAME");
		m->declare("math.lib/deprecated", "This library is deprecated and is not maintained anymore. It will be removed in August 2017.");
		m->declare("math.lib/license", "LGPL with exception");
		m->declare("math.lib/name", "Math Library");
		m->declare("math.lib/version", "1.0");
		m->declare("maths.lib/author", "GRAME");
		m->declare("maths.lib/copyright", "GRAME");
		m->declare("maths.lib/license", "LGPL with exception");
		m->declare("maths.lib/name", "Faust Math Library");
		m->declare("maths.lib/version", "2.3");
		m->declare("music.lib/author", "GRAME");
		m->declare("music.lib/copyright", "GRAME");
		m->declare("music.lib/deprecated", "This library is deprecated and is not maintained anymore. It will be removed in August 2017.");
		m->declare("music.lib/license", "LGPL with exception");
		m->declare("music.lib/name", "Music Library");
		m->declare("music.lib/version", "1.0");
		m->declare("name", "Karplus_filtrato");
		m->declare("platform.lib/name", "Generic Platform Library");
		m->declare("platform.lib/version", "0.1");
		m->declare("signals.lib/name", "Faust Signal Routing Library");
		m->declare("signals.lib/version", "0.0");
		m->declare("version", "1.1");
	}

	virtual int getNumInputs() {
		return 0;
	}
	virtual int getNumOutputs() {
		return 1;
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch ((channel)) {
			default: {
				rate = -1;
				break;
			}
		}
		return rate;
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch ((channel)) {
			case 0: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
		}
		return rate;
	}
	
	static void classInit(int sample_rate) {
	}
	
	virtual void instanceConstants(int sample_rate) {
		fSampleRate = sample_rate;
		fConst0 = std::min<float>(192000.0f, std::max<float>(1.0f, float(fSampleRate)));
		fConst1 = std::tan((31415.9258f / fConst0));
		fConst2 = (1.0f / fConst1);
		fConst3 = (1.0f / (((fConst2 + 0.618034005f) / fConst1) + 1.0f));
		fConst4 = mydsp_faustpower2_f(fConst1);
		fConst5 = (1.0f / fConst4);
		fConst6 = (1.0f / (((fConst2 + 1.61803401f) / fConst1) + 1.0f));
		fConst7 = (fConst2 + 1.0f);
		fConst8 = (0.0f - (1.0f / (fConst1 * fConst7)));
		fConst9 = (1.0f / fConst7);
		fConst10 = (1.0f - fConst2);
		fConst11 = (((fConst2 + -1.61803401f) / fConst1) + 1.0f);
		fConst12 = (2.0f * (1.0f - fConst5));
		fConst13 = (0.0f - (2.0f / fConst4));
		fConst14 = (((fConst2 + -0.618034005f) / fConst1) + 1.0f);
		fConst15 = std::tan((122.71846f / fConst0));
		fConst16 = (1.0f / fConst15);
		fConst17 = (1.0f / (((fConst16 + 1.61803401f) / fConst15) + 1.0f));
		fConst18 = (((fConst16 + -1.61803401f) / fConst15) + 1.0f);
		fConst19 = mydsp_faustpower2_f(fConst15);
		fConst20 = (1.0f / fConst19);
		fConst21 = (2.0f * (1.0f - fConst20));
		fConst22 = std::tan((245.43692f / fConst0));
		fConst23 = (1.0f / fConst22);
		fConst24 = (1.0f / (((fConst23 + 1.61803401f) / fConst22) + 1.0f));
		fConst25 = (((fConst23 + -1.61803401f) / fConst22) + 1.0f);
		fConst26 = mydsp_faustpower2_f(fConst22);
		fConst27 = (1.0f / fConst26);
		fConst28 = (2.0f * (1.0f - fConst27));
		fConst29 = std::tan((490.87384f / fConst0));
		fConst30 = (1.0f / fConst29);
		fConst31 = (1.0f / (((fConst30 + 1.61803401f) / fConst29) + 1.0f));
		fConst32 = (((fConst30 + -1.61803401f) / fConst29) + 1.0f);
		fConst33 = mydsp_faustpower2_f(fConst29);
		fConst34 = (1.0f / fConst33);
		fConst35 = (2.0f * (1.0f - fConst34));
		fConst36 = std::tan((981.747681f / fConst0));
		fConst37 = (1.0f / fConst36);
		fConst38 = (1.0f / (((fConst37 + 1.61803401f) / fConst36) + 1.0f));
		fConst39 = (((fConst37 + -1.61803401f) / fConst36) + 1.0f);
		fConst40 = mydsp_faustpower2_f(fConst36);
		fConst41 = (1.0f / fConst40);
		fConst42 = (2.0f * (1.0f - fConst41));
		fConst43 = std::tan((1963.49536f / fConst0));
		fConst44 = (1.0f / fConst43);
		fConst45 = (1.0f / (((fConst44 + 1.61803401f) / fConst43) + 1.0f));
		fConst46 = (((fConst44 + -1.61803401f) / fConst43) + 1.0f);
		fConst47 = mydsp_faustpower2_f(fConst43);
		fConst48 = (1.0f / fConst47);
		fConst49 = (2.0f * (1.0f - fConst48));
		fConst50 = std::tan((3926.99072f / fConst0));
		fConst51 = (1.0f / fConst50);
		fConst52 = (1.0f / (((fConst51 + 1.61803401f) / fConst50) + 1.0f));
		fConst53 = (((fConst51 + -1.61803401f) / fConst50) + 1.0f);
		fConst54 = mydsp_faustpower2_f(fConst50);
		fConst55 = (1.0f / fConst54);
		fConst56 = (2.0f * (1.0f - fConst55));
		fConst57 = std::tan((7853.98145f / fConst0));
		fConst58 = (1.0f / fConst57);
		fConst59 = (1.0f / (((fConst58 + 1.61803401f) / fConst57) + 1.0f));
		fConst60 = (((fConst58 + -1.61803401f) / fConst57) + 1.0f);
		fConst61 = mydsp_faustpower2_f(fConst57);
		fConst62 = (1.0f / fConst61);
		fConst63 = (2.0f * (1.0f - fConst62));
		fConst64 = std::tan((15707.9629f / fConst0));
		fConst65 = (1.0f / fConst64);
		fConst66 = (1.0f / (((fConst65 + 1.61803401f) / fConst64) + 1.0f));
		fConst67 = (((fConst65 + -1.61803401f) / fConst64) + 1.0f);
		fConst68 = mydsp_faustpower2_f(fConst64);
		fConst69 = (1.0f / fConst68);
		fConst70 = (2.0f * (1.0f - fConst69));
		fConst71 = (1.0f / (((fConst65 + 0.618034005f) / fConst64) + 1.0f));
		fConst72 = (1.0f / (((fConst65 + 1.61803401f) / fConst64) + 1.0f));
		fConst73 = (fConst65 + 1.0f);
		fConst74 = (1.0f / (fConst64 * fConst73));
		fConst75 = (0.0f - fConst74);
		fConst76 = (1.0f - fConst65);
		fConst77 = (fConst76 / fConst73);
		fConst78 = (((fConst65 + -1.61803401f) / fConst64) + 1.0f);
		fConst79 = (0.0f - (2.0f / fConst68));
		fConst80 = (((fConst65 + -0.618034005f) / fConst64) + 1.0f);
		fConst81 = (1.0f / (((fConst58 + 0.618034005f) / fConst57) + 1.0f));
		fConst82 = (1.0f / (((fConst58 + 1.61803401f) / fConst57) + 1.0f));
		fConst83 = (fConst58 + 1.0f);
		fConst84 = (1.0f / (fConst57 * fConst83));
		fConst85 = (1.0f / fConst73);
		fConst86 = (0.0f - fConst84);
		fConst87 = (1.0f - fConst58);
		fConst88 = (fConst87 / fConst83);
		fConst89 = (((fConst58 + -1.61803401f) / fConst57) + 1.0f);
		fConst90 = (0.0f - (2.0f / fConst61));
		fConst91 = (((fConst58 + -0.618034005f) / fConst57) + 1.0f);
		fConst92 = (1.0f / (((fConst51 + 0.618034005f) / fConst50) + 1.0f));
		fConst93 = (1.0f / (((fConst51 + 1.61803401f) / fConst50) + 1.0f));
		fConst94 = (fConst51 + 1.0f);
		fConst95 = (1.0f / (fConst50 * fConst94));
		fConst96 = (1.0f / fConst83);
		fConst97 = (0.0f - fConst95);
		fConst98 = (1.0f - fConst51);
		fConst99 = (fConst98 / fConst94);
		fConst100 = (((fConst51 + -1.61803401f) / fConst50) + 1.0f);
		fConst101 = (0.0f - (2.0f / fConst54));
		fConst102 = (((fConst51 + -0.618034005f) / fConst50) + 1.0f);
		fConst103 = (1.0f / (((fConst44 + 0.618034005f) / fConst43) + 1.0f));
		fConst104 = (1.0f / (((fConst44 + 1.61803401f) / fConst43) + 1.0f));
		fConst105 = (fConst44 + 1.0f);
		fConst106 = (1.0f / (fConst43 * fConst105));
		fConst107 = (1.0f / fConst94);
		fConst108 = (0.0f - fConst106);
		fConst109 = (1.0f - fConst44);
		fConst110 = (fConst109 / fConst105);
		fConst111 = (((fConst44 + -1.61803401f) / fConst43) + 1.0f);
		fConst112 = (0.0f - (2.0f / fConst47));
		fConst113 = (((fConst44 + -0.618034005f) / fConst43) + 1.0f);
		fConst114 = (1.0f / (((fConst37 + 0.618034005f) / fConst36) + 1.0f));
		fConst115 = (1.0f / (((fConst37 + 1.61803401f) / fConst36) + 1.0f));
		fConst116 = (fConst37 + 1.0f);
		fConst117 = (1.0f / (fConst36 * fConst116));
		fConst118 = (1.0f / fConst105);
		fConst119 = (0.0f - fConst117);
		fConst120 = (1.0f - fConst37);
		fConst121 = (fConst120 / fConst116);
		fConst122 = (((fConst37 + -1.61803401f) / fConst36) + 1.0f);
		fConst123 = (0.0f - (2.0f / fConst40));
		fConst124 = (((fConst37 + -0.618034005f) / fConst36) + 1.0f);
		fConst125 = (1.0f / (((fConst30 + 0.618034005f) / fConst29) + 1.0f));
		fConst126 = (1.0f / (((fConst30 + 1.61803401f) / fConst29) + 1.0f));
		fConst127 = (fConst30 + 1.0f);
		fConst128 = (1.0f / (fConst29 * fConst127));
		fConst129 = (1.0f / fConst116);
		fConst130 = (0.0f - fConst128);
		fConst131 = (1.0f - fConst30);
		fConst132 = (fConst131 / fConst127);
		fConst133 = (((fConst30 + -1.61803401f) / fConst29) + 1.0f);
		fConst134 = (0.0f - (2.0f / fConst33));
		fConst135 = (((fConst30 + -0.618034005f) / fConst29) + 1.0f);
		fConst136 = (1.0f / (((fConst23 + 0.618034005f) / fConst22) + 1.0f));
		fConst137 = (1.0f / (((fConst23 + 1.61803401f) / fConst22) + 1.0f));
		fConst138 = (fConst23 + 1.0f);
		fConst139 = (1.0f / (fConst22 * fConst138));
		fConst140 = (1.0f / fConst127);
		fConst141 = (0.0f - fConst139);
		fConst142 = (1.0f - fConst23);
		fConst143 = (fConst142 / fConst138);
		fConst144 = (((fConst23 + -1.61803401f) / fConst22) + 1.0f);
		fConst145 = (0.0f - (2.0f / fConst26));
		fConst146 = (((fConst23 + -0.618034005f) / fConst22) + 1.0f);
		fConst147 = (1.0f / (((fConst16 + 0.618034005f) / fConst15) + 1.0f));
		fConst148 = (1.0f / (((fConst16 + 1.61803401f) / fConst15) + 1.0f));
		fConst149 = (fConst16 + 1.0f);
		fConst150 = (1.0f / (fConst15 * fConst149));
		fConst151 = (1.0f / fConst138);
		fConst152 = (0.0f - fConst150);
		fConst153 = (1.0f - fConst16);
		fConst154 = (fConst153 / fConst149);
		fConst155 = (((fConst16 + -1.61803401f) / fConst15) + 1.0f);
		fConst156 = (0.0f - (2.0f / fConst19));
		fConst157 = (((fConst16 + -0.618034005f) / fConst15) + 1.0f);
		fConst158 = (1.0f / fConst149);
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(0.10000000000000001f);
		fHslider1 = FAUSTFLOAT(0.5f);
		fButton0 = FAUSTFLOAT(0.0f);
		fHslider2 = FAUSTFLOAT(256.0f);
		fHslider3 = FAUSTFLOAT(256.0f);
		fVslider0 = FAUSTFLOAT(-10.0f);
		fVslider1 = FAUSTFLOAT(-10.0f);
		fVslider2 = FAUSTFLOAT(-10.0f);
		fVslider3 = FAUSTFLOAT(-10.0f);
		fVslider4 = FAUSTFLOAT(-10.0f);
		fVslider5 = FAUSTFLOAT(-10.0f);
		fVslider6 = FAUSTFLOAT(-10.0f);
		fVslider7 = FAUSTFLOAT(-10.0f);
		fVslider8 = FAUSTFLOAT(-10.0f);
		fVslider9 = FAUSTFLOAT(-10.0f);
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fVec0[l0] = 0.0f;
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec12[l1] = 0.0f;
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			iRec13[l2] = 0;
		}
		IOTA = 0;
		for (int l3 = 0; (l3 < 1024); l3 = (l3 + 1)) {
			fVec1[l3] = 0.0f;
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec11[l4] = 0.0f;
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fVec2[l5] = 0.0f;
		}
		for (int l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			fRec10[l6] = 0.0f;
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec9[l7] = 0.0f;
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec8[l8] = 0.0f;
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec7[l9] = 0.0f;
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec6[l10] = 0.0f;
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec5[l11] = 0.0f;
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec4[l12] = 0.0f;
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec3[l13] = 0.0f;
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec2[l14] = 0.0f;
		}
		for (int l15 = 0; (l15 < 3); l15 = (l15 + 1)) {
			fRec1[l15] = 0.0f;
		}
		for (int l16 = 0; (l16 < 3); l16 = (l16 + 1)) {
			fRec0[l16] = 0.0f;
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec14[l17] = 0.0f;
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec27[l18] = 0.0f;
		}
		for (int l19 = 0; (l19 < 3); l19 = (l19 + 1)) {
			fRec26[l19] = 0.0f;
		}
		for (int l20 = 0; (l20 < 3); l20 = (l20 + 1)) {
			fRec25[l20] = 0.0f;
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fVec3[l21] = 0.0f;
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec24[l22] = 0.0f;
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec23[l23] = 0.0f;
		}
		for (int l24 = 0; (l24 < 3); l24 = (l24 + 1)) {
			fRec22[l24] = 0.0f;
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec21[l25] = 0.0f;
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec20[l26] = 0.0f;
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec19[l27] = 0.0f;
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec18[l28] = 0.0f;
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec17[l29] = 0.0f;
		}
		for (int l30 = 0; (l30 < 3); l30 = (l30 + 1)) {
			fRec16[l30] = 0.0f;
		}
		for (int l31 = 0; (l31 < 3); l31 = (l31 + 1)) {
			fRec15[l31] = 0.0f;
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec28[l32] = 0.0f;
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec40[l33] = 0.0f;
		}
		for (int l34 = 0; (l34 < 3); l34 = (l34 + 1)) {
			fRec39[l34] = 0.0f;
		}
		for (int l35 = 0; (l35 < 3); l35 = (l35 + 1)) {
			fRec38[l35] = 0.0f;
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fVec4[l36] = 0.0f;
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec37[l37] = 0.0f;
		}
		for (int l38 = 0; (l38 < 3); l38 = (l38 + 1)) {
			fRec36[l38] = 0.0f;
		}
		for (int l39 = 0; (l39 < 3); l39 = (l39 + 1)) {
			fRec35[l39] = 0.0f;
		}
		for (int l40 = 0; (l40 < 3); l40 = (l40 + 1)) {
			fRec34[l40] = 0.0f;
		}
		for (int l41 = 0; (l41 < 3); l41 = (l41 + 1)) {
			fRec33[l41] = 0.0f;
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec32[l42] = 0.0f;
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec31[l43] = 0.0f;
		}
		for (int l44 = 0; (l44 < 3); l44 = (l44 + 1)) {
			fRec30[l44] = 0.0f;
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec29[l45] = 0.0f;
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec41[l46] = 0.0f;
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec52[l47] = 0.0f;
		}
		for (int l48 = 0; (l48 < 3); l48 = (l48 + 1)) {
			fRec51[l48] = 0.0f;
		}
		for (int l49 = 0; (l49 < 3); l49 = (l49 + 1)) {
			fRec50[l49] = 0.0f;
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fVec5[l50] = 0.0f;
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec49[l51] = 0.0f;
		}
		for (int l52 = 0; (l52 < 3); l52 = (l52 + 1)) {
			fRec48[l52] = 0.0f;
		}
		for (int l53 = 0; (l53 < 3); l53 = (l53 + 1)) {
			fRec47[l53] = 0.0f;
		}
		for (int l54 = 0; (l54 < 3); l54 = (l54 + 1)) {
			fRec46[l54] = 0.0f;
		}
		for (int l55 = 0; (l55 < 3); l55 = (l55 + 1)) {
			fRec45[l55] = 0.0f;
		}
		for (int l56 = 0; (l56 < 3); l56 = (l56 + 1)) {
			fRec44[l56] = 0.0f;
		}
		for (int l57 = 0; (l57 < 3); l57 = (l57 + 1)) {
			fRec43[l57] = 0.0f;
		}
		for (int l58 = 0; (l58 < 3); l58 = (l58 + 1)) {
			fRec42[l58] = 0.0f;
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec53[l59] = 0.0f;
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec63[l60] = 0.0f;
		}
		for (int l61 = 0; (l61 < 3); l61 = (l61 + 1)) {
			fRec62[l61] = 0.0f;
		}
		for (int l62 = 0; (l62 < 3); l62 = (l62 + 1)) {
			fRec61[l62] = 0.0f;
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fVec6[l63] = 0.0f;
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec60[l64] = 0.0f;
		}
		for (int l65 = 0; (l65 < 3); l65 = (l65 + 1)) {
			fRec59[l65] = 0.0f;
		}
		for (int l66 = 0; (l66 < 3); l66 = (l66 + 1)) {
			fRec58[l66] = 0.0f;
		}
		for (int l67 = 0; (l67 < 3); l67 = (l67 + 1)) {
			fRec57[l67] = 0.0f;
		}
		for (int l68 = 0; (l68 < 3); l68 = (l68 + 1)) {
			fRec56[l68] = 0.0f;
		}
		for (int l69 = 0; (l69 < 3); l69 = (l69 + 1)) {
			fRec55[l69] = 0.0f;
		}
		for (int l70 = 0; (l70 < 3); l70 = (l70 + 1)) {
			fRec54[l70] = 0.0f;
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec64[l71] = 0.0f;
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec73[l72] = 0.0f;
		}
		for (int l73 = 0; (l73 < 3); l73 = (l73 + 1)) {
			fRec72[l73] = 0.0f;
		}
		for (int l74 = 0; (l74 < 3); l74 = (l74 + 1)) {
			fRec71[l74] = 0.0f;
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fVec7[l75] = 0.0f;
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec70[l76] = 0.0f;
		}
		for (int l77 = 0; (l77 < 3); l77 = (l77 + 1)) {
			fRec69[l77] = 0.0f;
		}
		for (int l78 = 0; (l78 < 3); l78 = (l78 + 1)) {
			fRec68[l78] = 0.0f;
		}
		for (int l79 = 0; (l79 < 3); l79 = (l79 + 1)) {
			fRec67[l79] = 0.0f;
		}
		for (int l80 = 0; (l80 < 3); l80 = (l80 + 1)) {
			fRec66[l80] = 0.0f;
		}
		for (int l81 = 0; (l81 < 3); l81 = (l81 + 1)) {
			fRec65[l81] = 0.0f;
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec74[l82] = 0.0f;
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec82[l83] = 0.0f;
		}
		for (int l84 = 0; (l84 < 3); l84 = (l84 + 1)) {
			fRec81[l84] = 0.0f;
		}
		for (int l85 = 0; (l85 < 3); l85 = (l85 + 1)) {
			fRec80[l85] = 0.0f;
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fVec8[l86] = 0.0f;
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec79[l87] = 0.0f;
		}
		for (int l88 = 0; (l88 < 3); l88 = (l88 + 1)) {
			fRec78[l88] = 0.0f;
		}
		for (int l89 = 0; (l89 < 3); l89 = (l89 + 1)) {
			fRec77[l89] = 0.0f;
		}
		for (int l90 = 0; (l90 < 3); l90 = (l90 + 1)) {
			fRec76[l90] = 0.0f;
		}
		for (int l91 = 0; (l91 < 3); l91 = (l91 + 1)) {
			fRec75[l91] = 0.0f;
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec83[l92] = 0.0f;
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec90[l93] = 0.0f;
		}
		for (int l94 = 0; (l94 < 3); l94 = (l94 + 1)) {
			fRec89[l94] = 0.0f;
		}
		for (int l95 = 0; (l95 < 3); l95 = (l95 + 1)) {
			fRec88[l95] = 0.0f;
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fVec9[l96] = 0.0f;
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec87[l97] = 0.0f;
		}
		for (int l98 = 0; (l98 < 3); l98 = (l98 + 1)) {
			fRec86[l98] = 0.0f;
		}
		for (int l99 = 0; (l99 < 3); l99 = (l99 + 1)) {
			fRec85[l99] = 0.0f;
		}
		for (int l100 = 0; (l100 < 3); l100 = (l100 + 1)) {
			fRec84[l100] = 0.0f;
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec91[l101] = 0.0f;
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec97[l102] = 0.0f;
		}
		for (int l103 = 0; (l103 < 3); l103 = (l103 + 1)) {
			fRec96[l103] = 0.0f;
		}
		for (int l104 = 0; (l104 < 3); l104 = (l104 + 1)) {
			fRec95[l104] = 0.0f;
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fVec10[l105] = 0.0f;
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec94[l106] = 0.0f;
		}
		for (int l107 = 0; (l107 < 3); l107 = (l107 + 1)) {
			fRec93[l107] = 0.0f;
		}
		for (int l108 = 0; (l108 < 3); l108 = (l108 + 1)) {
			fRec92[l108] = 0.0f;
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec98[l109] = 0.0f;
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec101[l110] = 0.0f;
		}
		for (int l111 = 0; (l111 < 3); l111 = (l111 + 1)) {
			fRec100[l111] = 0.0f;
		}
		for (int l112 = 0; (l112 < 3); l112 = (l112 + 1)) {
			fRec99[l112] = 0.0f;
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec102[l113] = 0.0f;
		}
	}
	
	virtual void init(int sample_rate) {
		classInit(sample_rate);
		instanceInit(sample_rate);
	}
	virtual void instanceInit(int sample_rate) {
		instanceConstants(sample_rate);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	
	virtual int getSampleRate() {
		return fSampleRate;
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("Karplus_filtrato");
		ui_interface->declare(0, "tooltip", "See Faust's filters.lib for documentation and references");
		ui_interface->openVerticalBox("CONSTANT-Q FILTER BANK (Butterworth dyadic tree)");
		ui_interface->declare(0, "0", "");
		ui_interface->openHorizontalBox("0x00");
		ui_interface->declare(&fCheckbox0, "0", "");
		ui_interface->declare(&fCheckbox0, "tooltip", "When this is checked, the filter-bank has no effect");
		ui_interface->addCheckButton("Bypass", &fCheckbox0);
		ui_interface->closeBox();
		ui_interface->declare(0, "1", "");
		ui_interface->openHorizontalBox("0x00");
		ui_interface->declare(&fVslider9, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider9, "unit", "dB");
		ui_interface->addVerticalSlider("Band 1", &fVslider9, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider8, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider8, "unit", "dB");
		ui_interface->addVerticalSlider("Band 2", &fVslider8, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider7, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider7, "unit", "dB");
		ui_interface->addVerticalSlider("Band 3", &fVslider7, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider6, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider6, "unit", "dB");
		ui_interface->addVerticalSlider("Band 4", &fVslider6, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider5, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider5, "unit", "dB");
		ui_interface->addVerticalSlider("Band 5", &fVslider5, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider4, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider4, "unit", "dB");
		ui_interface->addVerticalSlider("Band 6", &fVslider4, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider3, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider3, "unit", "dB");
		ui_interface->addVerticalSlider("Band 7", &fVslider3, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider2, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider2, "unit", "dB");
		ui_interface->addVerticalSlider("Band 8", &fVslider2, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider1, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider1, "unit", "dB");
		ui_interface->addVerticalSlider("Band 9", &fVslider1, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->declare(&fVslider0, "tooltip", "Bandpass filter   gain in dB");
		ui_interface->declare(&fVslider0, "unit", "dB");
		ui_interface->addVerticalSlider("Band10", &fVslider0, -10.0f, -70.0f, 10.0f, 0.100000001f);
		ui_interface->closeBox();
		ui_interface->closeBox();
		ui_interface->openVerticalBox("Eccitatore");
		ui_interface->addHorizontalSlider("excitation (samples)", &fHslider2, 256.0f, 2.0f, 1024.0f, 1.0f);
		ui_interface->addButton("play", &fButton0);
		ui_interface->closeBox();
		ui_interface->declare(&fHbargraph0, "style", "dB");
		ui_interface->addHorizontalBargraph("Level", &fHbargraph0, -60.0f, 0.0f);
		ui_interface->openVerticalBox("Risuonatore");
		ui_interface->addHorizontalSlider("attenuation", &fHslider0, 0.100000001f, 0.0f, 1.0f, 0.00999999978f);
		ui_interface->addHorizontalSlider("duration (samples)", &fHslider3, 256.0f, 2.0f, 1024.0f, 1.0f);
		ui_interface->closeBox();
		ui_interface->addHorizontalSlider("Volume", &fHslider1, 0.5f, 0.0f, 1.0f, 0.00999999978f);
		ui_interface->closeBox();
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* output0 = outputs[0];
		int iSlow0 = int(float(fCheckbox0));
		float fSlow1 = (0.5f * (1.0f - float(fHslider0)));
		float fSlow2 = (4.65661287e-10f * float(fHslider1));
		float fSlow3 = float(fButton0);
		float fSlow4 = (1.0f / float(fHslider2));
		int iSlow5 = (int((float(fHslider3) + -1.5f)) & 4095);
		float fSlow6 = (0.00100000005f * float(fVslider0));
		float fSlow7 = (0.00100000005f * float(fVslider1));
		float fSlow8 = (0.00100000005f * float(fVslider2));
		float fSlow9 = (0.00100000005f * float(fVslider3));
		float fSlow10 = (0.00100000005f * float(fVslider4));
		float fSlow11 = (0.00100000005f * float(fVslider5));
		float fSlow12 = (0.00100000005f * float(fVslider6));
		float fSlow13 = (0.00100000005f * float(fVslider7));
		float fSlow14 = (0.00100000005f * float(fVslider8));
		float fSlow15 = (0.00100000005f * float(fVslider9));
		for (int i = 0; (i < count); i = (i + 1)) {
			fVec0[0] = fSlow3;
			fRec12[0] = ((fRec12[1] + float(((fSlow3 - fVec0[1]) > 0.0f))) - (fSlow4 * float((fRec12[1] > 0.0f))));
			iRec13[0] = ((1103515245 * iRec13[1]) + 12345);
			fVec1[(IOTA & 1023)] = ((fSlow1 * (fRec11[1] + fRec11[2])) + (fSlow2 * (float((fRec12[0] > 0.0f)) * float(iRec13[0]))));
			fRec11[0] = fVec1[((IOTA - iSlow5) & 1023)];
			float fTemp0 = (iSlow0 ? 0.0f : fRec11[0]);
			fVec2[0] = fTemp0;
			fRec10[0] = ((fConst8 * fVec2[1]) - (fConst9 * ((fConst10 * fRec10[1]) - (fConst2 * fTemp0))));
			fRec9[0] = (fRec10[0] - (fConst6 * ((fConst11 * fRec9[2]) + (fConst12 * fRec9[1]))));
			fRec8[0] = ((fConst6 * (((fConst5 * fRec9[0]) + (fConst13 * fRec9[1])) + (fConst5 * fRec9[2]))) - (fConst3 * ((fConst14 * fRec8[2]) + (fConst12 * fRec8[1]))));
			float fTemp1 = (fConst21 * fRec7[1]);
			fRec7[0] = ((fConst3 * (((fConst5 * fRec8[0]) + (fConst13 * fRec8[1])) + (fConst5 * fRec8[2]))) - (fConst17 * ((fConst18 * fRec7[2]) + fTemp1)));
			float fTemp2 = (fConst28 * fRec6[1]);
			fRec6[0] = ((fRec7[2] + (fConst17 * (fTemp1 + (fConst18 * fRec7[0])))) - (fConst24 * ((fConst25 * fRec6[2]) + fTemp2)));
			float fTemp3 = (fConst35 * fRec5[1]);
			fRec5[0] = ((fRec6[2] + (fConst24 * (fTemp2 + (fConst25 * fRec6[0])))) - (fConst31 * ((fConst32 * fRec5[2]) + fTemp3)));
			float fTemp4 = (fConst42 * fRec4[1]);
			fRec4[0] = ((fRec5[2] + (fConst31 * (fTemp3 + (fConst32 * fRec5[0])))) - (fConst38 * ((fConst39 * fRec4[2]) + fTemp4)));
			float fTemp5 = (fConst49 * fRec3[1]);
			fRec3[0] = ((fRec4[2] + (fConst38 * (fTemp4 + (fConst39 * fRec4[0])))) - (fConst45 * ((fConst46 * fRec3[2]) + fTemp5)));
			float fTemp6 = (fConst56 * fRec2[1]);
			fRec2[0] = ((fRec3[2] + (fConst45 * (fTemp5 + (fConst46 * fRec3[0])))) - (fConst52 * ((fConst53 * fRec2[2]) + fTemp6)));
			float fTemp7 = (fConst63 * fRec1[1]);
			fRec1[0] = ((fRec2[2] + (fConst52 * (fTemp6 + (fConst53 * fRec2[0])))) - (fConst59 * ((fConst60 * fRec1[2]) + fTemp7)));
			float fTemp8 = (fConst70 * fRec0[1]);
			fRec0[0] = ((fRec1[2] + (fConst59 * (fTemp7 + (fConst60 * fRec1[0])))) - (fConst66 * ((fConst67 * fRec0[2]) + fTemp8)));
			fRec14[0] = (fSlow6 + (0.999000013f * fRec14[1]));
			fRec27[0] = (0.0f - (fConst9 * ((fConst10 * fRec27[1]) - (fTemp0 + fVec2[1]))));
			fRec26[0] = (fRec27[0] - (fConst6 * ((fConst11 * fRec26[2]) + (fConst12 * fRec26[1]))));
			fRec25[0] = ((fConst6 * (fRec26[2] + (fRec26[0] + (2.0f * fRec26[1])))) - (fConst3 * ((fConst14 * fRec25[2]) + (fConst12 * fRec25[1]))));
			float fTemp9 = (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])));
			fVec3[0] = fTemp9;
			fRec24[0] = ((fConst3 * ((fConst74 * fTemp9) + (fConst75 * fVec3[1]))) - (fConst77 * fRec24[1]));
			fRec23[0] = (fRec24[0] - (fConst72 * ((fConst78 * fRec23[2]) + (fConst70 * fRec23[1]))));
			fRec22[0] = ((fConst72 * (((fConst69 * fRec23[0]) + (fConst79 * fRec23[1])) + (fConst69 * fRec23[2]))) - (fConst71 * ((fConst80 * fRec22[2]) + (fConst70 * fRec22[1]))));
			float fTemp10 = (fConst21 * fRec21[1]);
			fRec21[0] = ((fConst71 * (((fConst69 * fRec22[0]) + (fConst79 * fRec22[1])) + (fConst69 * fRec22[2]))) - (fConst17 * ((fConst18 * fRec21[2]) + fTemp10)));
			float fTemp11 = (fConst28 * fRec20[1]);
			fRec20[0] = ((fRec21[2] + (fConst17 * (fTemp10 + (fConst18 * fRec21[0])))) - (fConst24 * ((fConst25 * fRec20[2]) + fTemp11)));
			float fTemp12 = (fConst35 * fRec19[1]);
			fRec19[0] = ((fRec20[2] + (fConst24 * (fTemp11 + (fConst25 * fRec20[0])))) - (fConst31 * ((fConst32 * fRec19[2]) + fTemp12)));
			float fTemp13 = (fConst42 * fRec18[1]);
			fRec18[0] = ((fRec19[2] + (fConst31 * (fTemp12 + (fConst32 * fRec19[0])))) - (fConst38 * ((fConst39 * fRec18[2]) + fTemp13)));
			float fTemp14 = (fConst49 * fRec17[1]);
			fRec17[0] = ((fRec18[2] + (fConst38 * (fTemp13 + (fConst39 * fRec18[0])))) - (fConst45 * ((fConst46 * fRec17[2]) + fTemp14)));
			float fTemp15 = (fConst56 * fRec16[1]);
			fRec16[0] = ((fRec17[2] + (fConst45 * (fTemp14 + (fConst46 * fRec17[0])))) - (fConst52 * ((fConst53 * fRec16[2]) + fTemp15)));
			float fTemp16 = (fConst63 * fRec15[1]);
			fRec15[0] = ((fRec16[2] + (fConst52 * (fTemp15 + (fConst53 * fRec16[0])))) - (fConst59 * ((fConst60 * fRec15[2]) + fTemp16)));
			fRec28[0] = (fSlow7 + (0.999000013f * fRec28[1]));
			fRec40[0] = (0.0f - (fConst85 * ((fConst76 * fRec40[1]) - (fConst3 * (fTemp9 + fVec3[1])))));
			fRec39[0] = (fRec40[0] - (fConst72 * ((fConst78 * fRec39[2]) + (fConst70 * fRec39[1]))));
			fRec38[0] = ((fConst72 * (fRec39[2] + (fRec39[0] + (2.0f * fRec39[1])))) - (fConst71 * ((fConst80 * fRec38[2]) + (fConst70 * fRec38[1]))));
			float fTemp17 = (fRec38[2] + (fRec38[0] + (2.0f * fRec38[1])));
			fVec4[0] = fTemp17;
			fRec37[0] = ((fConst71 * ((fConst84 * fTemp17) + (fConst86 * fVec4[1]))) - (fConst88 * fRec37[1]));
			fRec36[0] = (fRec37[0] - (fConst82 * ((fConst89 * fRec36[2]) + (fConst63 * fRec36[1]))));
			fRec35[0] = ((fConst82 * (((fConst62 * fRec36[0]) + (fConst90 * fRec36[1])) + (fConst62 * fRec36[2]))) - (fConst81 * ((fConst91 * fRec35[2]) + (fConst63 * fRec35[1]))));
			float fTemp18 = (fConst21 * fRec34[1]);
			fRec34[0] = ((fConst81 * (((fConst62 * fRec35[0]) + (fConst90 * fRec35[1])) + (fConst62 * fRec35[2]))) - (fConst17 * ((fConst18 * fRec34[2]) + fTemp18)));
			float fTemp19 = (fConst28 * fRec33[1]);
			fRec33[0] = ((fRec34[2] + (fConst17 * (fTemp18 + (fConst18 * fRec34[0])))) - (fConst24 * ((fConst25 * fRec33[2]) + fTemp19)));
			float fTemp20 = (fConst35 * fRec32[1]);
			fRec32[0] = ((fRec33[2] + (fConst24 * (fTemp19 + (fConst25 * fRec33[0])))) - (fConst31 * ((fConst32 * fRec32[2]) + fTemp20)));
			float fTemp21 = (fConst42 * fRec31[1]);
			fRec31[0] = ((fRec32[2] + (fConst31 * (fTemp20 + (fConst32 * fRec32[0])))) - (fConst38 * ((fConst39 * fRec31[2]) + fTemp21)));
			float fTemp22 = (fConst49 * fRec30[1]);
			fRec30[0] = ((fRec31[2] + (fConst38 * (fTemp21 + (fConst39 * fRec31[0])))) - (fConst45 * ((fConst46 * fRec30[2]) + fTemp22)));
			float fTemp23 = (fConst56 * fRec29[1]);
			fRec29[0] = ((fRec30[2] + (fConst45 * (fTemp22 + (fConst46 * fRec30[0])))) - (fConst52 * ((fConst53 * fRec29[2]) + fTemp23)));
			fRec41[0] = (fSlow8 + (0.999000013f * fRec41[1]));
			fRec52[0] = (0.0f - (fConst96 * ((fConst87 * fRec52[1]) - (fConst71 * (fTemp17 + fVec4[1])))));
			fRec51[0] = (fRec52[0] - (fConst82 * ((fConst89 * fRec51[2]) + (fConst63 * fRec51[1]))));
			fRec50[0] = ((fConst82 * (fRec51[2] + (fRec51[0] + (2.0f * fRec51[1])))) - (fConst81 * ((fConst91 * fRec50[2]) + (fConst63 * fRec50[1]))));
			float fTemp24 = (fRec50[2] + (fRec50[0] + (2.0f * fRec50[1])));
			fVec5[0] = fTemp24;
			fRec49[0] = ((fConst81 * ((fConst95 * fTemp24) + (fConst97 * fVec5[1]))) - (fConst99 * fRec49[1]));
			fRec48[0] = (fRec49[0] - (fConst93 * ((fConst100 * fRec48[2]) + (fConst56 * fRec48[1]))));
			fRec47[0] = ((fConst93 * (((fConst55 * fRec48[0]) + (fConst101 * fRec48[1])) + (fConst55 * fRec48[2]))) - (fConst92 * ((fConst102 * fRec47[2]) + (fConst56 * fRec47[1]))));
			float fTemp25 = (fConst21 * fRec46[1]);
			fRec46[0] = ((fConst92 * (((fConst55 * fRec47[0]) + (fConst101 * fRec47[1])) + (fConst55 * fRec47[2]))) - (fConst17 * ((fConst18 * fRec46[2]) + fTemp25)));
			float fTemp26 = (fConst28 * fRec45[1]);
			fRec45[0] = ((fRec46[2] + (fConst17 * (fTemp25 + (fConst18 * fRec46[0])))) - (fConst24 * ((fConst25 * fRec45[2]) + fTemp26)));
			float fTemp27 = (fConst35 * fRec44[1]);
			fRec44[0] = ((fRec45[2] + (fConst24 * (fTemp26 + (fConst25 * fRec45[0])))) - (fConst31 * ((fConst32 * fRec44[2]) + fTemp27)));
			float fTemp28 = (fConst42 * fRec43[1]);
			fRec43[0] = ((fRec44[2] + (fConst31 * (fTemp27 + (fConst32 * fRec44[0])))) - (fConst38 * ((fConst39 * fRec43[2]) + fTemp28)));
			float fTemp29 = (fConst49 * fRec42[1]);
			fRec42[0] = ((fRec43[2] + (fConst38 * (fTemp28 + (fConst39 * fRec43[0])))) - (fConst45 * ((fConst46 * fRec42[2]) + fTemp29)));
			fRec53[0] = (fSlow9 + (0.999000013f * fRec53[1]));
			fRec63[0] = (0.0f - (fConst107 * ((fConst98 * fRec63[1]) - (fConst81 * (fTemp24 + fVec5[1])))));
			fRec62[0] = (fRec63[0] - (fConst93 * ((fConst100 * fRec62[2]) + (fConst56 * fRec62[1]))));
			fRec61[0] = ((fConst93 * (fRec62[2] + (fRec62[0] + (2.0f * fRec62[1])))) - (fConst92 * ((fConst102 * fRec61[2]) + (fConst56 * fRec61[1]))));
			float fTemp30 = (fRec61[2] + (fRec61[0] + (2.0f * fRec61[1])));
			fVec6[0] = fTemp30;
			fRec60[0] = ((fConst92 * ((fConst106 * fTemp30) + (fConst108 * fVec6[1]))) - (fConst110 * fRec60[1]));
			fRec59[0] = (fRec60[0] - (fConst104 * ((fConst111 * fRec59[2]) + (fConst49 * fRec59[1]))));
			fRec58[0] = ((fConst104 * (((fConst48 * fRec59[0]) + (fConst112 * fRec59[1])) + (fConst48 * fRec59[2]))) - (fConst103 * ((fConst113 * fRec58[2]) + (fConst49 * fRec58[1]))));
			float fTemp31 = (fConst21 * fRec57[1]);
			fRec57[0] = ((fConst103 * (((fConst48 * fRec58[0]) + (fConst112 * fRec58[1])) + (fConst48 * fRec58[2]))) - (fConst17 * ((fConst18 * fRec57[2]) + fTemp31)));
			float fTemp32 = (fConst28 * fRec56[1]);
			fRec56[0] = ((fRec57[2] + (fConst17 * (fTemp31 + (fConst18 * fRec57[0])))) - (fConst24 * ((fConst25 * fRec56[2]) + fTemp32)));
			float fTemp33 = (fConst35 * fRec55[1]);
			fRec55[0] = ((fRec56[2] + (fConst24 * (fTemp32 + (fConst25 * fRec56[0])))) - (fConst31 * ((fConst32 * fRec55[2]) + fTemp33)));
			float fTemp34 = (fConst42 * fRec54[1]);
			fRec54[0] = ((fRec55[2] + (fConst31 * (fTemp33 + (fConst32 * fRec55[0])))) - (fConst38 * ((fConst39 * fRec54[2]) + fTemp34)));
			fRec64[0] = (fSlow10 + (0.999000013f * fRec64[1]));
			fRec73[0] = (0.0f - (fConst118 * ((fConst109 * fRec73[1]) - (fConst92 * (fTemp30 + fVec6[1])))));
			fRec72[0] = (fRec73[0] - (fConst104 * ((fConst111 * fRec72[2]) + (fConst49 * fRec72[1]))));
			fRec71[0] = ((fConst104 * (fRec72[2] + (fRec72[0] + (2.0f * fRec72[1])))) - (fConst103 * ((fConst113 * fRec71[2]) + (fConst49 * fRec71[1]))));
			float fTemp35 = (fRec71[2] + (fRec71[0] + (2.0f * fRec71[1])));
			fVec7[0] = fTemp35;
			fRec70[0] = ((fConst103 * ((fConst117 * fTemp35) + (fConst119 * fVec7[1]))) - (fConst121 * fRec70[1]));
			fRec69[0] = (fRec70[0] - (fConst115 * ((fConst122 * fRec69[2]) + (fConst42 * fRec69[1]))));
			fRec68[0] = ((fConst115 * (((fConst41 * fRec69[0]) + (fConst123 * fRec69[1])) + (fConst41 * fRec69[2]))) - (fConst114 * ((fConst124 * fRec68[2]) + (fConst42 * fRec68[1]))));
			float fTemp36 = (fConst21 * fRec67[1]);
			fRec67[0] = ((fConst114 * (((fConst41 * fRec68[0]) + (fConst123 * fRec68[1])) + (fConst41 * fRec68[2]))) - (fConst17 * ((fConst18 * fRec67[2]) + fTemp36)));
			float fTemp37 = (fConst28 * fRec66[1]);
			fRec66[0] = ((fRec67[2] + (fConst17 * (fTemp36 + (fConst18 * fRec67[0])))) - (fConst24 * ((fConst25 * fRec66[2]) + fTemp37)));
			float fTemp38 = (fConst35 * fRec65[1]);
			fRec65[0] = ((fRec66[2] + (fConst24 * (fTemp37 + (fConst25 * fRec66[0])))) - (fConst31 * ((fConst32 * fRec65[2]) + fTemp38)));
			fRec74[0] = (fSlow11 + (0.999000013f * fRec74[1]));
			fRec82[0] = (0.0f - (fConst129 * ((fConst120 * fRec82[1]) - (fConst103 * (fTemp35 + fVec7[1])))));
			fRec81[0] = (fRec82[0] - (fConst115 * ((fConst122 * fRec81[2]) + (fConst42 * fRec81[1]))));
			fRec80[0] = ((fConst115 * (fRec81[2] + (fRec81[0] + (2.0f * fRec81[1])))) - (fConst114 * ((fConst124 * fRec80[2]) + (fConst42 * fRec80[1]))));
			float fTemp39 = (fRec80[2] + (fRec80[0] + (2.0f * fRec80[1])));
			fVec8[0] = fTemp39;
			fRec79[0] = ((fConst114 * ((fConst128 * fTemp39) + (fConst130 * fVec8[1]))) - (fConst132 * fRec79[1]));
			fRec78[0] = (fRec79[0] - (fConst126 * ((fConst133 * fRec78[2]) + (fConst35 * fRec78[1]))));
			fRec77[0] = ((fConst126 * (((fConst34 * fRec78[0]) + (fConst134 * fRec78[1])) + (fConst34 * fRec78[2]))) - (fConst125 * ((fConst135 * fRec77[2]) + (fConst35 * fRec77[1]))));
			float fTemp40 = (fConst21 * fRec76[1]);
			fRec76[0] = ((fConst125 * (((fConst34 * fRec77[0]) + (fConst134 * fRec77[1])) + (fConst34 * fRec77[2]))) - (fConst17 * ((fConst18 * fRec76[2]) + fTemp40)));
			float fTemp41 = (fConst28 * fRec75[1]);
			fRec75[0] = ((fRec76[2] + (fConst17 * (fTemp40 + (fConst18 * fRec76[0])))) - (fConst24 * ((fConst25 * fRec75[2]) + fTemp41)));
			fRec83[0] = (fSlow12 + (0.999000013f * fRec83[1]));
			fRec90[0] = (0.0f - (fConst140 * ((fConst131 * fRec90[1]) - (fConst114 * (fTemp39 + fVec8[1])))));
			fRec89[0] = (fRec90[0] - (fConst126 * ((fConst133 * fRec89[2]) + (fConst35 * fRec89[1]))));
			fRec88[0] = ((fConst126 * (fRec89[2] + (fRec89[0] + (2.0f * fRec89[1])))) - (fConst125 * ((fConst135 * fRec88[2]) + (fConst35 * fRec88[1]))));
			float fTemp42 = (fRec88[2] + (fRec88[0] + (2.0f * fRec88[1])));
			fVec9[0] = fTemp42;
			fRec87[0] = ((fConst125 * ((fConst139 * fTemp42) + (fConst141 * fVec9[1]))) - (fConst143 * fRec87[1]));
			fRec86[0] = (fRec87[0] - (fConst137 * ((fConst144 * fRec86[2]) + (fConst28 * fRec86[1]))));
			fRec85[0] = ((fConst137 * (((fConst27 * fRec86[0]) + (fConst145 * fRec86[1])) + (fConst27 * fRec86[2]))) - (fConst136 * ((fConst146 * fRec85[2]) + (fConst28 * fRec85[1]))));
			float fTemp43 = (fConst21 * fRec84[1]);
			fRec84[0] = ((fConst136 * (((fConst27 * fRec85[0]) + (fConst145 * fRec85[1])) + (fConst27 * fRec85[2]))) - (fConst17 * ((fConst18 * fRec84[2]) + fTemp43)));
			fRec91[0] = (fSlow13 + (0.999000013f * fRec91[1]));
			fRec97[0] = (0.0f - (fConst151 * ((fConst142 * fRec97[1]) - (fConst125 * (fTemp42 + fVec9[1])))));
			fRec96[0] = (fRec97[0] - (fConst137 * ((fConst144 * fRec96[2]) + (fConst28 * fRec96[1]))));
			fRec95[0] = ((fConst137 * (fRec96[2] + (fRec96[0] + (2.0f * fRec96[1])))) - (fConst136 * ((fConst146 * fRec95[2]) + (fConst28 * fRec95[1]))));
			float fTemp44 = (fRec95[2] + (fRec95[0] + (2.0f * fRec95[1])));
			fVec10[0] = fTemp44;
			fRec94[0] = ((fConst136 * ((fConst150 * fTemp44) + (fConst152 * fVec10[1]))) - (fConst154 * fRec94[1]));
			fRec93[0] = (fRec94[0] - (fConst148 * ((fConst155 * fRec93[2]) + (fConst21 * fRec93[1]))));
			fRec92[0] = ((fConst148 * (((fConst20 * fRec93[0]) + (fConst156 * fRec93[1])) + (fConst20 * fRec93[2]))) - (fConst147 * ((fConst157 * fRec92[2]) + (fConst21 * fRec92[1]))));
			fRec98[0] = (fSlow14 + (0.999000013f * fRec98[1]));
			fRec101[0] = (0.0f - (fConst158 * ((fConst153 * fRec101[1]) - (fConst136 * (fTemp44 + fVec10[1])))));
			fRec100[0] = (fRec101[0] - (fConst148 * ((fConst155 * fRec100[2]) + (fConst21 * fRec100[1]))));
			fRec99[0] = ((fConst148 * (fRec100[2] + (fRec100[0] + (2.0f * fRec100[1])))) - (fConst147 * ((fConst157 * fRec99[2]) + (fConst21 * fRec99[1]))));
			fRec102[0] = (fSlow15 + (0.999000013f * fRec102[1]));
			float fTemp45 = (iSlow0 ? fRec11[0] : ((((((((((fRec0[2] + (fConst66 * (fTemp8 + (fConst67 * fRec0[0])))) * std::pow(10.0f, (0.0500000007f * fRec14[0]))) + ((fRec15[2] + (fConst59 * (fTemp16 + (fConst60 * fRec15[0])))) * std::pow(10.0f, (0.0500000007f * fRec28[0])))) + ((fRec29[2] + (fConst52 * (fTemp23 + (fConst53 * fRec29[0])))) * std::pow(10.0f, (0.0500000007f * fRec41[0])))) + ((fRec42[2] + (fConst45 * (fTemp29 + (fConst46 * fRec42[0])))) * std::pow(10.0f, (0.0500000007f * fRec53[0])))) + ((fRec54[2] + (fConst38 * (fTemp34 + (fConst39 * fRec54[0])))) * std::pow(10.0f, (0.0500000007f * fRec64[0])))) + ((fRec65[2] + (fConst31 * (fTemp38 + (fConst32 * fRec65[0])))) * std::pow(10.0f, (0.0500000007f * fRec74[0])))) + ((fRec75[2] + (fConst24 * (fTemp41 + (fConst25 * fRec75[0])))) * std::pow(10.0f, (0.0500000007f * fRec83[0])))) + ((fRec84[2] + (fConst17 * (fTemp43 + (fConst18 * fRec84[0])))) * std::pow(10.0f, (0.0500000007f * fRec91[0])))) + (fConst147 * (((((fConst20 * fRec92[0]) + (fConst156 * fRec92[1])) + (fConst20 * fRec92[2])) * std::pow(10.0f, (0.0500000007f * fRec98[0]))) + ((fRec99[2] + (fRec99[0] + (2.0f * fRec99[1]))) * std::pow(10.0f, (0.0500000007f * fRec102[0])))))));
			fHbargraph0 = FAUSTFLOAT((20.0f * std::log10(std::fabs(fTemp45))));
			output0[i] = FAUSTFLOAT(fTemp45);
			fVec0[1] = fVec0[0];
			fRec12[1] = fRec12[0];
			iRec13[1] = iRec13[0];
			IOTA = (IOTA + 1);
			fRec11[2] = fRec11[1];
			fRec11[1] = fRec11[0];
			fVec2[1] = fVec2[0];
			fRec10[1] = fRec10[0];
			fRec9[2] = fRec9[1];
			fRec9[1] = fRec9[0];
			fRec8[2] = fRec8[1];
			fRec8[1] = fRec8[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec6[2] = fRec6[1];
			fRec6[1] = fRec6[0];
			fRec5[2] = fRec5[1];
			fRec5[1] = fRec5[0];
			fRec4[2] = fRec4[1];
			fRec4[1] = fRec4[0];
			fRec3[2] = fRec3[1];
			fRec3[1] = fRec3[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec1[2] = fRec1[1];
			fRec1[1] = fRec1[0];
			fRec0[2] = fRec0[1];
			fRec0[1] = fRec0[0];
			fRec14[1] = fRec14[0];
			fRec27[1] = fRec27[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fVec3[1] = fVec3[0];
			fRec24[1] = fRec24[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec17[2] = fRec17[1];
			fRec17[1] = fRec17[0];
			fRec16[2] = fRec16[1];
			fRec16[1] = fRec16[0];
			fRec15[2] = fRec15[1];
			fRec15[1] = fRec15[0];
			fRec28[1] = fRec28[0];
			fRec40[1] = fRec40[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec38[2] = fRec38[1];
			fRec38[1] = fRec38[0];
			fVec4[1] = fVec4[0];
			fRec37[1] = fRec37[0];
			fRec36[2] = fRec36[1];
			fRec36[1] = fRec36[0];
			fRec35[2] = fRec35[1];
			fRec35[1] = fRec35[0];
			fRec34[2] = fRec34[1];
			fRec34[1] = fRec34[0];
			fRec33[2] = fRec33[1];
			fRec33[1] = fRec33[0];
			fRec32[2] = fRec32[1];
			fRec32[1] = fRec32[0];
			fRec31[2] = fRec31[1];
			fRec31[1] = fRec31[0];
			fRec30[2] = fRec30[1];
			fRec30[1] = fRec30[0];
			fRec29[2] = fRec29[1];
			fRec29[1] = fRec29[0];
			fRec41[1] = fRec41[0];
			fRec52[1] = fRec52[0];
			fRec51[2] = fRec51[1];
			fRec51[1] = fRec51[0];
			fRec50[2] = fRec50[1];
			fRec50[1] = fRec50[0];
			fVec5[1] = fVec5[0];
			fRec49[1] = fRec49[0];
			fRec48[2] = fRec48[1];
			fRec48[1] = fRec48[0];
			fRec47[2] = fRec47[1];
			fRec47[1] = fRec47[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec53[1] = fRec53[0];
			fRec63[1] = fRec63[0];
			fRec62[2] = fRec62[1];
			fRec62[1] = fRec62[0];
			fRec61[2] = fRec61[1];
			fRec61[1] = fRec61[0];
			fVec6[1] = fVec6[0];
			fRec60[1] = fRec60[0];
			fRec59[2] = fRec59[1];
			fRec59[1] = fRec59[0];
			fRec58[2] = fRec58[1];
			fRec58[1] = fRec58[0];
			fRec57[2] = fRec57[1];
			fRec57[1] = fRec57[0];
			fRec56[2] = fRec56[1];
			fRec56[1] = fRec56[0];
			fRec55[2] = fRec55[1];
			fRec55[1] = fRec55[0];
			fRec54[2] = fRec54[1];
			fRec54[1] = fRec54[0];
			fRec64[1] = fRec64[0];
			fRec73[1] = fRec73[0];
			fRec72[2] = fRec72[1];
			fRec72[1] = fRec72[0];
			fRec71[2] = fRec71[1];
			fRec71[1] = fRec71[0];
			fVec7[1] = fVec7[0];
			fRec70[1] = fRec70[0];
			fRec69[2] = fRec69[1];
			fRec69[1] = fRec69[0];
			fRec68[2] = fRec68[1];
			fRec68[1] = fRec68[0];
			fRec67[2] = fRec67[1];
			fRec67[1] = fRec67[0];
			fRec66[2] = fRec66[1];
			fRec66[1] = fRec66[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec74[1] = fRec74[0];
			fRec82[1] = fRec82[0];
			fRec81[2] = fRec81[1];
			fRec81[1] = fRec81[0];
			fRec80[2] = fRec80[1];
			fRec80[1] = fRec80[0];
			fVec8[1] = fVec8[0];
			fRec79[1] = fRec79[0];
			fRec78[2] = fRec78[1];
			fRec78[1] = fRec78[0];
			fRec77[2] = fRec77[1];
			fRec77[1] = fRec77[0];
			fRec76[2] = fRec76[1];
			fRec76[1] = fRec76[0];
			fRec75[2] = fRec75[1];
			fRec75[1] = fRec75[0];
			fRec83[1] = fRec83[0];
			fRec90[1] = fRec90[0];
			fRec89[2] = fRec89[1];
			fRec89[1] = fRec89[0];
			fRec88[2] = fRec88[1];
			fRec88[1] = fRec88[0];
			fVec9[1] = fVec9[0];
			fRec87[1] = fRec87[0];
			fRec86[2] = fRec86[1];
			fRec86[1] = fRec86[0];
			fRec85[2] = fRec85[1];
			fRec85[1] = fRec85[0];
			fRec84[2] = fRec84[1];
			fRec84[1] = fRec84[0];
			fRec91[1] = fRec91[0];
			fRec97[1] = fRec97[0];
			fRec96[2] = fRec96[1];
			fRec96[1] = fRec96[0];
			fRec95[2] = fRec95[1];
			fRec95[1] = fRec95[0];
			fVec10[1] = fVec10[0];
			fRec94[1] = fRec94[0];
			fRec93[2] = fRec93[1];
			fRec93[1] = fRec93[0];
			fRec92[2] = fRec92[1];
			fRec92[1] = fRec92[0];
			fRec98[1] = fRec98[0];
			fRec101[1] = fRec101[0];
			fRec100[2] = fRec100[1];
			fRec100[1] = fRec100[0];
			fRec99[2] = fRec99[1];
			fRec99[1] = fRec99[0];
			fRec102[1] = fRec102[0];
		}
	}

};

#include <stdio.h>
#include <string>
#include "m_pd.h"

#define sym(name) xsym(name)
#define xsym(name) #name
#define faust_setup(name) xfaust_setup(name)
#define xfaust_setup(name) name ## _tilde_setup(void)

// time for "active" toggle xfades in secs
#define XFADE_TIME 0.1f

static t_class *faust_class;

struct t_faust {
  t_object x_obj;
#ifdef __MINGW32__
  /* This seems to be necessary as some as yet undetermined Pd routine seems
     to write past the end of x_obj on Windows. */
  int fence; /* dummy field (not used) */
#endif
  mydsp *dsp;
  PdUI *ui;
  std::string *label;
  int active, xfade, n_xfade, rate, n_in, n_out;
  t_sample **inputs, **outputs, **buf;
  t_outlet *out;
  t_sample f;
};

static t_symbol *s_button, *s_checkbox, *s_vslider, *s_hslider, *s_nentry,
  *s_vbargraph, *s_hbargraph;

static inline void zero_samples(int k, int n, t_sample **out)
{
  for (int i = 0; i < k; i++)
#ifdef __STDC_IEC_559__
    /* IEC 559 a.k.a. IEEE 754 floats can be initialized faster like this */
    memset(out[i], 0, n*sizeof(t_sample));
#else
    for (int j = 0; j < n; j++)
      out[i][j] = 0.0f;
#endif
}

static inline void copy_samples(int k, int n, t_sample **out, t_sample **in)
{
  for (int i = 0; i < k; i++)
    memcpy(out[i], in[i], n*sizeof(t_sample));
}

static t_int *faust_perform(t_int *w)
{
  t_faust *x = (t_faust *)(w[1]);
  int n = (int)(w[2]);
  if (!x->dsp || !x->buf) return (w+3);
  AVOIDDENORMALS;
  if (x->xfade > 0) {
    float d = 1.0f/x->n_xfade, f = (x->xfade--)*d;
    d = d/n;
    x->dsp->compute(n, x->inputs, x->buf);
    if (x->active)
      if (x->n_in == x->n_out)
	/* xfade inputs -> buf */
	for (int j = 0; j < n; j++, f -= d)
	  for (int i = 0; i < x->n_out; i++)
	    x->outputs[i][j] = f*x->inputs[i][j]+(1.0f-f)*x->buf[i][j];
      else
	/* xfade 0 -> buf */
	for (int j = 0; j < n; j++, f -= d)
	  for (int i = 0; i < x->n_out; i++)
	    x->outputs[i][j] = (1.0f-f)*x->buf[i][j];
    else
      if (x->n_in == x->n_out)
	/* xfade buf -> inputs */
	for (int j = 0; j < n; j++, f -= d)
	  for (int i = 0; i < x->n_out; i++)
	    x->outputs[i][j] = f*x->buf[i][j]+(1.0f-f)*x->inputs[i][j];
      else
	/* xfade buf -> 0 */
	for (int j = 0; j < n; j++, f -= d)
	  for (int i = 0; i < x->n_out; i++)
	    x->outputs[i][j] = f*x->buf[i][j];
  } else if (x->active) {
    x->dsp->compute(n, x->inputs, x->buf);
    copy_samples(x->n_out, n, x->outputs, x->buf);
  } else if (x->n_in == x->n_out) {
    copy_samples(x->n_out, n, x->buf, x->inputs);
    copy_samples(x->n_out, n, x->outputs, x->buf);
  } else
    zero_samples(x->n_out, n, x->outputs);
  return (w+3);
}

static void faust_dsp(t_faust *x, t_signal **sp)
{
  int n = sp[0]->s_n, sr = (int)sp[0]->s_sr;
  if (x->rate <= 0) {
    /* default sample rate is whatever Pd tells us */
    PdUI *ui = x->ui;
    float *z = NULL;
    if (ui->nelems > 0 &&
	(z = (float*)malloc(ui->nelems*sizeof(float)))) {
      /* save the current control values */
      for (int i = 0; i < ui->nelems; i++)
	if (ui->elems[i].zone)
	  z[i] = *ui->elems[i].zone;
    }
    /* set the proper sample rate; this requires reinitializing the dsp */
    x->rate = sr;
    x->dsp->init(sr);
    if (z) {
      /* restore previous control values */
      for (int i = 0; i < ui->nelems; i++)
	if (ui->elems[i].zone)
	  *ui->elems[i].zone = z[i];
      free(z);
    }
  }
  if (n > 0)
    x->n_xfade = (int)(x->rate*XFADE_TIME/n);
  dsp_add(faust_perform, 2, x, n);
  for (int i = 0; i < x->n_in; i++)
    x->inputs[i] = sp[i+1]->s_vec;
  for (int i = 0; i < x->n_out; i++)
    x->outputs[i] = sp[x->n_in+i+1]->s_vec;
  if (x->buf != NULL)
    for (int i = 0; i < x->n_out; i++) {
      x->buf[i] = (t_sample*)malloc(n*sizeof(t_sample));
      if (x->buf[i] == NULL) {
	for (int j = 0; j < i; j++)
	  free(x->buf[j]);
	free(x->buf);
	x->buf = NULL;
	break;
      }
    }
}

static int pathcmp(const char *s, const char *t)
{
  int n = strlen(s), m = strlen(t);
  if (n == 0 || m == 0)
    return 0;
  else if (t[0] == '/')
    return strcmp(s, t);
  else if (n <= m || s[n-m-1] != '/')
    return strcmp(s+1, t);
  else
    return strcmp(s+n-m, t);
}

static void faust_any(t_faust *x, t_symbol *s, int argc, t_atom *argv)
{
  if (!x->dsp) return;
  PdUI *ui = x->ui;
  if (s == &s_bang) {
    for (int i = 0; i < ui->nelems; i++)
      if (ui->elems[i].label && ui->elems[i].zone) {
	t_atom args[6];
	t_symbol *_s;
	switch (ui->elems[i].type) {
	case UI_BUTTON:
	  _s = s_button;
	  break;
	case UI_CHECK_BUTTON:
	  _s = s_checkbox;
	  break;
	case UI_V_SLIDER:
	  _s = s_vslider;
	  break;
	case UI_H_SLIDER:
	  _s = s_hslider;
	  break;
	case UI_NUM_ENTRY:
	  _s = s_nentry;
	  break;
	case UI_V_BARGRAPH:
	  _s = s_vbargraph;
	  break;
	case UI_H_BARGRAPH:
	  _s = s_hbargraph;
	  break;
	default:
	  continue;
	}
	SETSYMBOL(&args[0], gensym(ui->elems[i].label));
	SETFLOAT(&args[1], *ui->elems[i].zone);
	SETFLOAT(&args[2], ui->elems[i].init);
	SETFLOAT(&args[3], ui->elems[i].min);
	SETFLOAT(&args[4], ui->elems[i].max);
	SETFLOAT(&args[5], ui->elems[i].step);
	outlet_anything(x->out, _s, 6, args);
      }
  } else {
    const char *label = s->s_name;
    int count = 0;
    for (int i = 0; i < ui->nelems; i++)
      if (ui->elems[i].label &&
	  pathcmp(ui->elems[i].label, label) == 0) {
	if (argc == 0) {
	  if (ui->elems[i].zone) {
	    t_atom arg;
	    SETFLOAT(&arg, *ui->elems[i].zone);
	    outlet_anything(x->out, gensym(ui->elems[i].label), 1, &arg);
	  }
	  ++count;
	} else if (argc == 1 &&
		   (argv[0].a_type == A_FLOAT ||
		    argv[0].a_type == A_DEFFLOAT) &&
		   ui->elems[i].zone) {
	  float f = atom_getfloat(argv);
	  *ui->elems[i].zone = f;
	  ++count;
	} else
	  pd_error(x, "[faust] %s: bad control argument: %s",
		   x->label->c_str(), label);
      }
    if (count == 0 && strcmp(label, "active") == 0) {
      if (argc == 0) {
	t_atom arg;
	SETFLOAT(&arg, (float)x->active);
	outlet_anything(x->out, gensym((char*)"active"), 1, &arg);
      } else if (argc == 1 &&
		 (argv[0].a_type == A_FLOAT ||
		  argv[0].a_type == A_DEFFLOAT)) {
	float f = atom_getfloat(argv);
	x->active = (int)f;
	x->xfade = x->n_xfade;
      }
    }
  }
}

static void faust_free(t_faust *x)
{
  if (x->label) delete x->label;
  if (x->dsp) delete x->dsp;
  if (x->ui) delete x->ui;
  if (x->inputs) free(x->inputs);
  if (x->outputs) free(x->outputs);
  if (x->buf) {
    for (int i = 0; i < x->n_out; i++)
      if (x->buf[i]) free(x->buf[i]);
    free(x->buf);
  }
}

static void *faust_new(t_symbol *s, int argc, t_atom *argv)
{
  t_faust *x = (t_faust*)pd_new(faust_class);
  int sr = -1;
  t_symbol *id = NULL;
  x->active = 1;
  for (int i = 0; i < argc; i++)
    if (argv[i].a_type == A_FLOAT || argv[i].a_type == A_DEFFLOAT)
      sr = (int)argv[i].a_w.w_float;
    else if (argv[i].a_type == A_SYMBOL || argv[i].a_type == A_DEFSYMBOL)
      id = argv[i].a_w.w_symbol;
  x->rate = sr;
  if (sr <= 0) sr = 44100;
  x->xfade = 0; x->n_xfade = (int)(sr*XFADE_TIME/64);
  x->inputs = x->outputs = x->buf = NULL;
    x->label = new std::string(sym(mydsp) "~");
  x->dsp = new mydsp();
  x->ui = new PdUI(sym(mydsp), id?id->s_name:NULL);
  if (!x->dsp || !x->ui || !x->label) goto error;
  if (id) {
    *x->label += " ";
    *x->label += id->s_name;
  }
  x->n_in = x->dsp->getNumInputs();
  x->n_out = x->dsp->getNumOutputs();
  if (x->n_in > 0)
    x->inputs = (t_sample**)malloc(x->n_in*sizeof(t_sample*));
  if (x->n_out > 0) {
    x->outputs = (t_sample**)malloc(x->n_out*sizeof(t_sample*));
    x->buf = (t_sample**)malloc(x->n_out*sizeof(t_sample*));
  }
  if ((x->n_in > 0 && x->inputs == NULL) ||
      (x->n_out > 0 && (x->outputs == NULL || x->buf == NULL)))
    goto error;
  for (int i = 0; i < x->n_out; i++)
    x->buf[i] = NULL;
  x->dsp->init(sr);
  x->dsp->buildUserInterface(x->ui);
  for (int i = 0; i < x->n_in; i++)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  x->out = outlet_new(&x->x_obj, 0);
  for (int i = 0; i < x->n_out; i++)
    outlet_new(&x->x_obj, &s_signal);
  return (void *)x;
 error:
  faust_free(x);
  x->dsp = NULL; x->ui = NULL;
  x->inputs = x->outputs = x->buf = NULL;
  return (void *)x;
}

extern "C" void faust_setup(mydsp)
{
  t_symbol *s = gensym(sym(mydsp) "~");
  faust_class =
    class_new(s, (t_newmethod)faust_new, (t_method)faust_free,
	      sizeof(t_faust), CLASS_DEFAULT,
	      A_GIMME, A_NULL);
  class_addmethod(faust_class, (t_method)faust_dsp, gensym((char*)"dsp"), A_NULL);
  class_addanything(faust_class, faust_any);
  class_addmethod(faust_class, nullfn, &s_signal, A_NULL);
  s_button = gensym((char*)"button");
  s_checkbox = gensym((char*)"checkbox");
  s_vslider = gensym((char*)"vslider");
  s_hslider = gensym((char*)"hslider");
  s_nentry = gensym((char*)"nentry");
  s_vbargraph = gensym((char*)"vbargraph");
  s_hbargraph = gensym((char*)"hbargraph");
  /* give some indication that we're loaded and ready to go */
  mydsp dsp = mydsp();
  post("[faust] %s: %d inputs, %d outputs", sym(mydsp) "~",
       dsp.getNumInputs(), dsp.getNumOutputs());
}

#endif
