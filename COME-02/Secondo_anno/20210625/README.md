# Appunti dell'incontro con il chitarrista del 25 giugno 2021

Brano ->

## Cose di cui abbiamo parlato

- https://it.scribd.com/document/250670914/Goffredo-Petrassi-Nunc-Ed-Suvini-Zerboni

- https://it.scribd.com/document/403111968/Mark-Applebaum-Dna

- https://www.google.com/search?q=simon+steen+andersen+gutar&ei=v5fVYOasHpCxkwWM2LuABw&oq=simon+steen+andersen+gutar&gs_lcp=Cgdnd3Mtd2l6EAMyBwghEAoQoAEyBwghEAoQoAEyBwghEAoQoAE6CAguELADEJMCOgkIABCwAxAHEB46BwgAELADEB46BAgAEBM6CAgAEBYQHhATOgUIIRCgAToECCEQFUoECEEYAVC6LliVN2DIOGgBcAB4AIABpwOIAcgJkgEHMC42LjQtMZgBAKABAaoBB2d3cy13aXrIAQrAAQE&sclient=gws-wiz&ved=0ahUKEwimiPfVsrLxAhWQ2KQKHQzsDnAQ4dUDCA4&uact=5

- https://www.youtube.com/watch?v=LSkKsRVTT6U&ab_channel=R%C3%A9myReber

- https://www.youtube.com/watch?v=3aH7edsRW8s&ab_channel=incipitsify

- https://www.google.com/search?q=guero+guitar&ei=I5_VYLWTGsOH9u8Pr-2AgAk

- https://www.youtube.com/watch?v=sVHl-pqaIYM&ab_channel=DennisSobolev

- https://www.google.com/search?q=festival+delle+arti+aquila+leonardo+meianti&ei=CqPVYMLEEpH3sAerzZ_ICw&oq=festival+delle+arti+aquila+leonardo+meianti&gs_lcp=Cgdnd3Mtd2l6EAM6CQgAELADEAgQHjoGCAAQFhAeOgUIIRCgAToECCEQFToHCCEQChCgAUoECEEYAVCcGljXPmCJQGgCcAB4AIAB6AGIAesWkgEGMC4xNi4zmAEAoAEBqgEHZ3dzLXdpesgBAcABAQ&sclient=gws-wiz&ved=0ahUKEwjC8a-4vbLxAhWRO-wKHavmB7kQ4dUDCA4&uact=5

- https://www.festival-automne.com/en/program

- https://it.wikipedia.org/wiki/Terontola

- https://www.google.com/search?q=tascam+dr05&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjqzvehqLLxAhUSHewKHRb4DhcQ_AUoAnoECAEQBA&biw=1280&bih=694

- https://www.google.com/search?q=especial+armonico+carbon&bih=798&biw=720&hl=it&ei=wovVYMSMH4zTsAf-tKPIAw&oq=especial+armonico+carbon&gs_lcp=Cgdnd3Mtd2l6EAMyBQghEKABOgcIABBHELADSgQIQRgAUNkTWIYVYKYZaAFwAngAgAG5AYgBsgOSAQMwLjOYAQCgAQGqAQdnd3Mtd2l6yAEIwAEB&sclient=gws-wiz&ved=0ahUKEwjE9cKep7LxAhWMKewKHX7aCDkQ4dUDCA4&uact=5

- https://www.tfront.com/p-457118-samblana-per-chitarra-e-sax-tenore-da-per-un-teatro-dellalba-1997.aspx
