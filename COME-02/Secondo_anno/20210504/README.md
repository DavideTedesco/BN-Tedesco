# Appunti della lezione di martedì 4 Maggio 2021

Con Bernardini in privato affrontati:
- realizzazione di una prima libreria di funzioni utili per le permutazioni ![tedesco-20210428_01.png](tedesco-20210428_01.png)
- visione ed utilizzo di [ruby-mext](https://git.smerm.org/DavideTedesco/ruby-mext)
- visione della [documentazione su ruby-mext](https://rubydoc.info/gems/ruby-mext)
- visione delle librerie che implementano octave per ruby: [octave-ruby](https://github.com/daikini/octave-ruby)
***
## Appunti

1. velocità del graffiato nella Successione utilizzando simboli diversi e non quelli del trombone
**specificare l'interazione e la relazione fra i rumori prodotti ed il resto**

2. Vi sono dei fatti incidentali come la fine del graffio sulla corda -> in cui si ha a che fare con un corpo vivo -> vi è quindi una contrapposizione forte fra gesto e risultante:
  - l'aspetto di mutare le corde è un qualcosa di imperativo
  - diviene un problema formale

3. abbiamo un concetto che si reitera più volte, e ci si aspetta in qualche punto uno sviluppo -> dopo cosí poco tempo

4. il materiale è ricco, ma lo è apparentemente un paio di volte; il problema dei glissandi diviene quindi la parte residuale del gesto -> **il focus può divenire il disturbo creato da un gesto**

5. cosa succede nelle pause? vi deve essere un equilibrio tra suono e silenzio o no? possiamo infatti dire qualcosa senza dire niente, come un brano realizzato con tutti "buongiorno" uguali...

6. bisogna chiedersi cosa producano i gesti, e se stiamo dicendo qualcosa, cosa diciamo?

7. bisogna quindi stare attenti a cosa si dice

8. **Cosa farne delle cose ulteriori al gesto che sentiamo?**

9. il processo reiterato soffre di divenire monodirezionale

10. bisogna tenere presente l'intorno

11. vi sono 4 percorsi diversi:
  - inizio
  - fine
  - gliassandi
  - silenzio

12. avremmo sempre dei pezzi realizzati che non sono perfetti -> ma cosa dobbiamo fare?

13. è importante la relazione tra compositore ed esecutore, ci chiediamo cosa succeda in prova?

14. Messaggio -> interesse sugli elementi, ma vi sono degli elementi inattesti, cosa ne facciamo, li reprimiamo?

15. la domanda che ci si pone quando si scrive: sono stato abbastanza chiaro?

16. È importante essere chiari? in Aitsi di Scelsi, realizzato dalla registrazione distorta di un pianoforte, la distorsione ruba l'attacco del cluster

17. si sfrutta la risonanza dopo il Cluster in Aitsi del 1974 -> diverrà poi Aitsi un quinto quartetto di Scelsi -> in esso abbiamo la reiterazione delle cose che ha sempre un significato

18. un esempio è il Bolero di Ravel che si concentra sull'orchestrazione

19. probabilmente sappiamo che la musica non finisce nel gesto

20. "There's no mistake in my music"

21. Un dialetto dice qualcosa? potrebbe ad esempio dire ed avere una prosodia, come il gesto

22. ma cosa succede in musica nelle pause? cosa fa l'esecutore

23. le pause fanno quindi pensare ed uscire altri ragionamenti

24. dei gesti fortemente retorici, ci narranno completamente il loro destino, come:
  - gliassandi -> punto di arrivo e fine -> vi è una perdita di interesse
  - terzine

25. come il "buongiorno..." puó avere delle risposte con una fine, anche la musica puó averne:
  - "...anche a voi"
  - "un ca***"

26. il virtuosismo compositivo romantico -> è un'idea che fa parte di un momento storico che non è quello attuale

27. l'arpeggio è un reiterare come in Partiels di Grisey

28. Vi sono compositori come Verdi e Grisey che pongono sempre una sorta di base tranquillizante

29. il concetto di sfondo <-> figura è ancora molto presente

30. Berio si trovo nel pubblico di un seminario di Grisey

31. al seminario a Berio venne posta una domanda, ovvero gli venne chiesto se siamo sempre a trattare di sfondo <-> figura

32. in Monet nel quadro la donna con il parasole, vediamo un germe dell'astrattismo e ci chiediamo qui abbiamo una figura, ma lo sfondo in che relazione è con essa?

![824px-Claude_Monet_-_Woman_with_a_Parasol_-_Madame_Monet_and_Her_Son_-_Google_Art_Project.jpeg](824px-Claude_Monet_-_Woman_with_a_Parasol_-_Madame_Monet_and_Her_Son_-_Google_Art_Project.jpeg)

33. abbiamo germi dell'astrattismo che saltano fuori, analogamente il discorso musicale di Berio ha dei germi di astrattismo compositivo che non seguono sempre il discorso sfondo<->figura

34. vi può essere tra l'altro un'altra relazione ovvero: virtuosismo compositivo<->sfondo-figura

35. la pioggia di sabbia del deserto diviene per i lavavetri occasione di lavoro aggiuntivo(lavavetri come musicisti elettronici...)

36. ogni lavavetro ha la sua strategia, ovvero una sua concezione formale

37. una concezione formale ce l'hanno anche i suonatori ambulanti in metro

38. in ogni cosa vi è quindi una logica formale di costruzione

39. in una situazione in cui non si ascolta piú -> far ascoltare qualcosa a qualcuno diviene una cosa strana e rara

40. abbiamo quindi un atteggiamento diverso:
  - gesto solo -> oggetto sonoro
  - coniugato di gesti -> suono-forma

41. abbiamo un'impalcatura complessa per creare, da sviluppare e rimpolpare di cose

42. abbiamo ad esempio il pan di spagna, che è la base costituente di dolci diversi

43. dobbiamo decidere quale comportamento avere

44. dobbiamo rivedere il percorso come una scoperta

45. minimalismo e decostruzione

46. la domanda da porsi potrebbe quindi essere: il testo rispecchia le volontà dell'autore?

47. dalla "lezione" di Roland Barthes, egli critica la lezione durante la lezione stessa dunque ad esempio riportandosi al concetto di parole: "la parola è fascista, per quello che ti permette di dire" -> essa rilega l'attenzione al senso di ciò che si sta dicendo...

48. ci chiediamo quindi nella società contemporanea: dobbiamo combattere il rumore? -> che viene generato o che è imposto?

49. ad esempio mettendo uno zero digitale laddove ci doveva essere una pausa in un brano musicale, potrebbe essere utile?

50. dobbiamo sempre considerare che le pause in musica, non sono degli zeri digitali

51. ascoltare ad esempio Ophanime di Berio e le pause come interpretate da Abbado

52. Ascoltare il silenzio nei brani di Luc Ferrari

***

53. il problema della notazione è che con essa si sta segnando in maniera imperativa la fine di un gesto, che magari musicalmente non è finito

54. la prova degli zeri digitali, permette di realizzare un artefatto di ciò che idealmente dovrebbe essere, ma che non puó essere nel mondo reale

***

55. tutto è quindi basato sulla familiarità

56. la musica pop e l'analisi di quello che vi è dietro serve proprio a questo, a portarsi nel piú familiare possibile

57. il metro del successo, nella società musicale d'oggi è un metro mercantile...

58. il metro del successo si riconosce in un gruppo sociale

59. anche il gruppo sociale della musica contemporanea, parala di tutto ciò che è contemporaneo

60. la musica contemporanea si basa su 5 colonne compositive fondamentali per il '900
  - Stockhausen
  - Boulez
  - Berio
  - Maderna
  - Nono

61. Il rapporto fra Berio e Rognoni, nacque per mezzo di Dallapiccola. Berio ando a Tanglewood da Dallapiccola a un seminario/masterclass(in cui vi erano pure Leuning e Ussachewsky) e Dallapiccola lo fece conoscere con Rognoni che lo fece divenire un programmista alla Rai, con a disposizione un orchestra tutti i giorni

62. Berio di fatto odiava il feticismo di una nota sulla carta -> scriveva e poi modificava, non gli interessava di mantenere sempre le idee

63. da vedere è la clip di Maderna che dirige e chiede a Stockhausen i 72,9 BPM di Gruppen cosa significassero, non aveva molto senso mettere un tempo cosī preciso su grandi gruppi di strumentisti

***

64. nel contemporaneo dobbiamo capire che il mercato del lavoro sta avendo una disgregazione e i meccanismi si stanno disfacendo -> ognuno di noi dovrà lavorare per dare senso alla propria vita -> "capire che cosa ha senso fare" -> non per forza lavorare per mangiare

65. sapendo il senso che si vuole dare a qualcosa -> cerchiamo il senso che emerge dal testo

***
## Cose di cui abbiamo parlato:
- Harmonizer
- [benchmark](https://www.userbenchmark.com/)
- le 5 colonne della musica contemporanea
- come Berio entrò alla Rai
- figura<->sfondo, Grisey e Berio
***
