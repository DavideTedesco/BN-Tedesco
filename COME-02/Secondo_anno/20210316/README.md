# Appunti della lezione di martedì 16 marzo 2021

[chitarra usata per i campioni](http://legacy.gibson.com/Products/Electric-Guitars/SG/Gibson-USA/SG-61-Reissue-Satin/Specs.aspx)

Jacques Tatie -> crema che serve per qualsiasi cosa

Cognome Berio a Porto Maurizio e Oneglia è diffusissimo...

_________
## Attività svolte
- Visione dello schema della Successione 1
- Ascolto degli esempi
- Visione della successione 1


Bernardini: Per far apprezzare la differenza fra corpo e manico la si deve molto sottolineare -> passaggio con corpo e manico che fa creare lo scalino

>Percussione sul corpo e manico -> bisogna sottolinearle!!

Stesso discorso su ponticello e tasto -> seguendo il discorso timbrico

**Fatto estetico molto sottile e delicato cambio tra pickup**

>Il punto di vista estetico, non può scadere sul personale

### Gli elementi traditori

Essi cercano di imbrogliare, ed aggiungere materiale al percorso sono in qualche modo _con-traditori_.

### Le sotto-trame

Esse sono delle virgolette narrative, che fanno il discorso, servono a realizzare corpo e varietà iniziale.

Le sottotrame introducono alcuni elementi e poi vanno a svanire, ma realizzano comunque parte del testo, come le sottotrame di un film...

Il percorso principale è direttamente correlato a queste sottotrame.

**Tenere da conto che le sottotrame possono anche non andare a zero, e mantenere anche se in piccole parti alcuni elementi fino alla fine del brano.**

### Scordatura

La scordatura puó essere vista come un passaggio cadenzale?

### Assegnazione di probabilità

Porre una probabilità che accada un evento a determinati istanti di tempo, ad esempio se voglio che un determinato evento accada sicuramente, esso avrà probabilità di accadimento pari al 100%.

Facendo ciò e incastrando le varie probabilità riuscirò a costruire un sistema polifonico, in cui si alternano e sovrappongono elementi.

### La scelta di varie possibilità

Essa puó essere svolta, scegliendo quella che piú piace facendosi assistere da un algoritmo pian piano disegnato.

### `permutation` in Ruby
Metodo `permutation` degli array di Ruby

```
words = %w[first second third fourth fifth sixth]
words.permutation(6).to_a
```

Mi restituisce tutte le permutazioni degli elementi nell'array usando tutti e 6 gli elementi!
