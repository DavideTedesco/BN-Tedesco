# Appunti della lezione di martedì 13 aprile 2021

## Sulle Successioni

- finita trascrizione Successione 1
- iniziata Successione 2?
- combinazione in Java
  - da documentazione ruby
- registrazioni a breve


### Scrittura

#### Le graffe fuori luogo
![per](per.png)

Essendo le graffe simbolo che contiene tutto ciò che vi è dentro, esse divengono fuori luogo in questa scrittura.

Realizzare un qualcosa che tenga il tempo senza fare rumore come le graffe esempio:
![dur](dur.png)

Utilizzare una corona misurata è piú opportuno.

#### Problema dei tempi metronomici
>Non scrivere qualcosa con figurazioni strane per far pensare all'esecutore che vi siano due tempi metronomici diversi.

Non si devono usare due sistemi con 2 connotazioni semantiche diverse, che non prendono il gesto

La notazione mensurale porta con se delle connotazioni dell'oggetto; è lo stesso discorso che si fa con traduzione diretta e traduzione ragionata.

#### La notazione proporzionale

Cercare di realizzare il brano con la notazione proporzionale, e non usando dei sistemi misti, la terzina è un qualcosa di importante, ma che va usato a dovere.

Come partiture d'esempio per la ntazione proporzinale vi sono:
- sequenza I di Berio
- sequenza V di Berio

In esse abbiamo una notazione proporzionale.

>Perseguire una notazione proporzionale realizzata con il minimo possibile.

Manrenere la partitura funzionale a quello che dovrebbe essere.

Dove vi è necessità di dettaglio nel tempo aumentare i secondi.

##### Sequenza V e la notazione proporzionale
Ad ogni cambio di rigo ad esempio nella Sequenza V vengono cambiate le considerazioni da fare sul trombone.

Vi è la preparazione del Why in sequenza V, preparazione simile ad una scrittura classica

>Bisogna quindi imparare da partitrue misurate, i gesti devono essere plateali, solo quando necessario.

#### Dubbi sulle terzine
Il ritmo indicato dalle terzine è un qualcosa di molto chiaro, che diviene una costrizione inutile per l'esecutore.

Essa è una notazione che ad esempio puó andar bene sul tema degli ottoni ad un punto di culmine.

#### Cambio del simbolo

Il simbolo in diagonale ha la connotazione di acciacatura, la decisione di cambiare il simbolo è utile a differenziare questo simbolo da una acciacatura.

Cambiamento di simbolo proposto a lezione:

![camb](camb.png)

![cambio](cambio.png)

### Schillinger, le armoniche ritmiche

Il problema delle armoniche ritmiche è che non si evince una relazione diversa fra multipli e primi dal punto di vista percettivo, quando invece nella realtà essa è molto presente.

>Lo schema di Schillinger non da di fatto il senso del ritmo...

### Meccansimo di ranking
Si puó realizzare con un discorso condizionale in cui le uniche note fisse sono la prima e l'ultima.
Ogni nota in una combinazione ha:
- punteggio
- misura
- condizione

Vi sono dei frammenti per realizzare pre-condizioni, la condizione per fare il ranking è ciò che fa la differenza.


### Berio e il meccanismo di decisione
Il meccanismo di decisione di berio, rubato da Maderna, permette di realizzare decisioni algoritmiche, salvo una particolare decisione, che è un eccezione. Essa è in relazione con il resto del materiale, ed ha una particolare elaborazion con un suo algoritmo e una sua logica.

(Citato lo Studie II con la perdità di controllo del suono per acquisire ricchezza(timbrica??)).

### Il ritmo nei salsieri portoricani

Essi pensano alla pulsazione come una montagna larga su cui si puó arrivare in ritardo, in anticipo o in tempo.

**Proposta di lezione/seminario sul ritmo.**
Con ospite il maestro Lupone e temi da trattare tra cui:
- affastellamenti ritmici di Ives
- Nancarrow e le elaborazioni ritmiche
- agli antipodi del ritmo: spaghetti music e pornografia musicale.
- Babbitt e gli accenti melodici in contrasto con il ritmo

>In generale, è l'articolazione che conta insieme al ritmo.

### Emanuale Casale e la natura silente

[Emanuele Casale](https://www.emanuelecasale.com)

- Acustica
- Acusmatica
- Dead Electronics -> fixed media -> natura silente

Elaborazione ritmica non abbastanza elaborata...

Mentre Francesco Galante ci prova...

_Ricordato da Giovanni un intervento sul ritmo realizzato da Bernardini da Uncini alla Sapienza_
***
### La cultura non è ecologica


***
### Cos'è il riposo ?
Chiedersi cosa sia il riposo, che deve essere obbligatorio fra le varie attività.

La cosa piú faticosa fra le varie attività è forse il task switching.
***
>Libro che sottrae alla cultura.

Citazione di Bernardini da...

***
### Studiare il Mozart meno conosciuto

Studiare(trascrivendo) le composizioni meno famose di Mozart:
- i primi 4 concerti

Il cambio dell'accesso alla memoria:
- fruizione della memoria per accesso continuo
- accesso alle informazioni della memoria volatile
_______

##Cose di cui abbiamo parlato:

Riscoperta della musica elettronica russa al Conservatiorio Rimskji Korsakov di San Pietroburgo.

[normalize-audio](http://manpages.ubuntu.com/manpages/trusty/man1/normalize-audio.1.html)

[normalize](http://normalize.nongnu.org/)

[Combinazioni array in Ruby](https://ruby-doc.org/core-2.4.1/Array.html#method-i-combination)

[Threshold~ in Pure Data](https://github.com/gusano/pure-data/blob/master/doc/5.reference/threshold~-help.pd)

[Feauture extraction](https://www.ismir.net/resources/software-tools/)

Libro consigliato: Robin Becker - Le guerre sessuali
