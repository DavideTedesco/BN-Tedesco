# Appunti della lezione di Martedì 2 Marzo 2021

## Argomenti da discutere a lezione
- visione partitura alfa v2
- tempi?
- esclusione del burst voce con la chitarra per poterlo approfondire piú avanti
- realizzazione di una miniatura? quale deve essere il dettaglio?
- elementi del tape numerati e riportati in legenda

[sito utile per calcolare lunghezza delle varie note](https://rechneronline.de/musik/note-length.php?)
[feedback chitarra elettrica](https://mixdownmag.com.au/features/columns/sound-advice-how-to-get-controlled-feedback/)
________

## Appunti

La travature(flags delle note)

### Manuali

Karkoscha, manuali per la scrittura musicale

(cosa migliore è ricopiare delle cose)

### Far realizzare il tape allo strumentista o no?

Questione del registrare o far registrare complica il brano, se non è strettamente necessario per le logiche musicali, il brano è piú facile da eseguire.

>Se si vuole un'identità tra chitarra e tape -> registrare tutto.

Registrare i glissandi in caso e poi fornirli sempre allo strumentista!

### Separare per chiarire...

Wave shaping e intervento dal vivo:
- waveshaping quanto? **linea operativa sul live electronics**
- linea tape

### Scrittura proporzionale

Realizzare un minimo di scrittura proporzionale!

La scrittura musicale è semi-proporzionale chiarezza locale...

(All'interno di una battuta vi sono delle imaginary barlines -> nota che attraversa un imaginary barline, conviene legare questa nota)

### Logica ritmica

A guardarlo sembra ci sia una logica ritmica che non si intende bene.

Problema: siamo filtrati dalle figure musicali che conosciamo -> procedimento di disgregazione e figurazione musicale -> forse la composizione assistita potrebbe aiutare molto -> elementi piú asettici con la composizione musicale assistita -> meno basate su intuizioni.

>Il ritmo  ha il problema grosso, che si ha un andamento metrico del ritmo della musica del passato, ed anche quello della musica di consumo, ma si potrebbero usare delle altre figure.

Accellerando misurato(contemporaneo):

![acc](acc.png)

**Composizione musicale assistita** -> ognuno si fa ciò che vuole da solo -> si prende un linguaggio di programmazione generale e si fa ciò che si vuole.

#### Come fare accumulazione e stasi?

- accell semplici
- accell misurati
- accell mescolati

La composizione assistita si puó vedere in due modi:
1. algoritmo
2. risultati statistici -> vedere i risultati e aggiungere delle regole per fare il ranking

##### Esempio con note distribuite randomicamente nel tempo

gruppo di note random-> ranking -> possibile

**Inizialmente fare con qualcosa di semplice** -> complicare la faccenda solo quando si vuole qualcosa di musicalmente.

Catene di Markov -> utile per non avere condizioni totalmente randomiche -> capendo un po' l'andamento del materiale in ingresso.

(brano per pianoforte di Bernardini -> brani adattati in range ed ad una serie, della quale venivano prese le altezze, facendo dei fitting tra le serie)

**Il computer è assolutamente asettico** esso non ha intuito e dobbiamo lavorare su questo fatto.

Algoritmo che cambia in base a come lo cambi.

________
Composizione musicale assistita usare una macchina per riflettere e scoprire altre inclinazioni.

**Iniziare con cose semplici.** -> iniziare a generare roba a caso.
________
_Rivedere le lagature per chitarra elettrica._
________
Attendo manuali
