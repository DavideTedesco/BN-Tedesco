# Appunti della lezione di martedì 25 maggio 2021

## Ascolti della settimana
- Mantra di Stockhausen
  - origini -> op 27 e Bartok
  - la sovrapposizione in due piani di pianoforte e altoparlanti
  - le percussioni
  - gli addensamenti
  - le onde corte -> il tape
  - 13
  ![img1](img1.png)
  ![img2](img2.png)
  ![img3](img3.png)
- Béla Bartók - Music for Strings, Percussion and Celesta
- Julia Wolfe - Lick
- Elliot Carter - Shard
- Alexander Schubert - Your Fox's, A Dirty Gold (Frauke Aulbert in Brussels)
- Dai FUJIKURA - Abandoned Time for electric guitar and ensemble
- Billone - OM ON

## Argomenti approfonditi
- [Ritardo segnale...](https://music.tutsplus.com/tutorials/how-to-calculate-a-delay-tower--audio-10471)
- [temperatura e ritardi](https://www.nde-ed.org/Physics/Sound/tempandspeed.xhtml)

## Questioni, domande e problemi

- come porsi per la ricerca dell'esecutore senza castrarsi compositivamente e senza avere commissioni?

>Do ut des tra esecutore e compositore, proiettare l'esecutore con una sorta di convenienza...

Le Sequenze di Berio per studiare gli strumenti sono una paraculata per la bravura del solista e il porsi dei solisti ad un certo livello.

**Riflessione su come funziona il mercato della musica: contemporanea e di consumo.**

***

## Perchè cercare un collegamento stretto con chitarrista se non vi è Live Electronics?

Elettronica morta puó essere in una relazione non causale...

>Bisogno di stare insieme all'elettronica e della chitarra...

## Scrittura ed espediente narrativo

Thomas Mann e Don Giovanni, la profondità del personaggio e il riscatto di se stesso...

Beckett e la scrittura musicale... (attrice con sola bocca)

Dopo la narrativa romantica...

## Conoscenza di un esecutore

Scrivere dei pezzi ad hoc per una persona dopo che la si è conosciuta...

## Distorsione e amplificatore

Maggior gestione della distorsione...

## Lilypond e postscript

Come parametrizzare dei simboli...

La maggior parte dei simboli si può fare con altre cose...

[http://lilypond.org/doc/v2.18/Documentation/notation/graphic](http://lilypond.org/doc/v2.18/Documentation/notation/graphic)

[https://lsr.di.unimi.it/LSR/Search?q=postscript](https://lsr.di.unimi.it/LSR/Search?q=postscript)

[https://www.slideshare.net/NicolaBernardini2/nicola-bernardini-intermezzo-i-for-percussion-set-and-bass-recorder-54643954](https://www.slideshare.net/NicolaBernardini2/nicola-bernardini-intermezzo-i-for-percussion-set-and-bass-recorder-54643954)

Brano di Bernardini -> trascrittura di Lilypond -> come trascriverlo?

Commissione per Berio di 1000 voci per l'anniversario della Rivoluzione Francese.

## Per le prove

Scaletta di cose da fare prima delle prove...


***

## Cose di cui abbiamo parlato:

- Utis di Berio
  - Utis opera insopportabile per una commissione della Scala, in mezzo c'è un pezzo di 4 minuti sublimi...
  - Utis un qualcosa che costò 3,7 miliardi di lire
- la disponibilità degli esecutori nella musica contemporanea(disponibilità maggiore delle esecutrici donne) -> investimenti su qualcuno
- concerto del Master -> link per i concerti -> possibilità di conoscere esecutori per parlarci e realizzare qualcosa
- concorsi di esecuzione per chitarra -> ce ne sono molti
- Compositori che scrivono solo su commissione -> tipica cosa americana, il mestiere del compositore
- mentre si studia incominciano a venire idee di cose che si possono fare, che altrimenti non esisteranno -> lo studio della storia è un qualcosa di profondo e lungo
- Sequenze di Berio, quasi mai erano commissionate
- chitarrista -> Stefano Cardi che suonò la Sequenza
- Casoli -> chitarrista elettrica
- Musica di Berio da studiare in base alle mogli(Berberian, Oyama, Peckett)
- senso di un qualcosa di musicale:
  - esplorazione
  - sensi musicali
  - ascolto di quello che c'è adesso:
    - capire cos'è che manca
    - capire cosa posso fare come compositore
- chitarra elettrica -> letteratura di riferimento da circo equestre
- Antichrist di Lars Von Trier(da non guardare) -> alterna due tecniche cinematografiche con linguaggio pubblicitario e pornografia
- esercizio da fare -> andare nelle case di Pasolini (Casarsa della delizia, Casa all'EUR)
- Leonardo(ragazzo chitarrista del master)
- dialettica con gli esecutori -> è bene cucire i pezzi addosso alle persone
- costruzione dell'identità e di un carattere che traspare del compositore e non dell'esecutore
- scordatura è importante farla sentire in un'unica successione o in tutte? -> soluzioni che vengono fuori levando cose
- Barthres -> il senso della parola viene esplicato levando le cose?
- farebbe comodo avere un testo di lavoro
- [http://www.cidim.it/cidim/content/314619?id=242472](http://www.cidim.it/cidim/content/314619?id=242472)
- ai matematici piacciono le funzioni continue e non le discontinue...
- uno strumento musicale per tenere una nota in vita, consuma energia, è molto più semplice non farla, mentre nello strumento elettronico fare qualcosa consuma, far smettere uno strumento è più complesso di ciò
- problema dell'elettronica è che essa è poco espressiva... perchè?
- non mostrare di essere ambiziosi in Danimarca(umile ma inutile)
- easy to hire, easy to fire -> mobilità del mercato del lavoro in Danimarca
- come mai in Italia vi è un'effervescenza culturale, e in Danimarca no(a parte Lars Von Trier e Ingels)?
- progetto con l'università di Berna con dipartimento di AI e Sound Studies... -> applicare tecniche di elaborazione numerica di segnali applicata al deep learning
- Leibniz -> "Ho cominciato molte cose e finito niente..."
