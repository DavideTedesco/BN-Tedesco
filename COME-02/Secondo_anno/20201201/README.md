# Appunti della lezione di Martedí 1mo Dicembre 2020

[Ascolto del terzo movimento della Sinfonia di Berio](https://www.youtube.com/watch?v=9YU-V2C4ryU&ab_channel=takatantan2910)

[Ascolto di Blue Monk di Thelonius Monk](https://www.youtube.com/watch?v=_40V2lcxM7k&ab_channel=inations)

[Sinfonia di Berio raccontata da Berio](http://www.lucianoberio.org/node/1493?585688881=1)

Effettuati:
- correzione del catalogo
- scelta frammenti trascrizioni brani
- inizio trascrizioni

_____
## Appunti

Macina pepe universale di brani.

Prima di fare molto lavoro sui dati converrebbe fare le fixture.

(Test driven development -> prima scrivere il test e poi il codice)

### Test driven development & composizione

Il test non funziona perchè non c'è il codice.

Progettare le cose e scrivere una partitura prima di sapere se è possibile suonarla oppure no. Questo approccio promuove una maggiore libertà di immaginazione.

Cosa ci condiziona il nostro immaginario.

(brano per acqua, metallo e Live Electronics)

Chitarra su un nastro, non è una chitarra dal vivo. "Come se ti vai a sposà però non hai la moglie."


(usare la registrazione di uno strumento significa usare una cartolina)

(Sinfonia di Berio ha una serie di riferimenti collaterali, tutta la presenza della voce nelle sinfonie ha una storia lunga con Beethoven, Berio si confronta con un ottetto e non un coro. "Cercare di fare quello che manca in un contesto tradizionale")

Berio a guardarlo con occhi di oggi è datata in quel periodo lí.

Poter comporre con connotazioni.

#### Testare la capacità di fare un ricombinatore algoritmico

In qualche modo, ricreare un'altra partitura partendo da dei frammenti.

1. prendere due brani
2. trascrizione in lilypond
  - alcuni dati separati
    - tempo
    - metro
3. ragionamento sul macina pepe -> riscrivere la partitura con un tempo imposto da me, con un altro yaml con caratteristiche dell'output -> avendo i frammenti si ridisegna l'aspetto ritmico.
  - trascrivere il frammento con suo tempo originale(frammenti di tipo cinematografico)
  - trascrivere il frammento con tempo imposto da me (per dare continuità bisogna dare un senso più veloce a alcuni frammenti)
    - rallentamento e velocizzazione, si può fare algoritmicamente(separare tutte le note e le pause in Ruby, a quel punto si è realizzato il parsing di una nota lilypond) -> problema è isolare il ritmo
4. tutto ciò si può testare separatamente(lettura di file)
  - gem che fa questa roba -> passo il file yaml e lui fa tutto
  - oggetto che è un interfaccia e fa il lettore di file yaml
  - si fa il testo -> il file yaml è una fixture
5. organizzazione di dati in sotto oggetti -> per ciascuna chiave si dovrebbe avere una partitura
6. altro file yaml per le caratteristiche del file di output -> con header, trader, tempo(o serie di tempi), etc...

__________

Leggi partitura -> passa la parte di yaml all'oggetto partitura, tra essi la sequenza di frammenti lilypond, la partitura nel crearsi

Oggetto nota decompone tutta la nota con:
- altezza
- tempo
- ritmo
- eventuali markup

Quando si legge il file si organizza tutto con un array di partiture.
Serve da fare:
- scrivere l'oggetto creatore (usare `spec` e non `test`) per fare un test driven development (documentazione fatta da testo puro che diviene pseudocodifica e lo usage che è nel readme vi è `cucumber` che serve per scrivere uno storyboard, e lui simula l'utente)

1. scrivere prima lo `spec` con tutte le asserzioni -> deve succedere questo -> debugger online (byebug)

- bundler permette di risolvere le dipendenze

- per ciascuna di queste cose serve scrivere un test -> partendo dall'oggetto piú piccolo, ovvero la nota, e all'interno del test si da un array di possibili note. Con tutti i modi di poter scrivere la nota in lilypond (risolvere il problema che le note fanno spesso riferimento a informazioni precedenti) (usare l'altezza assoluta o relativa?)

- prima creare l'oggetto frammento (che ha una collezione di note) ->fixture
 - collezione di dati senza le note e in questi dati si mette anche al relativa, il tempo, il metro e tutto ciò importante per il frammento.
 - il frammento ha un array di note -> questo oggetto frammento deve creare oggetti nota spacchettati -> e deve tenere tutti i dati pregressi (come l'ultimo ritmo fatto)
 - creata una nota -> spacchettare tutti i dati contenuti in una nota
 - n.b. salta e pause sono casi particolari ->
_____

Tutto separato cosí da poter adattare i frammenti al tempo di output
__________

Dopo si fa un oggetto generatore -> dopo avere tutti i dati si ha un oggetto generatore che è realmente la composizione, prende dei dati, uno YAML che configura un po' l'output e si mette li a fare robe.

Esempio:
Seleziona randomicamente dei frammenti tra 3 e 8 note, e si mette a pescare le note per 20 volte:
entrare in catalogo -> scegliere partitura -> scegliere frammento -> scegliere note -> aggiustare dentro l'output -> riscrivendolo con tutte le eccezioni del caso, per capire come inserirle

Sempre meglio fare dei test con casi complicati

_____

Se su usa bundler -> crea una gem con macinapepe, fa tutto lui!

Tutti gli oggetti li devo mettere sulla  directory lib.
