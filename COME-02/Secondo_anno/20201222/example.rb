require 'lyv'
require 'byebug'

byebug
parser = Lyv::LilyPondParser.new
doc = parser.parse_document(File.read('../Catologo_degli_affetti_chitarristici/mangore.ly'))

# top-level header fields
puts "#{doc.header['composer']}: #{doc.header['title']}"

# traverse scores and process their header fields and lyrics
doc.scores.each do |s|
  puts "#{s.header['opus']}: #{s.lyrics}"
end
