# Appunti della lezione di Martedí 15 Dicembre 2020

Carla Fioravanti contattò il Maestro per Demetrio Stratos, che si interessava alle tecniche vocali estese.

A Padova vi è un grande centro che si occupa della voce.

Documentario di Carla Fioravanti su Demetrio Stratos.

Doppiaggio per via dell'alfabetizzazione!

Gianni Sassi direttore dela CRAMS che produceva i dischi degli AREA e di Eugenio Finardi, Sassi era una persona fuori dall'ordinario ed era un milanese che credeva nella cultura e conobbe Cage facendogli fare i dischi a Cage. Sassi era sostanzialmente un grafico pubblicitario.

Documentario su Gianni Sassi, costruito da discussioni avute insieme.

Fender Rhodes -> martello che preme su un diapason
il ribatutto del Rhodes è impossibile capire

Il mercato spinge verso un'estetica di uniformità ed il mercato tende quindi all'omologazione. Ed uno dei problemi che ha la musica contemporanea, poichè è molto variegata.

Viviamo in una società in cui quello che si riesce a vendere è l'omologazione.

Sulla musica contemporanea su cui tutto è sempre diverso è...

Festival Traiettorie a Parma in cui ci vanno 10 persone, Parma ha molte possibilità.

Federico Avanzini al Politecnico di Milano, proviene dal CSC di Padova.

Esitono pochissimi riverberi che suonano come riverberi.

Siccome il riverbero è un qualcosa che ha un qualcosa legato all'ambiente.

Quando fai un riverbero con una coda lunga, fai un segnale lontano da te o che si sta allontanando, e questi suoni ti assicurano perchè si allontanano da te, ed un segnale vicino è piú preoccupante.

Lavoro sinfonico con microfoni relativamente vicini è diversa.
_______
Come mettere i materiali notali nel brano in un file.

Riduzione in simboli con il Deep Learning.

Per noi una nota è un oggetto sonoro.

Le tecniche di riconoscimento vocale sono aiutate dal fatto che vi è una mobilità spettrale molto accentuata. Mentre la mobilità spettrale nelle note musicali non è cosí accentuata.

Come funziona la dettatura nei telefoni?

Tutto sommato le macchine non riconoscono prima di noi.

Sistema di Deep Learning...

Studente di Padova Anna Barletta

_______
Trascrizione dei frammenti

Trascriverne uno

## TDD

Il test driven development si divide in varie parti

Test funzionale degli oggetti ovvero Unit testing, testare:
- se l'oggetto si crea

I test sono praticamente la documentazione.

È inutile testare se si crea oppure no

Il punto è che testi tutto quello che funziona.

**non testare se gli attribute reader tornano qualcosa**

### Test Driven Development

- testare se l'oggetto pensato è come pensi tu
- specialità di ruby è creare metodi dinamicamente
- fare un oggetto che crea un array e crea metodi che sono attr_reader per ciascuna chiave, è una cosa che si puó fare in automatico -> testare che l'oggetto creato `respond to` ogni metodo -> testare se quello che ritornano è del tipo corretto

### Functional Testing

Rails ha vari metodi che fanno da collante fra gli oggetti servono dei test trasversali fra gli oggetti e ceh fanno da collante sul resto.

(importante in model view controller, il controller viene testato nel funtional testing, perchè fa tante cose)

### Cosa non si puó fare con `spec`
Il behaviour driven testing...

Sono un utente arrivo sul sito, clicco su un link, cosa succede?

Cucumber in cui posso testare tutto il processo.

Celenium libreria che permette di simulare tutte le azioni...

_______

Cosa importante ora è lo unit testing non serve solo per vedere se l'oggetto esiste. Lo unit testing serve anche per vedere che non si sta sfasciando tutto.

### `rake`
Usare `rake` ovvero il `make` di Ruby.
Quando si hanno tutti i test, scrivi rake e fa tutti i test possibili e immaginabili.

`rake` tiene a mente tutti i processi che bisogna fare.

Con `spec` si può preparare tutto l'ambiente per preaprare tutto l'ambiente ogni volta.

Fare i test in ambiente controllato.

Mettere su una roba, la committo, fare il test e si scoprono un sacco di cosa.
______  

[François Pachet](https://www.francoispachet.fr/)

Conosciuto perchè fece il completatore, e lui fa il resto.

Il festival di Nuova Consonanza che va online.

Bocelli ha fatto 4 concerti in streaming in cui vendeva lo streaming a 25 euro allo streaming.

Soundscape delle partite di calcio.

(Renzo Arbore salvò l'istituto dei beni dell'audiovisivo.)
