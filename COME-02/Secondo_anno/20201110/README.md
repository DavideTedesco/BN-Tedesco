# Appunti della lezione di Martedí 10 Novembre 2020

## Chladni patterns
- video
- frequenze: 320, 218 Hz

## L'esame ed il materiale portato all'esame
- criticità
- il testo ?
- la musica (vero problema)
- mancanza di un brano concreto
- confronto con chitarrista chi?
- Lettura di: Contemporary Guitar di Schneider

## Cosa fare quest'anno?
- realizzazione di un primo brano per chitarra
- gli studi??
- la notazione per chitarra, PIC e LilyPond?

________
## Appunti

Brani di Adriano Guarnieri da sentire

Idea di conoscere lo strumento originale che lo somiglia, non si riesce a distaccarsi dallo strumento iniziale.

Autoreferenzialità mi ha creato dei problemi.

Concludiamo questo percorso con la chitarra!

- Proposizione di un altro punto di vista -> se rimaniamo sull'idea di fare degli studi, se questa cosa si blocca perchè si ha una storia magari esecutiva, forse un'idea è ripartire dagli studi stessi, abbracciandoli;
- prendere degli studi che fanno parte del bagaglio affettivo, provando a prenderne frammenti ed elementi non per intero, tipo spezzatino degli studi, inserendoli in un contesto che a loro è nuovo
  - passando per un'analisi, a cosa servono questi studi?
  - musicalmente che cosa fanno?
    1. per certi versi sviluppano capacità fisiche e fisiologiche
    2. altri sono piú degli studi compositivi, che servono a pensaro lo strumento nella maniera compositiva, non per acquisire tecniche piú particolari(come Debussy -> sensibilità diversa del pianoforte)

- vi è stato un blocco sulla creazione di un qualcosa dopo aver avuto un'esperienza sullo strumento
- analisi dello studio:
  1. sfigurare lo strumento
  2. queste capacità dovranno essere centrali di questi lavori
  3. per arrivare da una valenza musicale, bisogna fare un percorso al contrario
  4. provando a non preoccuparsi del materiale sonoro
  5. provando a inserirli in questo strumento

- passi:
  1. Analisi funzionale -> cosa fa questo studio?
  2. cosa ricercano questi studi?
  3. trascrizione per lo strumento -> cosa cambia? momenti di dilatazione, con battute mie e battute inserite da me, inventando un modo di trascrivere queste cose, cavandosi l'impiccio di dover scegliere le note(scrivere trascrizioni di trascrizioni, ovvero esperimenti)
  4. questo strumento nuovo non è piú una chitarra -> quindi? riscrivo lo studio.

(ritirata notturna di Boccherini, trascrizioni di Berio, sovrapposte con margini )

(Brano Rendering di Berio brano non finito di Schubert -> la sorella di Bernardini restaura i monumenti, e la tecnica moderna è che non si rifà ciò che non si ha; facendo questa discussione era interessato Berio e venne fuori che Rendering fu fatto con la stessa tecnica)

Le trascrizioni possono essere molto inventive, facendo emergere il nuovo strumento, una possibile strategia è quello di far emergere lo strumento.
- come se la chitarra esista ancora
- ma deve emergere il nuovo strumento

Quando si ha qualcosa che ci blocca, ci si stende sopra, vedendo che cosa succeda.

Ed il punto è che si approfitta del senso già dato da qualcun altro del senso musicale, assumendo ciò e mettendoci molto meno.

Nella composizione abbiamo sempre un senso delle cose, cosa significhi etc...

Negli studi, in parte questo problema è risolto...

___________
1. **ricognizione dei propri affetti** -> guardando i libri, e tirando giù tutto ciò che puó avvenire:
  - uno studio -> che fare?
  - più studi -> ipotesi(condensare più studi)

  Inventivo sia sull'analisi che sulla trascrizione effettiva.

2. che tabù si hanno dal punto di vista compositivo? -> (su questo si è raccolta tutta la letteratura pianistica e la si è introdotta)

(truccologia pianistica è un qualcosa di furbo che produce qualcosa di bello, e strepitosi, ma non affrontano il problema dei tabù)

Si parte ora da uno strumento trasfigurato, e si realizzano trascrizioni per uno strumento che lo assomiglia che studia le nuove proprietà per lo strumento, usando gli studi per chitarra come impulso iniziale.

**La notazione ora è tutta da inventare!**

Capace che per le partiture bisogna inventare una notazione.

(Si puó fare un pezzo virtuosistico per Live Electronics? Concerto per Vidolin con 3 movimenti e 3 movimenti diversissimi, stereotipo del concerto e rendere uno strumento gregario uno strumento virtuosistico. Sviluppo di una scrittura con un non determinismo e risultati sonori.)

Primo movimento del concerto di Ravel solista e orchestra come si devono comportare?

### Prossima volta

Quadro complessivo
- ricognizione degli affetti
- inizio delle trascrizioni
