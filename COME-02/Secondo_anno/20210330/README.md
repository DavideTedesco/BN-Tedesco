# Appunti della lezione di martedì 30 marzo 2021

[https://stackoverflow.com/questions/17120798/all-possible-permutations-with-a-condition](https://stackoverflow.com/questions/17120798/all-possible-permutations-with-a-condition)

```
array_note = []
array_durate = [" semibiscroma ", " biscroma ", " semicroma "," croma ", " semiminima ", " minima ", " semibreve"]
```

[https://it.wikipedia.org/wiki/Valore_(musica)](https://it.wikipedia.org/wiki/Valore_(musica))
_______

La scuola Romana è tutta separata, mentre la scuola milanese era tenuta insieme dall'editore.

Molte idee, molto sul pezzo molto interessanti...

Bellucci -> tesi sul suono-forma con una sorta di definizione -> musicalmente che senso ha? Cosa costruisce e che cosa decostruisce?
Da dove viene l'idea del suono-forma? E quali sono i problemi che crea?

Demusica -> serie di articoli che fanno uscire annualmente

Argomento monografico di una raccolta di testi!

Recupero di software che ha vent'anni realizzato dal CRM.

(Sistema dot-graphviz che fa schemi direttamente dal codice per documentare.)

__________

Permutazioni ritmiche -> non si può permutare il ritmo come se nulla fosse...

Stravinskji, Messiaen, Babbit ritmi e figure abbastanza standard.

Babbit ha un modo molto eccessivo di usare degli accenti, usando degli accenti secondo numeri primi.

Organizzare il ritmo macroscopicamente e altro microscopicamente -> elaborazione del ritmo molto destrutturante

Suono-forma è una delle risposte al problema con l'elaborazione ritmica che si è completamente destrutturato.

### Evitare la monodirezionalità

Le trame monodirezioniali tendono ad essere sfondate, come in un film di Hitchcock, tutte le trame monodirezioniali si fondono.

Anche la [terza sonata di Chopin](https://www.youtube.com/watch?v=xGJxmCOPFVM&ab_channel=AshishXiangyiKumar)...

### Rumore e segnali deboli

Brani come le Formazioni i Berio con una nota tenuta su tutto il brano, come una realizzazione artigianali.

Quattro pezzi su una nota sola di Scelsi, sono invece brani mediati da Tosatti, personaggio concettualmente duro.

### I glissandi

Essi sono gesti molto forti, quasi pornografici, va sviluppata un'attenzione particolare per questi gesti.

### Seconda Successione

- Tripartizione della seconda Successione

Quali sono gli elementi leganti?
- le altezze

Elementi che vengono sviluppati?
- problemi con elementi ritmici vengono a galla

Altro gesto presente?
- i glissandi, recupero delle altezze e degli aspetti ritmici

_______
## Suggerimenti per la scrittura
- corde stoppate, vicino al capo tasto durante il graffio delle stesse, per non far risuonare tutto ed avere un rumore indesiderato.
