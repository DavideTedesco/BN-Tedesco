<map version="1.1.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#990000" CREATED="1626189881707" ID="ID_1710440375" MODIFIED="1626190026164" TEXT="Feedback su chitarra classica">
<edge COLOR="#990000" WIDTH="1"/>
<node CREATED="1626190045403" ID="ID_1296289337" MODIFIED="1626190057299" POSITION="right" TEXT="percussione sul piano armonico"/>
<node CREATED="1626190058670" ID="ID_523525987" MODIFIED="1626190069240" POSITION="right" TEXT="gestione di punti di pressione"/>
<node CREATED="1626190086611" ID="ID_556175711" MODIFIED="1626190117835" POSITION="right" TEXT="feedback da tapping"/>
<node CREATED="1626190118445" ID="ID_1800113367" MODIFIED="1626190126773" POSITION="right" TEXT="feedback da corda pizzicata"/>
<node CREATED="1626190127736" ID="ID_204174502" MODIFIED="1626190159447" POSITION="right" TEXT="feedback da percussione con dita"/>
<node CREATED="1626190201967" ID="ID_441404017" MODIFIED="1626190231168" POSITION="right" TEXT="feedback e pitch shifting"/>
<node CREATED="1626190070005" ID="ID_360959328" MODIFIED="1626190085993" POSITION="left" TEXT="posizioni diverse di attuatore e piezo"/>
<node CREATED="1626190178986" ID="ID_1894746518" MODIFIED="1626190188859" POSITION="left" TEXT="feedback su strumenti diversi"/>
<node CREATED="1626190160575" ID="ID_494702376" MODIFIED="1626190178246" POSITION="left" TEXT="feedback da amplificazione senza eccitazione"/>
</node>
</map>
