# Appunti della lezione di mercoledì 6 ottobre 2021

## Alexander Schubert e metodo per organizzare il materiale

Link con tutto il materiale di lavoro per la realizzazione del brano.

Portfolio di brani da portare all'esame.

>Non considerare che l'insegnante da docente possa decidere cosa vorremmo noi fare.

Esigere tre cose dalla Scuola di Musica Elettronica:
- esami in presenza
- sentire le composizioni durante l'esame
- un giorno di saggio annuale

12/18 ore a parte per la tesi da richiedere al docente di COME 02

## Catalogo

Catalogo di gesti, che non fanno la composizione, con i gesti non si fanno la composizioni...

Comportamenti musicali che i gesti producono ovvero i suoni forma...

### Suono forma e gesto

1 nota singola è il gesto, varie note in più no!

Limiti fisici sulla chitarra in cui si concorre a fare una cosa mentre in altri strumenti no.

>Non porsi di fronte al limite fisico...

La parte ideale deve essere poli-percorso...

Distinzione dal punto di vista linguistico, in cui il limite è in genere un qualcosa di negativo, mentre la parola deriva dal latino:
- limes -> limite invalicabile per avere una differenza culturale ovvero per costruirsi un'essenza propria(confini dell'impero romano) -> questione per discriminare chi è o chi non
- limen -> soglia di casa, ovvero qualcosa che si attraversa di continuo, da spazio confortevole a non, che si decide di oltrepassare, non si prende in considerazione che fuori vi è qualcosa di culturalmente diverso

Il concetto di limite deve sempre essere diversificato.

Avendo la lavagna bianca, abbiamo tutte le possibilità universali decidendo cosa sono io e cosa sono gli altri. Prendiamo uno spazio e lo delimitiamo.

- Limes -> soglia
- Limen -> confine

![limes-limen](limes-limen.svg)

Grisey non ha mai amato il termine musica spettrale, lui chiamava la sua musica, musica liminale, ovvero sulla soglia della percezione.

Alcune cose non riusciamo a percepirle perchè sono brevi.

Ciò che sta sott la percezione è sub-liminale, sotto il limen.

Il processo d Grisey era prendere degli aspetti sub-liminali e farceli sentire.

Posso prendere una cosa e far esplodere un qualcosa e far divenire un'orchestra un meta-strumento.

Lucio Fontana sfonda un Limes...

Tagliamo la tela e per realizzare il passaggio... Fontana con i quadri _Concetto spaziale. Attesa_.

>La composizione è una specie di passeggiata in posti dove il compositore ti porta,decidendo determinati percorsi che qualcuno deve fare.

La composizione decide percorsi sonori...

## Vicinanza tra cinema e musica nel sonoro

Esigenza di base della musica di creare uno spazio...

Mettere mano alla percezione compositiva spaziale...

Avere un'importanza spaziale...

## Spazio come strumento

Scrivere per lo spazio e scrivere per il suono!
Gli spazi divengono strumenti...

Uno strumento suonerà sempre dentro un'altro strumento, che è uno spazio.

Integrare e capire che tipo di spazio richiede la composizione e decidere.

## Gesti e suoni forma

Fare i suoni forma e non i soli gesti...

Creare suoni forma in base ad i gesti che si hanno e porre e capire i percorsi...

Analizzare e capire cosa hanno i suoni forma, con leggero,distorto plateale etc...

Capire come 3 suoni forma si sviluppano tenendo un elemento del suonoforma precedente fisso.

Domanda base: cosa aggiunge tutto ciò, dove mi porta?
