# Appunti della lezione di Martedì 26 Gennaio 2021

## Perchè il corso di fondamenti:

- poca libertà di azione rispetto allo strumento computer
- realizzazione di un corso di fondamenti d'informatica

__________
## Le 6 macchie

[Le sei macchie per chitarra elettrica](https://gitlab.com/SMERM/BN-Tedesco/-/blob/master/COME-02/Secondo_anno/brani/2020_Davide_Tedesco_Sei_Macchie_per_Chitarra_Elettrica_matita_no_elettronica.pdf?expanded=true&viewer=rich)

Scrittura dei direttori e dei musicisti come Rachmaninovv -> problema nell'impossibiltà di fare un miscuglio compositivo.

Nei corsi di orchestrazione di Berklee:
>Non si deve scrivere qualcosa che non sia suonabile...

Per raggiungere certe cose, devi affrontare assieme allo strumentista un traguardo, ed un certo costo deve essere commisurato.

Bisogna riuscire a valutare appropriatamente i gesti che si richiedono di fare agli strumentisti.

>Si scrive e basta, quando si sa molto bene quello che si vuole dire.

Come Adriano Guarnieri, che ha un'estetica molto forte e ben delineata.

>Una visione del genere è auspicabile?

Quando ci si mette li e si scrive...

Studi per chitarra elettrica e classica

Corona -> sospende il tempo ma quanto deve durare? Corone diverse -> temporizzate e non (sequenze, formazioni, Ophanime)

## Lasciare un brano aperto per uno strumento

Brano per Vidolin per Live Electronics -> per strumento qualsiasi monodico

Come scrivere la partitura -> che sia adattabile automaticamente cambiando l'adattamento della partitura in forma monodica

>Approfittare di piú del carattere idiomatico per la chitarra elettrica, con tutto il lessico che si è sviluppato con il rock ed altri generi

Brani piú recenti per chitarra elettrica -> iper specializzazione

- suonare con l'archetto

>In generale è uno strumento poco specifico per la chitarra elettrica

__________

Dal punto di vista ritmico sono molto semplice e lineare -> aspetto timbrico molto poco dettagliato -> deve succedere qualcosa

__________

Specificare di più in ogni brano
__________

Quantizzare?!?

Scrittura troppo piena di semiminime con tactus troppo quadrato

La meditazione puó andare bene e puó andar male, in base all'esecutore

A fronte della quantizzazione ritimica, la quantità di abbellimenti è estremamente contenuta.

>Prendere in considerazione che non vi sono solo 12 altezze sulla chitarra

__________

Quarti di tono sul primo rigo di delta...

Sentire Lutoslawski su quarti di tono!

Microtoni, molto difficili da inserire

Haas -> Limited Approximations -> 6 pianoforti accordati in modi diversi -> intonazione realizzata molto precisamente prima.

I microtoni sono uno strumento potente ma difficile da maneggiare.

Non si possono fare le cose senza avere coscenza di ciò che si sta chiedendo.

## Separare i problemi

1. aspetti di scrittura specifica ritmica -> correggere aspetti separatamente

2. scrivere qualcosa che fosse sensato per le caratteristiche di estensione

__________
- scrivere studio per vedere se quello che ho in mente riesco a realizzarlo con uno strumento che in questo momento non lo puó fare

- piccoli pezzi invece non servono per studiare
__________

>Cercare di avere il pensiero elettronico mentre si scrive, poichè gli abbellimenti possono essere un espediente timbrico -> quanto è noiosa la faccenda e quanti aspetti di innovazione bisogna introdurre per avere l'attenzione alta

Coscienza del suono che deve uscire molto dalla scrittura -> che si accorda con i tempi metronomici

Identità di cosa sia ogni singola nota

______________

Corsi Hands-On

>Meno note su ogni rigo
