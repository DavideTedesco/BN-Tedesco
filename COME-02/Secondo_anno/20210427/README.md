# Appunti della lezione di martedì 27 Aprile 2021

- [libro Sorrentino](https://www.amazon.it/chitarra-elettrica-nella-musica-concerto/dp/8862316992/ref=sr_1_1?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Sergio+Sorrentino+%E2%80%93+La+chitarra+elettrica+nella+musica+da+concerto&qid=1619350372&sr=8-1)
- alla ricerca del chitarrista perduto
- in attesa delle registrazioni
- libri ritmo
- proposta di lezione mattutina per scrivere il programmino delle permutazioni

***

## Chitarristi musica elettronica

- Francesco Ferracuti
- Leonardo Polla

## Permutazioni

Domani alle 6

## Descrizione dei gesti

Graffiatura lineare della corda in 8 secondi o altro??

Difficile immaginarsi, esattamente quello che succede dal punto di vista sonoro.

## Timbro e ritmo

>Timidi nell'essere ritmici nel gesto, quando si stanno provando delle cose... Passaggi gradualissimi, ed assenza del gesto ritmico e quando si è poco sicuri della gestione di un parametro, se ne ferma un altro.

### Timbro e pornografia

Essendo bombardati dal ritmo pornografico...

## Testi sul ritmo

- [Cooper and Meyer](https://www.ibs.it/rhythmic-structure-of-music-libro-inglese-grosvenor-cooper-leonard-b-meyer/e/9780226115221)

- [Libro di Curt Sachs](https://www.amazon.it/Rhythm-tempo-study-music-history/dp/B0000CILYH)

Libro di C. e M. che parte dalla metrica greca, legata agli accenti.

## Affastellamento ritmico

Cosa succede quando si mettono insieme tanti metronomi che vanno in maniera differente si mettono insieme.

### Sinfonia N.4 di Ives - Secondo o terzo movimento

2 direttori di orchestra etc...

### Brano per una percussione di Bernardini

[Brano](https://soundcloud.com/nicb-4/bernardini-nicola-intermezzo-i-1997) per Kiki Dellisanti, per flauto dolce basso e strumento a percussione.

Sono state prese tutte le danze barocco, sono state sovrapposte, ed alla fine sono state prese funzioni quartali.

>Se non si mette una scrittura, il chitarrista capisce?

## Atteggiamento di Messiaen

Grammatica ritmica lineare in cui introduce degli elementi di disturbo che spostano la fase.

## Aspetto lessicale della musica

Parte notale delle note e dei timbri è lessicale, la parte ritmica è proprio la parte grammaticale.

>Per non usare un linguaggio con delle sue connotazioni notative, ci siamo fermati.

## Aspetto grammaticale del ritmo

C. & M. nel ritmo classico...

Loro arrivano a Beethoven, Brahms...

- [Libro sulla ](https://www.amazon.it/Geometry-Musical-Rhythm-What-Makes/dp/1466512024)

## Consonanza e dissonanza e ritmo

I rapporti sono sempre consonanti o dissonanti, e il ritmo è legato a questo discorso.

## Alternanza tra tempo metronomico e tempo cronometrico

Alternanza tra tempo scandito e tempo cronometrico.

### Tempo cronometrico

Esso è come un testo scritto. L'interprete lo deve leggere e non lo leggerà piatto, ma lo leggerà dandogli tutta l'ulteriore lettura di cui un attore è capace e riesce a pensare.

Il tempo cronometrico non si usa nella vita reale.

### Tempo metronomico

Esso ha tutto l'aspetto gestuale della parola.

>La parola scritta è la parola imbalsamata.

Se disponiamo di tutte e due le tecniche, ma esse hanno funzioni diverse.

## Stockhausen e il tempo 72,4

Essa ha un senso perchè vi sono due tensioni la precisione cronometrica del tempo metronomico.

## Rendering di Berio

La Sinfonia di Schubert ha una grammatica ritmica ben precisa, che poi Berio non tocca.

>Sorella di Bernardini, restauratrice. Gli affreschi di Giotto ad Assisi, non devono andar perduti i gesti dell'artista. Passaggio dal tempo metronomico al tempo cronometrico in Berio, e vi sono corone temporizzate e non è importante che si rispetti il cronometro e il direttore sa che si blocca tutta la grammatica temporale elaborata da Schubert perchè non c'è Schubert.

In questo brano è estremamente sensata le gestione temporale.

L'utilizzo di una cosa piuttosto che un'altra deve trovare un senso.

## Primi minimalisti americani e ritmo

Ritmi utilizzati sono di tipo additivi in cui viene shiftato costantemente l'accento, ciò ha senso in un contesto minimalista.

## Berio

Berio derivava tutto dalla sua esperienza dello Studio di Fonologia e dei jingles.

Questo lavoro gli ha dato un'esperienza dal punto di vista tecnico che molte poche persone hanno potuto avere.

## Il problema è che partiamo da suoni dati...

Dobbiamo creare suoni e non partire da suoni dati.

Limiti di creazione del suono...

Esperienza importante della musica concreta, e siccome i suoni si possono raccogliere per strada e sono interessanti.

Alvin Curran è in grado di gestire il ritmo.

## Chitarra e grammatica ritmica

Essa ha una grammatica ritmica legata al gesto musicale dell'interprete.

È come se ogni strumento fosse una lingua(Eco libro sulla traduzione), vi sono tutti i luoghi comuni legati ad aspetti concreti...

### Che cos'è l'assolo?

Esso è una superfetazione dell'aria lirica, con un'esposizione lirica rapsodica di combinazioni di temi.

Se si ha una grammatica semplice e solida come batteria, basso etc... Funzionerà tutto sopra.

## Specifica del ritmo

Specificare gli asintoti ritmici...

![as](as.png)

## Stuzzicare l'articolazione

Ciò porta a parole, fonemi e come si gestisce il rapporto con il materiale del bagaglio culturale...

Sovrapposizione di elementi di realizzazione algoritmica.

## Conoscere lo strumento?

È l'interprete che conosce il suo strumento...

Creare qualcosa che si sente profondamente che sia nostro e che è fatto per uno strumento.

La contrattazione con lo strumentista per la realizzazione del brano.

## Partita per voce sola di Bernardini

Per costruirla essa, è stata costruita come modello dell'esecuzione.

Flussi che si muovono in alcuni sensi con l'esecuzione di un esecutore specifico.

## Pause e lunghezze

Lunghezza delle pause e lunghezza delle note.

L'ordine algoritmico ha un suo senso, quando l'ordine algoritmico si arricchisce di un aspetto interpretativo, si raggiungono risultati fantastici.

## Director Musices

[Director Musices](https://www.speech.kth.se/music/performance/download/), per l'interpretazione di partiture.

## Ricerca sugli strumentisti

[Istituto di Intelligenza Artificiale](https://www.meduniwien.ac.at/ai/de/index.php)

## Aperitivi acusmatici

Concerti di musica acusmatica con aperitivo, si accede all'aperitivo solo se si ascolta tutto il concerto di musica acusmatica.

Concerti alle ore di 18

Bisogna gratificare le persone...

## Gameification della realtà

Si è attratti a fare certe cose se si è inclusi in un gioco.


***
- specificare il ritmo del graffio della corda
- le playlist non servono per allenarsi
- portare partitura finita

***

**La composizione non si puó insegnare ma si puó discutre!**

## Transizioni graduali

Le cose non si fanno la guerra...

[Shaw - Torniamo a Matusalemme](https://www.ibs.it/torniamo-a-matusalemme-saggio-di-libri-vintage-george-bernard-shaw/e/2560224185900)

>Cercare di smussare gli angoli...

[Chant D'Ailleurs descrizione](http://www.vinao.com/Chant%20D'Ailleurs.html)

[Chant D'Ailleurs video](https://vimeo.com/116237487)

***

## Critica del gusto

[Distinzione di Burdieu](https://www.ibs.it/distinzione-critica-sociale-del-gusto-libro-pierre-bourdieu/e/9788815080691?lgw_code=1122-B9788815080691&gclid=CjwKCAjw7J6EBhBDEiwA5UUM2tuz--GK1FZj6e7Ir5ygq8V3v1RuroXwAVzzCHrmt7yQbyKWkWGUIhoCNbIQAvD_BwE)

>L'arte è sempre servita per specificare il gusto della classe dominante.

Oggi a cosa serve l'arte?

## Burdieu e la teoria dei capitali

Capitali economici, sociali e culturali...

[Accelerate Manifesto: For an Accelerationist Politics](https://www.amazon.it/Accelerate-Manifesto-Accelerationist-Williams-Srnicek/dp/6079745046)

## Analisi pseudo-acustica del brano di Ferneyhough

[Brian Ferneyhough - Lemma Icon Epigram (1981)](https://www.youtube.com/watch?v=odTUqs8rJDg&ab_channel=ContemporaryClassical)

## Slap non con pitch

Tongue
***
- Comico George Carlin ->
- [dlscrib](https://wiac.info/)
- [Kagel Unguis Incarnatus Est](https://www.universaledition.com/mauricio-kagel-349/works/unguis-incarnatus-est-5546)
- [Victor Borge](https://www.youtube.com/watch?v=W8R0ZwYvXpg&ab_channel=thepolonaise)
