# Appunti della lezione di martedì 9 marzo 2021

## Successioni

Ereditarietà -> processo con memorie

Subentrare -> non è soltanto espandere in senso estensivo, ma anche intensivo

Espandere un frammento minimo il piú possibile.

>Aspetto musicale essendo un aspetto termodinamico, bisogna espandere...

Quantità di ridondanza per memorizzare ed usare la memoria in maniera efficace

Hindemith -> grande criticato dal punto di vista estetico -> perchè usava queste tecniche

(Emanuele Casale -> compositore elettroacustico -> lavora molto dal punto di vista ritmico -> Hindemith della musica elettroacustica -> Allievo di Cipriani)

Variazioni opera 27 Webern -> variazioni sul concetto di simmetria

brano pozzo -> potrebbe essere primo o ultimo -> la costruzione dei brani è indipendente dai brani prima

**filtro storico** -> al posto di testamento -> nel momento in cui uno strumento si presenta in scena già sappiamo -> generare la sorpresa

>Filtro storico nel rock e nel pop è un surrogato della voce di soprano, bending, vibrati, acuti, fanno parte della retorica presa dalla retorica belcantistica.

Con la letteratura pop e rock abbiamo una sorta di testamento^2...

Problema del testamento letterario della chitarra elettrica -> bisogna analizzarlo bene -> da dove viene...

Dipende a chi si riferisce il testamento -> chitarristica Tom Morello -> Rage Against The Machine -> chitarra come generatore di suoni

Dalla voce di soprano si passa alla gestualità fallica -> cambio di sesso avviene quasi senza parere...

A che testamento fa riferimento il chitarrista?

Area di coloratura di una cantante lirica -> facile essere piú veloci, piú dinamicamente forti
(problema della nota tenuta)

Nota tenuta per avvicinarsi all'acuto lirico e superarlo ed andare avanti ad libitum con Santana

Riferimenti relativi ad una letteratura piú antica

Chitarra elettrica ha 2 personalità diverse nel rock:
1. surrogato o della voce lirica
2. elemento ritmico
______

Prendendo alfa -> si prende una piccola cellula e sviluppo in uno studio

Limitarsi per avere un contesto in cui muoversi -> lavorando su un elemento

Gestualità -> significa che si agisce sulla dinamica -> armonici e gestualità sulla dinamica

Suono-forma = esplicitare il processo del suono-forma

Catalogo di suoni forma il catalogo di comportamenti

Quando si pensa molto e si realizza poco -> faccio il postino -> uso messaggi e la mano segue l'inconscio -> faccio una serie di valutazioni microscopiche ad intuito

Perchè come mi muovo la scrittura si espande in un certo modo ? -> verse scelte piú consapevoli

________
Alexander Schubert -> tomo da leggere: Switching Worlds -> sull'estetica di Schubert
________
A che servono le innovazioni tecniche? -> cross seminarci sulla costruzione di contenuto -> cosa c'è da dire oggi?

Interessante incominciare a sviluppare e discutere nei parchi...

Le lezioni sono molto aride...

Estetica ed immaginario si muove molto insieme all'immagine tecnica
________
Cosa succede nel mondo ora?

Problema di memoria storica -> contemporaneità -> lavoro di ricerca colossale, la storia fa tutto questo lavoro e nella storia rimangono coloro che hanno avuto una grande interazione con il mondo dell'editoria etc... Ma oggi Alexander Schubert lo si trova in rete...

Il problema è che la contemporaneità è molto difficile da esperirla...

________
Da portare per la prossima lezione:

- 1/2 percorsi timbrici
- realizzazione dei percorsi in note
