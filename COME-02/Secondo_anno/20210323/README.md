# Appunti della lezione di martedì 23 marzo 2021

- scordatura che si porta avanti sulle varie successioni(eredità)
- armonici come elementi traditori del destino
- realizzazione di una griglia di probabilità (0-100) degli elementi
- problema della percussione sul manico, sotto una certa soglia il gesto è poco rilevante

[permutazioni in Java](https://www.geeksforgeeks.org/generate-all-permutations-of-a-string-that-follow-given-constraints/)

(Dive bomb pizzicato con mano sinistra-> seguito da leva tremolo dive bomb -> tapping su altra corda portando la leva su)
______


- punti per violiono Pendercki

- scrittura di Ko-Tha di Scelsi percussiva

Componente fondamentale musicale ovvero l'aspetto ritmico -> dov'è nella percussione?

Metodo ritimico percussivo??

Il tipo di tensione e risoluzione che proponeun ritmo da un altro significa due cose diverse.

**Problema di far ascoltare le risonanze** -> impulsi che permettano di far valorizzare le risonanze.

La prima volta che succede una percussione -> l'ascoltatore non capirà quello che succede

![](.png)

Questa scrittura permetterebbe di fare dei percorsi graduali

La partitura piú tradizionale è, meglio è!

_Togliere rumore inutile dalla partitura_

-> Scordatura che si conclude con tutti i pezzi

## Scordatura

- suono di posizione scritto

- suono risultante (piú piccolo perchè non si deve leggere)

Direzione su una corda -> quando si cambia corda -> bisogna inserire un indicazione di cambio corda

**Calcare la mano sul virtuosismo di uno strumentista**

______

## Domande

- aspetto probabilistico -> nicb -> caricatura probabilistica

- è il 23 Marzo 2021 -> sessione d'esami a fine Luglio
