# Appunti dell'incontro di martedì 13 luglio 2021

Per il feedback con la chitarra, muoversi

Bisogna provare determinate cose per il feedback, Leonardo scenderà a Roma, cominciare a preparare materiale con idee di cose che si potrebbero provare per realizzare un megacatalogo di gesti musicali, tipo 100 gesti musicali.

Questa è la tecnica di Netti, in cui sperimenta tantissime cose sullo strumento...

Ciò pone 2 limiti:
1. fare delle cose troppo semplici dal punto di vista tecnico se realizzati esempi dal compositore
2. realizzando determinati gesti, un compositore li farebbe con una tecnica sbagliata ovvero da persona che non conosce lo strumento

- realizzare input per l'esecutore e non lasciarlo andare da solo, dandogli delle linee guida.
- tirar fuori suoni da un nuovo strumento, da compositori d'oggi; la cosa migliore è aver chiaro un risultato, e bisogna capire come far uscire un certo risultato

Spesso facendo delle registrazioni staccate si ha un rapporto con l'esecutore diverso.

**Preparare una serie di cose interessanti** da far provare allo strumentista...

(Pezzo di Mattia Clera con elementi non riproducibili...)

>Avere in mente l'idea di suono e non come si fa, ciò è ruolo dell'esecutore...

Provare assieme a Leonardo le idee e poi con le registrazioni in mano iniziare a strutturare il brano mettendo in sequenza degli elementi con i punti nodali che fanno da intertempo.

**Prima di provare le cose sugli strumenti, è interessante l'idea di suono a prescindere da chi lo suoni...**

>Pensando allo strumento si sbaglia subito, bisogna pensare quindi al suono...

Fino a un certo punto la composizione era divenuta un qualcosa di puramente teorico.

Esistono tantissime trascrizioni di brani...

>L'orchestra è stata costruita per i clichè, l'orchestra è servita prima della musica strumentale, per accompagnare le opere.

L'opera a commento serve a descrivere e rappresentare due cose:
- amore
- guerra

E tutte le declinazioni del caso...

Ogni famiglia di strumenti si porta appresso clichè molto forti...

>L'obiettivo è il percorso che si sta creando...

**I pezzi si scrivono in contemporanea, non uno alla volta...**

>il problema di scrivere per strumenti è che noi portiamo avanti delle idee, che poi chiamiamo clichè, ogni strumento ha in se una cosa che si chiama clichè con cose scontate e letteratura.

Quando ci poniamo a scrivere per uno strumento se pensiamo a scrivere automaticamente per uno strumento, gran parte delle cose le scriviamo per clichè, molte cose saranno portate avanti dal clichè stesso.

_Corso di analisi della musica contemporanea tenuto da Pasquale..._

___

Tutto quello che costruisce Clara Iannotta è suono puro, la forza piú grande che abbiamo è sulla parte elettronica.

___

Provare l'approccio al suono costruendo un acusmatico senza pensare alla chitarra -> ovvero ciò che fa Clara Iannotta...

Sorta di Sketchbook di suoni elettronici...

Live Electronics viene dal ricopiare un'idea di suono:
- disegno
- bozzetto in cera persa
- concept
