# Appunti della lezione di Martedì 16 Febbraio 2021

- analisi spettrale
- scrittura
- che forma?
- dare un senso complessivo a sei macchie?
- diverse tipologie di pickup
- come gestire strumenti diversi?

_________
## Strumenti diversi: cosa fare?

Con pickup diversi cosa succede?

Si puó realizzare un pezzo per strumento qualsiasi e Live Electronics?

Non importa ribaltare lo strumento ed i brani si possono realizzare con qualsiasi strumento che possa suonare le note scritte.

Bach nei concerti brandeburghesi fa delle specifiche strumentali, che possono essere realizzate solamente con una combinazione timbrica.

Il pickup è come la microfonazione per una chitarra classica.

Realizzare in due modi diversi per ottenere lo stesso effetto.

## Forma

>Elefante nella stanza, il brano non esce fuori.

Forme autoconlusive realizzano un discorso finale?

Suite di danze ad esempio, non sono cosí corte poichè bisognava far danzare. Scelta ritmica, scelta dei tempi metronomici.

Tema e variazioni:

1. discorso complessivo
2. discorso articolato in tante mode diversi

>Ragionare sulle suite, ad esempio e fare un qualcosa

### Miniature o studi

Miniature come studi, studi di tecnica strumentale e studi compositivi.

#### Chopin
- op 10 -> tecnica
- op 25 -> studi compositivi

(Miniature estrose per pianoforte)

_________
## Auto analisi

Perchè preferisco questo arco compositivo fatto di piccolo brani autosufficienti?

>Riesposizione dei tema, e costanti deviazioni, con costruzioni formali estremamente complesse.

Confrontarsi -> con lo strumento

Gli studi erano una forma di esplorazione dello strumento, e concepire qualcosa di non estesissimo e prende uno sviluppo preciso e lo sviluppa.

- scelgo degli elementi
- separo elementi artificialmente
- su ciascuna cosa invento una piccola costruzione musicale per studiare

Incedere è quasi burocratico...

>La forma dello studio ha lo scopo esplorativo.

Dal punto di vista costruttivo musicale -> incedere burocraticamente.

(Nynphomania di Lars Von Trier)

>Elenco di roba

Esercizi di stile di Raymond Quenau -> tema e variazioni -> 99 -> Eco traduzione

## Analisi spettrale

Ha senso realizzare un'analisi spettrale?

Per poter rendere interessante il lavoro sullo spettro, bisogna fermare tutto. (Scelsi, Netti, Grisey)

Questioni di estetica...

- pienisti
- vuotisti

>Lavorare dal punto di vista musicale sullo spettro ha senso?

Aspetto olistico della combinazione di cose è importante...

>L'estetica è un'ideologia...

## Filtro storico del tempo

Vediamo tutto incastrato nel flusso temporale storico, lavoriamo comunque partigiani di qualsiasi cosa, perchè delle parti sono ancora vive.

Difficile parlare in modo musicale asettico di tendenza, luogo musicale etc...

Sviluppo dell'estetica di Branchi -> non vuole più che esista la ritualità del concerto -> gli interessa la circostanza.

>Non si possono giustificare certe posizioni estetiche.

________
Cosa fanno Billone, Netti, Durazzi, Haas, Furrer?

Derivazioni moderne di concetti spettrali?
Non c'è un accordo sulle cose...

>Discorso della ricerca timbrica sulla chitarra elettrica -> porta da qualche parte?

Vibrazione della corda...

(Sono più importanti gli errori dei grandi -> c'è qualcosa da imparare)

Poliphonie X di Boulez -> sbagli
________

- esercizi di stile
- analisi spettrale dei pickup
