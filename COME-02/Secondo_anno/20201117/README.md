# Appunti della lezione di Martedí 17 Novembre 2020

## Ricognizione degli affetti musicali chitarristici - composizione

- Francis Puoulenc - Sarabande
- Heitor Villa-lobos - 12 Études for Guitar
- Leo Brouwer - 20 Études Simples (I, IV, V)
- Napoléon Coste - Studi Op. 38
- Matteo Carcassi - Studi op.60(II, scale)
- Fernando Sor - Studi (selezionati da Gangi e Carfagna 2 volumi)
- Mario Gangi - 22 Studi (II, III, IV)
- Robert de Visée - Suite in D Minor
- Mauro Giuliani - Studi op.111 (7, )
- Barrios Mangoré - Prelude in C minor
- Stephen Dodgson - 20 Studies for guitar

### Che caratteristiche hanno questi studi e brani? (Parametri di catalogazione)

#### Domande
- stile
- componente armonica
- componente melodica
- componente ritmica
- componente timbrica
- perchè sono un affetto importante e cosa vogliono dire nella mia storia musicale? (la storia musicale è il percorso formativo musicale della persona)
- vi sono frammenti asportabili?

#### Risposte
- la tecnica utilizzata è prettamente tradizionale
___________

## Appunti della lezione

(Il maestro di composizione faceva una seduta psicanalitca inizialmente.)

È giusto farsi delle domande per fare trascrizioni?

La trascrizione è una tecnica analitica:
- fare la trascrizione quando si cerca qualcosa
- questo mi serve intanto per studiare il pezzo

"Comporre se stesso" ovvero comporre tutte le parti, fare una sintesi.

Scritti Corsari di Pasolini poesia

Senza titolo, il libro delle cartoline di Sanguineti, poesia su delle cartoline.

Comporre tutti i pezzi.

Problemi:
1. comporre se stesso -> come ricucire il tutto? soluzione perfetta per me...
2. problema e poi elenco delle possibili soluzioni

___________

### Strategia compositiva
- Materiali abbondanti -> esempio del brano per pianoforte, ricognizione di tutta la letteratura a cui si è affezionati (letteratura top of mind)
- contare i brani
- guardare i brani -> essi spaziano nel tempo
- predisposizione in maniera storica -> storia avanti e all'indietro contemporaneamente e si culmina in quello che è l'apice (frammenti passati in specchi deformanti(se si hanno degli intervalli sovrapposti li si allarga o li si stringe) -> tecnica per deformare) *non citare letteralmente i brani -> ma citazione di tipo affettivo
- citazioni affettive, ovvero deformazione di una citazione
- regole astratte -> tipo particolare di serialismo, serialismo permutativo, cosí anche se ci sono tanti frammenti, sono collegati dal punto di vista  globale
- collezione di gesti pianistici tipici -> cluster, accordi lati
- timbri pianistici

L'estensione timbrica ti serve per levarti da una concezione classica.

Ho una collezione di oggetti:
1. frammenti di letteratura
2. gesti chitarristici (rasgueado, etc...)
3. timbrica normale
4. timbrica estesa

Ingredienti che incomincio a pensare a cosa farci

"Ad esempio tenere isolati gli ingredienti e distribuirli statisticamente, ad esempio piú probabile che ci siano dei frammenti affettivi in un punto, poi qualcosa di timbrico e "

Pesca degli elementi da inserire con un random per una distribuzione. Funzioni lineari che pesano il random incrociate in un certo punto e moltiplicano ai frammenti.

- Inventarsi un metodo per distribuzioni non uniformi, che si possono fare filtrando la distribuzione uniforme(con gaussiane)

Aspetto interessante di Netti -> estensione spettralista per strumenti solo, problema di fondo con spettralismo musicale, è fiera edonista dello strumento.

Una delle operazioni per far cantare ed emancipare il timbro, è fissare il timbro e realizzare questa roba.

Berio, concentrarsi sul timbro ma non azzerando gli altri parametri.

"È tutto timbro" -> cos'è la melodia, il ritmo.

Problema dell'unificazione dei sensi è complicato perchè si ha un aspetto hardware, e un aspetto software.

## Metodi per uscire dall'empasse

Le idee vengono se ci si muove, e non contemplare, ma agire.

Con le collezioni, catalogarle, con dei dizionari, usare le tecnologie infromatiche per quello che ci possono dare.

Fare un database con file YAML, accumulando dati salienti per me(tipo indice di affetto che permette di ordinare le cose).

Cosa farci con questi database?

Per muoversi si pesca a caso, nel pescare a caso, tipo con l'anno di composizione.

Catalogare in database -> **muoversi**

Logica interessante con la quale ho catalogato, perchè certi entrano e certi no?

Ma perchè non stava nel secondo scaffale?

**Indice di rilevanza potrebbe cambiare** ed in generale nell'azione.

Esercizio quotidiano farsi venire idee.

YAML ha un problema, ovvero devi scrivere tanto
___________
