# Appunti della lezione di martedì 8 giugno 2021

## Cosa abbiamo fatto
- dialogo con il chitarrista
- hub per lo strumentista
- visione della partitura

## Hub per lo strumentista

Simple is beautiful

## Traiettorie di Stroppa

Partitura di Marco Stroppa di Traiettorie con registratore reel-to-reel.

## Problema della velocità

Accellerando e velocizzando
![scri](scri.png)


## Nota risultante

Inserire pentagramma con nota risultante...

Più scordature, pentagrammino piú piccolo con note risultanti...

## Virtuosisimo e strumentisti

>Se il pezzo è virtuosistico il

____
Sciarrino -> brano per contrabbasso

Netti -> rigo di ossia per gli strumenti ad arco...

## Partitura di Guarnieri - Dissolto nella notte (2018) per pianoforte e Live Electronics

Guarnieri compositore molto espressivo e forte e scrive però sempre molto simile.

![gua](gua.png)

***

## Argomenti di cui si è parlato

- [Julian Scordato](https://www.julianscordato.com/bio.html)

- [Chigiana](https://www.sienacomunica.it/chigiana-international-festival-summer-academy-2021/)

***

## Seminario sull'FFT del Maestro Bernardini

### Breve ripasso sui numeri complessi

Essi sono numeri su un piano bidimensionale, essi sono numeri bicomponenti.

Essi sono essenzialmente numeri che stanno su un piano.

Essi sono un'estensione dei numeri reali in cui ciascun numero è fatto di 2 numeri reali...

Essi portano ad avere una rappresentazione con un continuo oscillare intorno ai valori i, i^2, i^3 e i^4=1 e i^5=i e cosí via.

Il fatto che giri in tondo significa che abbiamo la combinazione di una componente cosinusoidale e sinusoidale.

#### Rappresentazione di Eulero

Eulero scopre che si puó rappresentare un numero immaginario in questo modo, con la base e:
![eu](eu.png)

Stiamo dicendo che il coseno è la parte reale e il seno è la parte immaginaria.

Programmino in Python per Eulero `eulero.py`:

```
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-2*np.pi, 2*np.pi, 500)
y = np.exp(1j*x)

plt.plot(x, y.real, x, y.imag)
plt.show()
```
![eu1](eu1.png)

### Fourier

Egli era un matematico di Napolone della fine del '700.

Lasció il matematico in Egitto che organizzava e gestiva tutto in Egitto.

La matematica ha un certo modo di affrontare le cose, ed avendo dei punti nello spazio ci chiediamo quale sia l'equazione di questa funzione?

![eq](eq.png)

Abbiamo 4 equazioni diverse sommate...

Ciò non funziona però con le funzioni periodiche, perchè hanno un ciclo che si ripetono sempre uguale.

Avendo una funzione periodica, come si fa a capire quale sia la combinazione di funzioni che la riesce a comporre.

>L'idea è che essa sia una combinazione di funzioni periodiche.

L'idea è quindi di dire, che se calcoliamo l'area sottesa dalla funzione, tipicamente se la funzione è periodica ed è equilibrata intorno allo 0, l'integrale della funzione mi dovrebbe dare 0:
![in](in.png)


Il calcolo delle aree dovrebbe essere nulla...

Incidentalmente la costante è la frequenza 0.

Fourier pensò che prendendo una sinusoide e facendono l'integrale, calcolando l'area sottesa da una cosinusoide, avremo che prendendo l'integrale di una cosinusoide preso in maniera opportuna con un numero giusto di cicli, l'integrale sarà 0.

![cos](cos.png)

Ma se invece di un coseno, realizzoo un cos^2, avrò delle componenti che esistono oltre la frequenza base...

Moltiplico la forma d'onda che voglio analizzare con infinite frequenze.

Dove trovo l'area diversa da zero, vuol dire che c'è qualcosa...

![cos2](cos2.png)

>Nel caso delle funzioni periodiche è la somma di funzioni periodiche.

### Programmino per moltiplicare sinusoidi

Moltiplico una sinusoide:
- prima per una frequenza qualsiasi 0.12
- poi per se stessa `y1*y1`

![p](p.png)

Sommiamo e poi dividiamo per il numero di punti.

![pp](pp.png)

Possiamo immaginare che la prima a sinistra se facciamo la somma di tutto ci da 0.

A sinistra vediamo che la somma è 0.0005, aumentando i punti andrà a 0.

A destra vediamo che se facciamo la stessa cosa, il cos^2 avrà una somma diversa da 0.

Approssimamo a 0.5 -> vedremo successivamente perchè.

Una FFT o DFT consiste nel moltiplicare una forma d'onda con una serie di componenti sinusoidali o cosinusoidali.

### Programmino per la prima DFT

Abbiamo un ciclo che realizza la DFT:
![dft](dft.png)

Abbiamo x, come lo spazio nel tempo...

f è l'asse delle frequenze che elaboro tra -nyquist, fino alla frequenza di campionamento-un periodo.

Abbiamo un esponenziale complesso come forma d'onda.

![y](y.png)

Mi preparo un vettore vuoto dove metterò i risultati dell'analisi.

Prendo ciascuna frequenza che inserisco nella variabile k chiamata `analfun`, in essa mettiamo un esponenziale negativo.

Abbiamo stavolta la frequenza è `k`...

Ora moltiplichiamo la funzione per y....

Convertiamo l'array in un array di numpy e ne calcolo la magnitudine, ora mi serve di calcolarne la magnitudine.

Abbiamo 2 valori:
- la magnitudine -> distanza dal centro -> modulo -> chiamata m
- la fase -> ovvero l'angolo

![mf](mf.png)

Pensando ad un oscillatore come una cosa che ruota intorno ad un cerchio abbiamo un osccillatore che ruota in senso antiorario:
![os](os.png)

Prendiamo come funzione analitica è la funzione opposta e quando la fase corrisponderà, avró una sorta di allineamento.

La funzione analitica gira praticamente in senso opposto.

Ciò quando la frequenza corrisponde...

>Nel Signal Processing e nell'analisi di Foruier ci interessa la magnitudine(modulo) e la fase.

La funzione che vogliamo analizzare ha in teoria ampiezza 1.

![dft0](dft0.png)

Stiamo guardando tra +50 e -50...

### Dettagli non indifferenti...

La frequenza è stata scelta in maniera oculata, perchè venga questo risultato qui è necessario:
- usare una funzione complessa
- scegliere oculatamente una frequenza

Sono state scelte un tot di frequenze, da -500 a +500.

Abbiamo scelto la frequenza precisa e avremo 0 errori, ovvero avremo un ciclo preciso...

Ma le funzioni audio non sono complesse, ma reali.

Se combino 2 funzioni complesse a frequenze precise con ampiezze diverse:
![dft2](dft2.png)

Produciamo la figura vista, in linea di principio Fourier ci azzeccato.

### Fourier e l'audio
Dato che le funzioni audio sono funzioni reali.

![dft3p](dft3p.png)

Provo a fare l'analisi con il coseno con una funzione esponenziale complessa con una funzione reale.

![dft3](dft3.png)

Prendendo la formula di Eulero vediamo che:
![eee](eee.png)

Il seno di alfa equivale a una funzione che gira nel senso orario, un'altra tutto diviso per 2.
Se analizzo una funzione reale ottengo l'ampiezza a metà distribuita su 2 componenti simmetriche intorno allo 0.

Abbiamo preso ora da -Nyquist a +Nyquist, se prendiamo tra 0 e la frequenza di campionamento.

![cmp](cmp.png)

![fff](fff.png)

La portante è la frequenza di campionamento e la modulante è la nostra funzione.

Abbiamo in questo il modo fatto bene:
![ben](ben.png)

Se scomponiamo in sinusoidi è uguale ma le sinusoidi ce le troviamo rovesciate di fase.

La formula di eulero per le sinusoide è:
![eeee](eeee.png)

### Con frequenza non in tabella
![dft4](dft4.png)

Ora abbiamo diminuito i punti di analisi e vediamo che succede:
![dft4p](dft4p.png)

L'energia e l'area della funzione si stende e la magnitudine è molto minore perchè l'analisi si distribuisce spettralmente.

>La cosa funziona ma non è piú bella e pulita ed è anche piú realistica.

Nel secchiello della frequenza 150.59 diminuiamo la precisione per avere un secchiello largo, ed essa è diversa da 148.

Vedendo la funzione in quel punto abbiamo una roba che si avvicina a un quadrato perfetto.

Tutta l'energia non viene assorbita interamente:
![spa](spa.png)

Dire che uso 255 campioni di analisi, vuol dire che chiudo 255 campioni nel tempo...

Abbiamo una parte non bilanciata da nulla dopo la sbarra, avremo quindi che:
- i quadrati non corrispondono
- l'analisi non è illimitata -> approssimiamo un risultato

![bin](bin.png)

### Vero problema della DFT

Avendo dei bin sempre piú stretti, avremo un bin che corrisponde ad una frequenza precisa, ma perchè non lo facciamo?

Ci dobbiamo sempre ricordare che le funzioni devono essere periodiche per Fourire, se le funzioni non sono proprio periodiche che facciamo?

Gli ingegneri decisero di individuare funzioni periodiche per periodi limitati di tempo.

Allora facciamo finta che in un certo periodo limitato di tempo la frequenza è un'altra.

>Il problema grosso delle DFT è che vi è un trade off, perchè per fare un'analisi precisa significa che si devono prendere piú campioni, ma allora l'analisi non sarà piú corretta.

Avremo un **trade off** tra:
- definizione frequenziale
- definizione temporale

Possiamo quindi avere:
- analisi con pochi campioni -> molto rozze
- analisi con molti campioni -> con segnali stabili

### Risoluzione del Problema

Dobbiamo trovare il modo di ridurre l'approssimazione.

Se ho il problema che taglio in un punto qualsiasi che non so, potrei fare in modo che i bordi valgano di meno:

![pro](pro.png)

Finsetra di Von Hann...

Hamming realizzò invece un'altra finestra.

Il ruolo della funzione è per diminuire il rumore.

#### Le finestre

- finestre rettangolari -> Box car
- finestre di Von Hann
- finestra triangolare
![tri](tri.png)


Il segnale diviene di modulazione di ampiezza con una combinazione del nostro segnale piú una componente di Von Hann, ed avremo un bin a sinistra ed un bin a destra che non si riescono a tirar via.

Una finestra di Von Hann produrrà una componente sola.

***

Maniera in cui si scelgono le finestre (zero phase windowing) è importante individuare la costante bene...

FFT ha un grosso problema, ovvero la frequenza 0 non è mai rappresentata.
