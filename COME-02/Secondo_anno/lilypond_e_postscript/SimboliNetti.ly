

ringsps = #"
  0.10 setlinewidth
  0.9 0.6 moveto
  0.4 0.6 0.5 0 300 arc
  stroke
    "
  

  ring = \markup {
 \rotate #220
  \postscript #ringsps
}


arrow = \markup {
  \scale #'(0.17 . 0.17)
    \rotate #290
  \triangle ##t

}

circbow=
\markup{  \scale #'(1.6 . 1.6 )
  \combine
    \ring
 \translate #'(-0.4 . -1.24)
 \arrow
  }
  
  
  
  
  %OTHERS
%============================
  
croce=
\markup
\path #0.15 #'((moveto -2.25 0)
			 (lineto 2.25 0)
            		(moveto 0 -1.25)
		        (lineto 0 1.25))
		 
linea=
\markup
\path #0.15 #'(
            		(moveto 0 -1.25)
		        (lineto 0 1.25))

cerchio =
\markup {
  \fontsize #5
  \override #'(thickness . 2)
\center-align \draw-circle #.58 #0.15 ##f
}

cerchiocroce = \markup{
\combine \cerchio \croce}

cerchiolinea = \markup{
\combine \cerchio \linea}

arcops= #"
  0.05 setlinewidth
  0.9 0.6 moveto
  0.4 0.6 0.5 0 180 arc
  stroke"

arcoL = \markup { 
 \rotate #90
 \scale #'(2.5 . 2.5)
  \postscript #arcops
}

arcoR = \markup { 
 \rotate #270
 \scale #'(2.5 . 2.5)
  \postscript #arcops
}








dcroceo=
\markup{
  \combine
    \cerchiocroce
 \translate #'(-1.5 . 1)
 \arcoR
  }
  
  
 ocroced=
\markup{
  \combine
    \cerchiocroce
 \translate #'(1.5 . -1)
 \arcoL
  }
  
  odnovanta=
  \markup{
   \combine
 \cerchiolinea
  \translate #'(1.45 . -1)
  \arcoL
  }
  
  odduesette=
  \markup{
  \combine
 \cerchiolinea
  \translate #'(-1.45 . 1)
  \arcoR
  }
  
 tri=  \markup {
  \triangle ##f
 			}
  


#(define (airohalf radius height thick) 
    (string-append "gsave
                    /msellipse {
                          /endangle exch def
                          /startangle exch def
                          /yrad exch def
                          /xrad exch def
                          /y exch def
                          /x exch def
                          /savematrix matrix currentmatrix def
                          x y translate
                          xrad yrad scale
                          0 0 1 startangle endangle arc
                          savematrix setmatrix
                      } def
                    /rad " (number->string radius) " def
                    /offset " (number->string height) " rad mul def
                    /ecenter 0 offset add def
                    /xradius rad rad mul offset offset mul sub sqrt def
                    /yradius rad offset sub def
                    " (number->string thick) " setlinewidth
                    currentpoint translate
                    0 0 moveto
                    newpath
                            0 rad rad 0 360 arc
                            fill
                    0 rad translate
                    1 1 1 setcolor
                    newpath
                            0 ecenter xradius yradius 0 -180 msellipse
                            closepath
                            fill
                    0 0 rad sub translate
                    0 0 0 setcolor
                    newpath
                            0 rad rad 0 360 arc
                            stroke
                    grestore"))

#(define (airuhalf radius height thick) 
    (string-append "gsave
                    /msellipse {
                          /endangle exch def
                          /startangle exch def
                          /yrad exch def
                          /xrad exch def
                          /y exch def
                          /x exch def
                          /savematrix matrix currentmatrix def
                          x y translate
                          xrad yrad scale
                          0 0 1 startangle endangle arc
                          savematrix setmatrix
                      } def
                    /rad " (number->string radius) " def
                    /offset " (number->string height) " rad mul def
                    /ecenter 0 offset sub def
                    /xradius rad rad mul offset offset mul sub sqrt def
                    /yradius 0 rad offset sub sub def
                    " (number->string thick) " setlinewidth
                    currentpoint translate
                    0 0 moveto
                    newpath
                            0 rad rad 0 360 arc
                            stroke
                    0 rad translate
                    newpath
                            0 ecenter xradius yradius 0 -180 msellipse
                            closepath
                            fill
                    grestore"))


#(define-markup-command (airytone layout props radius height thick)
   (number? number? number?)
   (let ((longstring ""))
     (set! height (max (min height 1) 0))
     (if (> height 0.5)
         (set! longstring (airohalf radius (* 2 (- height 0.5)) thick))
         (set! longstring (airuhalf radius (- 1 (* 2 height)) thick)))
     (ly:make-stencil
      (list 'embedded-ps longstring)
      (cons 0 0) (cons 0 2))))


 halfcircle= \markup { \rotate #270  \airytone #1.0 #0.5 #0.1 } 
  


  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  
  
  %Esempio
 {
  \new Staff \with { \omit Clef \omit TimeSignature  }
   {   \stopStaff 
  \cadenzaOn
        s4 \break \bar" "
  	s1-\halfcircle s1-\markup{halfcircle}	\break \bar" "
        s1-\cerchio s1_\markup{cerchio}		\break \bar" "
        s1-\odnovanta s1_\markup{odnovanta}	\break \bar" "
        s1-\odduesette s1_\markup{odduesette}  	\break \bar" "
  	s1-\ocroced s1_\markup{ocroced}		\break \bar" "
  	s1-\dcroceo s1_\markup{dcroceo}		\break \bar" "
	s1-\tri s1_\markup{tri}			\break \bar" "
        s1-\circbow s1_\markup{circbow}
 \cadenzaOff
   }
 
  
  }
  
