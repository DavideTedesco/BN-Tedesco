RSpec.describe Dtml::Math::Linear do
  before :example do
    @good_parmeters = [0,10,10,20]
    @expected_values = [5,15]
    @bad_parameters = ["wrong"]
  end
  
  it "creates itself properly" do
    expect(Dtml::Math::Linear.new(@good_parmeters)).not_to be nil
  end
  it "recovers from errors" do
    expect{Dtml::Math::Linear.new(@bad_parmeters)}.to raise_error(StandardError)
  end
  it "works properly" do
    l=Dtml::Math::Linear.new(@good_parmeters)
    expect(l.y(@expected_values[0])).to eq(@expected_values[1])
  end
end
