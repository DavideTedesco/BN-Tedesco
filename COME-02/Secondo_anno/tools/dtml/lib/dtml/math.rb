module Dtml
  module Math
    require_relative File.join("math","base")
    require_relative File.join("math","linear")
    require_relative File.join("math","expon")
  end
end
