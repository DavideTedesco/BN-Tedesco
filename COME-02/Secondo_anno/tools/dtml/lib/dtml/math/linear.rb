module Dtml
  module Math
    class Linear < Base
      def y(x)
        @afact*x+@bfact
      end
    protected
      def setup()
        raise StandardError,"parametri non sufficienti" unless @parameters.size >= 4
        p=@parameters.map {|x| x.to_f}
        @afact=(p[3]-p[1])/(p[2]-p[0])
        @bfact=p[1]-@afact*p[0]
      end
    end
  end
end
