# Appunti della lezione di martedì 18 maggio 2021

- letto il testo su Haas
- [ascolto del Canto Sospeso con visione della partitura](https://www.youtube.com/watch?v=58YmocJewH0&ab_channel=SWRClassicSWRClassic)
  - canto generato dai timbri strumentali
  - canto che si conclude nelle percussioni (timpano a varie altezze) -> efficacia delle varie altezze del timbro
- [ascolto della sequenza XIVb di Berio](https://www.youtube.com/watch?v=2OjCzNeFuBo)
- descrizione del personaggio Tapping
- introduzione alla metodologia compositiva per mezzo di personaggi
- [il tapping, schemi](https://whimsical.com/tapping-NbyYnNRHYvCuxwtUE3sVVm)
***

Pensare per grande organico e poi portarlo per strumento solo?

Berio -> momenti di effervescenza estrema che si alternano a punti precisi...

Nel Canto sospeso Nono utilizza una struttura seriale integrale per la realizzazione...

Punto di vista fenomenologico delle cose sulla chitarra -> manca forse un aspetto metaforico emotivo

Alternare varie tipologie, di scrittura, cercando di realizzare un aspetto musicale delle cose...

Ottava sinfonia di Bruckner movimento realizzato tutto in pizzicato, salvo alcuni interventi...

Il suono trattenuto e che deve essere tenuto a bada perchè sennò scappa è un qualcosa di emotivo forte.

Figure ritmiche di Berio, sono totalmente diverse da quelle di Ferneyough, con Berio che usa delle figure ritmiche abbastanza regolari, e quando suonano gli strumentisti le figure ritmiche non sembrano regolari per niente.

## Strumento solo

- frenesia
- punteggiatura
- come si fa a rendere con uno strumento solo a renderlo polifonico?

## Serie difettiva

Esse sono pericolose, come difettare una serie difettiva?

***

Mantra -> serie difettive

***

Registrazione di gesti a pezzi e montarli come ci pare, per spostare dei gesti e fare i ragionamenti che servono, registrando 10 frammenti e su quelli ragionare

***
>La musica è un intervento umano e originale che modifica in maniera originale e dice qualcosa...

***

## Cose di cui abbiamo parlato

- [ruby-mext](https://rubygems.org/gems/ruby-mext/versions/0.21.5)
- [Naturale di Berio](https://www.youtube.com/watch?v=DBS7GGWOAtA)
- [Naturale di Berio](https://www.youtube.com/watch?v=MJFJO01kuH8&ab_channel=GiorgioChinniciGiorgioChinnici)
- [note di Berio su Naturale](http://www.lucianoberio.org/naturale-nota-dellautore?1228729248=1)
- [scale difettive e serie difettive](http://www3.unisi.it/ricerca/prog/musica/linguaggio/scala.htm)
- intelligenza artificiale e punto di vista analitico -> correlazione tra nastri e brani di Scelsi -> similitudini?
- curva dell'attenzione ![01curva-attenzione](01curva-attenzione.jpeg)
- [partiture sugli strumenti soli, sax](https://paxxx.github.io/Orchestrazione2021/Lezione03/L03.html)
- stefano petrarca
- Caproni - congedo di un viaggiatore cerimonioso
