require 'yaml'

class Brano
  attr_accessor :key, :title, :author, :fragments, :historical_time
  def initialize(key, title, author, fragments, historical_time)
    @key = key
    @title = title
    @author = author
    @fragments = fragments
    @historical_time = historical_time
  end
end

#brano1 = Brano.new("key", "title", "author", "fragments", "historical_time")

#FUNZIONE DI LETTURA DEL CATALOGO YAML
def leggi(file)
  letto = false
  file = YAML.load(File.read(file))

  if YAML.load_file ARGV[0]
    letto = true
  end
  return letto
end

#FUNZIONE DI DECODIFICA DEI FILE LILYPOND
def decodifica_lilyPond
  decodificato = false
 #https://github.com/igneus/lyv
  leggi("catalogo.yml")
  return decodificato
end

#FUNZIONE DI SEPARAZIONE E IDENTIFICAZIONE DEI FRAMMENTI
def separa_frammenti
  separati = false
  return separati
end

#FUNZIONE DI PESCA DEI FRAMMENTI "ER MACINAPEPE"
def randomizer
  complete = false

  return complete
end

#FUNZIONE DI AGGREGAZIONE DEI FRAMMENTI
def colleziona_frammenti
  composto = nil

  return composto
end

#FUNZIONE DI SCRITTURA
def scrivi
  scritto = false
  return scritto
end

#FUNZIONE DI SALVATAGGIO SU FILE LILYPOND
def salva_lilypond
  salvato = false
#https://github.com/hallelujah/rubypond
  return salvato
end
