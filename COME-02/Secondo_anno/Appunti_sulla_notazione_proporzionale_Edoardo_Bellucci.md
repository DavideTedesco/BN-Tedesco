# Notazione proporzionale consigli e appunti di Edoardo Bellucci

La maggior parte delle volte la notazione proporzionale per strumenti soli.

Le Sequenze di Berio per voce e Sax sono in notazione proporzionale, molti brani per voce sola come: Guaccero e Morricone...

Di scrittura proporzionale vi è molto per strumenti soli.

Necessità di interrogare il cielo di Netti in Notazione proporzionale.

Pezzo di Morricone, Wow, è in notazione proporzionale, è un pezzo di musica vocale contemporanea, è un pezzo in notazione proporzionale, con notazione libera.
Mancano le battute e una misurazione e le figure ritmiche vengono date con forme di accentazioni ritmiche.
Sta all'interprete modellare l'esecuzione in base a come si sta realizzando l'esecuzione.

Le sequenze di Berio, mette delle durate precise in secondi nelle Sequenze, con relazioni con centimetri, metnre il resto viene legato ad esso con una proporzionalità geometricà.

_partiture di Morricone di Wow_

## Perchè usare la notazione proporzionale e non usare la notazione tradizionale?

Non si tratta di un concetto di durata, perchè con la notazione tradizionale le durate si possono scrivere comunque.

Ma con la notazione tradizionale si possono usare tempi frazionari, come il brano di Pasquale Citera, Ascoltare...

Ci si possono inventare le suddivisioni possibili, ma l'interprete scandisce e accenta in base alla suddivisione.

Nella suddivisione di accenti si danno delle intenzioni e delle accentuazioni della scrittura quindi la scrittura tradizionale modella l'intenzione e l'estetica del brano.

La notazione proporzionale invece da un controllo sull'esecuzione che prescinde da termini musicali: come se si dovesse leggere una poesia in versi invece di leggere un rigo solo.

Come se la fine del verso non corrispondesse con l'a capo.

Si interpreta infatti il verso come se fosse una scansione musicale.

Su una poesia per rigo solo la struttura formale musicale decade.

Quindi, gli appigli per un'organizzazione formale del brano rimangono vincolati a gesti d'esecuzione come il respiro, paragonabile alla virgola(già nelle Sequenze di Berio i respiri sono fondamentali come punti di stasi e organizzazione).

## I brani
I brani citati, da chiedere ad Edoardo.

Quartetto per sassofoni, di Edoardo, realizzato in notazione proporzionale per Quartetto, ovvero per piú strumenti.

Alternanza di proporzionale e mensurale da i suoi effetti in termini di flusso sonoro...
