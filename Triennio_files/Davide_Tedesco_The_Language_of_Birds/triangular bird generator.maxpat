{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 51.0, 79.0, 1187.0, 614.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 704.5, 40.000004, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 846.666687, 3.666679, 20.0, 140.0 ],
					"size" : 49.0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 697.333374, 141.333344, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 10,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 836.0, 173.333344, 128.0, 22.0 ],
					"style" : "",
					"text" : "route 1 2 3 4 5 6 7 8 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 594.166687, 109.333336, 68.0, 22.0 ],
					"style" : "",
					"text" : "random 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 399.333344, 145.333344, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 352.666656, 56.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 255.333313, 70.333344, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 287.5, 121.666679, 58.0, 22.0 ],
					"style" : "",
					"text" : "metro $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 890.0, 295.333344, 74.0, 22.0 ],
					"presentation_rect" : [ 715.0, 93.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 22000 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 791.0, 295.333344, 81.0, 22.0 ],
					"presentation_rect" : [ 615.0, 94.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 18000 300"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 689.0, 295.333344, 81.0, 22.0 ],
					"presentation_rect" : [ 515.0, 94.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 12000 200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 609.0, 295.333344, 74.0, 22.0 ],
					"presentation_rect" : [ 430.0, 95.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 9000 120"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 527.0, 295.333344, 67.0, 22.0 ],
					"presentation_rect" : [ 351.0, 94.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 8000 90"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 443.0, 295.333344, 75.0, 22.0 ],
					"presentation_rect" : [ 267.0, 94.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 6000 30"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 357.0, 295.333344, 75.0, 22.0 ],
					"presentation_rect" : [ 181.0, 94.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "1, 5000 60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 1068.0, 285.333344, 76.0, 16.0 ],
					"preset_data" : [ 						{
							"number" : 1,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 8.413598, 0.961538, 0, 7, "obj-44", "function", "add", 18.696884, 0.557692, 0, 7, "obj-44", "function", "add", 24.305948, 0.294872, 0, 7, "obj-44", "function", "add", 33.966007, 0.923077, 0, 7, "obj-44", "function", "add", 43.314449, 0.326923, 0, 7, "obj-44", "function", "add", 50.481586, 0.160256, 0, 7, "obj-44", "function", "add", 62.01133, 0.839744, 0, 7, "obj-44", "function", "add", 67.620399, 0.448718, 0, 7, "obj-44", "function", "add", 81.643059, 0.102564, 0, 7, "obj-44", "function", "add", 87.563736, 0.858974, 0, 7, "obj-44", "function", "add", 97.535408, 0.544872, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5 ]
						}
, 						{
							"number" : 2,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 38.640228, 1.0, 0, 7, "obj-44", "function", "add", 62.322945, 0.108974, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5 ]
						}
, 						{
							"number" : 3,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 12.776213, 1.0, 0, 7, "obj-44", "function", "add", 55.467422, 1.0, 0, 7, "obj-44", "function", "add", 62.63456, 0.0, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5, 5, "obj-18", "toggle", "int", 1, 5, "obj-40", "number", "int", 101, 5, "obj-50", "number", "int", 1, 5, "obj-54", "slider", "float", 21.0, 5, "obj-56", "number", "int", 21 ]
						}
, 						{
							"number" : 4,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 29.915014, 0.942308, 0, 7, "obj-44", "function", "add", 42.067989, 0.717949, 0, 7, "obj-44", "function", "add", 46.74221, 0.961538, 0, 7, "obj-44", "function", "add", 52.662891, 0.788462, 0, 7, "obj-44", "function", "add", 55.467422, 1.0, 0, 7, "obj-44", "function", "add", 60.141644, 0.974359, 0, 7, "obj-44", "function", "add", 64.50425, 0.794872, 0, 7, "obj-44", "function", "add", 69.490082, 0.955128, 0, 7, "obj-44", "function", "add", 77.280457, 0.692308, 0, 7, "obj-44", "function", "add", 84.135979, 0.576923, 0, 7, "obj-44", "function", "add", 84.447594, 0.820513, 0, 7, "obj-44", "function", "add", 91.303116, 0.583333, 0, 7, "obj-44", "function", "add", 93.796036, 0.378205, 0, 7, "obj-44", "function", "add", 95.977341, 0.653846, 0, 7, "obj-44", "function", "add", 101.586403, 0.333333, 0, 7, "obj-44", "function", "add", 102.209633, 0.5, 0, 7, "obj-44", "function", "add", 104.702553, 0.25641, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5 ]
						}
, 						{
							"number" : 5,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 17.138811, 1.0, 0, 7, "obj-44", "function", "add", 40.198299, 0.0, 0, 7, "obj-44", "function", "add", 71.671387, 1.0, 0, 7, "obj-44", "function", "add", 81.643059, 0.0, 0, 7, "obj-44", "function", "add", 91.614731, 1.0, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5 ]
						}
, 						{
							"number" : 6,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 29.395664, 0.82265, 0, 7, "obj-44", "function", "add", 34.796986, 0.224359, 0, 7, "obj-44", "function", "add", 38.120876, 0.455128, 0, 7, "obj-44", "function", "add", 44.353172, 0.976496, 0, 7, "obj-44", "function", "add", 56.402275, 0.190171, 0, 7, "obj-44", "function", "add", 63.881031, 0.514957, 0, 7, "obj-44", "function", "add", 68.035896, 0.326923, 0, 7, "obj-44", "function", "add", 86.73278, 0.16453, 0, 7, "obj-44", "function", "add", 87.148262, 0.634615, 0, 7, "obj-44", "function", "add", 104.598686, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 5, 5, "obj-18", "toggle", "int", 1, 5, "obj-40", "number", "int", 193, 5, "obj-50", "number", "int", 0 ]
						}
 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1068.0, 260.333344, 86.0, 20.0 ],
					"style" : "",
					"text" : "presettons"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 249.5, 578.333313, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 752.0, 416.333344, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1133.0, 346.333344, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 796.0, 628.333313, 29.5, 22.0 ],
					"style" : "",
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 935.0, 642.333313, 36.0, 22.0 ],
					"style" : "",
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.0, 0, 29.395664, 0.82265, 0, 34.796986, 0.224359, 0, 38.120876, 0.455128, 0, 44.353172, 0.976496, 0, 56.402275, 0.190171, 0, 63.881031, 0.514957, 0, 68.035896, 0.326923, 0, 86.73278, 0.16453, 0, 87.148262, 0.634615, 0, 104.598686, 0.0, 0 ],
					"domain" : 110.0,
					"id" : "obj-44",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 853.0, 401.333344, 365.0, 181.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"calccount" : 5,
					"id" : "obj-29",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 124.5, 658.0, 549.0, 138.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 504.0, 609.333313, 40.0, 22.0 ],
					"style" : "",
					"text" : "tri~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 363.0, 545.333313, 47.0, 22.0 ],
					"style" : "",
					"text" : "curve~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1235.666626, 561.666626, 145.5, 145.5 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1033.333374, 730.666626, 37.0, 22.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"order" : 1,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"order" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 1 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 1 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-46", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-46", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-46", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-46", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-46", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-46", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-46", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"order" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 1 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
