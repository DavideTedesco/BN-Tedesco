{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 59.0, 104.0, 1129.0, 616.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 296.0, 100.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "0, 8000 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 703.0, 100.0, 74.0, 22.0 ],
					"presentation_rect" : [ 626.0, 97.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 20000 30"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 617.0, 100.0, 74.0, 22.0 ],
					"presentation_rect" : [ 540.0, 101.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 18000 40"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 892.0, 59.0, 86.0, 20.0 ],
					"style" : "",
					"text" : "presetti"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -0.5, 100.0, 75.0, 22.0 ],
					"presentation_rect" : [ 92.0, 152.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "0, 300 70"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 15,
					"id" : "obj-84",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 892.0, 89.0, 128.0, 24.0 ],
					"preset_data" : [ 						{
							"number" : 1,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 5, "<invalid>", "number", "int", 100, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 53.244682, 0.746667, 0, 7, "obj-44", "function", "add", 67.872337, 0.266667, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
, 						{
							"number" : 2,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 5, "<invalid>", "number", "int", 400, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 15.212766, 1.0, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
, 						{
							"number" : 3,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 5, "<invalid>", "number", "int", 400, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 21.063829, 1.0, 0, 7, "obj-44", "function", "add", 37.446808, 0.64, 0, 7, "obj-44", "function", "add", 85.425529, 0.64, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
, 						{
							"number" : 4,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 5, "<invalid>", "number", "int", 400, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 3.510638, 0.16, 0, 7, "obj-44", "function", "add", 22.819149, 1.0, 0, 7, "obj-44", "function", "add", 34.521278, 0.386667, 0, 7, "obj-44", "function", "add", 47.978722, 0.88, 0, 7, "obj-44", "function", "add", 58.510639, 0.6, 0, 7, "obj-44", "function", "add", 74.30851, 0.333333, 0, 7, "obj-44", "function", "add", 83.670212, 0.866667, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
, 						{
							"number" : 5,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 9.660057, 1.0, 0, 7, "obj-44", "function", "add", 10.283286, 0.5, 0, 7, "obj-44", "function", "add", 15.892351, 0.99359, 0, 7, "obj-44", "function", "add", 19.320114, 0.480769, 0, 7, "obj-44", "function", "add", 22.43626, 1.0, 0, 7, "obj-44", "function", "add", 26.487251, 0.474359, 0, 7, "obj-44", "function", "add", 30.849859, 0.987179, 0, 7, "obj-44", "function", "add", 34.589233, 0.474359, 0, 7, "obj-44", "function", "add", 38.640228, 0.987179, 0, 7, "obj-44", "function", "add", 46.430595, 0.384615, 0, 7, "obj-44", "function", "add", 51.728046, 0.974359, 0, 7, "obj-44", "function", "add", 56.713882, 0.198718, 0, 7, "obj-44", "function", "add", 63.569405, 0.961538, 0, 7, "obj-44", "function", "add", 68.866852, 0.102564, 0, 7, "obj-44", "function", "add", 81.643059, 0.980769, 0, 7, "obj-44", "function", "add", 97.223793, 0.0, 0, 7, "obj-44", "function", "add", 101.898018, 1.0, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
, 						{
							"number" : 6,
							"data" : [ 5, "obj-17", "toggle", "int", 1, 4, "obj-44", "function", "clear", 7, "obj-44", "function", "add", 0.0, 0.0, 0, 7, "obj-44", "function", "add", 15.580736, 0.961538, 0, 7, "obj-44", "function", "add", 30.538244, 0.955128, 0, 7, "obj-44", "function", "add", 38.951843, 0.076923, 0, 7, "obj-44", "function", "add", 49.546741, 0.974359, 0, 7, "obj-44", "function", "add", 64.815865, 0.974359, 0, 7, "obj-44", "function", "add", 73.852692, 0.102564, 0, 7, "obj-44", "function", "add", 84.135979, 0.967949, 0, 7, "obj-44", "function", "add", 99.405098, 0.942308, 0, 7, "obj-44", "function", "add", 110.0, 0.0, 0, 5, "obj-44", "function", "domain", 110.0, 6, "obj-44", "function", "range", 0.0, 1.0, 5, "obj-44", "function", "mode", 0, 5, "obj-75", "number", "int", 1 ]
						}
 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 438.0, 100.0, 73.0, 22.0 ],
					"presentation_rect" : [ 358.0, 97.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "1, 11000 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 369.0, 100.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "1, 9000 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 525.0, 100.0, 82.0, 22.0 ],
					"style" : "",
					"text" : "0, 14000 40"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 229.0, 100.0, 61.0, 22.0 ],
					"style" : "",
					"text" : "1, 7000 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 102.0, 414.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 150.0, 100.0, 74.0, 22.0 ],
					"style" : "",
					"text" : "1, 6000 90"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 576.0, 215.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 957.0, 145.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 620.0, 427.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 759.0, 441.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.0, 0, 53.244682, 0.746667, 0, 67.872337, 0.266667, 0, 110.0, 0.0, 0 ],
					"domain" : 110.0,
					"id" : "obj-44",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 677.0, 200.0, 365.0, 181.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"calccount" : 2,
					"id" : "obj-29",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 122.0, 445.0, 130.0, 130.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 328.0, 408.0, 45.0, 22.0 ],
					"style" : "",
					"text" : "cycle~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 79.0, 100.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "0, 4000 30"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 187.0, 344.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "curve~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 433.0, 475.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 608.0, 528.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"order" : 1,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"order" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 1 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"order" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"order" : 1,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
