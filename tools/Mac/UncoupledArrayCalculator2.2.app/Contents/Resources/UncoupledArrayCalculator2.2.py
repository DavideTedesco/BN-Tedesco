## Uncoupled Array Calculator 2.03 ##
## Daniel Lundberg
## daniel@lundbergsound.com
## 13.06.06
## copyright 2013

##New in 2.02: fixed bug where y coordinate in even-numbered
## arrays was off

##New in 2.03: First implementation of exportToMAPP

##2.1: tidied up, ready for distribution

##2.1.3 - Export to MAPP units fixed

##2.2 - Correct display in windows version, features
## suggested by 6o6 in UI


#######Some initial housekeeping########

import math
import os
from Tkinter import *
import webbrowser
import exportToMAPPv5
import tkFileDialog
from Tkconstants import *
import tkSimpleDialog
import string
import copy
from arrayMathv1 import *


class HyperlinkManager:
    #from Fredrik Lundh
    #http://effbot.org/zone/tkinter-text-hyperlink.htm

    def __init__(self, text):

        self.text = text

        self.text.tag_config("hyper", foreground="blue", underline=1)

        self.text.tag_bind("hyper", "<Enter>", self._enter)
        self.text.tag_bind("hyper", "<Leave>", self._leave)
        self.text.tag_bind("hyper", "<Button-1>", self._click)

        self.reset()

    def reset(self):
        self.links = {}

    def add(self, action):
        # add an action to the manager.  returns tags to use in
        # associated text widget
        tag = "hyper-%d" % len(self.links)
        self.links[tag] = action
        return "hyper", tag

    def _enter(self, event):
        self.text.config(cursor="hand2")

    def _leave(self, event):
        self.text.config(cursor="")

    def _click(self, event):
        for tag in self.text.tag_names(CURRENT):
            if tag[:6] == "hyper-":
                self.links[tag]()
                return



############  Running and Drawing Functions #########################

def updateLoudspeakers(mainFrame):
    """Takes the array values and makes
    a new list of loudspeakers for drawing"""
    loudspeakerQty = mainFrame.data.arrayElements
    #check if the quantity of elements has changed; if it has,
    #we'll make a new list; otherwise, we'll amend the current list
    #(this makes the program much less CPU hungry)
    if mainFrame.data.arrayElementsOld != loudspeakerQty:
        mainFrame.data.loudspeakers = []
    if mainFrame.data.arrayElements % 2 == 1: #odd number of loudspeakers
        rotationOffset = -1 * (loudspeakerQty / 2) * mainFrame.data.arraySplay
        for loudspeaker in xrange(loudspeakerQty):
            splay = loudspeaker * mainFrame.data.arraySplay + rotationOffset
            x = ((loudspeaker - (loudspeakerQty / 2)) *
                mainFrame.data.arraySpacing * math.sin(
                    math.radians(90 - splay / 2.0)))
            y = -abs((loudspeaker - (loudspeakerQty / 2)) *
                  mainFrame.data.arraySpacing * math.cos(
                    math.radians(90 - splay / 2.0)))
            if mainFrame.data.arraySplay < 0:
                y = -y
            if mainFrame.data.arrayElementsOld == loudspeakerQty:
                mainFrame.data.loudspeakers[loudspeaker][0] = x
                mainFrame.data.loudspeakers[loudspeaker][1] = y
                mainFrame.data.loudspeakers[loudspeaker][2] = splay
            else:
                mainFrame.data.loudspeakers.append(
                    [x, y, splay]) 
    else: #even number of loudspeakers
        rotationOffset = (-1 * (loudspeakerQty / 2.0) *
                          mainFrame.data.arraySplay +
                          mainFrame.data.arraySplay / 2.0)
        for loudspeaker in xrange(loudspeakerQty):
            splay = loudspeaker * mainFrame.data.arraySplay + rotationOffset
            x = ((loudspeaker - loudspeakerQty /2.0) *
                mainFrame.data.arraySpacing * math.sin(
                    math.radians(90 - splay / 2.0)
                    ) + mainFrame.data.arraySpacing / 2.0)
            if loudspeaker - loudspeakerQty / 2.0 < 0: #left half
                y = -((loudspeaker - loudspeakerQty / 2 + 0.5) *
                    mainFrame.data.arraySpacing * math.cos(
                        math.radians(90 - splay / 2.0)
                        ))
            else: #right half
                y = -((loudspeaker - loudspeakerQty / 2 + 0.5) *
                    mainFrame.data.arraySpacing * math.cos(
                        math.radians(90 - splay / 2.0)
                        ))
            if mainFrame.data.arraySplay < 0:
                y = -y
            if mainFrame.data.arrayElementsOld == loudspeakerQty:
                mainFrame.data.loudspeakers[loudspeaker][0] = x
                mainFrame.data.loudspeakers[loudspeaker][1] = y
                mainFrame.data.loudspeakers[loudspeaker][2] = splay
            else:
                mainFrame.data.loudspeakers.append(
                    [x, y, splay])
    #to track if the quantity has changed:
    mainFrame.data.arrayElementsOld = loudspeakerQty



def checkEntries(mainFrame):
    """Scans the fields in the GUI, recording their values to array data"""
    sigDigs = 2
    #set all fields as normal
    mainFrame.data.entrySpacing.config(state=NORMAL)
    mainFrame.data.entrySplay.config(state=NORMAL)
    mainFrame.data.entryUnity.config(state=NORMAL)
    mainFrame.data.entryCoverage.config(state=NORMAL)
    mainFrame.data.entryElements.config(state=NORMAL)
    mainFrame.data.entryPlaneLength.config(state=NORMAL)
    
    
    #Spacing
    if mainFrame.data.functionSelectInter.get() != "Spacing":
        #(if spacing is NOT the calculated parameter)
        try:
            mainFrame.data.arraySpacing = float(
                mainFrame.data.entrySpacing.get()
                )
            #(get value from spacing field)
        except:
            mainFrame.data.errorMessage.set(
                "Failed on Spacing; do the numbers you've entered make sense?"
                )
            mainFrame.data.errorsInPass += 1
            #if that wasn't a float, we'll ignore it and alert the user
    else:
        #if spacing IS the calculated parameter
        try:
            mainFrame.data.arraySpacing = mathSpacing(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entrySpacing,
                      round(mainFrame.data.arraySpacing, sigDigs))
            #let's try computing it
        except:
            mainFrame.data.errorMessage.set(
                "Failed on Spacing; do the numbers you've entered make sense?"
                )
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entrySpacing.config(state=DISABLED)
    #Splay
    if mainFrame.data.functionSelectInter.get() != "Splay":
        try:
            mainFrame.data.arraySplay = float(
                mainFrame.data.entrySplay.get()
                )
        except:
            mainFrame.data.errorMessage.set(
                "Failed on Splay; do the numbers you've entered make sense?"
                )
            mainFrame.data.errorsInPass += 1
    else:
        try:
            mainFrame.data.arraySplay = mathSplay(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entrySplay,
                      round(mainFrame.data.arraySplay, sigDigs))
        except:
            mainFrame.data.errorMessage.set(
                "Failed on Splay; do the numbers you've entered make sense?"
                )
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entrySplay.config(state=DISABLED)

    #Coverage
    if mainFrame.data.functionSelectInter.get() != "Loudspeaker Coverage":
        try:
            mainFrame.data.arrayCoverage = float(
                mainFrame.data.entryCoverage.get()
                )
        except:
            mainFrame.data.errorMessage.set("Failed on Coverage")
            mainFrame.data.errorsInPass += 1
    else:
        try:
            mainFrame.data.arrayCoverage = mathCoverage(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entryCoverage,
                      round(mainFrame.data.arrayCoverage, sigDigs))
        except:
            mainFrame.data.errorMessage.set("Failed on Coverage")
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entryCoverage.config(state=DISABLED)
    #Unity Distance
    if mainFrame.data.functionSelectInter.get() != "Unity Distance":
        try:
            mainFrame.data.arrayUnity = float(
                mainFrame.data.entryUnity.get()
                )
        except:
            mainFrame.data.errorMessage.set("Failed on Unity Distance")
            mainFrame.data.errorsInPass += 1
    else:
        try:
            mainFrame.data.arrayUnity = mathUnity(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entryUnity,
                      round(mainFrame.data.arrayUnity, sigDigs))
        except:
            mainFrame.data.errorMessage.set("Failed on Unity Distance")
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entryUnity.config(state=DISABLED)
    #Dmax
    try:
        if (mathDMax(mainFrame) >= 0):
            try:
                mainFrame.data.arrayDMax.set(mathDMax(mainFrame))
            except:
                mainFrame.data.errorMessage.set("Failed on Maximum Distance")
                mainFrame.data.errorsInPass += 1
        else:
            mainFrame.data.arrayDMax.set("N/A")
    except:
        if mainFrame.data.arrayCoverage == 2 * mainFrame.data.arraySplay:
            mainFrame.data.arrayDMax.set("N/A")
        else:
            mainFrame.data.errorMessage.set("Failed on Maximum Distance")
            mainFrame.data.errorsInPass += 1   
    #Plane Length
    if mainFrame.data.functionSelectQty.get() != "Listening Plane Length":
        try:
            mainFrame.data.arrayPlaneLength = float(
                mainFrame.data.entryPlaneLength.get()
                )
        except:
            mainFrame.data.errorMessage.set("Failed on Listening Plane Length")
            mainFrame.data.errorsInPass += 1
    else:
        try:
            mainFrame.data.arrayPlaneLength = mathPlaneLength(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entryPlaneLength,
                      round(mainFrame.data.arrayPlaneLength, sigDigs))
        except:
            mainFrame.data.errorMessage.set("Failed on Listening Plane Length")
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entryPlaneLength.config(state=DISABLED)
    #Quantity
    if mainFrame.data.functionSelectQty.get() != "Number of Elements":
        try:
            mainFrame.data.arrayElements = int(
                mainFrame.data.entryElements.get()
                )
        except:
            mainFrame.data.errorMessage.set("Failed on Quantity")
            mainFrame.data.errorsInPass += 1
    else:
        try:
            mainFrame.data.arrayElements = mathElements(mainFrame)
            fillEntry(mainFrame, mainFrame.data.entryElements,
                      mainFrame.data.arrayElements)
        except:
            mainFrame.data.errorMessage.set("Failed on Quantity")
            mainFrame.data.errorsInPass += 1
        mainFrame.data.entryElements.config(state=DISABLED)

def checkForErrors(mainFrame):
    """Looks for other likely errors in the array and alerts the user"""
    if mainFrame.data.arraySplay > mainFrame.data.arrayCoverage:
        mainFrame.data.errorMessage.set("Splay too large for loudspeaker!")
        mainFrame.data.errorsInPass += 1
    if mainFrame.data.arraySplay < 0:
        mainFrame.data.errorMessage.set("Loudspeakers toed-in!")
        mainFrame.data.errorsInPass += 1
    if mainFrame.data.arraySplay * mainFrame.data.arrayElements >= 180:
        mainFrame.data.errorMessage.set("We're going in a  circle.\
Reduce the length / number of elements")
        mainFrame.data.errorsInPass += 1
    if mainFrame.data.arrayCoverage > 360:
        mainFrame.data.errorMessage.set(
         "Where did you find a loudspeaker whose coverage exceeds 360 degrees?"
            )
        mainFrame.data.errorsInPass += 1
    #if we've made it through a pass with no errors
    #either here or in checkEntires, clear the error message:
    if mainFrame.data.errorsInPass == 0:
        mainFrame.data.errorMessage.set("")

def timerFired(mainFrame):
    mainFrame.data.errorsInPass = 0 #no errors yet in this pass
    #Rescan entry fields
    checkEntries(mainFrame)
    tooManyLoudspeakers = 153
    if mainFrame.data.arrayElements >= tooManyLoudspeakers:
        #that might make it hang, so we're not gonna try
        mainFrame.data.errorMessage.set(
            "That's too many loudspeakers... Sorry."
        )
    else:
        #Update the list of loudspeakers based on inter-loudspeaker parameters
        updateLoudspeakers(mainFrame)
        #If there are errors, report them
        checkForErrors(mainFrame)
        #Redraw loudspeaker graphics
        drawLoudspeakers(mainFrame)
    #Do it again!
    delay = 100 # milliseconds
    mainFrame.after(delay, timerFired, mainFrame)


############################### Initializing ##################################


def initLayoutSizes(mainFrame):
    #sizes of things, syntax is [xSize, ySize, xOffset, yOffset]
    #stuff on the left
    mainFrame.data.sizeLeftFrame = [400, 750, 0, 0]
    mainFrame.data.sizeTitleBlock = [390, 200, 5, 5]
    mainFrame.data.sizeFieldFrame = [390, 390, 5, 205]
    mainFrame.data.sizeExportFrame = [390, 150, 5, 595]
    #stuff on the right
    mainFrame.data.sizeRightFrame = [700, 750]
    mainFrame.data.sizeGraphicsCanvas = [700, 480, 0, 0]
    mainFrame.data.sizeKey = [390, 240, 5, 505]
    mainFrame.data.sizeConsole = [290, 240, 405, 505]

    

def initValues(mainFrame):
    #Initial values for array math
    mainFrame.data.arraySpacing = 5
    mainFrame.data.arraySplay = 9
    mainFrame.data.arrayCoverage = 90
    mainFrame.data.arrayUnity = mathUnity(mainFrame)
    mainFrame.data.arrayDMax = StringVar()
    mainFrame.data.arrayDMax.set(mathDMax(mainFrame))
    mainFrame.data.arrayPlaneLength = 15
    mainFrame.data.arrayElements = mathElements(mainFrame)
    mainFrame.data.arrayElementsOld = 0
    #Initial value for inter-element radio Buttons
    mainFrame.data.functionSelectInter = StringVar()
    mainFrame.data.functionSelectInter.set("Unity Distance")
    #Initial value for quantity radio buttons
    mainFrame.data.functionSelectQty = StringVar()
    mainFrame.data.functionSelectQty.set("Number of Elements")
    #Error message
    mainFrame.data.errorMessage = StringVar()
    mainFrame.data.errorMessage.set("")
    #List of Loudpspeakers
    mainFrame.data.loudspeakers = []
    
def fillEntry(mainFrame, entry, value):
    """Overwrites text in a given GUI field"""
    entry.delete(0, END)
    entry.insert(0, value)

def initFields(mainFrame):
    #Fill those initial values into fields when we begin
    fillEntry(mainFrame, mainFrame.data.entrySpacing,
              mainFrame.data.arraySpacing)
    fillEntry(mainFrame, mainFrame.data.entrySplay,
              mainFrame.data.arraySplay)
    fillEntry(mainFrame, mainFrame.data.entryCoverage,
              mainFrame.data.arrayCoverage)
    fillEntry(mainFrame, mainFrame.data.entryUnity,
              mainFrame.data.arrayUnity)
    fillEntry(mainFrame, mainFrame.data.entryElements,
              mainFrame.data.arrayElements)
    fillEntry(mainFrame, mainFrame.data.entryPlaneLength,
              mainFrame.data.arrayPlaneLength)





################ Drawing Functions ########################

def drawLoudspeakerGetPolyCoords(xCenter, yCenter,
                                 loudspeakerDim, rotation):
    """Computers the coordinates of a rotated square"""
    #front edge of loudspeaker is 0 point
    (x0, y0) = (xCenter + (loudspeakerDim / 2) *
                math.sin(math.radians(rotation)),
                yCenter + (loudspeakerDim / 2) *
                math.cos(math.radians(rotation)))
    #these are each of the four corners after rotation
    (x1, y1) = (x0 + loudspeakerDim / 2 *
                math.cos(math.radians(rotation)),
                y0 - loudspeakerDim / 2 *
                math.sin(math.radians(rotation)))
    (x2, y2) = (x1 - loudspeakerDim *
                math.sin(math.radians(rotation)),
                y1 - loudspeakerDim *
                math.cos(math.radians(rotation)))
    (x3, y3) = (x2 - loudspeakerDim *
                math.cos(math.radians(rotation)),
                y2 + loudspeakerDim *
                math.sin(math.radians(rotation)))
    (x4, y4) = (x3 + loudspeakerDim *
                math.sin(math.radians(rotation)),
                y3 + loudspeakerDim *
                math.cos(math.radians(rotation)))
    return [x0,y0,x1,y1,x2,y2,x3,y3,x4,y4]
    

def drawLoudspeaker(mainFrame, canvas, canvasWidth,
                    x, y, rotation, scale, yOffset):
    """Draws an individual loudspeaker in the graphicsCanvas"""
    #set variables
    loudspeakerDim = .3 * scale
    xCenter = x * scale + canvasWidth / 2
    yCenter = (y + yOffset) * scale
    #draw loudspeaker itself
    polyCoords = drawLoudspeakerGetPolyCoords(xCenter, yCenter,
                                 loudspeakerDim, rotation)
    (xFront, yFront) = (polyCoords[0], polyCoords[1])
    canvas.create_polygon(
        polyCoords[2],polyCoords[3],polyCoords[4],
        polyCoords[5],polyCoords[6],polyCoords[7],
        polyCoords[8],polyCoords[9], fill = "grey",
        outline = "black"
    )
    #draw on-axis line
    lineLength = mainFrame.data.arrayUnity * scale
    (onaxEndX, onaxEndY) = (lineLength *
                           math.sin(rotation * math.pi / 180),
                           lineLength *
                           math.cos(rotation * math.pi / 180))
    canvas.create_line(xFront, yFront, xFront +
                                              onaxEndX,
                                              yFront + onaxEndY)
    #draw coverage lines
    coverage = mainFrame.data.arrayCoverage
    (rightaxEndX, rightaxEndY) = (lineLength *
                           math.sin((rotation - coverage / 2) *
                            math.pi / 180),
                           lineLength *
                           math.cos((rotation - coverage / 2) *
                            math.pi / 180))
    (leftaxEndX, leftaxEndY) = (lineLength *
                           math.sin((rotation + coverage / 2) *
                            math.pi / 180),
                           lineLength *
                           math.cos((rotation + coverage / 2) *
                            math.pi / 180))
    canvas.create_line(xFront, yFront, xFront +
                                              rightaxEndX,
                                              yFront + rightaxEndY,
                                              dash = [5, 5])
    canvas.create_line(xFront, yFront, xFront +
                                              leftaxEndX,
                                              yFront + leftaxEndY,
                                              dash = [5, 5])
    #draw Unity arc
    canvas.create_arc(xFront - lineLength,
                    yFront - lineLength,
                    xFront + lineLength,
                    yFront + lineLength,
                    style = ARC,
                    start = 270 - coverage / 2 +
                    rotation,
                    extent = coverage,
                    outline = "blue")

def drawLoudspeakersScale(mainFrame):
    """Computes an appropriate scale factor for the given array"""
    if mainFrame.data.loudspeakers != []:
        leftBoundary = (mainFrame.data.loudspeakers[0][0] -
                        mainFrame.data.arrayUnity)
        rightBoundary = (mainFrame.data.loudspeakers[-1][0] +
                         mainFrame.data.arrayUnity)
        totalWidth = rightBoundary - leftBoundary
        if totalWidth != 0:
            scale = (mainFrame.data.sizeGraphicsCanvas[0] /
                     totalWidth)
            return scale

def drawLoudspeakerYOffset(mainFrame):
    """Computes yOffset in array units (not pixels)"""
    highestY = mainFrame.data.loudspeakers[0][1]
    someAdditionalMargin = 1
    return someAdditionalMargin - highestY
    

def drawLoudspeakers(mainFrame):
    """Draws all loudspeakers using the mainFrame.data.loudspeakers list"""
    mainFrame.data.graphicsCanvas.delete(ALL)
    scale = drawLoudspeakersScale(mainFrame)
    if type(scale) == float:
        yOffset = drawLoudspeakerYOffset(mainFrame)
        for loudspeaker in mainFrame.data.loudspeakers:
            drawLoudspeaker(mainFrame, mainFrame.data.graphicsCanvas,
                            mainFrame.data.sizeGraphicsCanvas[0],
                            loudspeaker[0],
                            loudspeaker[1], loudspeaker[2],
                            scale, yOffset)

def drawKey(mainFrame):
    """Draws key diagram with two example loudspeakers"""
    size = mainFrame.data.sizeKey
    frame = LabelFrame(mainFrame.data.rightFrame, text = "Key",
                       width = size[0], height = size[1])
    frame.pack()
    frame.place(x = size[2], y = size[3])
    canvasMargin = 10
    canvasWidth = size[0] - 2 * canvasMargin
    canvasHeight = size[1] - 4 * canvasMargin
    canvasCenterX = canvasWidth / 2
    canvasCenterY = canvasHeight / 2
    yOffset = -6
    canvas = Canvas(frame, width = canvasWidth,
                    height = canvasHeight)
    canvas.pack()
    canvas.place(x = canvasMargin, y = canvasMargin)
    #draw loudspeakers
    scale = 25 #scale that goes into drawLoudspeaker function
    keyCoefficient = 0.085 #scales loudspeakers for key
    splay = 25
    drawLoudspeaker(mainFrame, canvas, canvasWidth,
                    -canvasWidth / scale * keyCoefficient,
                    canvasHeight / scale * keyCoefficient + yOffset * keyCoefficient,
                    -splay, scale, canvasWidth / scale * 0.2)
    drawLoudspeaker(mainFrame, canvas, canvasWidth,
                    canvasWidth / scale * keyCoefficient,
                    canvasHeight / scale * keyCoefficient + yOffset * keyCoefficient,
                    splay, scale, canvasWidth / scale * 0.2)
    #Spacing
    linesYOffset = 25 + yOffset * keyCoefficient
    canvas.create_line(canvasCenterX - canvasWidth * keyCoefficient,
                       canvasCenterY - linesYOffset,
                       canvasCenterX + canvasWidth * keyCoefficient,
                       canvasCenterY - linesYOffset,
                       fill = "red")
    canvas.create_text(canvasCenterX, canvasCenterY - linesYOffset * .7,
                       text = "Spacing", fill = "red")
    #Unity Distance
    canvas.create_text(canvasCenterX, (1 - keyCoefficient) * canvasHeight,
                       text = "Unity Distance",
                       fill = "blue")
    #Splay
    canvas.create_line(canvasCenterX - canvasWidth * keyCoefficient,
                       canvasCenterY - linesYOffset,
                       canvasCenterX,
                       canvasCenterY - linesYOffset - canvasWidth *
                       keyCoefficient / math.tan(math.radians(splay)),
                       fill = "dark green")
    canvas.create_line(canvasCenterX + canvasWidth * keyCoefficient,
                       canvasCenterY - linesYOffset,
                       canvasCenterX,
                       canvasCenterY - linesYOffset - canvasWidth *
                       keyCoefficient / math.tan(math.radians(splay)),
                       fill = "dark green")
    canvas.create_text(canvasCenterX + canvasWidth * .1,
                       canvasCenterY - linesYOffset - 1.5 *
                       canvasHeight * keyCoefficient /
                       math.tan(math.radians(splay)),
                       text = "Splay",
                       fill = "dark green")
    #coverage
    canvas.create_text(.18 * canvasWidth, .3 * canvasHeight,
                       text = "Coverage")

def drawGraphics(mainFrame):
    #draw canvas for loudspeakers
    size = mainFrame.data.sizeGraphicsCanvas
    mainFrame.data.graphicsCanvas = Canvas(mainFrame.data.rightFrame,
                                           width = size[0], height = size[1])
    mainFrame.data.graphicsCanvas.pack()
    mainFrame.data.graphicsCanvas.place(x = size[2], y = size[3])
    
def drawTitleBlock(mainFrame):
    """Draws title block in top left corner with links"""
    size = mainFrame.data.sizeTitleBlock
    frame = LabelFrame(mainFrame.data.leftFrame, width = size[0],
                       height = size[1])
    frame.pack()
    frame.place(x = size[2], y = size[3])
    title = Label(frame, text = "Uncoupled Array Calculator Version 2",
                 anchor = CENTER, font = "arial 16")
    title.pack()
    title.place(x = 5, y = 5)
    #description with links
    description = Text(frame, width = 50, height = 9, font = "arial 10")
    hyperlink = HyperlinkManager(description)
    description.pack()
    description.place(x =5, y = 35)
    description.insert(INSERT, "Program for calculating parameters for \
uncoupled \nloudspeaker arrays. For more information see: \
\n \n")
    description.insert(INSERT, "Bob McCarthy's Blog Post on Uncoupled Arrays",
                       hyperlink.add(lambda:
    webbrowser.open("http://bobmccarthy.wordpress.com/2010/03/28/\
uncoupled-array-design-beginnings-and-endings/")))
    description.insert(INSERT, "\n \n")
    description.insert(INSERT, "My blog explaing the math",
                       hyperlink.add(lambda:
    webbrowser.open("http://www.lundbergsound.com/?p=130")))
    description.insert(INSERT, "\n\nCopyright Daniel Lundberg, 2013 \
                       \n daniel@lundbergsound.com")
    description.config(state=DISABLED) #user can't edit text box
    

def drawConsole(mainFrame):
    """Creates error reporter with label linked to mainFrame.data.errorMessage"""
    size = mainFrame.data.sizeConsole
    errorFrame = LabelFrame(mainFrame.data.rightFrame, text = "Error Reporter",
                            width = size[0], height = size[1])
    errorFrame.pack(expand = "yes")
    errorFrame.place(x = size[2], y = size[3])
    labelMargin = 5
    mainFrame.data.errorLabel = Label(errorFrame,
                                      textvariable =
                                      mainFrame.data.errorMessage,
                                      anchor = NW, justify = LEFT,
                                      fg = "red", font = "arial 12",
                                      width = 40, height = 6,
                                      wraplength = size[0] - 10 * labelMargin
                                      )
    mainFrame.data.errorLabel.pack()
    mainFrame.data.errorLabel.place(x = labelMargin, y = labelMargin)

    
def changeMode(mainFrame, variable, label):
    """Allows Radio Buttons to change variables"""
    variable.set(label)
    

def drawField(mainFrame, fieldFrame, kind, label, row, variable=""):
    """Draws an entry field or a label"""
    #set constants
    entryWidth = 10
    rowHeight = 35
    mainFrame.data.fieldFrameRowHeight = rowHeight
    yOffset = 40
    labelRightEdge = 210
    columnGap = 5
    if kind == "field":
        #draw radiobutton
        button = Radiobutton(fieldFrame, text = label + ":",
                             variable = variable,
                             value=label, command = changeMode(
                                mainFrame, variable, label
                                ))
        button.pack()
        button.place(anchor = NE, x = labelRightEdge, y = rowHeight * row)
        #draw entry (field)
        entry = Entry(fieldFrame, width = entryWidth)
        entry.pack()
        entry.place(anchor = NW, x = labelRightEdge + columnGap,
                    y = rowHeight * row)
        return entry
    elif kind == "label":
        label = Label(fieldFrame, anchor = NW, justify = LEFT,
                      text = label)
        label.pack()
        label.place(x = 5, y =rowHeight * row)
    elif kind == "DMax":
        #this is a calculated parameter that the user can't input,
        #so it's done differently
        label = Label(fieldFrame, text = label)
        label.pack()
        label.place(anchor = NE, x = labelRightEdge, y =rowHeight * row)
        dMax = Label(fieldFrame, textvariable = variable)
        dMax.pack()
        dMax.place(anchor = NW, x = labelRightEdge + columnGap,
                   y = rowHeight * row)
    

def drawFields(mainFrame):
    """Draws all fields and labels for the left sidebar"""
    size = mainFrame.data.sizeFieldFrame
    fieldFrame = LabelFrame(mainFrame.data.leftFrame,
                            text ="Array Parameters",
                            width = size[0], height = size[1])
    fieldFrame.pack()
    fieldFrame.place(x = size[2], y = size[3])
    drawField(mainFrame, fieldFrame, "label",
         """Select the inter-element parameter to be calculated with
the radio buttons, then enter other parameters:""", 0)
    mainFrame.data.entrySpacing = drawField(mainFrame, fieldFrame, "field",
                                            "Spacing",
                                            1,
                                            mainFrame.data.functionSelectInter)
    mainFrame.data.entrySplay = drawField(mainFrame, fieldFrame, "field",
                                          "Splay",
                                          2,
                                          mainFrame.data.functionSelectInter)
    mainFrame.data.entryCoverage = drawField(mainFrame, fieldFrame, "field",
                                             "Loudspeaker Coverage",
                                             3,
                                             mainFrame.data.functionSelectInter)
    mainFrame.data.entryUnity = drawField(mainFrame, fieldFrame, "field",
                                          "Unity Distance",
                                          4,
                                          mainFrame.data.functionSelectInter)
    drawField(mainFrame, fieldFrame, "DMax", "Maximum Distance:", 5,
              mainFrame.data.arrayDMax)
    drawField(mainFrame, fieldFrame, "label",
              """Loudspeaker Quantity vs Length of Plane Calculation:""", 6)
    mainFrame.data.entryPlaneLength = drawField(mainFrame, fieldFrame, "field",
                                                "Listening Plane Length",
                                                7,
                                            mainFrame.data.functionSelectQty)
    mainFrame.data.entryElements = drawField(mainFrame, fieldFrame, "field",
                                             "Number of Elements",
                                             8,
                                             mainFrame.data.functionSelectQty)
    note = Label(fieldFrame, justify = LEFT, anchor = NW,
                 text = "*Note that distance units can be anything;\
 they'll just all be the same. \n Coverage and Splay are in degrees.",
                 font = "arial 9 italic")
    note.pack()
    note.place(x = 3, y = mainFrame.data.fieldFrameRowHeight * 9)
    
def drawExportToMAPP(mainFrame):
    """Creates a box with a button in it"""
    #frame
    size = mainFrame.data.sizeExportFrame
    exportFrame = LabelFrame(mainFrame.data.leftFrame, text = "Export to MAPP",
                            width = size[0], height = size[1])
    exportFrame.pack(expand = "yes")
    exportFrame.place(x = size[2], y = size[3])
    
    #button
    button = Button(exportFrame,
                    text = "Export to MAPP",
                    command=lambda: exportToMAPP(mainFrame))
    button.pack()
    button.place(anchor=CENTER, x = size[0] / 2, y = 3 * size[1] / 5)
    
    #text
    text ="Creates an XML file for MAPP Online Pro that can be\nopened by\
 itself or by using\n'File->Import->Import Individual Loudspeakers...'"
    label = Label(exportFrame, anchor = N, justify = CENTER,
                  text = text)
    label.pack()
    label.place(anchor=CENTER, x = size[0] / 2, y = size[1] / 5)

def drawLeftFrame(mainFrame):
    #creates left container
    size = mainFrame.data.sizeLeftFrame
    mainFrame.data.leftFrame = Frame(mainFrame, width = size[0],
                                     height = size[1])
    mainFrame.data.leftFrame.pack(side = LEFT)
    
def drawRightFrame(mainFrame):
    #creates right container
    size = mainFrame.data.sizeRightFrame
    mainFrame.data.rightFrame = Frame(mainFrame, width = size[0],
                                      height = size[1])
    mainFrame.data.rightFrame.pack(side = RIGHT)


################ Export to MAPP ####################################

def exportToMAPP(mainFrame):
    exportToMAPPv5.run(mainFrame)
    
################ Run function #######################################

def run():
    #Create main frame
    root = Tk()
    root.wm_title(string = "Uncoupled Array Calculator 2.2")
    mainFrame = Frame(root)
    mainFrame.pack()
    root.resizable(width=0, height=0)
    #Set up main frame as data structure
    class Struct: pass
    mainFrame.data = Struct()
    #initialize sizes for layout
    initLayoutSizes(mainFrame)
    #initialize math / loudspeaker values
    initValues(mainFrame)
    #Set up entry fields and labels
    drawLeftFrame(mainFrame)
    drawRightFrame(mainFrame)
    drawTitleBlock(mainFrame)
    drawFields(mainFrame)
    drawGraphics(mainFrame)
    drawKey(mainFrame)
    drawConsole(mainFrame)
    drawExportToMAPP(mainFrame)
    mainFrame.data.functionSelectQty.set("Listening Plane Length")
    initFields(mainFrame)
    #Start timer
    timerFired(mainFrame)
    ##test exportToMapp
    root.mainloop() #rock 'n roll!


run()


################ An informal bibliography ######################
# http://effbot.org/tkinterbook/tkinter-index.htm 
# http://www.tutorialspoint.com/python/tk_place.htm
# http://www.python-course.eu/tkinter_layout_management.php