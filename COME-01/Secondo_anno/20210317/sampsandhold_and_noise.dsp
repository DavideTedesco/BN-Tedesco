// NOISE
random  = +(12345)~*(1103515245);
noise   = random/2147483647.0;

// S-&-H
but = button("Hold!");
SH(trig) = (*(1 - trig) + noise * trig) ~ _;
process = SH(but);