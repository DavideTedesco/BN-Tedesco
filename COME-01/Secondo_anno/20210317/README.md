# Appunti della lezione di mercoledì 17 marzo 2021

Il noise di Faust non è ancora pronto in Faust, ma ancora non esiste.

Il noise di ora tira fuori sempre gli stessi numeri.

Per vedere la tabella dei campioni di noise
```
// NOISE
random  = +(12345)~*(1103515245);
noise   = random/2147483647.0;

// S-&-H
but = button("Hold!");
SH(trig) = (*(1 - trig) + noise * trig) ~ _;
process = SH(but);
```

Il noise di Faust è realizzato deterministicamente...

Per avere una funzione in maniera randomica, bisogna realizzare una funzione che getta i dadi.

## Importare una funzione C in Faust

Essa è un'operazione potente perchè libera da molti ostacoli.

Con i ragazzi del triennio si è realizzato un selettore di numeri primi con una funzione esterna in C.

>Di solito le funzioni randomiche come si deve, devono avere un agente stimolante esterno, pescando ad esempio un'informazione dell'istante attuale.

Clock esterno per generare un qualcosa di indeterminato, che fa parte del concetto banale di teoria del caso.

Non si parla di caso, ma generando un segnale diverso ogni volta, per avere ogni volta un segnale diverso.

```
#include <stdio.h>
#include <math.h>

int is_prime(int num);
int next_pr(int num);

int next_pr(int num){
    int c;
    if(num < 2)
        c = 2;
    else if (num == 2)
        c = 3;
    else if(num & 1){
        num += 2;
        c = is_prime(num) ? num : next_pr(num);
    } else
        c = next_pr(num-1);

    return c;
}

int is_prime(int num){
    if((num & 1)==0)
        return num == 2;
    else {
        int i, limit = sqrt(num);
        for (i = 3; i <= limit; i+=2){
            if (num % i == 0)
                return 0;
        }
    }
    return 1;
}
```

Schroeder, Moorer e Dal Torro riverberi, di cui Schroeder utilizza numeri primi.

#### Perchè next prime?
Avendo un riverbero basato su 64 allpass passando un numero primo, si devono avere tutti numeri primi diversi.

[Riverbero](http://www.spinsemi.com/knowledge_base/effects.html#Reverberation)

[Riverbero con Keith Barr](http://blog.reverberate.ca/post/faust-reverb/)

(corso sui riverberi parallelo)
______

Corso legato al repertorio a cui stiamo dando una seconda chance storica.

Dato che Mobile Locale ha visto la luce, ed è partito da corso.

- spiegare le problematiche di Mobile Locale

Lavoro sul repositorio, lavoro attivo da parte di tutti.

Narrazione delle problematiche su mobile locale, e excursus.

Marco Di Gasbarro, esecutore con le percussioni.

- Mobile Locale di Michelangelo Lupone
- Libra di Mauro Cardi
- brani del master di composizione

Importante scrivere articoli su questi brani.

La sperimentazione sul brano di Cardi, ha portato a poterlo controllare di AnteScoFo.

### Libra

Tape che lascia poca libertà esecutiva...

Tape separato in due tronconi, ma non è separato.

Idea del percussionista è capire se il vuoto è automatizzabile.

#### AnteScoFo

è un oggetto di Max che fa analisi di testo in entrata, ed il continuo riferimento in termini di puntatori a quella partiture.

Programma che ha strutturato diverse epoche di analisi tutte simultaneamente.

[Sito di AnteScoFo](https://forum.ircam.fr/projects/detail/antescofo/)

**Trascrizione della partitura per AnteScoFo.**

### Affiancamento alla composizione
Davanti alla macchina intelligente Cardi, vorrebbe scrivere un pezzo con un brano scritto in modalità sostenuta.

Uno dei progetti ambiziosi di quest'anno è affiancare Cardi per la scrittura di brani.

Cardi si dichiara un compositore tradizionale, non esperto di elettronica, e tutto ciò di elettronico è fatto da altri o molto semplice.

#### Tape di Libra realizzato da Di Scipio
Il tape di Libra è realizzato da Di Scipio, con qualche sintetizzatore.

Cercare di ricostruire l'aspetto tecnico.

- Tape con noise che genera tecniche di sintesi -> è stato necessario un restauro

iZotope RX -> selezionare una porzione di spettro e cancellare quella parte di rumore.

### Espansione di SEAM
**Il progetto di SEAM sta divenendo sempre piú imponente**

Coinvolgimento di un'utenza -> attraverso Francesco Galante, come membro del gruppo SIM.

Estremi di far divenire il progetto pagato...

Alcuni personaggi come Bertoncini, Baggiani ed altri sono venuti a mancare.

Lavori di Guido Baggiani eseguite una sola volta...

[Collettivo metadiapason](https://www.metadiapason.eu/)

Guaccero fu uno dei motivi principali perchè la musica romana si sviluppasse elettricamente.

### Guaccero

Anche se non apparentemente Sinfonia I di Guaccero non ha elettronica, Guaccero ne parla...

Il brano preso con Sbordoni è basato sulle istruzioni per la Rai semplificate.

Il supporto che Di Scipio

## Due macro aree

1. valorizzazione del passato
2. assistenza alla composizione per una composizione sostenibile

>Per fare Mobile Locale, Ruggeri ebbe la tendinite.

Problema musicale forte, che non ha fatto sacrificio forte...

Marco ha lavorato sul brano 1 anno e mezzo...

**Realizzazione di un iter con lo stesso tipo di approccio.**

Strutturare un protocollo uguale per tutti, con una sorta di metodo SEAM.

### Probabile incisione

È probabile che si vada in incisione con questo progetto.

Ricerca -> Realizzazione -> Incisione

>Parcheggiare come si fa il brano.

- patrimonio pubblico
- patrimonio musicologico

### Post-praeludium per Donau
- Dialogue de l'hombre double -> interpretazione tridimensionale e documentazione del perchè si fa una cosa del genere -> Articolo di David Wessel
- Post praeludium realizzato con un Euphonium con un'estensione compatibile a quella del tuba -> puntando alla amplificazione trasparente tridimensionale -> il PPPD per come lo vede Nono ha già altoparlanti disposti, in modo da provare se acusticamente funziona

### Discorso su Ambisonics e UHJ

Quando si lavora in 3D si tende a cancellare le basse frequenze.

UHJ di Gerzon non fa perdite a bassa frequenza.

UHJ -> decodificabile in quadrifonia e altri formati ambisonici.

Ennesimo ingrediente non sviluppato.

Diffusione in streaming per un ascolto e riproporre tridimensionalmente.

### Lupone ed il futuro anteriore dell'elettronica

Vi sono 2 o 3 trattamenti che modificano timbricamente il suono e il gesto del percussionista, che dall'elettronica escono elaborati e l'ascoltatore escono delle frustate.

Vi sono alcuni elementi del brano che possono essere analizzati.

Sfibrare il timbro sempre di piú.

### Brani suonati da Marco Di Gasbarro

Mobile Locale a differenza di Libra di Mauro Cardi e The King of Denmark di Feldman.

In questo percorso, Feldman amplificato, Cardi amplificato e tape, Lupone amplificato e elettronica.

Pezzo di Feldman, suonato con i soli polpastrelli, con una sorta di mappa spaziale dell'amplificazione come un soundscape sulla spiaggia.

Brando di Cardi amplificato massicciamente.

Il brano di Lupone era difficilmente contenibile, tendendo a sfondare le orecchie, perchè è un brano realizzato per una sala da concerto.

La resa elettroacustica aumenta il concetto di aumentazione da parte di Lupone che aumenta la parte di elettronica.

Trattamenti elettronici che si passano la mano durante il brano.

Sfibrare la percussione, e la violenza, stracciando gli impulsi e gli attacchi.

Il suono è fermo ed istantaneo con la parte acustica che scompare totalmente e si sente l'elettronica.

**Chitarra classica aumentata che abbia problemi di dinamica, non è una chitarra classica**

>Cono nella buca della cassa per la chitarra
