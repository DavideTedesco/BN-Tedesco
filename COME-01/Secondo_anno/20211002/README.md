# Appunti della lezione di sabato 2 ottobre 2021

## Domanda a Di Scipio

Libro come ipertesto o come eco-sistemico?

Il libro è temporalmente di equilibrio con continuo pallaeggiare avanti ed indietro come i brani ecosistemici di Agostino Di Scipio.

Il testo è un continuo relazionarsi con i mezzi come elementi con un'appartenenza storica.

Compositori che scrivono la storia, sono libri di come dovrebbe essere il compositore.

Coscenza verticale su mezzi e strumenti.

Solo poche persone sono in grado di avere una visione storica e caratterizzare una visione storica e d'appartenenza.

Di Scipio è l'unico compositore che è riuscito a realizzare tale testo.

>La parte finale del libro, ovvero la sound art, con ragionamento sul presente di Di Scipio è l'immaginario di come leggere i suoi schemi degli ecosistemici.

### Cosa si vede in più negli schemi dopo la lettura del libro?

Circuiti del tempo, macchine, etc...

## Riepilogo di ciò che abbiamo fatto fino ad ora

```
import ("stdfaust.lib");

integrator(s) = an.amp_follower(s);
//
localmax(frame) = ba.peakholder(frame);
// max del 1 sec
delay(del,fb) = (+ : de.delay(ma.SR,ba.sec2samp(d)))~*(fb)
with{
    d = min(0, del);
    dmax = d : ba.sec2samp : ma.np2;
};

signal_flow_1a = _,_ <:
  _,_,(integrator(0.01), integrator(0.01) :
  delay(0.01,0.95),delay(0.01,0.95) : + :
  frame <: _,_,_) : _,ro.cross(2),_,_  :
  localmax, localmax,_ : -,_ : localmax // delay 12…
with{
    frame = 6 + (_*(6));
};

process = signal_flow_1a;
```
Brano per 4 microfoni e x altoparlanti.

2 microfoni sono operatori di controllo e 2 sono generatori di suono.

Alcuni microfoni servono solamente come segnali di controllo che sono il signal flow 1a, ed esso ha lo scopo di prendere i segnali e portarli ad essere un segnale di controllo.

>Attitudine alla scalabilità...

Nell'ottica ad ecosistema non vi è alcuna cura della diffusione acustica.

>Nella visione tecnologica di come diffondiamo i suoni dagli altoparlanti, non vi è una prassi compositiva, ma esso è il manifesto importato dalla Sound Art. Si è proceduto verso una concezione musicale e spaziale che magari non ha senso...

Altoparlante come strumento stereofonico...

Ci si è spostati verso l'ambito personal sia dal punto di vista dei computer che dell'ascolto.

Nottoli e gli acusmatici -> colpa del computer...

Se parliamo di campp sonoro e acustico, esso un qualcosa che possiamo navigare.

Uscire con molte informazioni e uscire con un processo reale.

Dare una veste unica allo spazio è creare una morfologia.

_localmax_ è un rilevatore di valore di picco, per stimare il valore di picco di ciascuna frame.

Analizzando i due localmax vediamo che a lui intere...

Questo envelope follower corrisponde a quello che necessita Di Scipio...

```
amp_follower
```

Ci vorrebbe che scenda a un lavoro più profondo per capire come si comporta quell'envelope follower.

Ciò si può fare solamente con una IR dell'envelope follower.

### Integratore

Esso ha un segnale tempo variante e ne ridà un segnale integrato con somme e sottrazioni sul segnale.

Avremmo infati un filtro passa basso.

Come fare questo integratore.

![](.png)
Dirac è l'unico strumento che abbiamo per analizzare l'efficacia dei convertitori analogici-digitali

Abbiamo visto la differenza della risposta all'impulso dell'integratore in double e non!

### Lista blocchi audio di cui realizzare l'IR

>Delay per ogni impostazione del brano e lista dei preset per cui va fatta l'IR, se abbiamo una variante come distanza fisica di altoparlanti, la possiamo simulare con 1 e poi usare i moltiplicatori.

- _integratore_
- forse _localmax_ (dipendentemente da come è realizzato)
- delay con feedback -> nel caso in cui un delay sia frazionario
- lowpass di primo ordine, do

Esercizio per usare l'envelope follower, oscillatore sincronizzato in ampiezza con un envelope follower.

Lentezza con il map che serve per far generre i feedback.

È necessaria una macchina che serve a far generare un feedback.

Abbiamo la frequenza di taglio, ovvero la fc indicata come 0.5 Hz, perchè non coefficente 0.001


Il filtro deve reagire in un tempo specifico, ovvero 2 secondi, ovvero la sua frequenza di aggiornamento.

```
import ("stdfaust.lib");

integrator(s) = an.amp_follower(s);
//
localmax(frame) = ba.peakholder(frame);
// max del 1 sec
delay(del,fb) = (+ : de.delay(ma.SR,ba.sec2samp(d)))~*(fb)
with{
    d = min(0, del);
    dmax = d : ba.sec2samp : ma.np2;
};
twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
cosq(x) = cos(x)*cos(x);
eavg(a) = *(a) : +~*(1-a);
acor(fc) = cos(omega(fc))-1+sqrt(cosq(omega(fc))-4*cos(omega(fc))+3);

signal_flow_1a = _,_ <:
  _,_,(integrator(0.01), integrator(0.01) :
  delay(0.01,0.95),delay(0.01,0.95) : + :
  frame <: _,_,_) : _,ro.cross(2),_,_  :
  localmax, localmax,_ : -,_ : localmax <: delay(12,0),_ : + : *(0.5) : eavg(acor(0.5))
with{
    frame = 6 + (_*(6));
};


process = signal_flow_1a;

//process = os.impulse : integrator(0.001) ;
```
