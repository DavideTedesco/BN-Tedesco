# Appunti della lezione di mercoledì 14 aprile 2021

Brano Markidis

Brano di Silvi con Trasformata di Haar:
Traformata di Haar -> divide il segnale a banda larga, in bin

Studiando Haar si scopre che filtrando

Stesso sistema dell'FFT ma nel dominio del tempo...

Ritardo nel tempo ma di una sola banda

Haar scoperta che si usa sempre troppi campioni...

Trasformata di Haar:
pensare una cellula come compressione di un qualcosa da decomprimere


>Se ho nel dominio del tempo la possibilità di dividere la banda in due parti, la parte dopo Nyquist

Upsampling efficiente...

La tecnica di Haar è una delle tecniche piú semplici, la frattalizzazione di Haar...
_______

## Fondo SEAM
Accesso al server di Giuseppe [SEAM](https://www.sphericaltechnologies.net/seam/)

## Fondo EMUFest
Fondo EMUFest, di Giuseppe Silvi, catalogo musicologico di 10 anni di EMUFest.

- mancano tutti i cd di Piero Schiavoni(materiale mixato e masterizzato) dal 2008 al 2012

________

## Mobile Locale

Avevamo realizzato la versione standalone.

[Lavore in Faust](https://github.com/s-e-a-m/fc1991lmml)

Ora è la revisione di alcuni files.

### Early reflections

Correzione delle early refrections dei vari comb...

Motivazioni per cui non sono indicati i vari feedback, vi sono forse dei comb usati senza retroazione, con solo delay.

```
//------------------------------------------------------------ EARLY REFLECTIONS
er8comb = _ <:
  g1*(0.5*(fi.fb_comb(maxdel,er1,0.5,0.5) + fi.fb_comb(maxdel,er2,0.9,0.9))),
  g2*(0.5*(fi.fb_comb(maxdel,er3,0.87,0.87) + fi.fb_comb(maxdel,er4,0.7,0.7))),
  g3*(0.5*(fi.fb_comb(maxdel,er5,0.55,0.55) + fi.fb_comb(maxdel,er6,0.8,0.8))),
  g4*(0.5*(fi.fb_comb(maxdel,er7,0.6,0.6) + fi.fb_comb(maxdel,er8,0.95,0.95)))
    with{
      maxdel = ma.SR/10 : int;
      er1 = ba.sec2samp(0.087) : int;
      er2 = ba.sec2samp(0.026) : int;
      er3 = ba.sec2samp(0.032) : int;
      er4 = ba.sec2samp(0.053) : int;
      er5 = ba.sec2samp(0.074) : int;
      er6 = ba.sec2samp(0.047) : int;
      er7 = ba.sec2samp(0.059) : int;
      er8 = ba.sec2samp(0.022) : int;
      b0 = .5; // gain applied to delay-line input and forwarded to output
      aN = .5; // minus the gain applied to delay-line output before sum
      g1 = ergroup(vslider("[01] ER 1 [midi:ctrl 83]", 0,0,1,0.01)) : si.smoo;
      g2 = ergroup(vslider("[02] ER 2 [midi:ctrl 84]", 0,0,1,0.01)) : si.smoo;
      g3 = ergroup(vslider("[03] ER 3 [midi:ctrl 85]", 0,0,1,0.01)) : si.smoo;
      g4 = ergroup(vslider("[04] ER 4 [midi:ctrl 86]", 0,0,1,0.01)) : si.smoo;
  };

ermix = +++:*(0.25);
```

Didatticamente, sarebbe utile realizzare i comb in Faust, e nell'implementazione da zero del Comb di Schroeder esce fuori un dato importante,

_Leggere articolo su Schroeder_

Abbiamo il diagramma a blocchi, impulse response e frequency response, che non suggerisce come fare il filtro informaticamente.

1. entra un impulso di dirac
2. esce un campione in ritardo alla posizione successiva
3. tornando indietro il campione viene attenutato
4. viene poi sommato a al campione successivo

Cosí come è scritta da Schroeder non si puó fare nessun programma.


### Vediamo come realizzare il comb

![Schoreder_Reverb](Schoreder_Reverb.png)

Vi è il problema del ritardo, ovvero che il campione che ci troviamo in uscita non è quello che volevamo.


```
import ("stdfaust.lib");

dfl(t,g) = (+ : @(t)) ~*(g);

process = os.impulse : dfl(1, 0.5);
```

L'articolo di Schroeder è riferito alla nascita dei riverberi digitali, attorno al quale si sono poste imperfezioni storiche che continuano a vedersi nel libro di 50 anni di riverberi digitali.

Il riverbero di Schroeder non è il free verb, perchè quello proposto da Schoreder nell'articolo ha una consapevolezza maggiore sull'aspetto timbrico, Schroeder implementa anche la formula di Sabine.

Ambiophonic reverb -> riverbero a tanti canali in grado di interagire con la stanza attraverso il riverbero.

Sabine diviene il papà dell'acustica, perchè deve perfezionare la sala dell'università; prima dell'università con Sabine, non vi erano formule per il riverbero.

Schoreder collega le formule di Sabine a livello astratto e non fa un giochino come Moorer.

Attraverso revisioni di soggetti non si cita mai il feedback delay network da parte di Gerzon.

Filtro passa basso per l'attenuazione alle alte frequenze, con filtro in coda al riverbero.

(Nottoli, porte logiche basate su calcolatori analogici...)

Il comb in Max lavora a unità di misura di ms, ovvero a 48 campioni ogni ms.

Lo strumento con cui si costruiscer determina il livello di precisione.

(Sapere che si puó risalire)

Versione corretta del comb!

Ricordarsi che `mem`=`@(1)`

```
import ("stdfaust.lib");

dflc(t,g) = (+ : @(t-1)) ~*(g): mem; //mem=@(1)

process = os.impulse : dflc(1, 0.5);
```
_________

File di Dattorro sul riverbo Lexicon

File con libro di Del Duca

[Keith Barr]https://valhalladsp.com/2010/08/25/rip-keith-barr/

File sui 50 anni di riverberi artificiali

_________

[Pagina di Dattorro](https://ccrma.stanford.edu/~dattorro/)


__________

**Mettere comb nel codice di Mobile Locale su Faust**
