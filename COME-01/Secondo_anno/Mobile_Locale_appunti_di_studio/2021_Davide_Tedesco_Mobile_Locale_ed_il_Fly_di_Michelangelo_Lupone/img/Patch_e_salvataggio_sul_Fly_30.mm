<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#990000" CREATED="1629838010778" ID="ID_1430382131" MODIFIED="1629838747791" STYLE="fork" TEXT="Patch e salvataggio sul Fly 30">
<edge COLOR="#990000" WIDTH="1"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
<node COLOR="#990000" CREATED="1629838040147" ID="ID_585134009" MODIFIED="1631979897258" POSITION="right" TEXT="5 files associati con estensioni diverse">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838085083" ID="ID_1370021696" MODIFIED="1631979897259" TEXT=".pat">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838214702" ID="ID_501990826" MODIFIED="1631979897257" TEXT="disegno della patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1631953889267" ID="ID_768395451" MODIFIED="1631979897255" TEXT="numeri di riferimento e coordinate grafiche dei moduli">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631954010294" ID="ID_947710251" MODIFIED="1631979897256" TEXT="file in codice ASCII">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631954047696" ID="ID_413802921" MODIFIED="1631979897256" TEXT="file non visibile all&apos;utente">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1629838088057" ID="ID_1727416244" MODIFIED="1631979897260" TEXT=".mlf">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838225247" ID="ID_1780186991" MODIFIED="1631979897251" TEXT="Music Language Fly">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838239180" ID="ID_693371952" MODIFIED="1631979897250" TEXT="metalinguaggio">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838245353" ID="ID_481979716" MODIFIED="1631979897251" TEXT="per un apposito compilatore">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838255416" ID="ID_1523429561" MODIFIED="1631979897252" TEXT="per connettere pi&#xf9; patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631954047696" ID="ID_1510316449" MODIFIED="1631979897252" TEXT="file non visibile all&apos;utente">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838100709" ID="ID_630993968" MODIFIED="1631979897262" TEXT=".asm">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838270793" ID="ID_919663055" MODIFIED="1631979897245" TEXT="traduzione in assembler 320C30 del patch disegnato">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631954327049" ID="ID_496286437" MODIFIED="1631979897245" TEXT="file visibile e modificabile da utenti esperti">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838104588" ID="ID_1666096770" MODIFIED="1631979897264" TEXT=".env">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838285458" ID="ID_337634698" MODIFIED="1631979897237" TEXT="parametri relativi agli inviluppi">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838296409" ID="ID_385530262" MODIFIED="1631979897239" TEXT="16 gruppi di 8x3 valori">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838311482" ID="ID_1607130200" MODIFIED="1631979897235" TEXT="curvatura">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838314496" ID="ID_1066411413" MODIFIED="1631979897235" TEXT="durata">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838316562" ID="ID_1762247575" MODIFIED="1631979897236" TEXT="valore finale">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631954487417" ID="ID_420659320" MODIFIED="1631979897241" TEXT="informazioni create grazie al comando TABLES">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838107587" ID="ID_1076344723" MODIFIED="1631979897266" TEXT=".waw">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838327770" ID="ID_777507617" MODIFIED="1631979897229" TEXT="parametri relativi alle forme d&apos;onda" VSHIFT="14">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838338233" ID="ID_637957217" MODIFIED="1631979897230" TEXT="10x2 valori tra cui:">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838352196" ID="ID_929862758" MODIFIED="1631979897227" TEXT="ampiezza">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838357292" ID="ID_1534584984" MODIFIED="1631979897228" TEXT="fase (in radianti)">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631954627841" HGAP="21" ID="ID_1702520790" MODIFIED="1631979897231" TEXT="i valori sono 10, per ognuna delle 10 armoniche possibili del segnale" VSHIFT="-7">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631954505213" ID="ID_726801270" MODIFIED="1631979897231" TEXT="informazioni create grazie al comando TABLES" VSHIFT="-3">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1629838052722" ID="ID_1493834129" MODIFIED="1631979897222" POSITION="right" TEXT="2 files di output da play">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838115586" ID="ID_905306028" MODIFIED="1631979897222" TEXT="src30.out">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838140092" ID="ID_1146699026" MODIFIED="1631979897220" TEXT="file temporaneo">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838127311" ID="ID_469655274" MODIFIED="1631979897223" TEXT="nomefile.out">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838148721" ID="ID_1109706765" MODIFIED="1631979897217" TEXT="file memorizzato su disco rigido">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838162769" ID="ID_1261921593" MODIFIED="1631979897218" TEXT="caricabile direttamente in memoria tramite il loader">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838180689" ID="ID_1064591864" MODIFIED="1631979897215" TEXT="con esso si pu&#xf2; far partire una patch senza aprire il Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
