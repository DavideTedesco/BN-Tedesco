<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1631956232182" ID="ID_1838933759" MODIFIED="1631956299329" TEXT="Menu FILE">
<node COLOR="#990000" CREATED="1629838888358" ID="ID_1010580011" MODIFIED="1631956300268" POSITION="right" TEXT="FILE">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839165624" ID="ID_1436776002" MODIFIED="1631979795302" TEXT="NEW">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839673426" ID="ID_226316840" MODIFIED="1631979795300" TEXT="azzera il contenuto della memoria">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839167784" ID="ID_669808245" MODIFIED="1631979795304" TEXT="DIR">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839653666" ID="ID_761520019" MODIFIED="1631979795298" TEXT="per la visualizzazione dei file con estensione .pat">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839171025" ID="ID_1330045882" MODIFIED="1631979795306" TEXT="GET">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839626967" ID="ID_1255972023" MODIFIED="1631979795295" TEXT="da usare con mouse e tasto F1">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839634361" ID="ID_1917998446" MODIFIED="1631979795296" TEXT="caricamento di un elemento grafico come modulo">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839253226" ID="ID_1387615037" MODIFIED="1631979795308" TEXT="PUT">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839561670" ID="ID_1252341588" MODIFIED="1631979795292" TEXT="salvataggio di un disegno come modulo">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839575539" ID="ID_391344562" MODIFIED="1631979795292" TEXT="non salvataggio delle forme d&apos;onda, inviluppi e file">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839263737" ID="ID_1632890853" MODIFIED="1631979795310" TEXT="LOAD">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839543095" ID="ID_1060504526" MODIFIED="1631979795288" TEXT="caricamento di patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839552762" ID="ID_1743219021" MODIFIED="1631979795289" TEXT="azzera il contenuto della memoria">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839266103" ID="ID_1029546637" MODIFIED="1631979795312" TEXT="SAVE">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839534209" ID="ID_985308190" MODIFIED="1631979795285" TEXT="salvataggio di patches">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631953722488" ID="ID_1760673278" MODIFIED="1631979795286" TEXT="estensione .pat">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839269506" ID="ID_1359290461" MODIFIED="1631979795314" TEXT="UPDT">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839514766" ID="ID_1543515154" MODIFIED="1631979795282" TEXT="aggiornamento con conservazione del vecchio nome di una patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839273354" ID="ID_265853901" MODIFIED="1631979795315" TEXT="HCPY">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839491045" ID="ID_42920531" MODIFIED="1631979795280" TEXT="copia dell&apos;aria per la stampa del disegno">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839280109" FOLDED="true" ID="ID_1070031241" MODIFIED="1631979795316" TEXT="PLAY">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839363064" ID="ID_1477966378" MODIFIED="1629839697577" TEXT="per l&apos;esecuzione in tempo reale">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839375359" ID="ID_805326033" MODIFIED="1631956300276" TEXT="compiling patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839387249" ID="ID_1188831427" MODIFIED="1629839697574" TEXT="linking patch">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839394494" ID="ID_622617395" MODIFIED="1631956300277" TEXT="inviluppi">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839401766" ID="ID_1376193361" MODIFIED="1629839697572" TEXT="loading envelope w.xx">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839435000" ID="ID_441440251" MODIFIED="1631956300277" TEXT="oscillatori presenti">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839445746" ID="ID_1268407660" MODIFIED="1629839697568" TEXT="loading waveform n.xx">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839466697" ID="ID_863485006" MODIFIED="1631956300278" TEXT="moduli e delays presenti">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839474153" ID="ID_1846310168" MODIFIED="1631956300279" TEXT="clear delay memory">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1631954924722" ID="ID_984302833" MODIFIED="1631979795274" TEXT="azzeramento della memoria">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1631954828068" ID="ID_914638195" MODIFIED="1631979795275" TEXT="genera due file eseguibili">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631954838870" ID="ID_1195915620" MODIFIED="1631979795275" TEXT="uno temporaneo">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631954843663" ID="ID_67498241" MODIFIED="1631979795276" TEXT="Src30.out">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631954852772" ID="ID_1210635727" MODIFIED="1631979795277" TEXT="uno permanente">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631954856545" ID="ID_81517979" MODIFIED="1631979795277" TEXT="nome-file.out">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631954889342" ID="ID_987304396" MODIFIED="1631979795278" TEXT="memorizzato su disco rigido nella directory corrente">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1629839283697" ID="ID_667959161" MODIFIED="1631979795322" TEXT="SHELL">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839311501" ID="ID_1902428740" MODIFIED="1631979795267" TEXT="entrare in DOS senza uscire dal programma">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839351003" ID="ID_1694096209" MODIFIED="1631979795265" TEXT="si esce con EXIT">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1629839288204" ID="ID_1842448988" MODIFIED="1631979795324" TEXT="QUIT">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839298546" ID="ID_1513658316" MODIFIED="1631979795261" TEXT="uscita dal Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1631955253632" ID="ID_302170827" MODIFIED="1631979795260" TEXT="chiede se si desidera il reset della memoria del processore numerico">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1629838891497" ID="ID_182012142" MODIFIED="1631955509626" POSITION="right" TEXT="EDIT">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1629838895973" ID="ID_1308026026" MODIFIED="1631955513602" POSITION="right" TEXT="MODL">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1629838907515" ID="ID_1062675799" MODIFIED="1629839697551" POSITION="right" TEXT="linea di stato">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1629838911981" ID="ID_1575800728" MODIFIED="1631956312537" POSITION="right" TEXT="HELP">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629838938224" ID="ID_1755314256" MODIFIED="1631979795230" TEXT="general">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839025043" ID="ID_1099079205" MODIFIED="1631979795229" TEXT="funzionamento del Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838943830" ID="ID_1545768347" MODIFIED="1631979795231" TEXT="keywords">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839040758" ID="ID_734399575" MODIFIED="1631979795226" TEXT="parole chiave">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838950879" ID="ID_1694295909" MODIFIED="1631979795232" TEXT="keyboard">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839050284" ID="ID_1035057974" MODIFIED="1631979795224" TEXT="comandi relativi ai tasti">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838956336" ID="ID_31489111" MODIFIED="1631979795233" TEXT="formulas">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839062506" ID="ID_907046584" MODIFIED="1631979795222" TEXT="formule presenti nell&apos;ambiente">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629838963074" ID="ID_272775387" MODIFIED="1631979795235" TEXT="modules">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839073866" ID="ID_144928134" MODIFIED="1631979795215" TEXT="+">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839078957" ID="ID_146851548" MODIFIED="1631979795216" TEXT="-">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839081568" ID="ID_713818244" MODIFIED="1631979795217" TEXT="*">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839085196" ID="ID_1307074126" MODIFIED="1631979795218" TEXT="+ per la fase in modulo 65536">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839103349" ID="ID_639133873" MODIFIED="1631979795219" TEXT="TAB">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839109908" ID="ID_378109992" MODIFIED="1631979795210" TEXT="lettura della forma d&apos;onda dell&apos;indirizzo A">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839144242" ID="ID_1216509388" MODIFIED="1631979795220" TEXT="MEM">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
