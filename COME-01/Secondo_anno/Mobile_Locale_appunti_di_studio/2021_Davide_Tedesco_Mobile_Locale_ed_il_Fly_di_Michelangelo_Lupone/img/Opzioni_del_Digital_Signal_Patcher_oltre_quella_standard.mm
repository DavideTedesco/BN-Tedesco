<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#00b439" CREATED="1629837483227" ID="ID_186261144" MODIFIED="1631976602108" TEXT="Opzioni del Digital Signal Patcher oltre quella standard">
<edge COLOR="#00b439" WIDTH="2"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#990000" CREATED="1629837568774" ID="ID_81229553" MODIFIED="1631976608556" POSITION="right" TEXT="/E">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629837670980" ID="ID_1167637311" MODIFIED="1631979862601" TEXT="senza caricare gli inviluppi">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="8"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629837633731" HGAP="19" ID="ID_1029911098" MODIFIED="1631976608553" POSITION="right" TEXT="/W">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629837693981" ID="ID_1698778530" MODIFIED="1631979862599" TEXT="senza caricare le forme d&apos;onda">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="8"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629837637723" ID="ID_1847874718" MODIFIED="1631976608550" POSITION="right" TEXT="/I">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629837706357" ID="ID_1165546026" MODIFIED="1631979862598" TEXT="esecuzione senza calcolo interpolato">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="8"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629837645004" ID="ID_1493747057" MODIFIED="1631976608546" POSITION="right" TEXT="/O">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629837749837" ID="ID_878269608" MODIFIED="1631979862595" TEXT="rieseguire l&apos;ultimo file .out con compilazione veloce">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="8"/>
<node COLOR="#111111" CREATED="1629837767485" ID="ID_1606355104" MODIFIED="1631979862594" TEXT="N.B. la patch deve essere gi&#xe0; stata compilata">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="8"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1629837645004" ID="ID_1350667575" MODIFIED="1631976608542" POSITION="left" TEXT="/N">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1631974986556" ID="ID_204572603" MODIFIED="1631976608538" POSITION="left" TEXT="/F">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631974990659" ID="ID_1900584542" MODIFIED="1631979862591" TEXT="consente di assegnare un valore di frequenza espresso in Hertz agli ingressi dei moduli oscillatori">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976619054" ID="ID_800598499" MODIFIED="1631979862592" TEXT="consente di assegnare un valore di ampiezza espresso in dB agli ingressi dei moduli oscillatori ">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631978348345" ID="ID_413306841" MODIFIED="1631979862587" POSITION="left" TEXT="/T">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631978353214" ID="ID_656201388" MODIFIED="1631979862588" TEXT="modalit&#xe0; per esprimere i valori in millisecondi anzich&#xe8; in numero di celle">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</map>
