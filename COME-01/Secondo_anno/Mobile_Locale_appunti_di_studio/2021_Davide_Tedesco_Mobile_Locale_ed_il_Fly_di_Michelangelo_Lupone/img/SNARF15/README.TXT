README.TXT     25-May-2007

Snarf is a "screen shooter" for DOS. It saves whatever is on the screen
to a file. It's particularly useful under Windows XP because XP will not
capture a DOS graphic screen using the Print Screen key.

When run, Snarf is installed into memory as a Terminate-and-Stay-
Resident (TSR) program. Pressing Alt+S captures what's on the screen. A
beep indicates the image is being saved to disk. A second beep indicates
that the capture is complete. The image is saved in the current directory
(or folder) in a file called SNARF000.BMP. Additional images are saved in
SNARF001.BMP, SNARF002.BMP ... SNARF999.BMP. Both text and graphics can
be saved, but the saved image is always in a graphic file format (i.e.
text is converted to graphics). Most images are saved in a second or two,
but some images can take up to 30 seconds to save.

Snarf works for all text and graphic modes: CGA, EGA, VGA, and VESA (up
through hex 11B).

If you hear three beeps this indicates an error, such as an unsupported
display mode. An error message will appear that provides a clue to the
problem (see below).

An 80386 CPU (or better) is required. Today's Pentiums and Athlons are
easily up to the task.

Because Alt+S might be used by the program you want to take a snapshot
of, other hot keys can be specified. When Snarf is first run, if a number
is entered on the command line, it will change the scan code used for the
hot key. For example, to change the hot key to Alt+C type: "snarf 46".
(The value is in decimal, not the traditional hex.) A list of scan codes
is provided in SCANCODE.TXT. After Snarf has been installed, the hot key
cannot be changed by attempting to reinstall Snarf.


KNOWN PROBLEMS

Under Windows XP, Snarf prevents the use of long file names. This is a
problem with any TSR running under the Windows NT family. The problem is
related to the fact that 32-bit CMD is switched to 16-bit Command without
any warning or indication.

A related problem is that DOSKEY quits working after Snarf (or any TSR)
is installed. (The up-arrow key will no longer select the previous
commands.) Reinstalling DOSKEY does not solve the problem, however
installing a similar program, called CED, does solve it.

Some game programs take complete control over the keyboard, which
prevents Snarf from receiving a keystroke and capturing the image.
(Modifying the code to use a timer, or some other kludge, might get
around the problem. Some games use mode-X, which is not supported.)

VESA version 2.0 allowed manufacturers to define their own modes above
hex 11B. These are currently not supported.


ERROR MESSAGES

If Snarf detects an error, it blanks the screen and displays a message
with a pair of error codes. Although your original screen image is gone,
your original program is still running. After writing down the error
message, type a command to try to refresh the screen. If you were at a
DOS prompt, a new prompt will appear when you press the Enter key.

Error: 1 mmm indicates an unsupported display mode. The mode is the
second number (mmm). Snarf supports 44 display modes.

Error: 2 indicates an attempt to save more than 999 image files in the
current directory.

Error: 3 xxx indicates that the output file could not be written to disk.
The DOS error code is the second number (xxx), which provides the reason.
Common DOS error codes are 3: diskette not inserted into the drive; and
5: write-protected diskette. Another possible error is that the root
directory is full.

Error: 4 indicates insufficient disk space to save the image file.


USE AT YOUR OWN RISK

Snarf has been verified on many different computers running DOS 5.0,
Windows 3.1, Windows 98, and Windows XP; however it's a complex program
that probably won't work on all PCs for all programs. If you discover a
problem, I'd appreciate an email.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program (in the file LICENSE.TXT); if not, write to the Free
Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


Thanks,
Loren

loren_blaney@idcomm.com
