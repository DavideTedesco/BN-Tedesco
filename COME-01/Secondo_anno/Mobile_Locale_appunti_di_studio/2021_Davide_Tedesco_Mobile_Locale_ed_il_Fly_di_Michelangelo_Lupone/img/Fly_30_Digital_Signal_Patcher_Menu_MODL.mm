<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#00b439" CREATED="1631959393652" ID="ID_222015608" MODIFIED="1631979716110" TEXT="Menu MODL">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1631959399803" ID="ID_413000299" MODIFIED="1631979716111" POSITION="right" TEXT="ADD">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975289600" ID="ID_1499327268" MODIFIED="1631979716112" TEXT="modulo aritmetico per l&apos;addizione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631975121215" ID="ID_410138984" MODIFIED="1631979716113" POSITION="right" TEXT="SUB">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975289600" ID="ID_704373670" MODIFIED="1631979716114" TEXT="modulo aritmetico per la sottrazione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631975123887" ID="ID_1847714267" MODIFIED="1631979716115" POSITION="right" TEXT="MUL">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975289600" ID="ID_651359232" MODIFIED="1631979716116" TEXT="modulo aritmetico per la moltiplicazione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631975130103" ID="ID_915650759" MODIFIED="1631979716117" POSITION="right" TEXT="PHS">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975357270" ID="ID_1082090738" MODIFIED="1631979716118" TEXT="&#xe8; un generatore di rampa, ovvero un oscillatore che produce in uscita un&apos;onda dente di sega">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631975389081" ID="ID_756310120" MODIFIED="1631979716119" TEXT="in ingresso si pone lo step incrementale desiderato della rampa">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631975438968" ID="ID_1316197796" MODIFIED="1631979716119" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631975476989" ID="ID_1597607853" MODIFIED="1631979716093" TEXT="valore iniziale della rampa">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631975600977" ID="ID_252809710" MODIFIED="1631979716121" TEXT="usato come">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631975604185" ID="ID_1220645770" MODIFIED="1631979716095" TEXT="generatore d&apos;onda a dente di sega">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631975623250" ID="ID_1819814865" MODIFIED="1631979716096" TEXT="modulo per l&apos;attribuzione di indirizzi per i moduli MEM e TAB per quanto riguarda la fase">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975143867" ID="ID_594690424" MODIFIED="1631979716122" POSITION="right" TEXT="TAB">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975700871" ID="ID_948945211" MODIFIED="1631979716123" TEXT="utilizzato per la generazione di forme d&apos;onda tramite tabella pre-editata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631975438968" ID="ID_1820860041" MODIFIED="1631979716124" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631975752865" ID="ID_1762608758" MODIFIED="1631979716077" TEXT="numero identificatore della tabella editata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975152292" ID="ID_1760128480" MODIFIED="1631979716126" POSITION="right" TEXT="MEM">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631975877413" ID="ID_1662495065" MODIFIED="1631979716126" TEXT="consente di memorizzare in una tabella di 4096 punti la forma d&apos;onda di un segnale associato al primo dei suoi due ingressi">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631975901022" ID="ID_633811866" MODIFIED="1631979716127" TEXT="la forma d&apos;onda va editata tramite l&apos;editor">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976126748" ID="ID_455461455" MODIFIED="1631979716128" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976129404" ID="ID_1254507056" MODIFIED="1631979716065" TEXT="numero identificatore della tabella editata">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976142680" ID="ID_1775346839" MODIFIED="1631979716059" TEXT="da 1 a 16">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1631976187361" ID="ID_705535217" MODIFIED="1631979716131" TEXT="non dispone di un&apos;uscita da collegare direttamente ad un altro modulo">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631975165120" ID="ID_392215569" MODIFIED="1631979716132" POSITION="right" TEXT="OSC">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631976305142" ID="ID_664400353" MODIFIED="1631979716134" TEXT="oscillatore">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976315206" ID="ID_1491885175" MODIFIED="1631979716041" TEXT="generatore di segnali periodici">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976339335" ID="ID_579825829" MODIFIED="1631979716136" TEXT="ampiezza e step di lettura scelti dall&apos;utente">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976374002" ID="ID_1799917323" MODIFIED="1631979716044" TEXT="valori costanti o variabili da porre ai due ingressi">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976399460" ID="ID_1081449675" MODIFIED="1631979716138" TEXT="utilizza tabelle di 4096 punti editabili tramite comando TABLES con un numero compreso tra 1 e 16">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976677609" ID="ID_595613734" MODIFIED="1631979716143" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976697915" ID="ID_726096750" MODIFIED="1631979716048" TEXT="indica il numero di tabella in cui sar&#xe0; editata la forma d&apos;onda da generare">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975171524" ID="ID_312703416" MODIFIED="1631979716145" POSITION="right" TEXT="ENV">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631976766388" ID="ID_1870192280" MODIFIED="1631979716146" TEXT="per la generazione di tabelle contenenti funzioni di inviluppo">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976677609" ID="ID_1129039302" MODIFIED="1631979716147" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976697915" ID="ID_376279289" MODIFIED="1631979716016" TEXT="indica il numero di tabella da editare desiderata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976842001" ID="ID_750134728" MODIFIED="1631979716148" TEXT="ha 2 ingressi e un&apos;uscita">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976858439" ID="ID_1627920239" MODIFIED="1631979716018" TEXT="in ingresso">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976863555" ID="ID_146111122" MODIFIED="1631979716000" TEXT="un fattore moltiplicativo di velocit&#xe0; (A) per l&apos;esecuzione dell&apos;inviluppo">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_202025138" MODIFIED="1631979715985" TEXT="=1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_1133865297" MODIFIED="1631979715978" TEXT="velocit&#xe0; di esecuzione inalterata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_815883553" MODIFIED="1631979715986" TEXT="&gt;1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_1118149657" MODIFIED="1631979715980" TEXT="velocit&#xe0; di esecuzione accellerata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_1770234035" MODIFIED="1631979715988" TEXT="&lt;1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_1777554482" MODIFIED="1631979715982" TEXT="velocit&#xe0; di esecuzione rallentata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1631976874382" ID="ID_1367789175" MODIFIED="1631979716004" TEXT="un fattore moltiplicativo per l&apos;ampiezza della funzione stessa">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_1403224040" MODIFIED="1631979715990" TEXT="=1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_1252381474" MODIFIED="1631979715959" TEXT="ampiezza inalterata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_311027117" MODIFIED="1631979715993" TEXT="&gt;1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_1874058454" MODIFIED="1631979715963" TEXT="ampiezza incrementata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976926475" ID="ID_678813805" MODIFIED="1631979715996" TEXT="&lt;1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976935359" ID="ID_624878251" MODIFIED="1631979715965" TEXT="ampiezza diminuita">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975177614" ID="ID_1270504836" MODIFIED="1631979716157" POSITION="right" TEXT="DLY">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631978049176" ID="ID_85282452" MODIFIED="1631979716158" TEXT="modulo per la realizzazione di una linea di ritardo composta da una sola cella di memoria">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976677609" ID="ID_1953451193" MODIFIED="1631979716159" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976697915" ID="ID_742581222" MODIFIED="1631979715940" TEXT="indica il valore da attribuire in partenza alla cella di memoria">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631978130683" ID="ID_1813260016" MODIFIED="1631979716161" TEXT="utilizzabile in vari modi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978135337" ID="ID_847296731" MODIFIED="1631979715942" TEXT="come segnale di controllo">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978146966" ID="ID_198462040" MODIFIED="1631979715943" TEXT="per filtrare">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978152078" ID="ID_1800221387" MODIFIED="1631979715944" TEXT="per riverberare segnali analogici">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978166181" ID="ID_1361261580" MODIFIED="1631979715945" TEXT="etc...">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975181408" ID="ID_254362259" MODIFIED="1631979716163" POSITION="left" TEXT="DLN">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631978130683" ID="ID_732956685" MODIFIED="1631979716164" TEXT="utilizzabile in vari modi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978135337" ID="ID_23649312" MODIFIED="1631979715914" TEXT="come segnale di controllo">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978146966" ID="ID_1967612298" MODIFIED="1631979715915" TEXT="per filtrare">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978152078" ID="ID_1544368310" MODIFIED="1631979715916" TEXT="per riverberare segnali analogici">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978166181" ID="ID_884858428" MODIFIED="1631979715917" TEXT="etc...">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631978049176" ID="ID_1717954481" MODIFIED="1631979716167" TEXT="modulo per la realizzazione di una linea di ritardo composta da 1 fino a 2^n celle di memoria">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976677609" ID="ID_290939095" MODIFIED="1631979716168" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631976697915" ID="ID_1666068071" MODIFIED="1631979715919" TEXT="indica il valore di n, ovvero numero di celle di memoria che si vogliono utilizzare">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631978424796" ID="ID_1978363186" MODIFIED="1631979716170" TEXT="utilizzato per la realizzazione di echi">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631978488453" ID="ID_1915573467" MODIFIED="1631979716170" TEXT="non si possono impiegare pi&#xfa; di 90112 celle di memoria per il ritardo, per via dei limiti hardware della RAM">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631975187321" ID="ID_1722694984" MODIFIED="1631979716172" POSITION="left" TEXT="DLS">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631978130683" ID="ID_1234974014" MODIFIED="1631979716172" TEXT="utilizzabile in vari modi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978135337" ID="ID_270678872" MODIFIED="1631979715877" TEXT="come segnale di controllo">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978146966" ID="ID_1447081324" MODIFIED="1631979715878" TEXT="per filtrare">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978152078" ID="ID_1111720245" MODIFIED="1631979715879" TEXT="per riverberare segnali analogici">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978166181" ID="ID_1138633356" MODIFIED="1631979715880" TEXT="etc...">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631978543336" ID="ID_346012321" MODIFIED="1631979716175" TEXT="delay multiplo con incremento">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978558798" ID="ID_1090067205" MODIFIED="1631979716185" TEXT="oltre a presentare funzioni analoghe al modulo DLN, presenta un secondo ingresso (B), con un valore che rappresenta lo step incrementale per la lettura delle celle impiegate">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978637816" ID="ID_1139624256" MODIFIED="1631979715889" TEXT="se B &#xe8; pari ad 1 avremo un&apos;uscita uguale all&apos;entrata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978708350" ID="ID_1028176185" MODIFIED="1631979715890" TEXT="con valore pari a 2 verr&#xe0; letto un campione ogni 2 del sengale originario">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975193833" ID="ID_1505030461" MODIFIED="1631979716190" POSITION="left" TEXT="FLT">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631978735570" ID="ID_1738291685" MODIFIED="1631979716191" TEXT="modulo per la realizzazione di filtri IIR di secondo ordine">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978755723" ID="ID_1486447079" MODIFIED="1631979716192" TEXT="possiede 3 ingressi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978766507" ID="ID_1427150330" MODIFIED="1631979715850" TEXT="A: un segnale">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978777515" ID="ID_114892401" MODIFIED="1631979715851" TEXT="B e C: i parametri che rappresentano i coefficienti">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631978855659" ID="ID_768329701" MODIFIED="1631979715843" TEXT="B deve essere compreso tra -2 e +2">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978866993" ID="ID_1925651455" MODIFIED="1631979715844" TEXT="C deve essere compreso tra -1 e 0">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631978896762" ID="ID_379436052" MODIFIED="1631979715844" TEXT="B deve essere minore di 2&#x221a;(-C)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975199865" ID="ID_1112267878" MODIFIED="1631979716195" POSITION="left" TEXT="RND">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631979027314" ID="ID_24365857" MODIFIED="1631979716196" TEXT="modulo per la generazione di segnale random">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976126748" ID="ID_351273491" MODIFIED="1631979716196" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979076897" ID="ID_87459406" MODIFIED="1631979715825" TEXT="numero in floating point tanto pi&#xfa; complesso tanto ci si avviciner&#xe0; al rumore bianco">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975202824" ID="ID_892038038" MODIFIED="1631979716198" POSITION="left" TEXT="VAL">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631979128083" ID="ID_700984510" MODIFIED="1631979716199" TEXT="modulo per l&apos;assegnazione di un valore costante">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979137575" ID="ID_1547551282" MODIFIED="1631979715816" TEXT="tale valore &#xe8; modificabile tramite comando MODIFY">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975210802" ID="ID_1091944099" MODIFIED="1631979716200" POSITION="left" TEXT="INP">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631979189404" ID="ID_1505309787" MODIFIED="1631979716201" TEXT="modulo per l&apos;acquisizione digitale di segnali analogici provenienti dall&apos;esterno del Digital Signal Patcher collegato all&apos;ADC">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976126748" ID="ID_102089381" MODIFIED="1631979716202" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979273030" ID="ID_373712134" MODIFIED="1631979715807" TEXT="valore indicante il numero del canale">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979293429" ID="ID_1140888946" MODIFIED="1631979715808" TEXT="1 o 2 se si utilizza una sola scheda di conversione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979309312" ID="ID_1041074113" MODIFIED="1631979715809" TEXT="da 1 a 4 con due schede">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975213501" ID="ID_1844264147" MODIFIED="1631979716204" POSITION="left" TEXT="OUT">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631979328096" ID="ID_487130401" MODIFIED="1631979716205" TEXT="modulo di uscita del segnale processato dal Digital Signal Patcher collegato al DAC">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979369388" ID="ID_624478734" MODIFIED="1631979716207" TEXT="modulo terminale di qualsiasi patch DSP">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979387191" ID="ID_73654901" MODIFIED="1631979716208" TEXT="ha solo connessione di ingresso">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631976126748" ID="ID_1361544796" MODIFIED="1631979716209" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979273030" ID="ID_411094991" MODIFIED="1631979715791" TEXT="valore indicante il numero del canale">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979293429" ID="ID_299337491" MODIFIED="1631979715776" TEXT="1 o 2 se si utilizza una sola scheda di conversione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979309312" ID="ID_149977086" MODIFIED="1631979715777" TEXT="da 1 a 4 con due schede">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631975216592" ID="ID_1748094635" MODIFIED="1631979716211" POSITION="left" TEXT="IFP">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631979425825" ID="ID_1233258453" MODIFIED="1631979716212" TEXT="selettore logico che a secondo del valore dei tre ingressi pone una diversa uscita">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979457186" ID="ID_1384495059" MODIFIED="1631979716212" TEXT="i tre ingressi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979461806" ID="ID_955507996" MODIFIED="1631979715762" TEXT="A: determina quale fra gli ingressi B e C andr&#xe0; in uscita">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631979481157" ID="ID_724250055" MODIFIED="1631979715763" TEXT="B e C: sono i due ingressi selezionabili">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631976126748" ID="ID_1711326889" MODIFIED="1631979716214" TEXT="necessita di un valore associato da fornire tramite il comando VALUE del menu EDIT ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631979549400" ID="ID_603169430" MODIFIED="1631979715765" TEXT="tale valore &#xe8; il valore di soglia">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</map>
