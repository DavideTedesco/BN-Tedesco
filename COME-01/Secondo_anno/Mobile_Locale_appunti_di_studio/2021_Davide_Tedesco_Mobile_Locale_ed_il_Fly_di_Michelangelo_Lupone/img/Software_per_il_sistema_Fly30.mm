<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#00b439" CREATED="1626011981264" ID="ID_211750711" MODIFIED="1631976017906" TEXT="Software per il sistema Fly30">
<edge COLOR="#00b439" WIDTH="2"/>
<font BOLD="true" NAME="Garamond" SIZE="16"/>
<node COLOR="#990000" CREATED="1626012018263" ID="ID_1004256324" MODIFIED="1631976017907" POSITION="left" TEXT="Polar Synthesis">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
</node>
<node COLOR="#990000" CREATED="1626012034404" ID="ID_1102350107" MODIFIED="1631976017909" POSITION="left" TEXT="Dsp Digital Signal Patcher">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
</node>
<node COLOR="#990000" CREATED="1626012046286" ID="ID_1474360519" MODIFIED="1631976017911" POSITION="left" TEXT="Linea">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
</node>
<node COLOR="#990000" CREATED="1626012071962" ID="ID_285665182" MODIFIED="1631976017912" POSITION="right" TEXT="Wave Table Converter">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
<node COLOR="#111111" CREATED="1626012225432" ID="ID_780986639" MODIFIED="1631979918782" TEXT="Per la conversione di forme d&apos;onda da tabelle ASCII a campioni floating point  ">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="16"/>
</node>
</node>
<node COLOR="#990000" CREATED="1626012082666" ID="ID_842699864" MODIFIED="1631976017914" POSITION="right" TEXT="Loadwave">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
<node COLOR="#111111" CREATED="1626012212312" ID="ID_979943287" MODIFIED="1631979918780" TEXT="Per il caricamento e l&apos;editing di forme d&apos;onda  ">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="16"/>
</node>
</node>
<node COLOR="#990000" CREATED="1626012087706" ID="ID_1508879714" MODIFIED="1631976017916" POSITION="right" TEXT="Loadenvelope">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
<node COLOR="#111111" CREATED="1626012245848" ID="ID_131288133" MODIFIED="1631979918778" TEXT="Per il caricamento e l&apos;editing di inviluppi">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="16"/>
</node>
</node>
<node COLOR="#990000" CREATED="1626012094297" ID="ID_496675899" MODIFIED="1631976017918" POSITION="right" TEXT="Vu-Meter">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
<node COLOR="#111111" CREATED="1626012271138" ID="ID_1859804506" MODIFIED="1631979918774" TEXT="Per la visualizzazione e il controllo dei livelli di soglia dei parametri in formato LED e VU-meter analogici.">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="16"/>
</node>
</node>
<node COLOR="#990000" CREATED="1626012057126" ID="ID_1047003875" MODIFIED="1631976017926" POSITION="left" TEXT="Kaleidophone">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
</node>
<node COLOR="#990000" CREATED="1626012063042" ID="ID_1152883927" MODIFIED="1631976017928" POSITION="left" TEXT="Pipe Simulator">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
</node>
<node COLOR="#990000" CREATED="1631975977481" ID="ID_1149616742" MODIFIED="1631976079183" POSITION="right" TEXT="PCM">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="16"/>
<node COLOR="#111111" CREATED="1631976053578" ID="ID_947833586" MODIFIED="1631979918757" TEXT="per la conversione di segnali analogici in digitali">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="16"/>
</node>
</node>
</node>
</map>
