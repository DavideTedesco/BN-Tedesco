# Appunti della lezione di mercoledì 31 marzo 2021

- Punto della situazione per i materiali di Incontro con Nottoli.

![per](per.png)

## Mobile Locale
- Questione Mobile Locale -> registrazione da fare poichè non definitiva -> mobile locale in celeste

Registrazione da rifare, poichè non ultimato il lavoro

## The King of Denmark
- Feldman - The King of Denmark -> mappa dell'amplificazione -> brano che usa tutte le percussioni -> suonate con le dita (dinamica molto bassa)

Registrazione da fare

## Libra
- Cardi -> Parte frontale
- parte di amplificazione

Registrazione da fare


## Microfonazione

- Per Feldman -> coppia ortf e 2 omnidirezionali da dietro -> amplificazione chiara e minima -> pensare di ricreare il cerchio di suono per la registrazione

- Per Cardi -> sola coppia ortf frontale

- Per Lupone -> 8 microfoni per la ripresa -> setup che approfondiremo oggi

## Partitura di Lupone

### Video
Video per l'esecuzione con vari video sincronizzati.

Partitura alla francese -> edizioni compatte -> non adatta per fare direzione -> partitura analitica e di studio.

![part](part.png)

>Individuare il cambio pagina, senza un solo fotogramma, e fatte in modo plateale per individuarne l'efettivo cambio.

Aggiunta dei colori in partitura per l'esecuzione.

Fondamentale averi i colori per l'esecuzione.

### Riedizione della partitura?!?

Vi sono degli errori nella partitura, che solamente l'esecutore ha trovato.

Partitura non tiene conto di voltare i fogli, ed i cambi pagina cadono in momenti sbagliati.

Edizione EDIPAN

### Call for works

Idea di provare a fare della partitura un unica partitura orizzontale incollando tutte le pagine e metterle allineate, facendo lo srotolamento della partitura, con bordi sfocati.

Per realizzare un unico rotolo lungo:
- after effects
- illustrator

>In Beethoven le edizioni moderne sono tutte state estratte analizzando i diari e non le vecchie partiture che riportavano degli errori.

### Confronto fra scritture

Rihm ha citato che apre la pagina e la scrive pagina per pagina, andando avanti senza tornare indietro, come un dettato a se stesso.

Netti va da uno schema logico sopra, a scendere di livello. Netti scende senza mai risalire, senza contraddire la decisione che aveva fatto prima.

Beethonven scrive nei suoi diari che arrivava con degli scritti illegibili, che i musicisti li trascrivevano e poi lui prendeva

>Netti: necessità di avere un feedback acustico

### Partitura di amplificazione

Partitura di amplificazione per essere attenti all'amplificazione.

Netti si dichiara compositore e non elettronico -> descrive quindi un rito dell'amplificazione

Netti si da dei simboli per il timbro e vi è già nella partitura la spazializzazione.

## Due movimenti

La partitura di Mobile Locale è divisa in due parti:
- mobile -> ostinato
- locale -> interferenza

Qualcosa che avviene statico e fisso ed altro che avviene soltanto in un momento opportuno con una divisione formale in due parti.

Vi sono quindi 2 video.

## Posizioni dei microfoni

Le posizioni dei microfoni sono state diverse da quelle in partitura.

Girando i metalli.
![setupp.png](setupp.png)

![setup.jpeg](setup.jpeg)

Tutto microfonato con gli omni, con la logica di omni, descrivendo in termine timbrico:
microfoni lineari a prescindere dalla distanza, e danno un dettaglio chiaro dei transienti di attacco delle percussioni.

Tutti gli attacchi sono molto nitidi e precisi con omnidirezionali.

>Ruggeri: Djembè piccolo che non si sentiva, ed è stato aggiunto un altro microfono per amplificare le basse frequenze.

ALla fine sono rimasti i microfoni G ed F senza il microfono H che era coincidente.

La cassa prevede una spazzola al posto dello stantuffo battente.

Marco -> ha inserito lo stantuffo insieme alla spazzola cambiando quello che era inserito in partitura -> problema di prassi esecutiva?!?

>Annotare tutto come fa Agostino Di Scipio con catalogazione di tutto.

Avere molte informazioni sul brano di Marco, maggiori di Gianluca Ruggeri.

## Il tape del brano non corrisponde

Cosa grave che è emersa, il tape non corrisponde.

Realizza il tape su un tape già esistente.

Il tape non corrisponde con la seconda parte del brano.

Si è deciso di fare il sincrono a mano, controllando i riferimenti temporali...

**Manca qualcosa nel tape...**

Non è un problema di velocità di trascrizione, vi sono tagli in punti in cui è difficile suonare, vi sono tagli del brano sulle parti difficili da suonare.

Ruggeri aveva tagliato delle parti del nastro.

>Prendere il nastro originale non maneggiato da EDIPAN

Si immaginava che il tape fosse preciso al millisecondo, mentre era invece tagliato.

## Da fare

- codice FLY
- nastro magnetico di mobile locale
- DAT -> nastro digitale di Mobile Locale

**Acqusisizione di nastro magnetici in conservatorio.**

_______
Problemi di REAPER per il brano di Lupone:
- gestione dei segnali da mono microfono a stone -> bisogna farsi tutto a mano -> vantaggio di Nuendo cambiare riproduzione
- automazione dei mute per il live -> senza avere feedback e problemi con microfoni

>A parte i livelli di equilibrio tra tape, percussioni e amplificazioni, non c;è molto da fare.

- canali VCA di Nuendo -> è solo l'emulazione di un VCA -> VCA mano santa per mix dinamici -> con un solo fader si muovono vari livelli


VCA -> sposta i canali in proporzione

![vca.png](vca.png)

Il master modifica l'ampiezza a livello a cui sono fissati due microfoni di partenza.

...

Architettura VST -> Steinberg -> miglior scalamento possibile fra dinamica reale e Architettura in virgola mobile di calcolo.

Hanno la gamma scalata al centro, con headroom infinito, e non si andrà mai in distorsione.

RME punto di clip a -3dB.

________

Registrazione -> RME

_______

Merging Technologies -> API

## NUENDO

- suite ADR
- suite allineamento di take
- montatore che monta su un solo microfono -> sync
- link a Wise e Real -> videogioco
- ambisonics integrato del terzo ordine
- sistemi per il timecode
