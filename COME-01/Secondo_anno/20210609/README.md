# Appunti della lezione di mercoledì 9 giugno 2021

## News

1. progetto SEAM -> madrelingua protocollato Trinity percorso per la revisione dell'articolo -> momento di crescita a prescindere dal percorso SEAM (percorso a pagamento)

2. invito per un ascolto da Walter Branchi al Roseto ad Orvieto -> intero giorno con Walter Branchi -> percorso di illustrazione sulle università delle rose

Ascolto di Branchi (non da chiamare concerto), dopo la visita alle rose, cheescake, due altoparlanti verso l'alto in una chiesetta.

(Testo da Utopia dell'ascolto di Mastrogiacomo)

Livello audio molto basso per l'integrazione del suono con la chiesa:

![1](1.jpg)

![2](2.jpg)

**Come si puó eseguire un brano di Walter Branchi?**

Bisogna inserire nel catalogo delle cose da fare le accortezze del compositore.

Branchi è difficile che dia da suonare qualche suo brano a qualcun altro.

>Dal silenzio al nuovo mondo sonoro. Evanglisti

## Branchi

- suono trovato
- suono creato -> prerogativa della musica elettronica

Differenza fra le due tipologie di suoni...

Chiave di lettura per capire le incomprensioni della musica di Walter Branchi, per l'inutilità della complessità.

Intero è il titolo che Branchi da alla sua intera composizione.

Ascolto di Intero al rientro di Branchi dagli states dopo la morte di Evangelisti realizzó molti problemi per i compositori.

Ruolo sociale del fare musica e di ascoltarle con estrema sensibilità, qualcosa da cercare all'interno dell'ambiente sonoro.

Qualcosa che condiziona la visione del fare musica.

Arte che a livello sinestesico non è piú divisa e non ha più senso pensare cose separate.

>Momento di fruizione artistica in cui non c'è separazione fra le cose.

Quando non si mette nulla in vetrina non vi è piú un gioco di punti, linee e superfici.

Abbiamo quindi un unico luogo sinestesico.

Scomposizione alla base degli odori...

***

Installazione di faust e jack su Linux

Discussione sulla spazializzazione su Olofoni.
