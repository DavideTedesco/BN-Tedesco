# Appunti della lezione di martedì 5 Maggio 2021

Meccanismo di seam a regime, puntando alle librerie non riscrivendo ogni volta il comb.

Usiamo il riverbero di Schroeder:
```
//------------------------------------------- DELAY FEEDBACK IN LOOP - FIXED ---
dflc(t,g) = (+ : @(t-1))~*(g) : mem;
//
```

Correggiamo il codice usando i metodi `min` e `max` che servono a limitare.  

Il clipper di max, da un minimo e un massimo, come una composizione di `min` e `max`.

Per far funzionare l'operatore tilde con un numero dichiarato.

La funzione `delay` di faust non è altro che l'opertore `@` con `min` e `max`.

```
import ("stdfaust.lib");
// COMB
dflc(t,g) = (+ : @(max(t-1,0)))~*(g) : mem;
er8comb(g1,g2,g3,g4) = _ <:
  g1*(0.5*(dflc(er1,0) + dflc(er2,0))),
  g2*(0.5*(dflc(er3,0) + dflc(er4,0))),
  g3*(0.5*(dflc(er5,0) + dflc(er6,0))),
  g4*(0.5*(dflc(er7,0) + dflc(er8,0)))
    with{
      er1 = ba.sec2samp(0.087) ;
      er2 = ba.sec2samp(0.026) ;
      er3 = ba.sec2samp(0.032) ;
      er4 = ba.sec2samp(0.053) ;
      er5 = ba.sec2samp(0.074) ;
      er6 = ba.sec2samp(0.047) ;
      er7 = ba.sec2samp(0.059) ;
      er8 = ba.sec2samp(0.022) ;
  };
    g1 = hslider("[01] ER 1 [midi:ctrl 83]", 0,0,1,0.01) : si.smoo;
    g2 = hslider("[02] ER 2 [midi:ctrl 84]", 0,0,1,0.01) : si.smoo;
    g3 = hslider("[03] ER 3 [midi:ctrl 85]", 0,0,1,0.01) : si.smoo;
    g4 = hslider("[04] ER 4 [midi:ctrl 86]", 0,0,1,0.01) : si.smoo;
  process = er8comb(g1,g2,g3,g4);
```

Lo realizzimo con l'oggetto `par`, per capire come creare la struttura all'interno del `par`.

Il segnale del feedback è differente dal tempo di ritardo.

>Possiamo avere un ritardo che va da 0 a sample rate.

Abbiamo 3 entrate per ogni comb in cui possiamo dare a `dflc` 3 variabili...

Abbiamo realizzato il comb filter per Lupone.

```
dflc(t,g) = (+ : @(min(max(t-1,0),ma.SR)))~*(g) : mem;
er1 = ba.sec2samp(0.087);
er2 = ba.sec2samp(0.026);
er3 = ba.sec2samp(0.032);
er4 = ba.sec2samp(0.053);
er5 = ba.sec2samp(0.074);
er6 = ba.sec2samp(0.047);
er7 = ba.sec2samp(0.059);
er8 = ba.sec2samp(0.022);
combN(N)= ro.interleave(N,3) : par(i,N,dflc);
g8 = par(i,8,0);
process = combN(8,(er1,er2,er3,er4,er5,er6,er7,er8),g8):par(i,4,+*(0.5));
```

Parlando di manutenzione librerie, dovranno...

Tempo massimo per passare da un comb a un delay è tra i 40 e i 50ms?

Lupone spegne il feedback e non abbiamo piú comb, ma solo ritardi.

Come sono scelti i numeri dei comb?
![nu](nu.png)
