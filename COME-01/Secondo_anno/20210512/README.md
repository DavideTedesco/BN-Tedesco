# Appunti della lezione di mercoledì 12 Maggio 2021

Musicista madrelingua per il testo del SEAM.

SMC -> ha bocciato l'articolo su Ambisonics...

Tavola rotonda con modello di riferimento LEMS per gli incontri...

***

```
qaqf(x) = de.fdelayltv(1,writesize, readindex, x) : *(gain) <: _,*(0),_,*(0)
  with{
    writesize = ba.sec2samp(0.046);
    readindex = poscil*(writesize);
    gain = delgroup(vslider("[03] QA [midi:ctrl 82]", 0, 0, 1, 0.01) : si.smoo);
  };
  ```

>Abbiamo un timbro complesso e ragionato, realizzato da un gesto.

![allocazione dinamica](allocazione dinamica.png)

L'oscillazione è un oscillazione di lettura.

  - Memoria interpolata oppure no?

## Qaudrifonia

La quadrifonia di mobile locale:
![q](q.png)

Essa è realizzata con quadrifonia incrociata, essa è una scelta estetica che non aumenta la precisione solo al centro, ma in tutta la sala.

I movimenti creano continui movimenti di fase.

![o](o.png)

## WAZA

22000/1024

((22050/1024)*1.02246093)

((22050/1024)*0.99699327)

((22050/1024)*1.02246093)-((22000/1024)*0.99699327) = 0.59708160791

Frequenza del battimento a cui si fanno le letture

## Presentazione di alcuni temi

Temi di materiale di tavola rotonda -> con incontri sul materiale presentando il libro di Agostino Di Scipio a Settembre.

Recensione del libro su Musica/Realtà

***

## Scuola Romana è un fatto storico o geografico?

Capire se nei nostri interessi vi è l'Ecosistemico di Di Scipio...

Scuola Romana come accezione negativa?

>Scuola di Pesaro è nata perchè da Roma si andava a Pesaro...

Galante non trova niente di male a parlare di "Scuola Romana"...

- Incontro di Nottoli -> brano per 3 ambienti -> destinazione -> 50 anni dall'evento del Lila -> 3 performer
- Ecosistemico di Di Scipio -> si studia e si suona -> 1 performer
- Baggiani -> lavoro profondo da fare sulla partitura -> costruzione di un Ring Modulatore di tipo analogico per Twins -> 3 performer
- Concerto su musiche di Lucier da mettere in piedi per Settembre, contattandolo almeno in conferenza a distanza(legame Lucier - Di Scipio)

***

Partiamo dal brano di Di Scipio il 26 che ha molti spunti di ragionamento oltre il fatto che è un brano di Agostino Di Scipio.

***

- ripubblicazione di Incontri di fasce Sonore -> una copia del lucido da visionare a studio per scrivere un apparato critico -> sarà possibile avere una copia dei lucidi di incontri di fasce sonore a circa 40 euro

***

## Realizzata [issue](https://github.com/s-e-a-m/fc1991lmml/issues/8) su github sullo stato di Mobile Locale

**Stato del porting di Mobile Locale ed osservazioni per la riedizione della partitura**
Relativamente al QAQF da riga 23 fino a 44 del .dsp

- vedere dal codice del FLY la natura della memoria, se interpolata o no(da questa informazione deriva il funzionamento generale della memoria)
- capire il QA che è in partitura se è in ingresso o in uscita(già presente a pagina 1 della partitura) -> il QA è l'ampiezza (presente in 2
- accertare se l'offset dell'oscillatore di QA è fisso ad 1 o è in funzione di QA(offset v=1) -> pagina 8 della partitura
- la quadrifonia sarà uno stereo incrociato per tutto il brano tranne le early reflections(ricordarsi del FLY30 double board)
- uso del termine COMB per le early reflections nella parte generalizzata della pagina 8, ma poi i COMB divengono DELAY LINE
- tutti i dettagli di errori ed imprecisioni della partitura sono da tenere a mente per la riedizione della partitura
- verificare come per QA e QF se WA e ZA sono interpolati
