import("stdfaust.lib");

N = 5;
S = 16;
rIdx = os.phasor(S, 100);
wIdx = ba.period(S);

process = os.osc(200) <:it.frwtable(5, 16, (float)0, wIdx, _, rIdx);