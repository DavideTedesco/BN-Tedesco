import("seam.discipio.lib");

var1 = 40;
var2 = 1000;
var3 = 1000;

//cntrlMain = 42;
//memWriteLev = 23;

mapsum(x,s,m) = s+(x*(m));
mapsub(x,s,m) = s-(x*(m));
mapmul(x,s,m) = m*(x+s);
mapcond(x) = select2(x>0.5,1, (1-x)*2);

//------------------------------------------- signal flow 1a

signal_flow_1a(mic1, mic2, mic3, mic4,var1, var2) = mic1, mic2,(mic3, mic4 <: +,(
  _,_,(integrator(0.01), integrator(0.01) :
  delay(0.01,0.95),delay(0.01,0.95) : 
  + :
  mapsum(_,6,6) <: 
  _,_,_) : 
  _,ro.cross(2),_,_  :
  localmax, localmax,_ : 
  -,_ : 
  localmax <: 
  delay(12,0),_ : 
  + : 
  mapsum(_,0,0.5) : 
  lp1p(0.5)) : 
  _ ,(_<: _,_) : 
  ro.cross(2),_ : 
  _, (_<: _,_,_,_),_ :
  _,_,_,_,* :
  mapsub(_,1,1),
  (fi.highpass(3,var2) : integrator(0.05)),
  (fi.lowpass(3,var2) : integrator(0.1)),
  integrator(0.1), 
  integrator(0.01) : 
  _,-,_,_ :
  *,_,_:
  delay(0.01,0.995),
    delay(0.01,0.9),
    delay(0.01,0.995) :
par(i,3,fi.lowpass(5,25)):
mapsum(_,0.5,0.5),
(( mapsub(_^2,1,1)) <: delay(var1/2,0),delay(var1/3,0),_),
(_<: delay(var1/3,0),delay(var1/2,0), mapcond,_));

//------------------------------------------- signal flow 1b

signal_flow_1b(grainOut1, grainOut2, mic1, mic2, memWriteLev, cntrlMain, var1, var3) = 
(
    mic1, mic2 : hp1(50),hp1(50) : lp1p(6000), lp1p(6000) : integrator(0.01), integrator(0.01) : delay(0.01,0.999), delay(0.01,0.999) : fi.lowpass(5, 0.5), fi.lowpass(5, 0.5)
    ), 
    (
 grainOut1, grainOut2: + : integrator(0.01) : delay(0.01, 0.97) : fi.lowpass(5,0.5) <: _+delay(var1*(2), (1-var3)*(0.5)) : mapsub(_,1,0.5) 
), (
    (timeIndex(var1)<: mapmul(_,-2,0.5),mapmul(_,1,0.5)), triangle1(var1, memWriteLev)*memWriteLev,triangle2(var1, cntrlMain), triangle3(var1)  
)with{
    timeIndex(var1) = os.lf_trianglepos(1/(var1*(2)));
    triangle1(var1, memWriteLev) = os.lf_trianglepos(1/(var1*(6)));
    triangle2(var1, cntrlMain) = os.lf_trianglepos(var1*(1-cntrlMain));
    triangle3(var1) = os.lf_trianglepos(1/var1);
}
;

signal_flow_2a(cntrlMic1, cntrlMic2, directLevel, triangle1, triangle2, mic1, mic2, diffHL, memWriteDel1, memWriteDel2, memWriteLev, cntrlFeed, cntrlMain, var1, var2) = cntrlMic1, cntrlMic2, directLevel, triangle1, triangle2, mic1, mic2;

//signal_flow_2b() = ;

//process = no.no.multinoise(2) : *(0.1),*(0.1) : signal_flow_1a;

//process = signal_flow_1b(no.noise*(0.1),no.pink_noise*(0.2), _, _, 0.5,23,1,23);

process = signal_flow_1a <: 
si.bus(30) :
//1b 
23, 42, _,_,!,!,!,_,!,!,!,_, var1,var2,
//2a
_,_,_,_,_,_,!,!,_,_,
//2b
!,!,!,_,_,_,_,_,!,!;

//: (signal_flow_1b <: si.bus(16)), _,_,_,_,_,!,!,!,var1,var2 : !,!,!,_,_,!,!,_, signal_flow_2a;

//process = signal_flow_1b;