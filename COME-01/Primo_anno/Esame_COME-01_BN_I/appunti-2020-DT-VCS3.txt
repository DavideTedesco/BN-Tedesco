L'EMS VCS3 e la sua matrice

ABSTRACT

La realizzazione del brano "incontro" di Giorgio Nottoli, ci ha posto di fronte a vari e profondamente diverse problematiche.
Esse affrontate con i giusti mezzi ci hanno permesso di iniziare a realizzare gli strumenti necessari per l'esecuzione e l'interpretazione del brano.
I documenti forniti dall'autore, oltre ad il manuale dello strumento principe(l'EMS VCS3) di questa composizione hanno reso più semplice il lavoro di "porting" dello strumento, facendoci intendere a fondo come realizzare digitalmente uno strumento analogico complesso e composto da varie tipologie interconnesse di hardware.
FAUST e Max ci hanno aiutato lungo il percorso, permettendoci di realizzare lo strumento ponendoci una "tabula rasa" capace di farci capire ed intendere quali scelte algoritme effettuare a seconda degli aspetti fisici necessari.

INTRODUZIONE: Il VCS3

LE FUNZIONALITA`

LA MATRICE

UTILIZZO DI FAUST PER LA REALIZZAZIONE DELLA MATRICE
codice finale matrice (

import("stdfaust.lib");

//switch integrato
switch(in) = checkbox("IN %3inn") : si.smoo
    with{
     inn = in+(1);
};

//gruppo di switches per l'uscita di una colonna
outcol(in,col) = vgroup("OUT %3coln", par(in, in, *(switch(in)))) :> _
    with{
        coln = col+(1);
};


matrix(in,out) = hgroup("MATRIX %in x %out", si.bus(in) <: par(out, out, outcol(in,out)));

process = matrix(16,16);

)

CONCLUSIONI

Riutilizzo della matrice


Bibliografia

1. https://vimeo.com/70164353
2. youtube.com/watch?v=-bTcf6kGcyg
3. https://www.youtube.com/watch?v=YyAoKhtpZ98
4. https://www.youtube.com/watch?v=sOw42WQIXog
5. https://sussexvcs.wordpress.com/page/2/
6. http://www.openculture.com/2017/11/what-the-future-sounded-like.html#:~:text=What%20the%20Future%20Sounded%20Like,Garde%20Electronic%20Musicians%20%7C%20Open%20Culture
