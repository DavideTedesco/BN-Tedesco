# Lezione del 29 aprile 2023

Programma dell'anno.

Brani Luigi Nono:
- _Risonanze Erranti_
- _Post-Praeludium Per Donau_
- _A Pierre. Dell’azzurro silenzio, inquietum_

>Come tutti i piani divini, un piano divino appare caotico.


## A Pierre. Dell’azzurro silenzio, inquietum

Esso è il brano che Nono scrive per Pierre Boulez e nella tradizione dei nuovi compositori vi è sempre stata la tradizione di regalare musica.

Boulez scrive Il Dialogue per regalarlo a Berio, esso è un disassemblamento e riassemblamentodella Sequenza per Clarinetto, cita la Sequenza per pianoforte di Berio con le risonanze.

Vi sono elementi che spesso finiscono in regali di compleanno, A Pierre è proprio un elemento particolare, perchè esso è un regalo.

_Risonanze Erranti_ è un omaggio a Cacciari.

Dal punto di vista del perchè si fanno le cose, si ha un qualcosa di veramente potente.

Per Nono in quel momento si viveva in contatto quotidiano per mesi interi per portare a compimento il lavoro del Brano, perchè il brano è assimilato.

Portare sulla partitura edita, qualcosa che in realtà non ha necessità della carta è utile solo per l'editore.

_[Ascolto di A Pierre. Dell’azzurro silenzio, inquietum](https://www.youtube.com/watch?v=qz3LXXcEVCc)_

### Ruolo dell'elettronica nel brano

Come staccare l'elettronica dagli strumenti?

Abbiamo un unico produttore di suono, come intendere cosa provenga dall'elettronica o dallo strumento?

Sappiamo che il suono è la nostra sensazione della vibrazione acustica. Sappiamo che le orecchie sono il nostro modo di interagire con lo spazio che ci circonda.

>Lo strumento, diventa lo strumento di indagine nello spazio.

Uno strumento diventa un elemento che permette di studiare l'indagine uomo-ambiente.

Si sonda lo spazio acustico e quello virtuale del suono riprodotto.

Tutto ciò che accinge al suono trovato, è la nostra realtà virtuale.

>Nono rende l'elettronica uno strumento indagabile.

Attitudine all'indagine di ciò che si sta facendo in un brano è qualcosa di indescrivibile.

Nono e Evangelisti hanno avuto un dramma personale, nell'accezione teatrale, con una drammaturgia del non scrivibile.

Nono chiama il brano Tragedia dell'ascolto perchè vuole sbattere davanti a tutti che vi è una storia dell'ascolto.

Abbiamo che la scrittura diviene un problema di necessità, abbiamo che la musica è un problema di pensiero, non di pezzi di carta.

Il problema è come accedere al sapere dopo le crisi del Novecento.

Beethoven ha portato alla crisi dei linguaggi ma per noi è chiaro che il riferimento sia assimilabile.

Che cos'è uno strumento? Da Netti si lega l'indagine sul che cos'è una realtà.

(Ciclo per sassofono scritto di getto, con un'ora di musica per sassofono soprano. Aggiunta di _Ultimo a lato_

### Partiture di Nono: strumenti ed elettronica

Nono viveva di elettronica per gli strumenti, non elementi solamente tecnici.

Creare un contatto di relazioni tra musicisti, i problemi musicali, sono problemi reali.

Se la carta ha dominato la composizione, non vi è gente capace di leggere la musica prima.

Vedere nell'istituzione teatrale il fare repertorio è la causa e non il problema dello scrivere.

### Arca di Nono

Essa è l'ambiente che solleva l'idea.


### SIM

Serie di 4 puntate ideata e diretta da Nicola Bernardini su Musica e Computer.

La Società di Informatica Musicale di Roma

>La composizione è un atto differito.

- paradigma di Boulez per la composizione  e il tempo reale -> nel tempo differito bisogna utilizzare la fantasia, per immaginare come la sinfonia deve svilupparsi e nel rapporto della sintesi non si può utilizzare la fantasia
- pardigma di Nono per la composizione e il tempo reale -> rispetto a Boulez, che diceva che nel tempo reale si ha un'oggettività del suono che suona subito, mentre Nono esprime che il procedimento è più veloce, e quindi non è che correggendo subito si finisce di avere una relazione da strumentista
- Bernardini -> problema del tempo reale è un problema politico

Brani realizzati con il Fly30:
- Canto di madre
- La forma del respiro
- Mobile locale

### Luigi Nono e l'elettronica

Capire come funzionano gli strumenti elettronici per capire come funzionano effettivamente.


### Boulez e l'elettronica

Spazio acustico modulare, e la creazione della situazione in cui ascoltare una data musica.

Boulez ha un'opera per adattare il nastro magnetico.

L'opera su nastro non cambia.

### Nono e il nastro

Spazio trasforma il nastro, il nastro trasforma lo spazio. Altoparlanti come strumento.

### Compositori interpellati per lo stesso periodo

Abbiamo due compositori interpellati nello stesso periodo, entrambi con un'enorme visione prospettica di ciò che sta avvenendo in quel momento.

### Sostenibilizzare, e concetti, perchè?

Presentazione di SEAM.

Sostenibilità della Musica Elettroacustica partendo da Ottorino Respighi. Si cerca di affrontare il repertorio cercando di portare i brani in concerto scrivendo e riportando gli elementi dei brani in concerto.

- non sottovalutare elementi tecnologici
- "la vera storia della musica è la musica degli strumenti musicali" -> la tecnologia è la musica, molta tecnologia è stata spinta in avanti dalla teconologia
- il vincolo tecnologia-musica ha portato a dei brani dettagliati, insuonabili e brani che non sono dettagliati ma con teconologia viva

### Walter Branchi: Thin

Edizioni TEAM, gruppo di Walter Branchi per suonare la musica che vi era in Conservatorio.

Il gruppo TEAM ha suonato i brani del gruppo.

Thin e le edizioni del gruppo, serviva per trasmettere quello che si stava facendo negli anni.

Elementi come suonare con il bicchiere era un qualcosa che era con pratica.

Unica incisione disponibile è sbagliata, realizzata da Ceccarelli

### Prassi della Scuola di Musica Elettronica Romana

Raremente i compositori romani pubblicano partiture con editori.

Si pubblicano in genere dischi.

Piatto rotante che è spazializzato grazie proprio alla microfonazione.

Bertoncini, trovata in cui il microfono a contatto fa da interruttore con azione deliberata dal contatto ed ascoltandosi si da vita al brano.

Per fare questo brano Marco di Gasbarro ha passato una giornata intera per scegliere il brano.

Piatto scelto per il brano, il titolo Thin si riferisce allo spessore del piatto. Il piatto è talmente sottile che se lo si suona con l'archetto si vedono i modi di risonanza del piatto.
