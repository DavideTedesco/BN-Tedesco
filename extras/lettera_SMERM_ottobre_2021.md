# Ai docenti della Scuola di Musica Elettronica

- Direzione ed obiettivi della Scuola di Musica Elettronica
- Organizzazione dei programmi didattici concordati fra i vari docenti
- Pubblicazione di chiare modalità di ammissione per il Triennio ed il Biennio
- Chiarezza nelle oppurtunità post-diploma
- Cura della gestione del materiale della Scuola di Musica Elettronica (EMUFest incluso)
- Riproposizione delle Borse di Collaborazione della Scuola di Musica Elettronica
- Adempimenti per poter istituire nuovamente un festival di Musica Elettronica sulla linea di EMUFest, ormai dimenticato o mai conosciuto dai ragazzi più giovani
- Utilizzo degli spazi e dei materiali della Scuola di Musica Elettronica
- Richiesta degli esami in presenza(per sentire le composizioni durante l'esame e per poter inizare o continuarele lezioni pratiche)
- Richiesta di un giorno di saggio annuale
