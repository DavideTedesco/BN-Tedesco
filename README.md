# Repository del ciclo di Biennio di Davide Tedesco

## [COME-01](/COME-01)
#### _Esecuzione e interpretazione della musica elettroacustica. Ambienti esecutivi e di controllo per il live electronics_

_Maestro Giuseppe Silvi_

### Primo anno

![foto](/COME-01/Primo_anno/Esame_COME-01_BN_I/2020_Davide_Tedesco_La_matrice_dell’EMS_VCS3/img/ems-vcs3-inferior-panel.jpg)
#### Brani trattati durante il corso del primo anno:
- Giorgio Nottoli - Incontro

##### Giorgio Nottoli - Incontro
- realizzazione del VCS3 in Faust
  - realizzazione dei 3 oscillatori
  - realizzazione del filter/oscillator
  - realizzazione della matrice

[Testo presentato all'esame del primo anno di Biennio sulla matrice del VCS3](/COME-01/Primo_anno/Esame_COME-01_BN_I/2020_Davide_Tedesco_La_matrice_dell’EMS_VCS3/2020_Davide_Tedesco_La_matrice_dell’EMS_VCS3.pdf)


### Secondo anno
![foto](/COME-01/Secondo_anno/Esame_COME-01_BN_II/AE2.svg)

#### Brani trattati durante il corso del secondo anno:
- Michelangelo Lupone - Mobile Locale
- Agostino Di Scipio - Ecosistemico Audibile 2a/2b

##### Michelangelo Lupone - Mobile Locale
- visione e montaggio della partitura su video
- realizzazione del porting in Faust degli strumenti elettronici
- prove e registrazione del brano in Sala Accademica

##### Agostino Di Scipio - Ecosistemico Audibile 2a/2b
- visione della partitura
- introduzione all'algoritmica utilizzata da Di Scipio in Faust
- visione degli algoritmi già implementati da altri(vedasi [il progetto di Davide Maggio](https://gitlab.com/DavideMaggio/ae))

[Mappa mentale esplicativa del lavoro realizzato](https://www.davidetedesco.it/agostino-di-scipio-audible-ecosystemic-2-mappa-mentale-di-davide-tedesco/)

[Materiale presentato all'esame del secondo anno di Biennio sull'Ecosistemico Audibile 2 di Agostino Di Scipio](https://github.com/s-e-a-m/fc2003dsaae2)

___
## [COME-02](/COME-02)
#### _Composizione musicale elettroacustica_

_Maestro Michelangelo Lupone - Maestro Nicola Bernardini - Maestro Pasquale Citera_

### Primo anno

![TP](COME-02/personal/TP.png)

[Mappa mentale esplicativa del brano](https://www.davidetedesco.it/davide-tedesco-talmente-poco-mappa-mentale/)

[Brano per ensemble vocale presentato per l'esame del primo anno di Biennio](https://certain-sweater-2c3.notion.site/Davide-Tedesco-Talmente-Poco-5e53590649704596abfefff2d3ec9a97)

### Secondo anno

![Guitar](COME-02/Primo_anno/Bernardini/20200114/Schemi_chitarra_classica/ebguitars_plan_complete_large.png)

Ricerca e composizione sulla chitarra classica e primo approccio allo sviluppo di studi per lo strumento _aumentato_.

- la chitarra come percussione, ovvero catalogazione delle modalità di percussione-> [schema ed esempi](COME-02/Primo_anno/Bernardini/20200204/Modi%20di%20percussione%20con%20le%20dita.pdf)
- eccitazione del piano armonico attraverso un attuatore [Visaton EX 60 R](COME-02/Primo_anno/Lezioni_in_Compresenza/20200407/ex60r_8.pdf) -> [analisi spettrale ed esempi](COME-02/Primo_anno/Lezioni_in_Compresenza/20200317/Various_Sweeps)
- ripresa con sensore piezoelettrico dello strumento(Rappresentazione del _micro_, il "da vicino")
- ripresa microfonica con microfono esterno dallo strumento (Rappresentazione del _macro_, il "davanti allo strumento")
- catalogazione dei suoni consonantici e vocalici per implementazione in live degli stessi sull'attuatore -> esempi
- criteri per la spazializzazione in sala da concerto ???
- gestione agevole del suono ripreso micro e macro (algoritmo di interpolazione fra i due???)
- algoritmo di sintesi della corda "Karplus-Strong" -> [articolo](COME-02/Primo_anno/Lezioni_in_Compresenza/20200303/Kevin%20Karplus%20and%20Alex%20Strong%20-%20Digital%20Synthesis%20of%20Plucked-String%20and%20Drum%20Timbres.pdf), [algoritmo realizzato in Faust](COME-02/Primo_anno/Lezioni_in_Compresenza/20200324/karplus_filtrato.dsp), [Esempi](/COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/Esempi_Karplus-Strong_Attuatore_su_chitarra)
- estensioni dell'algoritmo di sintesi della corda "Karplus-Strong" -> [articolo](COME-02/Primo_anno/Lezioni_in_Compresenza/20200407/David%20A.%20Jaffe%20and%20Julius%20O.%20Smith%20-%20Extensions%20of%20the%20Karplus-Strong%20Plucked-String%20Algorithm.pdf)
- Lettura e studio del manuale "The Contemporary Guitar" di John Schneider -> [Manuale](COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/The-Contemporary-Guitar-Schneider-Vol-5.pdf)
- Lettura e studio di "Principles of Idiomatic Guitar Writing" di Jonathan Godfrey ->[Testo](COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/Principles%20of%20Idiomatic%20Guitar%20Writing%20-%20Jonathan%20Godfrey.pdf),  [Esempi Audio](COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/Principles%20of%20Idiomatic%20Guitar%20Writing%20-%20Jonathan%20Godfrey_Audio_Tracks)
- sostegno infinito del suono di chitarra attraverso l'[algoritmo realizzato in Faust](COME-02/Primo_anno/Lezioni_in_Compresenza/20200324/karplus_filtrato.dsp)
- lettura del [testo di Emilio Pujol "The dilemma of Timbre on the Guitar"](COME-02/Primo_anno/Lezioni_in_Compresenza/1979-Emilio-Pujol-Il-Dilemma-del-Timbro-sulla-Chitarra/1979-emilio-pujol-el-dilema-del-sonido-en-la-guitarra.pdf), [traduzione in italiano del testo di Emilio Pujol "The dilemma of Timbre on the Guitar"(non ultimata)](COME-02/Primo_anno/Lezioni_in_Compresenza/1979-Emilio-Pujol-Il-Dilemma-del-Timbro-sulla-Chitarra/1979-Emilio-Pujol-Il-Dilemma-del-Timbro-sulla-Chitarra-Traduzione)
- analisi dei brani di [chitarra elettrica contemporanea](COME-02/Primo_anno/Lezioni_in_Compresenza/Chitarra_Elettrica_Contemporanea)
- [VIDEO - esempio chitarra classica distorta](https://youtu.be/K3yqyxcJStg)
- [VIDEO - esempio feedback strutturale(piezo-attuatore) su chitarra classica](https://www.youtube.com/watch?v=TraqAMf5Exo)-> nella descrizione del video, minutaggio segnato di alcuni primi esempi di feedback
- [algoritmo per la gestione del feedback realizzato in Faust(FEEDBACK_CONTROLLER v.0.1)](COME-02/Primo_anno/Lezioni_in_Compresenza/20200512/FEEDBACK_CONTROLLER.dsp)
- IR chitarra classica sistema sensore-attuatore [posizione 1](COME-02/Primo_anno/Lezioni_in_Compresenza/20200519/Sistema_Sensore-Attuatore_Posizione_1) ![ir chitarra posizione 1](COME-02/Primo_anno/Lezioni_in_Compresenza/20200519/Classica_Attuatore_Posizione_1_Cal.jpg)
- IR chitarra classica sistema sensore-attuatore posizione 2(da caricare)
- [VIDEO - implementazione di un LPF e un pedale MIDI(ON/OFF) per controllo del feedback, scordatura durante Feedback](https://youtu.be/7BwwTopM3Ek)


#### Lavori
##### Sketches
- [Sketches](COME-02/Primo_anno/Lezioni_in_Compresenza/20200324/Sketches.pdf)

##### Studi
- Legenda: [Draft_1_Legenda](COME-02/Primo_anno/Lezioni_in_Compresenza/20200407/Legenda_Part_Perc.jpg)
- Legenda elettroacustica: [Schema 1](/COME-02/Primo_anno/Lezioni_in_Compresenza/20200616/guitar_Electroacustic_Scheme_I.jpg)
- Studio n.1: [Intenzioni compositive dello Studio n.1](COME-02/Primo_anno/Lezioni_in_Compresenza/20200505/Intenzioni_compositive_dello_Studio_n.1.md),[Draft_1 Studio n.1 Partitura](COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/Draft_1_Studio_n.1_Partitura.pdf), [Draft_1 Studio n.1 Audio](COME-02/Primo_anno/Lezioni_in_Compresenza/20200331/Draft_1_Studio_n.1_Audio.wav)
- Studio n.2: [Draft_1 Studio n.2 Partitura](COME-02/Primo_anno/Lezioni_in_Compresenza/20200407/Draft_1%20Studio%20n.2%20Partitura.pdf)
- Studio n.3: [Intenzioni compositive dello Studio n.3](COME-02/Primo_anno/Lezioni_in_Compresenza/20200519/Intenzioni_compositive_dello_Studio_n.3.md), [Appunti Studio n.3 (a)](COME-02/Primo_anno/Lezioni_in_Compresenza/20200519/Appunti_Studio_n.3_a.jpeg)
- Studio n.4 a: [partitura alla prima versione](/COME-02/Primo_anno/Lezioni_in_Compresenza/20200616/Studio_n.4_a.pdf)
- Studio n.5:  (sfuttare la scordatura naturale della chitarra)
- Studio n.6:

#### Brani
- Come un dittero, aldilà dell'ombra

#### Papers
- [Verso una nuova frontiera della chitarra classica - Luglio 2020](COME-02/Primo_anno/Davide_Tedesco_Verso_una_nuova_frontiera_della_chitarra_classica/Davide_Tedesco_Verso_una_nuova_frontiera_della_chitarra_classica.pdf)
___

## [COME-03](/COME-03)
#### _Acustica degli ambienti musicali_

_Ingegnere Marco Giordano_

##### Prima annualità

![foto](/COME-03/Primo_anno/20200610/tuboottagonale.png)

[Libri e risorse utilizzate](/COME-03/Primo_anno/Risorse)

- [Prima Lezione](/COME-03/Primo_anno/20200527/)
- [Seconda Lezione](/COME-03/Primo_anno/20200603/)
- [Terza Lezione](/COME-03/Primo_anno/20200610/)
- [Quarta Lezione](/COME-03/Primo_anno/20200617/)
- [Quinta Lezione](/COME-03/Primo_anno/20200624/)
- [Sesta Lezione](/COME-03/Primo_anno/20200701)
- [Settima Lezione](/COME-03/Primo_anno/20200708/)
- [Ottava Lezione](/COME-03/Primo_anno/20200909/)
- [Nona Lezione](/COME-03/Primo_anno/20200916/)
- [Decima Lezione](/COME-03/Primo_anno/20200923/)

[Testo presentato all'esame di primo biennio sull'_Analisi acustica di un home studio_](/COME-03/Primo_anno/Esame_COME-03_BN_I/2020_Davide_Tedesco_Analisi_acustica_di_un_home_studio.pdf)

##### Seconda annualità
![foto](/COME-03/Secondo_anno/20210705/rms.png)

- [Prima Lezione](/COME-03/Secondo_anno/20200628/)
- [Seconda Lezione](/COME-03/Secondo_anno/20210705/)
- [Terza Lezione](/COME-03/Secondo_anno/20210717/)
- [Quarta Lezione](/COME-03/Secondo_anno/20210721/)
- [Quinta Lezione](/COME-03/Secondo_anno/20210914/)
- [Sesta Lezione](/COME-03/Secondo_anno/20210917/)
- [Settima Lezione](/COME-03/Secondo_anno/20210921/)

[Testo presentato all'esame di secondo biennio sull'_Analisi acustica di alcuni descrittori di strumenti musicali a corda pizzicata_](/COME-03/Secondo_anno/Esame_COME-03_BN_II/2021_Davide_Tedesco_Analisi_di_alcuni_descrittori_di_strumenti_musicali_a_corda_pizzicata.pdf)
___

## [COME-04](/COME-04)

#### _Elettroacustica_

_Maestro Federico Scalas_

##### Prima annualità

![farfallone](/COME-04/Primo_anno/Spherical_Harmonics_deg3.png)
### Argomenti trattati:
#### Teoria
- Audio multi-canale
- Ambiophonics
- Ambisonics
#### Pratica
- Inizio di saldatura di componenti hardware per l'audio
- La resistenza
- Il condensatore
- Esempi di primi filtri
- Il diodo
- Il transistor

#### Progetto per l'esame di primo biennio
- Realizzazione di due diffusori passivi; caratteristiche:
  - a 2 vie
  - crossover regolabile
  - mobile in multistrato

[Testo presentato all'esame sulla Realizzazione di una coppia di diffusori acustici a due vie](/COME-04/Primo_anno/Esame_COME-04/Primo_anno_BN_I/2020_Davide_Tedesco_Realizzazione_di_una_coppia_di_diffusori_acustici_a_due_vie)

##### Seconda annualità

- [Prima Lezione](/COME-04/Secondo_anno/20210108/)
- [Seconda Lezione](/COME-04/Secondo_anno/20210115/)
- [Terza Lezione](/COME-04/Secondo_anno/20210122/)
- [Quarta Lezione](/COME-04/Secondo_anno/20210205/)
- [Quinta Lezione](/COME-04/Secondo_anno/20210215/)
- [Sesta Lezione](/COME-04/Secondo_anno/20210219/)
- [Settima Lezione](/COME-04/Secondo_anno/20210308/)
- [Ottava Lezione](/COME-04/Secondo_anno/20210322/)
- [Nona Lezione](/COME-04/Secondo_anno/20210329/)
- [Decima Lezione](/COME-04/Secondo_anno/20210412/)
- [Undicesima Lezione](/COME-04/Secondo_anno/20210424/)
- [Dodicesima Lezione](/COME-04/Secondo_anno/20210503/)
- [Tredicesima Lezione](/COME-04/Secondo_anno/20210510/)
- [Quattordicesima Lezione](/COME-04/Secondo_anno/20210607/)

#### Progetto per l'esame di secondo biennio
- Realizzazione di un sistema senza fili economico per l'esecuzione della Musica Elettronica

[Testo presentato all'esame sulla Realizzazione di un sistema senza fili economico per l'esecuzione della Musica Elettronica](/COME-04/Secondo_anno/Secondo_anno/Materiali_esame_II_BN/2022-Davide_Tedesco-Realizzazione_di_un_sistema_senza_fili_economico_per_l'esecuzione_della_Musica_Elettronica.pdf)
___

## [COME-05](/COME-05)
#### _Informatica Musicale_

##### Prima annualità

_Maestro Pasquale Citera_

![foto](/COME-05/Primo_anno/20200611/FMcode.png)

- Filtri FIR
- Filtri IIR
- Filtro Biquad
- Filtro Comb
- Filtro Allpass
- [Il problema della riverberazione artificiale](https://www.dsprelated.com/freebooks/pasp/Artificial_Reverberation.html)
- I primi riverberi realizzati

[Schemi sugli argomenti trattati durante le lezioni del primo anno](https://certain-sweater-2c3.notion.site/Informatica-Musicale-I-ded1c08bbcf044dfa03cc79fd496a292)

[Testo presentato all'esame sulla Realizzazione di un riverbero quadrifonico](/COME-05/Primo_anno/esame_COME-05_primo_biennio/2022_Davide_Tedesco_Realizzazione_di_un_riverbero_quadrifonico)


##### Seconda annualità

_Maestro Riccardo Santoboni_

- [Prima Lezione](/COME-05/Secondo_anno/20210512/)
- [Seconda Lezione](/COME-05/Secondo_anno/20210519/)
- [Terza Lezione](/COME-05/Secondo_anno/20210526/)
- [Quarta Lezione](/COME-05/Secondo_anno/20210609/)
- [Quinta Lezione](/COME-05/Secondo_anno/20210616/)
- [Sesta Lezione](/COME-05/Secondo_anno/20210623/)
- [Settima Lezione](/COME-05/Secondo_anno/20210630/)
- [Ottava Lezione](/COME-05/Secondo_anno/20210911/)
- [Nona Lezione](/COME-05/Secondo_anno/20210916/)
- [Decima Lezione](/COME-05/Secondo_anno/20210923/)

[Testo presentato all'esame su La sintesi per modelli fisici degli strumenti musicali, l'interazione massa-molla](/COME-05/Secondo_anno/Materiali_esame_II_BN/2022-Davide_Tedesco-La_sintesi_per_modelli_fisici_degli_strumenti_musicali,_l'interazione_massa-molla.pdf)
___

## [CODC-01](/CODC-01)
#### _Analisi compositiva_

_Maestro Stefano Bracci_

- [Prima Lezione](/CODC-01/20201008)
- [Seconda Lezione](/CODC-01/20201015)
- [Terza Lezione](/CODC-01/20201022)
- [Quarta Lezione](/CODC-01/20201105)
- [Quinta Lezione](/CODC-01/20201112)
- [Sesta Lezione](/CODC-01/20201119)

##### Analisi realizzate

- [Breve analisi dei tre brani per quartetto d’archi di Igor Stravinskij](/CODC-01/20201119/2020_Davide_Tedesco_Breve_analisi_dei_tre_brani_per_quartetto_d'archi_di_Igor_Stravinskij/2020_Davide_Tedesco_Breve_analisi_dei_tre_brani_per_quartetto_d'archi_di_Igor_Stravinskij.pdf)
- [Breve analisi del terzo brano dell'Op.11 di Anton Webern](/CODC-01/20201203/2020_Davide_Tedesco_Breve_analisi_del_terzo_brano_dell'Op.11_di_Anton_Webern/2020_Davide_Tedesco_Breve_analisi_del_terzo_brano_dell'Op.11_di_Anton_Webern.pdf)
- [Grafizzazione dell'opera Op.11 III di Webern](/CODC-01/20210121/Grafizzazione_Webern_Op.11_III.pdf)
___

## [CODM-05](/CODM-05)
#### _Storia della musica elettroacustica_

_Maestro Luigino Pizzaleo_

- [Seminario sui software per l'analisi musicale](/CODM-05/Primo_anno/20200514/Seminario_software_per_l'analisi.md/)

##### Prima annualità
![paradigma_stock](/CODM-05/Primo_anno/Stockhausen_Paradigma/Stockhausen_I_prinicipi_costruttivi/schema.png)

- [Prima Lezione](/CODM-05/Primo_anno/20200521)
- [Seconda Lezione](/CODM-05/Primo_anno/20200528/)
- [Terza Lezione](/CODM-05/Primo_anno/20200604/)
- [Quarta Lezione](/CODM-05/Primo_anno/20200611/)
- [Quinta Lezione](/CODM-05/Primo_anno/20200618/)
- [Sesta Lezione](/CODM-05/Primo_anno/20200625/)
- [Settima Lezione](/CODM-05/Primo_anno/20200702/)
- [Ottava Lezione](/CODM-05/Primo_anno/20200706/)
- [Nona Lezione](/CODM-05/Primo_anno/20200709/)


- [Lezioni sul paradigma Stockhausen](/CODM-05/Primo_anno/Stockhausen_Paradigma)

[Schemi sugli argomenti trattati durante le lezioni del primo anno](https://www.notion.so/Storia-della-musica-elettroacustica-I-BN-6439c546c55c4ec2955802bb3a5c2d25)

##### Seconda annualità
![insula](/CODM-05/Secondo_anno/20210123/Paul_Klee_Insula_dulcamara.jpg)

- [Prima Lezione](/CODM-05/Secondo_anno/20201212/)
- [Seconda Lezione](/CODM-05/Secondo_anno/20201219/)
- [Terza Lezione](/CODM-05/Secondo_anno/20210109/)
- [Quarta Lezione](/CODM-05/Secondo_anno/20210116/)
- [Quinta Lezione](/CODM-05/Secondo_anno/20210123/)
- [Sesta Lezione](/CODM-05/Secondo_anno/20210130/)
- [Settima Lezione](/CODM-05/Secondo_anno/20210213/)
- [Ottava Lezione](/CODM-05/Secondo_anno/20210227/)
- [Nona Lezione](/CODM-05/Secondo_anno/20210313/)
- [Decima Lezione](/CODM-05/Secondo_anno/20210424/)

[Schemi sugli argomenti trattati durante le lezioni del secondo anno](https://certain-sweater-2c3.notion.site/Storia-della-musica-Elettroacustica-II-BN-556edf61243b4c3fa92b88e857ee9c3c)

---

## [COTP-06](/COTP-06)
#### _Ritmica della musica contemporanea_

_Maestro Silvio Mangiapelo_
